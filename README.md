# ClodeoWeb
Clodeo is an easy to use accounting software to help you manage your business faster and more efficiently, anytime anywhere. Clodeo built used Angular 6, Angular CLI and others tool.

### Requirement
- Node Version : 10.7.4
- NPM Version : 6.1.0
- Angular CLI : 6.1.3
- Yarn : 1.6.0

### Installation
```sh
$ git clone https://gitlab.com/clodeo/clodeo-frontend-main-web.git
$ clodeo-frontend-main-web
$ npm install
$ yarn install
```

### Deployment
```sh
$ available on Document WordOffice PM
```

### Notes Developer:
##### We used some development mode :
- Developer (dev)
- Production (prod)
- Test (test)
- Demo (demo)

### Running clodeo
```sh
$ ng serve or npm run start or yarn start listening on `http://localhost:4200/`
$ npm run start -- --prod (running mode production)
```

### Build clodeo
```sh
$ npm run build --prod (build mode production)
```

## Libraries:
Dillinger is currently extended with the following plugins. Instructions on how to use them in your own application are linked below.
#### Ui Template
| Plugin | Link |
| ------ | ------ |
| ng2centric | http://themicon.co/theme/centric/v1.9.5/angular5/dashboard |
#### Ui Components
| Plugin | Link |
| ------ | ------ |
| ng2centric | http://themicon.co/theme/centric/v1.9.5/angular5/dashboard |
| primeng | https://www.primefaces.org/primeng/#/|
| ngx-bootstrap | https://valor-software.com/ngx-bootstrap/#/|
#### Snackbar
| Plugin | Link |
| ------ | ------ |
| snackbar | https://github.com/polonel/SnackBar |
#### Chart
| Plugin | Link |
| ------ | ------ |
| ng2centric | http://themicon.co/theme/centric/v1.9.5/angular5/dashboard |
#### Translation
| Plugin | Link |
| ------ | ------ |
| ngx-translation | https://github.com/ngx-translate |
#### Others
| Plugin | Link |
| ------ | ------ |
| ngx-clipboard | https://github.com/maxisam/ngx-clipboard |
| ngx-webstorage | https://github.com/PillowPillow/ng2s-webstorage |
| ng-conditionally-validate | https://github.com/EliCDavis/ng-conditionally-validate |
| ngrx/store | https://github.com/ngrx/store |
| custom| some is custom library |
##### Running unit tests (Ignore)
Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

##### Running end-to-end tests (Ignore)
Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.
 
##### Code scaffolding (Ignore)
Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive/pipe/service/class/module`.

##### Further help (Ignore)
To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
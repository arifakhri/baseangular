export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyAstEsCAuxn30xHde_z2kzD8oMsg2uqQ_4',
    authDomain: 'clodeo-bot-dev.firebaseapp.com',
    databaseURL: 'https://clodeo-bot-dev.firebaseio.com',
    projectId: 'clodeo-bot-dev',
    storageBucket: 'clodeo-bot-dev.appspot.com',
    messagingSenderId: '488353602386',
  },
  ENDPOINTS: {
    API_ACCOUNT: 'https://clodeo-api-account-demo.azurewebsites.net',
    API_MAIN: 'https://clodeo-api-main-demo.azurewebsites.net',
    API_FILE: 'https://clodeo-api-file-demo.azurewebsites.net',
    API_REPORT: 'https://clodeo-api-report-demo.azurewebsites.net',
    API_SHIPPING: 'https://clodeo-dev-extsrv-shipping.azurewebsites.net',
    API_CHATBOT: 'https://clodeo-dev-extsrv-chatbot.azurewebsites.net',
    API_SALESCHANNEL: 'https://clodeo-dev-extsrv-saleschannel.azurewebsites.net',
    API_REPORT_RENDERER: 'http://52.76.212.61:8123',
    API_AUTO_PRINT: 'https://localhost:2501',
  },
};

// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyAstEsCAuxn30xHde_z2kzD8oMsg2uqQ_4',
    authDomain: 'clodeo-bot-dev.firebaseapp.com',
    databaseURL: 'https://clodeo-bot-dev.firebaseio.com',
    projectId: 'clodeo-bot-dev',
    storageBucket: 'clodeo-bot-dev.appspot.com',
    messagingSenderId: '488353602386',
  },
  ENDPOINTS: {
    API_ACCOUNT: 'https://clodeo-api-account-demo.azurewebsites.net',
    API_MAIN: 'https://clodeo-api-main-demo.azurewebsites.net',
    API_FILE: 'https://clodeo-api-file-demo.azurewebsites.net',
    API_REPORT: 'https://clodeo-api-report-demo.azurewebsites.net',
    // API_ACCOUNT: 'http://vm-dev.local:61001',
    // API_MAIN: 'http://vm-dev.local:61002',
    // API_FILE: 'http://vm-dev.local:61003',
    // API_REPORT: 'http://vm-dev.local:61004',
    API_SHIPPING: 'https://clodeo-dev-extsrv-shipping.azurewebsites.net',
    API_CHATBOT: 'https://clodeo-dev-extsrv-chatbot.azurewebsites.net',
    API_SALESCHANNEL: 'http://159.65.2.139:5555',
    API_REPORT_RENDERER: 'http://52.76.212.61:8123',
    API_AUTO_PRINT: 'https://localhost:2501',
  },
};

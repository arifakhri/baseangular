export const environment = {
  production: true,
  firebase: {
    apiKey: 'AIzaSyDzumumiN2UtAp1DzpnsE9-IvaQcUVOV2g',
    authDomain: 'clodeo-bot.firebaseapp.com',
    databaseURL: 'https://clodeo-bot.firebaseio.com',
    projectId: 'clodeo-bot',
    storageBucket: 'clodeo-bot.appspot.com',
    messagingSenderId: '1032875119211'
  },
  ENDPOINTS: {
    API_ACCOUNT: 'https://clodeo-prod-api-account.azurewebsites.net',
    API_MAIN: 'https://clodeo-prod-api-main.azurewebsites.net',
    API_FILE: 'https://clodeo-prod-api-file.azurewebsites.net',
    API_REPORT: 'https://clodeo-prod-api-report.azurewebsites.net',
    API_SHIPPING: 'https://clodeo-prod-extsrv-shipping.azurewebsites.net',
    API_CHATBOT: 'https://clodeo-prod-extsrv-chatbot.azurewebsites.net',
    API_SALESCHANNEL: 'https://clodeo-prod-extsrv-saleschannel.azurewebsites.net',
    API_REPORT_RENDERER: 'http://52.76.212.61:8123',
    API_AUTO_PRINT: 'http://localhost:2501',
  },
};

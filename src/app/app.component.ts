import * as _ from 'lodash';
import * as Domurl from 'domurl';
import { Component, OnInit } from '@angular/core';
import { UrlService } from './core/common/url.service';

import { ApiBootstrapService, AuthorizationService } from './core/core.module';
import { TranslatorService } from './core/common/translator.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  constructor(
    private _apiBoostrap: ApiBootstrapService,
    private _authorization: AuthorizationService,
    private _translator: TranslatorService,
    private _url: UrlService,
  ) { }

  ngOnInit() {
    const { urlOrigin, urlParams, urlPath } = this._url.extractHashParts();

    if (urlPath !== '/boot' && !this._authorization.publicRoutePaths.includes(urlPath)) {
      const redirectUrl = new Domurl(`${urlOrigin}/boot`);
      redirectUrl.query.sourcePath = urlPath;
      if (_.size(urlParams)) {
        redirectUrl.query.sourceQuery = urlParams;
      }

      window.location.href = `${urlOrigin}/#${redirectUrl.path}?${redirectUrl.query.toString()}`;
    }
  }
}

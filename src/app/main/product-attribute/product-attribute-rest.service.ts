import { Injectable } from '@angular/core';
import 'app/rxjs-imports.ts';

import { ApiQueryOption, RequestService } from '../../core/core.module';

import { APP_CONST } from '../../app.const';

@Injectable()
export class ProductAttributeRestService {
  baseURL = `${APP_CONST.API_MAIN}/product-attributes`;
  request = this._request.new(this.baseURL);

  constructor(
    private _request: RequestService,
  ) { }

  create(productAttribute: IProductAttribute) {
    return this.request.post<IProductAttribute>(``, productAttribute);
  }

  findAll(queryOption: ApiQueryOption) {
    return this.request.post<IApiPaginationResult<IProductAttribute>>(`q`, queryOption);
  }

  load(productAttributeId: string) {
    return this.request.get<IProductAttribute>(`${productAttributeId}`);
  }

  update(productAttributeId: string, updateObj: IProductAttribute) {
    return this.request.put<IProductAttribute>(`${productAttributeId}`, updateObj);
  }

  delete(productAttributeId: string) {
    return this.request.delete<any>(`${productAttributeId}`);
  }

  toggleInactive(productAttributeId: string, inactive: boolean) {
    return this.request.put<any>(`${productAttributeId}/mark-active/${!inactive}`, null, {
      headers: {
        'Content-Type': 'application/json'
      }
    });
  }
}

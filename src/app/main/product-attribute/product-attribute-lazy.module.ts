import { NgModule } from '@angular/core';

import { ProductAttributeModule } from './product-attribute.module';
import { ProductAttributeRoutingModule } from './product-attribute-routing.module';

@NgModule({
  imports: [
    ProductAttributeModule,
    ProductAttributeRoutingModule
  ]
})
export class ProductAttributeLazyModule { }

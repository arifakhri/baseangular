import { Component } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { BaseFormBComponent } from '../../../shared/base/base.module';

@Component({
  selector: 'app-product-attribute-form',
  templateUrl: 'product-attribute-form.component.html'
})
export class ProductAttributeFormComponent extends BaseFormBComponent {
  constructor() {
    super();
    this.componentId = 'ProductAttributeForm';

    this.registerHook('buildForm', event => {
      this.buildForm();
    });
  }

  buildForm() {
    const nameControl = new FormControl('', [Validators.required, Validators.maxLength(50)]);
    (<any>nameControl).validatorData = {
      maxLength: 50
    };

    this.form.addControl('name', nameControl);
    this.form.addControl('inactive', new FormControl(false));
  }
}

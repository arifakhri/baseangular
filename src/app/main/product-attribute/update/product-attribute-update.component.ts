import * as _ from 'lodash';
import { Component } from '@angular/core';

import { ProductAttributeRestService } from '../product-attribute-rest.service';

import { BaseUpdateBComponent } from '../../../shared/base/base.module';
import { MProductAttribute } from '../product-attribute.model';

@Component({
  selector: 'app-product-attribute-update',
  templateUrl: 'product-attribute-update.component.html'
})
export class ProductAttributeUpdateComponent extends BaseUpdateBComponent {

  constructor(
    private _productAttributeRest: ProductAttributeRestService,
  ) {
    super();
    this.componentId = 'ProductAttributeUpdateUpdate';
    this.routeURL = '/product-attributes';
    this.entrySuccessI18n = 'success.productAttribute.update';

    this.headerTitle = 'ui.productAttribute.update.title';

    this.registerHook('load', event => {
      return this._productAttributeRest.load(this.page.routeParams.id);
    });

    this.registerHook('save', event => {
      const formValue = this.form.value;
      let productAttribute = new MProductAttribute;
      productAttribute = _.assign(productAttribute, this.doc, formValue);

      return this._productAttributeRest.update(this.page.routeParams.id, productAttribute);
    });
  }

}

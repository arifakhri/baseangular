import { BsDropdownModule, PaginationModule } from 'ngx-bootstrap';
import { CommonModule } from '@angular/common';
import { DataTableModule } from 'primeng/components/datatable/datatable';
import { ListboxModule } from 'primeng/components/listbox/listbox';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { ProductAttributeCreateComponent } from './create/product-attribute-create.component';
import { ProductAttributeFormComponent } from './form/product-attribute-form.component';
import { ProductAttributeListComponent } from './list/product-attribute-list.component';
import { ProductAttributeUpdateComponent } from './update/product-attribute-update.component';

import { ProductAttributeQuickCreateComponent } from './quick-create/product-attribute-quick-create.component';
import { ProductAttributeQuickFormComponent } from './quick-form/product-attribute-quick-form.component';

import { ProductAttributeRestService } from './product-attribute-rest.service';

import { CoreModule } from '../../core/core.module';
import { SharedModule } from '../../shared/shared.module';

export const PROVIDERS = [
  ProductAttributeRestService,
];

@NgModule({
  imports: [
    BsDropdownModule,
    CommonModule,
    FlexLayoutModule,
    PaginationModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    TranslateModule,
    DataTableModule,
    ListboxModule,
    CoreModule,
    SharedModule,
  ],
  declarations: [
    ProductAttributeCreateComponent,
    ProductAttributeFormComponent,
    ProductAttributeListComponent,
    ProductAttributeUpdateComponent,

    ProductAttributeQuickCreateComponent,
    ProductAttributeQuickFormComponent
  ],
  exports: [
    ProductAttributeQuickCreateComponent
  ],
})
export class ProductAttributeModule { }

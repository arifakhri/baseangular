import * as _ from 'lodash';
import { Component } from '@angular/core';

import { ProductAttributeRestService } from '../product-attribute-rest.service';

import { BaseCreateBComponent } from '../../../shared/base/base.module';
import { MProductAttribute } from '../product-attribute.model';

@Component({
  selector: 'app-product-attribute-create',
  templateUrl: 'product-attribute-create.component.html'
})
export class ProductAttributeCreateComponent extends BaseCreateBComponent {

  constructor(
    private _productAttributeRest: ProductAttributeRestService,
  ) {
    super();

    this.componentId = 'ProductAttributeCreate';
    this.routeURL = '/product-attributes';
    this.entrySuccessI18n = 'success.productAttribute.create';

    this.headerTitle = 'ui.productAttribute.create.title';

    this.registerHook('save', event => {
      const formValue = this.form.value;
      let productAttribute = new MProductAttribute;
      productAttribute = _.assign(productAttribute, formValue);

      return this._productAttributeRest.create(productAttribute);
    });
  }
}

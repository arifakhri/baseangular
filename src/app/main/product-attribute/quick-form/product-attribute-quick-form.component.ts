import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-product-attribute-quick-form',
  templateUrl: 'product-attribute-quick-form.component.html'
})
export class ProductAttributeQuickFormComponent implements OnInit {
  @Input() form: FormGroup;

  ngOnInit() {
    this.buildForm();
  }

  buildForm() {
    const nameControl = new FormControl('', [Validators.required, Validators.maxLength(50)]);
    (<any>nameControl).validatorData = {
      maxLength: 50
    };

    this.form.addControl('name', nameControl);
  }
}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ProductAttributeCreateComponent } from './create/product-attribute-create.component';
import { ProductAttributeListComponent } from './list/product-attribute-list.component';
import { ProductAttributeUpdateComponent } from './update/product-attribute-update.component';

import { AuthorizationService } from '../../core/core.module';

export const routes: Routes = [{
  path: '',
  children: [{
    path: '',
    component: ProductAttributeListComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'productAttribute.list'
    }
  }, {
    path: 'create',
    component: ProductAttributeCreateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'productAttribute.create'
    }
  }, {
    path: ':id/update',
    component: ProductAttributeUpdateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'productAttribute.update'
    }
  }]
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class ProductAttributeRoutingModule { }

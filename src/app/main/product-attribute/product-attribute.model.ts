export class MProductAttribute {
    id: string;
    name: string;
    inactive: boolean;
  }
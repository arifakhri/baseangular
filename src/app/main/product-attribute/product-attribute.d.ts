declare interface IProductAttribute {
  id: string;
  name: string;
  inactive: boolean;
}
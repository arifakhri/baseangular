import { Component, EventEmitter, Inject, Input, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

import { CommonService, SystemMessageService } from '../../../core/core.module';
import { PurchasesDownPaymentRestService } from '../purchases-down-payment-rest.service';
import { PurchasesDownPaymentService } from '../purchases-down-payment.service';

@Component({
  selector: 'app-purchases-down-payment-quick-create',
  templateUrl: 'purchases-down-payment-quick-create.component.html',
  providers: [SystemMessageService]
})
export class PurchasesDownPaymentQuickCreateComponent implements OnInit {
  @Input() sourceTransaction: IPurchasesOrder;

  @Output() afterSubmit: EventEmitter<IPurchasesDownPayment> = new EventEmitter();
  @Output() close: EventEmitter<boolean> = new EventEmitter();

  form: FormGroup;
  compLoading: boolean = false;

  constructor(
    @Inject('GlobalSystemMessage') private _globalSystemMessage: SystemMessageService,
    private _router: Router,
    private _purchasesDownPayment: PurchasesDownPaymentService,
    private _purchasesDownPaymentRest: PurchasesDownPaymentRestService,
    public _systemMessage: SystemMessageService,
    private _translate: TranslateService
  ) { }

  ngOnInit() {
    this.form = new FormGroup({});

    this._purchasesDownPayment.setFormDefinitions(this.form);

    if (this.sourceTransaction) {
      this.form.patchValue({
        orderId: this.sourceTransaction.id,
        vendorEmail: this.sourceTransaction.vendorEmail,
      });
      if (this.sourceTransaction.vendor) {
        this.form.patchValue({
          vendor: this.sourceTransaction.vendor,
          vendorId: this.sourceTransaction.vendorId,
        });
      }
    }
  }

  onSubmit() {
    if (this.formValid()) {
      this.compLoading = true;
      this.save();
    }
  }

  formValid(): boolean {
    if (!this.form.valid) {
      CommonService.markAsDirty(this.form);
      this._systemMessage.log({
        message: this._translate.instant('error.form'),
        type: 'error'
      });
      return false;
    }
    return true;
  }

  save(): void {
    const downPayment: IPurchasesDownPayment = this.form.value;
    this._purchasesDownPayment.normalizeDoc(downPayment);

    this._purchasesDownPaymentRest.create(downPayment)
      .catch(error => {
        this._systemMessage.log({
          message: error,
          type: 'error'
        });
        this.compLoading = false;
        return Observable.throw(error);
      })
      .subscribe(record => {
        this._globalSystemMessage.log({
          message: this._translate.instant('success.purchasesDownPayment.quickCreate'),
          type: 'success',
          showAs: 'growl',
          showSnackBar: false,
        });
        this.compLoading = false;
        this.afterSubmit.emit(record);
        this.form.reset();
      });
  }
}

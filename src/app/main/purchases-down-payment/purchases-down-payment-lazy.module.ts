import { NgModule } from '@angular/core';

import { PurchasesDownPaymentModule } from './purchases-down-payment.module';
import { PurchasesDownPaymentRoutingModule } from './purchases-down-payment-routing.module';

@NgModule({
  imports: [
    PurchasesDownPaymentModule,
    PurchasesDownPaymentRoutingModule
  ]
})
export class PurchasesDownPaymentLazyModule { }

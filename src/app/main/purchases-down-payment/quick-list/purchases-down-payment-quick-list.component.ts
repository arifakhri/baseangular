import * as _ from 'lodash';
import { Component, Input, OnInit } from '@angular/core';

import { PurchasesDownPaymentRestService } from '../../purchases-down-payment/purchases-down-payment-rest.service';

import { AccountingService, } from '../../../core/core.module';

@Component({
  selector: 'app-purchases-down-payment-quick-list',
  templateUrl: './purchases-down-payment-quick-list.component.html',
})
export class PurchasesDownPaymentQuickListComponent implements OnInit {
  @Input() transaction: any;

  downPayments: IPurchasesDownPayment[] = [];
  total: number = 0;

  constructor(
    public _accounting: AccountingService,
    private _purchasesDownPaymentRest: PurchasesDownPaymentRestService,
  ) { }

  ngOnInit() {
    this._purchasesDownPaymentRest.findAll({
        filter: [],
        sort: [],
        take: 100,
        includeTotalCount: false,
        skip: 0,
      }, { orderId: this.transaction.id }).subscribe(downPayments => {
      this.downPayments = downPayments.data;
      this.total = _.sumBy(this.downPayments, 'total');
    });
  }
}

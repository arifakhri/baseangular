import { AutoCompleteModule } from 'primeng/components/autocomplete/autocomplete';
import { CalendarModule } from 'primeng/components/calendar/calendar';
import { CheckboxModule } from 'primeng/components/checkbox/checkbox';
import { DataTableModule } from 'primeng/components/datatable/datatable';
import { BsDropdownModule, ModalModule, PaginationModule } from 'ngx-bootstrap';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { PurchasesDownPaymentCreateComponent } from './create/purchases-down-payment-create.component';
import { PurchasesDownPaymentDetailComponent } from './detail/purchases-down-payment-detail.component';
import { PurchasesDownPaymentFormComponent } from './form/purchases-down-payment-form.component';
import { PurchasesDownPaymentQuickCreateComponent } from './quick-create/purchases-down-payment-quick-create.component';
import { PurchasesDownPaymentQuickFormComponent } from './quick-form/purchases-down-payment-quick-form.component';
import { PurchasesDownPaymentQuickListComponent } from './quick-list/purchases-down-payment-quick-list.component';
import { PurchasesDownPaymentUpdateComponent } from './update/purchases-down-payment-update.component';

import { PurchasesDownPaymentService } from './purchases-down-payment.service';
import { PurchasesDownPaymentRestService } from './purchases-down-payment-rest.service';

import { BusinessPartnerModule } from '../business-partner/business-partner.module';
import { CoreModule } from '../../core/core.module';
import { PaymentMethodModule } from '../payment-method/payment-method.module';
import { PurchasesModule } from '../purchases/purchases.module';
import { SharedModule } from '../../shared/shared.module';
import { TransactionModule } from '../transaction/transaction.module';

export const PROVIDERS = [
  PurchasesDownPaymentService,
  PurchasesDownPaymentRestService,
];

@NgModule({
  imports: [
    AutoCompleteModule,
    BsDropdownModule,
    BusinessPartnerModule,
    CalendarModule,
    CheckboxModule,
    CommonModule,
    CoreModule,
    DataTableModule,
    FlexLayoutModule,
    FormsModule,
    HttpModule,
    ModalModule,
    PaginationModule,
    PaymentMethodModule,
    PurchasesModule,
    ReactiveFormsModule,
    RouterModule,
    SharedModule,
    TransactionModule,
    TranslateModule,
  ],
  declarations: [
    PurchasesDownPaymentCreateComponent,
    PurchasesDownPaymentDetailComponent,
    PurchasesDownPaymentFormComponent,
    PurchasesDownPaymentQuickCreateComponent,
    PurchasesDownPaymentQuickFormComponent,
    PurchasesDownPaymentQuickListComponent,
    PurchasesDownPaymentUpdateComponent,
  ],
  exports: [
    PurchasesDownPaymentQuickCreateComponent,
    PurchasesDownPaymentQuickListComponent,
  ]
})
export class PurchasesDownPaymentModule { }

import * as _ from 'lodash';
import { Component, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import 'app/rxjs-imports.ts';

import { PurchasesDownPaymentRestService } from '../purchases-down-payment-rest.service';
import { PurchasesDownPaymentService } from '../purchases-down-payment.service';

import { BaseCreateBComponent } from '../../../shared/base/base.module';
import { MPurchasesDownPayment } from '../purchases-down-payment.model';

@Component({
  selector: 'app-purchases-down-payment-create',
  templateUrl: 'purchases-down-payment-create.component.html'
})
export class PurchasesDownPaymentCreateComponent extends BaseCreateBComponent {
  @ViewChild('reportModal') elReportModal: ModalDirective;

  constructor(
    private _purchasesDownPayment: PurchasesDownPaymentService,
    private _purchasesDownPaymentRest: PurchasesDownPaymentRestService
  ) {
    super();

    this.componentId = 'PurchasesDownPaymentCreate';
    this.headerTitle = 'ui.purchasesDownPayment.create.title';
    this.containerType = 1;
    this.routeURL = '/purchases/payments';
    this.entrySuccessI18n = 'success.purchasesDownPayment.create';

    this.autoprintButtons = [{
      checkboxConfigId: 'autoprint.sdownpayment.create',
      checkboxLabel: 'ui.purchasesDownPayment.create.action.apCheckbox.report',
      previewLabel: 'ui.purchasesDownPayment.create.action.previewReport',
      onPreview: () => {
        this.saveAndPrint();
      }
    }];

    this.registerHook('buildForm', event => {
      this._purchasesDownPayment.setFormDefinitions(this.form);
    });


    this.registerHook('save', event => {
      let downPayment = new MPurchasesDownPayment;
      downPayment = _.assign(downPayment, this.form.value);
      this._purchasesDownPayment.normalizeDoc(downPayment);

      const isPrinting = _.get(event, 'data.print');

      return this._purchasesDownPaymentRest.create(downPayment, { includeLines: isPrinting });
    });
  }

  async saveAndPrint() {
    try {
      const submitted = await this.onSubmit({ saveConfirm: true, print: true });
      if (submitted) {
        this._purchasesDownPaymentRest.loadRelatedData().subscribe(related => {
          this.doc['companyInfo'] = related.settings.companyInfo;
          this.elReportModal.show();
        });
      }
    } catch (e) {

    }
  }
}

import * as _ from 'lodash';
import { AutoComplete } from 'primeng/components/autocomplete/autocomplete';
import { ChangeDetectorRef, Component, OnChanges, OnInit, SimpleChanges, ViewChild, } from '@angular/core';
import 'app/rxjs-imports.ts';

import { AccountingService, AutocompleteService, CommonService } from '../../../core/core.module';
import { PurchasesDownPaymentRestService } from '../purchases-down-payment-rest.service';
import { PurchasesOrderRestService } from '../../purchases-order/purchases-order-rest.service';

import { TransactionFormPurchasesBComponent } from '../../transaction/transaction-form-purchases.bcomponent';

@Component({
  selector: 'app-purchases-down-payment-form',
  templateUrl: 'purchases-down-payment-form.component.html'
})
export class PurchasesDownPaymentFormComponent extends TransactionFormPurchasesBComponent implements OnChanges, OnInit {
  @ViewChild('orderAC') elOrderAC: AutoComplete;

  cloneTransaction: boolean = false;

  ACProductVariantHandler = CommonService.remoteACItemsHandler({
    remoteParams: this.ACProductVariantParams.bind(this),
    remoteRequest: this._productVariantRest.findAllPicker.bind(this._productVariantRest),
    remoteRequestMap: (response) => response.data,
  });

  ACOrderHandler = CommonService.remoteACItemsHandler({
    remoteParams: this.ACOrderParams.bind(this),
    remoteRequest: this._purchasesOrderRest.findAll.bind(this._purchasesOrderRest),
    remoteRequestMap: (response) => response.data,
    element: () => this.elOrderAC,
  });

  constructor(
    public _autocomplete: AutocompleteService,
    public _accounting: AccountingService,
    private _changeDetectorRef: ChangeDetectorRef,
    private _purchasesDownPaymentRest: PurchasesDownPaymentRestService,
    private _purchasesOrderRest: PurchasesOrderRestService,
  ) {
    super();
    super.init();
    this.componentId = 'PurchasesDownPaymentForm';

    this.registerHook('loadRelated', event => {
      return this._purchasesDownPaymentRest.loadRelatedData();
    });

    this.registerHook('patchForm', event => {
      this.DPApplyDoc();
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (_.has(changes, 'doc') && this.doc) {
      this.DPApplyDoc();
    }
  }

  DPApplyDoc() {
    this.form.patchValue({
      downPaymentAmount: this.doc.total,
    });
  }

  onVendorClear() {
    this.form.get('vendorId').reset();
    this.form.get('order').reset();
    this.form.get('orderId').reset();
  }

  onVendorSelected(vendor: IBusinessPartner) {
    this.form.get('vendorId').setValue(vendor.id);
    this.form.get('order').reset();
    this.form.get('orderId').reset();
    if (vendor.email) {
      this.form.get('vendorEmail').setValue(vendor.email);
    }
    this._changeDetectorRef.detectChanges();
  }

  ACOrderParams(event: any, type: string) {
    let filters = [];

    if (type === 'search') {
      filters = [{
        filterValues: [{
          field: 'transactionNumber',
          operator: 'contains',
          value: event.query
        }]
      }];
    }

    const vendor = this.form.get('vendor').value;

    return [{
      filter: filters,
      sort: [{
        field: 'transactionNumber',
        dir: 'asc'
      }],
      take: 30,
      skip: 0,
      includeTotalCount: false
    }, {
      vendorId: vendor ? vendor.id : '',
    }];
  }

  onOrderSelected(order: IPurchasesOrder) {
    this.form.get('orderId').setValue(order.id);
    this.form.get('vendor').setValue(order.vendor);
    this.form.get('vendorId').setValue(order.vendor.id);

    this._changeDetectorRef.detectChanges();
    this.syncAmountPercent();
  }

  syncAmountPercent() {
    const targetOrder: IPurchasesOrder = this.form.get('order').value;
    if (targetOrder) {
      const percent: number = this.form.get('downPaymentAmountPercent').value || 0;
      if (percent > 0) {
        const targetAmount = targetOrder.total * percent / 100;
        this.form.get('downPaymentAmount').setValue(targetAmount);
      }
    }
  }
}

import * as _ from 'lodash';
import * as moment from 'moment';
import { Component, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';

import { PurchasesDownPaymentRestService } from '../purchases-down-payment-rest.service';
import { PurchasesDownPaymentService } from '../purchases-down-payment.service';

import { BaseUpdateBComponent } from '../../../shared/base/base.module';

@Component({
  selector: 'app-purchases-down-payment-update',
  templateUrl: 'purchases-down-payment-update.component.html',
})
export class PurchasesDownPaymentUpdateComponent extends BaseUpdateBComponent {
  @ViewChild('reportModal') elReportModal: ModalDirective;

  constructor(
    private _purchasesDownPayment: PurchasesDownPaymentService,
    private _purchasesDownPaymentRest: PurchasesDownPaymentRestService,
  ) {
    super();

    this.componentId = 'PurchasesDownPaymentUpdate';
    this.headerTitle = 'ui.purchasesDownPayment.update.title';
    this.containerType = 1;
    this.routeURL = '/purchases/payments';
    this.entrySuccessI18n = 'success.purchasesDownPayment.update';

    this.registerHook('buildForm', event => {
      this._purchasesDownPayment.setFormDefinitions(this.form);
    });

    this.registerHook('load', event => {
      return this._purchasesDownPaymentRest.load(this.page.routeParams.id).switchMap(doc => {
        if (doc.transactionDate) {
          doc.transactionDate = moment(doc.transactionDate).toDate();
        }
        return Observable.of(doc);
      });
    });

    this.registerHook('save', event => {
      const downPayment: IPurchasesDownPayment = Object.assign({}, this.doc, this.form.value);
      this._purchasesDownPayment.normalizeDoc(downPayment);

      const isPrinting = _.get(event, 'data.print');

      return this._purchasesDownPaymentRest.update(this.page.routeParams.id, downPayment, { includeLines: isPrinting });
    });
  }

  async saveAndPrint() {
    try {
      const submitted = await this.onSubmit({ saveConfirm: true, print: true });
      if (submitted) {
        this._purchasesDownPaymentRest.loadRelatedData().subscribe(related => {
          this.doc['companyInfo'] = related.settings.companyInfo;
          this.elReportModal.show();
        });
      }
    } catch (e) {

    }
  }
}

import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { AccountingService } from '../../../core/core.module';
import { AdminLayoutService } from '../../../layouts/layouts.module';
import { PurchasesDownPaymentRestService } from '../purchases-down-payment-rest.service';

@Component({
  selector: 'app-purchases-down-payment-detail',
  templateUrl: 'purchases-down-payment-detail.component.html'
})
export class PurchasesDownPaymentDetailComponent implements OnInit {
  doc: IPurchasesDownPayment;
  routeParams: any;

  constructor(
    public _accounting: AccountingService,
    private _adminLayout: AdminLayoutService,
    private _purchasesDownPaymentRest: PurchasesDownPaymentRestService,
    private _route: ActivatedRoute,
  ) {
    this._adminLayout.containerType = 1;
    this._route.params.subscribe(params => this.routeParams = params);
  }

  ngOnInit() {
    this.loadData();
  }

  loadData() {
    this._purchasesDownPaymentRest.load(this.routeParams.id).subscribe(receipt => {
      this.doc = receipt;
    });
  }
}

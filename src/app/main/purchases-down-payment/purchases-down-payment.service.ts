import * as moment from 'moment';
import { CustomValidators } from 'ng2-validation';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Injectable } from '@angular/core';

import { StartingDataService } from '../../core/core.module';

@Injectable()
export class PurchasesDownPaymentService {
  constructor(
    private _startingData: StartingDataService,
  ) { }

  setFormDefinitions(form: FormGroup) {
    const startingData = this._startingData.data$.getValue();
    const vendorEmailControl = new FormControl('', [CustomValidators.email, Validators.maxLength(150)]);
    (<any>vendorEmailControl).validatorData = {
      maxLength: 150
    };
    const vendorRefNumberControl = new FormControl('', Validators.maxLength(30));
    (<any>vendorRefNumberControl).validatorData = {
      maxLength: 30
    };

    form.addControl('branchId', new FormControl(startingData.masterBranchId));
    form.addControl('downPaymentAmountPercent', new FormControl('', CustomValidators.number));
    form.addControl('downPaymentAmount', new FormControl('', [Validators.required, CustomValidators.number]));
    form.addControl('order', new FormControl);
    form.addControl('orderId', new FormControl(null));
    form.addControl('vendor', new FormControl(null));
    form.addControl('vendorId', new FormControl(null, Validators.required));
    form.addControl('vendorEmail', vendorEmailControl);
    form.addControl('description', new FormControl);
    form.addControl('paymentMethod', new FormControl);
    form.addControl('paymentMethodId', new FormControl(null));
    form.addControl('paymentAccount', new FormControl(null));
    form.addControl('paymentAccountId', new FormControl(null, Validators.required));
    form.addControl('transactionNumber', new FormControl);
    form.addControl('transactionDate', new FormControl(new Date(), Validators.required));
    form.addControl('vendorRefNumber', vendorRefNumberControl);
    form.addControl('vendorNote', new FormControl);
    form.addControl('note', new FormControl);

    form.get('paymentMethod').valueChanges.subscribe(paymentMethod => {
      if (paymentMethod && paymentMethod.defaultAccount) {
        form.get('paymentAccount').setValue(paymentMethod.defaultAccount);
        form.get('paymentAccountId').setValue(paymentMethod.defaultAccount.id);
      }
    });
  }

  normalizeDoc(record: any) {
    if (record.transactionDate) {
      record.transactionDate = moment(record.transactionDate).format('YYYY-MM-DD');
    }
  }
}

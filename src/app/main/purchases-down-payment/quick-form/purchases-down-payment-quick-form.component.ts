import { AutoComplete } from 'primeng/components/autocomplete/autocomplete';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ModalDirective } from 'ngx-bootstrap';

import { AutocompleteService } from '../../../core/core.module';
import { PurchasesDownPaymentRestService } from '../purchases-down-payment-rest.service';

@Component({
  selector: 'app-purchases-down-payment-quick-form',
  templateUrl: 'purchases-down-payment-quick-form.component.html'
})
export class PurchasesDownPaymentQuickFormComponent implements OnInit {
  @Input() form: FormGroup;
  @ViewChild('quickCreateModal') elQuickCreateModal: ModalDirective;

  quickCreateAC: AutoComplete;

  accounts: IAccount[] = [];
  paymentMethods: IPaymentMethod[] = [];

  constructor(
    public _autocomplete: AutocompleteService,
    private _purchasesPaymentRest: PurchasesDownPaymentRestService,
  ) { }

  ngOnInit() {
    this.loadRelatedData();
  }

  assignQuickCreatedEntity(entity: any, storageVar: string) {
    this[storageVar].push(entity);
    this.quickCreateAC.selectItem(entity);
    this.elQuickCreateModal.hide();
  }

  loadRelatedData() {
    this._purchasesPaymentRest.loadRelatedData().subscribe(related => {
      this.accounts = related.paymentAccounts;
      this.paymentMethods = [null].concat(related.paymentMethods);
    });
  }
}

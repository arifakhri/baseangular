import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PurchasesDownPaymentCreateComponent } from './create/purchases-down-payment-create.component';
import { PurchasesDownPaymentDetailComponent } from './detail/purchases-down-payment-detail.component';
import { PurchasesDownPaymentUpdateComponent } from './update/purchases-down-payment-update.component';

import { AuthorizationService } from '../../core/core.module';

export const routes: Routes = [{
  path: '',
  children: [{
    path: 'create',
    component: PurchasesDownPaymentCreateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'purchasesDownPayment.create'
    }
  }, {
    path: ':id',
    component: PurchasesDownPaymentDetailComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'purchasesDownPayment.detail'
    }
  }, {
    path: ':id/update',
    component: PurchasesDownPaymentUpdateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'purchasesDownPayment.update'
    }
  }]
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class PurchasesDownPaymentRoutingModule { }

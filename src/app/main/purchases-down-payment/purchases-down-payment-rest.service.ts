import { Injectable } from '@angular/core';
import 'app/rxjs-imports.ts';

import { ApiQueryOption, RequestService } from '../../core/core.module';

import { APP_CONST } from '../../app.const';

@Injectable()
export class PurchasesDownPaymentRestService {
  baseURL = `${APP_CONST.API_MAIN}/purchases/downpayments`;
  request = this._request.new(this.baseURL);

  constructor(
    private _request: RequestService,
  ) { }

  create(payment: IPurchasesDownPayment, queryParams: any = {}) {
    return this.request.post<IPurchasesDownPayment>(``, payment, { params: queryParams });
  }

  findAll(queryOption: ApiQueryOption, queryParams: any = {}) {
    return this.request.post<IApiPaginationResult<IPurchasesDownPayment>>(`q`, queryOption, { params: queryParams });
  }

  load(paymentId: string) {
    return this.request.get<IPurchasesDownPayment>(`${paymentId}`);
  }

  loadRelatedData() {
    return this.request.get<{ paymentMethods: IPaymentMethod[], paymentAccounts: IAccount[], settings: ISettings }>(`entry-related-data`);
  }

  update(paymentId: string, updateObj: IPurchasesDownPayment, queryParams: any = {}) {
    return this.request.put<IPurchasesDownPayment>(`${paymentId}`, updateObj, { params: queryParams });
  }

  void(paymentId: string) {
    return this.request.put<IPurchasesDownPayment>(`${paymentId}/void`, {});
  }
}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ReportsSalesOrderDetailPreviewComponent } from './preview/reports-sales-order-detail-preview.component';

import { AuthorizationService } from '../../core/core.module';

const routes: Routes = [{
  path: '',
  children: [{
    path: '',
    component: ReportsSalesOrderDetailPreviewComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'reportsSalesOrder.preview'
    }
  }]
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ],
})
export class ReportsSalesOrderDetailRoutingModule { }

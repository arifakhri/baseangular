import { NgModule } from '@angular/core';

import { ReportsSalesOrderDetailModule } from './reports-sales-order-detail.module';
import { ReportsSalesOrderDetailRoutingModule } from './reports-sales-order-detail-routing.module';

@NgModule({
  imports: [
    ReportsSalesOrderDetailModule,
    ReportsSalesOrderDetailRoutingModule,
  ],
})
export class ReportsSalesOrderDetailLazyModule { }

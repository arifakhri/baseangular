declare interface IReportsSalesOrderDetail {
  accountClass: string;
  accountType: string;
  accountName: string;
  total: 0;
}
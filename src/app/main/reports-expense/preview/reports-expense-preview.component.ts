import * as moment from 'moment';
import { AutoComplete } from 'primeng/components/autocomplete/autocomplete';
import { BusinessPartnerRestService } from '../../business-partner/business-partner-rest.service';
import { COARestService } from '../../coa/coa-rest.service';
import { Component, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

import { AdminLayoutService } from '../../../layouts/layouts.module';
import { CommonService, GridTableFilterService } from '../../../core/core.module';
import { ReportsExpenseRestService } from '../reports-expense-rest.service';
import { TransactionService } from '../../transaction/transaction.service';

@Component({
  selector: 'app-reports-expense-preview',
  templateUrl: './reports-expense-preview.component.html',
})
export class ReportsExpensePreviewComponent {
  @ViewChild('payeeAC') elPayeeAC: AutoComplete;
  @ViewChild('paymentAccountAC') elPaymentAccountAC: AutoComplete;
  @ViewChild('reportViewer') elReportViewer: any;

  compReady: boolean = false;
  data: any = [];
  form: FormGroup = new FormGroup({});
  reportViewerReady: boolean = false;

  reportParams: any = {
    title: 'Expense Summary'
  };

  ACPayeeHandler = CommonService.remoteACItemsHandler({
    remoteParams: this.ACPayeeParams.bind(this),
    remoteRequest: this._businessPartnerRest.findAll.bind(this._businessPartnerRest),
    remoteRequestMap: (response) => response.data,
    element: () => this.elPayeeAC,
  });

  ACPaymentAccountHandler = CommonService.remoteACItemsHandler({
    remoteParams: this.ACPaymentAccountParams.bind(this),
    remoteRequest: this._coaRest.findAll.bind(this._coaRest),
    remoteRequestMap: (response) => response.data,
    element: () => this.elPaymentAccountAC,
  });

  filtersMap: IGridTableFilterMap = {
    lowDate: {
      targetVar: 'date',
      targetFilter: 'param',
      dateFormat: 'YYYY-MM-DD',
    },
    highDate: {
      targetVar: 'date',
      targetFilter: 'param',
      dateFormat: 'YYYY-MM-DD',
    },
    paymentAccountId: {
      targetFilter: 'param',
    },
    payeeId: {
      targetFilter: 'param',
    },
    transactionNumber: {
      targetFilter: 'param',
    },
  };

  constructor(
    private _adminLayout: AdminLayoutService,
    private _businessPartnerRest: BusinessPartnerRestService,
    private _coaRest: COARestService,
    private _gridTableFilter: GridTableFilterService,
    private _reportsExpenseRest: ReportsExpenseRestService,
    public _transaction: TransactionService,
  ) {
    this._adminLayout.containerType = 1;

    this.loadData();
    this.buildForm();
  }

  buildForm() {
    this.form.addControl('lowDate', new FormControl(moment().set('date', 1).toDate()));
    this.form.addControl('highDate', new FormControl(new Date()));
    this.form.addControl('payee', new FormControl);
    this.form.addControl('payeeId', new FormControl);
    this.form.addControl('paymentAccount', new FormControl);
    this.form.addControl('paymentAccountId', new FormControl);
    this.form.addControl('transactionNumber', new FormControl(''));
  }

  ACPayeeParams(event: any, type: string) {
    let filters = [];

    if (type === 'search') {
      filters = [{
        filterValues: [{
          field: 'displayName',
          operator: 'contains',
          value: event.query
        }]
      }];
    }

    return [{
      filter: filters,
      sort: [{
        field: 'displayName',
        dir: 'asc'
      }],
      take: 30,
      skip: 0,
      includeTotalCount: false
    }];
  }

  ACPaymentAccountParams(event: any, type: string) {
    let filters = [];

    if (type === 'search') {
      filters = [{
        filterValues: [{
          field: 'name',
          operator: 'contains',
          value: event.query
        }]
      }];
    }

    return [{
      filter: filters,
      sort: [{
        field: 'name',
        dir: 'asc'
      }],
      take: 30,
      skip: 0,
      includeTotalCount: false
    }];
  }

  loadData() {
    const filters = this._gridTableFilter.parse(this.form.value, this.filtersMap);
    this.compReady = false;

    this._reportsExpenseRest.findAll(filters.qParams).subscribe(response => {
      this.data = response.data;

      this.prepareParams();
    }, null, () => this.compReady = true);
  }

  prepareParams() {
    const filterValues = this.form.value;

    if (filterValues.lowDate) {
      this.reportParams.lowDate = moment(filterValues.lowDate).valueOf();
    }

    if (filterValues.highDate) {
      this.reportParams.highDate = moment(filterValues.highDate).valueOf();
    }
  }
}

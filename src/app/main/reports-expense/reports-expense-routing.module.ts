import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ReportsExpensePreviewComponent } from './preview/reports-expense-preview.component';

import { AuthorizationService } from '../../core/core.module';

const routes: Routes = [{
  path: '',
  children: [{
    path: '',
    component: ReportsExpensePreviewComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'reportsExpense.preview'
    }
  }]
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ],
})
export class ReportsExpenseRoutingModule { }

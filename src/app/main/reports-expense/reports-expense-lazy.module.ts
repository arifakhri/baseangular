import { NgModule } from '@angular/core';

import { ReportsExpenseModule } from './reports-expense.module';
import { ReportsExpenseRoutingModule } from './reports-expense-routing.module';

@NgModule({
  imports: [
    ReportsExpenseModule,
    ReportsExpenseRoutingModule,
  ],
})
export class ReportsExpenseLazyModule { }

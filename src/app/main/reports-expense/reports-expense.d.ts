declare interface IReportsExpense {
  accountClass: string;
  accountType: string;
  accountName: string;
  total: 0;
}
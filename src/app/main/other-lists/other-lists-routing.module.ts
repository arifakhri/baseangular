import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { OtherListsComponent } from './other-lists.component';

import { AuthorizationService } from '../../core/core.module';

export const routes: Routes = [{
  path: '',
  component: OtherListsComponent,
  canActivate: [AuthorizationService],
  data: {
    name: 'otherLists'
  }
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule
  ]
})
export class OtherListsRoutingModule { }

import { NgModule } from '@angular/core';

import { OtherListsModule } from './other-lists.module';
import { OtherListsRoutingModule } from './other-lists-routing.module';

@NgModule({
  imports: [
    OtherListsModule,
    OtherListsRoutingModule,
  ]
})
export class OtherListsLazyModule { }

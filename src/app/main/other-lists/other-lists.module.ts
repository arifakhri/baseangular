import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { OtherListsComponent } from './other-lists.component';

@NgModule({
  imports: [
    CommonModule,
    FlexLayoutModule,
    RouterModule,
    TranslateModule,
  ],
  declarations: [
    OtherListsComponent,
  ]
})
export class OtherListsModule { }

import { Component } from '@angular/core';

@Component({
  selector: 'app-other-lists',
  templateUrl: 'other-lists.component.html'
})
export class OtherListsComponent {
  maps = [{
    i18nLabel: 'ui.otherLists.masterData',
    icon: 'assets/img/other-lists/master-data.png',
    lists: [
      [{
        i18nLabel: 'ui.otherLists.productCategory.label',
        i18nDescription: 'ui.otherLists.productCategory.description',
        link: '/product-categories',
      }, {
        i18nLabel: 'ui.otherLists.warehouse.label',
        i18nDescription: 'ui.otherLists.warehouse.description',
        link: '/warehouses',
      }],
      [{
        i18nLabel: 'ui.otherLists.productAttribute.label',
        i18nDescription: 'ui.otherLists.productAttribute.description',
        link: '/product-attributes',
      }, {
        i18nLabel: 'ui.otherLists.priceLevel.label',
        i18nDescription: 'ui.otherLists.priceLevel.description',
        link: '/price-levels',
      }],
      [{
        i18nLabel: 'ui.otherLists.productTag.label',
        i18nDescription: 'ui.otherLists.productTag.description',
        link: '/product-tags',
      }, {
        i18nLabel: 'ui.otherLists.tax.label',
        i18nDescription: 'ui.otherLists.tax.description',
        link: '/taxes',
      }],
      [{
        i18nLabel: 'ui.otherLists.productBrand.label',
        i18nDescription: 'ui.otherLists.productBrand.description',
        link: '/product-brands',
      }, {
        i18nLabel: 'ui.otherLists.paymentMethod.label',
        i18nDescription: 'ui.otherLists.paymentMethod.description',
        link: '/payment-methods',
      }],
      [{
        i18nLabel: 'ui.otherLists.uom.label',
        i18nDescription: 'ui.otherLists.uom.description',
        link: '/uom',
      }, {
        i18nLabel: 'ui.otherLists.shippingMethod.label',
        i18nDescription: 'ui.otherLists.shippingMethod.description',
        link: '/shipping-methods',
      }]
    ]
  }, {
    i18nLabel: 'ui.otherLists.transaction',
    icon: 'assets/img/other-lists/transaction.png',
    lists: [
      [{
        i18nLabel: 'ui.otherLists.stockOpname.label',
        i18nDescription: 'ui.otherLists.stockOpname.description',
        link: '/inventory-adjustments',
      }, {
        i18nLabel: 'ui.otherLists.manualJournal.label',
        i18nDescription: 'ui.otherLists.manualJournal.description',
        link: '/journal-entries',
      }],
      [{
        i18nLabel: 'ui.otherLists.warehouseTransfer.label',
        i18nDescription: 'ui.otherLists.warehouseTransfer.description',
        link: '/warehouse-transfers',
      }, {}]
    ]
  }];
}

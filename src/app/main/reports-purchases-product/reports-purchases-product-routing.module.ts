import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ReportsPurchasesProductPreviewComponent } from './preview/reports-purchases-product-preview.component';

import { AuthorizationService } from '../../core/core.module';

const routes: Routes = [{
  path: '',
  children: [{
    path: '',
    component: ReportsPurchasesProductPreviewComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'reportsPurchasesVendor.preview'
    }
  }]
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ],
})
export class ReportsPurchasesProductRoutingModule { }

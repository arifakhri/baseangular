declare interface IReportsPurchasesProduct {
  accountClass: string;
  accountType: string;
  accountName: string;
  total: 0;
}
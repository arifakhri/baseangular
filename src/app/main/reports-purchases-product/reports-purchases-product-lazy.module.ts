import { NgModule } from '@angular/core';

import { ReportsPurchasesProductModule } from './reports-purchases-product.module';
import { ReportsPurchasesProductRoutingModule } from './reports-purchases-product-routing.module';

@NgModule({
  imports: [
    ReportsPurchasesProductModule,
    ReportsPurchasesProductRoutingModule,
  ],
})
export class ReportsPurchasesProductLazyModule { }

import { Injectable } from '@angular/core';

import { ReportsRestService } from '../reports/reports-rest.service';

@Injectable()
export class ReportsPurchasesProductRestService {
  constructor(
    private _reportsRestService: ReportsRestService,
  ) { }

  findAll(queryParams: any = {}) {
    return this._reportsRestService.requestReport.post<IApiPaginationResult<IReportsPurchasesProduct>>(
      `purchase-by-product-summary`,
      queryParams
    );
  }
}

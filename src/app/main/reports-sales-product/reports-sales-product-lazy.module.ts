import { NgModule } from '@angular/core';

import { ReportsSalesProductModule } from './reports-sales-product.module';
import { ReportsSalesProductRoutingModule } from './reports-sales-product-routing.module';

@NgModule({
  imports: [
    ReportsSalesProductModule,
    ReportsSalesProductRoutingModule,
  ],
})
export class ReportsSalesProductLazyModule { }

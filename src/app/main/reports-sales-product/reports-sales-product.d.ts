declare interface IReportsSalesProduct {
  accountClass: string;
  accountType: string;
  accountName: string;
  total: 0;
}
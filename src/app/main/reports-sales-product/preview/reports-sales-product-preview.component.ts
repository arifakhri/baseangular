import * as _ from 'lodash';
import * as moment from 'moment';
import { AutoComplete } from 'primeng/components/autocomplete/autocomplete';
import { Component, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

import { AdminLayoutService } from '../../../layouts/layouts.module';
import { BusinessPartnerRestService } from '../../business-partner/business-partner-rest.service';
import { CommonService, GridTableFilterService } from '../../../core/core.module';
import { ReportsSalesProductRestService } from '../reports-sales-product-rest.service';
import { ProductBrandRestService } from '../../product-brand/product-brand-rest.service';
import { TransactionService } from '../../transaction/transaction.service';

@Component({
  selector: 'app-reports-sales-product-preview',
  templateUrl: './reports-sales-product-preview.component.html',
})
export class ReportsSalesProductPreviewComponent {
  @ViewChild('customerAC') elCustomerAC: AutoComplete;
  @ViewChild('reportViewer') elReportViewer: any;

  ACCustomerHandler = CommonService.remoteACItemsHandler({
    remoteParams: this.ACCustomerParams.bind(this),
    remoteRequest: this._businessPartnerRest.findAllCustomersPicker.bind(this._businessPartnerRest),
    remoteRequestMap: (response) => response.data,
    element: () => this.elCustomerAC,
  });

  brandProducts: any = [];
  compReady: boolean = false;
  data: any = [];
  form: FormGroup = new FormGroup({});
  reportViewerReady: boolean = false;
  reportParams: any = {
    title: 'Sales by Product Summary'
  };

  filtersMap: IGridTableFilterMap = {
    lowDate: {
      targetVar: 'date',
      targetFilter: 'param',
      dateFormat: 'YYYY-MM-DD',
    },
    highDate: {
      targetVar: 'date',
      targetFilter: 'param',
      dateFormat: 'YYYY-MM-DD',
    },
    transactionTypes: {
      targetFilter: 'param',
      targetVar: 'arrstring',
    },
    states: {
      targetFilter: 'param',
      targetVar: 'arrstring',
    },
    statuses: {
      targetFilter: 'param',
      targetVar: 'arrstring',
    },
    customerId: {
      targetFilter: 'param',
    },
    transactionNumber: {
      targetFilter: 'param',
    },
    brandIds: {
      targetFilter: 'param',
      targetVar: 'arrstring',
    },
  };

  constructor(
    private _adminLayout: AdminLayoutService,
    private _businessPartnerRest: BusinessPartnerRestService,
    private _gridTableFilter: GridTableFilterService,
    private _productBrandRest: ProductBrandRestService,
    private _reportsSalesProductRest: ReportsSalesProductRestService,
    public _transaction: TransactionService,
  ) {
    this._adminLayout.containerType = 1;

    this.buildForm();
    this.loadData();
  }

  buildForm() {
    this.form.addControl('lowDate', new FormControl(moment().set('date', 1).toDate()));
    this.form.addControl('highDate', new FormControl(new Date()));
    this.form.addControl('brandIds', new FormControl(''));
    this.form.addControl('customer', new FormControl);
    this.form.addControl('customerId', new FormControl);
    this.form.addControl('transactionTypes', new FormControl(''));
    this.form.addControl('states', new FormControl);
    this.form.addControl('statuses', new FormControl);
    this.form.addControl('transactionNumber', new FormControl(''));
  }

  ACCustomerParams(event: any, type: string) {
    let filters = [];

    if (type === 'search') {
      filters = [{
        filterValues: [{
          field: 'displayName',
          operator: 'contains',
          value: event.query
        }]
      }];
    }

    return [{
      filter: filters,
      sort: [{
        field: 'displayName',
        dir: 'asc'
      }],
      take: 30,
      skip: 0,
      includeTotalCount: false
    }];
  }

  loadProductBrandPicker() {
    this._productBrandRest.findAllProductBrandPicker().subscribe(brands => {
      const keyMap = {
        id: 'value',
        name: 'label'
      };
      this.brandProducts = brands.data.map((obj) => {
        return _.mapKeys(obj, function (value, key) {
          return keyMap[key];
        });
      });
    });
  }

  loadData() {
    const filters = this._gridTableFilter.parse(this.form.value, this.filtersMap);
    this.compReady = false;

    this.loadProductBrandPicker();

    this._reportsSalesProductRest.findAll(filters.qParams).subscribe(response => {
      this.data = response.data;

      this.prepareParams();
    }, null, () => this.compReady = true);
  }

  prepareParams() {
    const filterValues = this.form.value;

    if (filterValues.lowDate) {
      this.reportParams.lowDate = moment(filterValues.lowDate).valueOf();
    }

    if (filterValues.highDate) {
      this.reportParams.highDate = moment(filterValues.highDate).valueOf();
    }
  }
}

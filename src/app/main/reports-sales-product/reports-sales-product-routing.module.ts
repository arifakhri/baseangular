import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ReportsSalesProductPreviewComponent } from './preview/reports-sales-product-preview.component';

import { AuthorizationService } from '../../core/core.module';

const routes: Routes = [{
  path: '',
  children: [{
    path: '',
    component: ReportsSalesProductPreviewComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'reportsSalesProduct.preview'
    }
  }]
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ],
})
export class ReportsSalesProductRoutingModule { }

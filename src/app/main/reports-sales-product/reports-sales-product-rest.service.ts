import { Injectable } from '@angular/core';

import { ReportsRestService } from '../reports/reports-rest.service';

@Injectable()
export class ReportsSalesProductRestService {

  constructor(
    private _reportsRestService: ReportsRestService,
  ) { }

  findAll(queryParams: any = {}) {
    return this._reportsRestService.requestReport.post<IApiPaginationResult<IReportsSalesProduct>>(
      `sales-by-product-summary`,
      queryParams
    );
  }
}

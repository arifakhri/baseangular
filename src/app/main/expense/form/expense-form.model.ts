import { MBase } from '../../../core/common/base.model';

export class MExpenseForm extends MBase {
  branchId: number = 0;
  description: string;
  lines: MExpenseFormLine[] = [];
  note: string;
  total: number = 0;
  payeeId: string;
  paymentAccountId: string;
  transactionDate: string;
  transactionNumber: string;
}

export class MExpenseFormLine extends MBase {
  account: string;
  accountId: string;
  amount: number = 0;
  description: string;
}

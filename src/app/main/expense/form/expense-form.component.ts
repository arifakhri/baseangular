import * as _ from 'lodash';
import { AutoComplete } from 'primeng/components/autocomplete/autocomplete';
import { ChangeDetectorRef, Component, Input, OnChanges, OnInit, SimpleChanges, ViewChild, } from '@angular/core';
import { FormArray, FormGroup } from '@angular/forms';
import 'app/rxjs-imports.ts';

import { AccountingService, AutocompleteService, CommonService } from '../../../core/core.module';
import { ExpenseCloneService } from '../expense-clone.service';
import { ExpenseFormService } from './expense-form.service';
import { ExpenseRestService } from '../expense-rest.service';
import { ExpenseService } from '../expense.service';
import { TransactionFormBComponent } from '../../transaction/transaction-form.bcomponent';

@Component({
  selector: 'app-expense-form',
  templateUrl: 'expense-form.component.html',
})
export class ExpenseFormComponent extends TransactionFormBComponent implements OnChanges, OnInit {
  @Input() doc: IExpense;
  @Input() form: FormGroup;
  @ViewChild('payeeAC') elPayeeAC: AutoComplete;

  payeesSuggestion: IBusinessPartner[] = [];


  defaultValues = {
    master: this._expenseForm.getDefaultValues('master'),
    line: this._expenseForm.getDefaultValues('line'),
  };

  payeeChanged: boolean = false;

  cloneTransaction: boolean = false;

  ACPayeeHandler = CommonService.remoteACItemsHandler({
    remoteParams: this.ACPayeeParams.bind(this),
    remoteRequest: this._expenseRest.findAllBusinessPartner.bind(this._expenseRest),
    remoteRequestMap: (response) => response.data,
    element: () => this.elPayeeAC,
    ACConfig: {
      prependNull: true,
    },
  });

  constructor(
    public _autocomplete: AutocompleteService,
    public _accounting: AccountingService,
    private _changeDetectorRef: ChangeDetectorRef,
    private _expense: ExpenseService,
    public _expenseClone: ExpenseCloneService,
    public _expenseForm: ExpenseFormService,
    private _expenseRest: ExpenseRestService,
  ) {
    super();
    this.componentId = 'ExpenseForm';

    this.registerHook('loadRelated', event => {
      return this._expenseRest.loadRelatedData();
    }, event => {
      if (this._expenseClone.docOnHold) {
        const parentDoc = <any>this._expenseClone.docOnHold;
        parentDoc.deliveryDate = null;
        parentDoc.transactionDate = new Date;
        parentDoc.transactionNumber = null;

        this.cloneTransaction = true;
        this.doc = parentDoc;
        this._expenseClone.docOnHold = null;

        this.form.patchValue(this.doc);
        this.applyDoc();

      }
    });

    this.registerHook('patchForm', event => {
      this.applyDoc();
    });

  }

  ngOnInit() {
    super.ngOnInit();
    this.addLines();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (_.has(changes, 'doc') && this.doc) {
      this.applyDoc();
    }
  }

  applyDoc() {
    this.resetLines();
    if (this.doc.lines.length > 2) {
      this.addLines(this.doc.lines.length - 2);
    }
    this.form.patchValue({ lines: this.doc.lines });

    this._changeDetectorRef.detectChanges();
    this.syncTotal();

  }

  resetLines() {
    const formArray = <FormArray>this.form.controls.lines;
    while (formArray.length) {
      formArray.removeAt(0);
    }
    this.addLines();
    this.syncTotal();
  }

  addLines(length: number = 2) {
    const formArray = (<FormArray>this.form.controls.lines);
    _.range(length).forEach(() => {
      const newFormGroup = this._expenseForm.buildFormChildLine();
      formArray.push(newFormGroup);
    });
    this._changeDetectorRef.detectChanges();
  }


  clearLine(index: number) {
    const formArray = <FormArray>this.form.controls.lines;
    if ((formArray.length > 2 && index <= 1) || index > 1) {
      formArray.removeAt(index);
    } else {
      const lineFormGroup = <FormGroup>formArray.at(index);
      lineFormGroup.reset(this.defaultValues.line);

      this._expenseForm.lineConditionalValidation(lineFormGroup);
    }
    this.syncTotal();
  }



  ACPayeeParams(event: any, type: string) {
    let filters = [];

    if (type === 'search') {
      filters = [{
        filterValues: [{
          field: 'displayName',
          operator: 'contains',
          value: event.query
        }]
      }];
    }

    return [{
      filter: filters,
      sort: [{
        field: 'displayName',
        dir: 'asc'
      }],
      take: 30,
      skip: 0,
      includeTotalCount: false
    }];
  }

  syncTotal() {
    const formValueLines = this.form.get('lines').value;
    const total = _.sumBy(formValueLines, 'amount');
    this.form.get('total').setValue(total);
  }

}

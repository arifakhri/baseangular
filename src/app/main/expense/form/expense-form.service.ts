import * as _ from 'lodash';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { Injectable } from '@angular/core';

import { MExpenseForm, MExpenseFormLine } from './expense-form.model';

import { StartingDataService } from '../../../core/core.module';

@Injectable()
export class ExpenseFormService {
  constructor(
    private _startingData: StartingDataService,
  ) { }

  getDefaultValues(type: 'master' | 'line'): { [key: string]: any } {
    let model: any;
    switch (type) {
      case 'master':
        const startingData = this._startingData.data$.getValue();
        model = new MExpenseForm;
        model.defaultValues = {
          branchId: startingData.masterBranchId,
        };
        break;
      case 'line':
        const startingDataLine = this._startingData.data$.getValue();
        model = new MExpenseFormLine;
        model.defaultValues = {
        };
        break;
    }

    return model;
  }

  setFormDefinitions(form: FormGroup) {
    const startingData = this._startingData.data$.getValue();

    form.addControl('branchId', new FormControl(startingData.masterBranchId));
    form.addControl('transactionNumber', new FormControl);
    form.addControl('transactionDate', new FormControl(new Date(), Validators.required));
    form.addControl('paymentAccount', new FormControl);
    form.addControl('paymentAccountId', new FormControl(null, Validators.required));
    form.addControl('payee', new FormControl);
    form.addControl('payeeId', new FormControl(null, Validators.required));
    form.addControl('description', new FormControl);
    form.addControl('note', new FormControl);
    form.addControl('total', new FormControl(0));

    form.addControl('lines', new FormArray([]));

    form.patchValue(this.getDefaultValues('master'));
  }

  buildFormChildLine() {
    const formGroup = new FormGroup({
      id: new FormControl(null),
      rowVersion: new FormControl,
      account: new FormControl,
      accountId: new FormControl,
      amount: new FormControl,
      description: new FormControl,
    });

    formGroup.patchValue(this.getDefaultValues('line'));

    return formGroup;
  }

  lineConditionalValidation(formGroup: FormGroup) {
    const formGroupValue = formGroup.value;
    if (
      formGroup.dirty &&
      _.size(<any>_.pickBy(_.omit(formGroupValue, ['branchId']), _.identity))
    ) {
      formGroup.get('accountId').setValidators([Validators.required]);
      formGroup.get('accountId').updateValueAndValidity();
      formGroup.get('description').setValidators([Validators.required]);
      formGroup.get('description').updateValueAndValidity();
      formGroup.get('amount').setValidators([Validators.required]);
      formGroup.get('amount').updateValueAndValidity();
    } else {
      formGroup.get('accountId').clearValidators();
      formGroup.get('accountId').updateValueAndValidity();
      formGroup.get('description').clearValidators();
      formGroup.get('description').updateValueAndValidity();
      formGroup.get('amount').clearValidators();
      formGroup.get('amount').updateValueAndValidity();
    }
  }
}

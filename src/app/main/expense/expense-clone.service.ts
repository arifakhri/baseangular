import * as _ from 'lodash';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable()
export class ExpenseCloneService {
  private currentDoc: IExpense;

  constructor(
    private _router: Router
  ) { }

  set docOnHold(expense) {
    this.currentDoc = expense;
  }

  get docOnHold() {
    if (!this.currentDoc) {
      return null;
    }

    const targetDoc: any = _.cloneDeep(this.currentDoc);

    return targetDoc;
  }

  copy(expense) {
    this.docOnHold = expense;

    this._router.navigateByUrl('/expenses/create');
  }
}

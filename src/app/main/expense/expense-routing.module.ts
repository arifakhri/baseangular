import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ExpenseCreateComponent } from './create/expense-create.component';
import { ExpenseDetailComponent } from './detail/expense-detail.component';
import { ExpenseListComponent } from './list/expense-list.component';
import { ExpenseUpdateComponent } from './update/expense-update.component';

import { AuthorizationService } from '../../core/core.module';

export const routes: Routes = [{
  path: '',
  children: [{
    path: '',
    component: ExpenseListComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'expense.list'
    }
  }, {
    path: 'create',
    component: ExpenseCreateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'expense.create'
    }
  }, {
    path: ':id/create',
    component: ExpenseCreateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'expense.create'
    }
  }, {
    path: ':id',
    component: ExpenseDetailComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'expense.detail'
    }
  }, {
    path: ':id/update',
    component: ExpenseUpdateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'expense.update'
    }
  }]
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class ExpenseRoutingModule { }

import { AutoCompleteModule } from 'primeng/components/autocomplete/autocomplete';
import { BsDropdownModule, ModalModule, PaginationModule } from 'ngx-bootstrap';
import { CalendarModule } from 'primeng/components/calendar/calendar';
import { CommonModule } from '@angular/common';
import { DataTableModule } from 'primeng/components/datatable/datatable';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ListboxModule } from 'primeng/components/listbox/listbox';
import { HttpModule } from '@angular/http';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { ExpenseCreateComponent } from './create/expense-create.component';
import { ExpenseDetailComponent } from './detail/expense-detail.component';
import { ExpenseFormComponent } from './form/expense-form.component';
import { ExpenseListComponent } from './list/expense-list.component';
import { ExpenseUpdateComponent } from './update/expense-update.component';

import { ExpenseCloneService } from './expense-clone.service';
import { ExpenseFormService } from './form/expense-form.service';
import { ExpenseRestService } from './expense-rest.service';
import { ExpenseService } from './expense.service';

import { BusinessPartnerModule } from '../business-partner/business-partner.module';
import { CoreModule } from '../../core/core.module';
import { SharedModule } from '../../shared/shared.module';
import { TransactionModule } from '../transaction/transaction.module';

export const PROVIDERS = [
  ExpenseCloneService,
  ExpenseFormService,
  ExpenseRestService,
  ExpenseService,
];

@NgModule({
  imports: [
    AutoCompleteModule,
    BsDropdownModule,
    BusinessPartnerModule,
    CalendarModule,
    CommonModule,
    CoreModule,
    DataTableModule,
    FlexLayoutModule,
    FormsModule,
    HttpModule,
    ListboxModule,
    ModalModule,
    PaginationModule,
    ReactiveFormsModule,
    RouterModule,
    SharedModule,
    TransactionModule,
    TranslateModule,
  ],
  declarations: [
    ExpenseCreateComponent,
    ExpenseDetailComponent,
    ExpenseFormComponent,
    ExpenseListComponent,
    ExpenseUpdateComponent,
  ],
})
export class ExpenseModule { }

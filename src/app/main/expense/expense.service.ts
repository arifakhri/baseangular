import * as _ from 'lodash';
import * as moment from 'moment';
import { Injectable } from '@angular/core';

import { StartingDataService } from '../../core/core.module';

@Injectable()
export class ExpenseService {
  constructor(
    private _startingData: StartingDataService,
  ) { }

  normalizeDoc(record: any) {
    if (record.transactionDate) {
      record.transactionDate = moment(record.transactionDate).format('YYYY-MM-DD');
    }

    record.lines = _.reject(record.lines, line => {
      return !(<any>line).accountId;
    });
  }
}

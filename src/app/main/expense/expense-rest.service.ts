import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';

import { ApiQueryOption, RequestService } from '../../core/core.module';
import { BusinessPartnerRestService } from '../business-partner/business-partner-rest.service';

import { APP_CONST } from '../../app.const';

@Injectable()
export class ExpenseRestService {
  baseURL = `${APP_CONST.API_MAIN}/expenses`;
  request = this._request.new(this.baseURL);

  constructor(
    private _request: RequestService,
    public _businessPartnerRest: BusinessPartnerRestService,
  ) { }

  create(Expense: IExpense) {
    return this.request.post<IExpense>(``, Expense);
  }

  findAll(queryOption: ApiQueryOption, queryParams: any = {}) {
    return this.request.post<IApiPaginationResult<IExpense>>(`q`, queryOption, { params: queryParams });
  }

  findAllBusinessPartner(queryOption: ApiQueryOption = new ApiQueryOption): Observable<IApiPaginationResult<IBusinessPartner>> {
    return this._businessPartnerRest.findAll(queryOption);
  }

  load(ExpenseId: string, queryParams: any = {}) {
    return this.request.get<IExpense>(`${ExpenseId}`, { params: queryParams });
  }

  loadRelatedData() {
    return this.request.get<{ accounts: IAccount[] }>(`entry-related-data`);
  }

  update(ExpenseId: string, updateObj: IExpense) {
    return this.request.put<IExpense>(`${ExpenseId}`, updateObj);
  }

  void(ExpenseId: string) {
    return this.request.put<IExpense>(`${ExpenseId}/void`, {});
  }
}

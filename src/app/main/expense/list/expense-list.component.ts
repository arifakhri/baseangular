import * as SnackBar from 'node-snackbar';
import swal from 'sweetalert2';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { DataTable } from 'primeng/components/datatable/datatable';
import { Observable, Subscription } from 'rxjs';
import 'app/rxjs-imports.ts';
import { TranslateService } from '@ngx-translate/core';

import { SpinnerDirective } from '../../../shared/spinner/spinner.directive';

import {
  AccountingService,
  ExportDataTableService,
  GridTableDataSource,
  GridTableService,
  GridTableToggleService,
} from '../../../core/core.module';
import { ExpenseCloneService } from '../expense-clone.service';
import { ExpenseRestService } from '../expense-rest.service';
import { RetryDialogComponent } from '../../../shared/retry-dialog/retry-dialog.component';

@Component({
  selector: 'app-expense-list',
  templateUrl: 'expense-list.component.html'
})
export class ExpenseListComponent implements OnInit {
  @ViewChild('gridTable') public gridTable: DataTable;
  @ViewChild('pageLoading') elPageLoading: SpinnerDirective;
  @ViewChild('retryDialog') elRetryDialog: RetryDialogComponent;

  compReady: boolean;
  gridDataSource: GridTableDataSource<IExpense> = new GridTableDataSource<IExpense>();
  selectedRecords: IExpense[] = [];

  tableColumns: IGridTableColumn[] = [{
    i18nLabel: 'ui.expense.list.column.transactionDate',
    field: 'transactionDate',
    sort: true,
  }, {
    i18nLabel: 'ui.expense.list.column.transactionNumber',
    field: 'transactionNumber',
    link: (row: IExpense) => {
      return `/expenses/${row.id}`;
    },
    sort: true,
  }, {
    i18nLabel: 'ui.expense.list.column.description',
    field: 'description',
    sort: true,
  }, {
    i18nLabel: 'ui.expense.list.column.payFrom',
    field: 'paymentAccount.name',
    sort: true,
  }, {
    i18nLabel: 'ui.expense.list.column.payee',
    field: 'payee.displayName',
    sort: true,
  }, {
    i18nLabel: 'ui.expense.list.column.total',
    field: 'total',
    sort: true,
    formatter: (value, row: IExpense) => {
      return this._accounting.ac.formatMoney(value, '');
    },
    columnClasses: 'right-align',
  }];
  tableColumnsShow: boolean = false;
  tableColumnsToggle = this._gridTableToggle.mapToggleOptionsFromColumns(this.tableColumns);

  retryDialogShownState: boolean = false;
  retryDialogOnRetry: Function;

  qParams: any = {
    keyword: null
  };

  private loadDataTimeoutId: number;
  private gridFirstLoad: boolean = true;
  private subscriptions: { [key: string]: Subscription[] } = {
    load: []
  };

  constructor(
    private _accounting: AccountingService,
    private _activatedRoute: ActivatedRoute,
    private _dataTableExport: ExportDataTableService,
    private _gridTable: GridTableService,
    private _gridTableToggle: GridTableToggleService,
    public _expenseClone: ExpenseCloneService,
    private _expenseRest: ExpenseRestService,
    private _translate: TranslateService,
  ) { }

  ngOnInit() {
    this.loadData();
  }

  gridLoadDataWrapper() {
    // Grid always load at the first time. we dont want that. we already call on ngOnInit
    if (this.gridFirstLoad) {
      this.gridFirstLoad = false;
      return;
    }

    if (this.loadDataTimeoutId) {
      window.clearTimeout(this.loadDataTimeoutId);
      this.loadDataTimeoutId = null;
    }

    this.loadDataTimeoutId = window.setTimeout(() => {
      this.loadData();
    }, 300);
  }

  showVoidDialog(ExpenseId: string) {
    swal({
      title: this._translate.instant('confirm.expense.void.label'),
      text: this._translate.instant('confirm.expense.void.description'),
      type: 'question',
      showCancelButton: true,
    })
      .then(() => {
        this.voidOrder(ExpenseId);
      })
      .catch(() => { });
  }

  voidOrder(ExpenseId: string) {
    this._expenseRest.void(ExpenseId).subscribe(response => {
      this.gridLoadDataWrapper();
      SnackBar.show({
        text: this._translate.instant('success.Expense.void'),
        pos: 'bottom-right'
      });
    });
  }

  get exportRecords(): Observable<IExpense[]> {
    const qOption = {
      ...this._gridTable.generateApiQueryOptionFromGridInfo(
        this.gridDataSource.pager,
        false,
        this.gridTable,
      ),
      skip: 0,
      take: this._dataTableExport.exportLimit,
    };

    return this.elRetryDialog.createRetryEntry(
      this._expenseRest.findAll(qOption, this.qParams).map(response => response.data)
    );
  }

  exportPDF() {
    this.exportRecords.subscribe(records => {
      this._dataTableExport.export({
        records,
        mapOptions: this._dataTableExport.mapFromTableColumns(this.tableColumns),
        fileName: 'all-expenses',
        extension: 'pdf',
        templateData: {
          title: this._translate.instant('ui.Expense.list.title'),
        },
      });
    });
  }

  exportExcel() {
    this.exportRecords.subscribe(records => {
      this._dataTableExport.export({
        records: records,
        mapOptions: this._dataTableExport.mapFromTableColumns(this.tableColumns),
        fileName: 'all-expenses',
        extension: 'xls',
      });
    });
  }

  loadData() {
    this.elPageLoading.forceShow();

    const qOption = this._gridTable.generateApiQueryOptionFromGridInfo(
      this.gridDataSource.pager,
      true,
      this.gridTable,
    );

    this.subscriptions.load.forEach(sub => sub.unsubscribe());
    this.subscriptions.load.push(
      this.elRetryDialog.createRetryEntry(
        this._expenseRest.findAll(qOption, this.qParams).finally(() => {
        this.elPageLoading.forceHide();
      })
    ).subscribe(response => {
        this.gridDataSource.updateFromApiPaginationResult(response);

        this.compReady = true;
      })
    );
  }

  cloneTransaction(doc) {
    this.elPageLoading.forceShow();

    this._expenseRest.load(doc.id).subscribe(newDoc => {
      this.elPageLoading.forceHide();

      this._expenseClone.copy(newDoc);
    });
  }

  public onGridChange() {
    this.gridLoadDataWrapper();
  }

  public onGridPageChange() {
    this.gridLoadDataWrapper();
  }
}

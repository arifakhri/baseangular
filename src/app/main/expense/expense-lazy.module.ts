import { NgModule } from '@angular/core';

import { ExpenseModule } from './expense.module';
import { ExpenseRoutingModule } from './expense-routing.module';

@NgModule({
  imports: [
    ExpenseModule,
    ExpenseRoutingModule
  ]
})
export class ExpenseLazyModule { }

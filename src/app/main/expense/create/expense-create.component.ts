import * as _ from 'lodash';
import { Component, QueryList, ViewChildren } from '@angular/core';
import 'app/rxjs-imports.ts';

import { ExpenseFormComponent } from '../form/expense-form.component';

import { ExpenseFormService } from '../form/expense-form.service';
import { ExpenseRestService } from '../expense-rest.service';
import { ExpenseService } from '../expense.service';

import { BaseCreateBComponent } from '../../../shared/base/base.module';
import { MExpense } from '../expense.model';

@Component({
  selector: 'app-expense-create',
  templateUrl: 'expense-create.component.html'
})
export class ExpenseCreateComponent extends BaseCreateBComponent  {
  @ViewChildren('expenseForm') elExpenseForm: QueryList<ExpenseFormComponent>;
  constructor(
    private _expense: ExpenseService,
    public _expenseForm: ExpenseFormService,
    private _expenseRest: ExpenseRestService,

  ) {
    super();
    this.componentId = 'ExpenseCreate';
    this.headerTitle = 'ui.expense.create.title';
    this.containerType = 1;
    this.routeURL = '/expenses';
    this.entrySuccessI18n = 'success.expense.create';

    this.registerHook('buildForm', event => {
      this._expenseForm.setFormDefinitions(this.form);
    });
    this.registerHook('save', event => {
      let expense = new MExpense;
      expense = _.assign(expense, this.form.value);
      this._expense.normalizeDoc(expense);
      let obs = this._expenseRest.create(expense);
      return obs;
    });

  }
}

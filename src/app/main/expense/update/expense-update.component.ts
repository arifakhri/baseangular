import * as moment from 'moment';
import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';

import { ExpenseFormService } from '../form/expense-form.service';
import { ExpenseService } from '../expense.service';
import { ExpenseRestService } from '../expense-rest.service';
import { BaseUpdateBComponent } from '../../../shared/base/base.module';

@Component({
  selector: 'app-expense-update',
  templateUrl: 'expense-update.component.html'
})
export class ExpenseUpdateComponent extends BaseUpdateBComponent {

  constructor(
    private _expense: ExpenseService,
    public _expenseForm: ExpenseFormService,
    private _expenseRest: ExpenseRestService,
  ) {
    super();

    this.componentId = 'ExpenseUpdate';
    this.headerTitle = 'ui.expense.update.title';
    this.containerType = 1;
    this.routeURL = '/expenses';
    this.entrySuccessI18n = 'success.expense.update';


    this.registerHook('buildForm', event => {
      this._expenseForm.setFormDefinitions(this.form);
    });

    this.registerHook('load', event => {
      return this._expenseRest.load(this.page.routeParams.id).switchMap(doc => {
        if (doc.transactionDate) {
          doc.transactionDate = moment(doc.transactionDate).toDate();
        }
        return Observable.of(doc);
      });
    });

    this.registerHook('save', event => {
      const expense: IExpense = Object.assign({}, this.doc, this.form.value);
      this._expense.normalizeDoc(expense);
      return this._expenseRest.update(this.page.routeParams.id, expense);
    });
  }

}

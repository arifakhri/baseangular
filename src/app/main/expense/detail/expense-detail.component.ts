import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { ExpenseCloneService } from '../expense-clone.service';
import { ExpenseRestService } from '../expense-rest.service';
import { AccountingService } from '../../../core/core.module';

@Component({
  selector: 'app-expense-detail',
  templateUrl: 'expense-detail.component.html'
})
export class ExpenseDetailComponent implements OnInit {
  doc: IExpense;
  routeParams: any;

  constructor(
    public _accounting: AccountingService,
    public _expenseClone: ExpenseCloneService,
    private _expenseRest: ExpenseRestService,
    private _route: ActivatedRoute,
  ) {
    this._route.params.subscribe(params => this.routeParams = params);
  }

  ngOnInit() {
    this.loadData();
  }

  loadData() {
    this._expenseRest.load(this.routeParams.id).subscribe(expense => {
      this.doc = expense;
    });
  }
}

import { Component, EventEmitter, Inject, Input, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

import { CommonService, SystemMessageService } from '../../../core/core.module';
import { SalesDownPaymentRestService } from '../sales-down-payment-rest.service';
import { SalesDownPaymentService } from '../sales-down-payment.service';

import { MSalesOrder } from '../../sales-order/sales-order.model';

@Component({
  selector: 'app-sales-down-payment-quick-create',
  templateUrl: 'sales-down-payment-quick-create.component.html',
  providers: [SystemMessageService]
})
export class SalesDownPaymentQuickCreateComponent implements OnInit {
  @Input() sourceTransaction: MSalesOrder;

  @Output() afterSubmit: EventEmitter<ISalesDownPayment> = new EventEmitter();
  @Output() close: EventEmitter<boolean> = new EventEmitter();

  form: FormGroup = new FormGroup({});
  initalFormValues: any = {};
  compLoading: boolean = false;

  constructor(
    @Inject('GlobalSystemMessage') private _globalSystemMessage: SystemMessageService,
    private _router: Router,
    private _salesDownPayment: SalesDownPaymentService,
    private _salesDownPaymentRest: SalesDownPaymentRestService,
    public _systemMessage: SystemMessageService,
    private _translate: TranslateService
  ) { }

  ngOnInit() {
    this._salesDownPayment.setFormDefinitions(this.form);

    this.initalFormValues = this.form.value;

    if (this.sourceTransaction) {
      this.form.patchValue({
        orderId: this.sourceTransaction.id,
        customerEmail: this.sourceTransaction.customerEmail,
        customerMobile: this.sourceTransaction.customerMobile,
      });
      if (this.sourceTransaction.customer) {
        this.form.patchValue({
          customer: this.sourceTransaction.customer,
          customerId: this.sourceTransaction.customerId,
        });
      }
    }
  }

  onSubmit() {
    if (this.formValid()) {
      this.compLoading = true;
      this.save();
    }
  }

  formValid(): boolean {
    if (!this.form.valid) {
      CommonService.markAsDirty(this.form);
      this._systemMessage.log({
        message: this._translate.instant('error.form'),
        type: 'error'
      });
      return false;
    }
    return true;
  }

  save(): void {
    const downPayment: ISalesDownPayment = this.form.value;
    this._salesDownPayment.normalizeDoc(downPayment);

    this._salesDownPaymentRest.create(downPayment)
      .catch(error => {
        this._systemMessage.log({
          message: error,
          type: 'error'
        });
        this.compLoading = false;
        return Observable.throw(error);
      })
      .subscribe(record => {
        this._globalSystemMessage.log({
          message: this._translate.instant('success.salesDownPayment.quickCreate'),
          type: 'success',
          showAs: 'growl',
          showSnackBar: false,
        });
        this.compLoading = false;
        this.form.reset(this.initalFormValues);
        this.afterSubmit.emit(record);
      });
  }
}

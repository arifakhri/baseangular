import * as moment from 'moment';
import { CustomValidators } from 'ng2-validation';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Injectable } from '@angular/core';

import { StartingDataService } from '../../core/core.module';

@Injectable()
export class SalesDownPaymentService {
  constructor(
    private _startingData: StartingDataService,
  ) { }

  setFormDefinitions(form: FormGroup) {
    const startingData = this._startingData.data$.getValue();
    const customerEmailControl = new FormControl('', [CustomValidators.email, Validators.maxLength(150)]);
    (<any>customerEmailControl).validatorData = {
      maxLength: 150
    };
    const customerMobileControl = new FormControl('', [CustomValidators.digits, Validators.maxLength(30)]);
    (<any>customerMobileControl).validatorData = {
      maxLength: 30
    };
    const customerRefNumberControl = new FormControl('', Validators.maxLength(30));
    (<any>customerRefNumberControl).validatorData = {
      maxLength: 30
    };
    const customerPrintNameControl = new FormControl('', Validators.maxLength(50));
    (<any>customerPrintNameControl).validatorData = {
      maxLength: 50
    };

    form.addControl('branchId', new FormControl(startingData.masterBranchId));
    form.addControl('downPaymentAmountPercent', new FormControl('', CustomValidators.number));
    form.addControl('downPaymentAmount', new FormControl('', [Validators.required, CustomValidators.number]));
    form.addControl('order', new FormControl);
    form.addControl('orderId', new FormControl(null));
    form.addControl('customer', new FormControl(null));
    form.addControl('customerId', new FormControl(null, Validators.required));
    form.addControl('customerEmail', customerEmailControl);
    form.addControl('customerMobile', customerMobileControl);
    form.addControl('customCustomerName', customerPrintNameControl);
    form.addControl('description', new FormControl);
    form.addControl('paymentMethod', new FormControl);
    form.addControl('paymentMethodId', new FormControl(null));
    form.addControl('paymentAccount', new FormControl(null));
    form.addControl('paymentAccountId', new FormControl(null, Validators.required));
    form.addControl('transactionNumber', new FormControl);
    form.addControl('transactionDate', new FormControl(new Date(), Validators.required));
    form.addControl('customerRefNumber', customerRefNumberControl);
    form.addControl('customerNote', new FormControl);
    form.addControl('note', new FormControl);
    form.addControl('useCustomCustomerName', new FormControl(false));

    form.get('paymentMethod').valueChanges.subscribe(paymentMethod => {
      if (paymentMethod && paymentMethod.defaultAccount) {
        form.get('paymentAccount').setValue(paymentMethod.defaultAccount);
        form.get('paymentAccountId').setValue(paymentMethod.defaultAccount.id);
      }
    });
  }

  normalizeDoc(record: any) {
    if (record.transactionDate) {
      record.transactionDate = moment(record.transactionDate).format('YYYY-MM-DD');
    }
  }
}

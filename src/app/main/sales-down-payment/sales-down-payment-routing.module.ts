import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SalesDownPaymentCreateComponent } from './create/sales-down-payment-create.component';
import { SalesDownPaymentDetailComponent } from './detail/sales-down-payment-detail.component';
import { SalesDownPaymentUpdateComponent } from './update/sales-down-payment-update.component';

import { AuthorizationService } from '../../core/core.module';

export const routes: Routes = [{
  path: '',
  children: [{
    path: 'create',
    component: SalesDownPaymentCreateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'salesDownPayment.create'
    }
  }, {
    path: ':id',
    component: SalesDownPaymentDetailComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'salesDownPayment.detail'
    }
  }, {
    path: ':id/update',
    component: SalesDownPaymentUpdateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'salesDownPayment.update'
    }
  }]
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class SalesDownPaymentRoutingModule { }

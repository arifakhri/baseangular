import * as _ from 'lodash';
import { Component, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import 'app/rxjs-imports.ts';

import { SalesDownPaymentRestService } from '../sales-down-payment-rest.service';
import { SalesDownPaymentService } from '../sales-down-payment.service';

import { BaseCreateBComponent } from '../../../shared/base/base.module';
import { MSalesDownPayment } from '../sales-down-payment.model';

@Component({
  selector: 'app-sales-down-payment-create',
  templateUrl: 'sales-down-payment-create.component.html'
})
export class SalesDownPaymentCreateComponent extends BaseCreateBComponent  {
  @ViewChild('reportModal') elReportModal: ModalDirective;

  constructor(
    private _salesDownPayment: SalesDownPaymentService,
    private _salesDownPaymentRest: SalesDownPaymentRestService
  ) {
    super();

    this.componentId = 'SalesDownPaymentCreate';
    this.headerTitle = 'ui.salesDownPayment.create.title';
    this.containerType = 1;
    this.routeURL = '/sales/payments';
    this.entrySuccessI18n = 'success.salesDownPayment.create';

    this.autoprintButtons = [{
      checkboxConfigId: 'autoprint.sdownpayment.create',
      checkboxLabel: 'ui.salesDownPayment.create.action.apCheckbox.report',
      previewLabel: 'ui.salesDownPayment.create.action.previewReport',
      onPreview: () => {
        this.saveAndPrint();
      }
    }];

    this.registerHook('buildForm', event => {
      this._salesDownPayment.setFormDefinitions(this.form);
    });


    this.registerHook('save', event => {
      let downPayment = new MSalesDownPayment;
      downPayment = _.assign(downPayment, this.form.value);
      this._salesDownPayment.normalizeDoc(downPayment);

      const isPrinting = _.get(event, 'data.print');

      return this._salesDownPaymentRest.create(downPayment, { includeLines: isPrinting });
    });
   }

  async saveAndPrint() {
    try {
      const submitted = await this.onSubmit({ saveConfirm: true, print: true });
      if (submitted) {
        this._salesDownPaymentRest.loadRelatedData().subscribe(related => {
          this.doc['companyInfo'] = related.settings.companyInfo;
          this.elReportModal.show();
        });
      }
    } catch (e) {

    }
  }
}

import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { AccountingService } from '../../../core/core.module';
import { AdminLayoutService } from '../../../layouts/layouts.module';
import { SalesDownPaymentRestService } from '../sales-down-payment-rest.service';

@Component({
  selector: 'app-sales-down-payment-detail',
  templateUrl: 'sales-down-payment-detail.component.html'
})
export class SalesDownPaymentDetailComponent implements OnInit {
  doc: ISalesDownPayment;
  routeParams: any;

  mainReportParams: any;

  constructor(
    public _accounting: AccountingService,
    private _adminLayout: AdminLayoutService,
    private _route: ActivatedRoute,
    private _salesDownPaymentRest: SalesDownPaymentRestService,
  ) {
    this._adminLayout.containerType = 1;
    this._route.params.subscribe(params => this.routeParams = params);
  }

  ngOnInit() {
    this.loadData();
  }

  loadData() {
    this._salesDownPaymentRest.load(this.routeParams.id).subscribe(receipt => {
      this.doc = receipt;
    });
  }

  loadRelatedData() {
    this._salesDownPaymentRest.loadRelatedData().subscribe(relatedData => {
      this.mainReportParams = {
        companyInfo: relatedData.settings.companyInfo,
      };
    });
  }
}

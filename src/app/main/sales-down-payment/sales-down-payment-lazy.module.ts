import { NgModule } from '@angular/core';

import { SalesDownPaymentModule } from './sales-down-payment.module';
import { SalesDownPaymentRoutingModule } from './sales-down-payment-routing.module';

@NgModule({
  imports: [
    SalesDownPaymentModule,
    SalesDownPaymentRoutingModule
  ]
})
export class SalesDownPaymentLazyModule { }

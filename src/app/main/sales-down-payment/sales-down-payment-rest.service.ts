import { Injectable } from '@angular/core';
import 'app/rxjs-imports.ts';

import { ApiQueryOption, RequestService } from '../../core/core.module';

import { APP_CONST } from '../../app.const';

@Injectable()
export class SalesDownPaymentRestService {
  baseURL = `${APP_CONST.API_MAIN}/sales/downpayments`;
  request = this._request.new(this.baseURL);

  constructor(
    private _request: RequestService,
  ) { }

  create(payment: ISalesDownPayment, queryParams: any = {}) {
    return this.request.post<ISalesDownPayment>(``, payment, { params: queryParams });
  }

  findAll(queryOption: ApiQueryOption = new ApiQueryOption, queryParams: any = {}) {
    return this.request.post<IApiPaginationResult<ISalesDownPayment>>(`q`, queryOption, { params: queryParams });
  }

  load(paymentId: string) {
    return this.request.get<ISalesDownPayment>(`${paymentId}`);
  }

  loadRelatedData() {
    return this.request.get<{ paymentMethods: IPaymentMethod[], paymentAccounts: IAccount[], settings: ISettings }>(`entry-related-data`);
  }

  update(paymentId: string, updateObj: ISalesDownPayment, queryParams: any = {}) {
    return this.request.put<ISalesDownPayment>(`${paymentId}`, updateObj, { params: queryParams });
  }

  void(paymentId: string) {
    return this.request.put<ISalesDownPayment>(`${paymentId}/void`, {});
  }
}

import * as _ from 'lodash';
import * as moment from 'moment';
import { Component, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';

import { SalesDownPaymentRestService } from '../sales-down-payment-rest.service';
import { SalesDownPaymentService } from '../sales-down-payment.service';

import { BaseUpdateBComponent } from '../../../shared/base/base.module';

@Component({
  selector: 'app-sales-down-payment-update',
  templateUrl: 'sales-down-payment-update.component.html',
})
export class SalesDownPaymentUpdateComponent extends BaseUpdateBComponent {
  @ViewChild('reportModal') elReportModal: ModalDirective;

  constructor(
    private _salesDownPayment: SalesDownPaymentService,
    private _salesDownPaymentRest: SalesDownPaymentRestService,
  ) {
    super();

    this.componentId = 'SalesDownPaymentUpdate';
    this.headerTitle = 'ui.salesDownPayment.update.title';
    this.containerType = 1;
    this.routeURL = '/sales/payments';
    this.entrySuccessI18n = 'success.salesDownPayment.update';

    this.registerHook('buildForm', event => {
      this._salesDownPayment.setFormDefinitions(this.form);
    });

    this.registerHook('load', event => {
      return this._salesDownPaymentRest.load(this.page.routeParams.id).switchMap(doc => {
        if (doc.transactionDate) {
          doc.transactionDate = moment(doc.transactionDate).toDate();
        }
        return Observable.of(doc);
      });
    });

    this.registerHook('save', event => {
      const downPayment: ISalesDownPayment = Object.assign({}, this.doc, this.form.value);
      this._salesDownPayment.normalizeDoc(downPayment);

      const isPrinting = _.get(event, 'data.print');

      return this._salesDownPaymentRest.update(this.page.routeParams.id, downPayment, { includeLines: isPrinting });
    });
  }

  async saveAndPrint() {
    try {
      const submitted = await this.onSubmit({ saveConfirm: true, print: true });
      if (submitted) {
        this._salesDownPaymentRest.loadRelatedData().subscribe(related => {
          this.doc['companyInfo'] = related.settings.companyInfo;
          this.elReportModal.show();
        });
      }
    } catch (e) {

    }
  }
}

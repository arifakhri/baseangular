import { AutoComplete } from 'primeng/components/autocomplete/autocomplete';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ModalDirective } from 'ngx-bootstrap';

import { AutocompleteService } from '../../../core/core.module';
import { SalesDownPaymentRestService } from '../sales-down-payment-rest.service';

@Component({
  selector: 'app-sales-down-payment-quick-form',
  templateUrl: 'sales-down-payment-quick-form.component.html'
})
export class SalesDownPaymentQuickFormComponent implements OnInit {
  @Input() form: FormGroup;
  @ViewChild('quickCreateModal') elQuickCreateModal: ModalDirective;

  quickCreateAC: AutoComplete;

  accounts: IAccount[] = [];
  paymentMethods: IPaymentMethod[] = [];

  constructor(
    public _autocomplete: AutocompleteService,
    private _salesPaymentRest: SalesDownPaymentRestService,
  ) { }

  ngOnInit() {
    this.loadRelatedData();
  }

  assignQuickCreatedEntity(entity: any, storageVar: string) {
    this[storageVar].push(entity);
    this.quickCreateAC.selectItem(entity);
    this.elQuickCreateModal.hide();
  }

  loadRelatedData() {
    this._salesPaymentRest.loadRelatedData().subscribe(related => {
      this.accounts = related.paymentAccounts;
      this.paymentMethods = related.paymentMethods;
    });
  }
}

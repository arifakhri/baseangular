import * as _ from 'lodash';
import { Component, Input, OnInit } from '@angular/core';

import { SalesDownPaymentRestService } from '../sales-down-payment-rest.service';

import { AccountingService, } from '../../../core/core.module';

@Component({
  selector: 'app-sales-down-payment-quick-list',
  templateUrl: './sales-down-payment-quick-list.component.html',
})
export class SalesDownPaymentQuickListComponent implements OnInit {
  @Input() transaction: any;

  downPayments: ISalesDownPayment[] = [];
  total: number = 0;

  constructor(
    public _accounting: AccountingService,
    private _salesDownPaymentRest: SalesDownPaymentRestService,
  ) { }

  ngOnInit() {
    this._salesDownPaymentRest.findAll({
        filter: [],
        sort: [],
        take: 100,
        includeTotalCount: false,
        skip: 0,
      }, { orderId: this.transaction.id }).subscribe(downPayments => {
      this.downPayments = downPayments.data;
      this.total = _.sumBy(this.downPayments, 'total');
    });
  }
}

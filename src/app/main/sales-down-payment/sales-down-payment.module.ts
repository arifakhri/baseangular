import { AutoCompleteModule } from 'primeng/components/autocomplete/autocomplete';
import { CalendarModule } from 'primeng/components/calendar/calendar';
import { DataTableModule } from 'primeng/components/datatable/datatable';
import { BsDropdownModule, ModalModule, PaginationModule } from 'ngx-bootstrap';
import { CheckboxModule } from 'primeng/components/checkbox/checkbox';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { SalesDownPaymentCreateComponent } from './create/sales-down-payment-create.component';
import { SalesDownPaymentDetailComponent } from './detail/sales-down-payment-detail.component';
import { SalesDownPaymentFormComponent } from './form/sales-down-payment-form.component';
import { SalesDownPaymentQuickCreateComponent } from './quick-create/sales-down-payment-quick-create.component';
import { SalesDownPaymentQuickFormComponent } from './quick-form/sales-down-payment-quick-form.component';
import { SalesDownPaymentQuickListComponent } from './quick-list/sales-down-payment-quick-list.component';
import { SalesDownPaymentUpdateComponent } from './update/sales-down-payment-update.component';

import { SalesDownPaymentService } from './sales-down-payment.service';
import { SalesDownPaymentRestService } from './sales-down-payment-rest.service';

import { CoreModule } from '../../core/core.module';
import { BusinessPartnerModule } from '../business-partner/business-partner.module';
import { PaymentMethodModule } from '../payment-method/payment-method.module';
import { SalesModule } from '../sales/sales.module';
import { SharedModule } from '../../shared/shared.module';
import { TransactionModule } from '../transaction/transaction.module';

export const PROVIDERS = [
  SalesDownPaymentService,
  SalesDownPaymentRestService,
];

@NgModule({
  imports: [
    AutoCompleteModule,
    BsDropdownModule,
    BusinessPartnerModule,
    CalendarModule,
    CheckboxModule,
    CommonModule,
    CoreModule,
    DataTableModule,
    FlexLayoutModule,
    FormsModule,
    HttpModule,
    ModalModule,
    PaginationModule,
    PaymentMethodModule,
    ReactiveFormsModule,
    RouterModule,
    SalesModule,
    SharedModule,
    TransactionModule,
    TranslateModule,
  ],
  declarations: [
    SalesDownPaymentCreateComponent,
    SalesDownPaymentDetailComponent,
    SalesDownPaymentFormComponent,
    SalesDownPaymentQuickCreateComponent,
    SalesDownPaymentQuickFormComponent,
    SalesDownPaymentQuickListComponent,
    SalesDownPaymentUpdateComponent,
  ],
  exports: [
    SalesDownPaymentQuickCreateComponent,
    SalesDownPaymentQuickListComponent,
  ]
})
export class SalesDownPaymentModule { }

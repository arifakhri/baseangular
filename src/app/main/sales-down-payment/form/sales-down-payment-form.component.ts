import * as _ from 'lodash';
import { AutoComplete } from 'primeng/components/autocomplete/autocomplete';
import { ChangeDetectorRef, Component, OnChanges, SimpleChanges, ViewChild, } from '@angular/core';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';

import { AccountingService, AutocompleteService, CommonService } from '../../../core/core.module';
import { SalesDownPaymentRestService } from '../sales-down-payment-rest.service';
import { SalesOrderRestService } from '../../sales-order/sales-order-rest.service';

import { TransactionFormSalesBComponent } from '../../transaction/transaction-form-sales.bcomponent';

import { MSalesOrder } from '../../sales-order/sales-order.model';

@Component({
  selector: 'app-sales-down-payment-form',
  templateUrl: 'sales-down-payment-form.component.html'
})
export class SalesDownPaymentFormComponent extends TransactionFormSalesBComponent implements OnChanges {
  @ViewChild('orderAC') elOrderAC: AutoComplete;

  ACProductVariantHandler = CommonService.remoteACItemsHandler({
    remoteParams: this.ACProductVariantParams.bind(this),
    remoteRequest: this._productVariantRest.findAllPicker.bind(this._productVariantRest),
    remoteRequestMap: (response) => response.data,
  });

  ACOrderHandler = CommonService.remoteACItemsHandler({
    remoteParams: this.ACOrderParams.bind(this),
    remoteRequest: this._salesOrderRest.findAll.bind(this._salesOrderRest),
    remoteRequestMap: (response) => response.data,
    element: () => this.elOrderAC,
  });

  constructor(
    public _autocomplete: AutocompleteService,
    public _accounting: AccountingService,
    private _changeDetectorRef: ChangeDetectorRef,
    private _salesDownPaymentRest: SalesDownPaymentRestService,
    private _salesOrderRest: SalesOrderRestService,
  ) {
    super();
    super.init();
    this.componentId = 'SalesDownPaymentForm';

    this.registerHook('loadRelated', event => {
      return this._salesDownPaymentRest.loadRelatedData();
    });

    this.registerHook('patchForm', event => {
      this.applyDoc();
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (_.has(changes, 'doc') && this.doc) {
      this.applyDoc();
    }
  }

  applyDoc() {
    this.form.patchValue({
      ...this.doc,
      downPaymentAmount: this.doc.total,
    });
  }

  onCustomerClear() {
    this.form.get('customerId').reset();
    this.form.get('order').reset();
    this.form.get('orderId').reset();
  }

  onCustomerSelected(customer: IBusinessPartner) {
    this.form.get('customerId').setValue(customer.id);
    this.form.get('order').reset();
    this.form.get('orderId').reset();
    if (customer.email) {
      this.form.get('customerEmail').setValue(customer.email);
    }
    if (customer.mobile) {
      this.form.get('customerMobile').setValue(customer.mobile);
    }
    this._changeDetectorRef.detectChanges();

    return Observable.of(customer);
  }

  ACOrderParams(event: any, type: string) {
    let filters = [];

    if (type === 'search') {
      filters = [{
        filterValues: [{
          field: 'transactionNumber',
          operator: 'contains',
          value: event.query
        }]
      }];
    }

    const customer = this.form.get('customer').value;

    return [{
      filter: filters,
      sort: [{
        field: 'transactionNumber',
        dir: 'asc'
      }],
      take: 30,
      skip: 0,
      includeTotalCount: false
    }, {
      customerId : customer ? customer.id : '',
    }];
  }

  onOrderSelected(order: MSalesOrder) {
    this.form.get('orderId').setValue(order.id);
    this.form.get('customer').setValue(order.customer);
    this.form.get('customerId').setValue(order.customer.id);

    this._changeDetectorRef.detectChanges();
    this.syncAmountPercent();
  }

  syncAmountPercent() {
    const targetOrder: MSalesOrder = this.form.get('order').value;
    if (targetOrder) {
      const percent: number = this.form.get('downPaymentAmountPercent').value || 0;
      if (percent > 0) {
        const targetAmount = targetOrder.total * percent / 100;
        this.form.get('downPaymentAmount').setValue(targetAmount);
      }
    }
  }
}

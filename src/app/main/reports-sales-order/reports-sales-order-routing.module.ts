import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ReportsSalesOrderPreviewComponent } from './preview/reports-sales-order-preview.component';

import { AuthorizationService } from '../../core/core.module';

const routes: Routes = [{
  path: '',
  children: [{
    path: '',
    component: ReportsSalesOrderPreviewComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'reportsSalesOrder.preview'
    }
  }]
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ],
})
export class ReportsSalesOrderRoutingModule { }

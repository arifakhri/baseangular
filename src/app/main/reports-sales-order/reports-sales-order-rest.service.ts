import { Injectable } from '@angular/core';

import { ReportsRestService } from '../reports/reports-rest.service';
import { MSalesOrder } from '../sales-order/sales-order.model';

@Injectable()
export class ReportsSalesOrderRestService {
  constructor(
    private _reportRest: ReportsRestService,
  ) { }

  findAll(reportParams: any = {}) {
    return this._reportRest.requestReport.post<IApiPaginationResult<MSalesOrder>>(
      'sales-order-list',
      reportParams,
    );
  }
}

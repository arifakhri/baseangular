import { NgModule } from '@angular/core';

import { ReportsSalesOrderModule } from './reports-sales-order.module';
import { ReportsSalesOrderRoutingModule } from './reports-sales-order-routing.module';

@NgModule({
  imports: [
    ReportsSalesOrderModule,
    ReportsSalesOrderRoutingModule,
  ],
})
export class ReportsSalesOrderLazyModule { }

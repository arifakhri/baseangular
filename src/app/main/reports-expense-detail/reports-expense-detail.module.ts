import { AutoCompleteModule } from 'primeng/components/autocomplete/autocomplete';
import { CalendarModule } from 'primeng/components/calendar/calendar';
import { CommonModule } from '@angular/common';
import { DataTableModule } from 'primeng/components/datatable/datatable';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MultiSelectModule } from 'primeng/components/multiselect/multiselect';
import { NgModule } from '@angular/core';
import { PaginationModule } from 'ngx-bootstrap';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { ReportsExpenseDetailPreviewComponent } from './preview/reports-expense-detail-preview.component';

import { ReportsExpenseDetailRestService } from './reports-expense-detail-rest.service';

import { SharedModule } from '../../shared/shared.module';

export const PROVIDERS = [
  ReportsExpenseDetailRestService,
];

@NgModule({
  imports: [
    AutoCompleteModule,
    CalendarModule,
    CommonModule,
    DataTableModule,
    FlexLayoutModule,
    FormsModule,
    MultiSelectModule,
    PaginationModule,
    ReactiveFormsModule,
    RouterModule,
    SharedModule,
    TranslateModule,
  ],
  declarations: [
    ReportsExpenseDetailPreviewComponent,
  ]
})
export class ReportsExpenseDetailModule {

}

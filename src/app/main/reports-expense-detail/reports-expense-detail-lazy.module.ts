import { NgModule } from '@angular/core';

import { ReportsExpenseDetailModule } from './reports-expense-detail.module';
import { ReportsExpenseDetailRoutingModule } from './reports-expense-detail-routing.module';

@NgModule({
  imports: [
    ReportsExpenseDetailModule,
    ReportsExpenseDetailRoutingModule,
  ],
})
export class ReportsExpenseDetailLazyModule { }

import { Injectable } from '@angular/core';

import { ReportsRestService } from '../reports/reports-rest.service';

@Injectable()
export class ReportsExpenseDetailRestService {
  constructor(
    private _reportsRest: ReportsRestService,
  ) { }

  findAll(queryParams: any = {}) {
    return this._reportsRest.requestReport.post<IApiPaginationResult<IReportsExpenseDetail>>(
      `transaction-list-detail`,
      queryParams
    );
  }
}

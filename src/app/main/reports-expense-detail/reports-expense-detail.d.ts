declare interface IReportsExpenseDetail {
  accountClass: string;
  accountType: string;
  accountName: string;
  total: 0;
}
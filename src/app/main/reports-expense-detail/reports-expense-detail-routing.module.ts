import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ReportsExpenseDetailPreviewComponent } from './preview/reports-expense-detail-preview.component';

import { AuthorizationService } from '../../core/core.module';

const routes: Routes = [{
  path: '',
  children: [{
    path: '',
    component: ReportsExpenseDetailPreviewComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'reportsExpenseDetail.preview'
    }
  }]
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ],
})
export class ReportsExpenseDetailRoutingModule { }

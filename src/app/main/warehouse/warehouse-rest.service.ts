import { Injectable } from '@angular/core';
import 'app/rxjs-imports.ts';

import { ApiQueryOption, RequestService } from '../../core/core.module';

import { APP_CONST } from '../../app.const';

@Injectable()
export class WarehouseRestService {
  baseURL = `${APP_CONST.API_MAIN}/warehouses`;
  request = this._request.new(this.baseURL);

  constructor(
    private _request: RequestService,
  ) { }

  create(warehouse: IWarehouse) {
    return this.request.post<IWarehouse>(``, warehouse);
  }

  findAll(queryOption: ApiQueryOption) {
    return this.request.post<IApiPaginationResult<IWarehouse>>(`q`, queryOption);
  }

  load(warehouseId: string) {
    return this.request.get<IWarehouse>(`${warehouseId}`);
  }

  update(warehouseId: string, updateObj: IWarehouse) {
    return this.request.put<IWarehouse>(`${warehouseId}`, updateObj);
  }

  delete(warehouseId: string) {
    return this.request.delete<any>(`${warehouseId}`);
  }

  toggleInactive(warehouseId: string, inactive: boolean) {
    return this.request.put<any>(`${warehouseId}/mark-active/${!inactive}`, null, {
      headers: {
        'Content-Type': 'application/json'
      }
    });
  }
}

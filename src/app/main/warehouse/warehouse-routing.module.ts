import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { WarehouseCreateComponent } from './create/warehouse-create.component';
import { WarehouseListComponent } from './list/warehouse-list.component';
import { WarehouseUpdateComponent } from './update/warehouse-update.component';

import { AuthorizationService } from '../../core/core.module';

export const routes: Routes = [{
  path: '',
  children: [{
    path: '',
    component: WarehouseListComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'warehouse.list'
    }
  }, {
    path: 'create',
    component: WarehouseCreateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'warehouse.create'
    }
  }, {
    path: ':id/update',
    component: WarehouseUpdateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'warehouse.update'
    }
  }, ]
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class WarehouseRoutingModule { }

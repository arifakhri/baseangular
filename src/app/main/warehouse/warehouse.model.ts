export class MWarehouse {
  id: string;
  name: string;
  inactive: boolean;
}

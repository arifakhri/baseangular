import { BsDropdownModule, PaginationModule } from 'ngx-bootstrap';
import { CommonModule } from '@angular/common';
import { DataTableModule } from 'primeng/components/datatable/datatable';
import { ListboxModule } from 'primeng/components/listbox/listbox';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { WarehouseCreateComponent } from './create/warehouse-create.component';
import { WarehouseFormComponent } from './form/warehouse-form.component';
import { WarehouseListComponent } from './list/warehouse-list.component';
import { WarehouseUpdateComponent } from './update/warehouse-update.component';

import { WarehouseRestService } from './warehouse-rest.service';

import { CoreModule } from '../../core/core.module';
import { SharedModule } from '../../shared/shared.module';

export const PROVIDERS = [
  WarehouseRestService,
];

@NgModule({
  imports: [
    CommonModule,
    FlexLayoutModule,
    PaginationModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    TranslateModule,
    BsDropdownModule,
    DataTableModule,
    ListboxModule,
    CoreModule,
    SharedModule,
  ],
  declarations: [
    WarehouseCreateComponent,
    WarehouseFormComponent,
    WarehouseListComponent,
    WarehouseUpdateComponent,
  ],
})
export class WarehouseModule { }

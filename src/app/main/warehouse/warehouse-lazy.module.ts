import { NgModule } from '@angular/core';

import { WarehouseModule } from './warehouse.module';
import { WarehouseRoutingModule } from './warehouse-routing.module';

@NgModule({
  imports: [
    WarehouseModule,
    WarehouseRoutingModule
  ]
})
export class WarehouseLazyModule { }

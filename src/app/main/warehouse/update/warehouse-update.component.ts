import * as _ from 'lodash';
import { Component } from '@angular/core';

import { WarehouseRestService } from '../warehouse-rest.service';

import { BaseUpdateBComponent } from '../../../shared/base/base.module';
import { MWarehouse } from '../warehouse.model';

@Component({
  selector: 'app-warehouse-update',
  templateUrl: 'warehouse-update.component.html',
})
export class WarehouseUpdateComponent extends BaseUpdateBComponent {
  constructor(
    private _warehouseRest: WarehouseRestService,
  ) {
    super();

    this.componentId = 'WarehouseUpdate';
    this.routeURL = '/warehouses';
    this.entrySuccessI18n = 'success.warehouse.update';

    this.headerTitle = 'ui.warehouse.update.title';

    this.registerHook('load', event => {
      return this._warehouseRest.load(this.page.routeParams.id);
    });

    this.registerHook('save', event => {
      const formValue = this.form.value;
      let warehouse = new MWarehouse;
      warehouse = _.assign(warehouse, this.doc, formValue);

      return this._warehouseRest.update(this.page.routeParams.id, warehouse);
    });
  }
}

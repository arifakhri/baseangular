declare interface IWarehouse {
  id: string;
  name: string;
  inactive: boolean;
}
import * as _ from 'lodash';
import { Component } from '@angular/core';

import { WarehouseRestService } from '../warehouse-rest.service';

import { BaseCreateBComponent } from '../../../shared/base/base.module';
import { MWarehouse } from '../warehouse.model';

@Component({
  selector: 'app-warehouse-create',
  templateUrl: 'warehouse-create.component.html',
})
export class WarehouseCreateComponent extends BaseCreateBComponent {
  constructor(
    private _warehouseRest: WarehouseRestService,
  ) {
    super();

    this.componentId = 'WarehouseCreate';
    this.routeURL = '/warehouses';
    this.entrySuccessI18n = 'success.warehouse.create';

    this.headerTitle = 'ui.warehouse.create.title';

    this.registerHook('save', event => {
      const formValue = this.form.value;
      let warehouse = new MWarehouse;
      warehouse = _.assign(warehouse, formValue);

      return this._warehouseRest.create(warehouse);
    });
  }
}

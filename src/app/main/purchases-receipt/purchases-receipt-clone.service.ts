import * as _ from 'lodash';
import { FormArray, FormGroup } from '@angular/forms';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable()
export class PurchasesReceiptCloneService {
  private currentDoc: IPurchasesReceipt;

  constructor(
    private _router: Router
  ) { }

  set docOnHold(receipt) {
    this.currentDoc = receipt;
  }

  get docOnHold() {
    if (!this.currentDoc) {
      return null;
    }

    const targetDoc: any = _.cloneDeep(this.currentDoc);
    targetDoc.lines.forEach(line => {
      line.expenseAccountId = _.get(line, 'product.expenseAccountId') || null;
    });

    return targetDoc;
  }

  copy(receipt) {
    this.docOnHold = receipt;

    this._router.navigateByUrl('/purchases/receipts/create');
  }

  reset(form: FormGroup) {
    const linesFormGroup = <FormArray>form.get('lines');
    _.forEach(linesFormGroup.controls, (lineFormGroup, lineIdx) => {
      lineFormGroup.get('incomeAccountId').reset(null);
    });
  }
}

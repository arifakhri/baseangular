import { AutoCompleteModule } from 'primeng/components/autocomplete/autocomplete';
import { CalendarModule } from 'primeng/components/calendar/calendar';
import { CheckboxModule } from 'primeng/components/checkbox/checkbox';
import { DataTableModule } from 'primeng/components/datatable/datatable';
import { ListboxModule } from 'primeng/components/listbox/listbox';
import { BsDropdownModule, ModalModule, PaginationModule, PopoverModule } from 'ngx-bootstrap';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { PurchasesReceiptCloneService } from './purchases-receipt-clone.service';
import { PurchasesReceiptCreateComponent } from './create/purchases-receipt-create.component';
import { PurchasesReceiptDetailComponent } from './detail/purchases-receipt-detail.component';
import { PurchasesReceiptFormComponent } from './form/purchases-receipt-form.component';
import { PurchasesReceiptListComponent } from './list/purchases-receipt-list.component';
import { PurchasesReceiptMoreFilterComponent } from './more-filter/purchases-receipt-more-filter.component';
import { PurchasesReceiptUpdateComponent } from './update/purchases-receipt-update.component';

import { PurchasesReceiptFormService } from './form/purchases-receipt-form.service';
import { PurchasesReceiptMoreFilterService } from './more-filter/purchases-receipt-more-filter.service';
import { PurchasesReceiptRestService } from './purchases-receipt-rest.service';
import { PurchasesReceiptService } from './purchases-receipt.service';

import { BusinessPartnerModule } from '../business-partner/business-partner.module';
import { CoreModule } from '../../core/core.module';
import { PaymentMethodModule } from '../payment-method/payment-method.module';
import { ProductVariantModule } from '../product-variant/product-variant.module';
import { PurchasesModule } from '../purchases/purchases.module';
import { SharedModule } from '../../shared/shared.module';
import { TransactionModule } from '../transaction/transaction.module';

export const PROVIDERS = [
  PurchasesReceiptCloneService,
  PurchasesReceiptFormService,
  PurchasesReceiptMoreFilterService,
  PurchasesReceiptRestService,
  PurchasesReceiptService,
];

@NgModule({
  imports: [
    AutoCompleteModule,
    BsDropdownModule,
    BusinessPartnerModule,
    CalendarModule,
    CheckboxModule,
    CommonModule,
    CoreModule,
    DataTableModule,
    FlexLayoutModule,
    FormsModule,
    HttpModule,
    ListboxModule,
    ModalModule,
    PaginationModule,
    PopoverModule,
    PaymentMethodModule,
    ProductVariantModule,
    PurchasesModule,
    ReactiveFormsModule,
    RouterModule,
    SharedModule,
    TransactionModule,
    TranslateModule,
  ],
  declarations: [
    PurchasesReceiptCreateComponent,
    PurchasesReceiptDetailComponent,
    PurchasesReceiptFormComponent,
    PurchasesReceiptListComponent,
    PurchasesReceiptMoreFilterComponent,
    PurchasesReceiptUpdateComponent,
  ],
})
export class PurchasesReceiptModule { }

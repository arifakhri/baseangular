import * as _ from 'lodash';
import * as SnackBar from 'node-snackbar';
import swal from 'sweetalert2';
import { ActivatedRoute } from '@angular/router';
import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { DataTable } from 'primeng/components/datatable/datatable';
import { ModalDirective, PopoverDirective } from 'ngx-bootstrap';
import { Observable, Subscription } from 'rxjs';
import 'app/rxjs-imports.ts';
import { TranslateService } from '@ngx-translate/core';

import {
  AccountingService,
  ExportDataTableService,
  GridTableDataSource,
  GridTableService,
  GridTableToggleService,
  SystemMessageService,
} from '../../../core/core.module';
import { AdminLayoutService } from '../../../layouts/layouts.module';
import { SpinnerDirective } from '../../../shared/spinner/spinner.directive';
import { PurchasesReceiptCloneService } from '../purchases-receipt-clone.service';
import { PurchasesReceiptMoreFilterService } from '../more-filter/purchases-receipt-more-filter.service';
import { PurchasesReceiptRestService } from '../purchases-receipt-rest.service';
import { RetryDialogComponent } from '../../../shared/retry-dialog/retry-dialog.component';

@Component({
  selector: 'app-purchases-receipt-list',
  templateUrl: 'purchases-receipt-list.component.html',
  providers: [SystemMessageService]
})
export class PurchasesReceiptListComponent implements OnDestroy, OnInit {
  @ViewChild('gridTable') public gridTable: DataTable;
  @ViewChild('popMoreFilter') elPopMoreFilter: PopoverDirective;
  @ViewChild('pageLoading') elPageLoading: SpinnerDirective;
  @ViewChild('reportModal') elReportModal: ModalDirective;
  @ViewChild('retryDialog') elRetryDialog: RetryDialogComponent;

  compReady: boolean;
  doc: IPurchasesReceipt;
  gridDataSource: GridTableDataSource<IPurchasesReceipt> = new GridTableDataSource<IPurchasesReceipt>();
  selectedRecords: IPurchasesReceipt[] = [];

  combinedFilterValues: string = '';
  combinedFilterFields = {
    'vendor.displayName': true,
    transactionNumber: true,
  };

  qParamsFilters: any = {
    keyword: null
  };
  moreFilterValues: IGridTableFilterParsed;

  tableColumns: IGridTableColumn[] = [{
    i18nLabel: 'ui.purchasesReceipt.list.column.transactionDate',
    field: 'transactionDate',
    dateFormat: 'DD/MM/YYYY',
    sort: true,
  }, {
    i18nLabel: 'ui.purchasesReceipt.list.column.transactionNumber',
    field: 'transactionNumber',
    link: (row: IPurchasesReceipt) => {
      return `/purchases/receipts/${row.id}`;
    },
    sort: true,
  }, {
    i18nLabel: 'ui.purchasesReceipt.list.column.description',
    field: 'description',
    sort: true,
  }, {
    i18nLabel: 'ui.purchasesReceipt.list.column.vendorName',
    field: 'vendor.displayName',
    sort: true,
  }, {
    i18nLabel: 'ui.purchasesReceipt.list.column.paymentMethod',
    field: 'paymentMethod.name',
    sort: true,
  }, {
    i18nLabel: 'ui.purchasesReceipt.list.column.paymentAccountName',
    field: 'paymentAccount.name',
    sort: true,
  }, {
    i18nLabel: 'ui.purchasesReceipt.list.column.total',
    field: 'total',
    sort: true,
    formatter: (value, row: IPurchasesReceipt) => {
      return this._accounting.ac.formatMoney(value, '');
    },
    columnClasses: 'right-align',
  }, {
    i18nLabel: 'ui.purchasesReceipt.list.column.status',
    field: 'status',
    sort: true,
    formatter: (value, row: IPurchasesReceipt) => {
      return _.startCase(value);
    },
  }];
  tableColumnsShow: boolean = false;
  tableColumnsToggle = this._gridTableToggle.mapToggleOptionsFromColumns(this.tableColumns);

  retryDialogShownState: boolean = false;
  retryDialogOnRetry: Function;

  private loadDataTimeoutId: number;
  private gridFirstLoad: boolean = true;
  private subscriptions: { [key: string]: Subscription[] } = {
    load: []
  };

  constructor(
    public _accounting: AccountingService,
    private _activatedRoute: ActivatedRoute,
    private _adminLayout: AdminLayoutService,
    private _gridTable: GridTableService,
    private _dataTableExport: ExportDataTableService,
    private _gridTableToggle: GridTableToggleService,
    public _purchasesReceiptClone: PurchasesReceiptCloneService,
    private _purchasesReceiptMoreFilter: PurchasesReceiptMoreFilterService,
    private _purchasesReceiptRest: PurchasesReceiptRestService,
    public _systemMessage: SystemMessageService,
    private _translate: TranslateService,
  ) {
    this._adminLayout.containerType = 1;
  }

  ngOnInit() {
    this.loadData();
  }

  ngOnDestroy() {
    this._purchasesReceiptMoreFilter.lastMoreFilterValues = {};
  }

  onMoreFilter(filters: IGridTableFilterParsed) {
    this.elPopMoreFilter.hide();

    this.moreFilterValues = filters;
    this.loadData();
  }

  populateFilters() {
    let newFilters: any = _.pick(this.qParamsFilters, ['keyword']);

    if (this.moreFilterValues) {
      newFilters = Object.assign({}, newFilters, this.moreFilterValues.qParams);
    }

    this.qParamsFilters = newFilters;
  }

  gridLoadDataWrapper() {
    // Grid always load at the first time. we dont want that. we already call on ngOnInit
    if (this.gridFirstLoad) {
      this.gridFirstLoad = false;
      return;
    }

    if (this.loadDataTimeoutId) {
      window.clearTimeout(this.loadDataTimeoutId);
      this.loadDataTimeoutId = null;
    }

    this.loadDataTimeoutId = window.setTimeout(() => {
      this.loadData();
    }, 300);
  }

  showVoidDialog(receiptId: string) {
    swal({
      title: this._translate.instant('ui.purchasesReceipt.confirm.void.label'),
      text: this._translate.instant('ui.purchasesReceipt.confirm.void.description'),
      type: 'question',
      showCancelButton: true,
    })
      .then(() => {
        this.voidReceipt(receiptId);
      })
      .catch(() => { });
  }

  voidReceipt(receiptId: string) {
    this._purchasesReceiptRest.void(receiptId)
    .catch((error) => {
      this._systemMessage.log({
        message: error.response.data[0].errorMessage,
        type: 'error'
      });
      return Observable.throw(error);
    })
    .subscribe(response => {
      this.gridLoadDataWrapper();
      SnackBar.show({
        text: this._translate.instant('ui.purchasesReceipt.success.void'),
        pos: 'bottom-right'
      });
    });
  }

  printReport(receiptId: string) {
    this._purchasesReceiptRest.load(receiptId).subscribe(response => {
      this.doc = response;
      this._purchasesReceiptRest.loadRelatedData().subscribe(related => {
        this.doc['companyInfo'] = related.settings.companyInfo;
        this.elReportModal.show();
      });
    });
  }

  get exportRecords(): Observable<IPurchasesReceipt[]> {
    const qOption = {
      ...this._gridTable.generateApiQueryOptionFromGridInfo(
        this.gridDataSource.pager,
        false,
        this.gridTable,
        this._gridTable.generateCombinedApiFilters(this.combinedFilterValues, this.combinedFilterFields)
      ),
      skip: 0,
      take: this._dataTableExport.exportLimit,
    };

    this.populateFilters();

    return this.elRetryDialog.createRetryEntry(
      this._purchasesReceiptRest.findAll(qOption, this.qParamsFilters).map(response => response.data)
    );
  }

  exportPDF() {
    this.exportRecords.subscribe(records => {
      this._dataTableExport.export({
        records,
        mapOptions: this._dataTableExport.mapFromTableColumns(this.tableColumns),
        fileName: 'all-purchase-receipts',
        extension: 'pdf',
        templateData: {
          title: this._translate.instant('ui.purchaseExpense.list.title'),
        },
      });
    });
  }

  exportExcel() {
    this.exportRecords.subscribe(records => {
      this._dataTableExport.export({
        records: records,
        mapOptions: this._dataTableExport.mapFromTableColumns(this.tableColumns),
        fileName: 'all-purchase-receipts',
        extension: 'xls',
      });
    });
  }

  loadData() {
    this.elPageLoading.forceShow();

    const qOption = this._gridTable.generateApiQueryOptionFromGridInfo(
      this.gridDataSource.pager,
      true,
      this.gridTable,
      this._gridTable.generateCombinedApiFilters(this.combinedFilterValues, this.combinedFilterFields)
    );

    this.populateFilters();

    this.subscriptions.load.forEach(sub => sub.unsubscribe());
    this.subscriptions.load.push(
      this.elRetryDialog.createRetryEntry(
        this._purchasesReceiptRest.findAll(qOption, this.qParamsFilters)
          .finally(() => {
            this.elPageLoading.forceHide();
          })
      ).subscribe(response => {
        this.gridDataSource.updateFromApiPaginationResult(response);
        this.compReady = true;
      })
    );
  }

  cloneTransaction(doc) {
    this.elPageLoading.forceShow();

    this._purchasesReceiptRest.load(doc.id).subscribe(newDoc => {
      this.elPageLoading.forceHide();

      this._purchasesReceiptClone.copy(newDoc);
    });
  }

  public onGridChange() {
    this.gridLoadDataWrapper();
  }

  public onGridPageChange() {
    this.gridLoadDataWrapper();
  }
}

import * as _ from 'lodash';
import { Component, OnChanges, OnInit, SimpleChanges, } from '@angular/core';
import { FormGroup } from '@angular/forms';
import 'app/rxjs-imports.ts';

import { AccountingService, AutocompleteService, CommonService } from '../../../core/core.module';
import { PurchasesReceiptCloneService } from '../../purchases-receipt/purchases-receipt-clone.service';
import { PurchasesReceiptFormService } from '../form/purchases-receipt-form.service';
import { PurchasesReceiptRestService } from '../purchases-receipt-rest.service';

import { TransactionFormPurchasesBComponent } from '../../transaction/transaction-form-purchases.bcomponent';

@Component({
  selector: 'app-purchases-receipt-form',
  templateUrl: 'purchases-receipt-form.component.html',
})
export class PurchasesReceiptFormComponent extends TransactionFormPurchasesBComponent implements OnChanges, OnInit {
  cloneTransaction: boolean = false;

  ACProductVariantHandler = CommonService.remoteACItemsHandler({
    remoteParams: this.ACProductVariantParams.bind(this),
    remoteRequest: this._productVariantRest.findAllPicker.bind(this._productVariantRest),
    remoteRequestMap: (response) => response.data,
  });

  constructor(
    public _autocomplete: AutocompleteService,
    public _accounting: AccountingService,
    private _purchasesReceiptClone: PurchasesReceiptCloneService,
    public _purchasesReceiptForm: PurchasesReceiptFormService,
    private _purchasesReceiptRest: PurchasesReceiptRestService,
  ) {
    super();
    super.init();

    this.componentId = 'PurchasesReceiptForm';

    this.registerHook('loadRelated', event => {
      return this._purchasesReceiptRest.loadRelatedData();
    }, reletedData => {
      this._purchasesReceiptForm.patchTransactionSettings(this.form, reletedData.settings);

      if (this._purchasesReceiptClone.docOnHold) {
        const parentDoc = <any>this._purchasesReceiptClone.docOnHold;
        parentDoc.deliveryDate = null;
        parentDoc.transactionDate = new Date;
        parentDoc.transactionNumber = null;

        this.cloneTransaction = true;
        this.doc = parentDoc;
        this._purchasesReceiptClone.docOnHold = null;

        this.applyDoc();

        this.form.patchValue(this.doc);
      }
    });

    this.registerHook('patchForm', event => {
      this.applyDoc();
    });
  }

  ngOnInit() {
    super.ngOnInit();

    this.addLines();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (_.has(changes, 'doc') && this.doc) {
      this.applyDoc();
    }
  }

  getDefaultValue(type: 'master' | 'line'): any {
    return this._purchasesReceiptForm.getDefaultValues(type);
  }

  buildFormChildLine() {
    return this._purchasesReceiptForm.buildFormChildLine();
  }

  lineConditionalValidation(formGroup: FormGroup) {
    this._purchasesReceiptForm.lineConditionalValidation(formGroup);
  }
}

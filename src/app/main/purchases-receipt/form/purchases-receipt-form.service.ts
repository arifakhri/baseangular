import * as _ from 'lodash';
import { CustomValidators } from 'ng2-validation';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { Injectable } from '@angular/core';

import { MPurchasesReceiptForm, MPurchasesReceiptFormLine } from './purchases-receipt-form.model';

import { StartingDataService } from '../../../core/core.module';

@Injectable()
export class PurchasesReceiptFormService {
  constructor(
    private _startingData: StartingDataService,
  ) { }

  getDefaultValues(type: 'master' | 'line'): { [key: string]: any } {
    let model: any;
    switch (type) {
      case 'master':
        const startingData = this._startingData.data$.getValue();
        model = new MPurchasesReceiptForm;
        model.defaultValues = {
          branchId: startingData.masterBranchId,
        };
        break;
      case 'line':
        const startingDataLine = this._startingData.data$.getValue();
        model = new MPurchasesReceiptFormLine;
        model.defaultValues = {
          uomConversion: 1,
          warehouseId: startingDataLine.masterWarehouseId,
        };
        break;
    }
    return model;
  }

  patchTransactionSettings(form: FormGroup, settings: any) {
    if (form.contains('warehouseId') && !form.get('warehouseId').value && _.has(settings.productSetting, 'masterWarehouseId')) {
      form.patchValue({ warehouse: settings.productSetting.masterWarehouse });
      form.patchValue({ warehouseId: settings.productSetting.masterWarehouseId });
    }
  }

  setFormDefinitions(form: FormGroup) {
    const startingData = this._startingData.data$.getValue();
    const vendorEmailControl = new FormControl('', [CustomValidators.email, Validators.maxLength(150)]);
    (<any>vendorEmailControl).validatorData = {
      maxLength: 150
    };
    const vendorRefNumberControl = new FormControl('', Validators.maxLength(30));
    (<any>vendorRefNumberControl).validatorData = {
      maxLength: 30
    };


    form.addControl('branchId', new FormControl(startingData.masterBranchId));
    form.addControl('vendor', new FormControl);
    form.addControl('vendorId', new FormControl(null, Validators.required));
    form.addControl('vendorEmail', vendorEmailControl);
    form.addControl('billingAddress', new FormControl);
    form.addControl('shippingAddress', new FormControl);
    form.addControl('description', new FormControl);
    form.addControl('paymentMethod', new FormControl);
    form.addControl('paymentMethodId', new FormControl);
    form.addControl('paymentAccount', new FormControl);
    form.addControl('paymentAccountId', new FormControl(null, Validators.required));
    form.addControl('transactionNumber', new FormControl);
    form.addControl('vendorRefNumber', vendorRefNumberControl);
    form.addControl('transactionDate', new FormControl(new Date(), Validators.required));
    form.addControl('vendorNote', new FormControl);
    form.addControl('subtotal', new FormControl);
    form.addControl('discountPercent', new FormControl('', CustomValidators.number));
    form.addControl('discountAmount', new FormControl(null, [Validators.required, CustomValidators.number]));
    form.addControl('taxAmount', new FormControl);
    form.addControl('shippingCharge', new FormControl(null, [Validators.required, CustomValidators.number]));
    form.addControl('adjustmentAmount', new FormControl(null, [Validators.required, CustomValidators.number]));
    form.addControl('total', new FormControl(0));
    form.addControl('taxed', new FormControl);
    form.addControl('note', new FormControl);
    form.addControl('warehouse', new FormControl);
    form.addControl('warehouseId', new FormControl(null, Validators.required));
    form.addControl('lines', new FormArray([]));

    form.get('paymentMethod').valueChanges.subscribe(paymentMethod => {
      if (paymentMethod && paymentMethod.defaultAccount) {
        form.get('paymentAccount').setValue(paymentMethod.defaultAccount);
        form.get('paymentAccountId').setValue(paymentMethod.defaultAccount.id);
      }
    });

    form.patchValue(this.getDefaultValues('master'));
  }

  buildFormChildLine() {
    const formGroup = new FormGroup({
      id: new FormControl(null),
      rowVersion: new FormControl,
      amount: new FormControl,
      description: new FormControl,
      discountPercent: new FormControl,
      discountAmount: new FormControl,
      expenseAccountId: new FormControl,
      note: new FormControl,
      product: new FormControl,
      productId: new FormControl,
      productVariant: new FormControl,
      productVariantId: new FormControl,
      qty: new FormControl,
      tax: new FormControl,
      taxId: new FormControl,
      taxRate: new FormControl,
      totalDiscount: new FormControl,
      unitPrice: new FormControl,
      uom: new FormControl,
      uomId: new FormControl,
      uomConversion: new FormControl,
      warehouseId: new FormControl,
    });

    formGroup.patchValue(this.getDefaultValues('line'));

    return formGroup;
  }

  lineConditionalValidation(formGroup: FormGroup) {
    const formGroupValue = formGroup.value;
    if (
      formGroup.dirty &&
      _.size(<any>_.pickBy(_.omit(formGroupValue, ['amount', 'uomConversion', 'warehouseId']), _.identity))
    ) {
      formGroup.get('productVariant').setValidators([Validators.required]);
      formGroup.get('productVariant').updateValueAndValidity();
      formGroup.get('productVariantId').setValidators([Validators.required]);
      formGroup.get('productVariantId').updateValueAndValidity();
      formGroup.get('qty').setValidators([Validators.required]);
      formGroup.get('qty').updateValueAndValidity();
      formGroup.get('unitPrice').setValidators([Validators.required]);
      formGroup.get('unitPrice').updateValueAndValidity();
    } else {
      formGroup.get('productVariant').clearValidators();
      formGroup.get('productVariant').updateValueAndValidity();
      formGroup.get('productVariantId').clearValidators();
      formGroup.get('productVariantId').updateValueAndValidity();
      formGroup.get('qty').clearValidators();
      formGroup.get('qty').updateValueAndValidity();
      formGroup.get('unitPrice').clearValidators();
      formGroup.get('unitPrice').updateValueAndValidity();
    }
  }
}

import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { AccountingService } from '../../../core/core.module';
import { AdminLayoutService } from '../../../layouts/layouts.module';
import { PurchasesReceiptCloneService } from '../purchases-receipt-clone.service';
import { PurchasesReceiptRestService } from '../purchases-receipt-rest.service';

@Component({
  selector: 'app-purchases-receipt-detail',
  templateUrl: 'purchases-receipt-detail.component.html'
})
export class PurchasesReceiptDetailComponent implements OnInit {
  doc: IPurchasesReceipt;
  routeParams: any;

  constructor(
    public _accounting: AccountingService,
    private _adminLayout: AdminLayoutService,
    public _purchasesReceiptClone: PurchasesReceiptCloneService,
    private _purchasesReceiptRest: PurchasesReceiptRestService,
    private _route: ActivatedRoute,
  ) {
    this._adminLayout.containerType = 1;
    this._route.params.subscribe(params => this.routeParams = params);
  }

  ngOnInit() {
    this.loadData();
  }

  loadData() {
    this._purchasesReceiptRest.load(this.routeParams.id).subscribe(expense => {
      this.doc = expense;
    });
  }
}

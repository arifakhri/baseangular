import { NgModule } from '@angular/core';

import { PurchasesReceiptModule } from './purchases-receipt.module';
import { PurchasesReceiptRoutingModule } from './purchases-receipt-routing.module';

@NgModule({
  imports: [
    PurchasesReceiptModule,
    PurchasesReceiptRoutingModule
  ]
})
export class PurchasesReceiptLazyModule { }

import { Injectable } from '@angular/core';
import 'app/rxjs-imports.ts';

import { ApiQueryOption, RequestService } from '../../core/core.module';

import { APP_CONST } from '../../app.const';

@Injectable()
export class PurchasesReceiptRestService {
  baseURL = `${APP_CONST.API_MAIN}/purchases/receipts`;
  request = this._request.new(this.baseURL);

  constructor(
    private _request: RequestService,
  ) { }

  create(receipt: IPurchasesReceipt, queryParams: any = {}) {
    return this.request.post<IPurchasesReceipt>(``, receipt, { params: queryParams });
  }

  findAll(queryOption: ApiQueryOption, queryParams: any = {}) {
    return this.request.post<IApiPaginationResult<IPurchasesReceipt>>(`q`, queryOption, { params: queryParams });
  }

  load(receiptId: string) {
    return this.request.get<IPurchasesReceipt>(`${receiptId}?includeLines=true`);
  }

  loadRelatedData() {
    return this.request.get<{
      paymentAccounts: IAccount[],
      paymentMethods: IPaymentMethod[],
      taxes: ITax[],
      warehouses: IWarehouse[],
      settings: ISettings,
    }>(`entry-related-data`);
  }

  update(receiptId: string, updateObj: IPurchasesReceipt, queryParams: any = {}) {
    return this.request.put<IPurchasesReceipt>(`${receiptId}`, updateObj, { params: queryParams });
  }

  void(receiptId: string) {
    return this.request.put<IPurchasesReceipt>(`${receiptId}/void`, {});
  }
}

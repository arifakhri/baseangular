import * as _ from 'lodash';
import * as moment from 'moment';
import { Component, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';

import { PurchasesReceiptFormService } from '../form/purchases-receipt-form.service';
import { PurchasesReceiptRestService } from '../purchases-receipt-rest.service';
import { PurchasesReceiptService } from '../purchases-receipt.service';

import { BaseUpdateBComponent } from '../../../shared/base/base.module';

@Component({
  selector: 'app-purchases-receipt-update',
  templateUrl: 'purchases-receipt-update.component.html',
})
export class PurchasesReceiptUpdateComponent extends BaseUpdateBComponent {
  @ViewChild('reportModal') elReportModal: ModalDirective;


  constructor(
    private _purchasesReceipt: PurchasesReceiptService,
    private _purchasesReceiptForm: PurchasesReceiptFormService,
    private _purchasesReceiptRest: PurchasesReceiptRestService,
  ) {
    super();

    this.componentId = 'PurchasesReceiptUpdate';
    this.headerTitle = 'ui.purchasesReceipt.update.title';
    this.containerType = 1;
    this.routeURL = '/purchases/receipts';
    this.entrySuccessI18n = 'success.purchasesReceipt.update';

    this.registerHook('buildForm', event => {
      this._purchasesReceiptForm.setFormDefinitions(this.form);
    });

    this.registerHook('load', event => {
      return this._purchasesReceiptRest.load(this.page.routeParams.id).switchMap(doc => {
        if (doc.transactionDate) {
          doc.transactionDate = moment(doc.transactionDate).toDate();
        }
        return Observable.of(doc);
      });
    });

    this.registerHook('save', event => {
      const receipt: IPurchasesReceipt = Object.assign({}, this.doc, this.form.value);
      this._purchasesReceipt.normalizeDoc(receipt);

      const isPrinting = _.get(event, 'data.print');

      return this._purchasesReceiptRest.update(this.page.routeParams.id, receipt, { includeLines: isPrinting });
    });
  }

  async saveAndPrint() {
    try {
      const submitted = await this.onSubmit({ saveConfirm: true, print: true });
      if (submitted) {
        this._purchasesReceiptRest.loadRelatedData().subscribe(related => {
          this.doc['companyInfo'] = related.settings.companyInfo;
          this.elReportModal.show();
        });
      }
    } catch (e) {

    }
  }
}

import * as _ from 'lodash';
import { Component, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import 'app/rxjs-imports.ts';

import { PurchasesReceiptFormService } from '../form/purchases-receipt-form.service';
import { PurchasesReceiptRestService } from '../purchases-receipt-rest.service';
import { PurchasesReceiptService } from '../purchases-receipt.service';

import { BaseCreateBComponent } from '../../../shared/base/base.module';
import { MPurchasesReceipt } from '../purchases-receipt.model';

@Component({
  selector: 'app-purchases-receipt-create',
  templateUrl: 'purchases-receipt-create.component.html',
})
export class PurchasesReceiptCreateComponent extends BaseCreateBComponent {
  @ViewChild('reportModal') elReportModal: ModalDirective;

  constructor(
    private _purchasesReceipt: PurchasesReceiptService,
    private _purchasesReceiptForm: PurchasesReceiptFormService,
    private _purchasesReceiptRest: PurchasesReceiptRestService,
  ) {
    super();

    this.componentId = 'PurchasesReceiptCreate';
    this.headerTitle = 'ui.purchasesReceipt.create.title';
    this.containerType = 1;
    this.routeURL = '/purchases/receipts';
    this.entrySuccessI18n = 'success.purchasesReceipt.create';

    this.autoprintButtons = [{
      checkboxConfigId: 'autoprint.sreceipt.create',
      checkboxLabel: 'ui.purchasesReceipt.create.action.apCheckbox.report',
      previewLabel: 'ui.purchasesReceipt.create.action.previewReport',
      onPreview: () => {
        this.saveAndPrint();
      }
    }];

    this.registerHook('buildForm', event => {
      this._purchasesReceiptForm.setFormDefinitions(this.form);
    });

    this.registerHook('save', event => {
      let receipt = new MPurchasesReceipt;
      receipt = _.assign(receipt, this.form.value);
      this._purchasesReceipt.normalizeDoc(receipt);

      const isPrinting = _.get(event, 'data.print');

      return this._purchasesReceiptRest.create(receipt, { includeLines: isPrinting });
    });
  }

  async saveAndPrint() {
    try {
      const submitted = await this.onSubmit({ saveConfirm: true, print: true });
      if (submitted) {
        this._purchasesReceiptRest.loadRelatedData().subscribe(related => {
          this.doc['companyInfo'] = related.settings.companyInfo;
          this.elReportModal.show();
        });
      }
    } catch (e) {

    }
  }
}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PurchasesReceiptCreateComponent } from './create/purchases-receipt-create.component';
import { PurchasesReceiptDetailComponent } from './detail/purchases-receipt-detail.component';
import { PurchasesReceiptListComponent } from './list/purchases-receipt-list.component';
import { PurchasesReceiptUpdateComponent } from './update/purchases-receipt-update.component';

import { AuthorizationService } from '../../core/core.module';

export const routes: Routes = [{
  path: '',
  children: [{
    path: '',
    component: PurchasesReceiptListComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'purchasesReceipt.list',
    },
  }, {
    path: 'create',
    component: PurchasesReceiptCreateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'purchasesReceipt.create',
    },
  }, {
    path: ':id',
    component: PurchasesReceiptDetailComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'purchasesReceipt.detail',
    },
  }, {
    path: ':id/update',
    component: PurchasesReceiptUpdateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'purchasesReceipt.update',
    },
  }]
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class PurchasesReceiptRoutingModule { }

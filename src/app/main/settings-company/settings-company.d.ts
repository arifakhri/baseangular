declare interface ISettingsCompany {
  companyName: string;
  phone: string;
  fax: string;
  email: string;
  website: string;
  postalAddressSameAsMain: boolean;
  shippingAddressSameAsMain: boolean;
  legalAddressSameAsMain: boolean;
  logoId: string;
  industry: {
    id: string;
    name: string;
  };
  mainAddress: {
    attention: string;
    street1: string;
    street2: string;
    city: string;
    stateProvince: string;
    countryId: string;
    postalCode: string;
    longitude: number;
    latitude: number;
    country: {
      id: string;
      name: string;
      phoneCountryCode: string
    };
    fullAddress: string;
  };
  postalAddress: {
    attention: string;
    street1: string;
    street2: string;
    city: string;
    stateProvince: string;
    countryId: string;
    postalCode: string;
    longitude: number;
    latitude: number;
    country: {
      id: string;
      name: string;
      phoneCountryCode: string;
    };
    fullAddress: string;
  };
  shippingAddress: {
    attention: string;
    street1: string;
    street2: string;
    city: string;
    stateProvince: string;
    countryId: string;
    postalCode: string;
    longitude: number;
    latitude: number;
    country: {
      id: string;
      name: string;
      phoneCountryCode: string;
    };
    fullAddress: string;
  };
  legalAddress: {
    attention: string;
    street1: string;
    street2: string;
    city: string;
    stateProvince: string;
    countryId: string;
    postalCode: string;
    longitude: number;
    latitude: number;
    country: {
      id: string;
      name: string;
      phoneCountryCode: string;
    };
    fullAddress: string;
  };
  logo: {
    id: string;
    fileType: string;
    fileName: string;
    fileExt: string;
    fileSize: number;
    description: string;
    uploadByUserId: string;
    fileUrl: string;
    thumbnailUrl: string;
    smallUrl: string;
    mediumUrl: string;
    largeUrl: string;
    tabletUrl: string;
  };
}

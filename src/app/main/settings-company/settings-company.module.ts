import { AutoCompleteModule } from 'primeng/components/autocomplete/autocomplete';
import { CommonModule } from '@angular/common';
import { CheckboxModule } from 'primeng/components/checkbox/checkbox';
import { DataTableModule } from 'primeng/components/datatable/datatable';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgModule } from '@angular/core';
import { PaginationModule } from 'ngx-bootstrap';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { SettingsCompanyFormComponent } from './form/settings-company-form.component';
import { SettingsCompanyFormAddressComponent } from './form-address/settings-company-form-address.component';
import { SettingsCompanyFormCompanyComponent } from './form-company/settings-company-form-company.component';
import { SettingsCompanyFormContactComponent } from './form-contact/settings-company-form-contact.component';
import { SettingsCompanyUpdateComponent } from './update/settings-company-update.component';

import { SettingsCompanyService } from './settings-company.service';
import { SettingsCompanyRestService } from './settings-company-rest.service';

import { CoreModule } from '../../core/core.module';
import { SettingsModule } from '../settings/settings.module';
import { SharedModule } from '../../shared/shared.module';

export const PROVIDERS = [
  SettingsCompanyService,
  SettingsCompanyRestService,
];

@NgModule({
  imports: [
    AutoCompleteModule,
    CheckboxModule,
    CommonModule,
    CoreModule,
    DataTableModule,
    FlexLayoutModule,
    FormsModule,
    HttpModule,
    PaginationModule,
    ReactiveFormsModule,
    RouterModule,
    SettingsModule,
    SharedModule,
    TranslateModule,
  ],
  declarations: [
    SettingsCompanyFormComponent,
    SettingsCompanyFormAddressComponent,
    SettingsCompanyFormCompanyComponent,
    SettingsCompanyFormContactComponent,
    SettingsCompanyUpdateComponent,
  ],
})
export class SettingsCompanyModule { }

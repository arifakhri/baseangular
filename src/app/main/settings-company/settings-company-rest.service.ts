import { Injectable } from '@angular/core';
import 'app/rxjs-imports.ts';

import { RequestService } from '../../core/core.module';

import { APP_CONST } from '../../app.const';

@Injectable()
export class SettingsCompanyRestService {
  baseURL = `${APP_CONST.API_MAIN}/company`;
  baseURLCountry = `${APP_CONST.API_MAIN}/countries`;

  request = this._request.new(this.baseURL);
  requestCountry = this._request.new(this.baseURLCountry);

  constructor(
    private _request: RequestService,
  ) { }

  findAllCountries() {
    return this.requestCountry.get<any>(``)
      .map(response => response.data || []);
  }

  load() {
    return this.request.get<ISettingsCompany>(``);
  }

  update(updateObj: ISettingsCompany) {
    return this.request.put<ISettingsCompany>(``, updateObj);
  }
}

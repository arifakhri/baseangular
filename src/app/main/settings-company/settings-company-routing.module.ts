import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SettingsCompanyUpdateComponent } from './update/settings-company-update.component';

import { AuthorizationService } from '../../core/core.module';

export const routes: Routes = [{
  path: '',
  children: [{
    path: '',
    component: SettingsCompanyUpdateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'settingsCompany.update'
    }
  }]
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class SettingsCompanyRoutingModule { }

import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { AutocompleteService } from '../../../core/core.module';

@Component({
  selector: 'app-settings-company-form-address',
  templateUrl: 'settings-company-form-address.component.html'
})
export class SettingsCompanyFormAddressComponent {
  @Input() countries: ICountry[] = [];
  @Input() doc: string;
  @Input() parentForm: FormGroup;
  @Output() submit: EventEmitter<any> = new EventEmitter();

  updateForm: boolean = false;

  constructor(
    public _autocomplete: AutocompleteService
  ) {}

  onSubmit() {
    this.submit.emit();
  }
}

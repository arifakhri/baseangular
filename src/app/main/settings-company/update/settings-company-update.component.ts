import { ActivatedRoute, Router } from '@angular/router';
import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';
import { TranslateService } from '@ngx-translate/core';

import { SpinnerService } from '../../../shared/spinner/spinner.service';
import { SettingsCompanyRestService } from '../settings-company-rest.service';
import { SettingsCompanyService } from '../settings-company.service';
import { SystemMessageService } from '../../../core/core.module';
import { RetryDialogComponent } from '../../../shared/retry-dialog/retry-dialog.component';

@Component({
  selector: 'app-settings-company-update',
  templateUrl: 'settings-company-update.component.html',
  providers: [SystemMessageService]
})
export class SettingsCompanyUpdateComponent implements OnInit {
  @ViewChild('retryDialog') elRetryDialog: RetryDialogComponent;

  compReady: boolean;
  doc: ISettingsCompany;
  form: FormGroup = new FormGroup({});

  retryDialogShownState: boolean = false;
  retryDialogOnRetry: Function;

  constructor(
    @Inject('GlobalSystemMessage') private _globalSystemMessage: SystemMessageService,
    private _spinner: SpinnerService,
    private _settingsCompany: SettingsCompanyService,
    private _settingsCompanyRest: SettingsCompanyRestService,
    private _route: ActivatedRoute,
    private _router: Router,
    public _systemMessage: SystemMessageService,
    private _translate: TranslateService,
  ) { }

  ngOnInit() {
    this._settingsCompany.setFormDefinitions(this.form);

    this.loadData();
  }

  loadData() {
    this.elRetryDialog.createRetryEntry(this._settingsCompanyRest.load())
      .subscribe(settingCompany => {
        this.doc = settingCompany;
        this.form.patchValue({
          ...this.doc,
          legalAddress: this.doc.legalAddress || {},
          mainAddress: this.doc.mainAddress || {},
          postalAddress: this.doc.postalAddress || {},
          shippingAddress: this.doc.shippingAddress || {},
        });

        this.compReady = true;
      });
  }

  save(): void {
    const spinner = this._spinner.showDefault();

    const settingsCompany: ISettingsCompany = Object.assign({}, this.doc, this.form.value);
    this._settingsCompanyRest.update(settingsCompany)
      .catch(error => {
        spinner.dispose();

        this._systemMessage.log({
          message: error,
          type: 'error'
        });
        return Observable.throw(error);
      })
      .subscribe(result => {
        spinner.dispose();

        this.doc = result;
        this._globalSystemMessage.log({
          message: this._translate.instant('success.settingsCompany.update'),
          type: 'success',
          showAs: 'growl',
          showSnackBar: false,
        });
      });
  }

  onSubmit() {
    this.save();
  }
}

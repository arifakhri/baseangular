import { CustomValidators } from 'ng2-validation';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Injectable } from '@angular/core';
import 'app/rxjs-imports.ts';
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class SettingsCompanyService {
  constructor(
    private _formBuilder: FormBuilder,
    private _translate: TranslateService
  ) { }

  normalizeDoc(formValue: any) {
  }

  setFormDefinitions(form: FormGroup) {
    const companyNameControl = new FormControl('', Validators.maxLength(100));
    (<any>companyNameControl).validatorData = {
      maxLength: 100
    };
    const phoneControl = new FormControl('', [CustomValidators.digits, Validators.maxLength(30)]);
    (<any>phoneControl).validatorData = {
      maxLength: 30
    };
    const faxControl = new FormControl('', [CustomValidators.digits, Validators.maxLength(30)]);
    (<any>faxControl).validatorData = {
      maxLength: 30
    };
    const emailControl = new FormControl('', [CustomValidators.email, Validators.maxLength(150)]);
    (<any>emailControl).validatorData = {
      maxLength: 150
    };
    const websiteControl = new FormControl('', [
      Validators.pattern(
        '^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$'
      ),
      Validators.maxLength(150)
    ]);

    form.addControl('companyName', companyNameControl);
    form.addControl('phone', phoneControl);
    form.addControl('fax', faxControl);
    form.addControl('email', emailControl);
    form.addControl('website', websiteControl);
    form.addControl('logoId', new FormControl);
    form.addControl('mainAddress', this.buildFormChildAddress());
    form.addControl('postalAddressSameAsMain', new FormControl(true));
    form.addControl('postalAddress', this.buildFormChildAddress());
    form.addControl('shippingAddressSameAsMain', new FormControl(true));
    form.addControl('shippingAddress', this.buildFormChildAddress());
    form.addControl('legalAddressSameAsMain', new FormControl(true));
    form.addControl('legalAddress', this.buildFormChildAddress());

    form.controls.mainAddress.valueChanges.subscribe(values => {
      if (form.controls.postalAddressSameAsMain.value) {
        form.controls.postalAddress.patchValue(values);
      }
      if (form.controls.shippingAddressSameAsMain.value) {
        form.controls.shippingAddress.patchValue(values);
      }
      if (form.controls.legalAddressSameAsMain.value) {
        form.controls.legalAddress.patchValue(values);
      }
    });

    form.controls.postalAddressSameAsMain.valueChanges.subscribe(same => {
      if (same) {
        form.controls.postalAddress.patchValue(form.controls.mainAddress.value);
      } else {
        form.controls.postalAddress.reset();
      }
    });

    form.controls.shippingAddressSameAsMain.valueChanges.subscribe(same => {
      if (same) {
        form.controls.shippingAddress.patchValue(form.controls.mainAddress.value);
      } else {
        form.controls.shippingAddress.reset();
      }
    });

    form.controls.legalAddressSameAsMain.valueChanges.subscribe(same => {
      if (same) {
        form.controls.legalAddress.patchValue(form.controls.mainAddress.value);
      } else {
        form.controls.legalAddress.reset();
      }
    });
  }

  buildFormChildAddress() {
    const attentionControl = new FormControl('', Validators.maxLength(150));
    (<any>attentionControl).validatorData = {
      maxLength: 150
    };
    const cityControl = new FormControl('', Validators.maxLength(50));
    (<any>cityControl).validatorData = {
      maxLength: 50
    };
    const stateProvinceControl = new FormControl('', Validators.maxLength(50));
    (<any>stateProvinceControl).validatorData = {
      maxLength: 50
    };
    const countryIdControl = new FormControl(null, Validators.maxLength(5));
    (<any>countryIdControl).validatorData = {
      maxLength: 5
    };
    const postalCodeControl = new FormControl('', Validators.maxLength(10));
    (<any>postalCodeControl).validatorData = {
      maxLength: 10
    };

    return new FormGroup({
      attention: attentionControl,
      street1: new FormControl,
      street2: new FormControl,
      city: cityControl,
      stateProvince: stateProvinceControl,
      country: new FormControl(null),
      countryId: countryIdControl,
      postalCode: postalCodeControl,
      longitude : new FormControl,
      latitude : new FormControl,
    });
  }
}

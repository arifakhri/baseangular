import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-settings-company-form-company',
  templateUrl: 'settings-company-form-company.component.html'
})
export class SettingsCompanyFormCompanyComponent {
  @Input() doc: string;
  @Input() parentForm: FormGroup;
  @Output() submit: EventEmitter<any> = new EventEmitter();

  updateForm: boolean = false;

  onSubmit() {
    this.submit.emit();
  }

  onImageDeleted() {
    this.parentForm.patchValue({ logoId: null });
  }

  onImageUploaded(image) {
    this.parentForm.patchValue({ logoId: image.id });
  }
}

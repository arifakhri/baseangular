import { NgModule } from '@angular/core';
import { SettingsCompanyModule } from './settings-company.module';
import { SettingsCompanyRoutingModule } from './settings-company-routing.module';

@NgModule({
  imports: [
    SettingsCompanyModule,
    SettingsCompanyRoutingModule
  ]
})
export class SettingsCompanyLazyModule { }

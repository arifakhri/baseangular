import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-settings-company-form-contact',
  templateUrl: 'settings-company-form-contact.component.html'
})
export class SettingsCompanyFormContactComponent {
  @Input() doc: string;
  @Input() parentForm: FormGroup;
  @Output() submit: EventEmitter<any> = new EventEmitter();

  updateForm: boolean = false;

  onSubmit() {
    this.submit.emit();
  }
}

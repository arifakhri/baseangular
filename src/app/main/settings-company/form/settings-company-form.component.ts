import { Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { SettingsCompanyRestService } from '../settings-company-rest.service';

@Component({
  selector: 'app-settings-company-form',
  templateUrl: 'settings-company-form.component.html',
  styleUrls: ['./settings-company-form.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SettingsCompanyFormComponent implements OnInit {
  @Input() doc: string;
  @Input() form: FormGroup;
  @Output() submit: EventEmitter<ISettingsCompany> = new EventEmitter();
  @Output('compReady') compReady: EventEmitter<boolean> = new EventEmitter();

  countries: ICountry[] = [];

  constructor(
    private _settingsCompanyRest: SettingsCompanyRestService,
  ) {
  }

  ngOnInit() {
    this._settingsCompanyRest.findAllCountries().subscribe(countries => {
      this.countries = countries;
      this.compReady.emit(true);
    });
  }

  onSubmit() {
    this.submit.emit();
  }
}

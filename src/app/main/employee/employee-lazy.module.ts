import { NgModule } from '@angular/core';

import { EmployeeModule } from './employee.module';
import { EmployeeRoutingModule } from './employee-routing.module';

@NgModule({
  imports: [
    EmployeeModule,
    EmployeeRoutingModule
  ]
})
export class EmployeeLazyModule { }

import * as SnackBar from 'node-snackbar';
import swal from 'sweetalert2';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { DataTable } from 'primeng/components/datatable/datatable';
import { Observable, Subscription } from 'rxjs';
import 'app/rxjs-imports.ts';
import { TranslateService } from '@ngx-translate/core';

import { SpinnerDirective } from '../../../shared/spinner/spinner.directive';

import { ExportDataTableService, GridTableDataSource, GridTableService, GridTableToggleService, } from '../../../core/core.module';
import { EmployeeRestService } from '../employee-rest.service';
import { RetryDialogComponent } from '../../../shared/retry-dialog/retry-dialog.component';

@Component({
  selector: 'app-employee-list',
  templateUrl: 'employee-list.component.html'
})
export class EmployeeListComponent implements OnInit {
  @ViewChild('gridTable') public gridTable: DataTable;
  @ViewChild('pageLoading') elPageLoading: SpinnerDirective;
  @ViewChild('retryDialog') elRetryDialog: RetryDialogComponent;

  compReady: boolean;
  employees: IEmployee[] = [];
  gridDataSource: GridTableDataSource<IEmployee> = new GridTableDataSource<IEmployee>();
  selectedRecords: IEmployee[] = [];

  combinedFilterValues: string = '';
  combinedFilterFields = {
    displayName: true,
    phone: true,
    email: true,
  };

  tableColumns: IGridTableColumn[] = [{
    i18nLabel: 'ui.employee.list.column.displayName',
    field: 'displayName',
    link: (row: IBusinessPartner) => {
      return `/contacts/employees/${row.id}`;
    },
    sort: true,
  }, {
    i18nLabel: 'ui.employee.list.column.phone',
    field: 'phone',
    sort: true,
  }, {
    i18nLabel: 'ui.employee.list.column.email',
    field: 'email',
    sort: true,
  }];
  tableColumnsShow: boolean = false;
  tableColumnsToggle = this._gridTableToggle.mapToggleOptionsFromColumns(this.tableColumns);

  retryDialogShownState: boolean = false;
  retryDialogOnRetry: Function;

  private loadDataTimeoutId: number;
  private gridFirstLoad: boolean = true;
  private subscriptions: { [key: string]: Subscription[] } = {
    load: []
  };

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _gridTable: GridTableService,
    private _employeeRest: EmployeeRestService,
    private _dataTableExport: ExportDataTableService,
    private _translate: TranslateService,
    private _gridTableToggle: GridTableToggleService,

  ) { }

  ngOnInit() {
    this.loadData();
  }

  gridLoadDataWrapper() {
    // Grid always load at the first time. we dont want that. we already call on ngOnInit
    if (this.gridFirstLoad) {
      this.gridFirstLoad = false;
      return;
    }

    if (this.loadDataTimeoutId) {
      window.clearTimeout(this.loadDataTimeoutId);
      this.loadDataTimeoutId = null;
    }

    this.loadDataTimeoutId = window.setTimeout(() => {
      this.loadData();
    }, 300);
  }

  showDeleteDialog(employeeId: string) {
    swal({
      title: this._translate.instant('confirm.employee.delete.label'),
      text: this._translate.instant('confirm.employee.delete.description'),
      type: 'question',
      showCancelButton: true,
    })
      .then(() => {
        this._employeeRest.delete(employeeId).subscribe(response => {
          this.gridLoadDataWrapper();
          SnackBar.show({
            text: this._translate.instant('success.employee.delete'),
            pos: 'bottom-right'
          });
        });
      })
      .catch(() => { });
  }

  loadData() {
    this.elPageLoading.forceShow();

    const qOption = this._gridTable.generateApiQueryOptionFromGridInfo(
      this.gridDataSource.pager,
      true,
      this.gridTable,
      this._gridTable.generateCombinedApiFilters(this.combinedFilterValues, this.combinedFilterFields)
    );

    this.subscriptions.load.forEach(sub => sub.unsubscribe());
    this.subscriptions.load.push(
      this.elRetryDialog.createRetryEntry(
        this._employeeRest.findAll(qOption)
          .finally(() => {
            this.elPageLoading.forceHide();
          })
      ).subscribe(response => {
          this.employees = response.data;
          this.gridDataSource.updateFromApiPaginationResult(response);

          this.compReady = true;
        })
    );
  }

  get exportRecords(): Observable<IEmployee[]> {
    const qOption = {
      ...this._gridTable.generateApiQueryOptionFromGridInfo(
        this.gridDataSource.pager,
        false,
        this.gridTable,
        this._gridTable.generateCombinedApiFilters(this.combinedFilterValues, this.combinedFilterFields)
      ),
      skip: 0,
      take: this._dataTableExport.exportLimit,
    };
    return this.elRetryDialog.createRetryEntry(
      this._employeeRest.findAll(qOption).map(response => response.data)
    );
  }

  exportPDF() {
    this.exportRecords.subscribe(records => {
      this._dataTableExport.export({
        records,
        mapOptions: this._dataTableExport.mapFromTableColumns(this.tableColumns),
        fileName: 'employees-contact',
        extension: 'pdf',
        templateData: {
          title: this._translate.instant('ui.contact.list.title'),
        },
      });
    });
  }

  exportExcel() {
    this.exportRecords.subscribe(records => {
      this._dataTableExport.export({
        records: records,
        mapOptions: this._dataTableExport.mapFromTableColumns(this.tableColumns),
        fileName: 'employees-contact',
        extension: 'xls',
      });
    });
  }

  public onGridChange() {
    this.gridLoadDataWrapper();
  }

  public onGridPageChange() {
    this.gridLoadDataWrapper();
  }
}

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';

import { ApiQueryOption, RequestService } from '../../core/core.module';

import { APP_CONST } from '../../app.const';

@Injectable()
export class EmployeeRestService {
  baseURL = `${APP_CONST.API_MAIN}/contacts/employees`;
  request = this._request.new(this.baseURL);

  constructor(
    private _request: RequestService,
  ) { }

  create(employee: IEmployee) {
    return this.request.post<IEmployee>(``, employee);
  }

  findAll(queryOption: ApiQueryOption) {
    return this.request.post<IApiPaginationResult<IEmployee>>(`q`, queryOption);
  }

  load(employeeId: string) {
    return this.request.get<IEmployee>(`${employeeId}`);
  }

  loadRelatedData() {
    return this.request.get<{ countries: ICountry[] }>(`entry-related-data`);
  }

  update(employeeId: string, updateObj: IEmployee) {
    return this.request.put<IEmployee>(`${employeeId}`, updateObj);
  }

  delete(employeeId: string): Observable<any> {
    return this.request.delete<any>(`${employeeId}`);
  }

  checkDuplicate(
    values: {
      id?: string;
      firstName: string;
      middleName?: string;
      lastName?: string;
      phone?: string;
      mobile?: string;
      email?: string;
    }
  ) {
    return this.request.post<boolean>(`check-duplicate`, values);
  }
}

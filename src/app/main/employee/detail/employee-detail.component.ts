import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { EmployeeRestService } from '../employee-rest.service';

@Component({
  selector: 'app-employee-detail',
  templateUrl: 'employee-detail.component.html'
})
export class EmployeeDetailComponent implements OnInit {
  doc: IEmployee;
  routeParams: any;

  constructor(
    private _employeeRest: EmployeeRestService,
    private _route: ActivatedRoute,
  ) {
    this._route.params.subscribe(params => this.routeParams = params);
  }

  ngOnInit() {
    this.loadData();
  }

  loadData() {
    this._employeeRest.load(this.routeParams.id).subscribe(employee => {
      this.doc = employee;
    });
  }
}

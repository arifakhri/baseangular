import { AutoCompleteModule } from 'primeng/components/autocomplete/autocomplete';
import { CalendarModule } from 'primeng/components/calendar/calendar';
import { DataTableModule } from 'primeng/components/datatable/datatable';
import { ListboxModule } from 'primeng/components/listbox/listbox';
import { BsDropdownModule, PaginationModule } from 'ngx-bootstrap';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { EmployeeCreateComponent } from './create/employee-create.component';
import { EmployeeDetailComponent } from './detail/employee-detail.component';
import { EmployeeFormComponent } from './form/employee-form.component';
import { EmployeeListComponent } from './list/employee-list.component';
import { EmployeeUpdateComponent } from './update/employee-update.component';

import { EmployeeRestService } from './employee-rest.service';

import { ContactModule } from '../contact/contact.module';
import { CoreModule } from '../../core/core.module';
import { SharedModule } from '../../shared/shared.module';

export const PROVIDERS = [
  EmployeeRestService,
];

@NgModule({
  imports: [
    CommonModule,
    FlexLayoutModule,
    PaginationModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    ListboxModule,
    TranslateModule,
    AutoCompleteModule,
    BsDropdownModule,
    CalendarModule,
    DataTableModule,
    CoreModule,
    SharedModule,
    ContactModule
  ],
  declarations: [
    EmployeeCreateComponent,
    EmployeeDetailComponent,
    EmployeeFormComponent,
    EmployeeListComponent,
    EmployeeUpdateComponent,
  ],
})
export class EmployeeModule { }

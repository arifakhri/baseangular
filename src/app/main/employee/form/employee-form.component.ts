import * as _ from 'lodash';
import * as moment from 'moment';
import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { CustomValidators } from 'ng2-validation';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';

import { AutocompleteService } from '../../../core/core.module';
import { EmployeeRestService } from '../employee-rest.service';
import { RetryDialogComponent } from '../../../shared/retry-dialog/retry-dialog.component';

@Component({
  selector: 'app-employee-form',
  templateUrl: 'employee-form.component.html'
})
export class EmployeeFormComponent implements OnInit {
  @Input() doc: IEmployee;
  @Input() form: FormGroup;
  @Output('compReady') compReady: EventEmitter<boolean> = new EventEmitter();
  @ViewChild('retryDialog') elRetryDialog: RetryDialogComponent;

  countries: any[] = [];

  retryDialogShownState: boolean = false;
  retryDialogOnRetry: Function;

  displayNamesAvailable: string[] = [];

  constructor(
    public _autocomplete: AutocompleteService,
    private _employeeRest: EmployeeRestService,
    private _fb: FormBuilder,
  ) { }

  ngOnInit() {
    this.loadRelatedData();

    this.buildForm();

    this.applyDoc();
  }

  applyDoc() {
    if (this.doc) {
      if (this.doc.dateOfBirth) {
        this.doc.dateOfBirth = moment(this.doc.dateOfBirth).toDate();
      }
      if (this.doc.hireDate) {
        this.doc.hireDate = moment(this.doc.hireDate).toDate();
      }
      if (this.doc.releasedDate) {
        this.doc.releasedDate = moment(this.doc.releasedDate).toDate();
      }
      this.form.patchValue(this.doc);
    }
  }

  buildForm() {
    const firstNameControl = new FormControl('', Validators.maxLength(30));
    (<any>firstNameControl).validatorData = {
      maxLength: 30
    };
    const lastNameControl = new FormControl('', Validators.maxLength(30));
    (<any>lastNameControl).validatorData = {
      maxLength: 30
    };
    const displayNameControl = new FormControl('', [Validators.required, Validators.maxLength(150)]);
    (<any>displayNameControl).validatorData = {
      maxLength: 150
    };
    const emailControl = new FormControl('', [CustomValidators.email, Validators.maxLength(150)]);
    (<any>emailControl).validatorData = {
      maxLength: 150
    };
    const phoneControl = new FormControl('', [CustomValidators.digits, Validators.maxLength(30)]);
    (<any>phoneControl).validatorData = {
      maxLength: 30
    };
    const mobileControl = new FormControl('', [CustomValidators.digits, Validators.maxLength(30)]);
    (<any>mobileControl).validatorData = {
      maxLength: 30
    };
    const faxControl = new FormControl('', [CustomValidators.digits, Validators.maxLength(30)]);
    (<any>faxControl).validatorData = {
      maxLength: 30
    };
    const bankAccountNoControl = new FormControl('', Validators.maxLength(30));
    (<any>bankAccountNoControl).validatorData = {
      maxLength: 30
    };
    const bankAccountNameControl = new FormControl('', Validators.maxLength(50));
    (<any>bankAccountNameControl).validatorData = {
      maxLength: 50
    };
    const cityControl = new FormControl('', Validators.maxLength(50));
    (<any>cityControl).validatorData = {
      maxLength: 50
    };
    const stateProvinceControl = new FormControl('', Validators.maxLength(50));
    (<any>stateProvinceControl).validatorData = {
      maxLength: 50
    };
    const countryIdControl = new FormControl(null, Validators.maxLength(5));
    (<any>countryIdControl).validatorData = {
      maxLength: 5
    };
    const postalCodeControl = new FormControl('', Validators.maxLength(10));
    (<any>postalCodeControl).validatorData = {
      maxLength: 10
    };

    this.form.addControl('firstName', firstNameControl);
    this.form.addControl('lastName', lastNameControl);
    this.form.addControl('displayName', displayNameControl);
    this.form.addControl('gender', new FormControl(''));
    this.form.addControl('dateOfBirth', new FormControl(''));
    this.form.addControl('pictureId', new FormControl(null));
    this.form.addControl('email', emailControl);
    this.form.addControl('phone', phoneControl);
    this.form.addControl('mobile', mobileControl);
    this.form.addControl('fax', faxControl);
    this.form.addControl('idCardNo', new FormControl(''));
    this.form.addControl('address', this._fb.group({
      street1: new FormControl(),
      city: cityControl,
      stateProvince: stateProvinceControl,
      country: new FormControl,
      countryId: countryIdControl,
      postalCode: postalCodeControl
    }));
    this.form.addControl('hireDate', new FormControl);
    this.form.addControl('releasedDate', new FormControl);
    this.form.addControl('bankAccountNo', bankAccountNoControl);
    this.form.addControl('bankAccountName', bankAccountNameControl);
    this.form.addControl('note', new FormControl(''));

    Observable.merge(
      this.form.get('firstName').valueChanges,
      this.form.get('lastName').valueChanges,
    ).subscribe(() => {
      const firstName = this.form.get('firstName').value;
      const lastName = this.form.get('lastName').value;

      this.displayNamesAvailable = [];

      if (firstName || lastName) {
        this.displayNamesAvailable.push(`${firstName || ''} ${lastName || ''}`.trim());
        this.displayNamesAvailable.push(`${lastName || ''}${lastName && firstName ? ', ' + firstName : firstName || ''}`.trim());
        this.displayNamesAvailable = _.uniq(this.displayNamesAvailable);

        if (_.head(this.displayNamesAvailable)) {
          this.form.get('displayName').setValue(_.head(this.displayNamesAvailable));
        }
      }
    });
  }

  onDisplayNameTyped(AC, source, event: any) {
    this.form.get('displayName').setValue(event.query);
    this._autocomplete.onLocalACSearch(AC, source, ['name'], event.query);
  }

  onImageDeleted() {
    this.form.patchValue({ pictureId: null });
  }

  onImageUploaded(image) {
    this.form.patchValue({ pictureId: image.id });
  }

  loadRelatedData() {
    this.elRetryDialog.createRetryEntry(this._employeeRest.loadRelatedData())
      .subscribe(related => {
        this.countries = related.countries;

        this.compReady.emit(true);
      });
  }
}

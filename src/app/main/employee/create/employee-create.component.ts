import * as _ from 'lodash';
import swal from 'sweetalert2';
import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

import { CommonService, SystemMessageService } from '../../../core/core.module';
import { EmployeeRestService } from '../employee-rest.service';
import { SpinnerService } from '../../../shared/spinner/spinner.service';

@Component({
  selector: 'app-employee-create',
  templateUrl: 'employee-create.component.html',
  providers: [SystemMessageService]
})
export class EmployeeCreateComponent implements OnInit {
  compReady: boolean;
  form: FormGroup;

  formNavigations: IFormNav[] = [];

  constructor(
    @Inject('GlobalSystemMessage') private _globalSystemMessage: SystemMessageService,
    private _employeeRest: EmployeeRestService,
    private _fb: FormBuilder,
    private _spinner: SpinnerService,
    private _router: Router,
    private _translate: TranslateService,
    public _systemMessage: SystemMessageService,
  ) {
    this.defineFormNavigations();
    this._translate.onLangChange.subscribe(() => {
      this.defineFormNavigations();
    });
  }

  defineFormNavigations() {
    this.formNavigations = [{
      name: this._translate.instant('ui.employee.form.personalDetails'),
      target: '#fieldset-1'
    }, {
      name: this._translate.instant('ui.employee.form.address'),
      target: '#fieldset-2'
    }, {
      name: this._translate.instant('ui.employee.form.employeeInfo'),
      target: '#fieldset-3'
    }, {
      name: this._translate.instant('ui.employee.form.note'),
      target: '#fieldset-4'
    }];
  }

  ngOnInit() {
    this.form = this._fb.group({});
  }

  onSubmit({ saveAndNew, saveAndView }: any = {}): void {
    if (this.formValid()) {
      this._employeeRest.checkDuplicate(
        <any>_.pick(this.form.value, ['phone', 'mobile', 'email', 'firstName', 'middleName', 'lastName'])
      ).subscribe(exist => {
        if (exist) {
          swal({
            title: this._translate.instant('confirm.employee.exist.label'),
            text: this._translate.instant('confirm.employee.exist.description'),
            type: 'warning',
            showCancelButton: true
          }).then(() => {
            this.save().subscribe(result => {
              if (saveAndNew) {
                this._router.navigateByUrl('/empty', { skipLocationChange: true }).then(() => {
                  this._router.navigateByUrl('/contacts/employees/create');
                });
              } else if (saveAndView) {
                this._router.navigate(['/contacts/employees', result.id]);
              } else {
                this._router.navigateByUrl('/contacts/employees');
              }
            });
          }).catch();
        } else {
          this.save().subscribe(result => {
            if (saveAndNew) {
              this._router.navigateByUrl('/empty', { skipLocationChange: true }).then(() => {
                this._router.navigateByUrl('/contacts/employees/create');
              });
            } else if (saveAndView) {
              this._router.navigate(['/contacts/employees', result.id]);
            } else {
              this._router.navigateByUrl('/contacts/employees');
            }
          });
        }
      });
    }
  }

  formValid(): boolean {
    if (!this.form.valid) {
      CommonService.markAsDirty(this.form);
      this._systemMessage.log({
        message: this._translate.instant('error.form'),
        type: 'error'
      });
      return false;
    }
    return true;
  }

  save() {
    const spinner = this._spinner.showDefault();

    const employee: IEmployee = this.form.value;
    return this._employeeRest.create(employee)
      .catch(error => {
        spinner.dispose();

        this._systemMessage.log({
          message: error,
          type: 'error'
        });
        return Observable.throw(error);
      })
      .do(result => {
        spinner.dispose();

        this._globalSystemMessage.log({
          message: this._translate.instant('success.employee.create'),
          type: 'success',
          showAs: 'growl',
          showSnackBar: false,
        });
      });
  }
}

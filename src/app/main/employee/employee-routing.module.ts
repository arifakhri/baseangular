import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { EmployeeCreateComponent } from './create/employee-create.component';
import { EmployeeDetailComponent } from './detail/employee-detail.component';
import { EmployeeListComponent } from './list/employee-list.component';
import { EmployeeUpdateComponent } from './update/employee-update.component';

import { AuthorizationService } from '../../core/core.module';

export const routes: Routes = [{
  path: '',
  children: [{
    path: '',
    component: EmployeeListComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'contact.employee.list'
    }
  }, {
    path: 'create',
    component: EmployeeCreateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'contact.employee.create'
    }
  }, {
    path: ':id',
    component: EmployeeDetailComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'contact.employee.detail'
    }
  }, {
    path: ':id/update',
    component: EmployeeUpdateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'contact.employee.update'
    }
  }]
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class EmployeeRoutingModule { }

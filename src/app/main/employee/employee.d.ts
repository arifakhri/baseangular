declare interface IEmployee {
  id: string;
  code: string;
  displayName: string;
  company: string;
  title: string;
  firstName: string;
  middleName: string;
  lastName: string;
  phone: string;
  mobile: string;
  fax: string;
  email: string;
  website: string;
  dateOfBirth: string | Date;
  gender: string;
  picture: any;
  pictureId: string;
  note: string;
  hireDate: string | Date;
  releasedDate: string | Date;
}
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ReportsPurchasesOrderDetailPreviewComponent } from './preview/reports-purchases-order-detail-preview.component';

import { AuthorizationService } from '../../core/core.module';

const routes: Routes = [{
  path: '',
  children: [{
    path: '',
    component: ReportsPurchasesOrderDetailPreviewComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'reportsPurchasesOrderDetail.preview'
    }
  }]
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ],
})
export class ReportsPurchasesOrderDetailRoutingModule { }

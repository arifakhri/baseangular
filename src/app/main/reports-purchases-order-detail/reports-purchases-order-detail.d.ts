declare interface IReportsPurchasesOrderDetail {
  accountClass: string;
  accountType: string;
  accountName: string;
  total: 0;
}
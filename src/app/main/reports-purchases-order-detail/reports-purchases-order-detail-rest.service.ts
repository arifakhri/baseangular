import { Injectable } from '@angular/core';

import { ReportsRestService } from '../reports/reports-rest.service';

@Injectable()
export class ReportsPurchasesOrderDetailRestService {

  constructor(
    private _reportRest: ReportsRestService,
  ) { }

  findAll(queryParams: any = {}) {
    return this._reportRest.requestReport.post<IApiPaginationResult<IReportsPurchasesOrderDetail>>(
      `purchase-order-list-detail`,
      queryParams
    );
  }
}

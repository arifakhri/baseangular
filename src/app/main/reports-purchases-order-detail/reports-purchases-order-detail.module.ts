import { AutoCompleteModule } from 'primeng/components/autocomplete/autocomplete';
import { CalendarModule } from 'primeng/components/calendar/calendar';
import { CommonModule } from '@angular/common';
import { DataTableModule } from 'primeng/components/datatable/datatable';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MultiSelectModule } from 'primeng/components/multiselect/multiselect';
import { NgModule } from '@angular/core';
import { PaginationModule } from 'ngx-bootstrap';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { ReportsPurchasesOrderDetailPreviewComponent } from './preview/reports-purchases-order-detail-preview.component';

import { ReportsPurchasesOrderDetailRestService } from './reports-purchases-order-detail-rest.service';

import { SharedModule } from '../../shared/shared.module';

export const PROVIDERS = [
  ReportsPurchasesOrderDetailRestService,
];

@NgModule({
  imports: [
    AutoCompleteModule,
    CalendarModule,
    CommonModule,
    DataTableModule,
    FlexLayoutModule,
    FormsModule,
    MultiSelectModule,
    PaginationModule,
    ReactiveFormsModule,
    RouterModule,
    SharedModule,
    TranslateModule,
  ],
  declarations: [
    ReportsPurchasesOrderDetailPreviewComponent,
  ]
})
export class ReportsPurchasesOrderDetailModule {

}

import { NgModule } from '@angular/core';

import { ReportsPurchasesOrderDetailModule } from './reports-purchases-order-detail.module';
import { ReportsPurchasesOrderDetailRoutingModule } from './reports-purchases-order-detail-routing.module';

@NgModule({
  imports: [
    ReportsPurchasesOrderDetailModule,
    ReportsPurchasesOrderDetailRoutingModule,
  ],
})
export class ReportsPurchasesOrderDetailLazyModule { }

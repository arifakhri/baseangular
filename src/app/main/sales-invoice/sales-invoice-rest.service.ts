import { Injectable } from '@angular/core';
import 'app/rxjs-imports.ts';

import { ApiQueryOption, RequestService } from '../../core/core.module';

import { APP_CONST } from '../../app.const';

@Injectable()
export class SalesInvoiceRestService {
  baseURL = `${APP_CONST.API_MAIN}/sales/invoices`;
  request = this._request.new(this.baseURL);

  constructor(
    private _request: RequestService,
  ) { }

  create(invoice: ISalesInvoice, queryParams: any = {}) {
    return this.request.post<ISalesInvoice>(``, invoice, { params: queryParams });
  }

  findAll(queryOption: ApiQueryOption, queryParams: any = {}) {
    return this.request.post<IApiPaginationResult<ISalesInvoice>>(`q`, queryOption, { params: queryParams });
  }

  load(invoiceId: string) {
    return this.request.get<ISalesInvoice>(`${invoiceId}?includeLines=true`);
  }

  loadRelatedData() {
    return this.request.get<{
      shippingMethods: IShippingMethod[],
      shippingProviders: IShippingProvider[],
      taxes: ITax[],
      warehouses: IWarehouse[],
      settings: any,
    }>(`entry-related-data`);
  }

  update(invoiceId: string, updateObj: ISalesInvoice, queryParams: any = {}) {
    return this.request.put<ISalesInvoice>(`${invoiceId}`, updateObj, { params: queryParams });
  }

  void(invoiceId: string) {
    return this.request.put<ISalesInvoice>(`${invoiceId}/void`, {});
  }

  allocateCredit(invoiceId: string, credits: ISalesAllocateCredit[]) {
    return this.request.put<ISalesInvoice>(`${invoiceId}/allocate-credit`, credits);
  }

  autoAllocateDownPayment(invoiceId: string) {
    return this.request.put<ISalesInvoice>(`${invoiceId}/downpayments/auto-allocate`);
  }
}

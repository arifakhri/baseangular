import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { BsDropdownModule, ModalModule, PaginationModule, PopoverModule } from 'ngx-bootstrap';
import { AutoCompleteModule } from 'primeng/components/autocomplete/autocomplete';
import { CalendarModule } from 'primeng/components/calendar/calendar';
import { CheckboxModule } from 'primeng/components/checkbox/checkbox';
import { DataTableModule } from 'primeng/components/datatable/datatable';
import { ListboxModule } from 'primeng/components/listbox/listbox';

import { CoreModule } from '../../core/core.module';
import { SharedModule } from '../../shared/shared.module';
import { BusinessPartnerModule } from '../business-partner/business-partner.module';
import { ProductVariantModule } from '../product-variant/product-variant.module';
import { SalesDownPaymentModule } from '../sales-down-payment/sales-down-payment.module';
import { SalesPaymentModule } from '../sales-payment/sales-payment.module';
import { SalesCreditListModule } from '../sales/credit-list/sales-credit-list.module';
import { SalesPaymentCreditListModule } from '../sales/payment-credit-list/sales-payment-credit-list.module';
import { SalesModule } from '../sales/sales.module';
import { ShippingDetailModule } from '../shipping-detail/shipping-detail.module';
import { ShippingPriceModule } from '../shipping-price/shipping-price.module';
import { TransactionModule } from '../transaction/transaction.module';
import { SalesInvoiceCreateComponent } from './create/sales-invoice-create.component';
import { SalesInvoiceDetailComponent } from './detail/sales-invoice-detail.component';
import { SalesInvoiceFormComponent } from './form/sales-invoice-form.component';
import { SalesInvoiceFormService } from './form/sales-invoice-form.service';
import { SalesInvoiceListComponent } from './list/sales-invoice-list.component';
import { SalesInvoiceMoreFilterComponent } from './more-filter/sales-invoice-more-filter.component';
import { SalesInvoiceMoreFilterService } from './more-filter/sales-invoice-more-filter.service';
import { SalesInvoiceQuickCreateComponent } from './quick-create/sales-invoice-quick-create.component';
import { SalesInvoiceQuickFormComponent } from './quick-form/sales-invoice-quick-form.component';
import { SalesInvoiceCloneService } from './sales-invoice-clone.service';
import { SalesInvoiceCopyCreditNoteService } from './sales-invoice-copy-credit-note.service';
import { SalesInvoiceRestService } from './sales-invoice-rest.service';
import { SalesInvoiceService } from './sales-invoice.service';
import { SalesInvoiceUpdateComponent } from './update/sales-invoice-update.component';

export const PROVIDERS = [
  SalesInvoiceCloneService,
  SalesInvoiceCopyCreditNoteService,
  SalesInvoiceFormService,
  SalesInvoiceMoreFilterService,
  SalesInvoiceRestService,
  SalesInvoiceService,
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    FlexLayoutModule,
    HttpModule,
    ReactiveFormsModule,
    RouterModule,
    TranslateModule,
    AutoCompleteModule,
    BsDropdownModule,
    CalendarModule,
    CheckboxModule,
    DataTableModule,
    ModalModule,
    PaginationModule,
    CoreModule,
    SharedModule,
    ListboxModule,
    BusinessPartnerModule,
    PopoverModule,
    ProductVariantModule,
    SalesCreditListModule,
    SalesPaymentCreditListModule,
    SalesDownPaymentModule,
    SalesModule,
    SalesPaymentModule,
    ShippingDetailModule,
    ShippingPriceModule,
    TransactionModule,
  ],
  declarations: [
    SalesInvoiceCreateComponent,
    SalesInvoiceDetailComponent,
    SalesInvoiceFormComponent,
    SalesInvoiceListComponent,
    SalesInvoiceMoreFilterComponent,
    SalesInvoiceQuickCreateComponent,
    SalesInvoiceQuickFormComponent,
    SalesInvoiceUpdateComponent,
  ],
  exports: [
    SalesInvoiceQuickCreateComponent,
  ]
})
export class SalesInvoiceModule { }

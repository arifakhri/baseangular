import * as _ from 'lodash';
import * as moment from 'moment';
import { FormArray, FormGroup } from '@angular/forms';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable()
export class SalesInvoiceCopyCreditNoteService {
  private currentDoc: ISalesInvoice;
  private parentTransactionType: string = 'sales_invoice';

  constructor(
    private _router: Router
  ) { }

  set docOnHold(invoice) {
    this.currentDoc = invoice;
  }

  get docOnHold() {
    if (!this.currentDoc) {
      return null;
    }

    const targetDoc: any = _.cloneDeep(this.currentDoc);
    if (targetDoc.transactionDate) {
      targetDoc.transactionDate = moment(targetDoc.transactionDate).toDate();
    }

    targetDoc.parentTransactionType = this.parentTransactionType;

    targetDoc.lines.forEach(line => {
      line.parentTransactionId = line.salesTransactionId;
      line.parentTransactionLineId = line.id;
    });

    return targetDoc;
  }

  copy(invoice) {
    this.docOnHold = invoice;

    this._router.navigateByUrl('/sales/credit-notes/create');
  }

  reset(form: FormGroup) {
    form.get('parentTransactionType').reset(null);

    const linesFormGroup = <FormArray>form.get('lines');
    _.forEach(linesFormGroup.controls, (lineFormGroup, lineIdx) => {
      lineFormGroup.get('parentTransactionId').reset(null);
      lineFormGroup.get('parentTransactionLineId').reset(null);
      lineFormGroup.get('incomeAccountId').reset(null);
    });
  }
}

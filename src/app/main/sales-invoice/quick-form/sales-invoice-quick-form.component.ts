import * as _ from 'lodash';
import { Component, Input, OnInit } from '@angular/core';
import { FormArray, FormGroup } from '@angular/forms';

import { AutocompleteService } from '../../../core/core.module';
import { SalesInvoiceFormService } from '../form/sales-invoice-form.service';
import { SalesInvoiceRestService } from '../sales-invoice-rest.service';
import { SalesOrderCopyInvoiceService } from '../../sales-order/sales-order-copy-invoice.service';
import 'app/rxjs-imports.ts';

@Component({
  selector: 'app-sales-invoice-quick-form',
  templateUrl: 'sales-invoice-quick-form.component.html'
})
export class SalesInvoiceQuickFormComponent implements OnInit {
  @Input() form: FormGroup;

  copyTransaction: boolean = false;
  doc: ISalesInvoice;

  shippingMethods: IShippingMethod[] = [];
  shippingMethodsSuggestion: IShippingMethod[] = [];
  shippingProviders: IShippingProvider[] = [];
  shippingProviderProducts: IShippingProviderProduct[] = [];

  warehouses: any[] = [];

  constructor(
    public _autocomplete: AutocompleteService,
    private _salesInvoiceForm: SalesInvoiceFormService,
    private _salesInvoiceRest: SalesInvoiceRestService,
    private _salesOrderCopyInvoice: SalesOrderCopyInvoiceService,
  ) { }

  ngOnInit() {
    this.loadRelatedData();
  }

  addLines(length: number) {
    const formArray = (<FormArray>this.form.controls.lines);
    _.range(length).forEach(() => {
      const newFormGroup = this._salesInvoiceForm.buildFormChildLine();
      formArray.push(newFormGroup);
    });
  }

  loadRelatedData() {
    if (this._salesOrderCopyInvoice.docOnHold) {
      const parentDoc = <any>this._salesOrderCopyInvoice.docOnHold;
      parentDoc.deliveryDate = null;
      parentDoc.dueDate = new Date;
      parentDoc.transactionDate = new Date;
      parentDoc.transactionNumber = null;

      this.copyTransaction = true;
      this.doc = parentDoc;
      this._salesOrderCopyInvoice.docOnHold = null;

      this.applyDoc();

      this.form.patchValue(this.doc);
    }

    return this._salesInvoiceRest.loadRelatedData()
      .subscribe((related) => {
        this.shippingMethods = related.shippingMethods;
        this.shippingProviders = related.shippingProviders;
        this.warehouses = related.warehouses;
      });
  }

  applyDoc() {
      const linesFormArray = <FormArray>this.form.controls.lines;
      const currentLinesLength = linesFormArray.length;
      if (this.doc.lines.length > currentLinesLength) {
        this.addLines(this.doc.lines.length - currentLinesLength);
      }
      this.form.patchValue({ lines: this.doc.lines });
  }

  onShippingProviderSelected(shippingProvider: IShippingProvider) {
    this.form.get('shippingProviderId').setValue(shippingProvider.id);
    this.shippingProviderProducts = shippingProvider.products;
    this.form.get('shippingProviderProduct').reset(null);
    this.form.get('shippingProviderProductId').reset(null);
  }
}

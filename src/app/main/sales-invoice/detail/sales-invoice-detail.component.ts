import * as _ from 'lodash';
import swal from 'sweetalert2';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';
import { TranslateService } from '@ngx-translate/core';

import { AccountingService, RouteStateService } from '../../../core/core.module';
import { AdminLayoutService } from '../../../layouts/layouts.module';
import { BusinessPartnerRestService } from '../../business-partner/business-partner-rest.service';
import { SalesInvoiceCloneService } from '../sales-invoice-clone.service';
import { SalesInvoiceCopyCreditNoteService } from '../sales-invoice-copy-credit-note.service';
import { SalesInvoiceRestService } from '../sales-invoice-rest.service';

@Component({
  selector: 'app-sales-invoice-detail',
  templateUrl: 'sales-invoice-detail.component.html'
})
export class SalesInvoiceDetailComponent implements OnInit {
  compReady: boolean = false;
  creditOffered: boolean = false;
  credits: IBusinessPartnerARCredit[] = [];
  doc: ISalesInvoice;
  relatedData: any;
  routeParams: any;

  barcodeReport: any;
  mainReportParams: any;

  @ViewChild('quickPayModal') elQuickPayModal: ModalDirective;
  @ViewChild('allocateCreditModal') elAllocateCreditModal: ModalDirective;

  constructor(
    public _accounting: AccountingService,
    private _adminLayout: AdminLayoutService,
    private _businessPartnerRest: BusinessPartnerRestService,
    private _route: ActivatedRoute,
    private _routeState: RouteStateService,
    public _salesInvoiceClone: SalesInvoiceCloneService,
    public _salesInvoiceCopyCreditNote: SalesInvoiceCopyCreditNoteService,
    public _salesInvoiceRest: SalesInvoiceRestService,
    private _translate: TranslateService,
  ) {
    this._adminLayout.containerType = 1;
    this._route.params.subscribe(params => this.routeParams = params);
  }

  ngOnInit() {
    this.loadData().subscribe();
  }

  loadData(): Observable<[ISalesInvoice, any, any]> {
    return Observable.zip(
      this._salesInvoiceRest.load(this.routeParams.id),
      this._salesInvoiceRest.loadRelatedData(),
    ).switchMap(([invoice, relatedData]) => {
      return this._businessPartnerRest.getARCredits(invoice.customer.id)
        .do(credits => {
          this.doc = invoice;
          this.doc.siCepatMemberId = relatedData.settings.salesSetting.siCepatMemberId;

          this.relatedData = relatedData;

          this.barcodeReport = {
            config: {
              templateCode: 'BAR001',
            },
            data: this.doc,
          };

          this.mainReportParams = {
            companyInfo: relatedData.settings.companyInfo,
          };

          this.credits = credits;

          if (this.credits.length) {
            this.offerAllocateCredit();
          }

          this.compReady = true;
        })
        .switchMap(credits => {
          return [invoice, relatedData, credits];
        });
    });
  }

  onPaymentQuickCreated(payment) {
    this.loadData().subscribe(() => {
      this.elQuickPayModal.hide();
    });
  }

  onCreditAllocated(record: ISalesInvoice) {
    this.elAllocateCreditModal.hide();
    this.compReady = false;
    this.loadData().subscribe(() => {
      this.compReady = true;
    });
  }

  offerAllocateCredit() {
    if (!this.creditOffered && this.credits.length && this._routeState.previousUrl === '/sales/invoices/create') {
      swal({
        title: this._translate.instant('confirm.salesInvoice.detail.offerAllocateCredit.label'),
        text: this._translate.instant('confirm.salesInvoice.detail.offerAllocateCredit.description'),
        type: 'question',
        showCancelButton: true,
      }).then(() => {
        this.elAllocateCreditModal.show();
      }).catch();

      this.creditOffered = true;
    }
  }

  isSicepatShippingValid() {
    return _.get(this.relatedData, 'settings.salesSetting.shippingSenderName') &&
      _.get(this.relatedData, 'settings.salesSetting.shippingSenderPhoneNumber') &&
      _.get(this.doc, 'customerMobile');
  }
}

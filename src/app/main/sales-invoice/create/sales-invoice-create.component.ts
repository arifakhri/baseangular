import * as _ from 'lodash';
import { Component, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';

import { SalesInvoiceFormService } from '../form/sales-invoice-form.service';
import { SalesInvoiceRestService } from '../sales-invoice-rest.service';
import { SalesInvoiceService } from '../sales-invoice.service';

import { BaseCreateBComponent } from '../../../shared/base/base.module';
import { MSalesInvoice } from '../sales-invoice.model';

@Component({
  selector: 'app-sales-invoice-create',
  templateUrl: 'sales-invoice-create.component.html'
})
export class SalesInvoiceCreateComponent extends BaseCreateBComponent  {
  @ViewChild('reportModal') elReportModal: ModalDirective;

  constructor(
    private _salesInvoice: SalesInvoiceService,
    private _salesInvoiceForm: SalesInvoiceFormService,
    private _salesInvoiceRest: SalesInvoiceRestService
  ) {
    super();

    this.componentId = 'SalesInvoiceCreate';
    this.headerTitle = 'ui.salesInvoice.create.title';
    this.containerType = 1;
    this.routeURL = '/sales/invoices';
    this.entrySuccessI18n = 'success.salesInvoice.create';


    this.autoprintButtons = [{
      checkboxConfigId: 'autoprint.sinvoice.create',
      checkboxLabel: 'ui.salesInvoice.create.action.apCheckbox.report',
      previewLabel: 'ui.salesInvoice.create.action.previewReport',
      onPreview: () => {
        this.saveAndPrint();
      }
    }];

    this.registerHook('buildForm', event => {
      this._salesInvoiceForm.setFormDefinitions(this.form);
    });

    this.registerHook('save', event => {
      let invoice = new MSalesInvoice;
      invoice = _.assign(invoice, this.form.value);
      this._salesInvoice.normalizeDoc(invoice);

      const isPrinting = _.get(event, 'data.print');

      return this._salesInvoiceRest.create(invoice, { includeLines: isPrinting })
        .switchMap(result => {
          // for now, we always return result, event though auto-allocate failed
          // later backend will handle auto-allocate, so we dont need to call auto-allocate again
          return this._salesInvoiceRest.autoAllocateDownPayment(result.id)
            .switchMap(() => Observable.of(result))
            .catch((e) => Observable.of(result));
        });
    });
   }

  async saveAndPrint() {
    try {
      const submitted = await this.onSubmit({ saveConfirm: true, print: true });
      if (submitted) {
        this._salesInvoiceRest.loadRelatedData().subscribe(related => {
          this.doc['companyInfo'] = related.settings.companyInfo;
          this.elReportModal.show();
        });
      }
    } catch (e) {

    }
  }
}

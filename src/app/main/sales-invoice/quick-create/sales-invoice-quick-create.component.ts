import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

import { CommonService, SystemMessageService } from '../../../core/core.module';
import { SalesInvoiceRestService } from '../sales-invoice-rest.service';
import { SalesInvoiceService } from '../sales-invoice.service';
import { SalesInvoiceFormService } from '../form/sales-invoice-form.service';

@Component({
  selector: 'app-sales-invoice-quick-create',
  templateUrl: 'sales-invoice-quick-create.component.html',
  providers: [SystemMessageService]
})
export class SalesInvoiceQuickCreateComponent implements OnInit {
  @Output() afterSubmit: EventEmitter<ISalesInvoice> = new EventEmitter();
  @Output() close: EventEmitter<boolean> = new EventEmitter();
  @Output() detailForm: EventEmitter<boolean> = new EventEmitter();

  form: FormGroup = new FormGroup({});
  initalFormValues: any = {};
  showLoader: boolean = false;

  constructor(
    @Inject('GlobalSystemMessage') private _globalSystemMessage: SystemMessageService,
    private _router: Router,
    private _salesInvoice: SalesInvoiceService,
    private _salesInvoiceForm: SalesInvoiceFormService,
    private _salesInvoiceRest: SalesInvoiceRestService,
    public _systemMessage: SystemMessageService,
    private _translate: TranslateService
  ) { }

  ngOnInit() {
    this._salesInvoiceForm.setFormDefinitions(this.form);

    this.initalFormValues = this.form.value;
  }

  onSubmit() {
    if (this.formValid()) {
      this.showLoader = true;
      this.save();
    }
  }

  formValid(): boolean {
    if (!this.form.valid) {
      CommonService.markAsDirty(this.form);
      this._systemMessage.log({
        message: this._translate.instant('error.form'),
        type: 'error'
      });
      return false;
    }
    return true;
  }

  save(): void {
    const invoice: ISalesInvoice = this.form.value;
    this._salesInvoice.normalizeDoc(invoice);

    this._salesInvoiceRest.create(invoice)
      .catch(error => {
        this.showLoader = false;
        this._systemMessage.log({
          message: error,
          type: 'error'
        });
        return Observable.throw(error);
      })
      .subscribe(record => {
        this.showLoader = false;
        this._globalSystemMessage.log({
          message: this._translate.instant('success.salesInvoice.quickCreate'),
          type: 'success',
          showAs: 'growl',
          showSnackBar: false,
        });

        this.form.reset(this.initalFormValues);
        this.afterSubmit.emit(record);
      });
  }
}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SalesInvoiceCreateComponent } from './create/sales-invoice-create.component';
import { SalesInvoiceDetailComponent } from './detail/sales-invoice-detail.component';
import { SalesInvoiceListComponent } from './list/sales-invoice-list.component';
import { SalesInvoiceUpdateComponent } from './update/sales-invoice-update.component';

import { AuthorizationService } from '../../core/core.module';

export const routes: Routes = [{
  path: '',
  children: [{
    path: '',
    component: SalesInvoiceListComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'salesInvoice.list',
    },
  }, {
    path: 'create',
    component: SalesInvoiceCreateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'salesInvoice.create',
    },
  }, {
    path: ':id',
    component: SalesInvoiceDetailComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'salesInvoice.detail',
    },
  }, {
    path: ':id/update',
    component: SalesInvoiceUpdateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'salesInvoice.update',
    },
  }]
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class SalesInvoiceRoutingModule { }

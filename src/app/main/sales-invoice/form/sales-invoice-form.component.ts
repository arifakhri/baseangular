import * as _ from 'lodash';
import * as moment from 'moment';
import { ChangeDetectorRef, Component, OnChanges, OnInit, SimpleChanges, ViewChild, } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ModalDirective } from 'ngx-bootstrap';

import { AccountingService, AutocompleteService, CommonService } from '../../../core/core.module';
import { SalesInvoiceCloneService } from '../sales-invoice-clone.service';
import { SalesInvoiceFormService } from '../form/sales-invoice-form.service';
import { SalesInvoiceRestService } from '../sales-invoice-rest.service';
import { SalesInvoiceService } from '../sales-invoice.service';
import { SalesOrderCopyInvoiceService } from '../../sales-order/sales-order-copy-invoice.service';
import { TransactionFormSalesBComponent } from '../../transaction/transaction-form-sales.bcomponent';

@Component({
  selector: 'app-sales-invoice-form',
  templateUrl: 'sales-invoice-form.component.html'
})
export class SalesInvoiceFormComponent extends TransactionFormSalesBComponent implements OnChanges, OnInit {
  @ViewChild('shippingPriceModal') elShippingPriceModal: ModalDirective;

  shippingProviderProducts: IShippingProviderProduct[] = [];
  cloneTransaction: boolean = false;
  copyFromOrder: boolean = false;

  ACProductVariantHandler = CommonService.remoteACItemsHandler({
    remoteParams: this.ACProductVariantParams.bind(this),
    remoteRequest: this._productVariantRest.findAllPicker.bind(this._productVariantRest),
    remoteRequestMap: (response) => response.data,
  });

  constructor(
    public _autocomplete: AutocompleteService,
    public _accounting: AccountingService,
    private _changeDetectorRef: ChangeDetectorRef,
    public _salesInvoice: SalesInvoiceService,
    public _salesInvoiceClone: SalesInvoiceCloneService,
    public _salesInvoiceForm: SalesInvoiceFormService,
    private _salesInvoiceRest: SalesInvoiceRestService,
    private _salesOrderCopyInvoice: SalesOrderCopyInvoiceService,
  ) {
    super();
    super.init();
    this.componentId = 'SalesInvoiceForm';

    this.registerHook('loadRelated', event => {
      return this._salesInvoiceRest.loadRelatedData();
    }, relatedData => {
      this._salesInvoiceForm.patchTransactionSettings(this.form, relatedData.settings);

      if (_.has(this.page.routeQueryParams, 'orderId')) {
        const loading = this.page.createLoading('salesOrderCopyToInvoice');

        this._salesOrderCopyInvoice.copyById(this.page.routeQueryParams.orderId).subscribe(() => {
          this.applyCopyOrder();

          loading.dispose();
        });
      } else {
        this.applyCopyOrder();
      }

      if (this._salesInvoiceClone.docOnHold) {
        const parentDoc = <any>this._salesInvoiceClone.docOnHold;
        parentDoc.deliveryDate = null;
        parentDoc.transactionDate = new Date;
        parentDoc.transactionNumber = null;

        this.cloneTransaction = true;
        this.doc = parentDoc;
        this._salesInvoiceClone.docOnHold = null;

        this.applyDoc();

        this.form.patchValue(this.doc);
      }
    });

    this.registerHook('afterCustomerSelected', event => {
      const customer = event.data;
      let dueDate;
      if (customer.defaultSalesInvoiceDueDays) {
        dueDate = moment().add(customer.defaultSalesInvoiceDueDays, 'days').toDate();
      } else {
        dueDate = new Date;
      }
      this.form.patchValue({
        dueDate,
      });
    });

    this.registerHook('patchForm', event => {
      this.applyDoc();
    });

  }

  ngOnInit() {
    super.ngOnInit();

    this.addLines();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (_.has(changes, 'doc') && this.doc) {
      this.applyDoc();
    }
  }

  getDefaultValue(type: 'master' | 'line'): any {
    return this._salesInvoiceForm.getDefaultValues(type);
  }

  buildFormChildLine() {
    return this._salesInvoiceForm.buildFormChildLine();
  }

  lineConditionalValidation(formGroup: FormGroup) {
    this._salesInvoiceForm.lineConditionalValidation(formGroup);
  }

  onShippingProviderSelected(shippingProvider: IShippingProvider) {
    this.form.get('shippingProviderId').setValue(shippingProvider.id);
    this.shippingProviderProducts = shippingProvider.products;
    this.form.get('shippingProviderProduct').reset(null);
    this.form.get('shippingProviderProductId').reset(null);
  }

  onShippingPriceSelected(service) {
    const selectedShippingPrice = {
      shipppingProviderId: this.form.get('shippingProviderId').value,
      id: service.serviceName,
      name: service.serviceDescription
    };
    this.form.get('shippingProviderProductId').patchValue(service.serviceName);
    this.form.get('shippingProviderProduct').patchValue(selectedShippingPrice);
    this.form.get('shippingCharge').patchValue(Number(service.price));
    this.elShippingPriceModal.hide();
    this.syncTotal();
  }

  applyCopyOrder() {
    if (this._salesOrderCopyInvoice.docOnHold) {
      this.copyFromOrder = true;

      this.doc = this._salesOrderCopyInvoice.apply();

      this.applyDoc();

      this.form.patchValue(this.doc);
    }
  }
}

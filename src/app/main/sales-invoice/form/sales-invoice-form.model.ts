import { MBase } from '../../../core/common/base.model';

export class MSalesInvoiceForm extends MBase {
  adjustmentAmount: number = 0;
  billingAddress: string;
  branchId: number = 0;
  customerEmail: string;
  customer: string;
  customerId: string;
  customerMobile: string;
  customerNote: string;
  customCustomerName: string;
  customerRefNumber: string;
  description: string;
  discountAmount: number = 0;
  discountPercent: number = null;
  dueDate: string;
  lines: MSalesInvoiceFormLine[] = [];
  note: string;
  shippingAddress: string;
  shippingCharge: number = 0;
  shippingDate: string;
  shippingMethod: string;
  shippingMethodId: string;
  shippingTrackingNumber: string;
  subtotal: number = 0;
  taxAmount: number = 0;
  taxed: boolean = true;
  total: number = 0;
  totalPaid: number = 0;
  transactionDate: string;
  transactionNumber: string;

  parentTransactionType: string;
}

export class MSalesInvoiceFormLine extends MBase {
  amount: number = 0;
  description: string;
  discountAmount: number = 0;
  discountPercent: number = null;
  incomeAccountId: string;
  note: string;
  parentTransactionId: string;
  parentTransactionLineId: string;
  product: string;
  productId: string;
  productVariant: string;
  productVariantId: string;
  qty: number = 0;
  tax: string;
  taxId: string;
  taxRate: number = 0;
  totalDiscount: number = 0;
  unitPrice: number = 0;
  uomConversion: number = 0;
  uom: string;
  uomId: string;
  warehouseId: string;
}

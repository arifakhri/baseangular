import * as _ from 'lodash';
import { FormArray, FormGroup } from '@angular/forms';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable()
export class SalesInvoiceCloneService {
  private currentDoc: ISalesInvoice;

  constructor(
    private _router: Router
  ) { }

  set docOnHold(invoice) {
    this.currentDoc = invoice;
  }

  get docOnHold() {
    if (!this.currentDoc) {
      return null;
    }

    const targetDoc: any = _.cloneDeep(this.currentDoc);
    targetDoc.lines.forEach(line => {
      line.incomeAccountId = _.get(line, 'product.incomeAccountId') || null;
    });

    return targetDoc;
  }

  copy(invoice) {
    this.docOnHold = invoice;

    this._router.navigateByUrl('/sales/invoices/create');
  }

  reset(form: FormGroup) {
    const linesFormGroup = <FormArray>form.get('lines');
    _.forEach(linesFormGroup.controls, (lineFormGroup, lineIdx) => {
      lineFormGroup.get('incomeAccountId').reset(null);
    });
  }
}

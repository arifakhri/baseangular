import * as _ from 'lodash';
import * as SnackBar from 'node-snackbar';
import swal from 'sweetalert2';
import { ActivatedRoute } from '@angular/router';
import { ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { DataTable } from 'primeng/components/datatable/datatable';
import { ModalDirective, PopoverDirective } from 'ngx-bootstrap';
import { Observable, Subscription } from 'rxjs';
import 'app/rxjs-imports.ts';
import { TranslateService } from '@ngx-translate/core';

import {
  AccountingService,
  ExportDataTableService,
  GridTableDataSource,
  GridTableService,
  GridTableToggleService,
  SystemMessageService,
} from '../../../core/core.module';
import { AdminLayoutService } from '../../../layouts/layouts.module';
import { SpinnerDirective } from '../../../shared/spinner/spinner.directive';
import { SalesInvoiceCloneService } from '../../sales-invoice/sales-invoice-clone.service';
import { SalesInvoiceCopyCreditNoteService } from '../sales-invoice-copy-credit-note.service';
import { SalesInvoiceMoreFilterService } from '../more-filter/sales-invoice-more-filter.service';
import { SalesInvoiceRestService } from '../sales-invoice-rest.service';
import { RetryDialogComponent } from '../../../shared/retry-dialog/retry-dialog.component';

@Component({
  selector: 'app-sales-invoice-list',
  templateUrl: 'sales-invoice-list.component.html',
  providers: [SystemMessageService]
})
export class SalesInvoiceListComponent implements OnDestroy, OnInit {
  @ViewChild('gridTable') public gridTable: DataTable;
  @ViewChild('popMoreFilter') elPopMoreFilter: PopoverDirective;
  @ViewChild('pageLoading') elPageLoading: SpinnerDirective;
  @ViewChild('quickPayModal') elQuickPayModal: PopoverDirective;
  @ViewChild('reportModal') elReportModal: ModalDirective;
  @ViewChild('retryDialog') elRetryDialog: RetryDialogComponent;

  compReady: boolean;
  doc: ISalesInvoice;
  gridDataSource: GridTableDataSource<ISalesInvoice> = new GridTableDataSource<ISalesInvoice>();
  modalDoc: any;
  selectedRecords: ISalesInvoice[] = [];

  combinedFilterValues: string = '';
  combinedFilterFields = {
    transactionNumber: true,
    'customer.displayName': true,
  };

  qParamsFilters: any = {
    keyword: null
  };
  moreFilterValues: IGridTableFilterParsed;

  tableColumns: IGridTableColumn[] = [{
    i18nLabel: 'ui.salesInvoice.list.column.transactionDate',
    field: 'transactionDate',
    dateFormat: 'DD/MM/YYYY',
    sort: true,
  }, {
    i18nLabel: 'ui.salesInvoice.list.column.transactionNumber',
    field: 'transactionNumber',
    link: (row: ISalesInvoice) => {
      return `/sales/invoices/${row.id}`;
    },
    sort: true,
  }, {
    i18nLabel: 'ui.salesInvoice.list.column.orderNo',
    field: 'parentTransactions[0].transactionNumber',
    sortField: 'transactionNumber',
    link: (row: ISalesInvoice) => {
      return (row.parentTransactions[0] ? `/sales/orders/${row.parentTransactions[0].transactionId}` : '');
    },
    sort: true,
  }, {
    i18nLabel: 'ui.salesInvoice.list.column.customerName',
    field: 'customer.displayName',
    sort: true,
    formatter: (value, row: ISalesInvoice) => {
      return row.customCustomerName ? value + ' (' + row.customCustomerName + ')' : value;
    },
  }, {
    i18nLabel: 'ui.salesInvoice.list.column.dueDate',
    field: 'dueDate',
    dateFormat: 'DD/MM/YYYY',
    sort: true,
  }, {
    i18nLabel: 'ui.salesInvoice.list.column.total',
    field: 'total',
    sort: true,
    formatter: (value, row: ISalesInvoice) => {
      return this._accounting.ac.formatMoney(value, '');
    },
    columnClasses: 'right-align',
  }, {
    i18nLabel: 'ui.salesInvoice.list.column.balanceDue',
    field: 'balanceDue',
    sort: true,
    formatter: (value, row: ISalesInvoice) => {
      return this._accounting.ac.formatMoney(value, '');
    },
    columnClasses: 'right-align',
  }, {
    i18nLabel: 'ui.salesInvoice.list.column.status',
    field: 'status',
    sort: true,
    formatter: (value, row: ISalesInvoice) => {
      return _.startCase(value);
    },
  }];
  tableColumnsShow: boolean = false;
  tableColumnsToggle = this._gridTableToggle.mapToggleOptionsFromColumns(this.tableColumns);

  retryDialogShownState: boolean = false;
  retryDialogOnRetry: Function;

  private loadDataTimeoutId: number;
  private gridFirstLoad: boolean = true;
  private subscriptions: { [key: string]: Subscription[] } = {
    load: []
  };

  constructor(
    public _accounting: AccountingService,
    private _activatedRoute: ActivatedRoute,
    private _adminLayout: AdminLayoutService,
    private _changeDetectorRef: ChangeDetectorRef,
    private _gridTable: GridTableService,
    private _dataTableExport: ExportDataTableService,
    private _gridTableToggle: GridTableToggleService,
    private _salesInvoiceClone: SalesInvoiceCloneService,
    public _salesInvoiceCopyCreditNote: SalesInvoiceCopyCreditNoteService,
    private _salesInvoiceMoreFilter: SalesInvoiceMoreFilterService,
    private _salesInvoiceRest: SalesInvoiceRestService,
    public _systemMessage: SystemMessageService,
    private _translate: TranslateService,
  ) {
    this._adminLayout.containerType = 1;
  }

  ngOnInit() {
    this.loadData();
  }

  ngOnDestroy() {
    this._salesInvoiceMoreFilter.lastMoreFilterValues = {};
  }

  onMoreFilter(filters: IGridTableFilterParsed) {
    this.elPopMoreFilter.hide();

    this.moreFilterValues = filters;
    this.loadData();
  }

  populateFilters() {
    let newFilters: any = _.pick(this.qParamsFilters, ['keyword']);

    if (this.moreFilterValues) {
      newFilters = Object.assign({}, newFilters, this.moreFilterValues.qParams);
    }

    this.qParamsFilters = newFilters;
  }

  gridLoadDataWrapper() {
    // Grid always load at the first time. we dont want that. we already call on ngOnInit
    if (this.gridFirstLoad) {
      this.gridFirstLoad = false;
      return;
    }

    if (this.loadDataTimeoutId) {
      window.clearTimeout(this.loadDataTimeoutId);
      this.loadDataTimeoutId = null;
    }

    this.loadDataTimeoutId = window.setTimeout(() => {
      this.loadData();
    }, 300);
  }

  showVoidDialog(invoiceId: string) {
    swal({
      title: this._translate.instant('confirm.salesInvoice.void.label'),
      text: this._translate.instant('confirm.salesInvoice.void.description'),
      type: 'question',
      showCancelButton: true,
    })
      .then(() => {
        this.voidInvoice(invoiceId);
      })
      .catch(() => { });
  }

  voidInvoice(invoiceId: string) {
    this._salesInvoiceRest.void(invoiceId)
    .catch((error) => {
      this._systemMessage.log({
        message: error.response.data[0].errorMessage,
        type: 'error'
      });
      return Observable.throw(error);
    })
    .subscribe(response => {
      this.gridLoadDataWrapper();
      SnackBar.show({
        text: this._translate.instant('success.salesInvoice.void'),
        pos: 'bottom-right'
      });
    });
  }

  printReport(invoiceId: string) {
    this._salesInvoiceRest.load(invoiceId).subscribe(response => {
      this.doc = response;
      this._salesInvoiceRest.loadRelatedData().subscribe(related => {
        this.doc['companyInfo'] = related.settings.companyInfo;
        this.elReportModal.show();
      });
    });
  }

  get exportRecords(): Observable<ISalesInvoice[]> {
    const qOption = {
      ...this._gridTable.generateApiQueryOptionFromGridInfo(
        this.gridDataSource.pager,
        false,
        this.gridTable,
        this._gridTable.generateCombinedApiFilters(this.combinedFilterValues, this.combinedFilterFields)
      ),
      skip: 0,
      take: this._dataTableExport.exportLimit,
    };

    this.populateFilters();

    this.qParamsFilters.includeParentTransactions = true;

    return this._salesInvoiceRest.findAll(qOption, this.qParamsFilters).map(response => response.data);
  }

  exportPDF() {
    this.exportRecords.subscribe(records => {
      this._dataTableExport.export({
        records,
        mapOptions: this._dataTableExport.mapFromTableColumns(this.tableColumns),
        fileName: 'sales-invoice',
        extension: 'pdf',
        templateData: {
          title: this._translate.instant('ui.transaction.list.title'),
        },
      });
    });
  }

  exportExcel() {
    this.exportRecords.subscribe(records => {
      this._dataTableExport.export({
        records: records,
        mapOptions: this._dataTableExport.mapFromTableColumns(this.tableColumns),
        fileName: 'sales-invoice',
        extension: 'xls',
      });
    });
  }

  loadData() {
    this.elPageLoading.forceShow();

    const qOption = this._gridTable.generateApiQueryOptionFromGridInfo(
      this.gridDataSource.pager,
      true,
      this.gridTable,
      this._gridTable.generateCombinedApiFilters(this.combinedFilterValues, this.combinedFilterFields)
    );

    this.populateFilters();

    this.qParamsFilters.includeParentTransactions = true;

    this.subscriptions.load.forEach(sub => sub.unsubscribe());
    this.subscriptions.load.push(
      this.elRetryDialog.createRetryEntry(
        this._salesInvoiceRest.findAll(qOption, this.qParamsFilters)
          .finally(() => {
            this.elPageLoading.forceHide();
          })
      ).subscribe(response => {
        this.gridDataSource.updateFromApiPaginationResult(response);
        this.compReady = true;
      })
    );
  }

  cloneTransaction(doc) {
    this.elPageLoading.forceShow();

    this._salesInvoiceRest.load(doc.id).subscribe(newDoc => {
      this.elPageLoading.forceHide();

      this._salesInvoiceClone.copy(newDoc);
    });
  }

  copyInvoicetoCreditNote(doc) {
    this.elPageLoading.forceShow();

    this._salesInvoiceRest.load(doc.id).subscribe(newDoc => {
      this.elPageLoading.forceHide();

      this._salesInvoiceCopyCreditNote.copy(newDoc);
    });
  }

  showQuickPayModal(doc) {
    this.elPageLoading.forceShow();

    this._salesInvoiceRest.load(doc.id).subscribe(newDoc => {
      this.modalDoc = newDoc;

      this._changeDetectorRef.detectChanges();

      this.elQuickPayModal.show();
      this.elPageLoading.forceHide();
    });
  }

  didSalesPaymentQuickCreate() {
    this.loadData();
    this.elQuickPayModal.hide();
  }

  public onGridChange() {
    this.gridLoadDataWrapper();
  }

  public onGridPageChange() {
    this.gridLoadDataWrapper();
  }
}

import * as _ from 'lodash';
import * as moment from 'moment';
import { Component, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';

import { SalesInvoiceFormService } from '../form/sales-invoice-form.service';
import { SalesInvoiceRestService } from '../sales-invoice-rest.service';
import { SalesInvoiceService } from '../sales-invoice.service';

import { BaseUpdateBComponent } from '../../../shared/base/base.module';

@Component({
  selector: 'app-sales-invoice-update',
  templateUrl: 'sales-invoice-update.component.html',
})
export class SalesInvoiceUpdateComponent extends BaseUpdateBComponent {
  @ViewChild('reportModal') elReportModal: ModalDirective;

  constructor(
    private _salesInvoice: SalesInvoiceService,
    private _salesInvoiceForm: SalesInvoiceFormService,
    private _salesInvoiceRest: SalesInvoiceRestService,
  ) {
    super();

    this.componentId = 'SalesInvoiceUpdate';
    this.headerTitle = 'ui.salesInvoice.update.title';
    this.containerType = 1;
    this.routeURL = '/sales/invoices';
    this.entrySuccessI18n = 'success.salesInvoice.update';


    this.registerHook('buildForm', event => {
      this._salesInvoiceForm.setFormDefinitions(this.form);
    });

    this.registerHook('load', event => {
      return this._salesInvoiceRest.load(this.page.routeParams.id).switchMap(doc => {
        if (doc.transactionDate) {
          doc.transactionDate = moment(doc.transactionDate).toDate();
        }
        if (doc.dueDate) {
          doc.dueDate = moment(doc.dueDate).toDate();
        }
        if (doc.shippingDate) {
          doc.shippingDate = moment(doc.shippingDate).toDate();
        }
        return Observable.of(doc);
      });
    });

    this.registerHook('save', event => {
      const invoice: ISalesInvoice = Object.assign({}, this.doc, this.form.value);
      this._salesInvoice.normalizeDoc(invoice);

      const isPrinting = _.get(event, 'data.print');

      return this._salesInvoiceRest.update(this.page.routeParams.id, invoice, { includeLines: isPrinting });
    });
  }

  async saveAndPrint() {
    try {
      const submitted = await this.onSubmit({ saveConfirm: true, print: true });
      if (submitted) {
        this._salesInvoiceRest.loadRelatedData().subscribe(related => {
          this.doc['companyInfo'] = related.settings.companyInfo;
          this.elReportModal.show();
        });
      }
    } catch (e) {

    }
  }
}

import { NgModule } from '@angular/core';

import { SalesInvoiceModule } from './sales-invoice.module';
import { SalesInvoiceRoutingModule } from './sales-invoice-routing.module';

@NgModule({
  imports: [
    SalesInvoiceModule,
    SalesInvoiceRoutingModule
  ]
})
export class SalesInvoiceLazyModule { }

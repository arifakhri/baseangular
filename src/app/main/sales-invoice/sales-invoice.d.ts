declare interface ISalesInvoice {
  dueDate: string | Date;
  totalPaid: number;
  balanceDue: number;
  lines: [
    {
      id: string;
      transactionId: string;
      productId: string;
      productVariantId: string;
      description: string;
      qty: number;
      unitPrice: number;
      discountPercent: number;
      discountAmount: number;
      totalBeforeDiscount: number;
      totalDiscount: number;
      total: number;
      uomId: string;
      uomConversion: number;
      baseQty: number;
      taxId: string;
      taxRate: number;
      taxAmount: number;
      note: string;
      sortOrder: number;
      rowVersion: string;
      product: {
        id: string;
        productType: string;
        code: string;
        name: string;
        categoryId: string;
        sellable: boolean;
        salesDescription: string;
        incomeAccountId: string;
        salesTaxId: string;
        purchasable: boolean;
        purchaseDescription: string;
        expenseAccountId: string;
        purchaseTaxId: string;
        inventoryAccountId: string;
        uomId: string;
        variantCount: number;
        inactive: boolean;
        category: {
          id: string;
          name: string;
        };
        salesTax: {
          id: string;
          code: string;
          name: string;
          rate: number;
        };
        purchaseTax: {
          id: string;
          code: string;
          name: string;
          rate: number;
        };
        uom: {
          id: string;
          name: string;
          systemDefault: boolean;
        };
        masterVariant: {
          id: string;
          barcode: string;
          name: string;
          variantName: string;
          unitPrice: number;
          unitCost: number;
        }
      };
      productVariant: {
        id: string;
        productId: string;
        sku: string;
        name: string;
        variantName: string;
        attribute1Value: string;
        attribute2Value: string;
        attribute3Value: string;
        unitPrice: number;
        unitCost: number;
        isMaster: boolean;
        inactive: boolean;
        rowVersion: string;
      };
      uom: {
        id: string;
        name: string;
        systemDefault: boolean;
      };
      tax: {
        id: string;
        code: string;
        name: string;
        rate: number;
      }
    }
  ];
  id: string;
  transactionType: string;
  transactionNumber: string;
  transactionDate: string | Date;
  customerId: string;
  customCustomerName: string;
  customerRefNumber: string;
  customerEmail: string;
  customerMobile: string;
  description: string;
  billingAddress: string;
  shippingAddress: string;
  shippingDate: string | Date;
  siCepatMemberId: string;
  lineTotalBeforeDiscount: number;
  lineTotalDiscount: number;
  subtotal: number;
  discountPercent: number;
  discountAmount: number;
  taxAmount: number;
  shippingCharge: number;
  adjustmentAmount: number;
  total: number;
  taxed: boolean;
  note: string;
  parentTransactions: {
    transactionId: string;
    transactionNumber: string;
    transactionTypeStr: string;
  }
  customerNote: string;
  externalChannelId: string;
  externalDocumentId: string;
  status: string;
  state: string;
  isOpen: boolean;
  rowVersion: string;
  customer: {
    salesTaxable: boolean;
    defaultSalesTaxId: string;
    defaultSalesPriceLevelId: string;
    defaultSalesInvoiceDueDays: number;
    purchaseTaxable: boolean;
    defaultPurchaseTaxId: string;
    defaultPurchaseInvoiceDueDays: number;
    isCustomer: boolean;
    isVendor: boolean;
    defaultSalesTax: {
      id: string;
      code: string;
      name: string;
      rate: number;
    };
    defaultSalesPriceLevel: {
      id: string;
      name: string;
      isMaster: boolean;
    };
    defaultPurchaseTax: {
      id: string;
      code: string;
      name: string;
      rate: number;
    };
    id: string;
    contactType: string;
    code: string;
    displayName: string;
    company: string;
    fullName: string;
    phone: string;
    mobile: string;
    fax: string;
    email: string;
    website: string;
    inactive: boolean;
    postalAddress: {
      attention: string;
      street1: string;
      street2: string;
      city: string;
      stateProvince: string;
      countryId: string;
      postalCode: string;
      longitude: number;
      latitude: number;
      country: {
        code: string;
        name: string;
      }
    }
  }
}

declare interface ISalesInvoiceRelatedData {
  taxes: [
    {
      id: number;
      code: string;
      name: string;
      rate: number;
    }
  ];
  shippingMethods: [
    {
      id: number;
      name: string;
      courierId: string;
    }
  ];
  warehouses: [
    {
      id: number;
      name: string;
      isMaster: boolean;
    }
  ];
  settings: {
    companyInfo: {
      companyName: string;
      phone: string;
      fax: string;
      email: string;
      website: string;
      postalAddressSameAsMain: boolean;
      shippingAddressSameAsMain: boolean;
      legalAddressSameAsMain: boolean;
      logoId: number;
      mainAddress: {
        attention: string;
        street1: string;
        street2: string;
        city: string;
        stateProvince: string;
        countryId: string;
        postalCode: string;
        longitude: number;
        latitude: number;
        country: {
          id: string;
          name: string;
          phoneCountryCode: string;
        };
        fullAddress: string;
      };
      postalAddress: {
        attention: string;
        street1: string;
        street2: string;
        city: string;
        stateProvince: string;
        countryId: string;
        postalCode: string;
        longitude: number;
        latitude: number;
        country: {
          id: string;
          name: string;
          phoneCountryCode: string;
        };
        fullAddress: string;
      };
      shippingAddress: {
        attention: string;
        street1: string;
        street2: string;
        city: string;
        stateProvince: string;
        countryId: string;
        postalCode: string;
        longitude: number;
        latitude: number;
        country: {
          id: string;
          name: string;
          phoneCountryCode: string;
        };
        fullAddress: string;
      };
      legalAddress: {
        attention: string;
        street1: string;
        street2: string;
        city: string;
        stateProvince: string;
        countryId: string;
        postalCode: string;
        longitude: number;
        latitude: number;
        country: {
          id: string;
          name: string;
          phoneCountryCode: string;
        };
        fullAddress: string;
      };
      logo: {
        id: number;
        fileType: string;
        fileName: string;
        fileExt: string;
        fileSize: number;
        description: string;
        uploadByUserId: number;
        fileUrl: string;
        thumbnailUrl: string;
        smallUrl: string;
        mediumUrl: string;
        largeUrl: string;
        tabletUrl: string;
      }
    };
    companySetting: {
      masterBranchId: number;
      companyName: string;
      industryId: string;
      phone: string;
      fax: string;
      email: string;
      website: string;
      mainAddressId: number;
      postalAddressSameAsMain: boolean;
      postalAddressId: number;
      shippingAddressSameAsMain: boolean;
      shippingAddressId: number;
      legalAddressSameAsMain: boolean;
      legalAddressId: number;
      logoId: number;
    };
    accountingSetting: {
      fiscalYearMonthStart: number;
      accountingStartDate: string;
      lockDate: string;
    };
    accountMapping: {
      cashAccountId: number;
      undepositedFundsAccountId: number;
      arAccountId: number;
      unbilledARAccountId: number;
      apAccountId: number;
      unbilledAPAccountId: number;
      salesDepositAccountId: number;
      purchaseDepositAccountId: number;
      salesTaxAccountId: number;
      purchaseTaxAccountId: number;
      fixedAssetAccountId: number;
      accumulatedDepreciationAccountId: number;
      openingBalanceAccountId: number;
      retainEarningAccountId: number;
      salesIncomeAccountId: number;
      purchaseExpenseAccountId: number;
      cogsAccountId: number;
      inventoryAccountId: number;
      inventoryCostVarianceAccountId: number;
      salesDiscountAccountId: number;
      salesReturnAccountId: number;
      shippingIncomeAccountId: number;
      purchaseDiscountAccountId: number;
      shippingChargeAccountId: number;
      inventoryAdjustmentAccountId: number;
      depreciationExpenseAccountId: number;
      roundingAccountId: number;
    };
    salesSetting: {
      defaultSalesTaxId: number;
      defaultCustomerMessage: string;
      shipping: boolean;
      shippingSenderName: string;
      shippingSenderPhoneNumber: string;
      shippingSenderEmail: string;
      siCepatMemberId: number;
      defaultSalesTax: {
        id: number;
        code: string;
        name: string;
        rate: number;
      }
    };
    purchaseSetting: {
      defaultPurchaseTaxId: number;
      defaultPurchaseTax: {
        id: number;
        code: string;
        name: string;
        rate: number;
      }
    };
    productSetting: {
      masterPriceLevelId: number;
      masterWarehouseId: number;
      allowInventoryLevelBelowZero: boolean;
      defaultUomId: number;
      masterPriceLevel: {
        id: number;
        name: string;
        isMaster: boolean;
      };
      masterWarehouse: {
        id: number;
        name: string;
        isMaster: boolean;
      };
      defaultUom: {
        id: number;
        name: string;
        systemDefault: boolean;
      }
    }
  }
}
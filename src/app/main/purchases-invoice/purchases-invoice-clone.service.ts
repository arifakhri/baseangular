import * as _ from 'lodash';
import { FormArray, FormGroup } from '@angular/forms';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable()
export class PurchasesInvoiceCloneService {
  private currentDoc: IPurchasesInvoice;

  constructor(
    private _router: Router
  ) { }

  set docOnHold(invoice) {
    this.currentDoc = invoice;
  }

  get docOnHold() {
    if (!this.currentDoc) {
      return null;
    }

    const targetDoc: any = _.cloneDeep(this.currentDoc);
    targetDoc.lines.forEach(line => {
      line.expenseAccountId = _.get(line, 'product.expenseAccountId') || null;
    });

    return targetDoc;
  }

  copy(invoice) {
    this.docOnHold = invoice;

    this._router.navigateByUrl('/purchases/invoices/create');
  }

  reset(form: FormGroup) {
    const linesFormGroup = <FormArray>form.get('lines');
    _.forEach(linesFormGroup.controls, (lineFormGroup, lineIdx) => {
      lineFormGroup.get('incomeAccountId').reset(null);
    });
  }
}

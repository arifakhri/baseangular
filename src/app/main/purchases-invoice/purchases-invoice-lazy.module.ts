import { NgModule } from '@angular/core';

import { PurchasesInvoiceModule } from './purchases-invoice.module';
import { PurchasesInvoiceRoutingModule } from './purchases-invoice-routing.module';

@NgModule({
  imports: [
    PurchasesInvoiceModule,
    PurchasesInvoiceRoutingModule
  ]
})
export class PurchasesInvoiceLazyModule { }

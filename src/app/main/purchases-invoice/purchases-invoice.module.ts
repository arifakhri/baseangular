import { AutoCompleteModule } from 'primeng/components/autocomplete/autocomplete';
import { BsDropdownModule, ModalModule, PaginationModule, PopoverModule } from 'ngx-bootstrap';
import { CalendarModule } from 'primeng/components/calendar/calendar';
import { CheckboxModule } from 'primeng/components/checkbox/checkbox';
import { CommonModule } from '@angular/common';
import { DataTableModule } from 'primeng/components/datatable/datatable';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { ListboxModule } from 'primeng/components/listbox/listbox';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { PurchasesInvoiceCreateComponent } from './create/purchases-invoice-create.component';
import { PurchasesInvoiceDetailComponent } from './detail/purchases-invoice-detail.component';
import { PurchasesInvoiceFormComponent } from './form/purchases-invoice-form.component';
import { PurchasesInvoiceListComponent } from './list/purchases-invoice-list.component';
import { PurchasesInvoiceMoreFilterComponent } from './more-filter/purchases-invoice-more-filter.component';
import { PurchasesInvoiceUpdateComponent } from './update/purchases-invoice-update.component';

import { PurchasesInvoiceCloneService } from './purchases-invoice-clone.service';
import { PurchasesInvoiceCopyDebitNoteService } from './purchases-invoice-copy-debit-note.service';
import { PurchasesInvoiceFormService } from './form/purchases-invoice-form.service';
import { PurchasesInvoiceMoreFilterService } from './more-filter/purchases-invoice-more-filter.service';
import { PurchasesInvoiceRestService } from './purchases-invoice-rest.service';
import { PurchasesInvoiceService } from './purchases-invoice.service';

import { BusinessPartnerModule } from '../business-partner/business-partner.module';
import { CoreModule } from '../../core/core.module';
import { ProductVariantModule } from '../product-variant/product-variant.module';
import { PurchasesCreditListModule } from '../purchases/credit-list/purchases-credit-list.module';
import { PurchasesPaymentCreditListModule } from '../purchases/payment-credit-list/purchases-payment-credit-list.module';
import { PurchasesModule } from '../purchases/purchases.module';
import { PurchasesPaymentModule } from '../purchases-payment/purchases-payment.module';
import { SharedModule } from '../../shared/shared.module';
import { TransactionModule } from '../transaction/transaction.module';

export const PROVIDERS = [
  PurchasesInvoiceCloneService,
  PurchasesInvoiceCopyDebitNoteService,
  PurchasesInvoiceFormService,
  PurchasesInvoiceMoreFilterService,
  PurchasesInvoiceRestService,
  PurchasesInvoiceService,
];

@NgModule({
  imports: [
    AutoCompleteModule,
    BsDropdownModule,
    BusinessPartnerModule,
    CalendarModule,
    CheckboxModule,
    CommonModule,
    CoreModule,
    DataTableModule,
    FlexLayoutModule,
    FormsModule,
    HttpModule,
    ListboxModule,
    ModalModule,
    PaginationModule,
    PopoverModule,
    ProductVariantModule,
    PurchasesCreditListModule,
    PurchasesPaymentCreditListModule,
    PurchasesModule,
    PurchasesPaymentModule,
    ReactiveFormsModule,
    RouterModule,
    SharedModule,
    TransactionModule,
    TranslateModule,
  ],
  declarations: [
    PurchasesInvoiceCreateComponent,
    PurchasesInvoiceDetailComponent,
    PurchasesInvoiceFormComponent,
    PurchasesInvoiceListComponent,
    PurchasesInvoiceMoreFilterComponent,
    PurchasesInvoiceUpdateComponent,
  ],
})
export class PurchasesInvoiceModule { }

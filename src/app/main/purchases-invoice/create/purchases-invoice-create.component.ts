import { Component, ViewChild } from '@angular/core';
import * as _ from 'lodash';
import { ModalDirective } from 'ngx-bootstrap';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';

import { BaseCreateBComponent } from '../../../shared/base/base.module';
import { PurchasesInvoiceFormService } from '../form/purchases-invoice-form.service';
import { PurchasesInvoiceRestService } from '../purchases-invoice-rest.service';
import { MPurchasesInvoice } from '../purchases-invoice.model';
import { PurchasesInvoiceService } from '../purchases-invoice.service';

@Component({
  selector: 'app-purchases-invoice-create',
  templateUrl: 'purchases-invoice-create.component.html'
})
export class PurchasesInvoiceCreateComponent extends BaseCreateBComponent {
  @ViewChild('reportModal') elReportModal: ModalDirective;

  constructor(
    private _purchasesInvoice: PurchasesInvoiceService,
    private _purchasesInvoiceForm: PurchasesInvoiceFormService,
    private _purchasesInvoiceRest: PurchasesInvoiceRestService
  ) {
    super();

    this.componentId = 'PurchasesInvoiceCreate';
    this.headerTitle = 'ui.purchasesInvoice.create.title';
    this.containerType = 1;
    this.routeURL = '/purchases/invoices';
    this.entrySuccessI18n = 'success.purchasesInvoice.create';

    this.autoprintButtons = [{
      checkboxConfigId: 'autoprint.sinvoice.create',
      checkboxLabel: 'ui.purchasesInvoice.create.action.apCheckbox.report',
      previewLabel: 'ui.purchasesInvoice.create.action.previewReport',
      onPreview: () => {
        this.saveAndPrint();
      }
    }];

    this.registerHook('buildForm', event => {
      this._purchasesInvoiceForm.setFormDefinitions(this.form);
    });

    this.registerHook('save', event => {
      let invoice = new MPurchasesInvoice;
      invoice = _.assign(invoice, this.form.value);
      this._purchasesInvoice.normalizeDoc(invoice);

      const isPrinting = _.get(event, 'data.print');

      return this._purchasesInvoiceRest.create(invoice, { includeLines: isPrinting })
        .switchMap(result => {
          // for now, we always return result, event though auto-allocate failed
          // later backend will handle auto-allocate, so we dont need to call auto-allocate again
          return this._purchasesInvoiceRest.autoAllocateDownPayment(result.id)
            .switchMap(() => Observable.of(result))
            .catch((e) => Observable.of(result));
        });
    });
  }

  async saveAndPrint() {
    try {
      const submitted = await this.onSubmit({ saveConfirm: true, print: true });
      if (submitted) {
        this._purchasesInvoiceRest.loadRelatedData().subscribe(related => {
          this.doc['companyInfo'] = related.settings.companyInfo;
          this.elReportModal.show();
        });
      }
    } catch (e) {

    }
  }
}

import * as _ from 'lodash';
import * as SnackBar from 'node-snackbar';
import swal from 'sweetalert2';
import { ActivatedRoute } from '@angular/router';
import { ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { DataTable } from 'primeng/components/datatable/datatable';
import { ModalDirective, PopoverDirective } from 'ngx-bootstrap';
import { Observable, Subscription } from 'rxjs';
import 'app/rxjs-imports.ts';
import { TranslateService } from '@ngx-translate/core';

import {
  AccountingService,
  ExportDataTableService,
  GridTableDataSource,
  GridTableService,
  GridTableToggleService,
  SystemMessageService,
} from '../../../core/core.module';
import { AdminLayoutService } from '../../../layouts/layouts.module';
import { SpinnerDirective } from '../../../shared/spinner/spinner.directive';
import { PurchasesInvoiceCloneService } from '../../purchases-invoice/purchases-invoice-clone.service';
import { PurchasesInvoiceCopyDebitNoteService } from '../purchases-invoice-copy-debit-note.service';
import { PurchasesInvoiceMoreFilterService } from '../more-filter/purchases-invoice-more-filter.service';
import { PurchasesInvoiceRestService } from '../purchases-invoice-rest.service';
import { RetryDialogComponent } from '../../../shared/retry-dialog/retry-dialog.component';

@Component({
  selector: 'app-purchases-invoice-list',
  templateUrl: 'purchases-invoice-list.component.html',
  providers: [SystemMessageService]
})
export class PurchasesInvoiceListComponent implements OnDestroy, OnInit {
  @ViewChild('gridTable') public gridTable: DataTable;
  @ViewChild('popMoreFilter') elPopMoreFilter: PopoverDirective;
  @ViewChild('pageLoading') elPageLoading: SpinnerDirective;
  @ViewChild('quickPayModal') elQuickPayModal: PopoverDirective;
  @ViewChild('reportModal') elReportModal: ModalDirective;
  @ViewChild('retryDialog') elRetryDialog: RetryDialogComponent;

  compReady: boolean;
  doc: IPurchasesInvoice;
  gridDataSource: GridTableDataSource<IPurchasesInvoice> = new GridTableDataSource<IPurchasesInvoice>();
  modalDoc: any;
  selectedRecords: IPurchasesInvoice[] = [];

  combinedFilterValues: string = '';
  combinedFilterFields = {
    'vendor.displayName': true,
    transactionNumber: true,
  };

  qParamsFilters: any = {
    keyword: null
  };
  moreFilterValues: IGridTableFilterParsed;

  tableColumns: IGridTableColumn[] = [{
    i18nLabel: 'ui.purchasesInvoice.list.column.transactionDate',
    field: 'transactionDate',
    dateFormat: 'DD/MM/YYYY',
    sort: true,
  }, {
    i18nLabel: 'ui.purchasesInvoice.list.column.transactionNumber',
    field: 'transactionNumber',
    link: (row: IPurchasesInvoice) => {
      return `/purchases/invoices/${row.id}`;
    },
    sort: true,
  }, {
    i18nLabel: 'ui.purchasesInvoice.list.column.orderNo',
    field: 'parentTransactions[0].transactionNumber',
    link: (row: IPurchasesInvoice) => {
      return (row.parentTransactions[0] ? `/purchases/orders/${row.parentTransactions[0].transactionId}` : '');
    },
    sort: true,
  }, {
    i18nLabel: 'ui.purchasesInvoice.list.column.vendorName',
    field: 'vendor.displayName',
    sort: true,
  }, {
    i18nLabel: 'ui.purchasesInvoice.list.column.dueDate',
    field: 'dueDate',
    dateFormat: 'DD/MM/YYYY',
    sort: true,
  }, {
    i18nLabel: 'ui.purchasesInvoice.list.column.total',
    field: 'total',
    sort: true,
    formatter: (value, row: IPurchasesInvoice) => {
      return this._accounting.ac.formatMoney(value, '');
    },
    columnClasses: 'right-align',
  }, {
    i18nLabel: 'ui.purchasesInvoice.list.column.balanceDue',
    field: 'balanceDue',
    sort: true,
    formatter: (value, row: IPurchasesInvoice) => {
      return this._accounting.ac.formatMoney(value, '');
    },
    columnClasses: 'right-align',
  }, {
    i18nLabel: 'ui.purchasesInvoice.list.column.status',
    field: 'status',
    sort: true,
    formatter: (value, row: IPurchasesInvoice) => {
      return _.startCase(value);
    },
  }];
  tableColumnsShow: boolean = false;
  tableColumnsToggle = this._gridTableToggle.mapToggleOptionsFromColumns(this.tableColumns);

  retryDialogShownState: boolean = false;
  retryDialogOnRetry: Function;

  private loadDataTimeoutId: number;
  private gridFirstLoad: boolean = true;
  private subscriptions: { [key: string]: Subscription[] } = {
    load: []
  };

  constructor(
    public _accounting: AccountingService,
    private _activatedRoute: ActivatedRoute,
    private _adminLayout: AdminLayoutService,
    private _changeDetectorRef: ChangeDetectorRef,
    private _gridTable: GridTableService,
    private _dataTableExport: ExportDataTableService,
    private _gridTableToggle: GridTableToggleService,
    private _purchasesInvoiceClone: PurchasesInvoiceCloneService,
    public _purchasesInvoiceCopyDebitNote: PurchasesInvoiceCopyDebitNoteService,
    private _purchasesInvoiceMoreFilter: PurchasesInvoiceMoreFilterService,
    private _purchasesInvoiceRest: PurchasesInvoiceRestService,
    public _systemMessage: SystemMessageService,
    private _translate: TranslateService,
  ) {
    this._adminLayout.containerType = 1;
  }

  ngOnInit() {
    this.loadData();
  }

  ngOnDestroy() {
    this._purchasesInvoiceMoreFilter.lastMoreFilterValues = {};
  }

  onMoreFilter(filters: IGridTableFilterParsed) {
    this.elPopMoreFilter.hide();

    this.moreFilterValues = filters;
    this.loadData();
  }

  populateFilters() {
    let newFilters: any = _.pick(this.qParamsFilters, ['keyword']);

    if (this.moreFilterValues) {
      newFilters = Object.assign({}, newFilters, this.moreFilterValues.qParams);
    }

    this.qParamsFilters = newFilters;
  }


  gridLoadDataWrapper() {
    // Grid always load at the first time. we dont want that. we already call on ngOnInit
    if (this.gridFirstLoad) {
      this.gridFirstLoad = false;
      return;
    }

    if (this.loadDataTimeoutId) {
      window.clearTimeout(this.loadDataTimeoutId);
      this.loadDataTimeoutId = null;
    }

    this.loadDataTimeoutId = window.setTimeout(() => {
      this.loadData();
    }, 300);
  }

  showVoidDialog(invoiceId: string) {
    swal({
      title: this._translate.instant('ui.purchasesInvoice.confirm.void.label'),
      text: this._translate.instant('ui.purchasesInvoice.confirm.void.description'),
      type: 'question',
      showCancelButton: true,
    })
      .then(() => {
        this.voidInvoice(invoiceId);
      })
      .catch(() => { });
  }

  voidInvoice(invoiceId: string) {
    this._purchasesInvoiceRest.void(invoiceId)
    .catch((error) => {
      this._systemMessage.log({
        message: error.response.data[0].errorMessage,
        type: 'error'
      });
      return Observable.throw(error);
    })
    .subscribe(response => {
      this.gridLoadDataWrapper();
      SnackBar.show({
        text: this._translate.instant('ui.purchasesInvoice.success.void'),
        pos: 'bottom-right'
      });
    });
  }

  printReport(invoiceId: string) {
    this._purchasesInvoiceRest.load(invoiceId).subscribe(response => {
      this.doc = response;
      this._purchasesInvoiceRest.loadRelatedData().subscribe(related => {
        this.doc['companyInfo'] = related.settings.companyInfo;
        this.elReportModal.show();
      });
    });
  }

  get exportRecords(): Observable<IPurchasesInvoice[]> {
    const qOption = {
      ...this._gridTable.generateApiQueryOptionFromGridInfo(
        this.gridDataSource.pager,
        false,
        this.gridTable,
        this._gridTable.generateCombinedApiFilters(this.combinedFilterValues, this.combinedFilterFields)
      ),
      skip: 0,
      take: this._dataTableExport.exportLimit,
    };

    this.populateFilters();

    this.qParamsFilters.includeParentTransactions = true;

    return this.elRetryDialog.createRetryEntry(
      this._purchasesInvoiceRest.findAll(qOption, this.qParamsFilters).map(response => response.data)
    );
  }

  exportPDF() {
    this.exportRecords.subscribe(records => {
      this._dataTableExport.export({
        records,
        mapOptions: this._dataTableExport.mapFromTableColumns(this.tableColumns),
        fileName: 'all-purchase-invoices',
        extension: 'pdf',
        templateData: {
          title: this._translate.instant('ui.purchaseInvoice.list.title'),
        },
      });
    });
  }

  exportExcel() {
    this.exportRecords.subscribe(records => {
      this._dataTableExport.export({
        records: records,
        mapOptions: this._dataTableExport.mapFromTableColumns(this.tableColumns),
        fileName: 'all-purchase-invoices',
        extension: 'xls',
      });
    });
  }

  loadData() {
    this.elPageLoading.forceShow();

    const qOption = this._gridTable.generateApiQueryOptionFromGridInfo(
      this.gridDataSource.pager,
      true,
      this.gridTable,
      this._gridTable.generateCombinedApiFilters(this.combinedFilterValues, this.combinedFilterFields)
    );

    this.populateFilters();

    this.qParamsFilters.includeParentTransactions = true;

    this.subscriptions.load.forEach(sub => sub.unsubscribe());
    this.subscriptions.load.push(
      this.elRetryDialog.createRetryEntry(
        this._purchasesInvoiceRest.findAll(qOption, this.qParamsFilters)
          .finally(() => {
            this.elPageLoading.forceHide();
          })
      ).subscribe(response => {
        this.gridDataSource.updateFromApiPaginationResult(response);
        this.compReady = true;
      })
    );
  }

  cloneTransaction(doc) {
    this.elPageLoading.forceShow();

    this._purchasesInvoiceRest.load(doc.id).subscribe(newDoc => {
      this.elPageLoading.forceHide();

      this._purchasesInvoiceClone.copy(newDoc);
    });
  }

  copyInvoicetoDebitNote(doc) {
    this.elPageLoading.forceShow();

    this._purchasesInvoiceRest.load(doc.id).subscribe(newDoc => {
      this.elPageLoading.forceHide();

      this._purchasesInvoiceCopyDebitNote.copy(newDoc);
    });
  }

  showQuickPayModal(doc) {
    this.elPageLoading.forceShow();

    this._purchasesInvoiceRest.load(doc.id).subscribe(newDoc => {
      this.modalDoc = newDoc;
      this.elQuickPayModal.show();

      this._changeDetectorRef.detectChanges();

      this.elPageLoading.forceHide();
    });
  }

  didPurchasesPaymentQuickCreate() {
    this.loadData();
    this.elQuickPayModal.hide();
  }

  public onGridChange() {
    this.gridLoadDataWrapper();
  }

  public onGridPageChange() {
    this.gridLoadDataWrapper();
  }
}

import swal from 'sweetalert2';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';
import { TranslateService } from '@ngx-translate/core';

import { AccountingService, RouteStateService } from '../../../core/core.module';
import { AdminLayoutService } from '../../../layouts/layouts.module';
import { BusinessPartnerRestService } from '../../business-partner/business-partner-rest.service';
import { PurchasesInvoiceCloneService } from '../purchases-invoice-clone.service';
import { PurchasesInvoiceCopyDebitNoteService } from '../purchases-invoice-copy-debit-note.service';
import { PurchasesInvoiceRestService } from '../purchases-invoice-rest.service';

@Component({
  selector: 'app-purchases-invoice-detail',
  templateUrl: 'purchases-invoice-detail.component.html'
})
export class PurchasesInvoiceDetailComponent implements OnInit {
  creditOffered: boolean = false;
  credits: IBusinessPartnerAPCredit[] = [];
  doc: IPurchasesInvoice;
  routeParams: any;

  @ViewChild('quickPayModal') elQuickPayModal: ModalDirective;
  @ViewChild('allocateCreditModal') elAllocateCreditModal: ModalDirective;

  constructor(
    public _accounting: AccountingService,
    private _adminLayout: AdminLayoutService,
    private _businessPartnerRest: BusinessPartnerRestService,
    public _purchasesInvoiceClone: PurchasesInvoiceCloneService,
    public _purchasesInvoiceCopyDebitNote: PurchasesInvoiceCopyDebitNoteService,
    public _purchasesInvoiceRest: PurchasesInvoiceRestService,
    private _route: ActivatedRoute,
    private _routeState: RouteStateService,
    private _translate: TranslateService,
  ) {
    this._adminLayout.containerType = 1;
    this._route.params.subscribe(params => this.routeParams = params);
  }

  ngOnInit() {
    this.loadData().subscribe();
  }

  loadData(): Observable<IPurchasesInvoice> {
    return this._purchasesInvoiceRest.load(this.routeParams.id).do(invoice => {
      this.doc = invoice;

      this._businessPartnerRest.getAPCredits(this.doc.vendor.id).subscribe(credits => {
        this.credits = credits;

        this.offerAllocateCredit();
      });
    });
  }

  onPaymentQuickCreated(payment) {
    this.loadData().subscribe(() => {
      this.elQuickPayModal.hide();
    });
  }

  onCreditAllocated(record: ISalesInvoice) {
    this.loadData().subscribe(() => {
      this.elAllocateCreditModal.hide();
    });
  }

  offerAllocateCredit() {
    if (!this.creditOffered && this.credits.length && this._routeState.previousUrl === '/purchases/invoices/create') {
      swal({
        title: this._translate.instant('confirm.purchasesInvoice.detail.offerAllocateCredit.label'),
        text: this._translate.instant('confirm.purchasesInvoice.detail.offerAllocateCredit.description'),
        type: 'question',
        showCancelButton: true,
      }).then(() => {
        this.elAllocateCreditModal.show();
      }).catch();

      this.creditOffered = true;
    }
  }
}

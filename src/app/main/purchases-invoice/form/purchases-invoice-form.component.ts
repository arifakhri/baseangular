import * as _ from 'lodash';
import * as moment from 'moment';
import { Component, OnChanges, OnInit, SimpleChanges, } from '@angular/core';
import { FormGroup } from '@angular/forms';
import 'app/rxjs-imports.ts';

import { AccountingService, AutocompleteService, CommonService } from '../../../core/core.module';
import { PurchasesInvoiceCloneService } from '../purchases-invoice-clone.service';
import { PurchasesInvoiceFormService } from '../form/purchases-invoice-form.service';
import { PurchasesInvoiceRestService } from '../purchases-invoice-rest.service';
import { PurchasesOrderCopyInvoiceService } from '../../purchases-order/purchases-order-copy-invoice.service';

import { TransactionFormPurchasesBComponent } from '../../transaction/transaction-form-purchases.bcomponent';

@Component({
  selector: 'app-purchases-invoice-form',
  templateUrl: 'purchases-invoice-form.component.html'
})
export class PurchasesInvoiceFormComponent extends TransactionFormPurchasesBComponent implements OnChanges, OnInit {
  cloneTransaction: boolean = false;
  copyFromOrder: boolean = false;

  ACProductVariantHandler = CommonService.remoteACItemsHandler({
    remoteParams: this.ACProductVariantParams.bind(this),
    remoteRequest: this._productVariantRest.findAllPicker.bind(this._productVariantRest),
    remoteRequestMap: (response) => response.data,
  });

  constructor(
    public _autocomplete: AutocompleteService,
    public _accounting: AccountingService,
    public _purchasesInvoiceClone: PurchasesInvoiceCloneService,
    public _purchasesInvoiceForm: PurchasesInvoiceFormService,
    private _purchasesInvoiceRest: PurchasesInvoiceRestService,
    private _purchasesOrderCopyInvoice: PurchasesOrderCopyInvoiceService,
  ) {
    super();
    super.init();
    this.componentId = 'PurchasesInvoiceForm';

    this.registerHook('loadRelated', event => {
      return this._purchasesInvoiceRest.loadRelatedData();
    }, reletedData => {
      this._purchasesInvoiceForm.patchTransactionSettings(this.form, reletedData.settings);

      if (_.has(this.page.routeQueryParams, 'orderId')) {
        const loading = this.page.createLoading('purchasesOrderCopyToInvoice');

        this._purchasesOrderCopyInvoice.copyById(this.page.routeQueryParams.orderId).subscribe(() => {
          this.applyCopyOrder();

          loading.dispose();
        });
      } else {
        this.applyCopyOrder();
      }

      if (this._purchasesInvoiceClone.docOnHold) {
        const parentDoc = <any>this._purchasesInvoiceClone.docOnHold;
        parentDoc.deliveryDate = null;
        parentDoc.transactionDate = new Date;
        parentDoc.transactionNumber = null;

        this.cloneTransaction = true;
        this.doc = parentDoc;
        this._purchasesInvoiceClone.docOnHold = null;

        this.applyDoc();

        this.form.patchValue(this.doc);
      }
    });

    this.registerHook('patchForm', event => {
      this.applyDoc();
    });

    this.registerHook('afterVendorSelected', event => {
      const vendor = event.data;
      let dueDate;
      if (vendor.defaultPurchaseInvoiceDueDays) {
        dueDate = moment().add(vendor.defaultPurchaseInvoiceDueDays, 'days').toDate();
      } else {
        dueDate = new Date;
      }
      this.form.patchValue({
        dueDate,
      });
    });
  }

  ngOnInit() {
    super.ngOnInit();
    this.addLines();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (_.has(changes, 'doc') && this.doc) {
      this.applyDoc();
    }
  }

  onPageInit() {
    super.onPageInit();
  }

  getDefaultValue(type: 'master' | 'line'): any {
    return this._purchasesInvoiceForm.getDefaultValues(type);
  }

  buildFormChildLine() {
    return this._purchasesInvoiceForm.buildFormChildLine();
  }

  lineConditionalValidation(formGroup: FormGroup) {
    this._purchasesInvoiceForm.lineConditionalValidation(formGroup);
  }

  applyCopyOrder() {
    if (this._purchasesOrderCopyInvoice.docOnHold) {
      this.copyFromOrder = true;

      this.doc = this._purchasesOrderCopyInvoice.apply();

      this.applyDoc();

      this.form.patchValue(this.doc);
    }
  }
}

import { MBase } from '../../../core/common/base.model';

export class MPurchasesInvoiceForm extends MBase {
  adjustmentAmount: number = 0;
  billingAddress: string;
  branchId: number = 0;
  description: string;
  discountAmount: number = 0;
  discountPercent: number = null;
  dueDate: string;
  lines: MPurchasesInvoiceFormLine[] = [];
  note: string;
  shippingAddress: string;
  shippingCharge: number = 0;
  subtotal: number = 0;
  taxAmount: number = 0;
  taxed: boolean = true;
  total: number = 0;
  totalPaid: number = 0;
  transactionDate: string;
  transactionNumber: string;
  vendorEmail: string;
  vendor: string;
  vendorId: string;
  vendorNote: string;
  vendorRefNumber: string;

  parentTransactionType: string;
}

export class MPurchasesInvoiceFormLine extends MBase {
  amount: number = 0;
  description: string;
  discountAmount: number = 0;
  discountPercent: number = null;
  expenseAccountId: string;
  note: string;
  parentTransactionId: string;
  parentTransactionLineId: string;
  product: string;
  productId: string;
  productVariant: string;
  productVariantId: string;
  qty: number = 0;
  tax: string;
  taxId: string;
  taxRate: number = 0;
  totalDiscount: number = 0;
  unitPrice: number = 0;
  uomConversion: number = 0;
  uom: string;
  uomId: string;
  warehouseId: string;
}

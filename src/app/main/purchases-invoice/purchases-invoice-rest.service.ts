import { Injectable } from '@angular/core';
import 'app/rxjs-imports.ts';

import { ApiQueryOption, RequestService } from '../../core/core.module';

import { APP_CONST } from '../../app.const';

@Injectable()
export class PurchasesInvoiceRestService {
  baseURL = `${APP_CONST.API_MAIN}/purchases/invoices`;
  request = this._request.new(this.baseURL);

  constructor(
    private _request: RequestService,
  ) { }

  create(invoice: IPurchasesInvoice, queryParams: any = {}) {
    return this.request.post<IPurchasesInvoice>(``, invoice, { params: queryParams });
  }

  findAll(queryOption: ApiQueryOption, queryParams: any = {}) {
    return this.request.post<IApiPaginationResult<IPurchasesInvoice>>(`q`, queryOption, { params: queryParams });
  }

  load(invoiceId: string) {
    return this.request.get<IPurchasesInvoice>(`${invoiceId}?includeLines=true`);
  }

  loadRelatedData() {
    return this.request.get<{ taxes: ITax[], warehouses: IWarehouse[], settings: ISettings }>(`entry-related-data`);
  }

  update(invoiceId: string, updateObj: IPurchasesInvoice, queryParams: any = {}) {
    return this.request.put<IPurchasesInvoice>(`${invoiceId}`, updateObj, { params: queryParams });
  }

  void(invoiceId: string) {
    return this.request.put<IPurchasesInvoice>(`${invoiceId}/void`, {});
  }

  allocateCredit(invoiceId: string, credits: IPurchasesAllocateCredit[]) {
    return this.request.put<IPurchasesInvoice>(`${invoiceId}/allocate-credit`, credits);
  }

  autoAllocateDownPayment(invoiceId: string) {
    return this.request.put<IPurchasesInvoice>(`${invoiceId}/downpayments/auto-allocate`);
  }
}

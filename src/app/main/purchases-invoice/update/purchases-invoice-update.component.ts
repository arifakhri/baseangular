import * as _ from 'lodash';
import * as moment from 'moment';
import { Component, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';

import { PurchasesInvoiceFormService } from '../form/purchases-invoice-form.service';
import { PurchasesInvoiceRestService } from '../purchases-invoice-rest.service';
import { PurchasesInvoiceService } from '../purchases-invoice.service';

import { BaseUpdateBComponent } from '../../../shared/base/base.module';

@Component({
  selector: 'app-purchases-invoice-update',
  templateUrl: 'purchases-invoice-update.component.html',
})
export class PurchasesInvoiceUpdateComponent extends BaseUpdateBComponent {
  @ViewChild('reportModal') elReportModal: ModalDirective;

  constructor(
    private _purchasesInvoice: PurchasesInvoiceService,
    private _purchasesInvoiceForm: PurchasesInvoiceFormService,
    private _purchasesInvoiceRest: PurchasesInvoiceRestService,
  ) {
    super();

    this.componentId = 'PurchasesInvoiceUpdate';
    this.headerTitle = 'ui.purchasesInvoice.update.title';
    this.containerType = 1;
    this.routeURL = '/purchases/invoices';
    this.entrySuccessI18n = 'success.purchasesInvoice.update';

    this.registerHook('buildForm', event => {
      this._purchasesInvoiceForm.setFormDefinitions(this.form);
    });

    this.registerHook('load', event => {
      return this._purchasesInvoiceRest.load(this.page.routeParams.id).switchMap(doc => {
        if (doc.transactionDate) {
          doc.transactionDate = moment(doc.transactionDate).toDate();
        }
        if (doc.dueDate) {
          doc.dueDate = moment(doc.dueDate).toDate();
        }
        return Observable.of(doc);
      });
    });

    this.registerHook('save', event => {
      const invoice: IPurchasesInvoice = Object.assign({}, this.doc, this.form.value);
      this._purchasesInvoice.normalizeDoc(invoice);

      const isPrinting = _.get(event, 'data.print');

      return this._purchasesInvoiceRest.update(this.page.routeParams.id, invoice, { includeLines: isPrinting });
    });
  }

  async saveAndPrint() {
    try {
      const submitted = await this.onSubmit({ saveConfirm: true, print: true });
      if (submitted) {
        this._purchasesInvoiceRest.loadRelatedData().subscribe(related => {
          this.doc['companyInfo'] = related.settings.companyInfo;
          this.elReportModal.show();
        });
      }
    } catch (e) {

    }
  }
}

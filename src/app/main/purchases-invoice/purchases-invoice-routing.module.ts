import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PurchasesInvoiceCreateComponent } from './create/purchases-invoice-create.component';
import { PurchasesInvoiceDetailComponent } from './detail/purchases-invoice-detail.component';
import { PurchasesInvoiceListComponent } from './list/purchases-invoice-list.component';
import { PurchasesInvoiceUpdateComponent } from './update/purchases-invoice-update.component';

import { AuthorizationService } from '../../core/core.module';

export const routes: Routes = [{
  path: '',
  children: [{
    path: '',
    component: PurchasesInvoiceListComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'purchasesInvoice.list',
    },
  }, {
    path: 'create',
    component: PurchasesInvoiceCreateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'purchasesInvoice.create',
    },
  }, {
    path: ':id',
    component: PurchasesInvoiceDetailComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'purchasesInvoice.detail',
    },
  }, {
    path: ':id/update',
    component: PurchasesInvoiceUpdateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'purchasesInvoice.update',
    },
  }]
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class PurchasesInvoiceRoutingModule { }

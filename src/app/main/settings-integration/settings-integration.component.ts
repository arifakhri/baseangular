import { Component } from '@angular/core';

@Component({
  selector: 'app-settings-integration',
  templateUrl: 'settings-integration.component.html',
  styleUrls: ['settings-integration-component.scss']
})
export class SettingsIntegrationComponent {
  settingItems = [
    [{
      i18nTitle: 'ui.settingsIntegration.item.chatbot',
      icon: '/assets/img/settings/chatbot-icon.png',
      link: '/settings/chatbot/externals',
    }, {
      i18nTitle: 'ui.settingsIntegration.item.saleschannel',
      icon: '/assets/img/settings/saleschannel-icon.png',
      link: '/settings/saleschannel',
    }]
  ];
}

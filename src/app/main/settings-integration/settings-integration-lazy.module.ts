import { NgModule } from '@angular/core';
import { SettingsIntegrationModule } from './settings-integration.module';
import { SettingsIntegrationRoutingModule } from './settings-integration-routing.module';

@NgModule({
  imports: [
    SettingsIntegrationModule,
    SettingsIntegrationRoutingModule
  ]
})
export class SettingsIntegrationLazyModule { }

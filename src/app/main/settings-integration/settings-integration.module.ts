import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { SettingsIntegrationComponent } from './settings-integration.component';
import { SettingsModule } from '../settings/settings.module';

@NgModule({
  imports: [
    CommonModule,
    FlexLayoutModule,
    RouterModule,
    SettingsModule,
    TranslateModule,
  ],
  declarations: [
    SettingsIntegrationComponent
  ]
})
export class SettingsIntegrationModule { }

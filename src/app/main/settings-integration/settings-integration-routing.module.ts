import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SettingsIntegrationComponent } from './settings-integration.component';

import { AuthorizationService } from '../../core/core.module';

export const routes: Routes = [{
  path: '',
  children: [{
    path: '',
    component: SettingsIntegrationComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'settingsIntegration'
    }
  }]
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class SettingsIntegrationRoutingModule { }

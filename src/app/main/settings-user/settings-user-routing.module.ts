import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SettingsUserListComponent } from './list/settings-user-list.component';

import { AuthorizationService } from '../../core/core.module';

const routes: Routes = [{
  path: '',
  children: [{
    path: '',
    component: SettingsUserListComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'settingsUser.list'
    }
  }]
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ],
})
export class SettingsUserRoutingModule { }

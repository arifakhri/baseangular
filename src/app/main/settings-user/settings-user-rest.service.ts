import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';

import { ApiQueryOption, RequestService } from '../../core/core.module';

import { APP_CONST } from '../../app.const';

@Injectable()
export class SettingsUserRestService {
  baseURL = `${APP_CONST.API_ACCOUNT}/company`;
  basePickerURL = `${APP_CONST.API_MAIN}/pickers/users`;

  request = this._request.new(this.baseURL);
  requestPicker = this._request.new(this.basePickerURL);
  constructor(
    private _request: RequestService,
  ) { }

  createInvite(settingsUser: ISettingsUser) {
    return this.request.post<ISettingsUser>(`invites`, settingsUser);
  }

  findAllInvites() {
    return this.request.get<ISettingsUser[]>(`invites`);
  }

  findAllUsers() {
    return this.request.get<ISettingsUser[]>(`users`);
  }

  findAllUsersPicker(queryOption: ApiQueryOption, queryParams: any = {}): Observable<IApiPaginationResult<ISettingsUser>> {
    return this.requestPicker.post<IApiPaginationResult<ISettingsUser>>(``, queryOption, { params: queryParams });
  }

  deleteInvite(ISettingsUserId: string) {
    return this.request.delete<any>(`invites/${ISettingsUserId}`);
  }
}

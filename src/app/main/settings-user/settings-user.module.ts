import { BsDropdownModule, ModalModule, PaginationModule } from 'ngx-bootstrap';
import { CalendarModule } from 'primeng/components/calendar/calendar';
import { CommonModule } from '@angular/common';
import { DataTableModule } from 'primeng/components/datatable/datatable';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ListboxModule } from 'primeng/components/listbox/listbox';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

import { SettingsUserFormComponent } from './form/settings-user-form.component';
import { SettingsUserInviteComponent } from './invite/settings-user-invite.component';
import { SettingsUserListComponent } from './list/settings-user-list.component';

import { SettingsUserRestService } from './settings-user-rest.service';

import { CoreModule } from '../../core/core.module';
import { SettingsModule } from '../settings/settings.module';
import { SharedModule } from '../../shared/shared.module';

export const PROVIDERS = [
  SettingsUserRestService,
];

@NgModule({
  imports: [
    BsDropdownModule,
    CalendarModule,
    CommonModule,
    CoreModule,
    DataTableModule,
    FlexLayoutModule,
    FormsModule,
    ListboxModule,
    ModalModule,
    PaginationModule,
    ReactiveFormsModule,
    SettingsModule,
    SharedModule,
    TranslateModule,
  ],
  declarations: [
    SettingsUserFormComponent,
    SettingsUserInviteComponent,
    SettingsUserListComponent,
  ],
})
export class SettingsUserModule { }

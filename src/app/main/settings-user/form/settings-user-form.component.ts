import { Component, Input, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-settings-user-form',
  templateUrl: './settings-user-form.component.html',
})
export class SettingsUserFormComponent implements OnInit {
  @Input() form: FormGroup;

  ngOnInit() {
    this.form.addControl('firstName', new FormControl('', Validators.required));
    this.form.addControl('lastName', new FormControl('', Validators.required));
    this.form.addControl('email', new FormControl('', [Validators.required, Validators.email]));
    this.form.addControl('roles', new FormArray([]));
  }
}

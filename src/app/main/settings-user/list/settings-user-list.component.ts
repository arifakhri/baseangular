import * as SnackBar from 'node-snackbar';
import swal from 'sweetalert2';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { DataTable } from 'primeng/components/datatable/datatable';
import { ModalDirective } from 'ngx-bootstrap';
import { Observable, Subscription } from 'rxjs';
import 'app/rxjs-imports.ts';
import { TranslateService } from '@ngx-translate/core';

import { GridTableDataSource, GridTableService, SystemMessageService, } from '../../../core/core.module';

import { SettingsUserRestService } from '../settings-user-rest.service';
import { RetryDialogComponent } from '../../../shared/retry-dialog/retry-dialog.component';

@Component({
  selector: 'app-settings-user-list',
  templateUrl: './settings-user-list.component.html',
  providers: [SystemMessageService]
})
export class SettingsUserListComponent implements OnInit {
  @ViewChild('inviteModal') elInviteModal: ModalDirective;
  @ViewChild('retryDialog') elRetryDialog: RetryDialogComponent;

  compReady: boolean;
  companyInvite: ISettingsUser[] = [];
  companyUser: ISettingsUser[] = [];
  gridDataSource: GridTableDataSource<ISettingsUser> = new GridTableDataSource<ISettingsUser>();

  retryDialogShownState: boolean = false;
  retryDialogOnRetry: Function;

  private subscriptions: { [key: string]: Subscription[] } = {
    load: []
  };

  @ViewChild('gridTable') public gridTable: DataTable;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _gridTable: GridTableService,
    private _settingsUserRest: SettingsUserRestService,
    public _systemMessage: SystemMessageService,
    private _translate: TranslateService,
  ) { }

  ngOnInit() {
    this.loadData();
  }

  loadData() {
    this.compReady = false;

    this.subscriptions.load.forEach(sub => sub.unsubscribe());
    this.subscriptions.load.push(
      this.elRetryDialog.createRetryEntry(this._settingsUserRest.findAllInvites())
        .subscribe(response => {
          this.companyInvite = response;

          this.compReady = true;
        }),
      this.elRetryDialog.createRetryEntry(this._settingsUserRest.findAllUsers())
        .subscribe(response => {
          this.companyUser = response;

          this.compReady = true;
        })
    );
  }

  showDeleteDialog(userInviteId: string) {
    swal({
      title: this._translate.instant(`confirm.settingsUser.delete.label`),
      text: this._translate.instant(`confirm.settingsUser.delete.description`),
      type: 'question',
      showCancelButton: true,
    })
      .then(() => {
        this.compReady = false;
        this._settingsUserRest.deleteInvite(userInviteId)
          .catch(error => {
            this.compReady = true;

            this._systemMessage.log({
              message: error,
              type: 'error'
            });
            return Observable.throw(error);
          })
        .subscribe(response => {
          this.loadData();
          SnackBar.show({
            text: this._translate.instant(`success.settingsUser.delete`),
            pos: 'bottom-right'
          });
        });
      })
      .catch(() => { });
  }
}

import { Component, EventEmitter, Inject, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

import { CommonService, SystemMessageService } from '../../../core/core.module';
import { SpinnerService } from '../../../shared/spinner/spinner.service';
import { SettingsUserRestService } from '../settings-user-rest.service';

@Component({
  selector: 'app-settings-user-invite',
  exportAs: 'app-settings-user-invite',
  templateUrl: 'settings-user-invite.component.html',
  providers: [SystemMessageService]
})
export class SettingsUserInviteComponent {
  @Output() afterSubmit: EventEmitter<ISettingsUser> = new EventEmitter();
  @Output() close: EventEmitter<boolean> = new EventEmitter();

  form: FormGroup = new FormGroup({});

  constructor(
    @Inject('GlobalSystemMessage') private _globalSystemMessage: SystemMessageService,
    private _spinner: SpinnerService,
    private _router: Router,
    private _settingsUserRest: SettingsUserRestService,
    public _systemMessage: SystemMessageService,
    private _translate: TranslateService
  ) { }

  onSubmit() {
    if (this.formValid()) {
      this.save();
    }
  }

  formValid(): boolean {
    if (!this.form.valid) {
      CommonService.markAsDirty(this.form);
      this._systemMessage.log({
        message: this._translate.instant('error.form'),
        type: 'error'
      });
      return false;
    }
    return true;
  }

  save(): void {
    const spinner = this._spinner.showDefault();

    const settingsUser: ISettingsUser = this.form.value;
    this._settingsUserRest.createInvite(settingsUser)
      .catch(error => {
        spinner.dispose();

        this._systemMessage.log({
          message: error,
          type: 'error'
        });
        return Observable.throw(error);
      })
      .subscribe(result => {
        spinner.dispose();

        this._globalSystemMessage.log({
          message: this._translate.instant('success.settingsUser.invite'),
          type: 'success',
          showAs: 'growl',
          showSnackBar: false,
        });
        this.form.reset();
        this.afterSubmit.emit();
      });
  }
}

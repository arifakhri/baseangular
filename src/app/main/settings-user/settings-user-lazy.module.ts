import { NgModule } from '@angular/core';

import { SettingsUserModule } from './settings-user.module';
import { SettingsUserRoutingModule } from './settings-user-routing.module';

@NgModule({
  imports: [
    SettingsUserModule,
    SettingsUserRoutingModule,
  ],
})
export class SettingsUserLazyModule { }

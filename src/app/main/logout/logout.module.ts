import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

import { LogoutComponent } from './logout.component';

import { SharedModule as CentricSharedModule } from '../../modules/ng2centric/shared/shared.module';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FlexLayoutModule,
    TranslateModule.forChild(),
    CentricSharedModule,
    SharedModule,
  ],
  declarations: [
    LogoutComponent
  ]
})
export class LogoutModule { }

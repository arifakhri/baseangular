import { NgModule } from '@angular/core';

import { LogoutModule } from './logout.module';
import { LogoutRoutingModule } from './logout-routing.module';

@NgModule({
  imports: [
    LogoutModule,
    LogoutRoutingModule
  ]
})
export class LogoutLazyModule { }

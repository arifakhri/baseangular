import { AfterViewInit, Component } from '@angular/core';

import { AuthenticationService } from '../../core/core.module';

@Component({
  selector: 'app-logout',
  templateUrl: 'logout.component.html'
})
export class LogoutComponent implements AfterViewInit {
  constructor(
    private _authentication: AuthenticationService
  ) { }

  ngAfterViewInit() {
    this._authentication.logout();
  }
}

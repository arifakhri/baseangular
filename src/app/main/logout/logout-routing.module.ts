import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LogoutComponent } from './logout.component';

import { AuthorizationService } from '../../core/core.module';

export const routes: Routes = [{
  path: '',
  component: LogoutComponent,
  canActivate: [AuthorizationService],
  data: {
    name: 'logout'
  }
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class LogoutRoutingModule { }

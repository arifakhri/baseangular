import { BsDropdownModule, PaginationModule } from 'ngx-bootstrap';
import { CommonModule } from '@angular/common';
import { DataTableModule } from 'primeng/components/datatable/datatable';
import { ListboxModule } from 'primeng/components/listbox/listbox';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { ContactAllComponent } from './all/contact-all.component';
import { ContactButtonAddComponent } from './button-add/contact-button-add.component';
import { ContactTabLinksComponent } from './tab-links/contact-tab-links.component';

import { ContactRestService } from './contact-rest.service';

import { CoreModule } from '../../core/core.module';
import { SharedModule } from '../../shared/shared.module';

export const PROVIDERS = [
  ContactRestService,
];

@NgModule({
  imports: [
    BsDropdownModule,
    CommonModule,
    CoreModule,
    FormsModule,
    ListboxModule,
    FlexLayoutModule,
    HttpModule,
    ReactiveFormsModule,
    RouterModule,
    TranslateModule,
    DataTableModule,
    PaginationModule,
    ListboxModule,
    SharedModule,
  ],
  declarations: [
    ContactAllComponent,
    ContactButtonAddComponent,
    ContactTabLinksComponent,
  ],
  exports: [
    ContactButtonAddComponent,
    ContactTabLinksComponent,
  ],
})
export class ContactModule { }

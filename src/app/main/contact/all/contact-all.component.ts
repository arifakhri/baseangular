import * as _ from 'lodash';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { DataTable } from 'primeng/components/datatable/datatable';
import { Observable, Subscription } from 'rxjs';
import 'app/rxjs-imports.ts';
import { TranslateService } from '@ngx-translate/core';

import { SpinnerDirective } from '../../../shared/spinner/spinner.directive';

import { ExportDataTableService, GridTableDataSource, GridTableService, GridTableToggleService, } from '../../../core/core.module';
import { ContactRestService } from '../contact-rest.service';
import { RetryDialogComponent } from '../../../shared/retry-dialog/retry-dialog.component';

@Component({
  selector: 'app-contact-all',
  templateUrl: 'contact-all.component.html'
})
export class ContactAllComponent implements OnInit {
  @ViewChild('gridTable') public gridTable: DataTable;
  @ViewChild('pageLoading') elPageLoading: SpinnerDirective;
  @ViewChild('retryDialog') elRetryDialog: RetryDialogComponent;

  compReady: boolean;
  contacts: (IEmployee | IBusinessPartner)[] = [];
  gridDataSource: GridTableDataSource<IBusinessPartner | IEmployee> = new GridTableDataSource<IBusinessPartner | IEmployee>();
  selectedRecords: (IEmployee | IBusinessPartner)[] = [];

  tableColumns: IGridTableColumn[] = [{
    i18nLabel: 'ui.contact.all.column.displayName',
    field: 'displayName',
    link: (row: any) => {
      return `/contacts/${_.kebabCase(row.contactType)}s/${row.id}`;
    },
    sort: true,
  }, {
    i18nLabel: 'ui.contact.all.column.phone',
    field: 'phone',
    sort: true,
  }, {
    i18nLabel: 'ui.contact.all.column.email',
    field: 'email',
    sort: true,
  }];
  tableColumnsShow: boolean = false;
  tableColumnsToggle = this._gridTableToggle.mapToggleOptionsFromColumns(this.tableColumns);

  retryDialogShownState: boolean = false;
  retryDialogOnRetry: Function;

  qParams: any = {
    keyword: null
  };

  private loadDataTimeoutId: number;
  private gridFirstLoad: boolean = true;
  private subscriptions: { [key: string]: Subscription[] } = {
    load: []
  };

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _dataTableExport: ExportDataTableService,
    private _gridTable: GridTableService,
    private _gridTableToggle: GridTableToggleService,
    private _contactRest: ContactRestService,
    private _translate: TranslateService,
  ) { }

  ngOnInit() {
    this.loadData();
  }

  gridLoadDataWrapper() {
    // Grid always load at the first time. we dont want that. we already call on ngOnInit
    if (this.gridFirstLoad) {
      this.gridFirstLoad = false;
      return;
    }

    if (this.loadDataTimeoutId) {
      window.clearTimeout(this.loadDataTimeoutId);
      this.loadDataTimeoutId = null;
    }

    this.loadDataTimeoutId = window.setTimeout(() => {
      this.loadData();
    }, 300);
  }

  loadData() {
    this.elPageLoading.forceShow();

    const qOption = this._gridTable.generateApiQueryOptionFromGridInfo(
      this.gridDataSource.pager,
      true,
      this.gridTable,
    );

    this.subscriptions.load.forEach(sub => sub.unsubscribe());
    this.subscriptions.load.push(
      this.elRetryDialog.createRetryEntry(
        this._contactRest.findAll(qOption, this.qParams)
          .finally(() => {
            this.elPageLoading.forceHide();
          })
      ).subscribe(response => {
        this.contacts = response.data;
        this.gridDataSource.updateFromApiPaginationResult(response);

        this.compReady = true;
        })
    );
  }

  get exportRecords(): Observable<IEmployee[]> {
    const qOption = {
      ...this._gridTable.generateApiQueryOptionFromGridInfo(
        this.gridDataSource.pager,
        false,
        this.gridTable,
      ),
      skip: 0,
      take: this._dataTableExport.exportLimit,
    };

    return this._contactRest.findAll(qOption, this.qParams).map(response => response.data);
  }

  exportPDF() {
    this.exportRecords.subscribe(records => {
      this._dataTableExport.export({
        records,
        mapOptions: this._dataTableExport.mapFromTableColumns(this.tableColumns),
        fileName: 'employees-contact',
        extension: 'pdf',
        templateData: {
          title: this._translate.instant('ui.contact.list.title'),
        },
      });
    });
  }

  exportExcel() {
    this.exportRecords.subscribe(records => {
      this._dataTableExport.export({
        records: records,
        mapOptions: this._dataTableExport.mapFromTableColumns(this.tableColumns),
        fileName: 'employees-contact',
        extension: 'xls',
      });
    });
  }

  public onGridChange() {
    this.gridLoadDataWrapper();
  }

  public onGridPageChange() {
    this.gridLoadDataWrapper();
  }
}

import { NgModule } from '@angular/core';

import { ContactModule } from './contact.module';
import { ContactRoutingModule } from './contact-routing.module';

@NgModule({
  imports: [
    ContactModule,
    ContactRoutingModule
  ]
})
export class ContactLazyModule { }

import { Injectable } from '@angular/core';
import 'app/rxjs-imports.ts';

import { ApiQueryOption, RequestService } from '../../core/core.module';

import { APP_CONST } from '../../app.const';

@Injectable()
export class ContactRestService {
  baseURL = `${APP_CONST.API_MAIN}/contacts/all`;
  request = this._request.new(this.baseURL);

  constructor(
    private _request: RequestService,
  ) { }

  findAll(queryOption: ApiQueryOption, queryParams: any = {}) {
    return this.request.post<IApiPaginationResult<IEmployee>>(`q`, queryOption, { params: queryParams });
  }
}

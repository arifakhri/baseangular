import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ContactAllComponent } from './all/contact-all.component';

import { AuthorizationService } from '../../core/core.module';

export const routes: Routes = [{
  path: '',
  children: [{
    path: '',
    redirectTo: 'all',
    pathMatch: 'full',
  }, {
    path: 'all',
    component: ContactAllComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'contact.all'
    }
  }, {
    path: '',
    loadChildren: '../business-partner/business-partner-lazy.module#BusinessPartnerLazyModule'
  }, {
    path: 'employees',
    loadChildren: '../employee/employee-lazy.module#EmployeeLazyModule'
  }]
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class ContactRoutingModule { }

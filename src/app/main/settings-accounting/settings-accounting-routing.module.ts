import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SettingsAccountingUpdateComponent } from './update/settings-accounting-update.component';

import { AuthorizationService } from '../../core/core.module';

export const routes: Routes = [{
  path: '',
  children: [{
    path: '',
    component: SettingsAccountingUpdateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'settingsPurchase.update'
    }
  }]
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class SettingsAccountingRoutingModule { }

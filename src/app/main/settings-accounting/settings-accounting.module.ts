import { AutoCompleteModule } from 'primeng/components/autocomplete/autocomplete';
import { CalendarModule } from 'primeng/components/calendar/calendar';
import { CommonModule } from '@angular/common';
import { CheckboxModule } from 'primeng/components/checkbox/checkbox';
import { DataTableModule } from 'primeng/components/datatable/datatable';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgModule } from '@angular/core';
import { PaginationModule } from 'ngx-bootstrap';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { SettingsAccountingFormComponent } from './form/settings-accounting-form.component';
import { SettingsAccountingUpdateComponent } from './update/settings-accounting-update.component';

import { SettingsAccountingService } from './settings-accounting.service';
import { SettingsAccountingRestService } from './settings-accounting-rest.service';

import { CoreModule } from '../../core/core.module';
import { SettingsModule } from '../settings/settings.module';
import { SharedModule } from '../../shared/shared.module';

export const PROVIDERS = [
  SettingsAccountingService,
  SettingsAccountingRestService,
];

@NgModule({
  imports: [
    AutoCompleteModule,
    CalendarModule,
    CheckboxModule,
    CommonModule,
    CoreModule,
    DataTableModule,
    FlexLayoutModule,
    FormsModule,
    HttpModule,
    PaginationModule,
    ReactiveFormsModule,
    RouterModule,
    SettingsModule,
    SharedModule,
    TranslateModule,
  ],
  declarations: [
    SettingsAccountingFormComponent,
    SettingsAccountingUpdateComponent,
  ],
})
export class SettingsAccountingModule { }

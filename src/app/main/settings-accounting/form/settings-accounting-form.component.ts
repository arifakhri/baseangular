import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { SettingsAccountingRestService } from '../settings-accounting-rest.service';

import { CommonService } from '../../../core/core.module';

@Component({
  selector: 'app-settings-accounting-form',
  templateUrl: 'settings-accounting-form.component.html'
})
export class SettingsAccountingFormComponent implements OnInit {
  @Input() doc: string;
  @Input() form: FormGroup;
  @Output() submit: EventEmitter<any> = new EventEmitter();

  taxes: ITax[] = [];
  taxesSuggestion: ITax[] = [];
  updateForm: boolean = false;

  searchACItems = CommonService.searchLocalACItems.bind(this);
  onACDropdown = CommonService.onLocalACDropdown.bind(this);

  constructor(
    private _settingsPurchaseRest: SettingsAccountingRestService,
  ) {
  }

  ngOnInit() {
    this._settingsPurchaseRest.loadRelatedData().subscribe(related => {
      this.taxes = related.taxes;
    });
  }

  onSubmit() {
    this.submit.emit();
  }
}

declare interface ISettingsAccounting {
  fiscalYearMonthStart: number;
  accountingStartDate: string;
  lockDate: string | Date;
}

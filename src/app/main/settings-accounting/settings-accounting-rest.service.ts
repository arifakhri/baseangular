import { Injectable } from '@angular/core';
import 'app/rxjs-imports.ts';

import { RequestService } from '../../core/core.module';

import { APP_CONST } from '../../app.const';

@Injectable()
export class SettingsAccountingRestService {
  baseURL = `${APP_CONST.API_MAIN}/settings`;

  request = this._request.new(this.baseURL);

  constructor(
    private _request: RequestService,
  ) { }

  load() {
    return this.request.get<ISettingsAccounting>(`accounting`);
  }

  update(updateObj: ISettingsAccounting) {
    return this.request.put<ISettingsAccounting>(`accounting`, updateObj);
  }

  loadRelatedData() {
    return this.request.get<{ taxes: ITax[] }>(`entry-related-data`);
  }
}

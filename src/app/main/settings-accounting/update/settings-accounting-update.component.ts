import * as moment from 'moment';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';
import { TranslateService } from '@ngx-translate/core';

import { SpinnerService } from '../../../shared/spinner/spinner.service';
import { SettingsAccountingRestService } from '../settings-accounting-rest.service';
import { SettingsAccountingService } from '../settings-accounting.service';
import { SystemMessageService } from '../../../core/core.module';
import { RetryDialogComponent } from '../../../shared/retry-dialog/retry-dialog.component';

@Component({
  selector: 'app-settings-accounting-update',
  templateUrl: 'settings-accounting-update.component.html',
  providers: [SystemMessageService]
})
export class SettingsAccountingUpdateComponent implements OnInit {
  @ViewChild('retryDialog') elRetryDialog: RetryDialogComponent;

  compReady: boolean;
  doc: ISettingsAccounting;
  form: FormGroup = new FormGroup({});

  retryDialogShownState: boolean = false;
  retryDialogOnRetry: Function;

  constructor(
    @Inject('GlobalSystemMessage') private _globalSystemMessage: SystemMessageService,
    private _spinner: SpinnerService,
    private _settingsAccounting: SettingsAccountingService,
    private _settingsAccountingRest: SettingsAccountingRestService,
    private _route: ActivatedRoute,
    private _router: Router,
    public _systemMessage: SystemMessageService,
    private _translate: TranslateService,
  ) { }

  ngOnInit() {
    this._settingsAccounting.setFormDefinitions(this.form);

    this.loadData();
  }

  loadData() {
    this.elRetryDialog.createRetryEntry(this._settingsAccountingRest.load())
      .subscribe(settingAccounting => {
        this.doc = settingAccounting;
        if (this.doc.lockDate) {
          this.doc.lockDate = moment(this.doc.lockDate).toDate();
        }
        this.form.patchValue(this.doc);

        this.compReady = true;
      });
  }

  save(): void {
    const spinner = this._spinner.showDefault();

    const settingsAccounting: ISettingsAccounting = Object.assign({}, this.doc, this.form.value);
    this._settingsAccounting.normalizeDoc(settingsAccounting);
    this._settingsAccountingRest.update(settingsAccounting)
      .catch(error => {
        spinner.dispose();

        this._systemMessage.log({
          message: error,
          type: 'error'
        });
        return Observable.throw(error);
      })
      .subscribe(result => {
        spinner.dispose();

        this.doc = result;
        this._globalSystemMessage.log({
          message: this._translate.instant('success.settingsAccounting.update'),
          type: 'success',
          showAs: 'growl',
          showSnackBar: false,
        });
      });
  }

  onSubmit() {
    this.save();
  }
}

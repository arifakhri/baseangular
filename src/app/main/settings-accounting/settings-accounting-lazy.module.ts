import { NgModule } from '@angular/core';
import { SettingsAccountingModule } from './settings-accounting.module';
import { SettingsAccountingRoutingModule } from './settings-accounting-routing.module';

@NgModule({
  imports: [
    SettingsAccountingModule,
    SettingsAccountingRoutingModule
  ]
})
export class SettingsAccountingLazyModule { }

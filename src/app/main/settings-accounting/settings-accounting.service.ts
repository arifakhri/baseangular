import * as moment from 'moment';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class SettingsAccountingService {
  constructor(
    private _formBuilder: FormBuilder,
    private _translate: TranslateService
  ) { }

  normalizeDoc(record: any) {
    if (record.lockDate) {
      record.lockDate = moment(record.lockDate).format('YYYY-MM-DD');
    }
  }

  setFormDefinitions(form: FormGroup) {
    form.addControl('fiscalYearMonthStart', new FormControl(0, Validators.required));
    form.addControl('accountingStartDate', new FormControl(null, Validators.required));
    form.addControl('lockDate', new FormControl(null, Validators.required));
  }
}

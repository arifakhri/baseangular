import { NgModule } from '@angular/core';

import { ProfileModule } from './profile.module';
import { ProfileRoutingModule } from './profile-routing.module';

@NgModule({
  imports: [
    ProfileModule,
    ProfileRoutingModule,
  ]
})
export class ProfileLazyModule { }

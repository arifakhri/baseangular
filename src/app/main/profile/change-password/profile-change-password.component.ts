import { Component, EventEmitter, Inject, Output } from '@angular/core';
import { CustomValidators } from 'ng2-validation';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';
import { TranslateService } from '@ngx-translate/core';

import { ProfileRestService } from '../profile-rest.service';
import { CommonService, SystemMessageService } from '../../../core/core.module';

@Component({
  selector: 'app-profile-change-password',
  templateUrl: './profile-change-password.component.html',
  providers: [SystemMessageService],
})
export class ProfileChangePasswordComponent {
  @Output() cancel: EventEmitter<any> = new EventEmitter;
  @Output() success: EventEmitter<any> = new EventEmitter;

  form: FormGroup = new FormGroup({});

  constructor(
    @Inject('GlobalSystemMessage') private _globalSystemMessage: SystemMessageService,
    private _profileRest: ProfileRestService,
    public _systemMessage: SystemMessageService,
    private _translate: TranslateService,
  ) {
    this.buildForm();
  }

  buildForm() {
    this.form.addControl('oldPassword', new FormControl('', Validators.required));
    this.form.addControl('newPassword', new FormControl('', Validators.required));
    this.form.addControl(
      '_confirmPassword',
      new FormControl('', [Validators.required, CustomValidators.equalTo(this.form.get('newPassword'))])
    );
  }

  onSubmit() {
    if (this.formValid()) {
      this.update();
    }
  }

  formValid(): boolean {
    if (!this.form.valid) {
      CommonService.markAsDirty(this.form);
      this._systemMessage.log({
        message: this._translate.instant('error.form'),
        type: 'error'
      });
      return false;
    }
    return true;
  }

  update() {
    this._profileRest.changePassword(this.form.value)
      .catch(error => {
        this._systemMessage.log({
          message: error,
          type: 'error'
        });
        return Observable.throw(error);
      })
      .subscribe(result => {
        this._globalSystemMessage.log({
          message: this._translate.instant('success.profile.changePassword'),
          type: 'success',
          showAs: 'growl',
          showSnackBar: false,
        });
        this.form.reset();
        this.success.emit(result);
      });
  }
}

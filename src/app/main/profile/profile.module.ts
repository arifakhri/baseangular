import { ModalModule } from 'ngx-bootstrap';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { ProfileChangePasswordComponent } from './change-password/profile-change-password.component';
import { ProfileFormComponent } from './form/profile-form.component';
import { ProfileUpdateComponent } from './update/profile-update.component';

import { ProfileRestService } from './profile-rest.service';

import { CoreModule } from '../../core/core.module';
import { SharedModule } from '../../shared/shared.module';

export const PROVIDERS = [
  ProfileRestService,
];

@NgModule({
  imports: [
    CommonModule,
    CoreModule,
    FlexLayoutModule,
    FormsModule,
    ModalModule,
    ReactiveFormsModule,
    RouterModule,
    SharedModule,
    TranslateModule,
  ],
  declarations: [
    ProfileChangePasswordComponent,
    ProfileFormComponent,
    ProfileUpdateComponent,
  ],
})
export class ProfileModule { }

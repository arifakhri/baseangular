import { Injectable } from '@angular/core';
import 'app/rxjs-imports.ts';

import { RequestService } from '../../core/core.module';

import { APP_CONST } from '../../app.const';

@Injectable()
export class ProfileRestService {
  baseURL = `${APP_CONST.API_ACCOUNT}/users/me`;
  request = this._request.new(this.baseURL);

  constructor(
    private _request: RequestService,
  ) { }

  changePassword(payload: { oldPassword: string; newPassword: string }) {
    return this.request.put<any>(`change-password`, payload);
  }

  load() {
    return this.request.get<IProfile>(``);
  }

  update(profile: IProfile) {
    return this.request.put<IProfile>(``, profile);
  }
}

declare interface IProfile {
  id: string;
  email: string;
  userName: string;
  firstName: string;
  lastName: string;
  fullName: string;
  dateOfBirth: string;
  gender: string;
  pictureId: string;
  picture: {
    id: string;
    fileType: string;
    fileName: string;
    fileExt: string;
    fileSize: number;
    description: string;
    uploadByUserId: string;
    fileUrl: string;
    thumbnailUrl: string;
  };
  emailConfirmed: boolean;
  phoneNumberConfirmed: boolean;
}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ProfileChangePasswordComponent } from './change-password/profile-change-password.component';
import { ProfileUpdateComponent } from './update/profile-update.component';

import { AuthorizationService } from '../../core/auth/authorization.service';

const routes: Routes = [{
  path: '',
  children: [{
    path: 'change-password',
    component: ProfileChangePasswordComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'profile.changePassword'
    }
  }, {
    path: 'update',
    component: ProfileUpdateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'profile.update'
    }
  }]
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ],
})
export class ProfileRoutingModule { }

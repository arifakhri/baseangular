import { Component, Input, OnInit } from '@angular/core';
import { CustomValidators } from 'ng2-validation';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-profile-form',
  templateUrl: './profile-form.component.html',
})
export class ProfileFormComponent implements OnInit {
  @Input() doc: IProfile;
  @Input() form: FormGroup;

  ngOnInit() {
    this.buildForm();
  }

  buildForm() {
    this.form.addControl('firstName', new FormControl('', Validators.required));
    this.form.addControl('lastName', new FormControl('', Validators.required));
    this.form.addControl('phoneNumberCountryId', new FormControl('id', Validators.required));
    this.form.addControl('nationalPhoneNumber', new FormControl('', [Validators.required, CustomValidators.digits]));
    this.form.addControl('pictureId', new FormControl(null));
    this.form.addControl('_password', new FormControl('password'));
  }

  onImageDeleted() {
    this.form.patchValue({ pictureId: null });
  }

  onImageUploaded(image) {
    this.form.patchValue({ pictureId: image.id });
  }
}

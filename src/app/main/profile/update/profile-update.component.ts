import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';
import { TranslateService } from '@ngx-translate/core';

import { CommonService, SystemMessageService } from '../../../core/core.module';
import { ProfileRestService } from '../profile-rest.service';

@Component({
  selector: 'app-profile-update',
  templateUrl: './profile-update.component.html',
  providers: [SystemMessageService],
})
export class ProfileUpdateComponent implements OnInit {
  compReady: boolean;
  doc: IProfile;
  form: FormGroup = new FormGroup({});
  routeParams: any;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _profileRest: ProfileRestService,
    public _systemMessage: SystemMessageService,
    private _translate: TranslateService,
  ) {
    this._activatedRoute.params.subscribe(routeParams => this.routeParams = routeParams);
  }

  ngOnInit() {
    this._profileRest.load().subscribe(profile => {
      this.doc = profile;
      this.form.patchValue(profile);

      this.compReady = true;
    });
  }

  onCancel() {

  }

  onSubmit() {
    if (this.formValid()) {
      this.update();
    }
  }

  formValid(): boolean {
    if (!this.form.valid) {
      CommonService.markAsDirty(this.form);
      this._systemMessage.log({
        message: this._translate.instant('error.form'),
        type: 'error'
      });
      return false;
    }
    return true;
  }

  update() {
    const profile: IProfile = Object.assign({}, this.doc, this.form.value);

    this.compReady = false;

    this._profileRest.update(profile)
      .catch(error => {
        this.compReady = true;

        this._systemMessage.log({
          message: error,
          type: 'error'
        });
        return Observable.throw(error);
      })
      .subscribe(result => {
        this.compReady = true;

        this._systemMessage.log({
          message: this._translate.instant('success.profile.update'),
          type: 'success',
          showSnackBar: false,
        });
      });
  }
}

import { NgModule } from '@angular/core';

import { ReportsModule } from './reports.module';
import { ReportsRoutingModule } from './reports-routing.module';

@NgModule({
  imports: [
    ReportsModule,
    ReportsRoutingModule,
  ]
})
export class ReportsLazyModule { }

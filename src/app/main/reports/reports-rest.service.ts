import { Injectable } from '@angular/core';

import { RequestService } from '../../core/core.module';

import { APP_CONST } from '../../app.const';

@Injectable()
export class ReportsRestService {
  requestFinancial = this._request.new(`${APP_CONST.API_REPORT}/accounting-financial-reports`);
  requestReport = this._request.new(`${APP_CONST.API_REPORT}/reports`);

  constructor(
    private _request: RequestService,
  ) { }
}

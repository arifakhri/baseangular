import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { ReportsComponent } from './reports.component';

import { ReportsRestService } from './reports-rest.service';

export const PROVIDERS = [
  ReportsRestService,
];

@NgModule({
  imports: [
    CommonModule,
    FlexLayoutModule,
    RouterModule,
    TranslateModule,
  ],
  declarations: [
    ReportsComponent,
  ]
})
export class ReportsModule { }

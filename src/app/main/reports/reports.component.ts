import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-reports',
  templateUrl: 'reports.component.html'
})
export class ReportsComponent implements OnInit {
  maps: any;
  selectedMap: any;

  ngOnInit() {
    this.maps = [{
      i18nTab: 'ui.reports.tab1.label',
      i18nLabel: 'ui.reports.tab1.title',
      lists: [
        [{
          i18nLabel: 'ui.reports.balance.label',
          i18nDescription: 'ui.reports.balance.description',
          link: '/reports/balance-sheet',
        }, {
            i18nLabel: 'ui.reports.pnl.label',
            i18nDescription: 'ui.reports.pnl.description',
            link: '/reports/pnl',
          }],
      ],
    }, {
      i18nTab: 'ui.reports.tab2.label',
      i18nLabel: 'ui.reports.tab2.title',
      lists: [
        [{
          i18nLabel: 'ui.reports.sales.label',
          i18nDescription: 'ui.reports.sales.description',
          link: '/reports/sales',
        }, {
          i18nLabel: 'ui.reports.salesDetail.label',
          i18nDescription: 'ui.reports.salesDetail.description',
          link: '/reports/sales-detail',
        }],
        [{
          i18nLabel: 'ui.reports.salesOrder.label',
          i18nDescription: 'ui.reports.salesOrder.description',
          link: '/reports/sales-order',
        }, {
          i18nLabel: 'ui.reports.salesOrderDetail.label',
          i18nDescription: 'ui.reports.salesOrderDetail.description',
          link: '/reports/sales-order-detail',
        }],
        [{
          i18nLabel: 'ui.reports.salesCustomer.label',
          i18nDescription: 'ui.reports.salesCustomer.description',
          link: '/reports/sales-customer',
        }, {
          i18nLabel: 'ui.reports.salesProduct.label',
          i18nDescription: 'ui.reports.salesProduct.description',
          link: '/reports/sales-product',
        }],
        [{
          i18nLabel: 'ui.reports.arAging.label',
          i18nDescription: 'ui.reports.arAging.description',
          link: '/reports/ar-aging-summary',
        }]
      ],
    }, {
      i18nTab: 'ui.reports.tab3.label',
      i18nLabel: 'ui.reports.tab3.title',
      lists: [
        [{
          i18nLabel: 'ui.reports.purchases.label',
          i18nDescription: 'ui.reports.purchases.description',
          link: '/reports/purchases',
        }, {
          i18nLabel: 'ui.reports.purchasesDetail.label',
          i18nDescription: 'ui.reports.purchasesDetail.description',
          link: '/reports/purchases-detail',
        }],
        [{
          i18nLabel: 'ui.reports.purchasesOrder.label',
          i18nDescription: 'ui.reports.purchasesOrder.description',
          link: '/reports/purchases-order',
        }, {
          i18nLabel: 'ui.reports.purchasesOrderDetail.label',
          i18nDescription: 'ui.reports.purchasesOrderDetail.description',
          link: '/reports/purchases-order-detail',
        }],
        [{
          i18nLabel: 'ui.reports.purchasesVendor.label',
          i18nDescription: 'ui.reports.purchasesVendor.description',
          link: '/reports/purchases-vendor',
        },
        {
          i18nLabel: 'ui.reports.purchasesProduct.label',
          i18nDescription: 'ui.reports.purchasesProduct.description',
          link: '/reports/purchases-product',
        }],
        [{
          i18nLabel: 'ui.reports.apAging.label',
          i18nDescription: 'ui.reports.apAging.description',
          link: '/reports/ap-aging-summary',
        }]
      ],
    }, {
      i18nTab: 'ui.reports.tab4.label',
      i18nLabel: 'ui.reports.tab4.title',
      lists: [
        [{
          i18nLabel: 'ui.reports.inventory.label',
          i18nDescription: 'ui.reports.inventory.description',
          link: '/reports/inventory',
        }],
      ],
    }, {
      i18nTab: 'ui.reports.tab5.label',
      i18nLabel: 'ui.reports.tab5.title',
      lists: [],
    }, {
      i18nTab: 'ui.reports.tab6.label',
      i18nLabel: 'ui.reports.tab6.title',
      lists: [],
    }, {
      i18nTab: 'ui.reports.tab7.label',
      i18nLabel: 'ui.reports.tab7.title',
      lists: [
        [{
          i18nLabel: 'ui.reports.expenses.label',
          i18nDescription: 'ui.reports.expenses.description',
          link: '/reports/expenses',
        }, {
            i18nLabel: 'ui.reports.expensesDetail.label',
            i18nDescription: 'ui.reports.expensesDetail.description',
            link: '/reports/expenses-detail',
        }]
      ],
    }];
    this.selectedMap = this.maps[0];
  }

  selectMap(map) {
    this.selectedMap = map;
  }

  isActive(map) {
    return this.selectedMap === map;
  }
}

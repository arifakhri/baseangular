import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ReportsComponent } from './reports.component';

import { AuthorizationService } from '../../core/core.module';

export const routes: Routes = [{
  path: '',
  component: ReportsComponent,
  canActivate: [AuthorizationService],
  data: {
    name: 'reports'
  }
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule
  ]
})
export class ReportsRoutingModule { }

import * as moment from 'moment';
import { Component, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

import { AdminLayoutService } from '../../../layouts/layouts.module';
import { GridTableFilterService } from '../../../core/core.module';
import { ReportsPNLRestService } from '../reports-pnl-rest.service';

@Component({
  selector: 'app-reports-pnl-preview',
  templateUrl: './reports-pnl-preview.component.html',
})
export class ReportsPNLPreviewComponent {
  @ViewChild('reportViewer') elReportViewer: any;

  compReady: boolean = false;
  data: any = [];
  form: FormGroup = new FormGroup({});
  reportViewerReady: boolean = false;

  reportParams: any = {
    title: 'Profit & Loss'
  };

  filtersMap: IGridTableFilterMap = {
    lowDate: {
      targetVar: 'date',
      targetFilter: 'param',
      dateFormat: 'YYYY-MM-DD',
    },
    highDate: {
      targetVar: 'date',
      targetFilter: 'param',
      dateFormat: 'YYYY-MM-DD',
    },
  };

  constructor(
    private _adminLayout: AdminLayoutService,
    private _gridTableFilter: GridTableFilterService,
    private _reportsPNLRest: ReportsPNLRestService,
  ) {
    this._adminLayout.containerType = 1;

    this.buildForm();
    this.loadData();
  }

  buildForm() {
    this.form.addControl('lowDate', new FormControl(moment().set('date', 1).toDate()));
    this.form.addControl('highDate', new FormControl(new Date()));
  }

  loadData() {
    const filters = this._gridTableFilter.parse(this.form.value, this.filtersMap);
    this.compReady = false;

    this._reportsPNLRest.findAll(filters.qParams).subscribe(response => {
      this.data = this.prepareData(response);

      this.prepareParams();
    }, null, () => this.compReady = true);
  }

  prepareParams() {
    const filterValues = this.form.value;

    if (filterValues.lowDate) {
      this.reportParams.lowDate = moment(filterValues.lowDate).format('DD/MM/YYYY');
    }

    if (filterValues.highDate) {
      this.reportParams.highDate = moment(filterValues.highDate).format('DD/MM/YYYY');
    }
  }

  prepareData(rawData: IReportsPNL[]) {
    const results: Array<any> = [];

    for (let i = 0; i < rawData.length; i++) {
      const element = rawData[i];
      const rowData = <any>Object.assign({}, element);
      let rowDescription = element.description;
      if (element && element.level && element.level > 0) {
        for (let j = 1; j <= element.level; j++) {
          rowDescription = '&nbsp;&nbsp;&nbsp;&nbsp;' + rowDescription; // add padding
        }
      }

      rowData.rowDescription = rowDescription;
      results.push(rowData);
    }

    return results;

    // return _(rawData).groupBy('accountClass').reduce((array1, children1, key1) => {
    //   const populatedChildren1 = _(<any>children1).groupBy('accountType').reduce((array2, children2, key2) => {
    //     array2.push({
    //       name: key2,
    //       children: _(children2).map(child2 => ({ name: child2.accountName, total: child2.total })).value(),
    //       total: _.sumBy(<any>children2, 'total'),
    //     });
    //     return array2;
    //   }, []);

    //   array1.push({
    //     name: key1,
    //     children: populatedChildren1,
    //     total: _.sumBy(populatedChildren1, 'total'),
    //   });
    //   return array1;
    // }, []);
  }
}

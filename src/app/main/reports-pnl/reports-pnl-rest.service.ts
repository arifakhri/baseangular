import { Injectable } from '@angular/core';

import { ReportsRestService } from '../reports/reports-rest.service';

@Injectable()
export class ReportsPNLRestService {
  constructor(
    private _reportsRest: ReportsRestService,
  ) { }

  findAll(queryParams: any = {}) {
    return this._reportsRest.requestFinancial.get<IReportsPNL[]>(`pnl`, {
      params: queryParams,
    });
  }
}

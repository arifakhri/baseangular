import { NgModule } from '@angular/core';

import { ReportsPNLModule } from './reports-pnl.module';
import { ReportsPNLRoutingModule } from './reports-pnl-routing.module';

@NgModule({
  imports: [
    ReportsPNLModule,
    ReportsPNLRoutingModule,
  ],
})
export class ReportsPNLLazyModule { }

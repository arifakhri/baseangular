import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ReportsARAgingPreviewComponent } from './preview/reports-ar-aging-preview.component';

import { AuthorizationService } from '../../core/core.module';

const routes: Routes = [{
  path: '',
  children: [{
    path: '',
    component: ReportsARAgingPreviewComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'reportsARAging.preview'
    }
  }]
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ],
})
export class ReportsARAgingRoutingModule { }

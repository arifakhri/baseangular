import { NgModule } from '@angular/core';

import { ReportsARAgingModule } from './reports-ar-aging.module';
import { ReportsARAgingRoutingModule } from './reports-ar-aging-routing.module';

@NgModule({
  imports: [
    ReportsARAgingModule,
    ReportsARAgingRoutingModule,
  ],
})
export class ReportsARAgingLazyModule { }

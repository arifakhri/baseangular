import * as moment from 'moment';
import { Component, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

import { AdminLayoutService } from '../../../layouts/layouts.module';
import { ReportsARAgingRestService } from '../reports-ar-aging-rest.service';
import { GridTableFilterService } from '../../../core/core.module';

@Component({
  selector: 'app-reports-ar-aging-preview',
  templateUrl: './reports-ar-aging-preview.component.html',
})
export class ReportsARAgingPreviewComponent {
  @ViewChild('reportViewer') elReportViewer: any;

  compReady: boolean = false;
  data: any = [];
  form: FormGroup = new FormGroup({});
  reportViewerReady: boolean = false;

  reportParams: any = {
    title: 'AR Aging Summary'
  };

  filtersMap: IGridTableFilterMap = {
    asOfDate: {
      targetVar: 'date',
      targetFilter: 'param',
      dateFormat: 'YYYY-MM-DD',
    }
  };

  constructor(
    private _adminLayout: AdminLayoutService,
    private _gridTableFilter: GridTableFilterService,
    private _reportsARAgingRest: ReportsARAgingRestService,
  ) {
    this._adminLayout.containerType = 1;

    this.buildForm();
    this.loadData();
  }

  buildForm() {
    this.form.addControl('asOfDate', new FormControl(new Date()));
  }

  loadData() {
    const filters = this._gridTableFilter.parse(this.form.value, this.filtersMap);
    this.compReady = false;

    this._reportsARAgingRest.findAll(filters.qParams).subscribe(response => {
      this.data = response.data;

      this.prepareParams();
    }, null, () => this.compReady = true);
  }

  prepareParams() {
    const filterValues = this.form.value;

    if (filterValues.asOfDate) {
      this.reportParams.asOfDate = moment(filterValues.asOfDate).format('DD/MM/YYYY');
    }
  }
}

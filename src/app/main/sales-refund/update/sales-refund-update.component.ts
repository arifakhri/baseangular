import * as _ from 'lodash';
import * as moment from 'moment';
import { Component, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';

import { SalesRefundRestService } from '../sales-refund-rest.service';
import { SalesRefundService } from '../sales-refund.service';

import { BaseUpdateBComponent } from '../../../shared/base/base.module';

@Component({
  selector: 'app-sales-refund-update',
  templateUrl: 'sales-refund-update.component.html',
})
export class SalesRefundUpdateComponent extends BaseUpdateBComponent {
  @ViewChild('reportModal') elReportModal: ModalDirective;

  constructor(
    private _salesRefund: SalesRefundService,
    private _salesRefundRest: SalesRefundRestService,
  ) {
    super();

    this.componentId = 'SalesRefundUpdate';
    this.headerTitle = 'ui.salesRefund.update.title';
    this.containerType = 1;
    this.routeURL = '/sales/refunds';
    this.entrySuccessI18n = 'success.salesRefund.update';

    this.registerHook('buildForm', event => {
      this._salesRefund.setFormDefinitions(this.form);
    });

    this.registerHook('load', event => {
      return this._salesRefundRest.load(this.page.routeParams.id, { includeLines: true }).switchMap(doc => {
        if (doc.transactionDate) {
          doc.transactionDate = moment(doc.transactionDate).toDate();
        }
        return Observable.of(doc);
      });
    });

    this.registerHook('save', event => {
      const refund: ISalesRefund = Object.assign({}, this.doc, this.form.value);
      this._salesRefund.normalizeDoc(refund);

      const isPrinting = _.get(event, 'data.print');
      return this._salesRefundRest.update(this.page.routeParams.id, refund);
    });
  }


  async saveAndPrint() {
    try {
      const submitted = await this.onSubmit({ saveConfirm: true, print: true });
      if (submitted) {
        this._salesRefundRest.loadRelatedData().subscribe(related => {
          this.doc['companyInfo'] = related.settings.companyInfo;
          this.elReportModal.show();
        });
      }
    } catch (e) {

    }
  }
}

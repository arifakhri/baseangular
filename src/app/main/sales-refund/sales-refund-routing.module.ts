import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SalesRefundCreateComponent } from './create/sales-refund-create.component';
import { SalesRefundDetailComponent } from './detail/sales-refund-detail.component';
import { SalesRefundUpdateComponent } from './update/sales-refund-update.component';

import { AuthorizationService } from '../../core/core.module';

export const routes: Routes = [{
  path: '',
  children: [{
    path: 'create',
    component: SalesRefundCreateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'salesRefund.create'
    }
  }, {
    path: ':id',
    component: SalesRefundDetailComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'salesRefund.detail'
    }
  }, {
    path: ':id/update',
    component: SalesRefundUpdateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'salesRefund.update'
    }
  }]
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class SalesRefundRoutingModule { }

import { AutoCompleteModule } from 'primeng/components/autocomplete/autocomplete';
import { CalendarModule } from 'primeng/components/calendar/calendar';
import { CheckboxModule } from 'primeng/components/checkbox/checkbox';
import { DataTableModule } from 'primeng/components/datatable/datatable';
import { ListboxModule } from 'primeng/components/listbox/listbox';
import { BsDropdownModule, ModalModule, PaginationModule, PopoverModule } from 'ngx-bootstrap';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { SalesRefundCreateComponent } from './create/sales-refund-create.component';
import { SalesRefundQuickCreateComponent } from './quick-create/sales-refund-quick-create.component';
import { SalesRefundQuickFormComponent } from './quick-form/sales-refund-quick-form.component';
import { SalesRefundDetailComponent } from './detail/sales-refund-detail.component';
import { SalesRefundFormComponent } from './form/sales-refund-form.component';
import { SalesRefundUpdateComponent } from './update/sales-refund-update.component';

import { SalesRefundRestService } from './sales-refund-rest.service';
import { SalesRefundService } from './sales-refund.service';

import { BusinessPartnerModule } from '../business-partner/business-partner.module';
import { CoreModule } from '../../core/core.module';
import { PaymentMethodModule } from '../payment-method/payment-method.module';
import { SalesModule } from '../sales/sales.module';
import { SharedModule } from '../../shared/shared.module';
import { TransactionModule } from '../transaction/transaction.module';

export const PROVIDERS = [
  SalesRefundRestService,
  SalesRefundService,
];

@NgModule({
  imports: [
    AutoCompleteModule,
    BsDropdownModule,
    BusinessPartnerModule,
    CalendarModule,
    CheckboxModule,
    CommonModule,
    CoreModule,
    DataTableModule,
    FlexLayoutModule,
    FormsModule,
    HttpModule,
    ListboxModule,
    ModalModule,
    PaginationModule,
    PaymentMethodModule,
    PopoverModule,
    ReactiveFormsModule,
    RouterModule,
    SalesModule,
    SharedModule,
    TransactionModule,
    TranslateModule,
  ],
  declarations: [
    SalesRefundQuickCreateComponent,
    SalesRefundQuickFormComponent,
    SalesRefundCreateComponent,
    SalesRefundDetailComponent,
    SalesRefundFormComponent,
    SalesRefundUpdateComponent,
  ],
  exports: [
    SalesRefundQuickCreateComponent,
  ]
})
export class SalesRefundModule { }

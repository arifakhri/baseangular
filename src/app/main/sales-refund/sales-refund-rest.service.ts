import { Injectable } from '@angular/core';
import 'app/rxjs-imports.ts';

import { RequestService } from '../../core/core.module';

import { APP_CONST } from '../../app.const';

@Injectable()
export class SalesRefundRestService {
  baseURL = `${APP_CONST.API_MAIN}/sales/refunds`;
  request = this._request.new(this.baseURL);

  constructor(
    private _request: RequestService,
  ) { }

  create(payment: ISalesRefund) {
    return this.request.post<ISalesRefund>(``, payment);
  }

  load(paymentId: string, queryParams: any = {}) {
    return this.request.get<ISalesRefund>(`${paymentId}`, { params: queryParams });
  }

  loadRelatedData() {
    return this.request.get<{ paymentMethods: IPaymentMethod[]; paymentAccounts: IAccount[]; settings: ISettings; }>(`entry-related-data`);
  }

  update(paymentId: string, updateObj: ISalesRefund) {
    return this.request.put<ISalesRefund>(`${paymentId}`, updateObj);
  }

  void(paymentId: string) {
    return this.request.put<ISalesRefund>(`${paymentId}/void`, {});
  }
}

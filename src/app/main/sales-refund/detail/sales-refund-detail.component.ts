import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { AccountingService } from '../../../core/core.module';
import { AdminLayoutService } from '../../../layouts/layouts.module';
import { SalesRefundRestService } from '../sales-refund-rest.service';

@Component({
  selector: 'app-sales-refund-detail',
  templateUrl: 'sales-refund-detail.component.html'
})
export class SalesRefundDetailComponent implements OnInit {
  doc: ISalesRefund;
  routeParams: any;

  mainReportParams: any;

  constructor(
    public _accounting: AccountingService,
    private _adminLayout: AdminLayoutService,
    private _route: ActivatedRoute,
    private _salesRefundRest: SalesRefundRestService,
  ) {
    this._adminLayout.containerType = 1;
    this._route.params.subscribe(params => this.routeParams = params);
  }

  ngOnInit() {
    this.loadData();
    this.loadRelatedData();
  }

  loadData() {
    this._salesRefundRest.load(this.routeParams.id, { includeLines: true }).subscribe(receipt => {
      this.doc = receipt;
    });
  }

  loadRelatedData() {
    this._salesRefundRest.loadRelatedData().subscribe(relatedData => {
      this.mainReportParams = {
        companyInfo: relatedData.settings.companyInfo,
      };
    });
  }
}

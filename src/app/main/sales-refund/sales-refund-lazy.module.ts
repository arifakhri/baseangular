import { NgModule } from '@angular/core';

import { SalesRefundModule } from './sales-refund.module';
import { SalesRefundRoutingModule } from './sales-refund-routing.module';

@NgModule({
  imports: [
    SalesRefundModule,
    SalesRefundRoutingModule
  ]
})
export class SalesRefundLazyModule { }

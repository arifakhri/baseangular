import * as _ from 'lodash';
import * as moment from 'moment';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { Injectable } from '@angular/core';

import { StartingDataService } from '../../core/core.module';

@Injectable()
export class SalesRefundService {
  constructor(
    private _startingData: StartingDataService,
  ) { }

  normalizeDoc(record: any) {
    if (record.transactionDate) {
      record.transactionDate = moment(record.transactionDate).format('YYYY-MM-DD');
    }

    record.lines = _.reject(record.lines, line => {
      return !(<any>line).refundAmount;
    });
  }

  setFormDefinitions(form: FormGroup) {
    const startingData = this._startingData.data$.getValue();
    const customerPrintNameControl = new FormControl('', Validators.maxLength(50));
    (<any>customerPrintNameControl).validatorData = {
      maxLength: 50
    };

    form.addControl('branchId', new FormControl(startingData.masterBranchId));
    form.addControl('paymentAmount', new FormControl(0, Validators.required));
    form.addControl('customer', new FormControl(null));
    form.addControl('customerId', new FormControl(null, Validators.required));
    form.addControl('customCustomerName', customerPrintNameControl);
    form.addControl('paymentMethod', new FormControl);
    form.addControl('paymentMethodId', new FormControl(null));
    form.addControl('paymentAccount', new FormControl(null));
    form.addControl('paymentAccountId', new FormControl(null, Validators.required));
    form.addControl('transactionNumber', new FormControl(null));
    form.addControl('transactionDate', new FormControl(new Date(), Validators.required));
    form.addControl('customerNote', new FormControl);
    form.addControl('note', new FormControl);
    form.addControl('useCustomCustomerName', new FormControl(true));

    form.get('paymentMethod').valueChanges.subscribe(paymentMethod => {
      if (paymentMethod && paymentMethod.defaultAccount) {
        form.get('paymentAccount').setValue(paymentMethod.defaultAccount);
        form.get('paymentAccountId').setValue(paymentMethod.defaultAccount.id);
      }
    });

    form.addControl('lines', new FormArray([]));
  }

  buildFormChildLine() {
    return new FormGroup({
      id: new FormControl(null),
      rowVersion: new FormControl,
      transaction: new FormControl,
      transactionType: new FormControl,
      transactionId: new FormControl,
      refundAmount: new FormControl
    });
  }
}

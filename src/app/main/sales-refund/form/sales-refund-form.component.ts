import * as _ from 'lodash';
import { Component, OnChanges, SimpleChanges, ViewChild, } from '@angular/core';
import { FormArray } from '@angular/forms';
import { PopoverDirective } from 'ngx-bootstrap';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';

import { AccountingService, AutocompleteService } from '../../../core/core.module';
import { SalesRefundRestService } from '../sales-refund-rest.service';
import { SalesRefundService } from '../sales-refund.service';
import { SalesCreditNoteRestService } from '../../sales-credit-note/sales-credit-note-rest.service';

import { TransactionFormSalesBComponent } from '../../transaction/transaction-form-sales.bcomponent';

@Component({
  selector: 'app-sales-refund-form',
  templateUrl: 'sales-refund-form.component.html',
})
export class SalesRefundFormComponent extends TransactionFormSalesBComponent implements OnChanges {
  @ViewChild('popoverFindByInvoice') elPopoverFindByInvoice: PopoverDirective;

  constructor(
    public _accounting: AccountingService,
    public _autocomplete: AutocompleteService,
    private _salesCreditNoteRest: SalesCreditNoteRestService,
    private _salesRefund: SalesRefundService,
    private _salesRefundRest: SalesRefundRestService,
  ) {
    super();
    super.init();
    this.componentId = 'SalesRefundForm';

    this.registerHook('loadRelated', event => {
      return this._salesRefundRest.loadRelatedData();
    });

    this.registerHook('patchForm', event => {
      this.applyDoc();
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (_.has(changes, 'doc') && this.doc) {
      this.applyDoc();
    }
  }

  applyDoc() {
    this.applyInvoices(this.doc.lines);
  }

  onInvoiceFound(invoice: ISalesInvoice) {
    this.onCustomerSelected(<any>invoice.customer);
    this.elPopoverFindByInvoice.hide();
  }

  onCustomerClear() {
    this.form.get('customerId').reset();
    this.clearInvoices();
  }

  onCustomerSelected(customer: IBusinessPartner) {
    this.form.get('customer').setValue(customer);
    this.form.get('customerId').setValue(customer.id);

    return this.populateInvoices().switchMap(() => Observable.of(customer));
  }

  populateInvoices() {
    return this._salesCreditNoteRest.findAll(
      {
        filter: [],
        sort: [{
          field: 'transactionNumber',
          dir: 'asc'
        }],
        take: 30,
        skip: 0,
        includeTotalCount: false
      },
      {
        customerId: this.form.get('customerId').value,
        isOpen: true,
      }
    ).do(invoices => {
      this.applyInvoices(invoices.data);
    });
  }

  clearInvoices() {
    const formLines = (<FormArray>this.form.get('lines'));
    while (formLines.length) {
      formLines.removeAt(0);
    }
  }

  applyInvoices(invoices) {
    this.clearInvoices();

    invoices.forEach(transaction => {
      const lineFormGroup = this._salesRefund.buildFormChildLine();
      lineFormGroup.patchValue(transaction);

      lineFormGroup.get('transaction').setValue(transaction.transaction || transaction);
      lineFormGroup.get('transactionType').setValue(transaction.transactionType);
      lineFormGroup.get('transactionId').setValue(_.get(transaction, 'transaction.id', transaction.id || null));

      if (transaction.paymentAmount) {
        lineFormGroup.get('refundAmount').setValue(transaction.paymentAmount);
      }

      (<FormArray>this.form.get('lines')).push(lineFormGroup);
    });
  }

  syncAmountByLineRefund() {
    let totalAmount = 0;
    (<FormArray>this.form.get('lines')).controls.forEach(line => {
      const lineRefundValue = line.get('refundAmount').value || 0;
      totalAmount += lineRefundValue;
    });

    this.form.get('paymentAmount').setValue(totalAmount);
  }

  syncTotal() {
    this.syncAmountByLineRefund();
  }
}

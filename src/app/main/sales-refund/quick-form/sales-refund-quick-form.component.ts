import { AutoComplete } from 'primeng/components/autocomplete/autocomplete';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ModalDirective } from 'ngx-bootstrap';

import { AutocompleteService } from '../../../core/core.module';
import { SalesRefundRestService } from '../sales-refund-rest.service';

@Component({
  selector: 'app-sales-refund-quick-form',
  templateUrl: 'sales-refund-quick-form.component.html'
})
export class SalesRefundQuickFormComponent implements OnInit {
  @Input() form: FormGroup;
  @ViewChild('quickCreateModal') elQuickCreateModal: ModalDirective;

  quickCreateAC: AutoComplete;

  accounts: IAccount[] = [];
  paymentMethods: IPaymentMethod[] = [];

  constructor(
    public _autocomplete: AutocompleteService,
    private _salesRefundRest: SalesRefundRestService,
  ) { }

  ngOnInit() {
    this.loadRelatedData();
  }

  assignQuickCreatedEntity(entity: any, storageVar: string) {
    this[storageVar].push(entity);
    this.quickCreateAC.selectItem(entity);
    this.elQuickCreateModal.hide();
  }

  loadRelatedData() {
    this._salesRefundRest.loadRelatedData().subscribe(related => {
      this.accounts = related.paymentAccounts;
      this.paymentMethods = related.paymentMethods;
    });
  }
}

import * as _ from 'lodash';
import { Component, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import 'app/rxjs-imports.ts';

import { SalesRefundRestService } from '../sales-refund-rest.service';
import { SalesRefundService } from '../sales-refund.service';

import { BaseCreateBComponent } from '../../../shared/base/base.module';
import { MSalesRefund } from '../sales-refund.model';

@Component({
  selector: 'app-sales-refund-create',
  templateUrl: 'sales-refund-create.component.html'
})
export class SalesRefundCreateComponent extends BaseCreateBComponent {
  @ViewChild('reportModal') elReportModal: ModalDirective;
  @ViewChild('printShippingModal') elShippingModal: ModalDirective;
  @ViewChild('printShippingModalInvalid') elShippingInvModal: ModalDirective;

  constructor(
    private _salesRefund: SalesRefundService,
    private _salesRefundRest: SalesRefundRestService
  ) {
    super();

    this.componentId = 'SalesRefundCreate';
    this.headerTitle = 'ui.salesRefund.create.title';
    this.containerType = 1;
    this.routeURL = '/sales/payments';
    this.entrySuccessI18n = 'success.salesRefund.create';

    this.autoprintButtons = [{
      checkboxConfigId: 'autoprint.srefund.create',
      checkboxLabel: 'ui.salesRefund.create.action.apCheckbox.report',
      previewLabel: 'ui.salesRefund.create.action.previewReport',
      onPreview: () => {
        this.saveAndPrint();
      }
    }];

    this.registerHook('buildForm', event => {
      this._salesRefund.setFormDefinitions(this.form);
    });

    this.registerHook('save', event => {
      let refund = new MSalesRefund;

      refund = _.assign(refund, this.form.value);
      this._salesRefund.normalizeDoc(refund);

      return this._salesRefundRest.create(refund);
    });
  }

  async saveAndPrint() {
    try {
      const submitted = await this.onSubmit({ saveConfirm: true, print: true });
      if (submitted) {
        this._salesRefundRest.loadRelatedData().subscribe(related => {
          this.doc['companyInfo'] = related.settings.companyInfo;
          this.elReportModal.show();
        });
      }
    } catch (e) {

    }
  }
}

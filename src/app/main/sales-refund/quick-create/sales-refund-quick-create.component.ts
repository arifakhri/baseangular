import { Component, EventEmitter, Inject, Input, OnInit, Output } from '@angular/core';
import { FormArray, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

import { CommonService, SystemMessageService } from '../../../core/core.module';
import { SalesRefundRestService } from '../sales-refund-rest.service';
import { SalesRefundService } from '../sales-refund.service';

@Component({
  selector: 'app-sales-refund-quick-create',
  templateUrl: 'sales-refund-quick-create.component.html',
  providers: [SystemMessageService]
})
export class SalesRefundQuickCreateComponent implements OnInit {
  @Input() sourceTransaction: any;

  @Output() afterSubmit: EventEmitter<ISalesRefund> = new EventEmitter();
  @Output() close: EventEmitter<boolean> = new EventEmitter();

  form: FormGroup;
  showLoader: boolean = false;

  constructor(
    @Inject('GlobalSystemMessage') private _globalSystemMessage: SystemMessageService,
    private _router: Router,
    private _salesRefund: SalesRefundService,
    private _salesRefundRest: SalesRefundRestService,
    public _systemMessage: SystemMessageService,
    private _translate: TranslateService
  ) { }

  ngOnInit() {
    this.form = new FormGroup({});

    this._salesRefund.setFormDefinitions(this.form);

    if (this.sourceTransaction) {
      const formGroupLine = this._salesRefund.buildFormChildLine();
      formGroupLine.patchValue({
        transaction: this.sourceTransaction,
        transactionType: this.sourceTransaction.transactionType,
        transactionId: this.sourceTransaction.id,
      });

      if (this.sourceTransaction.customer) {
        this.form.patchValue({
          customer: this.sourceTransaction.customer,
          customerId: this.sourceTransaction.customerId,
        });
      }

      (<FormArray>this.form.get('lines')).push(formGroupLine);
    }
  }

  onSubmit() {
    if (this.formValid()) {
      this.showLoader = true;
      this.save();
    }
  }

  formValid(): boolean {
    if (!this.form.valid) {
      CommonService.markAsDirty(this.form);
      this._systemMessage.log({
        message: this._translate.instant('error.form'),
        type: 'error'
      });
      return false;
    }
    return true;
  }

  save(): void {
    const refund: ISalesRefund = this.form.value;
    this._salesRefund.normalizeDoc(refund);

    this._salesRefundRest.create(refund)
      .catch(error => {
        this.showLoader = false;
        this._systemMessage.log({
          message: error,
          type: 'error'
        });
        return Observable.throw(error);
      })
      .subscribe(record => {
        this.showLoader = false;
        this._globalSystemMessage.log({
          message: this._translate.instant('success.salesRefund.quickCreate'),
          type: 'success',
          showAs: 'growl',
          showSnackBar: false,
        });

        this.afterSubmit.emit(record);
        this.form.reset();
      });
  }
}

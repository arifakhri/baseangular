import { AutoCompleteModule } from 'primeng/components/autocomplete/autocomplete';
import { CalendarModule } from 'primeng/components/calendar/calendar';
import { DataTableModule } from 'primeng/components/datatable/datatable';
import { ListboxModule } from 'primeng/components/listbox/listbox';
import { BsDropdownModule, ModalModule, PaginationModule, PopoverModule } from 'ngx-bootstrap';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { PurchasesRefundQuickCreateComponent } from './quick-create/purchases-refund-quick-create.component';
import { PurchasesRefundQuickFormComponent } from './quick-form/purchases-refund-quick-form.component';
import { PurchasesRefundCreateComponent } from './create/purchases-refund-create.component';
import { PurchasesRefundDetailComponent } from './detail/purchases-refund-detail.component';
import { PurchasesRefundFormComponent } from './form/purchases-refund-form.component';
import { PurchasesRefundUpdateComponent } from './update/purchases-refund-update.component';

import { PurchasesRefundRestService } from './purchases-refund-rest.service';
import { PurchasesRefundService } from './purchases-refund.service';

import { BusinessPartnerModule } from '../business-partner/business-partner.module';
import { CoreModule } from '../../core/core.module';
import { PaymentMethodModule } from '../payment-method/payment-method.module';
import { PurchasesModule } from '../purchases/purchases.module';
import { SharedModule } from '../../shared/shared.module';
import { TransactionModule } from '../transaction/transaction.module';

export const PROVIDERS = [
  PurchasesRefundRestService,
  PurchasesRefundService,
];

@NgModule({
  imports: [
    AutoCompleteModule,
    BsDropdownModule,
    BusinessPartnerModule,
    CalendarModule,
    CommonModule,
    CoreModule,
    DataTableModule,
    FlexLayoutModule,
    FormsModule,
    HttpModule,
    ListboxModule,
    ModalModule,
    PaginationModule,
    PopoverModule,
    PaymentMethodModule,
    PurchasesModule,
    ReactiveFormsModule,
    RouterModule,
    SharedModule,
    TransactionModule,
    TranslateModule,
  ],
  declarations: [
    PurchasesRefundQuickCreateComponent,
    PurchasesRefundQuickFormComponent,
    PurchasesRefundCreateComponent,
    PurchasesRefundDetailComponent,
    PurchasesRefundFormComponent,

    PurchasesRefundUpdateComponent,
  ],
  exports: [
    PurchasesRefundQuickCreateComponent
  ]
})
export class PurchasesRefundModule { }

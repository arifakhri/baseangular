import { NgModule } from '@angular/core';

import { PurchasesRefundModule } from './purchases-refund.module';
import { PurchasesRefundRoutingModule } from './purchases-refund-routing.module';

@NgModule({
  imports: [
    PurchasesRefundModule,
    PurchasesRefundRoutingModule
  ]
})
export class PurchasesRefundLazyModule { }

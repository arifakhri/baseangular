import * as _ from 'lodash';
import { Component, OnChanges, SimpleChanges, ViewChild } from '@angular/core';
import { FormArray } from '@angular/forms';
import { PopoverDirective } from 'ngx-bootstrap';
import 'app/rxjs-imports.ts';

import { TransactionFormPurchasesBComponent } from '../../transaction/transaction-form-purchases.bcomponent';

import { AccountingService, AutocompleteService } from '../../../core/core.module';
import { PurchasesDebitNoteRestService } from '../../purchases-debit-note/purchases-debit-note-rest.service';
import { PurchasesRefundRestService } from '../purchases-refund-rest.service';
import { PurchasesRefundService } from '../purchases-refund.service';

@Component({
  selector: 'app-purchases-refund-form',
  templateUrl: 'purchases-refund-form.component.html'
})
export class PurchasesRefundFormComponent extends TransactionFormPurchasesBComponent implements OnChanges {
  @ViewChild('popoverFindByInvoice') elPopoverFindByInvoice: PopoverDirective;

  constructor(
    public _autocomplete: AutocompleteService,
    public _accounting: AccountingService,
    private _purchasesDebitNoteRest: PurchasesDebitNoteRestService,
    private _purchasesRefund: PurchasesRefundService,
    private _purchasesRefundRest: PurchasesRefundRestService,
  ) {
    super();
    super.init();
    this.componentId = 'PurchasesRefundForm';

    this.registerHook('loadRelated', event => {
      return this._purchasesRefundRest.loadRelatedData();
    });

    this.registerHook('patchForm', event => {
      this.applyDoc();
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (_.has(changes, 'doc') && this.doc) {
      this.applyDoc();
    }
  }

  applyDoc() {
    this.applyInvoices(this.doc.lines);
  }

  onInvoiceFound(invoice: ISalesInvoice) {
    this.onVendorSelected(<any>invoice.customer);
    this.elPopoverFindByInvoice.hide();
  }

  onVendorClear() {
    this.form.get('vendorId').reset();
    this.clearInvoices();
  }

  onVendorSelected(vendor: IBusinessPartner) {
    this.form.get('vendor').setValue(vendor);
    this.form.get('vendorId').setValue(vendor.id);

    this.populateInvoices();
  }

  populateInvoices() {
    this._purchasesDebitNoteRest.findAll(
      {
        filter: [],
        sort: [{
          field: 'transactionNumber',
          dir: 'asc'
        }],
        take: 30,
        skip: 0,
        includeTotalCount: false
      },
      {
        vendorId: this.form.get('vendorId').value,
        isOpen: true,
      }
    ).subscribe(invoices => {
      this.applyInvoices(invoices.data);
    });
  }

  clearInvoices() {
    const formLines = (<FormArray>this.form.get('lines'));
    while (formLines.length) {
      formLines.removeAt(0);
    }
  }

  applyInvoices(invoices) {
    this.clearInvoices();

    invoices.forEach(transaction => {
      const lineFormGroup = this._purchasesRefund.buildFormChildLine();
      lineFormGroup.patchValue(transaction);

      lineFormGroup.get('transaction').setValue(transaction.transaction || transaction);
      lineFormGroup.get('transactionType').setValue(transaction.transactionType);
      lineFormGroup.get('transactionId').setValue(_.get(transaction, 'transaction.id', transaction.id || null));

      if (transaction.refundAmount) {
        lineFormGroup.get('refundAmount').setValue(transaction.refundAmount);
      }

      (<FormArray>this.form.get('lines')).push(lineFormGroup);
    });
  }

  syncAmountByLinePayment() {
    let totalAmount = 0;
    (<FormArray>this.form.get('lines')).controls.forEach(line => {
      const linePaymentValue = line.get('refundAmount').value || 0;
      totalAmount += linePaymentValue;
    });

    this.form.get('paymentAmount').setValue(totalAmount);
  }

  syncTotal() {
    this.syncAmountByLinePayment();
  }
}

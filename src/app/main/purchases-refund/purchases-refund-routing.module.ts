import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PurchasesRefundCreateComponent } from './create/purchases-refund-create.component';
import { PurchasesRefundDetailComponent } from './detail/purchases-refund-detail.component';
import { PurchasesRefundUpdateComponent } from './update/purchases-refund-update.component';

import { AuthorizationService } from '../../core/core.module';

export const routes: Routes = [{
  path: '',
  children: [{
    path: 'create',
    component: PurchasesRefundCreateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'purchasesPayment.create'
    }
  }, {
    path: ':id',
    component: PurchasesRefundDetailComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'purchasesPayment.detail'
    }
  }, {
    path: ':id/update',
    component: PurchasesRefundUpdateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'purchasesPayment.update'
    }
  }]
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class PurchasesRefundRoutingModule { }

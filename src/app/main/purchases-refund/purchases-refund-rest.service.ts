import { Injectable } from '@angular/core';
import 'app/rxjs-imports.ts';

import { ApiQueryOption, RequestService } from '../../core/core.module';

import { APP_CONST } from '../../app.const';

@Injectable()
export class PurchasesRefundRestService {
  baseURL = `${APP_CONST.API_MAIN}/purchases/all-refunds`;
  baseURLPayment = `${APP_CONST.API_MAIN}/purchases/refunds`;

  request = this._request.new(this.baseURL);
  requestPayment = this._request.new(this.baseURLPayment);

  constructor(
    private _request: RequestService,
  ) { }

  create(refund: IPurchasesRefund) {
    return this.requestPayment.post<IPurchasesRefund>(``, refund);
  }

  findAll(queryOption: ApiQueryOption, queryParams: any = {}) {
    return this.request.post<IApiPaginationResult<IPurchasesRefund>>(`q`, queryOption);
  }

  load(refundId: string, queryParams: any = {}) {
    return this.requestPayment.get<IPurchasesRefund>(`${refundId}`, { params: queryParams });
  }

  loadRelatedData() {
    return this.requestPayment.get<{
      paymentMethods: IPaymentMethod[],
      paymentAccounts: IAccount[],
      settings: ISettings,
    }>(`entry-related-data`);
  }

  update(refundId: string, updateObj: IPurchasesRefund) {
    return this.requestPayment.put<IPurchasesRefund>(`${refundId}`, updateObj);
  }

  void(refundId: string) {
    return this.requestPayment.put<IPurchasesRefund>(`${refundId}/void`, {});
  }
}

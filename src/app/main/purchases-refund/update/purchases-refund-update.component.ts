import * as _ from 'lodash';
import * as moment from 'moment';
import { Component, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';

import { PurchasesRefundRestService } from '../purchases-refund-rest.service';
import { PurchasesRefundService } from '../purchases-refund.service';

import { BaseUpdateBComponent } from '../../../shared/base/base.module';

import { MPurchasesRefund } from '../purchases-refund.model';

@Component({
  selector: 'app-purchases-refund-update',
  templateUrl: 'purchases-refund-update.component.html',
})
export class PurchasesRefundUpdateComponent extends BaseUpdateBComponent {
  @ViewChild('reportModal') elReportModal: ModalDirective;

  constructor(
    private _purchasesRefund: PurchasesRefundService,
    private _purchasesRefundRest: PurchasesRefundRestService,
  ) {
    super();

    this.componentId = 'PurchasesRefundUpdate';
    this.headerTitle = 'ui.purchasesRefund.update.title';
    this.containerType = 1;
    this.routeURL = '/purchases/payments';
    this.entrySuccessI18n = 'success.purchasesRefund.update';

    this.autoprintButtons = [{
      checkboxConfigId: 'autoprint.srefund.update',
      checkboxLabel: 'ui.purchasesRefund.update.action.apCheckbox.report',
      previewLabel: 'ui.purchasesRefund.update.action.previewReport',
      onPreview: () => {
        this.saveAndPrint();
      }
    }];

    this.registerHook('buildForm', event => {
      this._purchasesRefund.setFormDefinitions(this.form);
    });

    this.registerHook('load', event => {
      return this._purchasesRefundRest.load(this.page.routeParams.id, { includeLines: true }).switchMap(doc => {
        if (doc.transactionDate) {
          doc.transactionDate = moment(doc.transactionDate).toDate();
        }
        return Observable.of(doc);
      });
    });

    this.registerHook('save', event => {
      let refund = new MPurchasesRefund;

      refund = _.assign(refund, this.doc, this.form.value);
      this._purchasesRefund.normalizeDoc(refund);

      return this._purchasesRefundRest.update(this.page.routeParams.id, refund);
    });
  }

  async saveAndPrint() {
    try {
      const submitted = await this.onSubmit({ saveConfirm: true, print: true });
      if (submitted) {
        this._purchasesRefundRest.loadRelatedData().subscribe(related => {
          this.doc['companyInfo'] = related.settings.companyInfo;
          this.elReportModal.show();
        });
      }
    } catch (e) {

    }
  }
}

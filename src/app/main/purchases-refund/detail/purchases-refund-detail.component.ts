import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { AccountingService } from '../../../core/core.module';
import { AdminLayoutService } from '../../../layouts/layouts.module';
import { PurchasesRefundRestService } from '../purchases-refund-rest.service';

@Component({
  selector: 'app-purchases-refund-detail',
  templateUrl: 'purchases-refund-detail.component.html'
})
export class PurchasesRefundDetailComponent implements OnInit {
  doc: IPurchasesRefund;
  routeParams: any;

  constructor(
    public _accounting: AccountingService,
    private _adminLayout: AdminLayoutService,
    private _route: ActivatedRoute,
    private _purchasesPaymentRest: PurchasesRefundRestService,
  ) {
    this._adminLayout.containerType = 1;
    this._route.params.subscribe(params => this.routeParams = params);
  }

  ngOnInit() {
    this.loadData();
  }

  loadData() {
    this._purchasesPaymentRest.load(this.routeParams.id, { includeLines: true }).subscribe(receipt => {
      this.doc = receipt;
    });
  }
}

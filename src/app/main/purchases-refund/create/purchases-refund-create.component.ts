import * as _ from 'lodash';
import { Component, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import 'app/rxjs-imports.ts';

import { BaseCreateBComponent } from '../../../shared/base/base.module';

import { PurchasesRefundRestService } from '../purchases-refund-rest.service';
import { PurchasesRefundService } from '../purchases-refund.service';

import { MPurchasesRefund } from '../purchases-refund.model';

@Component({
  selector: 'app-purchases-refund-create',
  templateUrl: 'purchases-refund-create.component.html',
})
export class PurchasesRefundCreateComponent extends BaseCreateBComponent {
  @ViewChild('reportModal') elReportModal: ModalDirective;

  constructor(
    private _purchasesRefund: PurchasesRefundService,
    private _purchasesRefundRest: PurchasesRefundRestService,
  ) {
    super();

    this.componentId = 'PurchasesRefundCreate';
    this.headerTitle = 'ui.purchasesRefund.create.title';
    this.containerType = 1;
    this.routeURL = '/purchases/payments';
    this.entrySuccessI18n = 'success.purchasesRefund.create';

    this.autoprintButtons = [{
      checkboxConfigId: 'autoprint.srefund.create',
      checkboxLabel: 'ui.purchasesRefund.create.action.apCheckbox.report',
      previewLabel: 'ui.purchasesRefund.create.action.previewReport',
      onPreview: () => {
        this.saveAndPrint();
      }
    }];

    this.registerHook('buildForm', event => {
      this._purchasesRefund.setFormDefinitions(this.form);
    });

    this.registerHook('save', event => {
      let refund = new MPurchasesRefund;

      refund = _.assign(refund, this.form.value);
      this._purchasesRefund.normalizeDoc(refund);

      return this._purchasesRefundRest.create(refund);
    });
  }

  async saveAndPrint() {
    try {
      const submitted = await this.onSubmit({ saveConfirm: true, print: true });
      if (submitted) {
        this._purchasesRefundRest.loadRelatedData().subscribe(related => {
          this.doc['companyInfo'] = related.settings.companyInfo;
          this.elReportModal.show();
        });
      }
    } catch (e) {

    }
  }
}

export class MPurchasesRefund {
  id: string;
  transactionType: string;
  transactionTypeGroup: string;
  transactionNumber: string;
  transactionDate: Date | string;
  vendorId: string;
  description: string;
  paymentAccountId: string;
  paymentMethodId: string;
  linePaymentTotal: number;
  paymentAmount: number;
  note: string;
  vendorNote: string;
  rowVersion: string;
  vendor: {
    salesTaxable: boolean;
    defaultSalesTaxId: string;
    defaultSalesPriceLevelId: string;
    defaultSalesInvoiceDueDays: number;
    purchaseTaxable: boolean;
    defaultPurchaseTaxId: string;
    defaultPurchaseInvoiceDueDays: number;
    isCustomer: boolean;
    isVendor: boolean;
    defaultSalesTax: {
      id: string;
      code: string;
      name: string;
      rate: number;
    };
    defaultSalesPriceLevel: {
      id: string;
      name: string;
      isMaster: boolean;
    };
    defaultPurchaseTax: {
      id: string;
      code: string;
      name: string;
      rate: number;
    };
    id: string;
    contactType: string;
    code: string;
    displayName: string;
    company: string;
    fullName: string;
    phone: string;
    mobile: string;
    fax: string;
    email: string;
    website: string;
    inactive: boolean;
    postalAddress: {
      attention: string;
      street1: string;
      street2: string;
      city: string;
      stateProvince: string;
      countryId: string;
      postalCode: string;
      longitude: number;
      latitude: number;
      country: {
        code: string;
        name: string;
      }
    }
  };
  paymentAccount: {
    id: string;
    accountClassId: string;
    accountTypeId: string;
    code: string;
    name: string;
    description: string;
    isChildAccount: boolean;
    level: number;
    locked: boolean;
    systemType: string;
    inactive: boolean;
  };
  paymentMethod: {
    id: string;
    paymentMethodType: string;
    name: string;
    systemType: string;
  };
  lines: [
    {
      id: string;
      paymentId: string;
      transactionType: string;
      transactionId: string;
      refundAmount: number;
      sortOrder: number;
      rowVersion: string;
      transaction: {
        id: string;
        transactionType: string;
        transactionNumber: string;
        transactionDate: string;
        vendorId: string;
        vendorRefNumber: string;
        billingAddress: string;
        shippingAddress: string;
        total: number;
        status: string;
        state: string;
        vendor: {
          salesTaxable: boolean;
          defaultSalesTaxId: string;
          defaultSalesPriceLevelId: string;
          defaultSalesInvoiceDueDays: number;
          purchaseTaxable: boolean;
          defaultPurchaseTaxId: string;
          defaultPurchaseInvoiceDueDays: number;
          isCustomer: boolean;
          isVendor: boolean;
          defaultSalesTax: {
            id: string;
            code: string;
            name: string;
            rate: number;
          };
          defaultSalesPriceLevel: {
            id: string;
            name: string;
            isMaster: boolean;
          };
          defaultPurchaseTax: {
            id: string;
            code: string;
            name: string;
            rate: number;
          };
          id: string;
          contactType: string;
          code: string;
          displayName: string;
          company: string;
          fullName: string;
          phone: string;
          mobile: string;
          fax: string;
          email: string;
          website: string;
          inactive: boolean;
          postalAddress: {
            attention: string;
            street1: string;
            street2: string;
            city: string;
            stateProvince: string;
            countryId: string;
            postalCode: string;
            longitude: number;
            latitude: number;
            country: {
              code: string;
              name: string;
            }
          }
        }
      }
    }
  ]
}

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';

import { ApiQueryOption, RequestService } from '../../core/core.module';
import { TransactionRestService } from '../transaction/transaction-rest.service';

import { APP_CONST } from '../../app.const';

@Injectable()
export class BusinessPartnerRestService {
  baseURL = `${APP_CONST.API_MAIN}/contacts/business-partners`;
  baseURLPicker = `${APP_CONST.API_MAIN}/pickers/contacts`;

  request = this._request.new(this.baseURL);
  requestPicker = this._request.new(this.baseURLPicker);

  constructor(
    private _request: RequestService,
    private _transactionRest: TransactionRestService,
  ) { }

  create(businessPartner: IBusinessPartner) {
    return this.request.post<IBusinessPartner>(``, businessPartner);
  }

  findAll(queryOption: ApiQueryOption, queryParams: any = {}) {
    return this.request.post<IApiPaginationResult<IBusinessPartner>>(`q`, queryOption, { params: queryParams });
  }

  findAllCustomers(queryOption: ApiQueryOption, queryParams: any = {}): Observable<IApiPaginationResult<IBusinessPartner>> {
    queryParams.customer = true;
    queryParams.vendor = false;

    return this.findAll(queryOption, queryParams);
  }

  findAllCustomersPicker(queryOption: ApiQueryOption, queryParams: any = {}): Observable<IApiPaginationResult<IBusinessPartner>> {
    queryParams.customer = true;
    queryParams.vendor = false;
    queryParams.employee = false;

    return this.requestPicker.post<IApiPaginationResult<IBusinessPartner>>(``, queryOption, { params: queryParams });
  }

  findAllVendors(queryOption: ApiQueryOption, queryParams: any = {}): Observable<IApiPaginationResult<IBusinessPartner>> {
    queryParams.customer = false;
    queryParams.vendor = true;

    return this.findAll(queryOption, queryParams);
  }

  findAllVendorsPicker(queryOption: ApiQueryOption, queryParams: any = {}): Observable<IApiPaginationResult<IBusinessPartner>> {
    queryParams.customer = false;
    queryParams.vendor = true;
    queryParams.employee = false;

    return this.requestPicker.post<IApiPaginationResult<IBusinessPartner>>(``, queryOption, { params: queryParams });
  }

  findAllTransactions(queryOption: ApiQueryOption, queryParams: any = {}) {
    return this._transactionRest.findAll<IBusinessPartnerTransaction>(queryOption, queryParams);
  }

  load(businessPartnerId: string) {
    return this.request.get<IBusinessPartner>(`${businessPartnerId}`);
  }

  loadRelatedData() {
    return this.request.get<{ taxes: ITax[], countries: ICountry[], priceLevels: IPriceLevel[] }>(`entry-related-data`);
  }

  update(businessPartnerId: string, updateObj: IBusinessPartner) {
    return this.request.put<IBusinessPartner>(`${businessPartnerId}`, updateObj);
  }

  delete(businessPartnerId: string) {
    return this.request.delete<IBusinessPartner>(`${businessPartnerId}`);
  }

  checkDuplicate(
    values: {
      id?: string;
      displayName: string;
      firstName: string;
      middleName?: string;
      lastName?: string;
      phone?: string;
      mobile?: string;
      email?: string;
    }
  ) {
    return this.request.post<boolean>(`check-duplicate`, values);
  }

  getARCredits(businessPartnerId) {
    return this.request.get<any>(`${businessPartnerId}/open-ar-credits`)
      .map(response => response.data || []);
  }

  getAPCredits(businessPartnerId) {
    return this.request.get<any>(`${businessPartnerId}/open-ap-credits`)
      .map(response => response.data || []);
  }
}

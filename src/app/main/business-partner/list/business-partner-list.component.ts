import * as SnackBar from 'node-snackbar';
import swal from 'sweetalert2';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { DataTable } from 'primeng/components/datatable/datatable';
import { Observable, Subscription } from 'rxjs';
import 'app/rxjs-imports.ts';
import { TranslateService } from '@ngx-translate/core';

import { SpinnerDirective } from '../../../shared/spinner/spinner.directive';

import {
  AccountingService,
  ExportDataTableService,
  GridTableDataSource,
  GridTableService,
  GridTableToggleService,
} from '../../../core/core.module';
import { BusinessPartnerRestService } from '../business-partner-rest.service';
import { RetryDialogComponent } from '../../../shared/retry-dialog/retry-dialog.component';

@Component({
  selector: 'app-business-partner-list',
  templateUrl: 'business-partner-list.component.html'
})
export class BusinessPartnerListComponent implements OnInit {
  @ViewChild('gridTable') public gridTable: DataTable;
  @ViewChild('pageLoading') elPageLoading: SpinnerDirective;
  @ViewChild('retryDialog') elRetryDialog: RetryDialogComponent;

  compReady: boolean;
  type: string;
  businessPartners: IBusinessPartner[] = [];
  gridDataSource: GridTableDataSource<IBusinessPartner> = new GridTableDataSource<IBusinessPartner>();
  selectedRecords: IBusinessPartner[] = [];

  combinedFilterValues: string = '';
  combinedFilterFields = {
    displayName: true,
    phone: true,
    email: true,
  };

  tableColumns: IGridTableColumn[];
  tableColumnsToggle: any;

  retryDialogShownState: boolean = false;
  retryDialogOnRetry: Function;

  private loadDataTimeoutId: number;
  private gridFirstLoad: boolean = true;
  private subscriptions: { [key: string]: Subscription[] } = {
    load: []
  };

  constructor(
    private _accounting: AccountingService,
    private _activatedRoute: ActivatedRoute,
    private _businessPartnerRest: BusinessPartnerRestService,
    private _gridTable: GridTableService,
    private _dataTableExport: ExportDataTableService,
    private _gridTableToggle: GridTableToggleService,
    private _translate: TranslateService,
  ) {
    this._activatedRoute.data.subscribe(data => {
      this.type = data.type || 'customer';

      const tableColumns: IGridTableColumn[] = [{
        i18nLabel: 'ui.businessPartner.list.column.displayName',
        field: 'displayName',
        link: (row: IBusinessPartner) => {
          return `/contacts/business-partners/${row.id}`;
        },
        sort: true,
      }, {
        i18nLabel: 'ui.businessPartner.list.column.phone',
        field: 'phone',
        sort: true,
      }, {
        i18nLabel: 'ui.businessPartner.list.column.email',
        field: 'email',
        sort: true,
      }];

      tableColumns.push({
        i18nLabel: 'ui.businessPartner.list.column.balance',
        field: this.type === 'vendor' ? 'apBalance' : 'arBalance',
        sort: true,
        formatter: (value: any, row: IBusinessPartner) => {
          return this._accounting.ac.formatMoney(value, '');
        },
        columnClasses: 'right-align',
      });

      this.tableColumns = tableColumns;
      this.tableColumnsToggle = this._gridTableToggle.mapToggleOptionsFromColumns(this.tableColumns);
    });
  }

  ngOnInit() {
    this.loadData();
  }

  gridLoadDataWrapper() {
    // Grid always load at the first time. we dont want that. we already call on ngOnInit
    if (this.gridFirstLoad) {
      this.gridFirstLoad = false;
      return;
    }

    if (this.loadDataTimeoutId) {
      window.clearTimeout(this.loadDataTimeoutId);
      this.loadDataTimeoutId = null;
    }

    this.loadDataTimeoutId = window.setTimeout(() => {
      this.loadData();
    }, 300);
  }

  showDeleteDialog(businessPartnerId: string) {
    swal({
      title: this._translate.instant(`confirm.${this.type}.delete.label`),
      text: this._translate.instant(`confirm.${this.type}.delete.description`),
      type: 'question',
      showCancelButton: true,
    })
      .then(() => {
        this._businessPartnerRest.delete(businessPartnerId).subscribe(response => {
          this.gridLoadDataWrapper();
          SnackBar.show({
            text: this._translate.instant(`success.${this.type}.delete`),
            pos: 'bottom-right'
          });
        });
      })
      .catch(() => { });
  }

  loadData() {
    this.elPageLoading.forceShow();

    const qOption = this._gridTable.generateApiQueryOptionFromGridInfo(
      this.gridDataSource.pager,
      true,
      this.gridTable,
      this._gridTable.generateCombinedApiFilters(this.combinedFilterValues, this.combinedFilterFields)
    );

    const queryParams: any = {};
    switch (this.type) {
      case 'customer':
        queryParams.vendor = false;
        break;
      case 'vendor':
        queryParams.customer = false;
        break;
    }

    this.subscriptions.load.forEach(sub => sub.unsubscribe());
    this.subscriptions.load.push(
      this.elRetryDialog.createRetryEntry(
        this._businessPartnerRest.findAll(qOption, queryParams).finally(() => {
          this.elPageLoading.forceHide();
        })
      ).subscribe(response => {
        this.businessPartners = response.data;
        this.gridDataSource.updateFromApiPaginationResult(response);
        this.compReady = true;
      })
    );
  }

  get exportRecords(): Observable<IBusinessPartner[]> {
    const qOption = {
      ...this._gridTable.generateApiQueryOptionFromGridInfo(
        this.gridDataSource.pager,
        false,
        this.gridTable,
        this._gridTable.generateCombinedApiFilters(this.combinedFilterValues, this.combinedFilterFields)
      ),
      skip: 0,
      take: this._dataTableExport.exportLimit,
    };

    return this.elRetryDialog.createRetryEntry(
      this._businessPartnerRest.findAll(qOption).map(response => response.data)
    );
  }

  exportPDF() {
    this.exportRecords.subscribe(records => {
      this._dataTableExport.export({
        records,
        mapOptions: this._dataTableExport.mapFromTableColumns(this.tableColumns),
        fileName: 'business-partner',
        extension: 'pdf',
        templateData: {
          title: this._translate.instant('ui.contact.list.title'),
        },
      });
    });
  }

  exportExcel() {
    this.exportRecords.subscribe(records => {
      this._dataTableExport.export({
        records: records,
        mapOptions: this._dataTableExport.mapFromTableColumns(this.tableColumns),
        fileName: 'business-partner',
        extension: 'xls',
      });
    });
  }

  public onGridChange() {
    this.gridLoadDataWrapper();
  }

  public onGridPageChange() {
    this.gridLoadDataWrapper();
  }
}

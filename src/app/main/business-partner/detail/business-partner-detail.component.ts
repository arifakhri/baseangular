import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { BusinessPartnerRestService } from '../business-partner-rest.service';

@Component({
  selector: 'app-business-partner-detail',
  templateUrl: 'business-partner-detail.component.html'
})
export class BusinessPartnerDetailComponent implements OnInit {
  doc: IBusinessPartner;
  routeParams: any;
  type: string;
  moreInfoCollapsed: boolean = true;
  shown: string = 'transaction';

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _businessPartnerRest: BusinessPartnerRestService,
  ) {
    this._activatedRoute.params.subscribe(params => this.routeParams = params);
    this._activatedRoute.data.subscribe(data => this.type = data.type || 'customer');
  }

  ngOnInit() {
    this.loadData();
  }

  loadData() {
    this._businessPartnerRest.load(this.routeParams.id).subscribe(businessPartner => {
      this.doc = businessPartner;
    });
  }
}

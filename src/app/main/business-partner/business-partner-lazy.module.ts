import { NgModule } from '@angular/core';

import { BusinessPartnerModule } from './business-partner.module';
import { BusinessPartnerRoutingModule } from './business-partner-routing.module';

@NgModule({
  imports: [
    BusinessPartnerModule,
    BusinessPartnerRoutingModule
  ]
})
export class BusinessPartnerLazyModule { }

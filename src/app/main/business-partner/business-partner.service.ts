import { CustomValidators } from 'ng2-validation';
import { Injectable } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Injectable()
export class BusinessPartnerService {
  setFormDefinitions(form: FormGroup, type) {
    const firstNameControl = new FormControl('', Validators.maxLength(30));
    (<any>firstNameControl).validatorData = {
      maxLength: 30
    };
    const lastNameControl = new FormControl('', Validators.maxLength(30));
    (<any>lastNameControl).validatorData = {
      maxLength: 30
    };
    const companyControl = new FormControl('', Validators.maxLength(50));
    (<any>companyControl).validatorData = {
      maxLength: 50
    };
    const displayNameControl = new FormControl('', [Validators.required, Validators.maxLength(150)]);
    (<any>displayNameControl).validatorData = {
      maxLength: 150
    };
    const emailControl = new FormControl('', [CustomValidators.email, Validators.maxLength(150)]);
    (<any>emailControl).validatorData = {
      maxLength: 150
    };
    const phoneControl = new FormControl('', [CustomValidators.digits, Validators.maxLength(30)]);
    (<any>phoneControl).validatorData = {
      maxLength: 30
    };
    const mobileControl = new FormControl('', [CustomValidators.digits, Validators.maxLength(30)]);
    (<any>mobileControl).validatorData = {
      maxLength: 30
    };
    const faxControl = new FormControl('', [CustomValidators.digits, Validators.maxLength(30)]);
    (<any>faxControl).validatorData = {
      maxLength: 30
    };
    const websiteControl = new FormControl('', [
      Validators.pattern(
        '^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$'
      ),
      Validators.maxLength(150)
    ]);
    (<any>websiteControl).validatorData = {
      maxLength: 150,
      patternType: 'url'
    };
    const bankAccountNoControl = new FormControl('', Validators.maxLength(30));
    (<any>bankAccountNoControl).validatorData = {
      maxLength: 30
    };
    const bankAccountNameControl = new FormControl('', Validators.maxLength(50));
    (<any>bankAccountNameControl).validatorData = {
      maxLength: 50
    };
    const taxRegistrationNumberControl = new FormControl('', Validators.maxLength(30));
    (<any>taxRegistrationNumberControl).validatorData = {
      maxLength: 30
    };

    form.addControl('isCustomer', new FormControl(type === 'customer'));
    form.addControl('isVendor', new FormControl(type === 'vendor'));
    form.addControl('firstName', firstNameControl);
    form.addControl('lastName', lastNameControl);
    form.addControl('company', companyControl);
    form.addControl('displayName', displayNameControl);
    form.addControl('dateOfBirth', new FormControl(''));
    form.addControl('email', emailControl);
    form.addControl('phone', phoneControl);
    form.addControl('mobile', mobileControl);
    form.addControl('fax', faxControl);
    form.addControl('website', websiteControl);
    form.addControl('postalAddress', this.buildFormChildAddress());
    form.addControl('shippingAddress', this.buildFormChildAddress());
    form.addControl('shippingAddressSameAsPostal', new FormControl(true));
    form.addControl('defaultSalesInvoiceDueDays', new FormControl('', CustomValidators.digits));
    form.addControl('salesTaxable', new FormControl(false));
    form.addControl('defaultSalesTax', new FormControl);
    form.addControl('defaultSalesTaxId', new FormControl(null));
    form.addControl('defaultSalesPriceLevel', new FormControl);
    form.addControl('defaultSalesPriceLevelId', new FormControl(null));
    form.addControl('defaultPurchaseInvoiceDueDays', new FormControl('', CustomValidators.digits));
    form.addControl('purchaseTaxable', new FormControl(false));
    form.addControl('defaultPurchaseTax', new FormControl);
    form.addControl('defaultPurchaseTaxId', new FormControl(null));
    form.addControl('bankAccountNo', bankAccountNoControl);
    form.addControl('bankAccountName', bankAccountNameControl);
    form.addControl('taxRegistrationNumber', taxRegistrationNumberControl);
    form.addControl('note', new FormControl(''));

    form.controls.postalAddress.valueChanges.subscribe(values => {
      if (form.controls.shippingAddressSameAsPostal.value) {
        form.controls.shippingAddress.patchValue(values);
      }
    });
    form.controls.shippingAddressSameAsPostal.valueChanges.subscribe(same => {
      if (same) {
        form.controls.shippingAddress.patchValue(form.controls.postalAddress.value);
      } else {
        form.controls.shippingAddress.reset();
      }
    });
  }

  buildFormChildAddress() {
    const attentionControl = new FormControl('', Validators.maxLength(150));
    (<any>attentionControl).validatorData = {
      maxLength: 150
    };
    const cityControl = new FormControl('', Validators.maxLength(50));
    (<any>cityControl).validatorData = {
      maxLength: 50
    };
    const stateProvinceControl = new FormControl('', Validators.maxLength(50));
    (<any>stateProvinceControl).validatorData = {
      maxLength: 50
    };
    const countryIdControl = new FormControl(null, Validators.maxLength(5));
    (<any>countryIdControl).validatorData = {
      maxLength: 5
    };
    const postalCodeControl = new FormControl('', Validators.maxLength(10));
    (<any>postalCodeControl).validatorData = {
      maxLength: 10
    };

    return new FormGroup({
      attention: attentionControl,
      street1: new FormControl,
      street2: new FormControl,
      city: cityControl,
      stateProvince: stateProvinceControl,
      country: new FormControl(null),
      countryId: countryIdControl,
      postalCode: postalCodeControl,
    });
  }
}

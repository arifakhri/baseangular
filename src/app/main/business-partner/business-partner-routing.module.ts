import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { BusinessPartnerCreateComponent } from './create/business-partner-create.component';
import { BusinessPartnerDetailComponent } from './detail/business-partner-detail.component';
import { BusinessPartnerListComponent } from './list/business-partner-list.component';
import { BusinessPartnerUpdateComponent } from './update/business-partner-update.component';

import { AuthorizationService } from '../../core/core.module';

export const routes: Routes = [{
  path: '',
  children: [{
    path: 'business-partners',
    children: [{
      path: ':id',
      component: BusinessPartnerDetailComponent,
      canActivate: [AuthorizationService],
      data: {
        name: 'businessPartner.detail'
      }
    }, {
      path: ':id/update',
      component: BusinessPartnerUpdateComponent,
      canActivate: [AuthorizationService],
      data: {
        name: 'businessPartner.update'
      }
    }]
  }, {
    path: 'customers',
    children: [{
      path: '',
      component: BusinessPartnerListComponent,
      canActivate: [AuthorizationService],
      data: {
        name: 'businessPartner.customer.list',
        type: 'customer'
      }
    }, {
      path: 'create',
      component: BusinessPartnerCreateComponent,
      canActivate: [AuthorizationService],
      data: {
        name: 'businessPartner.customer.create',
        type: 'customer'
      }
    }]
  }, {
    path: 'vendors',
    children: [{
      path: '',
      component: BusinessPartnerListComponent,
      canActivate: [AuthorizationService],
      data: {
        name: 'businessPartner.vendor.list',
        type: 'vendor'
      }
    }, {
      path: 'create',
      component: BusinessPartnerCreateComponent,
      canActivate: [AuthorizationService],
      data: {
        name: 'businessPartner.vendor.create',
        type: 'vendor'
      }
    }]
  }]
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class BusinessPartnerRoutingModule {}

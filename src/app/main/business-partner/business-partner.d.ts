declare interface IAddress {
  attention: string;
  street1: string;
  street2: string;
  city: string;
  stateProvince: string;
  countryId: string;
  postalCode: string;
  longitude: number;
  latitude: number;
}

declare interface IBusinessPartner {
  id: string;
  code: string;
  name: string;
  displayName: string;
  company: string;
  title: string;
  firstName: string;
  middleName: string;
  lastName: string;
  phone: string;
  mobile: string;
  fax: string;
  email: string;
  website: string;
  dateOfBirth: string | Date;
  gender: string;
  pictureId: string;
  note: string;
  shippingAddressSameAsPostal: boolean;
  salesTaxable: boolean;
  defaultSalesTaxId: string;
  defaultSalesInvoiceDueDays: number;
  purchaseTaxable: boolean;
  defaultPurchaseTaxId: string;
  defaultPurchaseInvoiceDueDays: number;
  isCustomer: boolean;
  isVendor: boolean;
  postalAddress: IAddress;
  billingAddress: IAddress;
  shippingAddress: IAddress;
  arBalance: number;
  apBalance: number;
}

declare interface IBusinessPartnerAPCredit {
  branchId: number;
  transactionType: string;
  transactionId: string;
  transactionNumber: string;
  customerRefNumber: string;
  orderId: string;
  orderNumber: string;
  orderTotal: number;
  amount: number;
  allocated: number;
  remaining: number;
}

declare interface IBusinessPartnerARCredit {
  branchId: number;
  transactionType: string;
  transactionId: string;
  transactionNumber: string;
  customerRefNumber: string;
  orderId: string;
  orderNumber: string;
  orderTotal: number;
  amount: number;
  allocated: number;
  remaining: number;
}

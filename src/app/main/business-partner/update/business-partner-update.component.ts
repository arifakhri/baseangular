import * as _ from 'lodash';
import swal from 'sweetalert2';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';
import { TranslateService } from '@ngx-translate/core';

import { CommonService, SystemMessageService, UserVariableService } from '../../../core/core.module';
import { SpinnerService } from '../../../shared/spinner/spinner.service';
import { BusinessPartnerRestService } from '../business-partner-rest.service';
import { BusinessPartnerService } from '../business-partner.service';
import { RetryDialogComponent } from '../../../shared/retry-dialog/retry-dialog.component';

@Component({
  selector: 'app-business-partner-update',
  templateUrl: 'business-partner-update.component.html',
  providers: [SystemMessageService]
})
export class BusinessPartnerUpdateComponent implements OnInit {
  @ViewChild('retryDialog') elRetryDialog: RetryDialogComponent;

  compReady: any = {};
  doc: IBusinessPartner;
  form: FormGroup = new FormGroup({});
  fullView: boolean = false;
  fullViewState: IAppVariable = this._userVariable.get('simpledetailstate.contact', {
    value: false,
  });
  routeParams: any;
  type: string;

  formNavigations: IFormNav[] = [];

  retryDialogShownState: boolean = false;
  retryDialogOnRetry: Function;

  constructor(
    @Inject('GlobalSystemMessage') private _globalSystemMessage: SystemMessageService,
    private _activatedRoute: ActivatedRoute,
    private _spinner: SpinnerService,
    private _userVariable: UserVariableService,
    private _businessPartner: BusinessPartnerService,
    private _businessPartnerRest: BusinessPartnerRestService,
    private _router: Router,
    public _systemMessage: SystemMessageService,
    private _translate: TranslateService,
  ) {
    this._activatedRoute.params.subscribe(params => this.routeParams = params);

    this.defineFormNavigations();
    this._translate.onLangChange.subscribe(() => {
      this.defineFormNavigations();
    });

    this.fullView = this.fullViewState.value;
  }

  defineFormNavigations() {
    this.formNavigations = [{
      name: this._translate.instant('ui.businessPartner.form.personalDetails'),
      target: '#fieldset-1'
    }, {
      name: this._translate.instant('ui.businessPartner.form.address'),
      target: '#fieldset-2'
    }, {
      name: this._translate.instant('ui.businessPartner.form.salesInfo'),
      target: '#fieldset-3'
    }, {
      name: this._translate.instant('ui.businessPartner.form.purchasesInfo'),
      target: '#fieldset-4'
    }, {
      name: this._translate.instant('ui.businessPartner.form.paymentInfo'),
      target: '#fieldset-5'
    }, {
      name: this._translate.instant('ui.businessPartner.form.taxInfo'),
      target: '#fieldset-6'
    }, {
      name: this._translate.instant('ui.businessPartner.form.note'),
      target: '#fieldset-7'
    }];
  }

  ngOnInit() {
    this._businessPartner.setFormDefinitions(this.form, this.type);

    this.loadData();
  }

  onFullViewStateChange() {
    this.compReady.form = false;

    this.fullViewState.value = this.fullView;
    this._userVariable.update();
  }

  onSubmit({ saveAndNew, saveAndView }: any = {}): void {
    if (this.formValid()) {
      this._businessPartnerRest.checkDuplicate(
        Object.assign(
          {},
          { id: this.doc.id },
          <any>_.pick(this.form.value, ['phone', 'mobile', 'email', 'displayName', 'firstName', 'middleName', 'lastName'])
        )
      ).subscribe(exist => {
        if (exist) {
          swal({
            title: this._translate.instant(`confirm.${this.type}.exist.label`),
            text: this._translate.instant(`confirm.${this.type}.exist.description`),
            type: 'warning',
            showCancelButton: true
          }).then(() => {
            this.save().subscribe(result => {
              if (saveAndNew) {
                this._router.navigateByUrl(`/contacts/${this.type}s/create`);
              } else if (saveAndView) {
                this._router.navigate([`/contacts/business-partners`, result.id]);
              } else {
                this._router.navigateByUrl(`/contacts/${this.type}s`);
              }
            });
          }).catch();
        } else {
          this.save().subscribe(result => {
            if (saveAndNew) {
              this._router.navigateByUrl(`/contacts/${this.type}s/create`);
            } else if (saveAndView) {
              this._router.navigate([`/contacts/business-partners`, result.id]);
            } else {
              this._router.navigateByUrl(`/contacts/${this.type}s`);
            }
          });
        }
      });
    }
  }

  loadData() {
    this.elRetryDialog.createRetryEntry(this._businessPartnerRest.load(this.routeParams.id))
      .subscribe(businessPartner => {
        this.doc = businessPartner;
        this.type = this.doc.isCustomer ? 'customer' : 'vendor';

        this.compReady.self = true;
      });
  }

  formValid(): boolean {
    if (!this.form.valid) {
      CommonService.markAsDirty(this.form);
      this._systemMessage.log({
        message: this._translate.instant('error.form'),
        type: 'error'
      });
      return false;
    }
    return true;
  }

  save() {
    const spinner = this._spinner.showDefault();

    const businessPartner: IBusinessPartner = Object.assign({}, this.doc, this.form.value);
    return this._businessPartnerRest.update(this.routeParams.id, businessPartner)
      .catch(error => {
        spinner.dispose();

        this._systemMessage.log({
          message: error,
          type: 'error'
        });
        return Observable.throw(error);
      })
      .do(result => {
        spinner.dispose();

        this._globalSystemMessage.log({
          message: this._translate.instant(`success.${this.type}.update`),
          type: 'success',
          showAs: 'growl',
          showSnackBar: false,
        });
      });
  }
}

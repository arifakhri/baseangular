import { AutoCompleteModule } from 'primeng/components/autocomplete/autocomplete';
import { CalendarModule } from 'primeng/components/calendar/calendar';
import { CheckboxModule } from 'primeng/components/checkbox/checkbox';
import { DataTableModule } from 'primeng/components/datatable/datatable';
import { DropdownModule } from 'primeng/components/dropdown/dropdown';
import { InputSwitchModule } from 'primeng/components/inputswitch/inputswitch';
import { ListboxModule } from 'primeng/components/listbox/listbox';
import { BsDropdownModule, PaginationModule } from 'ngx-bootstrap';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { BusinessPartnerCreateComponent } from './create/business-partner-create.component';
import { BusinessPartnerDetailComponent } from './detail/business-partner-detail.component';
import { BusinessPartnerFormComponent } from './form/business-partner-form.component';
import { BusinessPartnerFormMiniComponent } from './form/mini/business-partner-form-mini.component';
import { BusinessPartnertFormFieldBankAccNameComponent } from './form/field-bankaccname/business-partner-form-field-bankaccname.component';
import { BusinessPartnertFormFieldBankAccNoComponent } from './form/field-bankaccno/business-partner-form-field-bankaccno.component';
import { BusinessPartnertFormFieldBillingAsShippingComponent } from './form/field-billingasshipping/business-partner-form-field-billingasshipping.component';
import { BusinessPartnertFormFieldCityComponent } from './form/field-city/business-partner-form-field-city.component';
import { BusinessPartnertFormFieldCompanyComponent } from './form/field-company/business-partner-form-field-company.component';
import { BusinessPartnertFormFieldCountryComponent } from './form/field-country/business-partner-form-field-country.component';
import { BusinessPartnertFormFieldDateOfBirthComponent } from './form/field-dateofbirth/business-partner-form-field-dateofbirth.component';
import { BusinessPartnertFormFieldDisplayNameComponent } from './form/field-displayname/business-partner-form-field-displayname.component';
import { BusinessPartnertFormFieldEmailComponent } from './form/field-email/business-partner-form-field-email.component';
import { BusinessPartnertFormFieldFaxComponent } from './form/field-fax/business-partner-form-field-fax.component';
import { BusinessPartnertFormFieldFirstNameComponent } from './form/field-firstname/business-partner-form-field-firstname.component';
import { BusinessPartnertFormFieldIsCustomerComponent } from './form/field-iscustomer/business-partner-form-field-iscustomer.component';
import { BusinessPartnertFormFieldIsVendorComponent } from './form/field-isvendor/business-partner-form-field-isvendor.component';
import { BusinessPartnertFormFieldLastNameComponent } from './form/field-lastname/business-partner-form-field-lastname.component';
import { BusinessPartnertFormFieldMobileComponent } from './form/field-mobile/business-partner-form-field-mobile.component';
import { BusinessPartnertFormFieldNoteComponent } from './form/field-note/business-partner-form-field-note.component';
import { BusinessPartnertFormFieldPhoneComponent } from './form/field-phone/business-partner-form-field-phone.component';
import { BusinessPartnertFormFieldPostalCodeComponent } from './form/field-postalcode/business-partner-form-field-postalcode.component';
import { BusinessPartnertFormFieldPriceLevelComponent } from './form/field-pricelevel/business-partner-form-field-pricelevel.component';
import { BusinessPartnertFormFieldPurchaseDueComponent } from './form/field-purchasedue/business-partner-form-field-purchasedue.component';
import { BusinessPartnertFormFieldPurchaseTaxComponent } from './form/field-purchasetax/business-partner-form-field-purchasetax.component';
import { BusinessPartnertFormFieldPurchaseTaxableComponent } from './form/field-purchasetaxable/business-partner-form-field-purchasetaxable.component';
import { BusinessPartnertFormFieldSalesDueComponent } from './form/field-salesdue/business-partner-form-field-salesdue.component';
import { BusinessPartnertFormFieldSalesTaxComponent } from './form/field-salestax/business-partner-form-field-salestax.component';
import { BusinessPartnertFormFieldSalesTaxableComponent } from './form/field-salestaxable/business-partner-form-field-salestaxable.component';
import { BusinessPartnertFormFieldStateComponent } from './form/field-state/business-partner-form-field-state.component';
import { BusinessPartnertFormFieldStreet1Component } from './form/field-street1/business-partner-form-field-street1.component';
import { BusinessPartnertFormFieldTaxRegNumComponent } from './form/field-taxregnum/business-partner-form-field-taxregnum.component';
import { BusinessPartnertFormFieldWebsiteComponent } from './form/field-website/business-partner-form-field-website.component';
import { BusinessPartnerListComponent } from './list/business-partner-list.component';
import { BusinessPartnerUpdateComponent } from './update/business-partner-update.component';
import { BusinessPartnerQuickCreateComponent } from './quick-create/business-partner-quick-create.component';
import { BusinessPartnerQuickFormComponent } from './quick-form/business-partner-quick-form.component';
import { BusinessPartnerTransactionComponent } from './transaction/business-partner-transaction.component';
import { BusinessPartnerTransactionInvoiceComponent } from './transaction-invoice/business-partner-transaction-invoice.component';

import { BusinessPartnerRestService } from './business-partner-rest.service';
import { BusinessPartnerService } from './business-partner.service';

import { ContactModule } from '../contact/contact.module';
import { CoreModule } from '../../core/core.module';
import { SharedModule } from '../../shared/shared.module';

export const PROVIDERS = [
  BusinessPartnerRestService,
  BusinessPartnerService,
];

@NgModule({
  imports: [
    CommonModule,
    FlexLayoutModule,
    PaginationModule,
    FormsModule,
    ListboxModule,
    BsDropdownModule,
    ReactiveFormsModule,
    RouterModule,
    TranslateModule,
    AutoCompleteModule,
    CalendarModule,
    CheckboxModule,
    DataTableModule,
    DropdownModule,
    InputSwitchModule,
    CoreModule,
    ContactModule,
    SharedModule,
  ],
  declarations: [
    BusinessPartnerCreateComponent,
    BusinessPartnerDetailComponent,
    BusinessPartnerFormComponent,
    BusinessPartnerFormMiniComponent,
    BusinessPartnertFormFieldBankAccNameComponent,
    BusinessPartnertFormFieldBankAccNoComponent,
    BusinessPartnertFormFieldBillingAsShippingComponent,
    BusinessPartnertFormFieldCityComponent,
    BusinessPartnertFormFieldCompanyComponent,
    BusinessPartnertFormFieldCountryComponent,
    BusinessPartnertFormFieldDateOfBirthComponent,
    BusinessPartnertFormFieldDisplayNameComponent,
    BusinessPartnertFormFieldEmailComponent,
    BusinessPartnertFormFieldFaxComponent,
    BusinessPartnertFormFieldFirstNameComponent,
    BusinessPartnertFormFieldIsCustomerComponent,
    BusinessPartnertFormFieldIsVendorComponent,
    BusinessPartnertFormFieldLastNameComponent,
    BusinessPartnertFormFieldMobileComponent,
    BusinessPartnertFormFieldNoteComponent,
    BusinessPartnertFormFieldPhoneComponent,
    BusinessPartnertFormFieldPostalCodeComponent,
    BusinessPartnertFormFieldPriceLevelComponent,
    BusinessPartnertFormFieldPurchaseDueComponent,
    BusinessPartnertFormFieldPurchaseTaxComponent,
    BusinessPartnertFormFieldPurchaseTaxableComponent,
    BusinessPartnertFormFieldSalesDueComponent,
    BusinessPartnertFormFieldSalesTaxComponent,
    BusinessPartnertFormFieldSalesTaxableComponent,
    BusinessPartnertFormFieldStateComponent,
    BusinessPartnertFormFieldStreet1Component,
    BusinessPartnertFormFieldTaxRegNumComponent,
    BusinessPartnertFormFieldWebsiteComponent,
    BusinessPartnerListComponent,
    BusinessPartnerUpdateComponent,
    BusinessPartnerQuickCreateComponent,
    BusinessPartnerQuickFormComponent,
    BusinessPartnerTransactionComponent,
    BusinessPartnerTransactionInvoiceComponent,
  ],
  exports: [
    BusinessPartnerQuickCreateComponent,
  ]
})
export class BusinessPartnerModule { }

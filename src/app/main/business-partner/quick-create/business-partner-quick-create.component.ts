import { Component, EventEmitter, Inject, Input, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

import { CommonService, SystemMessageService } from '../../../core/core.module';
import { BusinessPartnerRestService } from '../business-partner-rest.service';
import { BusinessPartnerService } from '../business-partner.service';

@Component({
  selector: 'app-business-partner-quick-create',
  templateUrl: 'business-partner-quick-create.component.html',
  providers: [SystemMessageService]
})
export class BusinessPartnerQuickCreateComponent implements OnInit {
  @Input() sourceTransaction: any;
  @Input() type: string;

  @Output() afterSubmit: EventEmitter<IProductCategory> = new EventEmitter();
  @Output() close: EventEmitter<boolean> = new EventEmitter();

  form: FormGroup = new FormGroup({});
  initialFormValues: any = {};
  showLoader: boolean = false;

  constructor(
    @Inject('GlobalSystemMessage') private _globalSystemMessage: SystemMessageService,
    private _businessPartner: BusinessPartnerService,
    private _businessPartnerRest: BusinessPartnerRestService,
    private _router: Router,
    public _systemMessage: SystemMessageService,
    private _translate: TranslateService
  ) { }

  ngOnInit() {
    this._businessPartner.setFormDefinitions(this.form, this.type);

    this.initialFormValues = this.form.value;
  }

  onSubmit() {
    if (this.formValid()) {
      this.showLoader = true;
      this.save();
    }
  }

  formValid(): boolean {
    if (!this.form.valid) {
      CommonService.markAsDirty(this.form);
      this._systemMessage.log({
        message: this._translate.instant('error.form'),
        type: 'error'
      });
      return false;
    }
    return true;
  }

  save(): void {
    const businessPartner: IBusinessPartner = this.form.value;
    this._businessPartnerRest.create(businessPartner)
      .catch(error => {
        this.showLoader = false;
        this._systemMessage.log({
          message: error,
          type: 'error'
        });
        return Observable.throw(error);
      })
      .subscribe(record => {
        this.showLoader = false;
        this._globalSystemMessage.log({
          message: this._translate.instant('success.businessPartner.quickCreate'),
          type: 'success',
          showAs: 'growl',
          showSnackBar: false,
        });

        this.form.reset(this.initialFormValues);
        this.afterSubmit.emit(record);
      });
  }
}

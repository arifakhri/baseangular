import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-business-partner-form-field-billingasshipping',
  templateUrl: 'business-partner-form-field-billingasshipping.component.html'
})
export class BusinessPartnertFormFieldBillingAsShippingComponent {
  @Input() label: boolean = true;
  @Input() parentForm: FormGroup;
}

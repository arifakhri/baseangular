import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-business-partner-form-field-website',
  templateUrl: 'business-partner-form-field-website.component.html'
})
export class BusinessPartnertFormFieldWebsiteComponent {
  @Input() label: boolean = true;
  @Input() parentForm: FormGroup;
}

import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-business-partner-form-field-taxregnum',
  templateUrl: 'business-partner-form-field-taxregnum.component.html'
})
export class BusinessPartnertFormFieldTaxRegNumComponent {
  @Input() label: boolean = true;
  @Input() parentForm: FormGroup;
}

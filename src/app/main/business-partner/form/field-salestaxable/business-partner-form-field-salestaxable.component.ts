import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-business-partner-form-field-salestaxable',
  templateUrl: 'business-partner-form-field-salestaxable.component.html'
})
export class BusinessPartnertFormFieldSalesTaxableComponent {
  @Input() label: boolean = true;
  @Input() parentForm: FormGroup;
}

import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-business-partner-form-field-mobile',
  templateUrl: 'business-partner-form-field-mobile.component.html'
})
export class BusinessPartnertFormFieldMobileComponent {
  @Input() label: boolean = true;
  @Input() parentForm: FormGroup;
}

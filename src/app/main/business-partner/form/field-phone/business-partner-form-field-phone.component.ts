import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-business-partner-form-field-phone',
  templateUrl: 'business-partner-form-field-phone.component.html'
})
export class BusinessPartnertFormFieldPhoneComponent {
  @Input() label: boolean = true;
  @Input() parentForm: FormGroup;
}

import * as _ from 'lodash';
import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';

import { CommonService } from '../../../../core/core.module';

@Component({
  selector: 'app-business-partner-form-field-displayname',
  templateUrl: 'business-partner-form-field-displayname.component.html'
})
export class BusinessPartnertFormFieldDisplayNameComponent implements OnInit {
  @Input() label: boolean = true;
  @Input() parentForm: FormGroup;

  searchACItems = CommonService.searchLocalACItems.bind(this);
  onACDropdown = CommonService.onLocalACDropdown.bind(this);

  displayNamesAvailable: string[] = [];
  displayNameSuggestions: string[] = [];

  ngOnInit() {
    Observable.merge(
      this.parentForm.get('firstName').valueChanges,
      this.parentForm.get('lastName').valueChanges,
      this.parentForm.get('company').valueChanges,
    ).subscribe(() => {
      const firstName = this.parentForm.get('firstName').value;
      const lastName = this.parentForm.get('lastName').value;
      const company = this.parentForm.get('company').value;

      this.displayNamesAvailable = [];

      if (firstName || lastName || company) {
        this.displayNamesAvailable.push(`${firstName || ''} ${lastName || ''}`.trim());
        this.displayNamesAvailable.push(`${lastName || ''}${lastName && firstName ? ', ' + firstName : firstName || ''}`.trim());
        this.displayNamesAvailable.push(company.trim());
        this.displayNamesAvailable = _.uniq(_.filter(this.displayNamesAvailable, _.identity));

        if (_.head(this.displayNamesAvailable)) {
          this.parentForm.get('displayName').setValue(_.head(this.displayNamesAvailable));
        }
      }
    });
  }

  onDisplayNameTyped(event: any) {
    this.parentForm.get('displayName').setValue(event.query);

    this.searchACItems(event, 'displayNamesAvailable', 'displayNameSuggestions');
  }
}

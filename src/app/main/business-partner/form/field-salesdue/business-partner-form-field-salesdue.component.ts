import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-business-partner-form-field-salesdue',
  templateUrl: 'business-partner-form-field-salesdue.component.html'
})
export class BusinessPartnertFormFieldSalesDueComponent {
  @Input() label: boolean = true;
  @Input() parentForm: FormGroup;
}

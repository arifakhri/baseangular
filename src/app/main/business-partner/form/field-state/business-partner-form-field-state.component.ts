import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-business-partner-form-field-state',
  templateUrl: 'business-partner-form-field-state.component.html'
})
export class BusinessPartnertFormFieldStateComponent {
  @Input() parentForm: FormGroup;
}

import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-business-partner-form-field-bankaccno',
  templateUrl: 'business-partner-form-field-bankaccno.component.html'
})
export class BusinessPartnertFormFieldBankAccNoComponent {
  @Input() label: boolean = true;
  @Input() parentForm: FormGroup;
}

import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { AutocompleteService } from '../../../../core/core.module';

@Component({
  selector: 'app-business-partner-form-field-salestax',
  templateUrl: 'business-partner-form-field-salestax.component.html'
})
export class BusinessPartnertFormFieldSalesTaxComponent {
  @Input() taxes: ITax[] = [];
  @Input() label: boolean = true;
  @Input() parentForm: FormGroup;

  constructor(
    public _autocomplete: AutocompleteService
  ) { }
}

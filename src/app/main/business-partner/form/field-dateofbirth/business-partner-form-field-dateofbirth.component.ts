import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-business-partner-form-field-dateofbirth',
  templateUrl: 'business-partner-form-field-dateofbirth.component.html'
})
export class BusinessPartnertFormFieldDateOfBirthComponent {
  @Input() label: boolean = true;
  @Input() parentForm: FormGroup;
}

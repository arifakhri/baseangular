import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-business-partner-form-field-isvendor',
  templateUrl: 'business-partner-form-field-isvendor.component.html'
})
export class BusinessPartnertFormFieldIsVendorComponent {
  @Input() label: boolean = true;
  @Input() parentForm: FormGroup;
}

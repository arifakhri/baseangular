import * as _ from 'lodash';
import * as moment from 'moment';
import { ActivatedRoute } from '@angular/router';
import { Component, EventEmitter, Input, NgZone, OnChanges, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import 'app/rxjs-imports.ts';

import { BusinessPartnerRestService } from '../../business-partner-rest.service';
import { RetryDialogComponent } from '../../../../shared/retry-dialog/retry-dialog.component';

@Component({
  selector: 'app-business-partner-form-mini',
  templateUrl: 'business-partner-form-mini.component.html'
})
export class BusinessPartnerFormMiniComponent implements OnChanges, OnInit {
  @Input() doc: IBusinessPartner;
  @Input() form: FormGroup;
  @Output('compReady') compReady: EventEmitter<boolean> = new EventEmitter();
  @ViewChild('retryDialog') elRetryDialog: RetryDialogComponent;

  type: string;

  countries: ICountry[] = [];
  priceLevels: any = [];
  taxes: ITax[] = [];

  retryDialogShownState: boolean = false;
  retryDialogOnRetry: Function;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _businessPartnerRest: BusinessPartnerRestService,
    private _zone: NgZone,
  ) {
    this._activatedRoute.data.subscribe(data => this.type = data.type || 'customer');
  }

  ngOnInit() {
    this.loadRelatedData();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (_.has(changes, 'doc') && this.doc) {
      if (this.doc.dateOfBirth) {
        this.doc.dateOfBirth = moment(this.doc.dateOfBirth).toDate();
      }
      if (!this.doc.postalAddress) {
        _.unset(this.doc, 'postalAddress');
      }
      if (!this.doc.shippingAddress) {
        _.unset(this.doc, 'shippingAddress');
      }
      this.form.patchValue(this.doc);
    }
  }

  loadRelatedData() {
    this.elRetryDialog.createRetryEntry(this._businessPartnerRest.loadRelatedData())
      .subscribe(related => {
        this.taxes = related.taxes;
        this.countries = related.countries;
        this.priceLevels = related.priceLevels;

        this.compReady.emit(true);
      });
  }
}

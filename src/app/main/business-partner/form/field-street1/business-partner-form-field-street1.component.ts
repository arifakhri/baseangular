import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-business-partner-form-field-street1',
  templateUrl: 'business-partner-form-field-street1.component.html'
})
export class BusinessPartnertFormFieldStreet1Component {
  @Input() parentForm: FormGroup;
}

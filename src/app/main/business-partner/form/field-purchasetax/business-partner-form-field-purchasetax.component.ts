import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { AutocompleteService } from '../../../../core/core.module';

@Component({
  selector: 'app-business-partner-form-field-purchasetax',
  templateUrl: 'business-partner-form-field-purchasetax.component.html'
})
export class BusinessPartnertFormFieldPurchaseTaxComponent {
  @Input() taxes: ITax[] = [];
  @Input() label: boolean = true;
  @Input() parentForm: FormGroup;

  constructor(
    public _autocomplete: AutocompleteService
  ) { }
}

import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-business-partner-form-field-purchasetaxable',
  templateUrl: 'business-partner-form-field-purchasetaxable.component.html'
})
export class BusinessPartnertFormFieldPurchaseTaxableComponent {
  @Input() label: boolean = true;
  @Input() parentForm: FormGroup;
}

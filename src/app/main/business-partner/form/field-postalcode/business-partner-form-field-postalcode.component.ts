import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-business-partner-form-field-postalcode',
  templateUrl: 'business-partner-form-field-postalcode.component.html'
})
export class BusinessPartnertFormFieldPostalCodeComponent {
  @Input() parentForm: FormGroup;
}

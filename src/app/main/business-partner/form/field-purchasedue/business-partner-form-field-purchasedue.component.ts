import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-business-partner-form-field-purchasedue',
  templateUrl: 'business-partner-form-field-purchasedue.component.html'
})
export class BusinessPartnertFormFieldPurchaseDueComponent {
  @Input() label: boolean = true;
  @Input() parentForm: FormGroup;
}

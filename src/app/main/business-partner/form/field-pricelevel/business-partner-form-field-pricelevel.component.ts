import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { AutocompleteService } from '../../../../core/core.module';

@Component({
  selector: 'app-business-partner-form-field-pricelevel',
  templateUrl: 'business-partner-form-field-pricelevel.component.html'
})
export class BusinessPartnertFormFieldPriceLevelComponent {
  @Input() priceLevels: IPriceLevel[] = [];
  @Input() label: boolean = true;
  @Input() parentForm: FormGroup;

  constructor(
    public _autocomplete: AutocompleteService
  ) { }
}

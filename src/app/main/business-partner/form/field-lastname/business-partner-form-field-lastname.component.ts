import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-business-partner-form-field-lastname',
  templateUrl: 'business-partner-form-field-lastname.component.html'
})
export class BusinessPartnertFormFieldLastNameComponent {
  @Input() label: boolean = true;
  @Input() parentForm: FormGroup;
}

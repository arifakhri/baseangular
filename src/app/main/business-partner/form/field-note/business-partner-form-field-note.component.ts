import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-business-partner-form-field-note',
  templateUrl: 'business-partner-form-field-note.component.html'
})
export class BusinessPartnertFormFieldNoteComponent {
  @Input() label: boolean = true;
  @Input() parentForm: FormGroup;
}

import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-business-partner-form-field-email',
  templateUrl: 'business-partner-form-field-email.component.html'
})
export class BusinessPartnertFormFieldEmailComponent {
  @Input() label: boolean = true;
  @Input() parentForm: FormGroup;
}

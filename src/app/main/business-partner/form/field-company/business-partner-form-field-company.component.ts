import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-business-partner-form-field-company',
  templateUrl: 'business-partner-form-field-company.component.html'
})
export class BusinessPartnertFormFieldCompanyComponent {
  @Input() label: boolean = true;
  @Input() parentForm: FormGroup;
}

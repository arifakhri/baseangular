import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-business-partner-form-field-bankaccname',
  templateUrl: 'business-partner-form-field-bankaccname.component.html'
})
export class BusinessPartnertFormFieldBankAccNameComponent {
  @Input() label: boolean = true;
  @Input() parentForm: FormGroup;
}

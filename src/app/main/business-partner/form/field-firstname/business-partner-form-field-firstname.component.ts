import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-business-partner-form-field-firstname',
  templateUrl: 'business-partner-form-field-firstname.component.html'
})
export class BusinessPartnertFormFieldFirstNameComponent {
  @Input() label: boolean = true;
  @Input() parentForm: FormGroup;
}

import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-business-partner-form-field-iscustomer',
  templateUrl: 'business-partner-form-field-iscustomer.component.html'
})
export class BusinessPartnertFormFieldIsCustomerComponent {
  @Input() label: boolean = true;
  @Input() parentForm: FormGroup;
}

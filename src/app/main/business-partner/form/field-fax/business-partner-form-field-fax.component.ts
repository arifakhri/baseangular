import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-business-partner-form-field-fax',
  templateUrl: 'business-partner-form-field-fax.component.html'
})
export class BusinessPartnertFormFieldFaxComponent {
  @Input() label: boolean = true;
  @Input() parentForm: FormGroup;
}

import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { AutocompleteService } from '../../../../core/core.module';

@Component({
  selector: 'app-business-partner-form-field-country',
  templateUrl: 'business-partner-form-field-country.component.html'
})
export class BusinessPartnertFormFieldCountryComponent {
  @Input() countries: ICountry[] = [];
  @Input() label: boolean = true;
  @Input() parentForm: FormGroup;

  constructor(
    public _autocomplete: AutocompleteService
  ) { }
}

import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-business-partner-form-field-city',
  templateUrl: 'business-partner-form-field-city.component.html'
})
export class BusinessPartnertFormFieldCityComponent {
  @Input() parentForm: FormGroup;
}

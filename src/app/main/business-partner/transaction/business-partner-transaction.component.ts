import * as _ from 'lodash';
import { ActivatedRoute } from '@angular/router';
import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { DataTable } from 'primeng/components/datatable/datatable';
import { Subscription } from 'rxjs';
import 'app/rxjs-imports.ts';
import { TranslateService } from '@ngx-translate/core';
import { TransactionRoutingService } from '../../transaction/transaction-routing.service'

import {
  AccountingService,
  ExportDataTableService,
  GridTableDataSource,
  GridTableService,
  GridTableToggleService,
} from '../../../core/core.module';
import { BusinessPartnerRestService } from '../business-partner-rest.service';
import { RetryDialogComponent } from '../../../shared/retry-dialog/retry-dialog.component';

@Component({
  selector: 'app-business-partner-transaction',
  templateUrl: 'business-partner-transaction.component.html',
  styleUrls: ['business-partner-transaction.component.scss']
})
export class BusinessPartnerTransactionComponent implements OnInit {
  @Input() doc: IBusinessPartner;
  @ViewChild('tableDescription') elTableDescription: ElementRef;
  @ViewChild('retryDialog') elRetryDialog: RetryDialogComponent;

  _ = _;
  filters: any = {};

  gridDataSource: GridTableDataSource<IBusinessPartnerTransaction> = new GridTableDataSource<IBusinessPartnerTransaction>();
  selectedRecords: IBusinessPartnerTransaction[] = [];

  retryDialogShownState: boolean = false;
  retryDialogOnRetry: Function;

  combinedFilterValues: string = '';
  combinedFilterFields = {
    transactionNumber: true,
    description: true,
  };

  tableColumns: IGridTableColumn[] = [];
  tableColumnsToggle: any;
  tableColumnsShow: boolean;

  private loadDataTimeoutId: number;
  private gridFirstLoad: boolean = true;
  private subscriptions: { [key: string]: Subscription[] } = {
    load: []
  };

  @ViewChild('gridTable') public gridTable: DataTable;

  constructor(
    public _accounting: AccountingService,
    private _activatedRoute: ActivatedRoute,
    private _dataTableExport: ExportDataTableService,
    private _gridTable: GridTableService,
    private _gridTableToggle: GridTableToggleService,
    private _businessPartnerRest: BusinessPartnerRestService,
    private _translate: TranslateService,
    private _transactionRoutingService: TransactionRoutingService
  ) {
    this.gridDataSource.pager.itemsPerPage = 10;
  }

  ngOnInit() {
    this.prepareTable();
    this.loadData();
  }

  prepareTable() {
    this.tableColumns = [{
      i18nLabel: 'ui.businessPartner.transaction.column.description',
      field: '',
      template: this.elTableDescription,
    }, {
      i18nLabel: 'ui.businessPartner.transaction.column.amount',
      field: 'total',
      formatter: (value, row: IBusinessPartnerTransaction) => {
        return this._accounting.ac.formatMoney(value, '');
      },
      columnClasses: 'right-align',
      sort: true,
    }, {
      i18nLabel: 'ui.businessPartner.transaction.column.amountDue',
      field: 'balanceDue',
      formatter: (value, row: IBusinessPartnerTransaction) => {
        return this._accounting.ac.formatMoney(value, '');
      },
      columnClasses: 'right-align',
      sort: true,
    }, {
      i18nLabel: 'ui.businessPartner.transaction.column.status',
      field: 'status',
      sort: true,
      formatter: (value, row: IBusinessPartnerTransaction) => {
        return _.startCase(value);
      },
    }];
    this.tableColumnsShow = false;
    this.tableColumnsToggle = this._gridTableToggle.mapToggleOptionsFromColumns(this.tableColumns);
  }

  gridLoadDataWrapper() {
    // Grid always load at the first time. we dont want that. we already call on ngOnInit
    if (this.gridFirstLoad) {
      this.gridFirstLoad = false;
      return;
    }

    if (this.loadDataTimeoutId) {
      window.clearTimeout(this.loadDataTimeoutId);
      this.loadDataTimeoutId = null;
    }

    this.loadDataTimeoutId = window.setTimeout(() => {
      this.loadData();
    }, 300);
  }

  loadData() {
    const qOption = this._gridTable.generateApiQueryOptionFromGridInfo(
      this.gridDataSource.pager,
      true,
      this.gridTable,
      this._gridTable.generateCombinedApiFilters(this.combinedFilterValues, this.combinedFilterFields)
    );

    const queryParams: any = {};
    queryParams.contactId = this.doc.id;
    queryParams.includeVoided = false;

    this.subscriptions.load.forEach(sub => sub.unsubscribe());
    this.subscriptions.load.push(
    this.elRetryDialog.createRetryEntry(this._businessPartnerRest.findAllTransactions(qOption, queryParams))
      .subscribe(response => {
        this.gridDataSource.updateFromApiPaginationResult(response.data);
      })
    );
  }

  public onGridChange() {
    this.gridLoadDataWrapper();
  }

  public onGridPageChange() {
    this.gridLoadDataWrapper();
  }

  getUpdateLink(doc: any) {
    return this._transactionRoutingService.resolve(doc, 'update');
  }

}

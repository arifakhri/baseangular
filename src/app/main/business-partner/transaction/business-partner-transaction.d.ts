declare interface IBusinessPartnerTransaction {
  transactionId: string;
  transactionType: string;
  transactionTypeGroup: string;
  transactionNumber: string;
  transactionDate: string;
  transactionDueDate: string;
  contactId: string;
  description: string;
  state: string;
  status: string;
  total: number;
  contact: {
    id: string;
    contactType: string;
    code: string;
    displayName: string;
    company: string;
    fullName: string;
    phone: string;
    mobile: string;
    fax: string;
    email: string;
    website: string;
    inactive: boolean;
    postalAddress: {
      attention: string;
      street1: string;
      street2: string;
      city: string;
      stateProvince: string;
      countryId: string;
      postalCode: string;
      longitude: number;
      latitude: number;
      country: {
        code: string;
        name: string;
      }
    }
  };
}
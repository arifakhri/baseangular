import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-business-partner-quick-form',
  templateUrl: 'business-partner-quick-form.component.html'
})
export class BusinessPartnerQuickFormComponent {
  @Input() form: FormGroup;
}

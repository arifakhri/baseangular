import { Component } from '@angular/core';
import { BaseDetailBComponent } from '../../../shared/base/base.module';
import { ProductBrandRestService } from '../product-brand-rest.service';

@Component({
  selector: 'app-product-brand-detail',
  templateUrl: 'product-brand-detail.component.html'
})
export class ProductBrandDetailComponent extends BaseDetailBComponent {
  constructor(
    private _productBrandRest: ProductBrandRestService,
  ) {
    super()
    this.componentId = 'ProductBrandDetail';
    this.headerButtons.push({
      type: 'edit',
    });

    this.registerHook('load', event => {
      return this._productBrandRest
        .load(this.page.routeParams.id)
        .do(doc => this.headerTitle = doc.name);
    });
  }
}

import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-product-brand-quick-form',
  templateUrl: 'product-brand-quick-form.component.html'
})
export class ProductBrandQuickFormComponent implements OnInit {
  @Input() form: FormGroup;

  ngOnInit() {
    this.buildForm();
  }

  buildForm() {
    this.form.addControl('name', new FormControl('', Validators.required));
  }
}

import * as _ from 'lodash';
import { Component } from '@angular/core';
import { ProductBrandRestService } from '../product-brand-rest.service';
import { BaseUpdateBComponent } from '../../../shared/base/base.module';
import { MProductBrand } from '../product-brand.model';

@Component({
  selector: 'app-product-brand-update',
  templateUrl: 'product-brand-update.component.html'
})
export class ProductBrandUpdateComponent extends BaseUpdateBComponent {
    constructor(
    private _productBrandRest: ProductBrandRestService,
    ) {
    super();
    this.componentId = 'ProductBrandUpdateUpdate';
    this.routeURL = '/product-brands';
    this.entrySuccessI18n = 'success.productBrand.update';

    this.headerTitle = 'ui.productBrand.update.title';

    this.registerHook('load', event => {
      return this._productBrandRest.load(this.page.routeParams.id);
    });

    this.registerHook('save', event => {
      const formValue = this.form.value;
      let productBrand = new MProductBrand;
      productBrand = _.assign(productBrand, this.doc, formValue);

      return this._productBrandRest.update(this.page.routeParams.id, productBrand);
    });
  }

}

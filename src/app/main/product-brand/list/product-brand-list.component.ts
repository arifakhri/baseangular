import * as SnackBar from 'node-snackbar';
import swal from 'sweetalert2';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { DataTable } from 'primeng/components/datatable/datatable';
import { Observable, Subscription } from 'rxjs';
import 'app/rxjs-imports.ts';
import { TranslateService } from '@ngx-translate/core';

import { SpinnerDirective } from '../../../shared/spinner/spinner.directive';

import { ExportDataTableService, GridTableDataSource, GridTableService, GridTableToggleService, } from '../../../core/core.module';
import { ProductBrandRestService } from '../product-brand-rest.service';
import { RetryDialogComponent } from '../../../shared/retry-dialog/retry-dialog.component';

@Component({
  selector: 'app-product-brand-list',
  templateUrl: 'product-brand-list.component.html'
})
export class ProductBrandListComponent implements OnInit {
  @ViewChild('gridTable') public gridTable: DataTable;
  @ViewChild('pageLoading') elPageLoading: SpinnerDirective;
  @ViewChild('retryDialog') elRetryDialog: RetryDialogComponent;

  compReady: boolean;
  gridDataSource: GridTableDataSource<IProductBrand> = new GridTableDataSource<IProductBrand>();
  selectedRecords: IProductBrand[] = [];

  combinedFilterValues: string = '';
  combinedFilterFields = {
    name: true,
  };

  tableColumns: IGridTableColumn[] = [{
    i18nLabel: 'ui.productBrand.list.column.name',
    field: 'name',
    link: (row: IProductBrand) => {
      return `/product-brands/${row.id}`;
    },
    sort: true,
  }];
  tableColumnsShow: boolean = false;
  tableColumnsToggle = this._gridTableToggle.mapToggleOptionsFromColumns(this.tableColumns);

  retryDialogShownState: boolean = false;
  retryDialogOnRetry: Function;

  private loadDataTimeoutId: number;
  private gridFirstLoad: boolean = true;
  private subscriptions: { [key: string]: Subscription[] } = {
    load: []
  };

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _dataTableExport: ExportDataTableService,
    private _gridTable: GridTableService,
    private _gridTableToggle: GridTableToggleService,
    private _productBrandRest: ProductBrandRestService,
    private _translate: TranslateService,
  ) { }

  ngOnInit() {
    this.loadData();
  }

  gridLoadDataWrapper() {
    // Grid always load at the first time. we dont want that. we already call on ngOnInit
    if (this.gridFirstLoad) {
      this.gridFirstLoad = false;
      return;
    }

    if (this.loadDataTimeoutId) {
      window.clearTimeout(this.loadDataTimeoutId);
      this.loadDataTimeoutId = null;
    }

    this.loadDataTimeoutId = window.setTimeout(() => {
      this.loadData();
    }, 300);
  }

  get exportRecords(): Observable<IProductBrand[]> {
    const qOption = {
      ...this._gridTable.generateApiQueryOptionFromGridInfo(
        this.gridDataSource.pager,
        false,
        this.gridTable,
        this._gridTable.generateCombinedApiFilters(this.combinedFilterValues, this.combinedFilterFields)
      ),
      skip: 0,
      take: this._dataTableExport.exportLimit,
    };

    return this._productBrandRest.findAll(qOption).map(response => response.data);
  }

  exportPDF() {
    this.exportRecords.subscribe(records => {
      this._dataTableExport.export({
        records,
        mapOptions: this._dataTableExport.mapFromTableColumns(this.tableColumns),
        fileName: 'all-brands',
        extension: 'pdf',
        templateData: {
          title: this._translate.instant('ui.productBrand.list.title'),
        },
      });
    });
  }

  exportExcel() {
    this.exportRecords.subscribe(records => {
      this._dataTableExport.export({
        records: records,
        mapOptions: this._dataTableExport.mapFromTableColumns(this.tableColumns),
        fileName: 'all-brands',
        extension: 'xls',
      });
    });
  }

  showDeleteDialog(productBrandId: string) {
    swal({
      title: this._translate.instant('ui.productBrand.confirm.delete.label'),
      text: this._translate.instant('ui.productBrand.confirm.delete.description'),
      type: 'question',
      showCancelButton: true,
    }).then(() => {
      this._productBrandRest.delete(productBrandId).subscribe(response => {
        this.gridLoadDataWrapper();
        SnackBar.show({
          text: this._translate.instant('ui.productBrand.success.delete'),
          pos: 'bottom-right'
        });
      });
    }).catch(() => { });
  }

  loadData() {
    this.elPageLoading.forceShow();

    const qOption = this._gridTable.generateApiQueryOptionFromGridInfo(
      this.gridDataSource.pager,
      true,
      this.gridTable,
      this._gridTable.generateCombinedApiFilters(this.combinedFilterValues, this.combinedFilterFields)
    );

    this.subscriptions.load.forEach(sub => sub.unsubscribe());
    this.subscriptions.load.push(
    this.elRetryDialog.createRetryEntry(
        this._productBrandRest.findAll(qOption)
          .finally(() => {
            this.elPageLoading.forceHide();
          })
    ).subscribe(response => {
        this.gridDataSource.updateFromApiPaginationResult(response);

        this.compReady = true;
      })
    );
  }

  public onGridChange() {
    this.gridLoadDataWrapper();
  }

  public onGridPageChange() {
    this.gridLoadDataWrapper();
  }
}

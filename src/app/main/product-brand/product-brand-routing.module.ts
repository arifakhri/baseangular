import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ProductBrandCreateComponent } from './create/product-brand-create.component';
import { ProductBrandDetailComponent } from './detail/product-brand-detail.component';
import { ProductBrandListComponent } from './list/product-brand-list.component';
import { ProductBrandUpdateComponent } from './update/product-brand-update.component';

import { AuthorizationService } from '../../core/core.module';

export const routes: Routes = [{
  path: '',
  children: [{
    path: '',
    component: ProductBrandListComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'productBrand.list'
    }
  }, {
    path: 'create',
    component: ProductBrandCreateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'productBrand.create'
    }
  }, {
    path: ':id',
    component: ProductBrandDetailComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'productBrand.detail'
    }
  }, {
    path: ':id/update',
    component: ProductBrandUpdateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'productBrand.update'
    }
  }]
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class ProductBrandRoutingModule { }

import { Injectable } from '@angular/core';
import 'app/rxjs-imports.ts';

import { ApiQueryOption, RequestService } from '../../core/core.module';

import { APP_CONST } from '../../app.const';

@Injectable()
export class ProductBrandRestService {
  baseURL = `${APP_CONST.API_MAIN}/product-brands`;
  baseURLPicker = `${APP_CONST.API_MAIN}/pickers/product-brands`;

  request = this._request.new(this.baseURL);
  requestPicker = this._request.new(this.baseURLPicker);

  constructor(
    private _request: RequestService,
  ) { }

  create(productBrand: IProductBrand) {
    return this.request.post<IProductBrand>(``, productBrand);
  }

  findAll(queryOption: ApiQueryOption) {
    return this.request.post<IApiPaginationResult<IProductBrand>>(`q`, queryOption);
  }

  load(productBrandId: string) {
    return this.request.get<IProductBrand>(`${productBrandId}`);
  }

  update(productBrandId: string, updateObj: IProductBrand) {
    return this.request.put<IProductBrand>(`${productBrandId}`, updateObj);
  }

  delete(productBrandId: string) {
    return this.request.delete<any>(`${productBrandId}`);
  }

  findAllProductBrandPicker() {
    return this.requestPicker.post<any>(``);
  }
}

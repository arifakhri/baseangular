import * as _ from 'lodash';
import { Component } from '@angular/core';

import { ProductBrandRestService } from '../product-brand-rest.service';

import { BaseCreateBComponent } from '../../../shared/base/base.module';
import { MProductBrand } from '../product-brand.model';

@Component({
  selector: 'app-product-brand-create',
  templateUrl: 'product-brand-create.component.html'
})
export class ProductBrandCreateComponent extends BaseCreateBComponent {
  constructor(
    private _productBrandRest: ProductBrandRestService
  ) {
    super();

    this.componentId = 'ProductBrandCreate';
    this.routeURL = '/product-brands';
    this.entrySuccessI18n = 'success.productBrand.create';

    this.headerTitle = 'ui.productBrand.create.title';

    this.registerHook('save', event => {
      const formValue = this.form.value;
      let productBrand = new MProductBrand;
      productBrand = _.assign(productBrand, formValue);

      return this._productBrandRest.create(productBrand);
    });
   }
}

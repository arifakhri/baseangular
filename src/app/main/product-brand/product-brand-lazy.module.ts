import { NgModule } from '@angular/core';

import { ProductBrandModule } from './product-brand.module';
import { ProductBrandRoutingModule } from './product-brand-routing.module';

@NgModule({
  imports: [
    ProductBrandModule,
    ProductBrandRoutingModule
  ]
})
export class ProductBrandLazyModule { }

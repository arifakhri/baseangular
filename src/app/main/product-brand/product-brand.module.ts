import { BsDropdownModule, PaginationModule } from 'ngx-bootstrap';
import { CommonModule } from '@angular/common';
import { DataTableModule } from 'primeng/components/datatable/datatable';
import { ListboxModule } from 'primeng/components/listbox/listbox';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { ProductBrandCreateComponent } from './create/product-brand-create.component';
import { ProductBrandDetailComponent } from './detail/product-brand-detail.component';
import { ProductBrandFormComponent } from './form/product-brand-form.component';
import { ProductBrandListComponent } from './list/product-brand-list.component';
import { ProductBrandUpdateComponent } from './update/product-brand-update.component';
import { ProductBrandQuickCreateComponent } from './quick-create/product-brand-quick-create.component';
import { ProductBrandQuickFormComponent } from './quick-form/product-brand-quick-form.component';

import { ProductBrandRestService } from './product-brand-rest.service';

import { CoreModule } from '../../core/core.module';
import { SharedModule } from '../../shared/shared.module';

export const PROVIDERS = [
  ProductBrandRestService,
];

@NgModule({
  imports: [
    BsDropdownModule,
    CommonModule,
    CoreModule,
    DataTableModule,
    FlexLayoutModule,
    FormsModule,
    ListboxModule,
    PaginationModule,
    ReactiveFormsModule,
    RouterModule,
    SharedModule,
    TranslateModule,
  ],
  declarations: [
    ProductBrandCreateComponent,
    ProductBrandDetailComponent,
    ProductBrandFormComponent,
    ProductBrandListComponent,
    ProductBrandUpdateComponent,
    ProductBrandQuickCreateComponent,
    ProductBrandQuickFormComponent,
  ],
  exports: [
    ProductBrandQuickCreateComponent,
  ]
})
export class ProductBrandModule { }

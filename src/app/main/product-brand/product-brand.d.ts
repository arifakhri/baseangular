declare interface IProductBrand {
  id: string;
  name: string;
}
import { Component } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { BaseFormBComponent } from '../../../shared/base/base.module';

@Component({
  selector: 'app-product-brand-form',
  templateUrl: 'product-brand-form.component.html'
})
export class ProductBrandFormComponent extends BaseFormBComponent {
  constructor() {
    super();
    this.componentId = 'ProductBrandForm';

    this.registerHook('buildForm', event => {
      this.buildForm();
    });
  }

  buildForm() {
    const nameControl = new FormControl('', [Validators.required, Validators.maxLength(50)]);
    (<any>nameControl).validatorData = {
      maxLength: 50
    };

    this.form.addControl('name', nameControl);
  }
}

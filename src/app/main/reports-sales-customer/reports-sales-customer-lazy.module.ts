import { NgModule } from '@angular/core';

import { ReportsSalesCustomerModule } from './reports-sales-customer.module';
import { ReportsSalesCustomerRoutingModule } from './reports-sales-customer-routing.module';

@NgModule({
  imports: [
    ReportsSalesCustomerModule,
    ReportsSalesCustomerRoutingModule,
  ],
})
export class ReportsSalesCustomerLazyModule { }

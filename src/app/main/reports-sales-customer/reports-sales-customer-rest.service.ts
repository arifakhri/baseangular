import { Injectable } from '@angular/core';

import { ReportsRestService } from '../reports/reports-rest.service';

@Injectable()
export class ReportsSalesCustomerRestService {

  constructor(
    private _reportRest: ReportsRestService,
  ) { }

  findAll(queryParams: any = {}) {
    return this._reportRest.requestReport.post<IApiPaginationResult<IReportsSalesCustomer>>(
      `sales-by-customer-summary`,
      queryParams
    );
  }
}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ReportsSalesCustomerPreviewComponent } from './preview/reports-sales-customer-preview.component';

import { AuthorizationService } from '../../core/core.module';

const routes: Routes = [{
  path: '',
  children: [{
    path: '',
    component: ReportsSalesCustomerPreviewComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'reportsSalesCustomer.preview'
    }
  }]
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ],
})
export class ReportsSalesCustomerRoutingModule { }

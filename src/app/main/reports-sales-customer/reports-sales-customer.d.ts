declare interface IReportsSalesCustomer {
  accountClass: string;
  accountType: string;
  accountName: string;
  total: 0;
}
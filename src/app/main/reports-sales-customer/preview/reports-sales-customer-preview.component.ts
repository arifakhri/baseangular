import * as moment from 'moment';
import { AutoComplete } from 'primeng/components/autocomplete/autocomplete';
import { Component, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

import { AdminLayoutService } from '../../../layouts/layouts.module';
import { BusinessPartnerRestService } from '../../business-partner/business-partner-rest.service';
import { CommonService, GridTableFilterService } from '../../../core/core.module';
import { ReportsSalesCustomerRestService } from '../reports-sales-customer-rest.service';
import { TransactionService } from '../../transaction/transaction.service';

@Component({
  selector: 'app-reports-sales-customer-preview',
  templateUrl: './reports-sales-customer-preview.component.html',
})
export class ReportsSalesCustomerPreviewComponent {
  @ViewChild('customerAC') elCustomerAC: AutoComplete;
  @ViewChild('reportViewer') elReportViewer: any;

  compReady: boolean = false;
  data: any = [];
  form: FormGroup = new FormGroup({});
  reportViewerReady: boolean = false;

  reportParams: any = {
    title: 'Sales by Customer Summary'
  };

  ACCustomerHandler = CommonService.remoteACItemsHandler({
    remoteParams: this.ACCustomerParams.bind(this),
    remoteRequest: this._businessPartnerRest.findAllCustomersPicker.bind(this._businessPartnerRest),
    remoteRequestMap: (response) => response.data,
    element: () => this.elCustomerAC,
  });

  filtersMap: IGridTableFilterMap = {
    lowDate: {
      targetVar: 'date',
      targetFilter: 'param',
      dateFormat: 'YYYY-MM-DD',
    },
    highDate: {
      targetVar: 'date',
      targetFilter: 'param',
      dateFormat: 'YYYY-MM-DD',
    },
    transactionTypes: {
      targetFilter: 'param',
      targetVar: 'arrstring',
    },
    states: {
      targetFilter: 'param',
      targetVar: 'arrstring',
    },
    statuses: {
      targetFilter: 'param',
      targetVar: 'arrstring',
    },
    customerId: {
      targetFilter: 'param',
    },
    transactionNumber: {
      targetFilter: 'param',
    },
  };

  constructor(
    private _adminLayout: AdminLayoutService,
    private _businessPartnerRest: BusinessPartnerRestService,
    private _gridTableFilter: GridTableFilterService,
    private _reportsSalesCustomerRest: ReportsSalesCustomerRestService,
    public _transaction: TransactionService,
  ) {
    this._adminLayout.containerType = 1;

    this.loadData();
    this.buildForm();
  }

  buildForm() {
    this.form.addControl('lowDate', new FormControl(moment().set('date', 1).toDate()));
    this.form.addControl('highDate', new FormControl(new Date()));
    this.form.addControl('customer', new FormControl);
    this.form.addControl('customerId', new FormControl);
    this.form.addControl('transactionTypes', new FormControl(''));
    this.form.addControl('states', new FormControl);
    this.form.addControl('statuses', new FormControl);
    this.form.addControl('transactionNumber', new FormControl(''));
  }

  ACCustomerParams(event: any, type: string) {
    let filters = [];

    if (type === 'search') {
      filters = [{
        filterValues: [{
          field: 'displayName',
          operator: 'contains',
          value: event.query
        }]
      }];
    }

    return [{
      filter: filters,
      sort: [{
        field: 'displayName',
        dir: 'asc'
      }],
      take: 30,
      skip: 0,
      includeTotalCount: false
    }];
  }

  loadData() {
    const filters = this._gridTableFilter.parse(this.form.value, this.filtersMap);
    this.compReady = false;

    this._reportsSalesCustomerRest.findAll(filters.qParams).subscribe(response => {
      this.data = response.data;

      this.prepareParams();
    }, null, () => this.compReady = true);
  }

  prepareParams() {
    const filterValues = this.form.value;

    if (filterValues.lowDate) {
      this.reportParams.lowDate = moment(filterValues.lowDate).valueOf();
    }

    if (filterValues.highDate) {
      this.reportParams.highDate = moment(filterValues.highDate).valueOf();
    }
  }
}

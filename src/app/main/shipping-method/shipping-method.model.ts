export class MShippingMethod {
  id: string;
  name: string;
  courierId: string;
  inactive: boolean;
  rowVersion: string;
}

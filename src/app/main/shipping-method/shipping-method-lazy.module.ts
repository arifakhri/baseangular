import { NgModule } from '@angular/core';

import { ShippingMethodModule } from './shipping-method.module';
import { ShippingMethodRoutingModule } from './shipping-method-routing.module';

@NgModule({
  imports: [
    ShippingMethodModule,
    ShippingMethodRoutingModule
  ]
})
export class ShippingMethodLazyModule { }

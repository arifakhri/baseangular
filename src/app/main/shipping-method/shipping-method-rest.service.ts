import { Injectable } from '@angular/core';
import 'app/rxjs-imports.ts';

import { ApiQueryOption, RequestService } from '../../core/core.module';

import { APP_CONST } from '../../app.const';

@Injectable()
export class ShippingMethodRestService {
  baseURL = `${APP_CONST.API_MAIN}/shipping-methods`;
  request = this._request.new(this.baseURL);

  constructor(
    private _request: RequestService,
  ) { }

  create(shippingMethod: IShippingMethod) {
    return this.request.post<IShippingMethod>(``, shippingMethod);
  }

  findAll(queryOption: ApiQueryOption) {
    return this.request.post<IApiPaginationResult<IShippingMethod>>(`q`, queryOption);
  }

  load(shippingMethodId: string) {
    return this.request.get<IShippingMethod>(`${shippingMethodId}`);
  }

  loadRelatedData() {
    return this.request.get<{ couriers: any[] }>(`entry-related-data`);
  }

  update(shippingMethodId: string, updateObj: IShippingMethod) {
    return this.request.put<IShippingMethod>(`${shippingMethodId}`, updateObj);
  }
}

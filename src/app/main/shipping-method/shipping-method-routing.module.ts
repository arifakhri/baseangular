import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ShippingMethodCreateComponent } from './create/shipping-method-create.component';
import { ShippingMethodListComponent } from './list/shipping-method-list.component';
import { ShippingMethodUpdateComponent } from './update/shipping-method-update.component';

import { AuthorizationService } from '../../core/core.module';

export const routes: Routes = [{
  path: '',
  children: [{
    path: '',
    component: ShippingMethodListComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'shippingMethod.list'
    }
  }, {
    path: 'create',
    component: ShippingMethodCreateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'shippingMethod.create'
    }
  }, {
    path: ':id/update',
    component: ShippingMethodUpdateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'shippingMethod.update'
    }
  }]
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class ShippingMethodRoutingModule { }

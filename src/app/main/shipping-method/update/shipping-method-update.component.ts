import * as _ from 'lodash';
import { Component } from '@angular/core';

import { ShippingMethodRestService } from '../shipping-method-rest.service';

import { BaseUpdateBComponent } from '../../../shared/base/base.module';
import { MShippingMethod } from '../shipping-method.model';

@Component({
  selector: 'app-shipping-method-update',
  templateUrl: 'shipping-method-update.component.html',
})
export class ShippingMethodUpdateComponent extends BaseUpdateBComponent {
  constructor(
    private _shippingMethodRest: ShippingMethodRestService,
  ) {
    super();

    this.componentId = 'ShippingMethodUpdate';
    this.routeURL = '/shipping-methods';
    this.entrySuccessI18n = 'success.shippingMethod.update';

    this.headerTitle = 'ui.shippingMethod.update.title';

    this.registerHook('load', event => {
      return this._shippingMethodRest.load(this.page.routeParams.id);
    });

    this.registerHook('save', event => {
      const formValue = this.form.value;
      let shippingMethod = new MShippingMethod;
      shippingMethod = _.assign(shippingMethod, this.doc, formValue);

      return this._shippingMethodRest.update(this.page.routeParams.id, shippingMethod);
    });
  }
}

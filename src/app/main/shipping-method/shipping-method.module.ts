import { BsDropdownModule, PaginationModule } from 'ngx-bootstrap';
import { CheckboxModule } from 'primeng/components/checkbox/checkbox';
import { DataTableModule } from 'primeng/components/datatable/datatable';
import { ListboxModule } from 'primeng/components/listbox/listbox';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { ShippingMethodCreateComponent } from './create/shipping-method-create.component';
import { ShippingMethodFormComponent } from './form/shipping-method-form.component';
import { ShippingMethodListComponent } from './list/shipping-method-list.component';
import { ShippingMethodUpdateComponent } from './update/shipping-method-update.component';

import { ShippingMethodRestService } from './shipping-method-rest.service';
import { ShippingMethodService } from './shipping-method.service';

import { CoreModule } from '../../core/core.module';
import { SharedModule } from '../../shared/shared.module';

export const PROVIDERS = [
  ShippingMethodRestService,
  ShippingMethodService,
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    FlexLayoutModule,
    HttpModule,
    ReactiveFormsModule,
    RouterModule,
    TranslateModule,
    BsDropdownModule,
    CheckboxModule,
    DataTableModule,
    ListboxModule,
    PaginationModule,
    CoreModule,
    SharedModule,
  ],
  declarations: [
    ShippingMethodCreateComponent,
    ShippingMethodFormComponent,
    ShippingMethodListComponent,
    ShippingMethodUpdateComponent
  ],
})
export class ShippingMethodModule { }

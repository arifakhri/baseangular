declare interface IShippingMethod {
  id: string;
  name: string;
  courierId: string;
  inactive: boolean;
  rowVersion: string;
}

import * as _ from 'lodash';
import { Component } from '@angular/core';

import { ShippingMethodRestService } from '../shipping-method-rest.service';

import { BaseCreateBComponent } from '../../../shared/base/base.module';
import { MShippingMethod } from '../shipping-method.model';

@Component({
  selector: 'app-shipping-method-create',
  templateUrl: 'shipping-method-create.component.html',
})

export class ShippingMethodCreateComponent extends BaseCreateBComponent {
  constructor(
    private _shippingMethodRest: ShippingMethodRestService,
  ) {
    super();

    this.componentId = 'ShippingMethodCreate';
    this.routeURL = '/shipping-methods';
    this.entrySuccessI18n = 'success.shippingMethod.create';

    this.headerTitle = 'ui.shippingMethod.create.title';

    this.registerHook('save', event => {
      const formValue = this.form.value;
      let shippingMethod = new MShippingMethod;
      shippingMethod = _.assign(shippingMethod, this.doc, formValue);

      return this._shippingMethodRest.create(shippingMethod);
    });
  }
}

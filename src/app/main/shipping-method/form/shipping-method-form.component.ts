import { Component } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import 'app/rxjs-imports.ts';

import { AutocompleteService } from '../../../core/core.module';
import { ShippingMethodRestService } from '../shipping-method-rest.service';

import { BaseFormBComponent } from '../../../shared/base/base.module';

@Component({
  selector: 'app-shipping-method-form',
  templateUrl: 'shipping-method-form.component.html'
})
export class ShippingMethodFormComponent extends BaseFormBComponent {
  constructor(
    public _autocomplete: AutocompleteService,
    private _shippingMethodRest: ShippingMethodRestService
  ) {
    super();
    this.componentId = 'ShippingMethodForm';

    this.registerHook('buildForm', event => {
      this.buildForm();
    });
  }

  buildForm() {
    const nameControl = new FormControl('', [Validators.required, Validators.maxLength(50)]);
    (<any>nameControl).validatorData = {
      maxLength: 50
    };

    this.form.addControl('name', nameControl);
  }

}

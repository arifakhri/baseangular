import { Injectable } from '@angular/core';
import 'app/rxjs-imports.ts';

import { ApiQueryOption, RequestService } from '../../core/core.module';

import { APP_CONST } from '../../app.const';

@Injectable()
export class ProductVariantRestService {
  baseURL = `${APP_CONST.API_MAIN}/product-variants`;
  baseURLPicker = `${APP_CONST.API_MAIN}/pickers/product-variants`;

  request = this._request.new(this.baseURL);
  requestPicker = this._request.new(this.baseURLPicker);

  constructor(
    private _request: RequestService,
  ) { }

  findAll(queryOption: ApiQueryOption, queryParams: any = {}) {
    return this.request.post<IApiPaginationResult<IProductVariant>>(`q`, queryOption, { params: queryParams });
  }

  findAllPicker(queryOption: ApiQueryOption, queryParams: any = {}) {
    return this.requestPicker.post<IApiPaginationResult<IProductVariant>>(``, queryOption, { params: queryParams });
  }

  load(productVariantId: string) {
    return this.request.get<IProductVariant>(`${productVariantId}`);
  }

  loadSalesRelatedData(productVariantId: string) {
    return this.request.get<IProductVariantSalesRelatedData>(`${productVariantId}/sales-entry-related-data`);
  }

  loadPurchasesRelatedData(productVariantId: string) {
    return this.request.get<IProductVariantSalesRelatedData>(`${productVariantId}/purchase-entry-related-data`);
  }
}

declare interface IProductVariant {
  product: {
    masterVariant: {
      id: string;
      barcode: string;
      name: string;
      variantName: string;
      unitPrice: number;
      unitCost: number;
    };
    id: string;
    productType: string;
    code: string;
    name: string;
    expenseAccountId: string;
    incomeAccountId: string;
    categoryId: string;
    sellable: boolean;
    salesTaxId: string;
    salesDescription: string;
    purchasable: boolean;
    purchaseTaxId: string;
    purchaseDescription: string;
    uomId: string;
    variantCount: number;
    inactive: boolean;
    category: {
      id: string;
      name: string;
    };
    salesTax: ITax;
    purchaseTax: ITax;
    uom: IUOM;
  };
  id: string;
  productId: string;
  sku: string;
  name: string;
  variantName: string;
  attribute1Value: string;
  attribute2Value?: string;
  attribute3Value?: string;
  unitCost?: number;
  isMaster?: boolean;
  inactive: boolean;
  rowVersion: string;
  masterPrice: {
    unitPrice: number;
    discountPercent: number;
    discountAmount: number;
    unitPrice2: number;
    discountPercent2: number;
    discountAmount2: number;
    unitPrice3: number;
    discountPercent3: number;
    discountAmount3: number;
  };
  inventory: {
    qty: number;
    totalCost: number;
    averageCost: number;
    lastCost: number;
  }
}
declare interface IProductVariantSalesRelatedData {
  inventory: {
    qty: number;
    totalCost: number;
    averageCost: number;
    lastCost: number;
  };
  prices: [
    {
      priceLevelId: string;
      productId: string;
      productVariantId: string;
      unitPrice: number;
      discountPercent: number;
      discountAmount: number;
      unitPrice2: number;
      discountPercent2: number;
      discountAmount2: number;
      unitPrice3: number;
      discountPercent3: number;
      discountAmount3: number;
      priceLevel: {
        id: string;
        name: string;
        isMaster: boolean;
      }
    }
  ]
}
import { NgModule } from '@angular/core';

import { ProductVariantRestService } from './product-variant-rest.service';

export const PROVIDERS = [
  ProductVariantRestService,
];

@NgModule({})
export class ProductVariantModule { }

export * from './product-variant-rest.service';

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ProductTagCreateComponent } from './create/product-tag-create.component';
import { ProductTagListComponent } from './list/product-tag-list.component';
import { ProductTagUpdateComponent } from './update/product-tag-update.component';

import { AuthorizationService } from '../../core/core.module';

export const routes: Routes = [{
  path: '',
  children: [{
    path: '',
    component: ProductTagListComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'productTag.list'
    }
  }, {
    path: 'create',
    component: ProductTagCreateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'productTag.create'
    }
  }, {
    path: ':id/update',
    component: ProductTagUpdateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'productTag.update'
    }
  }]
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class ProductTagRoutingModule { }

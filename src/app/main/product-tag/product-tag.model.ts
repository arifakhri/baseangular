export class MProductTag {
    id: string;
    name: string;
    inactive: boolean;
}

import { NgModule } from '@angular/core';

import { ProductTagModule } from './product-tag.module';
import { ProductTagRoutingModule } from './product-tag-routing.module';

@NgModule({
  imports: [
    ProductTagModule,
    ProductTagRoutingModule
  ]
})
export class ProductTagLazyModule { }

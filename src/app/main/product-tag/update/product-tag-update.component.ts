import * as _ from 'lodash';
import { Component } from '@angular/core';

import { ProductTagRestService } from '../product-tag-rest.service';

import { BaseUpdateBComponent } from '../../../shared/base/base.module';
import { MProductTag } from '../product-tag.model';

@Component({
  selector: 'app-product-tag-update',
  templateUrl: 'product-tag-update.component.html',
})
export class ProductTagUpdateComponent extends BaseUpdateBComponent {
  constructor(
    private _productTagRest: ProductTagRestService,
  ) {
    super();

    this.componentId = 'ProductTagUpdate';
    this.routeURL = '/product-tags';
    this.entrySuccessI18n = 'success.productTag.update';

    this.headerTitle = 'ui.productTag.update.title';

    this.registerHook('load', event => {
      return this._productTagRest.load(this.page.routeParams.id);
    });

    this.registerHook('save', event => {
      const formValue = this.form.value;
      let productTag = new MProductTag;
      productTag = _.assign(productTag, this.doc, formValue);

      return this._productTagRest.update(this.page.routeParams.id, productTag);
    });
  }
}

import { Injectable } from '@angular/core';
import 'app/rxjs-imports.ts';

import { ApiQueryOption, RequestService } from '../../core/core.module';

import { APP_CONST } from '../../app.const';

@Injectable()
export class ProductTagRestService {
  baseURL = `${APP_CONST.API_MAIN}/product-tags`;
  baseURLPicker = `${APP_CONST.API_MAIN}/pickers/product-tags`;

  request = this._request.new(this.baseURL);
  requestPicker = this._request.new(this.baseURLPicker);

  constructor(
    private _request: RequestService,
  ) { }

  autoCompleteSearch(query: string) {
    return this.requestPicker.post<any>(``, {
      filter: [{
        filterValues: [{
          field: 'name',
          operator: 'contains',
          value: query
        }]
      }],
      take: 10
    }).map(response => response.data);
  }

  create(productTag: IProductTag) {
    return this.request.post<IProductTag>(``, productTag);
  }

  findAll(queryOption: ApiQueryOption) {
    return this.request.post<IApiPaginationResult<IProductTag>>(`q`, queryOption);
  }

  load(productTagId: string) {
    return this.request.get<IProductTag>(`${productTagId}`);
  }

  update(productTagId: string, updateObj: IProductTag) {
    return this.request.put<IProductTag>(`${productTagId}`, updateObj);
  }

  delete(productTagId: string) {
    return this.request.delete<any>(`${productTagId}`);
  }

  countProductsUsed(productTagId: string) {
    return this.request.get<number>(`${productTagId}/product-count`);
  }

  toggleInactive(productTagId: string, inactive: boolean) {
    return this.request.put<any>(`${productTagId}/mark-active/${!inactive}`, null, {
      headers: {
        'Content-Type': 'application/json'
      }
    });
  }
}

import * as _ from 'lodash';
import { Component } from '@angular/core';

import { ProductTagRestService } from '../product-tag-rest.service';

import { BaseCreateBComponent } from '../../../shared/base/base.module';
import { MProductTag } from '../product-tag.model';

@Component({
  selector: 'app-product-tag-create',
  templateUrl: 'product-tag-create.component.html',
})
export class ProductTagCreateComponent extends BaseCreateBComponent {
  constructor(
    private _productTagRest: ProductTagRestService,
  ) {
    super();

    this.componentId = 'ProductTagCreate';
    this.routeURL = '/product-tags';
    this.entrySuccessI18n = 'success.productTag.create';

    this.headerTitle = 'ui.productTag.create.title';

    this.registerHook('save', event => {
      const formValue = this.form.value;
      let productTag = new MProductTag;
      productTag = _.assign(productTag, formValue);

      return this._productTagRest.create(productTag);
    });
  }
}

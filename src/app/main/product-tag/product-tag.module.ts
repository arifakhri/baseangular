import { BsDropdownModule, PaginationModule } from 'ngx-bootstrap';
import { CommonModule } from '@angular/common';
import { DataTableModule } from 'primeng/components/datatable/datatable';
import { ListboxModule } from 'primeng/components/listbox/listbox';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { ProductTagCreateComponent } from './create/product-tag-create.component';
import { ProductTagFormComponent } from './form/product-tag-form.component';
import { ProductTagListComponent } from './list/product-tag-list.component';
import { ProductTagUpdateComponent } from './update/product-tag-update.component';

import { ProductTagRestService } from './product-tag-rest.service';

import { CoreModule } from '../../core/core.module';
import { SharedModule } from '../../shared/shared.module';

export const PROVIDERS = [
  ProductTagRestService,
];

@NgModule({
  imports: [
    CommonModule,
    FlexLayoutModule,
    PaginationModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    TranslateModule,
    BsDropdownModule,
    DataTableModule,
    ListboxModule,
    CoreModule,
    SharedModule,
  ],
  declarations: [
    ProductTagCreateComponent,
    ProductTagFormComponent,
    ProductTagListComponent,
    ProductTagUpdateComponent
  ],
})
export class ProductTagModule { }

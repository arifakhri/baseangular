declare interface IProductTag {
  id: string;
  name: string;
  inactive: boolean;
}
import * as _ from 'lodash';
import { Component, Input, OnInit } from '@angular/core';

import { AccountingService, } from '../../../core/core.module';
import { PurchasesRestService } from '../purchases-rest.service';

@Component({
  selector: 'app-purchases-payment-credit-list',
  templateUrl: './purchases-payment-credit-list.component.html',
})
export class PurchasesPaymentCreditListComponent implements OnInit {
  @Input() transaction: any;

  paymentAndCredits: any;
  total: number = 0;
  loading: boolean = false;

  constructor(
    public _accounting: AccountingService,
    private _purchasesRest: PurchasesRestService,
  ) { }

  ngOnInit() {
    this.loading = true;
    this._purchasesRest.getTransactionPaymentsAndCredits(
      this.transaction.transactionType,
      this.transaction.id,
    ).subscribe(
      response => {
        this.loading = false;
        this.paymentAndCredits = response;
        this.total = _.sumBy(this.paymentAndCredits, 'amount');
      },
      error => {
        this.loading = false;
      });
  }
}

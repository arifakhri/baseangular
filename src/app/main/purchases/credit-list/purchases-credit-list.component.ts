import * as _ from 'lodash';
import { Component, Input, OnInit } from '@angular/core';

import { AccountingService, } from '../../../core/core.module';
import { PurchasesRestService } from '../purchases-rest.service';

@Component({
  selector: 'app-purchases-credit-list',
  templateUrl: './purchases-credit-list.component.html',
})
export class PurchasesCreditListComponent implements OnInit {
  @Input() transaction: any;

  credits: any;
  total: number = 0;

  constructor(
    public _accounting: AccountingService,
    private _purchasesRest: PurchasesRestService,
  ) { }

  ngOnInit() {
    this._purchasesRest.findAllCreditAllocations(
      this.transaction.transactionType,
      this.transaction.id,
    ).subscribe(credits => {
      this.credits = credits.data;
      this.total = _.sumBy(this.credits, 'total');
    });
  }
}

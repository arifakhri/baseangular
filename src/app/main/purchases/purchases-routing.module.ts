import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PurchasesAllComponent } from './all/purchases-all.component';

import { AuthorizationService } from '../../core/core.module';

export const routes: Routes = [{
  path: '',
  children: [{
    path: '',
    redirectTo: 'all',
    pathMatch: 'full',
  }, {
    path: 'all',
    component: PurchasesAllComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'purchases.all'
    }
  }, {
    path: 'orders',
    loadChildren: '../purchases-order/purchases-order-lazy.module#PurchasesOrderLazyModule'
  }, {
    path: 'debit-notes',
    loadChildren: '../purchases-debit-note/purchases-debit-note-lazy.module#PurchasesDebitNoteLazyModule'
  }, {
    path: 'down-payments',
    loadChildren: '../purchases-down-payment/purchases-down-payment-lazy.module#PurchasesDownPaymentLazyModule'
  }, {
    path: 'invoices',
    loadChildren: '../purchases-invoice/purchases-invoice-lazy.module#PurchasesInvoiceLazyModule'
  }, {
    path: 'payments',
    loadChildren: '../purchases-payment/purchases-payment-lazy.module#PurchasesPaymentLazyModule'
  }, {
    path: 'receipts',
    loadChildren: '../purchases-receipt/purchases-receipt-lazy.module#PurchasesReceiptLazyModule'
  }, {
    path: 'refunds',
    loadChildren: '../purchases-refund/purchases-refund-lazy.module#PurchasesRefundLazyModule'
  }]
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class PurchasesRoutingModule { }

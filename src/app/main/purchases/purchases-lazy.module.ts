import { NgModule } from '@angular/core';

import { PurchasesModule } from './purchases.module';
import { PurchasesRoutingModule } from './purchases-routing.module';

@NgModule({
  imports: [
    PurchasesModule,
    PurchasesRoutingModule
  ]
})
export class PurchasesLazyModule { }

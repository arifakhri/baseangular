import * as _ from 'lodash';
import { Component, EventEmitter, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';

import { PurchasesInvoiceRestService } from '../../purchases-invoice/purchases-invoice-rest.service';

@Component({
  selector: 'app-purchases-find-invoice-by-number',
  templateUrl: 'purchases-find-invoice-by-number.component.html',
})
export class PurchasesFindInvoiceByNumberComponent {
  @Output() cancel: EventEmitter<boolean> = new EventEmitter;
  @Output() success: EventEmitter<IPurchasesInvoice> = new EventEmitter;

  form: FormGroup = new FormGroup({});

  constructor(
    private _purchasesInvoiceRest: PurchasesInvoiceRestService,
  ) {
    this.form.addControl('transactionNumber', new FormControl('', Validators.required));
  }

  onSubmit() {
    if (this.form.valid) {
      this.findInvoice()
        .catch(obsError => {
          this.invoiceNotFound();
          return Observable.throw(obsError);
        })
        .subscribe(invoice => {
          if (!invoice) {
            this.invoiceNotFound();
          } else {
            this.success.emit(invoice);
          }
        });
    }
  }

  findInvoice() {
    const { transactionNumber } = this.form.value;

    return this._purchasesInvoiceRest.findAll({
      filter: [{
        filterValues: [{
          field: 'transactionNumber',
          operator: 'eq',
          value: transactionNumber,
        }]
      }],
      skip: 0,
      includeTotalCount: false,
      take: 1,
    }).map(records => _.first(records.data));
  }

  invoiceNotFound() {
    this.form.get('transactionNumber').setErrors({
      recordNotFound: true,
    });
  }
}

import { Injectable } from '@angular/core';
import Axios, { AxiosInstance, AxiosResponse } from 'axios';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';

import { APP_CONST } from '../../app.const';
import { ApiQueryOption, AuthenticationService } from '../../core/core.module';
import { ProductVariantRestService } from '../product-variant/product-variant-rest.service';

@Injectable()
export class PurchasesRestService {
  axios: AxiosInstance;
  axiosCreditAllocation: AxiosInstance;
  axiosOrderParse: AxiosInstance;

  baseURL = `${APP_CONST.API_MAIN}/purchases`;
  baseURLCreditAllocation = `${APP_CONST.API_MAIN}/purchases/credit-allocations`;
  baseURLParse = `${APP_CONST.API_MAIN}/purchases-order-parsing`;

  constructor(
    private _auth: AuthenticationService,
    public _productVariantRest: ProductVariantRestService,
  ) {
    this.axios = Axios.create();
    this.axios.defaults.baseURL = this.baseURL;
    _auth.axiosInterceptors(this.axios);

    this.axiosOrderParse = Axios.create();
    this.axiosOrderParse.defaults.baseURL = this.baseURLParse;
    _auth.axiosInterceptors(this.axiosOrderParse);

    this.axiosCreditAllocation = Axios.create();
    this.axiosCreditAllocation.defaults.baseURL = this.baseURLCreditAllocation;
    _auth.axiosInterceptors(this.axiosCreditAllocation);
  }

  findAll(queryOption: ApiQueryOption, queryParams: any = {}): Observable<IApiPaginationResult<IPurchases>> {
    return Observable.fromPromise(
      <Promise<AxiosResponse>>this.axios.post(`all/q`, queryOption, {
        params: queryParams,
      })
    ).map(response => response.data);
  }

  findAllProducts(queryOption: ApiQueryOption = new ApiQueryOption): Observable<IApiPaginationResult<IProductVariant>> {
    return this._productVariantRest.findAll(queryOption);
  }

  findAllCreditAllocations(
    transactionType: string,
    transactionId: string,
    customerId?: string,
  ): Observable<IApiPaginationResult<IPurchaseAllocatedCredit>> {
    return Observable.fromPromise(
      <Promise<AxiosResponse>>this.axiosCreditAllocation.get('', {
        params: {
          targetTransactionType: transactionType,
          targetTransactionId: transactionId,
          customerId,
          options: {
            take: 100,
          },
        }
      })
    ).map(response => response.data);
  }

  getTransactionPaymentsAndCredits(
    transactionType: string,
    transactionId: string,
  ): Observable<IApiPaginationResult<IPurchasesPaymentAndCredit>> {
    return Observable.fromPromise(
      <Promise<AxiosResponse>>this.axios.get(`payments-and-credits/${transactionType}/${transactionId}`)
    ).map(response => response.data);
  }

  validateParsingData(customQueryOption: {
    vendorName: string;
    vendorEmail: string;
    products: string[];
  }) {
    return Observable.fromPromise(
      <Promise<AxiosResponse>>this.axiosOrderParse.post(`match-data`, customQueryOption)
    ).map(response => <{
      vendors: [
        {
          id: string,
          displayName: string,
          email: string,
          mobile: string
        }
      ],
      lines: [
        {
          parseText: string,
          productId: string,
          productVariantId: string,
          sku: string,
          name: string,
          salesDescription: string
        }
      ]
    }>response.data);
  }
}

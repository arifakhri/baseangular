declare interface IPurchases {
  dueDate: string | Date;
  lines: [
    {
      id: string;
      transactionId: string;
      lineNumber: number;
      productId: string;
      productVariantId: string;
      description: string;
      qty: number;
      unitPrice: number;
      discountPercent: number;
      discountAmount: number;
      totalBeforeDiscount: number;
      totalDiscount: number;
      total: number;
      uomId: string;
      uomConversion: number;
      baseQty: number;
      taxId: string;
      taxRate: number;
      taxAmount: number;
      note: string;
      rowVersion: string;
      product: IProduct;
      productVariant: IProductVariant;
      uom: {
        id: string;
        name: string;
        systemDefault: boolean
      };
      tax: {
        id: string;
        code: string;
        name: string;
        rate: number
      }
    }
  ];
  id: string;
  transactionId: string;
  transactionType: string;
  transactionNumber: string;
  transactionDate: string | Date;
  vendorId: string;
  vendorRefNumber: string;
  vendorEmail: string;
  dropship: boolean;
  dropshipCustomerName: string;
  dropshipCustomerPhone: string;
  billingAddress: string;
  shippingAddress: string;
  lineTotalBeforeDiscount: number;
  lineTotalDiscount: number;
  subtotal: number;
  discountPercent: number;
  discountAmount: number;
  taxAmount: number;
  shippingCharge: number;
  adjustmentAmount: number;
  total: number;
  taxed: boolean;
  note: string;
  vendorNote: string;
  status: string;
  state: string;
  rowVersion: string;
  vendor: {
    salesTaxable: boolean;
    defaultPurchaseTaxId: string;
    defaultPurchaseInvoiceDueDays: number;
    purchaseTaxable: boolean;
    defaultPurchasesTaxId: string;
    defaultPurchasesInvoiceDueDays: number;
    isCustomer: boolean;
    isVendor: boolean;
    id: string;
    contactType: string;
    code: string;
    displayName: string;
    company: string;
    fullName: string;
    phone: string;
    mobile: string;
    fax: string;
    email: string;
    website: string;
    inactive: boolean;
    postalAddress: {
      attention: string;
      street1: string;
      street2: string;
      city: string;
      stateProvince: string;
      countryId: string;
      postalCode: string;
      longitude: number;
      latitude: number;
      country: {
        code: string;
        name: string
      }
    }
  }
}

declare interface IPurchasesAllocateCredit {
  transactionType: string;
  transactionId: string;
  allocateAmount: number;
}

declare interface IPurchaseAllocatedCredit {
  targetTransaction: {
    id: string;
    transactionType: string;
    transactionNumber: string;
    transactionDate: string | Date;
    vendorId: string;
    vendorRefNumber: string;
    billingAddress: string;
    shippingAddress: string;
    warehouseId: string;
    total: number;
    status: string;
    state: string;
    vendor: {
      salesTaxable: boolean;
      purchaseTaxable: boolean;
      defaultPurchaseTaxId: string;
      defaultPurchaseInvoiceDueDays: number;
      isCustomer: boolean;
      isVendor: boolean;
      arBalance: number;
      apBalance: number;
      defaultPurchaseTax: {
        id: string;
        code: string;
        name: string;
        rate: number
      };
      id: string;
      contactType: string;
      code: string;
      displayName: string;
      company: string;
      fullName: string;
      phone: string;
      mobile: string;
      fax: string;
      email: string;
      website: string;
      inactive: boolean;
      postalAddress: {
        attention: string;
        street1: string;
        street2: string;
        city: string;
        stateProvince: string;
        countryId: string;
        postalCode: string;
        longitude: number;
        latitude: number;
        country: {
          id: string;
          name: string;
          phoneCountryCode: string
        };
        fullAddress: string
      }
    };
    warehouse: {
      id: string;
      name: string;
      isMaster: boolean
    }
  };
  offsetTransaction: {
    id: string;
    transactionType: string;
    transactionNumber: string;
    transactionDate: string | Date;
    vendorId: string;
    vendorRefNumber: string;
    billingAddress: string;
    shippingAddress: string;
    warehouseId: string;
    total: number;
    status: string;
    state: string;
    vendor: {
      purchaseTaxable: boolean;
      defaultPurchaseTaxId: string;
      defaultPurchaseInvoiceDueDays: number;
      isCustomer: boolean;
      isVendor: boolean;
      arBalance: number;
      apBalance: number;
      defaultPurchaseTax: {
        id: string;
        code: string;
        name: string;
        rate: number
      };
      defaultPurchasePriceLevel: {
        id: string;
        name: string;
        isMaster: boolean
      };
      id: string;
      contactType: string;
      code: string;
      displayName: string;
      company: string;
      fullName: string;
      phone: string;
      mobile: string;
      fax: string;
      email: string;
      website: string;
      inactive: boolean;
      postalAddress: {
        attention: string;
        street1: string;
        street2: string;
        city: string;
        stateProvince: string;
        countryId: string;
        postalCode: string;
        longitude: number;
        latitude: number;
        country: {
          id: string;
          name: string;
          phoneCountryCode: string
        };
        fullAddress: string
      }
    };
    warehouse: {
      id: string;
      name: string;
      isMaster: boolean
    }
  };
  vendor: {
    id: string;
    contactType: string;
    code: string;
    displayName: string;
    company: string;
    fullName: string;
    phone: string;
    mobile: string;
    fax: string;
    email: string;
    website: string;
    inactive: boolean;
    postalAddress: {
      attention: string;
      street1: string;
      street2: string;
      city: string;
      stateProvince: string;
      countryId: string;
      postalCode: string;
      longitude: number;
      latitude: number;
      country: {
        id: string;
        name: string;
        phoneCountryCode: string
      };
      fullAddress: string
    }
  };
  transactionType: string;
  targetTransactionType: string;
  targetTransactionId: string;
  offsetTransactionType: string;
  offsetTransactionId: string;
  vendorId: string;
  amount: number;
  note: string
}

declare interface IPurchasesPaymentAndCredit {
  transactionType: string;
  transactionId: string;
  transactionDate: string;
  transactionNumber: string;
  transactionDescription: string;
  offsetTransactionType: string;
  offsetTransactionId: string;
  offsetTransactionDate: string;
  offsetTransactionNumber: string;
  offsetTransactionDescription: string;
  amount: number;
}

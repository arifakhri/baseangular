import * as _ from 'lodash';
import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-purchases-parse-step1',
  templateUrl: 'purchases-parse-step1.component.html',
})
export class PurchasesParseStep1Component implements OnChanges {
  @Input() form: FormGroup;
  @Input() formState: any;

  constructor() { }

  ngOnChanges(changes: SimpleChanges) {
    if (_.has(changes, 'form')) {
      if (this.form && !this.form.controls.length) {
        this.form.addControl('raw', new FormControl(`Nama : Mr. Vendor One
Alamat : itc roxy mas blok c3 no 33 jakarta pusat
No hp : 081313222293
Email : vendoroneparse@mail.com
Orderan : 
-5 x jeans 1 / 38
-2 x blouse 1
-3 x blouse 2`, Validators.required));
      }
    }
  }

}

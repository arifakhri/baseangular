export const ACTIONS = {
  SET: 'PPFormStateSet',
  SETATTR: 'PPFormStateSetAttr',
  RESET: 'PPFormStateReset',
};

export const STORE_ID = 'PurchasesParseFormStateStore';

export function STORE (state: any = {}, action) {
  switch (action.type) {
    case ACTIONS.SET:
      return {
        ...state,
        [action.payload.key]: action.payload.value,
      };
    case ACTIONS.SETATTR:
      const newState = state;
      newState[action.payload.key][action.payload.attr] = action.payload.value;
      return newState;
    case ACTIONS.RESET:
      return {};
    default:
      return state;
  }
}

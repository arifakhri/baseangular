import { Component, Input, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';

import { PurchasesRestService } from '../../purchases-rest.service';

@Component({
  selector: 'app-purchases-parse-step2',
  templateUrl: 'purchases-parse-step2.component.html'
})
export class PurchasesParseStep2Component implements OnInit {
  @Input() form: FormGroup;
  @Input() formState: any;

  productVariantsSuggestion: IProductVariant[] = [];

  constructor(
    private _purchasesRest: PurchasesRestService
  ) { }

  ngOnInit() {
    this.form.addControl('lines', new FormArray([], Validators.required));
    this.form.addControl('vendor', new FormControl(''));

    if (this.formState[1].vendors.length > 1) {
      this.form.get('vendor').setValue(this.formState[1].vendors[0]);
    }

    this.formState[1].lines.forEach((productQueryResult, idx) => {
      const productControl = new FormControl(null, Validators.required);

      if (productQueryResult.productVariant) {
        productControl.setValue(productQueryResult.productVariant);
      }

      (<FormArray>this.form.get('lines')).push(productControl);
    });
  }

  searchProductVariant(ev) {
    this._purchasesRest.findAllProducts({
      filter: [{
        filterValues: [{
          field: 'name',
          operator: 'contains',
          value: ev.query
        }, {
          field: 'sku',
          operator: 'contains',
          value: ev.query
        }]
      }],
      sort: [{
        field: 'name',
        dir: 'asc'
      }],
      take: 30,
      skip: 0,
      includeTotalCount: false
    }).subscribe(productVariants => {
      this.productVariantsSuggestion = productVariants.data;
    });
  }

  onProductVariantDropdown(ev: any) {
    this.productVariantsSuggestion = [];
    if (ev.query.length) {
      this.searchProductVariant(ev);
    } else {
      this._purchasesRest.findAllProducts({
        filter: [],
        sort: [{
          field: 'name',
          dir: 'asc'
        }],
        take: 30,
        skip: 0,
        includeTotalCount: false
      }).subscribe(productVariants => {
        this.productVariantsSuggestion = productVariants.data;
      });
    }
  }
}

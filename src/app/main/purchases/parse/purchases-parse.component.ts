import * as _ from 'lodash';
import { ChangeDetectorRef, Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ModalDirective } from 'ngx-bootstrap';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';
import { Store } from '@ngrx/store';

import { BusinessPartnerRestService } from '../../business-partner/business-partner-rest.service';
import * as PurchasesParseFormStepStore from './purchases-parse-form-step.store';
import * as PurchasesParseFormStateStore from './purchases-parse-form-state.store';
import { PurchasesService } from '../purchases.service';
import { PurchasesRestService } from '../purchases-rest.service';

@Component({
  selector: 'app-purchases-parse',
  templateUrl: 'purchases-parse.component.html'
})
export class PurchasesParseComponent implements OnChanges, OnInit {
  @Input() show: boolean = false;
  @Output() onFinished: EventEmitter<any> = new EventEmitter();
  @Output() close: EventEmitter<boolean> = new EventEmitter();
  @ViewChild('purchasesParseModal') elPurchasesParseModal: ModalDirective;

  form: FormGroup;

  formStep$: Observable<number>;
  formState$: Observable<any>;

  formStep: number;
  formState: any;
  formProcessing: boolean = false;

  finished: boolean = false;
  maxStep: number = 2;

  results: any = {};

  constructor(
    private _businessPartnerRest: BusinessPartnerRestService,
    private _changeDetectorRef: ChangeDetectorRef,
    private _purchases: PurchasesService,
    private _purchasesRest: PurchasesRestService,
    private _store: Store<any>,
  ) {
    this.buildForm();

    this.formStep$ = _store.select(PurchasesParseFormStepStore.STORE_ID) as any;
    this.formState$ = _store.select(PurchasesParseFormStateStore.STORE_ID) as any;

    this.formStep$.subscribe(formStep => this.formStep = formStep);
    this.formState$.subscribe(formState => {
      return this.formState = formState;
    });
  }

  return() {
    this.results.states = this.formState;
    this.onFinished.emit(this.results);
    this.elPurchasesParseModal.hide();
    this.finished = true;
  }

  buildForm() {
    this.form = new FormGroup({});
  }

  ngOnInit() {
    this.elPurchasesParseModal.onHidden.subscribe(() => {
      this.close.emit(true);
      this.reset();
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (_.has(changes, 'show')) {
      if (this.show && !this.elPurchasesParseModal.isShown) {
        this.elPurchasesParseModal.show();
      } else if (!this.show && this.elPurchasesParseModal.isShown) {
        this.elPurchasesParseModal.hide();
      }
    }
  }

  async formNext() {
    if (!this.form.valid) {
      return;
    }

    this.formProcessing = true;

    this._store.dispatch({
      type: PurchasesParseFormStateStore.ACTIONS.SET, payload: {
        key: this.formStep,
        value: this.form.value
      }
    });

    if (this.formStep === 1) {
      await this.form1Process(this.form.value);
    }

    if (this.formStep === 2) {
      await this.form2Process(this.form.value);
    }

    if (this.finished) {
      this.finished = false;
      return;
    }

    this._store.dispatch({ type: PurchasesParseFormStepStore.ACTIONS.NEXT });

    this.buildForm();
    this._changeDetectorRef.detectChanges();
    this.form.patchValue(_.get(this.formState, this.formStep, {}));

    this.formProcessing = false;
  }

  formPrev() {
    this._store.dispatch({ type: PurchasesParseFormStepStore.ACTIONS.PREV });

    this.buildForm();
    this._changeDetectorRef.detectChanges();
    this.form.patchValue(_.get(this.formState, this.formStep, {}));
  }

  reset() {
    this._store.dispatch({ type: PurchasesParseFormStateStore.ACTIONS.RESET });
    this._store.dispatch({ type: PurchasesParseFormStepStore.ACTIONS.RESET });
    this.formProcessing = false;
    this.buildForm();
  }

  async form1Process(formValue: any) {
    const parsedData: any = this._purchases.parseOrderText(formValue.raw);

    const { vendors, lines } = await this._purchasesRest.validateParsingData({
      ...parsedData,
      lines: parsedData.lines.map(p => p.productName),
    }).toPromise();

    this._store.dispatch({
      type: PurchasesParseFormStateStore.ACTIONS.SETATTR,
      payload: { key: 1, attr: 'parsedData', value: parsedData }
    });

    if (vendors.length === 1 && _.filter(lines, (p) => !(<any>p).productVariant).length === 0) {
      this.results = Object.assign({}, { vendor: _.first(vendors), lines: _(lines).map(p => (<any>p).productVariant) });
      this.return();
    } else {
      this._store.dispatch({
        type: PurchasesParseFormStateStore.ACTIONS.SETATTR,
        payload: { key: 1, attr: 'vendors', value: vendors }
      });
      this._store.dispatch({
        type: PurchasesParseFormStateStore.ACTIONS.SETATTR,
        payload: { key: 1, attr: 'lines', value: lines }
      });
    }
  }

  async form2Process(formValue: any) {
    if (!formValue.vendor) {
      const targetVendor: any = {
        isCustomer: false,
        isVendor: true,
      };
      targetVendor.displayName = this.formState[1].parsedData.vendorName;

      if (this.formState[1].parsedData.vendorEmail) {
        targetVendor.email = this.formState[1].parsedData.vendorEmail;
      }

      formValue.vendor = await this._businessPartnerRest.create(targetVendor).toPromise();
    }
    this.results = Object.assign({}, formValue);
    this.return();
  }
}

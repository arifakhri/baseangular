import { Action } from '@ngrx/store';

export const ACTIONS = {
  NEXT: 'PPFormStepNext',
  PREV: 'PPFormStepPrev',
  RESET: 'PPFormStepReset',
};

export const STORE_ID = 'PurchasesParseFormStepStore';

export function STORE(step: number = 1, action: Action) {
  switch (action.type) {
    case ACTIONS.NEXT:
      return step + 1;
    case ACTIONS.PREV:
      return step - 1;
    case ACTIONS.RESET:
      return 1;
    default:
      return step;
  }
}

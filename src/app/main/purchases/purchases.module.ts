import { AutoCompleteModule } from 'primeng/components/autocomplete/autocomplete';
import { CalendarModule } from 'primeng/components/calendar/calendar';
import { DataTableModule } from 'primeng/components/datatable/datatable';
import { ListboxModule } from 'primeng/components/listbox/listbox';
import { BsDropdownModule, ModalModule, PaginationModule, PopoverModule } from 'ngx-bootstrap';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { PurchasesAllComponent } from './all/purchases-all.component';
import { PurchasesButtonAddComponent } from './button-add/purchases-button-add.component';
import { PurchasesFindInvoiceByNumberComponent } from './find-invoice-by-number/purchases-find-invoice-by-number.component';
import { PurchasesMoreFilterComponent } from './more-filter/purchases-more-filter.component';
import { PurchasesTabLinksComponent } from './tab-links/purchases-tab-links.component';
import { PurchasesParseComponent } from './parse/purchases-parse.component';
import { PurchasesParseStep1Component } from './parse/step1/purchases-parse-step1.component';
import { PurchasesParseStep2Component } from './parse/step2/purchases-parse-step2.component';

import { PurchasesMoreFilterService } from './more-filter/purchases-more-filter.service';
import { PurchasesRestService } from './purchases-rest.service';
import { PurchasesService } from './purchases.service';

import { BusinessPartnerModule } from '../business-partner/business-partner.module';
import { CoreModule } from '../../core/core.module';
import { ProductVariantModule } from '../product-variant/product-variant.module';
import { SharedModule } from '../../shared/shared.module';

import * as PurchasesParseFormStepStore from './parse/purchases-parse-form-step.store';
import * as PurchasesParseFormStateStore from './parse/purchases-parse-form-state.store';

export const STORES = {};
STORES[PurchasesParseFormStepStore.STORE_ID] = PurchasesParseFormStepStore.STORE;
STORES[PurchasesParseFormStateStore.STORE_ID] = PurchasesParseFormStateStore.STORE;

export const PROVIDERS = [
  PurchasesMoreFilterService,
  PurchasesRestService,
  PurchasesService,
];

@NgModule({
  imports: [
    CalendarModule,
    CommonModule,
    FormsModule,
    ListboxModule,
    FlexLayoutModule,
    HttpModule,
    ReactiveFormsModule,
    RouterModule,
    TranslateModule,
    AutoCompleteModule,
    BsDropdownModule,
    DataTableModule,
    ModalModule,
    PaginationModule,
    CoreModule,
    SharedModule,
    PopoverModule,
    BusinessPartnerModule,
    ProductVariantModule,
  ],
  declarations: [
    PurchasesAllComponent,
    PurchasesButtonAddComponent,
    PurchasesFindInvoiceByNumberComponent,
    PurchasesMoreFilterComponent,
    PurchasesParseComponent,
    PurchasesParseStep1Component,
    PurchasesParseStep2Component,
    PurchasesTabLinksComponent,
  ],
  exports: [
    PurchasesButtonAddComponent,
    PurchasesFindInvoiceByNumberComponent,
    PurchasesParseComponent,
    PurchasesParseStep1Component,
    PurchasesParseStep2Component,
    PurchasesTabLinksComponent
  ],
})
export class PurchasesModule { }

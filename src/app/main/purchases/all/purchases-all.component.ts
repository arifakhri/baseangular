import * as _ from 'lodash';
import * as SnackBar from 'node-snackbar';
import swal from 'sweetalert2';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { DataTable } from 'primeng/components/datatable/datatable';
import { Observable, Subscription } from 'rxjs';
import 'app/rxjs-imports.ts';
import { PopoverDirective } from 'ngx-bootstrap';
import { TranslateService } from '@ngx-translate/core';

import { SpinnerDirective } from '../../../shared/spinner/spinner.directive';

import {
  AccountingService,
  ExportDataTableService,
  GridTableDataSource,
  GridTableService,
  GridTableToggleService,
  SystemMessageService,
} from '../../../core/core.module';
import { AdminLayoutService } from '../../../layouts/layouts.module';
import { PurchasesRestService } from '../purchases-rest.service';
import { PurchasesDebitNoteRestService } from '../../purchases-debit-note/purchases-debit-note-rest.service';
import { PurchasesDownPaymentRestService } from '../../purchases-down-payment/purchases-down-payment-rest.service';
import { PurchasesInvoiceRestService } from '../../purchases-invoice/purchases-invoice-rest.service';
import { PurchasesMoreFilterService } from '../more-filter/purchases-more-filter.service';
import { PurchasesOrderRestService } from '../../purchases-order/purchases-order-rest.service';
import { PurchasesPaymentRestService } from '../../purchases-payment/purchases-payment-rest.service';
import { PurchasesReceiptRestService } from '../../purchases-receipt/purchases-receipt-rest.service';
import { PurchasesRefundRestService } from '../../purchases-refund/purchases-refund-rest.service';
import { TransactionRoutingService } from '../../transaction/transaction-routing.service';
import { RetryDialogComponent } from '../../../shared/retry-dialog/retry-dialog.component';

@Component({
  selector: 'app-purchases-all',
  templateUrl: 'purchases-all.component.html',
  providers: [SystemMessageService]
})
export class PurchasesAllComponent implements OnInit {
  @ViewChild('gridTable') public gridTable: DataTable;
  @ViewChild('pageLoading') elPageLoading: SpinnerDirective;
  @ViewChild('popMoreFilter') elPopMoreFilter: PopoverDirective;
  @ViewChild('retryDialog') elRetryDialog: RetryDialogComponent;

  compReady: boolean;
  gridDataSource: GridTableDataSource<IPurchases> = new GridTableDataSource<IPurchases>();
  selectedRecords: IPurchases[] = [];
  purchasesType: string;

  qParams: any = {
    keyword: null
  };
  moreFilterValues: IGridTableFilterParsed;

  retryDialogShownState: boolean = false;
  retryDialogOnRetry: Function;

  tableColumns: IGridTableColumn[] = [];
  tableColumnsShow: boolean = false;
  tableColumnsToggle: any;

  private loadDataTimeoutId: number;
  private gridFirstLoad: boolean = true;
  private subscriptions: { [key: string]: Subscription[] } = {
    load: []
  };

  constructor(
    public _accounting: AccountingService,
    private _activatedRoute: ActivatedRoute,
    private _adminLayout: AdminLayoutService,
    private _gridTable: GridTableService,
    private _dataTableExport: ExportDataTableService,
    private _gridTableToggle: GridTableToggleService,
    private _purchasesRest: PurchasesRestService,
    private _purchasesCreditNoteRest: PurchasesDebitNoteRestService,
    private _purchasesDownPaymentRest: PurchasesDownPaymentRestService,
    private _purchasesInvoiceRest: PurchasesInvoiceRestService,
    private _purchasesMoreFilter: PurchasesMoreFilterService,
    private _purchasesOrderRest: PurchasesOrderRestService,
    private _purchasesPaymentRest: PurchasesPaymentRestService,
    private _purchasesReceiptRest: PurchasesReceiptRestService,
    private _purchasesRefundRest: PurchasesRefundRestService,
    public _systemMessage: SystemMessageService,
    private _translate: TranslateService,
    private _trasactionRoutingService: TransactionRoutingService
  ) {
    this._adminLayout.containerType = 1;
  }

  ngOnInit() {
    this.loadData();

    this.tableColumns = [{
      i18nLabel: 'ui.purchases.all.column.transactionDate',
      field: 'transactionDate',
      dateFormat: 'DD/MM/YYYY',
      sort: true,
    }, {
      i18nLabel: 'ui.purchases.all.column.transactionType',
      field: 'transactionType',
      sort: true,
      formatter: (value, row: IPurchases) => {
        return _.startCase(value);
      },
    }, {
      i18nLabel: 'ui.purchases.all.column.transactionNumber',
      field: 'transactionNumber',
      link: (row: IPurchases) => {
        const routesType = _.kebabCase(row.transactionType.substr(row.transactionType.indexOf('_') + 1));
        return `/purchases/${routesType}s/${row.transactionId}`;
      },
      sort: true,
    }, {
      i18nLabel: 'ui.purchases.all.column.vendorName',
      field: 'vendor.displayName',
      sort: true,
    }, {
      i18nLabel: 'ui.purchases.all.column.total',
      field: 'total',
      sort: true,
      formatter: (value, row: IPurchases) => {
        return this._accounting.ac.formatMoney(value, '');
      },
      columnClasses: 'right-align',
    }, {
      i18nLabel: 'ui.purchases.all.column.status',
      field: 'status',
      sort: true,
      formatter: (value, row: IPurchases) => {
        return _.startCase(value);
      },
    }];
    this.tableColumnsToggle = this._gridTableToggle.mapToggleOptionsFromColumns(this.tableColumns);
  }

  getUpdateLink(doc: any) {
    return this._trasactionRoutingService.resolve(doc, 'update');
  }

  gridLoadDataWrapper() {
    // Grid always load at the first time. we dont want that. we already call on ngOnInit
    if (this.gridFirstLoad) {
      this.gridFirstLoad = false;
      return;
    }

    if (this.loadDataTimeoutId) {
      window.clearTimeout(this.loadDataTimeoutId);
      this.loadDataTimeoutId = null;
    }

    this.loadDataTimeoutId = window.setTimeout(() => {
      this.loadData();
    }, 300);
  }

  populateQParams() {
    let newFilters: any = _.pick(this.qParams, ['keyword']);

    if (this.moreFilterValues) {
      newFilters = Object.assign({}, newFilters, this.moreFilterValues.qParams);
    }

    this.qParams = newFilters;
  }

  showVoidDialog(purchasesId: string, purchasesType: string) {
    if (purchasesType === 'purchase_receipt') {
      this.purchasesType = 'purchasesReceipt';
    } else {
      this.purchasesType = _.camelCase(
        purchasesType.slice(0, purchasesType.indexOf('_')) + 's' + purchasesType.slice(purchasesType.indexOf('_'))
      );
    }
    swal({
      title: this._translate.instant(`confirm.${this.purchasesType}.void.label`),
      text: this._translate.instant(`confirm.${this.purchasesType}.void.description`),
      type: 'question',
      showCancelButton: true,
    })
      .then(() => {
        this.voidPayment(purchasesId, this.purchasesType);
      })
      .catch(() => { });
  }

  voidPayment(purchasesId: string, type: string) {
    const restType = `_${type}Rest`;
    this[restType].void(purchasesId)
    .catch((error) => {
      this._systemMessage.log({
        message: error.response.data[0].errorMessage,
        type: 'error'
      });
      return Observable.throw(error);
    })
    .subscribe(response => {
      this.gridLoadDataWrapper();
      SnackBar.show({
        text: this._translate.instant(`success.${type}.void`),
        pos: 'bottom-right'
      });
    });
  }

  onMoreFilter(filters: IGridTableFilterParsed) {
    this.elPopMoreFilter.hide();

    this.moreFilterValues = filters;

    this.loadData();
  }

  get exportRecords(): Observable<IPurchases[]> {
    const qOption = {
      ...this._gridTable.generateApiQueryOptionFromGridInfo(
        this.gridDataSource.pager,
        false,
        this.gridTable,
      ),
      skip: 0,
      take: this._dataTableExport.exportLimit,
    };

    this.populateQParams();

    return this._purchasesRest.findAll(qOption, this.qParams).map(response => response.data);
  }

  exportPDF() {
    this.exportRecords.subscribe(records => {
      this._dataTableExport.export({
        records,
        mapOptions: this._dataTableExport.mapFromTableColumns(this.tableColumns),
        fileName: 'all-purchases',
        extension: 'pdf',
        templateData: {
          title: this._translate.instant('ui.purchases.all.title'),
        },
      });
    });
  }

  exportExcel() {
    this.exportRecords.subscribe(records => {
      this._dataTableExport.export({
        records: records,
        mapOptions: this._dataTableExport.mapFromTableColumns(this.tableColumns),
        fileName: 'all-purchases',
        extension: 'xls',
      });
    });
  }


  loadData() {
    this.elPageLoading.forceShow();

    const qOption = this._gridTable.generateApiQueryOptionFromGridInfo(
      this.gridDataSource.pager,
      true,
      this.gridTable,
    );

    this.populateQParams();

    this.subscriptions.load.forEach(sub => sub.unsubscribe());
    this.subscriptions.load.push(
      this.elRetryDialog.createRetryEntry(
        this._purchasesRest.findAll(qOption, this.qParams)
          .finally(() => {
            this.elPageLoading.forceHide();
          })
      ).subscribe(response => {
        this.gridDataSource.updateFromApiPaginationResult(<any>response);
        this.compReady = true;
      })
    );
  }

  public onGridChange() {
    this.gridLoadDataWrapper();
  }

  public onGridPageChange() {
    this.gridLoadDataWrapper();
  }
}

import { AutoCompleteModule } from 'primeng/components/autocomplete/autocomplete';
import { BsDropdownModule, PaginationModule } from 'ngx-bootstrap';
import { CheckboxModule } from 'primeng/components/checkbox/checkbox';
import { DataTableModule } from 'primeng/components/datatable/datatable';
import { ListboxModule } from 'primeng/components/listbox/listbox';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { TaxCreateComponent } from './create/tax-create.component';
import { TaxDetailComponent } from './detail/tax-detail.component';
import { TaxFormComponent } from './form/tax-form.component';
import { TaxListComponent } from './list/tax-list.component';
import { TaxUpdateComponent } from './update/tax-update.component';

import { TaxRestService } from './tax-rest.service';
import { TaxService } from './tax.service';

import { CoreModule } from '../../core/core.module';
import { SharedModule } from '../../shared/shared.module';

export const PROVIDERS = [
  TaxRestService,
  TaxService,
];

@NgModule({
  imports: [
    AutoCompleteModule,
    BsDropdownModule,
    CheckboxModule,
    CommonModule,
    CoreModule,
    DataTableModule,
    FlexLayoutModule,
    FormsModule,
    HttpModule,
    ListboxModule,
    PaginationModule,
    ReactiveFormsModule,
    RouterModule,
    SharedModule,
    TranslateModule,
  ],
  declarations: [
    TaxCreateComponent,
    TaxDetailComponent,
    TaxFormComponent,
    TaxListComponent,
    TaxUpdateComponent
  ],
})
export class TaxModule { }

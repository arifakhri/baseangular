import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { TaxCreateComponent } from './create/tax-create.component';
import { TaxDetailComponent } from './detail/tax-detail.component';
import { TaxListComponent } from './list/tax-list.component';
import { TaxUpdateComponent } from './update/tax-update.component';

import { AuthorizationService } from '../../core/core.module';

export const routes: Routes = [{
  path: '',
  children: [{
    path: '',
    component: TaxListComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'tax.list'
    }
  }, {
    path: 'create',
    component: TaxCreateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'tax.create'
    }
  }, {
    path: ':id',
    component: TaxDetailComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'tax.detail'
    }
  }, {
    path: ':id/update',
    component: TaxUpdateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'tax.update'
    }
  }]
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class TaxRoutingModule { }

declare interface ITax {
  id: string;
  code: string;
  name: string;
  rate: number;
}

declare interface ITaxRelatedData {
  accounts: IAccount[];
}

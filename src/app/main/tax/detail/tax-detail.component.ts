import { Component } from '@angular/core';

import { TaxRestService } from '../tax-rest.service';

import { BaseDetailBComponent } from '../../../shared/base/base.module';

@Component({
  selector: 'app-tax-detail',
  templateUrl: 'tax-detail.component.html',
})
export class TaxDetailComponent extends BaseDetailBComponent {
  constructor(
    private _taxRest: TaxRestService,
  ) {
    super();

    this.componentId = 'TaxDetail';
    this.headerButtons.push({
      type: 'edit',
    });

    this.registerHook('load', event => {
      return this._taxRest
        .load(this.page.routeParams.id)
        .do(doc => this.headerTitle = doc.name);
    });
  }
}

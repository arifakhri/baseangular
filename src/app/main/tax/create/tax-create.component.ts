import * as _ from 'lodash';
import { Component } from '@angular/core';

import { TaxRestService } from '../tax-rest.service';

import { BaseCreateBComponent } from '../../../shared/base/base.module';
import { MTax } from '../tax.model';

@Component({
  selector: 'app-tax-create',
  templateUrl: 'tax-create.component.html',
})
export class TaxCreateComponent extends BaseCreateBComponent {
  constructor(
    private _taxRest: TaxRestService,
  ) {
    super();

    this.componentId = 'TaxCreate';
    this.routeURL = '/taxes';
    this.entrySuccessI18n = 'success.tax.create';

    this.headerTitle = 'ui.tax.create.title';

    this.registerHook('save', event => {
      const formValue = this.form.value;
      let tax = new MTax;
      tax = _.assign(tax, formValue);

      return this._taxRest.create(tax);
    });
  }
}

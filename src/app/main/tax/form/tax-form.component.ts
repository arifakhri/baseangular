import { Component } from '@angular/core';
import { CustomValidators } from 'ng2-validation';
import { FormControl, Validators } from '@angular/forms';
import 'app/rxjs-imports.ts';

import { AutocompleteService } from '../../../core/core.module';
import { TaxRestService } from '../tax-rest.service';

import { BaseFormBComponent } from '../../../shared/base/base.module';

@Component({
  selector: 'app-tax-form',
  templateUrl: 'tax-form.component.html'
})
export class TaxFormComponent extends BaseFormBComponent {
  constructor(
    public _autocomplete: AutocompleteService,
    private _taxRest: TaxRestService,
  ) {
    super();
    this.componentId = 'TaxForm';

    this.registerHook('buildForm', event => {
      this.buildForm();
    });

    this.registerHook('loadRelated', event => {
      return this._taxRest.loadRelatedData();
    });
  }

  buildForm() {
    const codeControl = new FormControl('', [Validators.required, Validators.maxLength(10)]);
    (<any>codeControl).validatorData = {
      maxLength: 10
    };
    const nameControl = new FormControl('', [Validators.required, Validators.maxLength(50)]);
    (<any>nameControl).validatorData = {
      maxLength: 50
    };
    const rateControl = new FormControl('', [Validators.required, CustomValidators.min(0), CustomValidators.max(100)]);
    (<any>rateControl).validatorData = {
      min: 0,
      max: 100
    };

    this.form.addControl('code', codeControl);
    this.form.addControl('name', nameControl);
    this.form.addControl('rate', rateControl);
    this.form.addControl('salesTaxAccount', new FormControl);
    this.form.addControl('salesTaxAccountId', new FormControl(null));
    this.form.addControl('purchaseTaxAccount', new FormControl);
    this.form.addControl('purchaseTaxAccountId', new FormControl(null));
  }
}

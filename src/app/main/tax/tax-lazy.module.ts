import { NgModule } from '@angular/core';

import { TaxModule } from './tax.module';
import { TaxRoutingModule } from './tax-routing.module';

@NgModule({
  imports: [
    TaxModule,
    TaxRoutingModule
  ]
})
export class TaxLazyModule { }

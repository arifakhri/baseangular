import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { DataTable } from 'primeng/components/datatable/datatable';
import { Observable, Subscription } from 'rxjs';
import 'app/rxjs-imports.ts';
import { TranslateService } from '@ngx-translate/core';

import { SpinnerDirective } from '../../../shared/spinner/spinner.directive';

import { RetryDialogComponent } from '../../../shared/retry-dialog/retry-dialog.component';

import { ExportDataTableService, GridTableDataSource, GridTableService, GridTableToggleService, } from '../../../core/core.module';
import { TaxRestService } from '../tax-rest.service';

@Component({
  selector: 'app-tax-list',
  templateUrl: 'tax-list.component.html'
})
export class TaxListComponent implements OnInit {
  @ViewChild('gridTable') public gridTable: DataTable;
  @ViewChild('pageLoading') elPageLoading: SpinnerDirective;
  @ViewChild('retryDialog') elRetryDialog: RetryDialogComponent;

  compReady: boolean;
  gridDataSource: GridTableDataSource<ITax> = new GridTableDataSource<ITax>();
  selectedRecords: ITax[] = [];

  combinedFilterValues: string = '';
  combinedFilterFields = {
    code: {
      operator: 'eq'
    },
    name: true,
  };

  tableColumns: IGridTableColumn[] = [{
    i18nLabel: 'ui.tax.list.column.code',
    field: 'code',
    sort: true,
  }, {
    i18nLabel: 'ui.tax.list.column.name',
    field: 'name',
    link: (row: ITax) => {
      return `/taxes/${row.id}`;
    },
    sort: true,
  }, {
    i18nLabel: 'ui.tax.list.column.rate',
    field: 'rate',
    sort: true,
    columnClasses: 'right-align',
  }];
  tableColumnsShow: boolean = false;
  tableColumnsToggle = this._gridTableToggle.mapToggleOptionsFromColumns(this.tableColumns);

  retryDialogShownState: boolean = false;
  retryDialogOnRetry: Function;

  private loadDataTimeoutId: number;
  private gridFirstLoad: boolean = true;
  private subscriptions: { [key: string]: Subscription[] } = {
    load: []
  };

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _dataTableExport: ExportDataTableService,
    private _gridTable: GridTableService,
    private _gridTableToggle: GridTableToggleService,
    private _taxRest: TaxRestService,
    private _translate: TranslateService,
  ) { }
  ngOnInit() {
    this.loadData();
  }

  gridLoadDataWrapper() {
    // Grid always load at the first time. we dont want that. we already call on ngOnInit
    if (this.gridFirstLoad) {
      this.gridFirstLoad = false;
      return;
    }

    if (this.loadDataTimeoutId) {
      window.clearTimeout(this.loadDataTimeoutId);
      this.loadDataTimeoutId = null;
    }

    this.loadDataTimeoutId = window.setTimeout(() => {
      this.loadData();
    }, 300);
  }

  get exportRecords(): Observable<ITax[]> {
    const qOption = {
      ...this._gridTable.generateApiQueryOptionFromGridInfo(
        this.gridDataSource.pager,
        false,
        this.gridTable,
        this._gridTable.generateCombinedApiFilters(this.combinedFilterValues, this.combinedFilterFields)
      ),
      skip: 0,
      take: this._dataTableExport.exportLimit,
    };

    return this.elRetryDialog.createRetryEntry(
      this._taxRest.findAll(qOption).map(response => response.data)
    );
  }

  exportPDF() {
    this.exportRecords.subscribe(records => {
      this._dataTableExport.export({
        records,
        mapOptions: this._dataTableExport.mapFromTableColumns(this.tableColumns),
        fileName: 'all-taxes',
        extension: 'pdf',
        templateData: {
          title: this._translate.instant('ui.tax.list.title'),
        },
      });
    });
  }

  exportExcel() {
    this.exportRecords.subscribe(records => {
      this._dataTableExport.export({
        records: records,
        mapOptions: this._dataTableExport.mapFromTableColumns(this.tableColumns),
        fileName: 'all-taxes',
        extension: 'xls',
      });
    });
  }

  loadData() {
    this.elPageLoading.forceShow();

    const qOption = this._gridTable.generateApiQueryOptionFromGridInfo(
      this.gridDataSource.pager,
      true,
      this.gridTable,
      this._gridTable.generateCombinedApiFilters(this.combinedFilterValues, this.combinedFilterFields)
    );

    this.subscriptions.load.forEach(sub => sub.unsubscribe());
    this.subscriptions.load.push(
      this.elRetryDialog.createRetryEntry(
        this._taxRest.findAll(qOption).finally(() => {
          this.elPageLoading.forceHide();
        })
      ).subscribe(response => {
        this.gridDataSource.updateFromApiPaginationResult(response);

        this.compReady = true;
      })
    );
  }

  public onGridChange() {
    this.gridLoadDataWrapper();
  }

  public onGridPageChange() {
    this.gridLoadDataWrapper();
  }
}

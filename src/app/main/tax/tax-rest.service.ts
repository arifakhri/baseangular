import { Injectable } from '@angular/core';
import 'app/rxjs-imports.ts';

import { ApiQueryOption, RequestService } from '../../core/core.module';

import { APP_CONST } from '../../app.const';

@Injectable()
export class TaxRestService {
  baseURL = `${APP_CONST.API_MAIN}/taxes`;
  request = this._request.new(this.baseURL);

  constructor(
    private _request: RequestService,
  ) { }

  create(tax: ITax) {
    return this.request.post<ITax>(``, tax);
  }

  findAll(queryOption: ApiQueryOption) {
    return this.request.post<IApiPaginationResult<ITax>>(`q`, queryOption);
  }

  load(taxId: string) {
    return this.request.get<ITax>(`${taxId}`)
  }

  update(taxId: string, updateObj: ITax) {
    return this.request.put<ITax>(`${taxId}`, updateObj)
  }

  loadRelatedData() {
    return this.request.get<ITaxRelatedData>(`entry-related-data`);
  }
}

export class MTax {
  id: string;
  code: string;
  name: string;
  rate: number;
}

export class MTaxRelatedData {
  accounts: IAccount[] = [];
}

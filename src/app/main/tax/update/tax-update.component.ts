import * as _ from 'lodash';
import { Component } from '@angular/core';

import { TaxRestService } from '../tax-rest.service';

import { BaseUpdateBComponent } from '../../../shared/base/base.module';
import { MTax } from '../tax.model';

@Component({
  selector: 'app-tax-update',
  templateUrl: 'tax-update.component.html',
})
export class TaxUpdateComponent extends BaseUpdateBComponent {
  constructor(
    private _taxRest: TaxRestService,
  ) {
    super();

    this.componentId = 'TaxUpdate';
    this.routeURL = '/taxes';
    this.entrySuccessI18n = 'success.tax.update';

    this.headerTitle = 'ui.tax.update.title';

    this.registerHook('load', event => {
      return this._taxRest.load(this.page.routeParams.id);
    });

    this.registerHook('save', event => {
      const formValue = this.form.value;
      let tax = new MTax;
      tax = _.assign(tax, this.doc, formValue);

      return this._taxRest.update(this.page.routeParams.id, tax);
    });
  }
}

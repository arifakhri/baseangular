import * as _ from 'lodash';
import { Component, OnInit } from '@angular/core';

import { SettingsSalesChannelRestService } from '../settings-sales-channel-rest.service';

import { BaseUpdateBComponent } from '../../../shared/base/base.module';
import { MSettingsMarketplace, MSettingsWebstore } from '../settings-sales-channel.model';

@Component({
  selector: 'app-settings-sales-channel-update',
  templateUrl: './settings-sales-channel-update.component.html',
})
export class SettingsSalesChannelUpdateComponent extends BaseUpdateBComponent implements OnInit {

  channelAccountType: string;
  constructor(
    public _settingsSaleschannelRest: SettingsSalesChannelRestService,
  ) {
    super();

    this.componentId = 'SettingsSalesChannelUpdate';
    this.containerType = 1;
    this.routeURL = '/settings/saleschannel';
    this.entrySuccessI18n = 'success.settingsSalesChannel.update';
    this.saveAndViewEnabled = false;

    this.registerHook('load', event => {
      this.saveAndNewURL = `/settings/saleschannel/${this.page.routeData.type}/create`;

      return this._settingsSaleschannelRest.load(this.page.routeParams.id).switchMap(saleschannel => {
        this.channelAccountType = saleschannel.channelAccountType;
        const loadFunction = `load${_.startCase(saleschannel.channelAccountType)}`;
        return this._settingsSaleschannelRest[loadFunction](this.page.routeParams.id);
      });
    });

    this.registerHook('save', event => {
      switch (this.channelAccountType) {
        case 'marketplace':
          let accountMarketplace = new MSettingsMarketplace;
          accountMarketplace = _.assign(accountMarketplace, this.form.value);
          return this._settingsSaleschannelRest.updateMarketplace(this.page.routeParams.id, accountMarketplace);
        case 'webstore':
          let accountWebstore = new MSettingsWebstore;
          accountWebstore = _.assign(accountWebstore, this.form.value);
          return this._settingsSaleschannelRest.updateWebstore(this.page.routeParams.id, accountWebstore);
      }
    });
  }

  ngOnInit() {
  }

  onPageInit() {
    super.onPageInit();
    this.headerTitle = `ui.settingsSalesChannel.update.${this.page.routeData.type}Title`;
  }
}

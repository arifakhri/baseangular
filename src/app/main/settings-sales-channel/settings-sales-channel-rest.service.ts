import { Injectable } from '@angular/core';

import { RequestService } from '../../core/core.module';
import { APP_CONST } from '../../app.const';
import { MSettingsMarketplace, MSettingsSalesChannel, MSettingsWebstore } from './settings-sales-channel.model';

@Injectable()
export class SettingsSalesChannelRestService {
  request = this._request.new(`${APP_CONST.API_MAIN}/integrations`);
  requestDelete = this._request.new(`${APP_CONST.API_MAIN}/integration-accounts`);
  requestList = this._request.new(`${APP_CONST.API_MAIN}/integrations/sales-channels/accounts`);
  constructor(
    private _request: RequestService,
  ) { }

  findAll(queryParams: any = {}) {
    return this.requestList.get<IApiPaginationResult<MSettingsSalesChannel>>(``, { params: queryParams });
  }

  loadMarketplace(accountId: string) {
    return this.request.get<MSettingsMarketplace>(`marketplaces/accounts/${accountId}`);
  }

  loadWebstore(accountId: string) {
    return this.request.get<MSettingsMarketplace>(`webstores/accounts/${accountId}`);
  }

  load(accountId: string) {
    return this.requestList.get<MSettingsSalesChannel>(accountId);
  }

  createMarketplace(account: MSettingsMarketplace, queryParams: any = {}) {
    return this.request.post<MSettingsMarketplace>(`marketplaces/accounts`, account, { params: queryParams });
  }
  createWebstore(account: MSettingsWebstore, queryParams: any = {}) {
    return this.request.post<MSettingsWebstore>(`webstores/accounts`, account, { params: queryParams });
  }

  updateMarketplace(accountId: string, account: MSettingsMarketplace) {
    return this.request.put<MSettingsMarketplace>(`marketplaces/accounts/${accountId}`, account);
  }
  updateWebstore(accountId: string, account: MSettingsWebstore) {
    return this.request.put<MSettingsWebstore>(`webstores/accounts/${accountId}`, account);
  }

  delete(accountId: string) {
    return this.requestDelete.delete<MSettingsSalesChannel>(accountId);
  }
}

export class MSettingsSalesChannel {
  channelAccountId: string;
  name: string;
  description: string;
  channelAccountType: string;
  channelId: string;
 }

 export class MSettingsMarketplace {
  channelId: string;
  name: string;
  description: string;
  sellerUserId: string;
  password: string;
  apiKey: string;
  priceLevelId: string;
  warehouseId: string;
 }

 export class MSettingsWebstore {
  channelId: string;
  name: string;
  description: string;
  storeKey: string;
  password: string;
  apiKey: string;
  apiSecret: string;
  priceLevelId: string;
  warehouseId: string;
 }

import { Component, OnInit, ViewChild } from '@angular/core';
import * as _ from 'lodash';

import * as SnackBar from 'node-snackbar';
import swal from 'sweetalert2';
import { TranslateService } from '@ngx-translate/core';

import { SettingsSalesChannelRestService } from '../settings-sales-channel-rest.service';

import { BasePageBComponent } from '../../../shared/base/base.module';
import { MSettingsSalesChannel } from '../settings-sales-channel.model';
import { ModalDirective } from 'ngx-bootstrap';

@Component({
  selector: 'app-settings-sales-channel-list',
  templateUrl: './settings-sales-channel-list.component.html',
})
export class SettingsSalesChannelListComponent extends BasePageBComponent implements OnInit {
  @ViewChild('newSalesChannelModal') elnewSalesChannelModal: ModalDirective;
  dataSalesChannel: MSettingsSalesChannel[] = [];
  data: any;
  _ = _;

  constructor(
    private _settingSalesChannelRest: SettingsSalesChannelRestService,
    private _translate: TranslateService,

  ) {
    super();
    this.componentId = 'SettingsSalesChannelList';

    this.headerButtons.push({
      type: 'custom',
      label: 'ui.button.add',
      color: 'success',
      onClick: () => this.showModal(),
    });
  }

  showModal() {
    return this.elnewSalesChannelModal.show();
  }

  ngOnInit() {
  }

  onPageInit() {
    super.onPageInit();
    this.headerTitle = `ui.settingsSalesChannel.list.title`;
    this.loadData();
  }

  loadData() {
    const loading = this.page.createLoading();

    this._settingSalesChannelRest.findAll().do(result => {
      this.data = result;
    }).finally(() => {
      loading.dispose();
    }).subscribe();
  }

  showDeleteDialog(channelAccountId: string, name: string) {
    swal({
      title: this._translate.instant(`confirm.settingsSalesChannel.delete.label`) + name + '?',
      text: this._translate.instant(`confirm.settingsSalesChannel.delete.description`),
      type: 'question',
      showCancelButton: true,
    })
      .then(() => {
        this.deleteAccount(channelAccountId);
      })
      .catch(() => { });
  }

  deleteAccount(channelAccountId: string) {
    const loading = this.page.createLoading();
    this._settingSalesChannelRest.delete(channelAccountId).subscribe(response => {
      SnackBar.show({
        text: this._translate.instant(`success.settingsSalesChannel.delete`),
        pos: 'bottom-right'
      });
      loading.dispose();
    });
  }
}

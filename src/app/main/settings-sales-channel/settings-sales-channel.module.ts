import { AutoCompleteModule } from 'primeng/components/autocomplete/autocomplete';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ModalModule } from 'ngx-bootstrap';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { SettingsSalesChannelCreateComponent } from './create/settings-sales-channel-create.component';
import { SettingsSalesChannelFormBukalapakComponent } from './form-bukalapak/settings-sales-channel-form-bukalapak.component';
import { SettingsSalesChannelFormLazadaComponent } from './form-lazada/settings-sales-channel-form-lazada.component';
import { SettingsSalesChannelFormShopifyComponent } from './form-shopify/settings-sales-channel-form-shopify.component';
import { SettingsSalesChannelFormWoocommerceComponent } from './form-woocommerce/settings-sales-channel-form-woocommerce.component';
import { SettingsSalesChannelListComponent } from './list/settings-sales-channel-list.component';
import { SettingsSalesChannelUpdateComponent } from './update/settings-sales-channel-update.component';

import { SettingsSalesChannelRestService } from './settings-sales-channel-rest.service';

import { CoreModule } from '../../core/core.module';
import { SharedModule } from '../../shared/shared.module';

export const PROVIDERS = [
  SettingsSalesChannelRestService,
];

@NgModule({
  imports: [
    AutoCompleteModule,
    CommonModule,
    CoreModule,
    FlexLayoutModule,
    ModalModule,
    ReactiveFormsModule,
    RouterModule,
    SharedModule,
    TranslateModule,
  ],
  declarations: [
    SettingsSalesChannelCreateComponent,
    SettingsSalesChannelFormBukalapakComponent,
    SettingsSalesChannelFormLazadaComponent,
    SettingsSalesChannelFormShopifyComponent,
    SettingsSalesChannelFormWoocommerceComponent,
    SettingsSalesChannelListComponent,
    SettingsSalesChannelUpdateComponent,
  ],
})
export class SettingsSalesChannelModule { }

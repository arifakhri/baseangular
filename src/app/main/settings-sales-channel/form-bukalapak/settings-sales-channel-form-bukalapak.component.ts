import { AutoComplete } from 'primeng/components/autocomplete/autocomplete';
import { Component, ViewChild } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

import { AutocompleteService, CommonService } from '../../../core/core.module';
import { BaseFormBComponent } from '../../../shared/base/base.module';
import { PriceLevelRestService } from '../../price-level/price-level-rest.service';
import { WarehouseRestService } from '../../warehouse/warehouse-rest.service';

@Component({
  selector: 'app-settings-sales-channel-form-bukalapak',
  templateUrl: './settings-sales-channel-form-bukalapak.component.html',
})
export class SettingsSalesChannelFormBukalapakComponent extends BaseFormBComponent {
  @ViewChild('priceLevelAC') elPriceLevelAC: AutoComplete;
  @ViewChild('warehouseAC') elWarehouseAC: AutoComplete;

  ACPriceLevelHandler = CommonService.remoteACItemsHandler({
    remoteParams: this.ACPriceLevelParams.bind(this),
    remoteRequest: this._priceLevelRest.findAll.bind(this._priceLevelRest),
    remoteRequestMap: (response) => response.data,
    element: () => this.elPriceLevelAC,
  });

  ACWarehouseHandler = CommonService.remoteACItemsHandler({
    remoteParams: this.ACWarehouseParams.bind(this),
    remoteRequest: this._warehouseRest.findAll.bind(this._warehouseRest),
    remoteRequestMap: (response) => response.data,
    element: () => this.elWarehouseAC,
  });

  constructor(
    public _autocomplete: AutocompleteService,
    private _priceLevelRest: PriceLevelRestService,
    private _warehouseRest: WarehouseRestService,
  ) {
    super();

    this.componentId = 'SettingsSalesChannelFormBukalapak';

    this.registerHook('buildForm', event => {
      this.buildForm();
    });
  }

  buildForm() {
    this.form.addControl('name', new FormControl(null, Validators.required));
    this.form.addControl('description', new FormControl);
    this.form.addControl('sellerUserId', new FormControl(null, Validators.required));
    this.form.addControl('password', new FormControl(null, Validators.required));
    this.form.addControl('warehouse', new FormControl);
    this.form.addControl('warehouseId', new FormControl);
    this.form.addControl('priceLevel', new FormControl);
    this.form.addControl('priceLevelId', new FormControl);
    this.form.addControl('channelId', new FormControl('bukalapak', Validators.required));
  }

  ACPriceLevelParams(event: any, type: string) {
    let filters = [];

    if (type === 'search') {
      filters = [{
        filterValues: [{
          field: 'name',
          operator: 'contains',
          value: event.query
        }]
      }];
    }

    return [{
      filter: filters,
      sort: [{
        field: 'name',
        dir: 'asc'
      }],
      take: 30,
      skip: 0,
      includeTotalCount: false
    }];
  }

  ACWarehouseParams(event: any, type: string) {
    let filters = [];

    if (type === 'search') {
      filters = [{
        filterValues: [{
          field: 'name',
          operator: 'contains',
          value: event.query
        }]
      }];
    }

    return [{
      filter: filters,
      sort: [{
        field: 'name',
        dir: 'asc'
      }],
      take: 30,
      skip: 0,
      includeTotalCount: false
    }];
  }
}

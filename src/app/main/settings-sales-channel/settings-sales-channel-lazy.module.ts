import { NgModule } from '@angular/core';

import { SettingsSalesChannelModule } from './settings-sales-channel.module';
import { SettingsSalesChannelRoutingModule } from './settings-sales-channel-routing.module';

@NgModule({
  imports: [
    SettingsSalesChannelModule,
    SettingsSalesChannelRoutingModule,
  ]
})
export class SettingsSalesChannelLazyModule { }

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SettingsSalesChannelCreateComponent } from './create/settings-sales-channel-create.component';
import { SettingsSalesChannelListComponent } from './list/settings-sales-channel-list.component';
import { SettingsSalesChannelUpdateComponent } from './update/settings-sales-channel-update.component';

const routes: Routes = [{
  path: '',
  component: SettingsSalesChannelListComponent,
}, {
  path: 'bukalapak/create',
  component: SettingsSalesChannelCreateComponent,
  data: {
    type: 'bukalapak'
  }
}, {
  path: 'lazada/create',
  component: SettingsSalesChannelCreateComponent,
  data: {
    type: 'lazada'
  }
}, {
  path: 'shopify/create',
  component: SettingsSalesChannelCreateComponent,
  data: {
    type: 'shopify'
  }
}, {
  path: 'woocommerce/create',
  component: SettingsSalesChannelCreateComponent,
  data: {
    type: 'woocommerce'
  }
}, {
  path: 'bukalapak/:id/update',
  component: SettingsSalesChannelUpdateComponent,
  data: {
    type: 'bukalapak'
  }
}, {
  path: 'lazada/:id/update',
  component: SettingsSalesChannelUpdateComponent,
  data: {
    type: 'lazada'
  }
}, {
  path: 'shopify/:id/update',
  component: SettingsSalesChannelUpdateComponent,
  data: {
    type: 'shopify'
  }
},  {
  path: 'woocommerce/:id/update',
  component: SettingsSalesChannelUpdateComponent,
  data: {
    type: 'woocommerce'
  }
}
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ],
})
export class SettingsSalesChannelRoutingModule { }

import * as _ from 'lodash';
import { Component, OnInit } from '@angular/core';

import { SettingsSalesChannelRestService } from '../settings-sales-channel-rest.service';

import { BaseCreateBComponent } from '../../../shared/base/base.module';
import { MSettingsMarketplace, MSettingsWebstore } from '../settings-sales-channel.model';

@Component({
  selector: 'app-settings-sales-channel-create',
  templateUrl: './settings-sales-channel-create.component.html',
})
export class SettingsSalesChannelCreateComponent extends BaseCreateBComponent implements OnInit {
  constructor(
    public _settingsSaleschannelRest: SettingsSalesChannelRestService,
  ) {
    super();

    this.componentId = 'SettingsSalesChannelCreate';
    this.headerTitle = 'ui.settingsSaleschannel.create.title';
    this.containerType = 1;
    this.routeURL = '/settings/saleschannel';
    this.entrySuccessI18n = 'success.settingsSalesChannel.create';
    this.saveAndViewEnabled = false;

    this.registerHook('save', event => {
      switch (this.page.routeData.type) {
        case 'bukalapak':
          let accountBukalapak = new MSettingsMarketplace;
          accountBukalapak = _.assign(accountBukalapak, this.form.value);
          return this._settingsSaleschannelRest.createMarketplace(accountBukalapak);
        case 'lazada':
          let accountLazada = new MSettingsMarketplace;
          accountLazada = _.assign(accountLazada, this.form.value);
          return this._settingsSaleschannelRest.createMarketplace(accountLazada);
        case 'shopify':
          let accountShopify = new MSettingsWebstore;
          accountShopify = _.assign(accountShopify, this.form.value);
          return this._settingsSaleschannelRest.createWebstore(accountShopify);
        case 'woocommerce':
          let accountWoocommerce = new MSettingsWebstore;
          accountWoocommerce = _.assign(accountWoocommerce, this.form.value);
          return this._settingsSaleschannelRest.createWebstore(accountWoocommerce);
      }
    });
  }
  ngOnInit() {
  }

  onPageInit() {
    super.onPageInit();
    this.saveAndNewURL = `/settings/saleschannel/${this.page.routeData.type}/create`;
    this.headerTitle = `ui.settingsSalesChannel.create.${this.page.routeData.type}Title`;
  }
}

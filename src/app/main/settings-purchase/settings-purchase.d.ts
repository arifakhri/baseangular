declare interface ISettingsPurchases {
  defaultPurchaseTax: string;
  defaultPurchaseTaxId: string;
}

import { AutoCompleteModule } from 'primeng/components/autocomplete/autocomplete';
import { CommonModule } from '@angular/common';
import { CheckboxModule } from 'primeng/components/checkbox/checkbox';
import { DataTableModule } from 'primeng/components/datatable/datatable';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgModule } from '@angular/core';
import { PaginationModule } from 'ngx-bootstrap';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { SettingsPurchaseFormComponent } from './form/settings-purchase-form.component';
import { SettingsPurchaseUpdateComponent } from './update/settings-purchase-update.component';

import { SettingsPurchaseService } from './settings-purchase.service';
import { SettingsPurchaseRestService } from './settings-purchase-rest.service';

import { CoreModule } from '../../core/core.module';
import { SettingsModule } from '../settings/settings.module';
import { SharedModule } from '../../shared/shared.module';

export const PROVIDERS = [
  SettingsPurchaseService,
  SettingsPurchaseRestService,
];

@NgModule({
  imports: [
    AutoCompleteModule,
    CheckboxModule,
    CommonModule,
    CoreModule,
    DataTableModule,
    FlexLayoutModule,
    FormsModule,
    HttpModule,
    PaginationModule,
    ReactiveFormsModule,
    RouterModule,
    SettingsModule,
    SharedModule,
    TranslateModule,
  ],
  declarations: [
    SettingsPurchaseFormComponent,
    SettingsPurchaseUpdateComponent,
  ],
})
export class SettingsPurchaseModule { }

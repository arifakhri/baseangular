import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SettingsPurchaseUpdateComponent } from './update/settings-purchase-update.component';

import { AuthorizationService } from '../../core/core.module';

export const routes: Routes = [{
  path: '',
  children: [{
    path: '',
    component: SettingsPurchaseUpdateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'settingsPurchase.update'
    }
  }]
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class SettingsPurchaseRoutingModule { }

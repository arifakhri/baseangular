import { NgModule } from '@angular/core';
import { SettingsPurchaseModule } from './settings-purchase.module';
import { SettingsPurchaseRoutingModule } from './settings-purchase-routing.module';

@NgModule({
  imports: [
    SettingsPurchaseModule,
    SettingsPurchaseRoutingModule
  ]
})
export class SettingsPurchaseLazyModule { }

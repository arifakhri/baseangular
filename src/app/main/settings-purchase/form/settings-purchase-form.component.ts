import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { SettingsPurchaseRestService } from '../settings-purchase-rest.service';

import { AutocompleteService } from '../../../core/core.module';

@Component({
  selector: 'app-settings-purchase-form',
  templateUrl: 'settings-purchase-form.component.html'
})
export class SettingsPurchaseFormComponent implements OnInit {
  @Input() doc: string;
  @Input() form: FormGroup;
  @Output() submit: EventEmitter<any> = new EventEmitter();

  taxes: ITax[] = [];
  updateForm: boolean = false;

  constructor(
    public _autocomplete: AutocompleteService,
    private _settingsPurchaseRest: SettingsPurchaseRestService,
  ) {
  }

  ngOnInit() {
    this._settingsPurchaseRest.loadRelatedData().subscribe(related => {
      this.taxes = related.taxes;
    });
  }

  onSubmit() {
    this.submit.emit();
  }
}

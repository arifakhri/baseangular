import { ActivatedRoute, Router } from '@angular/router';
import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';
import { TranslateService } from '@ngx-translate/core';

import { SpinnerService } from '../../../shared/spinner/spinner.service';
import { SettingsPurchaseRestService } from '../settings-purchase-rest.service';
import { SettingsPurchaseService } from '../settings-purchase.service';
import { SystemMessageService } from '../../../core/core.module';
import { RetryDialogComponent } from '../../../shared/retry-dialog/retry-dialog.component';

@Component({
  selector: 'app-settings-purchase-update',
  templateUrl: 'settings-purchase-update.component.html',
  providers: [SystemMessageService]
})
export class SettingsPurchaseUpdateComponent implements OnInit {
  @ViewChild('retryDialog') elRetryDialog: RetryDialogComponent;

  compReady: boolean;
  doc: ISettingsPurchases;
  form: FormGroup = new FormGroup({});

  retryDialogShownState: boolean = false;
  retryDialogOnRetry: Function;

  constructor(
    @Inject('GlobalSystemMessage') private _globalSystemMessage: SystemMessageService,
    private _spinner: SpinnerService,
    private _settingsPurchase: SettingsPurchaseService,
    private _settingsPurchaseRest: SettingsPurchaseRestService,
    private _route: ActivatedRoute,
    private _router: Router,
    public _systemMessage: SystemMessageService,
    private _translate: TranslateService,
  ) { }

  ngOnInit() {
    this._settingsPurchase.setFormDefinitions(this.form);

    this.loadData();
  }

  loadData() {
    this.elRetryDialog.createRetryEntry(this._settingsPurchaseRest.load())
      .subscribe(settingCompany => {
        this.doc = settingCompany;
        this.form.patchValue(this.doc);

        this.compReady = true;
      });
  }

  save(): void {
    const spinner = this._spinner.showDefault();

    const settingsPurchase: ISettingsPurchases = Object.assign({}, this.doc, this.form.value);
    this._settingsPurchaseRest.update(settingsPurchase)
      .catch(error => {
        spinner.dispose();

        this._systemMessage.log({
          message: error,
          type: 'error'
        });
        return Observable.throw(error);
      })
      .subscribe(result => {
        spinner.dispose();

        this.doc = result;
        this._globalSystemMessage.log({
          message: this._translate.instant('success.settingsPurchase.update'),
          type: 'success',
          showAs: 'growl',
          showSnackBar: false,
        });
      });
  }

  onSubmit() {
    this.save();
  }
}

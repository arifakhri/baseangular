import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class SettingsPurchaseService {
  constructor(
    private _formBuilder: FormBuilder,
    private _translate: TranslateService
  ) { }

  setFormDefinitions(form: FormGroup) {
    form.addControl('defaultPurchaseTax', new FormControl);
    form.addControl('defaultPurchaseTaxId', new FormControl('', Validators.required));
  }
}

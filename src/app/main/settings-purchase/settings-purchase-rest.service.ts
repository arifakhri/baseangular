import { Injectable } from '@angular/core';
import 'app/rxjs-imports.ts';

import { RequestService } from '../../core/core.module';

import { APP_CONST } from '../../app.const';

@Injectable()
export class SettingsPurchaseRestService {
  baseURL = `${APP_CONST.API_MAIN}/settings`;

  request = this._request.new(this.baseURL);

  constructor(
    private _request: RequestService,
  ) { }

  load() {
    return this.request.get<ISettingsPurchases>(`purchase`);
  }

  update(updateObj: ISettingsPurchases) {
    return this.request.put<ISettingsPurchases>(`purchase`, updateObj);
  }

  loadRelatedData() {
    return this.request.get<{ taxes: ITax[] }>(`entry-related-data`);
  }
}

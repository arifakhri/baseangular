declare interface IAccount {
  id: string;
  accountClassId: string;
  accountTypeId: string;
  code: string;
  name: string;
  description: string;
  isChildAccount: boolean;
  parentAccountId: string;
  level: number;
  idIndex: string;
  codeIndex: string;
  nameIndex: string;
  systemType: string;
  inactive: boolean;
  rowVersion: string;
  accountClass: {
    id: string;
    name: string;
    sortOrder: number;
  };
  accountType: {
    id: string;
    name: string;
    accountClassId: string;
    sortOrder: number
  };
  parentAccount: {
    id: string;
    accountClassId: string;
    accountTypeId: string;
    code: string;
    name: string;
    description: string;
    isChildAccount: boolean;
    level: number;
    locked: boolean;
    systemType: string;
    inactive: boolean;
  }
}

declare interface IAccountSequences {
  accountTypeId: string;
  codePrefix: string;
  lastCode: string;
  nextCode: string;
}

declare interface IAccountType {
  id: string;
  code: string;
  name: string;
  accountClassId: string;
  sortOrder: number;
  accountClass: {
    id: string;
    name: string;
    sortOrder: number;
  }
}

declare interface IAccountClass {
  id: string;
  name: string;
  sortOrder: number;
  accountTypes: [
    {
      id: string;
      name: string;
      accountClassId: string;
      sortOrder: number;
    }
  ]
}

declare interface IAccountLedger {
  transactionType: string;
  transactionTypeGroup: string;
  transactionId: string;
  transactionNumber: string;
  transactionDate: string;
  contactId: string;
  description: string;
  debit: number;
  credit: number;
  balance: number;
  contact: {
    id: string;
    contactType: string;
    code: string;
    displayName: string;
    company: string;
    fullName: string;
    phone: string;
    mobile: string;
    fax: string;
    email: string;
    website: string;
    inactive: boolean;
    postalAddress: {
      attention: string;
      street1: string;
      street2: string;
      city: string;
      stateProvince: string;
      countryId: string;
      postalCode: string;
      longitude: number;
      latitude: number;
      country: {
        code: string;
        name: string
      }
    }
  }
}

import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

import { CommonService, SystemMessageService } from '../../../core/core.module';
import { SpinnerService } from '../../../shared/spinner/spinner.service';
import { COARestService } from '../coa-rest.service';

@Component({
  selector: 'app-coa-create',
  templateUrl: 'coa-create.component.html',
  providers: [SystemMessageService]
})
export class COACreateComponent implements OnInit {
  compReady: boolean;
  accountTypeId: string;
  form: FormGroup;
  routeParams: any = {};

  constructor(
    @Inject('GlobalSystemMessage') private _globalSystemMessage: SystemMessageService,
    private _activatedRoute: ActivatedRoute,
    private _fb: FormBuilder,
    private _spinner: SpinnerService,
    private _router: Router,
    public _systemMessage: SystemMessageService,
    private _coaRest: COARestService,
    private _translate: TranslateService
  ) { }

  ngOnInit() {
    this.form = this._fb.group({});

    this.routeParams = this._activatedRoute.snapshot.queryParams;

    if (this.routeParams) {
      this.accountTypeId = this.routeParams.accountTypeId;
    }
  }

  onSubmit({ saveAndNew, saveAndView }: any = {}) {
    if (this.formValid()) {
      this.save().subscribe(result => {
        if (saveAndNew) {
          this._router.navigateByUrl('/empty', { skipLocationChange: true }).then(() => {
            this._router.navigateByUrl('/accounts/create');
          });
        } else if (saveAndView) {
          this._router.navigate(['/accounts', result.id]);
        } else {
          this._router.navigateByUrl('/accounts');
        }
      });
    }
  }

  formValid(): boolean {
    if (!this.form.valid) {
      CommonService.markAsDirty(this.form);
      this._systemMessage.log({
        message: this._translate.instant('error.form'),
        type: 'error'
      });
      return false;
    }
    return true;
  }

  save() {
    const spinner = this._spinner.showDefault();

    const coa: IAccount = this.form.value;
    return this._coaRest.create(coa)
      .catch(error => {
        spinner.dispose();

        this._systemMessage.log({
          message: error,
          type: 'error'
        });

        return Observable.throw(error);
      })
      .do(result => {
        spinner.dispose();

        this._globalSystemMessage.log({
          message: this._translate.instant('success.coa.create'),
          type: 'success',
          showAs: 'growl',
          showSnackBar: false,
        });
      });
  }
}

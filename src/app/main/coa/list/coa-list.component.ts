import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { DataTable } from 'primeng/components/datatable/datatable';
import { Observable, Subscription } from 'rxjs';
import 'app/rxjs-imports.ts';
import { TranslateService } from '@ngx-translate/core';

import { SpinnerDirective } from '../../../shared/spinner/spinner.directive';

import {
  AccountingService,
  AutocompleteService,
  ExportDataTableService,
  GridTableDataSource,
  GridTableService,
  GridTableToggleService,
} from '../../../core/core.module';
import { COARestService } from '../coa-rest.service';
import { RetryDialogComponent } from '../../../shared/retry-dialog/retry-dialog.component';

@Component({
  selector: 'app-coa-list',
  templateUrl: 'coa-list.component.html'
})
export class COAListComponent implements OnInit {
  @ViewChild('gridTable') public gridTable: DataTable;
  @ViewChild('pageLoading') elPageLoading: SpinnerDirective;
  @ViewChild('retryDialog') elRetryDialog: RetryDialogComponent;

  compReady: any = {};
  gridDataSource: GridTableDataSource<IAccount> = new GridTableDataSource<IAccount>();
  selectedRecords: IAccount[] = [];

  tableColumns: IGridTableColumn[] = [{
    i18nLabel: 'ui.coa.list.column.code',
    field: 'code',
    classes: (value, row: IAccount) => {
      if (!this.gridTable.sortField || (this.gridTable.sortField === 'code' && this.gridTable.sortOrder === 1)) {
        return `list-tree-level-${row.level}`;
      } else {
        return '';
      }
    },
    link: (row: IAccount) => {
      return (`/accounts/${row.id}`);
    },
    sort: true,
  }, {
    i18nLabel: 'ui.coa.list.column.name',
    field: 'name',
    sort: true,
  }, {
    i18nLabel: 'ui.coa.list.column.class',
    field: 'accountClassName',
    sort: true,
  }, {
    i18nLabel: 'ui.coa.list.column.type',
    field: 'accountTypeName',
    sort: true,
  }, {
    i18nLabel: 'ui.coa.list.column.balance',
    field: 'balance',
    sort: true,
    formatter: (value, row: IAccount) => {
      return this._accounting.ac.formatMoney(value, '');
    },
    columnClasses: 'right-align',
  }];
  tableColumnsShow: boolean = false;
  tableColumnsToggle = this._gridTableToggle.mapToggleOptionsFromColumns(this.tableColumns);

  COAClasses: IAccountClass[] = [];
  selectedClassFilter: any;

  retryDialogShownState: boolean = false;
  retryDialogOnRetry: Function;

  qParams: any = {
    keyword: null
  };

  private loadDataTimeoutId: number;
  private gridFirstLoad: boolean = true;
  private subscriptions: { [key: string]: Subscription[] } = {
    load: []
  };

  constructor(
    private _accounting: AccountingService,
    private _activatedRoute: ActivatedRoute,
    public _autocomplete: AutocompleteService,
    private _gridTable: GridTableService,
    private _dataTableExport: ExportDataTableService,
    private _gridTableToggle: GridTableToggleService,
    private _coaRest: COARestService,
    private _translate: TranslateService,
  ) { }

  ngOnInit() {
    this.loadAccountClasses();

    this.loadData();
  }

  gridLoadDataWrapper() {
    // Grid always load at the first time. we dont want that. we already call on ngOnInit
    if (this.gridFirstLoad) {
      this.gridFirstLoad = false;
      return;
    }

    if (this.loadDataTimeoutId) {
      window.clearTimeout(this.loadDataTimeoutId);
      this.loadDataTimeoutId = null;
    }

    this.loadDataTimeoutId = window.setTimeout(() => {
      this.loadData();
    }, 300);
  }

  treeClasses(rowData: IAccount) {
    return `list-tree-level-${rowData.level}`;
  }

  populateQParams() {
    if (this.selectedClassFilter) {
      this.qParams.accountClassId = this.selectedClassFilter.id;
    } else {
      delete this.qParams.accountClassId;
    }
  }

  loadData() {
    this.elPageLoading.forceShow();

    const qOption = this._gridTable.generateApiQueryOptionFromGridInfo(
      this.gridDataSource.pager,
      true,
      this.gridTable,
    );

    this.populateQParams();

    this.subscriptions.load.forEach(sub => sub.unsubscribe());
    this.subscriptions.load.push(
      this.elRetryDialog.createRetryEntry(
        this._coaRest.findAll(qOption, this.qParams)
          .finally(() => {
            this.elPageLoading.forceHide();
          })
      ).subscribe(response => {
        this.gridDataSource.updateFromApiPaginationResult(response);

        this.compReady.table = true;
        })
    );
  }

  get exportRecords(): Observable<IAccount[]> {
    const qOption = {
      ...this._gridTable.generateApiQueryOptionFromGridInfo(
        this.gridDataSource.pager,
        false,
        this.gridTable,
      ),
      skip: 0,
      take: this._dataTableExport.exportLimit,
    };

    this.populateQParams();
    return this.elRetryDialog.createRetryEntry(
      this._coaRest.findAll(qOption, this.qParams).map(response => response.data)
    );
  }

  exportPDF() {
    this.exportRecords.subscribe(records => {
      this._dataTableExport.export({
        records,
        mapOptions: this._dataTableExport.mapFromTableColumns(this.tableColumns),
        fileName: 'Chart-of-account',
        extension: 'pdf',
        templateData: {
          title: this._translate.instant('ui.transaction.list.title'),
        },
      });
    });
  }

  exportExcel() {
    this.exportRecords.subscribe(records => {
      this._dataTableExport.export({
        records: records,
        mapOptions: this._dataTableExport.mapFromTableColumns(this.tableColumns),
        fileName: 'Chart-of-account',
        extension: 'xls',
      });
    });
  }

  loadAccountClasses() {
    this._coaRest.findAllClasses().subscribe(classes => {
      this.COAClasses = classes;
      this.compReady.related = true;
    });
  }

  onAccountClassChange(selectedClass) {
    // This is an alternative method since ngModel on p-autocomplete doesnt updated when selected
    this.selectedClassFilter = selectedClass;
    this.onGridChange();
  }

  public onGridChange() {
    this.gridLoadDataWrapper();
  }

  public onGridPageChange() {
    this.gridLoadDataWrapper();
  }
}

import { AutoCompleteModule } from 'primeng/components/autocomplete/autocomplete';
import { CalendarModule } from 'primeng/components/calendar/calendar';
import { DataTableModule } from 'primeng/components/datatable/datatable';
import { ListboxModule } from 'primeng/components/listbox/listbox';
import { BsDropdownModule, PaginationModule } from 'ngx-bootstrap';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { COALedgerComponent } from './ledger/coa-ledger.component';
import { COACreateComponent } from './create/coa-create.component';
import { COADetailComponent } from './detail/coa-detail.component';
import { COAFormComponent } from './form/coa-form.component';
import { COAListComponent } from './list/coa-list.component';
import { COAUpdateComponent } from './update/coa-update.component';

import { COARestService } from './coa-rest.service';

import { CoreModule } from '../../core/core.module';
import { SharedModule } from '../../shared/shared.module';

export const PROVIDERS = [
  COARestService,
];

@NgModule({
  imports: [
    BsDropdownModule,
    CommonModule,
    FormsModule,
    FlexLayoutModule,
    HttpModule,
    ReactiveFormsModule,
    RouterModule,
    TranslateModule,
    AutoCompleteModule,
    CalendarModule,
    DataTableModule,
    PaginationModule,
    ListboxModule,
    CoreModule,
    SharedModule,
  ],
  declarations: [
    COALedgerComponent,
    COACreateComponent,
    COADetailComponent,
    COAFormComponent,
    COAListComponent,
    COAUpdateComponent
  ],
})
export class COAModule { }

import { Injectable } from '@angular/core';
import 'app/rxjs-imports.ts';

import { ApiQueryOption, RequestService } from '../../core/core.module';

import { APP_CONST } from '../../app.const';

@Injectable()
export class COARestService {
  baseURL = `${APP_CONST.API_MAIN}/accounts`;
  baseURLClass = `${APP_CONST.API_MAIN}/account-classes`;

  request = this._request.new(this.baseURL);
  requestClass = this._request.new(this.baseURLClass);

  constructor(
    private _request: RequestService,
  ) { }

  create(coa: IAccount) {
    return this.request.post<IAccount>(``, coa);
  }

  findAll(queryOption: ApiQueryOption, queryParams: any = {}) {
    return this.request.post<IApiPaginationResult<IAccount>>(`q`, queryOption, { params: queryParams });
  }

  findAllClasses() {
    return this.requestClass.get<any>(``)
    .map(response => response.data);
  }

  findAllLedger(coaId: string, queryParams: any = {}) {
    return this.request.get<IApiPaginationResult<IAccountLedger>>(`${coaId}/ledger`, {
      params: queryParams,
    });
  }

  load(coaId: string) {
    return this.request.get<IAccount>(`${coaId}`);
  }

  loadRelatedData() {
    return this.request.get<{ accountSequences: IAccountSequences[], accountTypes: IAccountType[] }>(`entry-related-data`);
  }

  update(coaId: string, updateObj: IAccount) {
    return this.request.put<IAccount>(`${coaId}`, updateObj);
  }
}

import { NgModule } from '@angular/core';

import { COAModule } from './coa.module';
import { COARoutingModule } from './coa-routing.module';

@NgModule({
  imports: [
    COAModule,
    COARoutingModule
  ]
})
export class COALazyModule { }

import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { COARestService } from '../coa-rest.service';
import { AccountingService } from '../../../core/core.module';

@Component({
  selector: 'app-coa-detail',
  templateUrl: 'coa-detail.component.html'
})
export class COADetailComponent implements OnInit {
  doc: IAccount;
  routeParams: any;

  constructor(
    public _accounting: AccountingService,
    private _coaRest: COARestService,
    private _route: ActivatedRoute,
  ) {
    this._route.params.subscribe(params => this.routeParams = params);
  }

  ngOnInit() {
    this.loadData();
  }

  loadData() {
    this._coaRest.load(this.routeParams.id).subscribe(coa => {
      this.doc = coa;
    });
  }
}

import * as _ from 'lodash';
import * as moment from 'moment';
import { ActivatedRoute, Router } from '@angular/router';
import { AutoComplete } from 'primeng/components/autocomplete/autocomplete';
import { Component, OnInit, ViewChild } from '@angular/core';
import { DataTable } from 'primeng/components/datatable/datatable';
import { Observable, Subscription } from 'rxjs';
import 'app/rxjs-imports.ts';
import { TranslateService } from '@ngx-translate/core';

import { SpinnerDirective } from '../../../shared/spinner/spinner.directive';

import {
  AccountingService,
  CommonService,
  ExportDataTableService,
  GridTableDataSource,
  GridTableService,
  GridTableToggleService,
} from '../../../core/core.module';
import { COARestService } from '../coa-rest.service';
import { TransactionRoutingService } from '../../transaction/transaction-routing.service';
import { RetryDialogComponent } from '../../../shared/retry-dialog/retry-dialog.component';

@Component({
  selector: 'app-coa-ledger',
  templateUrl: 'coa-ledger.component.html'
})
export class COALedgerComponent implements OnInit {
  @ViewChild('coaAC') elCoaAC: AutoComplete;
  @ViewChild('gridTable') public gridTable: DataTable;
  @ViewChild('pageLoading') elPageLoading: SpinnerDirective;
  @ViewChild('retryDialog') elRetryDialog: RetryDialogComponent;

  account: IAccount;
  compReady: boolean;
  filters: any = {};
  gridDataSource: GridTableDataSource<IAccountLedger> = new GridTableDataSource<IAccountLedger>();
  routeParams: any;

  retryDialogShownState: boolean = false;
  retryDialogOnRetry: Function;

  tableColumns: IGridTableColumn[] = [{
    i18nLabel: 'ui.coa.ledger.column.transactionDate',
    field: 'transactionDate',
  }, {
    i18nLabel: 'ui.coa.ledger.column.transactionNumber',
    field: 'transactionNumber',
    link: (row: IAccountLedger) => {
      return this._transactionRoutingService.resolve(row, 'detail');
    },
  }, {
    i18nLabel: 'ui.coa.ledger.column.transactionType',
    field: 'transactionType',
    formatter: (value, row: IAccountLedger) => {
      return _.startCase(value);
    },
  }, {
    i18nLabel: 'ui.coa.ledger.column.contact',
    field: 'contact.displayName',
  }, {
    i18nLabel: 'ui.coa.ledger.column.description',
    field: 'description',
  }, {
    i18nLabel: 'ui.coa.ledger.column.debit',
    field: 'debit',
    formatter: (value, row: IAccountLedger) => {
      return this._accounting.ac.formatMoney(value, '');
    },
    columnClasses: 'right-align',
  }, {
    i18nLabel: 'ui.coa.ledger.column.credit',
    field: 'credit',
    formatter: (value, row: IAccountLedger) => {
      return this._accounting.ac.formatMoney(value, '');
    },
    columnClasses: 'right-align',
  }];
  tableColumnsShow: boolean = false;
  tableColumnsToggle = this._gridTableToggle.mapToggleOptionsFromColumns(this.tableColumns);

  COASSuggestion: IAccount[] = [];

  ACCOAHandler = CommonService.remoteACItemsHandler({
    remoteParams: this.ACCOAParams.bind(this),
    remoteRequest: this._coaRest.findAll.bind(this._coaRest),
    remoteRequestMap: (response) => response.data,
    element: () => this.elCoaAC,
  });

  private loadDataTimeoutId: number;
  private gridFirstLoad: boolean = true;
  private subscriptions: { [key: string]: Subscription[] } = {
    load: []
  };

  constructor(
    private _accounting: AccountingService,
    private _activatedRoute: ActivatedRoute,
    private _coaRest: COARestService,
    private _dataTableExport: ExportDataTableService,
    private _gridTable: GridTableService,
    private _gridTableToggle: GridTableToggleService,
    private _router: Router,
    private _translate: TranslateService,
    private _transactionRoutingService: TransactionRoutingService
  ) {
    this._activatedRoute.params.subscribe(params => {
      this.routeParams = params;

      this._coaRest.load(this.routeParams.id).subscribe(doc => {
        this.account = doc;
        this.filters.account = doc;
      });
    });
  }

  ngOnInit() {
    this.loadData();
  }

  gridLoadDataWrapper() {
    // Grid always load at the first time. we dont want that. we already call on ngOnInit
    if (this.gridFirstLoad) {
      this.gridFirstLoad = false;
      return;
    }

    if (this.loadDataTimeoutId) {
      window.clearTimeout(this.loadDataTimeoutId);
      this.loadDataTimeoutId = null;
    }

    this.loadDataTimeoutId = window.setTimeout(() => {
      this.loadData();
    }, 300);
  }

  loadData() {
    this.elPageLoading.forceShow();

    const { qParams } = this.buildApiOptions({
      accountId: this.routeParams.id,
      take: this.gridDataSource.pager.itemsPerPage,
      skip: (this.gridDataSource.pager.pageNum - 1) * this.gridDataSource.pager.itemsPerPage,
      includeTotalCount: true,
    });

    this.subscriptions.load.forEach(sub => sub.unsubscribe());
    this.subscriptions.load.push(
      this.elRetryDialog.createRetryEntry(
        this._coaRest.findAllLedger(qParams.accountId, _.omit(qParams, ['accountId']))
          .finally(() => {
            this.elPageLoading.forceHide();
          })
      ).subscribe(response => {
        this.gridDataSource.updateFromApiPaginationResult(<any>response);
        this.compReady = true;
      })
    );
  }

  changeRoute() {
    this._router.navigate(['/accounts', this.filters.account.id, 'ledger']);
    this.loadData();
  }

  get exportRecords(): Observable<IAccountLedger[]> {
    const { qParams } = this.buildApiOptions({
      accountId: this.routeParams.id,
      take: this._dataTableExport.exportLimit,
      skip: 0,
    });

    return this._coaRest.findAllLedger(qParams.accountId, _.omit(qParams, 'accountId')).map(response => response.data);
  }

  exportPDF() {
    this.exportRecords.subscribe(records => {
      this._dataTableExport.export({
        records,
        mapOptions: this._dataTableExport.mapFromTableColumns(this.tableColumns),
        fileName: 'account-ledger',
        extension: 'pdf',
        templateData: {
          title: this._translate.instant('ui.transaction.list.title'),
        },
      });
    });
  }

  exportExcel() {
    this.exportRecords.subscribe(records => {
      this._dataTableExport.export({
        records: records,
        mapOptions: this._dataTableExport.mapFromTableColumns(this.tableColumns),
        fileName: 'account-ledger',
        extension: 'xls',
      });
    });
  }

  ACCOAParams(event: any, type: string) {
    let filters = [];

    if (type === 'search') {
      filters = [{
        filterValues: [{
          field: 'nameIndex',
          operator: 'contains',
          value: event.query
        }, {
          field: 'codeIndex',
          operator: 'contains',
          value: event.query
        }]
      }];
    }

    return [{
      filter: filters,
      sort: [{
        field: 'nameIndex',
        dir: 'asc'
      }],
      take: 30,
      skip: 0,
      includeTotalCount: false
    }];
  }

  private buildApiOptions(defaultOptions: any): {
    qBody: any,
    qParams: any,
  } {
    const populatedFilters: any = {};
    if (this.filters.account) {
      populatedFilters.accountId = this.filters.account.id;
    }
    if (this.filters.lowDate) {
      populatedFilters.lowDate = moment(this.filters.lowDate).format('YYYY-MM-DD');
    }
    if (this.filters.highDate) {
      populatedFilters.highDate = moment(this.filters.highDate).format('YYYY-MM-DD');
    }

    return {
      qBody: {},
      qParams: Object.assign({}, defaultOptions, populatedFilters)
    };
  }

  public onGridChange() {
    this.gridLoadDataWrapper();
  }

  public onGridPageChange() {
    this.gridLoadDataWrapper();
  }
}

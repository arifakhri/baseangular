import * as _ from 'lodash';
import { AutoComplete } from 'primeng/components/autocomplete/autocomplete';
import { CustomValidators } from 'ng2-validation';
import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';

import { COARestService } from '../../coa/coa-rest.service';
import { AutocompleteService, CommonService } from '../../../core/core.module';
import { RetryDialogComponent } from '../../../shared/retry-dialog/retry-dialog.component';

@Component({
  selector: 'app-coa-form',
  templateUrl: 'coa-form.component.html'
})
export class COAFormComponent implements OnInit {
  @Input() accountTypeId: string;
  @Input() state: string = 'create';
  @Input() doc: IAccount;
  @Input() form: FormGroup;
  @Output('compReady') compReady: EventEmitter<boolean> = new EventEmitter();
  @ViewChild('parentAccountAC') elParentAccountAC: AutoComplete;
  @ViewChild('retryDialog') elRetryDialog: RetryDialogComponent;

  accountSequences: IAccountSequences[] = [];
  accountTypes: IAccountType[] = [];

  COAS: IAccount[] = [];
  COASSuggestion: IAccount[] = [];

  ACCOAHandler = CommonService.remoteACItemsHandler({
    remoteParams: this.ACCOAParams.bind(this),
    remoteRequest: this._coaRest.findAll.bind(this._coaRest),
    remoteRequestMap: (response) => response.data,
    element: () => this.elParentAccountAC,
  });

  retryDialogShownState: boolean = false;
  retryDialogOnRetry: Function;

  constructor(
    public _autocomplete: AutocompleteService,
    private _coaRest: COARestService,
    private _formBuilder: FormBuilder,
  ) { }

  ngOnInit() {
    this.loadRelatedData();

    this.buildForm();
  }

  buildForm() {
    const accountTypeIdControl = new FormControl('', [Validators.required, Validators.maxLength(20)]);
    (<any>accountTypeIdControl).validatorData = {
      maxLength: 20
    };
    const codeControl = new FormControl('', [Validators.required, Validators.maxLength(15)]);
    (<any>codeControl).validatorData = {
      maxLength: 15
    };
    const nameControl = new FormControl('', [Validators.required, Validators.maxLength(50)]);
    (<any>nameControl).validatorData = {
      maxLength: 50
    };

    this.form.addControl('accountTypeId', accountTypeIdControl);
    this.form.addControl('accountType', new FormControl(null, Validators.required));
    this.form.addControl('code', codeControl);
    this.form.addControl('name', nameControl);
    this.form.addControl('description', new FormControl);
    this.form.addControl('isChildAccount', new FormControl(false));
    this.form.addControl('parentAccountId', new FormControl);
    this.form.addControl('parentAccount', new FormControl(null));
    this.form.addControl('openingBalance', new FormControl('', CustomValidators.number));
    this.form.addControl('openingBalanceDate', new FormControl);

    this.form.controls.accountType.valueChanges.subscribe(values => {
      const accountSequences = _.find(this.accountSequences, { accountTypeId: values.id });
      if (accountSequences) {
        this.form.controls.code.setValue(accountSequences.nextCode);
      }
    });
  }

  ACCOAParams(event: any, type: string) {
    let filters = [{
      filterValues: []
    }];

    if (type === 'search') {
      filters = [{
        filterValues: [{
          field: 'nameIndex',
          operator: 'contains',
          value: event.query
        }, {
          field: 'codeIndex',
          operator: 'contains',
          value: event.query
        }]
      }];
    }

    const accountType = this.form.get('accountType').value;
    if (accountType) {
      filters[0].filterValues.push({
        field: 'accountTypeId',
        operator: 'eq',
        value: accountType.id,
      });
    }

    // if (this.doc) {
    //   filters.push({ filterValues: [{ field: 'id', operator: 'noteq', value: this.doc.id }] });
    // }

    return [{
      filter: filters,
      sort: [{
        field: 'nameIndex',
        dir: 'asc'
      }],
      take: 30,
      skip: 0,
      includeTotalCount: false
    }];
  }

  loadRelatedData() {
    this.elRetryDialog.createRetryEntry(this._coaRest.loadRelatedData())
      .subscribe(related => {
        this.accountTypes = related.accountTypes;
        this.accountSequences = related.accountSequences;

        if (this.accountTypeId) {
          const accountType = _.find(this.accountTypes, { id: this.accountTypeId });
          this.form.controls.accountType.setValue(accountType);
        }

        Observable.from(this.accountTypes)
          .mergeMap(accountType => {
            const accountSequences$ = Observable.from(this.accountSequences)
              .filter(accountSequences => accountSequences.accountTypeId === accountType.id);

            return Observable.forkJoin(accountSequences$, (accountSequence) => {
              const index = this.accountTypes.map((el) => el.id).indexOf(accountSequence.accountTypeId)
              this.accountTypes[index].code = accountSequence.nextCode;
            });
          })
          .subscribe();

        this.compReady.emit(true);
      });
  }
}

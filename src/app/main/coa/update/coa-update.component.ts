import { ActivatedRoute, Router } from '@angular/router';
import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';
import { TranslateService } from '@ngx-translate/core';

import { CommonService, SystemMessageService } from '../../../core/core.module';
import { COARestService } from '../coa-rest.service';
import { SpinnerService } from '../../../shared/spinner/spinner.service';
import { RetryDialogComponent } from '../../../shared/retry-dialog/retry-dialog.component';

@Component({
  selector: 'app-coa-update',
  templateUrl: 'coa-update.component.html',
  providers: [SystemMessageService]
})
export class COAUpdateComponent implements OnInit {
  @ViewChild('retryDialog') elRetryDialog: RetryDialogComponent;

  compReady: any = {};
  form: FormGroup = new FormGroup({});
  routeParams: any;
  doc: IAccount;

  retryDialogShownState: boolean = false;
  retryDialogOnRetry: Function;

  constructor(
    @Inject('GlobalSystemMessage') private _globalSystemMessage: SystemMessageService,
    private _fb: FormBuilder,
    private _spinner: SpinnerService,
    private _route: ActivatedRoute,
    private _router: Router,
    public _systemMessage: SystemMessageService,
    private _coaRest: COARestService,
    private _translate: TranslateService,
  ) {
    this._route.params.subscribe(params => this.routeParams = params);
  }

  ngOnInit() {
    this.loadData();
  }

  loadData() {
    this.elRetryDialog.createRetryEntry(this._coaRest.load(this.routeParams.id))
      .subscribe(coa => {
        this.doc = coa;

        this.form.patchValue(coa);

        this.compReady.self = true;
      });
  }

  onSubmit({ saveAndNew, saveAndView }: any = {}) {
    if (this.formValid()) {
      this.save().subscribe(result => {
        if (saveAndNew) {
          this._router.navigateByUrl('/accounts/create');
        } else if (saveAndView) {
          this._router.navigate(['/accounts', result.id]);
        } else {
          this._router.navigateByUrl('/accounts');
        }
      });
    }
  }

  formValid(): boolean {
    if (!this.form.valid) {
      CommonService.markAsDirty(this.form);
      this._systemMessage.log({
        message: this._translate.instant('error.form'),
        type: 'error'
      });
      return false;
    }
    return true;
  }

  save() {
    const spinner = this._spinner.showDefault();

    const coa: IAccount = Object.assign({}, this.doc, this.form.value);
    return this._coaRest.update(this.routeParams.id, coa)
      .catch(error => {
        spinner.dispose();

        this._systemMessage.log({
          message: error,
          type: 'error'
        });
        return Observable.throw(error);
      })
      .do(result => {
        spinner.dispose();

        this._globalSystemMessage.log({
          message: this._translate.instant('success.coa.update'),
          type: 'success',
          showAs: 'growl',
          showSnackBar: false,
        });
      });
  }
}

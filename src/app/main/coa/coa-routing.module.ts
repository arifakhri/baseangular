import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { COALedgerComponent } from './ledger/coa-ledger.component';
import { COACreateComponent } from './create/coa-create.component';
import { COADetailComponent } from './detail/coa-detail.component';
import { COAListComponent } from './list/coa-list.component';
import { COAUpdateComponent } from './update/coa-update.component';

import { AuthorizationService } from '../../core/core.module';

export const routes: Routes = [{
  path: '',
  children: [{
    path: '',
    component: COAListComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'coa.list'
    }
  }, {
    path: 'create',
    component: COACreateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'coa.create'
    }
  }, {
    path: ':id',
    redirectTo: ':id/ledger',
    canActivate: [AuthorizationService],
    data: {
      name: 'coa.ledger'
    }
  }, {
    path: ':id/update',
    component: COAUpdateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'coa.update'
    }
  }, {
    path: ':id/ledger',
    component: COALedgerComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'coa.ledger'
    }
  }, {
    path: ':id/detail',
    component: COADetailComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'coa.detail'
    }
  }]
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class COARoutingModule { }

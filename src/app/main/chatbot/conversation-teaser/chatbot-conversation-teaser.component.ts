import * as moment from 'moment';
import * as _ from 'lodash';
import { Component, Inject, Input, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/components/common/menuitem';
import { TranslateService } from '@ngx-translate/core';

import { ChatbotConversationService } from '../chatbot-conversation.service';
import { EventService, StartingDataService } from '../../../core/core.module';

@Component({
  selector: 'app-chatbot-conversation-teaser',
  templateUrl: './chatbot-conversation-teaser.component.html',
})
export class ChatbotConversationTeaserComponent implements OnInit {
  @Input() channel: string;
  @Input() conversationKey: string;
  @Input() conversation: any;

  _ = _;
  defaultAvatar: string = '/assets/chatbot/avatar.png';

  contextMenus: MenuItem[] = [{
    icon: 'garbage-icon',
    label: this._translate.instant('ui.chatbot.conversationTeaser.contextMenu.endChat'),
    command: () => this.onEndChat(),
  }];

  constructor(
    private _chatbotConversation: ChatbotConversationService,
    @Inject('GlobalEvent') private _event: EventService,
    private _startingData: StartingDataService,
    private _translate: TranslateService,
  ) { }

  ngOnInit() {
    // this._chatbotConversation.getUser(this.channel, this.conversationKey, this.conversation.SenderID).subscribe(user => this.sender = user);

    // this._chatbotConversation.getLastMessage(this.channel, this.conversationKey).subscribe(lastMessage => this.lastMessage = lastMessage);
  }

  onClick() {
    this._event.emit('MAIN:CHATBOT:ACTIVATECONVERSATION', {
      channel: this.channel,
      conversationKey: this.conversationKey,
      conversation: this.conversation,
    });
  }

  onEndChat() {
    this._event.emit('MAIN:CHATBOT:ENDCHAT', {
      channel: this.channel,
      conversationKey: this.conversationKey,
      conversation: this.conversation,
    });
  }

  get lastMessageTime() {
    if (this.conversation && this.conversation.LastMessageTime) {
      return moment(this.conversation.LastMessageTime).calendar(moment(), {
        sameDay: 'HH:mm',
        lastDay: '[Yesterday]',
        lastWeek: 'dddd',
        sameElse: 'DD MMM YYYY'
      });
    }

    return null;
  }
}

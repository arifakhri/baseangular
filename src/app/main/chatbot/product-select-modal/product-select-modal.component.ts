import * as _ from 'lodash';
import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { DataTable } from 'primeng/components/datatable/datatable';
import { Subscription } from 'rxjs';
import 'app/rxjs-imports.ts';
import {
  AccountingService,
  GridTableDataSource,
  GridTableService,
  GridTableToggleService,
  SystemMessageService
} from '../../../core/core.module';
import { MProduct } from '../../product/product.model';
import { SpinnerDirective } from '../../../shared/spinner/spinner.directive';
import { RetryDialogComponent } from '../../../shared/retry-dialog/retry-dialog.component';
import { ProductRestService } from '../../product/product-rest.service';

@Component({
  selector: 'app-product-select-modal',
  templateUrl: './product-select-modal.component.html',
  providers: [SystemMessageService],
})
export class ProductSelectModalComponent implements OnInit {
  @Input() multiple: boolean = true;
  @ViewChild('gridTable') public gridTable: DataTable;
  @ViewChild('pageLoading') elPageLoading: SpinnerDirective;
  @ViewChild('retryDialog') elRetryDialog: RetryDialogComponent;
  @ViewChild('tableColumn1') elTableColumn1: ElementRef;
  @ViewChild('tableColumn2') elTableColumn2: ElementRef;
  @Output() cancel: EventEmitter<any> = new EventEmitter;
  @Output() submit: EventEmitter<any> = new EventEmitter;

  private gridFirstLoad: boolean = true;
  private loadDataTimeoutId: number;
  private subscriptions: { [key: string]: Subscription[] } = {
    load: []
  };

  _ = _;
  gridDataSource: GridTableDataSource<MProduct> = new GridTableDataSource<MProduct>();
  loadedVariants: any;
  products: MProduct[] = [];
  qParams: any = {
    keyword: null,
    includeVariants: true
  };
  selectedRecords: any;
  selectedProductVariantIds: any = [];
  tableColumns: IGridTableColumn[] = [];
  tableColumnsToggle: any;
  warehouseInventory: { [key: string]: MProduct[] };

  constructor(
    public _accounting: AccountingService,
    private _gridTable: GridTableService,
    private _gridTableToggle: GridTableToggleService,
    private _productRest: ProductRestService,
    public _systemMessage: SystemMessageService,
  ) {
    this.gridDataSource.pager.itemsPerPage = 10;
   }

  ngOnInit() {
    this.prepareTable();
    this.loadData();
  }

  findWarehouseInventory(productId: string) {
    this._productRest.findWarehouseInventory(productId).subscribe(result => {
      this.warehouseInventory = _.groupBy(result, 'warehouseId');
    });
  }

  loadVariants(record: MProduct) {
    this.loadedVariants = undefined;
    this._productRest.load(record.id, {
      includeVariants: true,
    }).subscribe(newRecord => {
      this.loadedVariants = newRecord.variants;
    }, error => {
      this.loadedVariants = false;
    });
  }

  prepareTable() {
      this.tableColumns = [{
        i18nLabel: 'ui.product.list.column.name',
        field: 'name',
        sort: true,
      }, {
        i18nLabel: 'ui.product.list.column.sku',
        field: 'sku',
        sort: true,
      }, {
        i18nLabel: 'ui.product.list.column.category',
        field: 'category.name',
        sort: true,
      }, {
        i18nLabel: 'ui.product.list.column.inventory.qty',
        field: 'inventory.qty',
        sort: true,
        template: this.elTableColumn2,
        columnClasses: 'right-align',
      }, {
        i18nLabel: 'ui.product.list.column.sellingPrice',
        field: 'masterPrice.unitPrice',
        sort: true,
        formatter: (value, row: MProduct) => {
          return this._accounting.ac.formatMoney(value, '');
        },
        columnClasses: 'right-align',
      }, {
        i18nLabel: 'ui.product.list.column.variantCount',
        field: 'variantCount',
        sort: true,
        template: this.elTableColumn1,
        columnClasses: 'center',
      }];
    this.tableColumnsToggle = this._gridTableToggle.mapToggleOptionsFromColumns(this.tableColumns);
  }

  gridLoadDataWrapper() {
    // Grid always load at the first time. we dont want that. we already call on ngOnInit
    if (this.gridFirstLoad) {
      this.gridFirstLoad = false;
      return;
    }

    if (this.loadDataTimeoutId) {
      window.clearTimeout(this.loadDataTimeoutId);
      this.loadDataTimeoutId = null;
    }

    this.loadDataTimeoutId = window.setTimeout(() => {
      this.loadData();
    }, 300);
  }

  loadData() {
    this.elPageLoading.forceShow();

    const qOption = this._gridTable.generateApiQueryOptionFromGridInfo(
      this.gridDataSource.pager,
      false,
      this.gridTable,
    );

    this.subscriptions.load.forEach(sub => sub.unsubscribe());
    this.subscriptions.load.push(
      this.elRetryDialog.createRetryEntry(
        this._productRest.findAll(qOption, this.qParams)
          .finally(() => {
            this.elPageLoading.forceHide();
          })
      ).subscribe(response => {
        this.products = response.data;
        this.gridDataSource.updateFromApiPaginationResult(response);
      })
    );
  }

  public onGridChange() {
    this.gridLoadDataWrapper();
  }

  public onGridPageChange() {
    this.gridLoadDataWrapper();
  }

  doSubmit() {
    this.elPageLoading.forceShow();
    this.selectedRecords.map(product => {
     product.variants.map(variant => {
       return this.selectedProductVariantIds.push(variant.id);
     });
    });
    this.submit.emit(this.selectedProductVariantIds);
  }
}

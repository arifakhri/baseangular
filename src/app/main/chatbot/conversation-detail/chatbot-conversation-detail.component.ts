import * as _ from 'lodash';
import { Component, Input, OnInit } from '@angular/core';

import { ChatbotConversationService } from '../chatbot-conversation.service';

@Component({
  selector: 'app-chatbot-conversation-detail',
  templateUrl: './chatbot-conversation-detail.component.html',
})
export class ChatbotConversationDetailComponent implements OnInit {
  @Input() channel: string;
  @Input() conversation: any;
  @Input() conversationKey: string;

  _ = _;

  conversationMessages: any;
  users: any;

  constructor(
    private _chatbotConversation: ChatbotConversationService,
  ) { }

  ngOnInit() {
    this.loadMessages();
  }

  loadMessages() {
    this._chatbotConversation.getMessages(this.channel, this.conversationKey)
      .subscribe(messages => {
        this.conversationMessages = messages;
      });
  }
}

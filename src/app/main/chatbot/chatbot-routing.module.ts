import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ChatbotPanelComponent } from './panel/chatbot-panel.component';

const routes: Routes = [{
  path: '',
  component: ChatbotPanelComponent,
  data: {
    name: 'chatbot.panel',
  },
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ],
})
export class ChatbotRoutingModule { }

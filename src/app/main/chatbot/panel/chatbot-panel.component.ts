import { Component, Inject, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore } from 'angularfire2/firestore';
import * as _ from 'lodash';
import { BehaviorSubject, Observable } from 'rxjs';
import 'app/rxjs-imports.ts';
import swal from 'sweetalert2';
import { EventService, StartingDataService, SystemMessageService } from '../../../core/core.module';
import { ChatbotRestService } from '../chatbot-rest.service';

@Component({
  selector: 'app-chatbot-panel',
  templateUrl: './chatbot-panel.component.html',
  styleUrls: ['./chatbot-panel.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [SystemMessageService],
})
export class ChatbotPanelComponent implements OnInit, OnDestroy {
  _ = _;

  compReady$: BehaviorSubject<boolean> = new BehaviorSubject(null);

  channels: string[] = [];
  conversations: any = {};

  constructor(
    private _angularFirestore: AngularFirestore,
    private _angularFireAuth: AngularFireAuth,
    private _chatbotRest: ChatbotRestService,
    @Inject('GlobalEvent') private _event: EventService,
    private _startingData: StartingDataService,
    public _systemMessage: SystemMessageService,
    private _translate: TranslateService,
  ) {
  }

  ngOnInit(): void {
    // this._angularFireAuth.auth.signInWithEmailAndPassword(environment.firebaseAuth.user, environment.firebaseAuth.password)

    this._chatbotRest.getAuthToken()
      .subscribe(
        token => {
          this._angularFireAuth.auth.signInWithCustomToken(token)
            .then(() => {
              this.compReady$.next(true);
              this.init();
            })
            .catch(console.error);
        },
        error => {

        }
      );
  }

  ngOnDestroy(): void {
    this._angularFireAuth.auth.signOut()
      .then(() => { })
      .catch(console.error);
  }

  init() {
    this.handleChannels();

    this._event.listen('MAIN:CHATBOT:ENDCHAT').subscribe(event => {
      swal({
        title: this._translate.instant(`confirm.chatbot.panel.endchat.label`),
        text: this._translate.instant(`confirm.chatbot.panel.endchat.description`),
        type: 'warning',
        showCancelButton: true
      }).then(() => {
        this._chatbotRest.endChat(
          event.data.conversation.ChannelID,
          event.data.conversation.ChannelAccountID,
          event.data.conversationKey,
          event.data.conversation.ConversationID)
          .catch(error => {
            this._systemMessage.log({
              message: error,
              showAs: 'growl',
              type: 'error',
            });

            return Observable.throw(error);
          })
          .subscribe(response => {
            // remove conversation manually. because event not triggered
            this.conversations[event.data.channel] = _.pickBy(this.conversations[event.data.channel], function (value, key) {
              return key !== event.data.conversationKey;
            });

            this._systemMessage.log({
              message: this._translate.instant('success.chatbot.panel.endchat'),
              showAs: 'growl',
              type: 'success',
              showSnackBar: false,
            });
          });
      }).catch();
    });
  }

  handleChannels() {
    this._angularFirestore.collection<any>(
      `/Companies/${this._startingData.data.companyId}/Conversations`,
    ).snapshotChanges().subscribe(convResponses => {
      const allConversations: any = {};
      convResponses.forEach(chat => {
        allConversations[chat.payload.doc.id] = chat.payload.doc.data();
      });

      const conversationsByChannels: any = _.transform<any, any>(allConversations, function (r, v, k) {
        r[v.ChannelID] = r[v.ChannelID] || {};
        r[v.ChannelID][k] = v;
      });

      this.channels = _.keys(conversationsByChannels);

      _.forEach(conversationsByChannels, (channelConversations, channelId) => {
        this.conversations[channelId] = channelConversations;
      });
    });
  }
}

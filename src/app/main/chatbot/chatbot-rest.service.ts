import { Injectable } from '@angular/core';

import { AuthenticationService, RequestService, StartingDataService } from '../../core/core.module';

import { APP_CONST } from '../../app.const';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';

@Injectable()
export class ChatbotRestService {
  baseURL = `${APP_CONST.API_MAIN}/integrations/external-bots`;
  request = this._request.new(this.baseURL);

  constructor(
    private _authentication: AuthenticationService,
    private _request: RequestService,
    private _startingData: StartingDataService,
  ) { }

  getAuthToken(): Observable<any> {
    return this.request.get('auth-token');
  }

  sendTextMessage(
    channelId: string,
    channelAccountId: string,
    conversationKey: string,
    conversationId: string,
    message: string,
  ) {
    return this.request.post(
      'send-text-message',
      {
        channelId: channelId,
        channelAccountId: channelAccountId,
        conversationKey: conversationKey,
        conversationId: conversationId,
        message: message,
      },
    );
  }

  sendImageMessage(
    channelId: string,
    channelAccountId: string,
    conversationKey: string,
    conversationId: string,
    imageUrl: string,
  ) {
    return this.request.post(
      'send-image-message',
      {
        channelId: channelId,
        channelAccountId: channelAccountId,
        conversationKey: conversationKey,
        conversationId: conversationId,
        imageUrl: imageUrl,
      },
    );
  }

  sendProductInfo(
    channelId: string,
    channelAccountId: string,
    conversationKey: string,
    conversationId: string,
    productVariantIds: any[]
  ) {
    return this.request.post(
      'send-product-info',
      {
        channelId: channelId,
        channelAccountId: channelAccountId,
        conversationKey: conversationKey,
        conversationId: conversationId,
        productVariantIds: productVariantIds
      },
    );
  }

  switchMode(
    channelId: string,
    channelAccountId: string,
    conversationKey: string,
    conversationId: string,
    conversationMode: string
  ) {
    return this.request.post(
      'switch-mode',
      {
        channelId: channelId,
        channelAccountId: channelAccountId,
        conversationKey: conversationKey,
        conversationId: conversationId,
        conversationMode: conversationMode
      },
    );
  }

  endChat(
    channelId: string,
    channelAccountId: string,
    conversationKey: string,
    conversationId: string,
  ) {
    return this.request.post(
      'endchat',
      {
        channelId: channelId,
        channelAccountId: channelAccountId,
        conversationKey: conversationKey,
        conversationId: conversationId,
      }
    );
  }
}

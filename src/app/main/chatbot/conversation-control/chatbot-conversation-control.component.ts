import { Component, Input, TemplateRef } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

import { ProductVariantRestService } from '../../product-variant/product-variant-rest.service';
import { ChatbotRestService } from '../chatbot-rest.service';
import { ApiFileRestService } from '../../../core/http/api-file-rest.service';
import { FileService } from '../../../core/file/file.service';

@Component({
  selector: 'app-chatbot-conversation-control',
  templateUrl: './chatbot-conversation-control.component.html',
})
export class ChatbotConversationControlComponent {
  @Input() channel: string;
  @Input() conversation: any;
  @Input() conversationKey: string;
  modalRef: BsModalRef;

  constructor(
    private _productVariantService: ProductVariantRestService,
    private _chatbotRest: ChatbotRestService,
    private _apiFileRest: ApiFileRestService,
    private _file: FileService,
    private modalService: BsModalService,
  ) {
  }

  sendTextMessage(formValue: any) {
    this._chatbotRest.sendTextMessage(
      this.conversation.ChannelID,
      this.conversation.ChannelAccountID,
      this.conversationKey,
      this.conversation.ConversationID,
      formValue.messageText,
    ).subscribe(console.log);
  }

  sendImageMessage(inputFile) {
    const fileBlob = inputFile.files[0];
    this._file.blobToBase64(fileBlob)
      .switchMap(base64File => {
        return this._apiFileRest.uploadTempBase64File(
          base64File.replace(`data:${fileBlob.type};base64,`, ''),
          fileBlob.name,
          fileBlob.type
        );
      })
      .switchMap(imgData => {
        return this._chatbotRest.sendImageMessage(
          this.conversation.ChannelID,
          this.conversation.ChannelAccountID,
          this.conversationKey,
          this.conversation.ConversationID,
          imgData.fileUrl,
        );
      })
      .subscribe(
        success => {

        },
        error => {
        });
  }

  openSelectProductModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, {class: 'modal-lg'});
  }

  onSelectProducts(products) {
    this._chatbotRest.sendProductInfo(
      this.conversation.ChannelID,
      this.conversation.ChannelAccountID,
      this.conversationKey,
      this.conversation.ConversationID,
      products)
      .subscribe(
        success => {
          this.modalRef.hide();
        },
        error => {
          this.modalRef.hide();
        });
  }
}

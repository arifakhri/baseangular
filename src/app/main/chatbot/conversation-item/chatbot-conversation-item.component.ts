import * as _ from 'lodash';
import * as moment from 'moment';
import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-chatbot-conversation-item',
  templateUrl: './chatbot-conversation-item.component.html',
})
export class ChatbotConversationItemComponent {
  @Input() conversation: any;
  @Input() conversationMessage: any;

  _= _;

  get userName() {
    if (this.conversationMessage && this.conversationMessage.UserType) {
      if (this.conversationMessage.UserType === 'bot') { return 'Bot'; }
      if (this.conversationMessage.UserType === 'agent') { return 'Agent'; }
      if (this.conversationMessage.UserType === 'user') { return this.conversation.SenderName; }
      return null;
    }

    return null;
  }

  get messageTime() {
    if (this.conversation && this.conversationMessage.Timestamp) {
      return moment(this.conversationMessage.Timestamp).calendar(moment(), {
        sameDay: 'HH:mm',
        lastDay: '[Yesterday] [at] HH:mm',
        lastWeek: 'dddd | HH:mm',
        sameElse: 'DD MMM YYYY | HH:mm'
      });
    }

    return null;
  }

  get isPartner() {
    return this.conversationMessage.UserType === 'user'; // tslint:disable-line
  }
}

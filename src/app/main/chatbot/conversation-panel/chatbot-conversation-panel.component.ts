import * as _ from 'lodash';
import { Component, Inject, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';

import { ChatbotConversationCacheService } from '../chatbot-conversation-cache.service';
import { ChatbotConversationService } from '../chatbot-conversation.service';
import { EventService } from '../../../core/core.module';
import { ChatbotRestService } from '../chatbot-rest.service';

@Component({
  selector: 'app-chatbot-conversation-panel',
  templateUrl: './chatbot-conversation-panel.component.html',
})
export class ChatbotConversationPanelComponent implements OnInit, OnChanges {
  @Input() conversations: any = [];

  isBotMode: boolean;

  _ = _;

  activeConversation: string;
  activeConversationChannel: string;
  activeConversationKey: string;

  constructor(
    private _chatbotRest: ChatbotRestService,
    private _chatbotConversation: ChatbotConversationService,
    private _chatbotConversationCache: ChatbotConversationCacheService,
    @Inject('GlobalEvent') private _event: EventService,
  ) {
    this.handleConversationChange();
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.conversations) {
      this.syncBotMode();
    }
  }

  syncBotMode() {
    if (this.conversations && this.activeConversationChannel && this.activeConversationKey) {
      const conv = this.conversations[this.activeConversationChannel][this.activeConversationKey];
      const newMode = conv.HandledBy === 'bot';
      if (newMode !== this.isBotMode) {
        this.isBotMode = conv.HandledBy === 'bot';
      }
    }
  }

  handleConversationChange() {
    this._event.listen('MAIN:CHATBOT:ACTIVATECONVERSATION')
      .subscribe(
        event => {
          this.activeConversation = event.data.conversation;
          this.activeConversationChannel = event.data.channel;
          this.activeConversationKey = event.data.conversationKey;
          this.syncBotMode();
        });
  }

  switchMode(event) {
    if (this.conversations && this.activeConversationChannel && this.activeConversationKey) {
      const conv = this.conversations[this.activeConversationChannel][this.activeConversationKey];
      let convMode: string;
      if (event.checked) {
        convMode = 'bot';
      } else {
        convMode = 'agent';
      }

      this._chatbotRest.switchMode(
        conv.ChannelID,
        conv.ChannelAccountID,
        this.activeConversationKey,
        conv.ConversationID,
        convMode,
      ).subscribe(console.log);
    }
  }
}

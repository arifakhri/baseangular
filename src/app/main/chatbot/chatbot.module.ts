import { CommonModule } from '@angular/common';
import { BsDropdownModule, ModalModule, PopoverModule } from 'ngx-bootstrap';
import { ContextMenuModule } from 'primeng/components/contextmenu/contextmenu';
import { DataTableModule } from 'primeng/components/datatable/datatable';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';
import { InputSwitchModule } from 'primeng/components/inputswitch/inputswitch';
import { TableModule } from 'primeng/components/table/table';
import { TranslateModule } from '@ngx-translate/core';
import { NgModule } from '@angular/core';

import { ChatbotConversationControlComponent } from './conversation-control/chatbot-conversation-control.component';
import { ChatbotConversationDetailComponent } from './conversation-detail/chatbot-conversation-detail.component';
import { ChatbotConversationItemComponent } from './conversation-item/chatbot-conversation-item.component';
import { ChatbotConversationListComponent } from './conversation-list/chatbot-conversation-list.component';
import { ChatbotConversationPanelComponent } from './conversation-panel/chatbot-conversation-panel.component';
import { ChatbotConversationTeaserComponent } from './conversation-teaser/chatbot-conversation-teaser.component';
import { ChatbotPanelComponent } from './panel/chatbot-panel.component';
import { ProductSelectModalComponent } from './product-select-modal/product-select-modal.component';

import { ChatbotConversationCacheService } from './chatbot-conversation-cache.service';
import { ChatbotConversationService } from './chatbot-conversation.service';
import { ChatbotRestService } from './chatbot-rest.service';
import { ChatbotService } from './chatbot.service';

import { AdminLayoutHeaderAppsComponent } from '../../layouts/admin/header-apps/admin-layout-header-apps.component';

import { SharedModule } from '../../shared/shared.module';
import { CoreModule } from '../../core/core.module';

export const PROVIDERS = [
  ChatbotConversationCacheService,
  ChatbotConversationService,
  ChatbotRestService,
  ChatbotService,
];

@NgModule({
  imports: [
    BsDropdownModule,
    CommonModule,
    ContextMenuModule,
    CoreModule,
    DataTableModule,
    FlexLayoutModule,
    FormsModule,
    InputSwitchModule,
    ModalModule,
    PopoverModule,
    TableModule,
    TranslateModule,
    SharedModule,
  ],
  declarations: [
    AdminLayoutHeaderAppsComponent,
    ChatbotConversationControlComponent,
    ChatbotConversationDetailComponent,
    ChatbotConversationItemComponent,
    ChatbotConversationListComponent,
    ChatbotConversationPanelComponent,
    ChatbotConversationTeaserComponent,
    ChatbotPanelComponent,
    ProductSelectModalComponent
  ],
})
export class ChatbotModule { }

import * as _ from 'lodash';
import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-chatbot-conversation-list',
  templateUrl: './chatbot-conversation-list.component.html',
})
export class ChatbotConversationListComponent {
  @Input() conversations: any = [];

  _ = _;
}

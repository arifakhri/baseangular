import * as _ from 'lodash';
import { AngularFirestore } from 'angularfire2/firestore';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';

import { StartingDataService } from '../../core/core.module';

@Injectable()
export class ChatbotConversationService {
  constructor(
    private _angularFirestore: AngularFirestore,
    private _startingData: StartingDataService,
  ) { }

  getMessages(channel, conversationKey) {
    return this._angularFirestore.collection(
      `/Companies/${this._startingData.data.companyId}/Conversations/${conversationKey}/Messages`,
      ref => ref.orderBy('Timestamp', 'asc')
    ).snapshotChanges().switchMap(values => this.transformKeyVal(values));
  }

  // getLastMessage(channel, conversationKey) {
  //   return this._angularFirestore.collection(
  //     `/Companies/${this._startingData.data.companyId}/Conversations/${conversationKey}/Messages`,
  //     ref => ref.orderBy('Timestamp', 'desc').limit(1)
  //   ).valueChanges().switchMap(values => this.transformSingle(values));
  // }

  // getUsers(channel, conversationKey) {
  //   return this._angularFirestore.collection(
  //     `/Companies/${this._startingData.data.companyId}/Conversations/${conversationKey}/Users`
  //   ).snapshotChanges().switchMap(values => this.transformKeyVal(values));
  // }

  // getUser(channel, conversationKey, userId, userType = 'User') {
  //   return this._angularFirestore.collection(
  //     `/Companies/${this._startingData.data.companyId}/Conversations/${conversationKey}/Users`,
  //     ref => ref.where('UserID', '==', userId).where('UserType', '==', userType).limit(1),
  //   ).valueChanges().switchMap(values => this.transformSingle(values));
  // }

  transformKeyVal(values: any) {
    return Observable.of(_.transform<any, any>(values, (r, v, k) => r[v.payload.doc.id] = v.payload.doc.data()));
  }

  transformSingle(values: any) {
    return Observable.of(_.head(values));
  }

}

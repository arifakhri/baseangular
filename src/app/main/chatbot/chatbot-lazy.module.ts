import { NgModule } from '@angular/core';

import { ChatbotModule } from './chatbot.module';
import { ChatbotRoutingModule } from './chatbot-routing.module';

@NgModule({
  imports: [
    ChatbotModule,
    ChatbotRoutingModule,
  ],
})
export class ChatbotLazyModule { }

import * as moment from 'moment';
import { Component, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

import { AdminLayoutService } from '../../../layouts/layouts.module';
import { GridTableFilterService } from '../../../core/core.module';
import { ReportsBalanceRestService } from '../reports-balance-rest.service';

@Component({
  selector: 'app-reports-balance-preview',
  templateUrl: './reports-balance-preview.component.html',
})
export class ReportsBalancePreviewComponent {
  @ViewChild('reportViewer') elReportViewer: any;

  compReady: boolean = false;
  data: any = [];
  form: FormGroup = new FormGroup({});
  reportViewerReady: boolean = false;

  reportParams: any = {
    title: 'Balance Sheet'
  };

  filtersMap: IGridTableFilterMap = {
    date: {
      targetVar: 'date',
      targetFilter: 'param',
      dateFormat: 'YYYY-MM-DD',
    }
  };

  constructor(
    private _adminLayout: AdminLayoutService,
    private _gridTableFilter: GridTableFilterService,
    private _reportsBalanceRest: ReportsBalanceRestService,
  ) {
    this._adminLayout.containerType = 1;

    this.buildForm();
    this.loadData();
  }

  buildForm() {
    this.form.addControl('date', new FormControl(new Date()));
  }

  loadData() {
    const filters = this._gridTableFilter.parse(this.form.value, this.filtersMap);
    this.compReady = false;

    this._reportsBalanceRest.findAll(filters.qParams).subscribe(response => {
      this.data = this.prepareData(response);

      this.prepareParams();
    }, null, () => this.compReady = true);
  }

  prepareParams() {
    const filterValues = this.form.value;

    if (filterValues.date) {
      this.reportParams.date = moment(filterValues.date).format('DD/MM/YYYY');
    }
  }

  prepareData(rawData: IReportsBalance[]) {
    const results: Array<any> = [];

    for (let i = 0; i < rawData.length; i++) {
      const element = rawData[i];
      const rowData = <any>Object.assign({}, element);
      let rowDescription = element.description;
      if (element && element.level && element.level > 0) {
        for (let j = 1; j <= element.level; j++) {
          rowDescription = '&nbsp;&nbsp;&nbsp;&nbsp;' + rowDescription; // add padding
        }
      }

      rowData.rowDescription = rowDescription;
      results.push(rowData);
    }

    return results;

    // return _(rawData).groupBy('accountClass').reduce((array1, children1, key1) => {
    //   const populatedChildren1 = _(<any>children1).groupBy('accountType').reduce((array2, children2, key2) => {
    //     array2.push({
    //       name: key2,
    //       children: _(children2).map(child2 => ({ name: child2.accountName, total: child2.total })).value(),
    //       total: _.sumBy(<any>children2, 'total'),
    //     });
    //     return array2;
    //   }, []);

    //   array1.push({
    //     name: key1,
    //     children: populatedChildren1,
    //     total: _.sumBy(populatedChildren1, 'total'),
    //   });
    //   return array1;
    // }, []);
  }
}

import { NgModule } from '@angular/core';

import { ReportsBalanceModule } from './reports-balance.module';
import { ReportsBalanceRoutingModule } from './reports-balance-routing.module';

@NgModule({
  imports: [
    ReportsBalanceModule,
    ReportsBalanceRoutingModule,
  ],
})
export class ReportsBalanceLazyModule { }

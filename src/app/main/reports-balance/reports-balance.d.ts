declare interface IReportsBalance {
  rowType: string;
  rowIndex: number;
  parentRowIndex: number;
  accountId: string;
  accountCode: string;
  accountName: string;
  description: string;
  accountClassId: string;
  accountClassName: string;
  accountTypeId: string;
  accountTypeName: string;
  level: number;
  journalAmount: number;
  amount: number;
}
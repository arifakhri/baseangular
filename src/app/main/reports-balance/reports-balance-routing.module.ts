import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ReportsBalancePreviewComponent } from './preview/reports-balance-preview.component';

import { AuthorizationService } from '../../core/core.module';

const routes: Routes = [{
  path: '',
  children: [{
    path: '',
    component: ReportsBalancePreviewComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'reportsBalance.preview'
    }
  }]
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ],
})
export class ReportsBalanceRoutingModule { }

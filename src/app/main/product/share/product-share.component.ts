import * as _ from 'lodash';
import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';

import { UrlService } from '../../../core/common/url.service';

import { MProduct } from '../product.model';

@Component({
  selector: 'app-product-share',
  templateUrl: 'product-share.component.html'
})
export class ProductShareComponent implements OnChanges, OnInit {
  @Input() product: MProduct;
  @Input() show: boolean = false;
  @Output() close: EventEmitter<boolean> = new EventEmitter();
  @ViewChild('productShareModal') elProductShareModal: ModalDirective;

  constructor(
    private _url: UrlService,
  ) { }

  ngOnInit() {
    this.elProductShareModal.onHidden.subscribe(() => this.close.emit(true));
  }

  ngOnChanges(changes: SimpleChanges) {
    if (_.has(changes, 'show')) {
      if (this.show && !this.elProductShareModal.isShown) {
        this.elProductShareModal.show();
      } else if (!this.show && this.elProductShareModal.isShown) {
        this.elProductShareModal.hide();
      }
    }
  }

  shareFacebook() {
    const params: any = {
      u: 'http://google.com',
      title: this.product.name,
      description: this.product.salesDescription || ''
    };
    if (this.product.mainPicture) {
      params.picture = this.product.mainPicture.thumbnailUrl;
    }
    const qs = this._url.objectToQueryString(params);
    const targetURL = `https://www.facebook.com/sharer.php?${qs}`;
    window.open(targetURL, '_blank');
  }
}

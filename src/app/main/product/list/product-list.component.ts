import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import * as _ from 'lodash';
import * as SnackBar from 'node-snackbar';
import { DataTable } from 'primeng/components/datatable/datatable';
import { Observable, Subscription } from 'rxjs';
import 'app/rxjs-imports.ts';
import swal from 'sweetalert2';

import {
  AccountingService,
  ExportDataTableService,
  GridTableDataSource,
  GridTableService,
  GridTableToggleService,
} from '../../../core/core.module';
import { AdminLayoutService } from '../../../layouts/layouts.module';
import { RetryDialogComponent } from '../../../shared/retry-dialog/retry-dialog.component';
import { SpinnerDirective } from '../../../shared/spinner/spinner.directive';
import { SalesChannelRestService } from '../../sales-channel/sales-channel-rest.service';
import { MSalesChannelAccount } from '../../sales-channel/sales-channel.model';
import { ProductRestService } from '../product-rest.service';
import { MProduct } from '../product.model';

@Component({
  selector: 'app-product-list',
  templateUrl: 'product-list.component.html',
})
export class ProductListComponent implements OnInit {
  @ViewChild('gridTable') public gridTable: DataTable;
  @ViewChild('pageLoading') elPageLoading: SpinnerDirective;
  @ViewChild('tableColumn1') elTableColumn1: ElementRef;
  @ViewChild('tableColumn2') elTableColumn2: ElementRef;
  @ViewChild('salesChannelCol') elSalesChannelCol: ElementRef;
  @ViewChild('retryDialog') elRetryDialog: RetryDialogComponent;

  _ = _;
  compReady: boolean;
  products: MProduct[] = [];
  gridDataSource: GridTableDataSource<MProduct> = new GridTableDataSource<MProduct>();
  selectedRecords: MProduct[] = [];
  warehouseInventory: { [key: string]: MProduct[] };
  salesChannelAccounts: MSalesChannelAccount[] = [];

  tableColumns: IGridTableColumn[] = [];
  tableColumnsToggle: any;

  retryDialogShownState: boolean = false;
  retryDialogOnRetry: Function;

  loadedVariants: any;

  qParams: any = {
    keyword: null,
    includeSalesChannels: true
  };

  private loadDataTimeoutId: number;
  private gridFirstLoad: boolean = true;
  private subscriptions: { [key: string]: Subscription[] } = {
    load: []
  };

  private productStateType: string = 'all';

  constructor(
    public _accounting: AccountingService,
    private _activatedRoute: ActivatedRoute,
    private _adminLayout: AdminLayoutService,
    private _gridTable: GridTableService,
    private _dataTableExport: ExportDataTableService,
    private _gridTableToggle: GridTableToggleService,
    private _productRest: ProductRestService,
    private _router: Router,
    private _salesChannelRest: SalesChannelRestService,
    private _translate: TranslateService,
  ) {
    this._adminLayout.containerType = 1;

    this._activatedRoute.data.subscribe(data => {
      this.productStateType = _.snakeCase(data.name.substr(data.name.lastIndexOf('.') + 1));
    });
  }

  ngOnInit() {
    this.prepareTable();

    this.elPageLoading.forceShow();
    this.loadSalesChannelAccounts().do(() => this.loadData()).subscribe();
  }

  findWarehouseInventoy(productId: string) {
    this._productRest.findWarehouseInventory(productId).subscribe(result => {
      this.warehouseInventory = _.groupBy(result, 'warehouseId');
    });
  }

  prepareTable() {
    if (this.productStateType === 'all' || this.productStateType === 'inventory') {
      this.tableColumns = [{
        i18nLabel: 'ui.product.list.column.name',
        field: 'name',
        link: (row: MProduct) => {
          return `/products/${row.id}`;
        },
        sort: true,
      }, {
        i18nLabel: 'ui.product.list.column.sku',
        field: 'sku',
        sort: true,
      }, {
        i18nLabel: 'ui.product.list.column.category',
        field: 'category.name',
        sort: true,
      }, {
        i18nLabel: 'ui.product.list.column.inventory.qty',
        field: 'inventory.qty',
        sort: true,
        template: this.elTableColumn2,
        columnClasses: 'right-align',
      }, {
        i18nLabel: 'ui.product.list.column.sellingPrice',
        field: 'masterPrice.unitPrice',
        sort: true,
        formatter: (value, row: MProduct) => {
          return this._accounting.ac.formatMoney(value, '');
        },
        columnClasses: 'right-align',
      }, {
        i18nLabel: 'ui.product.list.column.inventory.averageCost',
        field: 'inventory.averageCost',
        sort: true,
        formatter: (value, row: MProduct) => {
          return this._accounting.ac.formatMoney(value, '');
        },
        columnClasses: 'right-align',
      }, {
        i18nLabel: 'ui.product.list.column.inventory.lastCost',
        field: 'inventory.lastCost',
        sort: true,
        formatter: (value, row: MProduct) => {
          return this._accounting.ac.formatMoney(value, '');
        },
        columnClasses: 'right-align',
      }, {
        i18nLabel: 'ui.product.list.column.variantCount',
        field: 'variantCount',
        sort: true,
        template: this.elTableColumn1,
        columnClasses: 'center',
      }, {
        i18nLabel: 'ui.product.list.column.channel',
        field: '',
        sort: false,
        template: this.elSalesChannelCol,
      }];
    } else {
      this.tableColumns = [{
        i18nLabel: 'ui.product.list.column.name',
        field: 'name',
        link: (row: MProduct) => {
          return `/products/${row.id}`;
        },
        sort: true,
      }, {
        i18nLabel: 'ui.product.list.column.sku',
        field: 'sku',
        sort: true,
      }, {
        i18nLabel: 'ui.product.list.column.category',
        field: 'category.name',
        sort: true,
      }, {
        i18nLabel: 'ui.product.list.column.channel',
        field: '',
        sort: false,
        template: this.elSalesChannelCol,
      }];
    }
    this.tableColumnsToggle = this._gridTableToggle.mapToggleOptionsFromColumns(this.tableColumns);
  }

  gridLoadDataWrapper() {
    // Grid always load at the first time. we dont want that. we already call on ngOnInit
    if (this.gridFirstLoad) {
      this.gridFirstLoad = false;
      return;
    }

    if (this.loadDataTimeoutId) {
      window.clearTimeout(this.loadDataTimeoutId);
      this.loadDataTimeoutId = null;
    }

    this.loadDataTimeoutId = window.setTimeout(() => {
      this.loadData();
    }, 300);
  }

  populateQParams() {
    if (this.productStateType !== 'all') {
      this.qParams.productType = this.productStateType;
    }
  }

  loadData() {
    this.elPageLoading.forceShow();

    const qOption = this._gridTable.generateApiQueryOptionFromGridInfo(
      this.gridDataSource.pager,
      true,
      this.gridTable,
    );

    this.populateQParams();

    this.subscriptions.load.forEach(sub => sub.unsubscribe());
    this.subscriptions.load.push(
      this.elRetryDialog.createRetryEntry(
        this._productRest.findAll(qOption, this.qParams)
          .finally(() => {
            this.elPageLoading.forceHide();
          })
      ).subscribe(response => {
        this.products = response.data;
        this.gridDataSource.updateFromApiPaginationResult(response);

        this.compReady = true;
      })
    );
  }

  loadVariants(record: MProduct) {
    this.loadedVariants = undefined;
    this._productRest.load(record.id, {
      includeVariants: true,
    }).subscribe(newRecord => {
      this.loadedVariants = newRecord.variants;
    }, error => {
      this.loadedVariants = false;
    });
  }

  loadSalesChannelAccounts() {
    return this._salesChannelRest.findAllAccounts().do(accounts => {
      return this.salesChannelAccounts = accounts;
    });
  }

  get exportRecords(): Observable<MProduct[]> {
    const qOption = {
      ...this._gridTable.generateApiQueryOptionFromGridInfo(
        this.gridDataSource.pager,
        false,
        this.gridTable,
      ),
      skip: 0,
      take: this._dataTableExport.exportLimit,
    };

    this.populateQParams();

    return this.elRetryDialog.createRetryEntry(
      this._productRest.findAll(qOption, this.qParams).map(response => response.data)
    );
  }

  exportPDF() {
    this.exportRecords.subscribe(records => {
      this._dataTableExport.export({
        records,
        mapOptions: this._dataTableExport.mapFromTableColumns(this.tableColumns),
        fileName: 'products',
        extension: 'pdf',
        templateData: {
          title: this._translate.instant('ui.transaction.list.title'),
        },
      });
    });
  }

  exportExcel() {
    this.exportRecords.subscribe(records => {
      this._dataTableExport.export({
        records: records,
        mapOptions: this._dataTableExport.mapFromTableColumns(this.tableColumns),
        fileName: 'products',
        extension: 'xls',
      });
    });
  }

  showDeleteDialog(productId: string) {
    swal({
      title: this._translate.instant('ui.product.confirm.delete.label'),
      text: this._translate.instant('ui.product.confirm.delete.description'),
      type: 'question',
      showCancelButton: true,
    })
      .then(() => {
        this._productRest.delete(productId).subscribe(response => {
          this.gridLoadDataWrapper();
          SnackBar.show({
            text: this._translate.instant('ui.product.success.delete'),
            pos: 'bottom-right'
          });
        });
      })
      .catch(() => { });
  }

  public onGridChange() {
    this.gridLoadDataWrapper();
  }

  public onGridPageChange() {
    this.gridLoadDataWrapper();
  }

  onSelectedChannelAccount(salesChannelAccount: { account: MSalesChannelAccount }) {
    this._router.navigate(['/products/import-channel', salesChannelAccount.account.channelAccountId]);
  }
}

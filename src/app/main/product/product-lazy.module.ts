import { NgModule } from '@angular/core';

import { ProductModule } from './product.module';
import { ProductRoutingModule } from './product-routing.module';

@NgModule({
  imports: [
    ProductModule,
    ProductRoutingModule
  ]
})
export class ProductLazyModule { }

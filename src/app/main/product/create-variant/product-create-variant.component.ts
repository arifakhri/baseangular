import { Component, EventEmitter, Inject, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';
import { ProductRestService } from '../product-rest.service';
import { ProductService } from '../product.service';
import { CommonService, SystemMessageService } from '../../../core/core.module';
import { TranslateService } from '@ngx-translate/core';

import { ProductFormService } from '../product-form.service';

import { MProduct } from '../product.model';

@Component({
  selector: 'app-product-create-variant',
  templateUrl: './product-create-variant.component.html',
  providers: [SystemMessageService]
})
export class ProductCreateVariantComponent implements OnInit {
  @Output() afterSubmit: EventEmitter<ISettingsUser> = new EventEmitter();
  @Output() close: EventEmitter<boolean> = new EventEmitter();

  @Input() doc: MProduct;

  form: FormGroup = new FormGroup({});
  defaultValue: any = { isMaster: true };

  constructor(
    @Inject('GlobalSystemMessage') private _globalSystemMessage: SystemMessageService,
    private _fb: FormBuilder,
    private _product: ProductService,
    private _productForm: ProductFormService,
    private _productRest: ProductRestService,
    public _systemMessage: SystemMessageService,
    private _translate: TranslateService,
  ) {
    this.form = this._productForm.buildFormChildVariant(this.defaultValue);
  }

  ngOnInit() {
  }

  onSubmit(): void {
    if (this.formValid()) {
      this.save();
    }
  }

  formValid(): boolean {
    if (!this.form.valid) {
      CommonService.markAsDirty(this.form);
      this._systemMessage.log({
        message: this._translate.instant('error.form'),
        type: 'error'
      });
      return false;
    }
    return true;
  }

  save(): void {
    const productVariants: IProductVariant = Object.assign({}, this.form.value);
    this._productRest.createVariant(this.doc.id, productVariants)
      .catch(error => {
        this._systemMessage.log({
          message: error,
          type: 'error'
        });
        return Observable.throw(error);
      })
      .subscribe(result => {
        this._globalSystemMessage.log({
          message: this._translate.instant('success.productVariant.create'),
          type: 'success',
          showAs: 'growl',
          showSnackBar: false,
        });
        this.form.reset();
        this.afterSubmit.emit();
      });
  }
}

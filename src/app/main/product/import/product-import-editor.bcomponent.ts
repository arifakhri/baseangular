import 'app/rxjs-imports.ts';

import { Input, NgZone, OnInit } from '@angular/core';
import { FormArray, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import * as _ from 'lodash';
import { Observable } from 'rxjs';

import { MFormValidationFailed } from '../../../core/common/form.model';
import { CommonService, FormService, InjectorService, SystemMessageService } from '../../../core/core.module';
import { BasePageBComponent } from '../../../shared/base/base.module';
import { ProductFormService } from '../product-form.service';
import { MProductRelatedData } from '../product.model';
import { ProductService } from '../product.service';
import { ProductImportService } from './product-import.service';

export class ProductImportEditorBComponent extends BasePageBComponent implements OnInit {
  @Input() relatedData: MProductRelatedData;
  @Input() formState: any = {};

  filteredRecords: any = [];
  activeFilter = 'all';

  selectedUOMS: IUOM[][] = [];

  form: FormGroup = new FormGroup({
    products: new FormArray([])
  });

  lastResults: any = [];

  public _form = InjectorService.get<FormService>(FormService);
  public _globalSystemMessage = InjectorService.get<SystemMessageService>('GlobalSystemMessage');
  public _ngZone = InjectorService.get<NgZone>(NgZone);
  public _product = InjectorService.get<ProductService>(ProductService);
  public _productForm = InjectorService.get<ProductFormService>(ProductFormService);
  public _productImport = InjectorService.get<ProductImportService>(ProductImportService);
  public _router = InjectorService.get<Router>(Router);
  public _translate = InjectorService.get<TranslateService>(TranslateService);

  ngOnInit() {
    this.filteredRecords = this.formState.records;

    this.buildFormFromRecords(this.filteredRecords);
  }

  buildFormFromRecords(records: any) { }

  save(): Observable<any> { return Observable.of([]); }

  onSaveFailed() {
    _.forEach(this.lastResults, (result, resultIdx) => {
      const product = _.find(this.formState.records, { seqNumber: result.seqNumber }) as any;
      if (product) {
        product.status = result.success ? 'success' : 'failed';
        product.errors = result.errors;
      }
    });

    this.filterRecords('failed');
  }

  onValidationFailed(errors: MFormValidationFailed[]) {
    const formSeqNumberErrors = [];
    _.forEach(errors, error => {
      if (error.parentArrayKey === 'products') {
        const seqNumber: number = error.control.parent.get('_identifier').value;
        formSeqNumberErrors.push(seqNumber);

        const product = _.find(this.formState.records, { seqNumber });
        if (product) {
          product.status = 'validation_failed';
        }
      }
    });

    this.filterRecords('validation_failed');
  }

  onSaveSuccess() { }

  formValid(): boolean {
    if (!this.form.valid) {
      CommonService.markAsDirty(this.form);
      this.page._systemMessage.log({
        message: this._translate.instant('error.form'),
        type: 'error',
      });
      return false;
    }
    return true;
  }

  process() {
    if (this.formValid()) {
      return this.save();
    }

    const errors = this._form.getFormValidationErrors(this.form);
    this.onValidationFailed(errors);

    return Observable.throw(false);
  }

  filterRecords(status: string) {
    if (status === this.activeFilter) {
      return;
    }

    if (status === 'all') {
      this.filteredRecords = this.formState.records;
      return;
    }

    if (status === 'import') {
      this.filteredRecords = _.filter(this.formState.records, record => record.status === 'failed' || record.status === 'validation_failed' || !record.status);
    } else {
      this.filteredRecords = _.filter(this.formState.records, { status });
      this.activeFilter = status;
    }
  }
}

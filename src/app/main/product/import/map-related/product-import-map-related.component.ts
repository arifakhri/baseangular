import * as _ from 'lodash';
import { Component, EventEmitter, Input, NgZone, OnInit, Output } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';

import { CommonService } from '../../../../core/core.module';
import { ProductImportService } from '../product-import.service';

import { MProductRelatedData } from '../../product.model';

@Component({
  selector: 'app-product-import-map-related',
  templateUrl: './product-import-map-related.component.html',
})
export class ProductImportMapRelatedComponent implements OnInit {
  @Input() relatedData: MProductRelatedData;
  @Input() formState: any = {};
  @Output() onSkip: EventEmitter<boolean> = new EventEmitter;

  unmatchAttributes: {
    label: string;
    form: FormGroup
  }[] = [];
  unmatchBrands: {
    label: string;
    form: FormGroup
  }[] = [];
  unmatchCategories: {
    label: string;
    form: FormGroup
  }[] = [];
  unmatchUnits: {
    label: string;
    form: FormGroup
  }[] = [];

  form: FormGroup = new FormGroup({
    attributes: new FormArray([]),
    brands: new FormArray([]),
    categories: new FormArray([]),
    units: new FormArray([]),
  });

  constructor(
    private _ngZone: NgZone,
    private _productImport: ProductImportService,
  ) { }

  ngOnInit() {
    this._ngZone.run(() => {
      this._productImport.resolveRecords(this.formState.records, {
        accounts: this.relatedData.accounts,
        attributes: this.relatedData.attributes,
        brands: this.relatedData.productBrands,
        categories: this.relatedData.categories,
        taxes: this.relatedData.taxes,
        uoms: this.relatedData.uoms,
      });

      this.unmatchAttributes = this.unmatchAttributes.concat(_(this.formState.records)
        .filter(record => (
          !(<any>record).isVariant &&
          (<any>record).attribute1Search &&
          !(<any>record).variantAttribute1 &&
          !(<any>record).variantAttribute1Id)
        )
        .uniqBy('attribute1Search')
        .map(record => {
          const formGroup = this.buildFormChildAttribute((<any>record).attribute1Search, 1);
          return {
            index: 1,
            label: (<any>record).attribute1Search,
            form: formGroup,
          };
        })
        .value());

      this.unmatchAttributes = this.unmatchAttributes.concat(_(this.formState.records)
        .filter(record => (
          !(<any>record).isVariant &&
          (<any>record).attribute2Search &&
          !(<any>record).variantAttribute2 &&
          !(<any>record).variantAttribute2Id)
        )
        .uniqBy('attribute2Search')
        .map(record => {
          const formGroup = this.buildFormChildAttribute((<any>record).attribute2Search, 2);
          return {
            index: 2,
            label: (<any>record).attribute2Search,
            form: formGroup,
          };
        })
        .value());

      this.unmatchAttributes = this.unmatchAttributes.concat(_(this.formState.records)
        .filter(record => (
          !(<any>record).isVariant &&
          (<any>record).attribute3Search &&
          !(<any>record).variantAttribute3 &&
          !(<any>record).variantAttribute3Id)
        )
        .uniqBy('attribute3Search')
        .map(record => {
          const formGroup = this.buildFormChildAttribute((<any>record).attribute3Search, 3);
          return {
            index: 3,
            label: (<any>record).attribute3Search,
            form: formGroup,
          };
        })
        .value());

      this.unmatchBrands = _(this.formState.records)
        .filter(record => (<any>record).brandSearch && !(<any>record).brand && !(<any>record).brandId)
        .uniqBy('brandSearch')
        .map(record => {
          const formGroup = this.buildFormChildBrand((<any>record).brandSearch);
          return {
            label: (<any>record).brandSearch,
            form: formGroup,
          };
        })
        .value();

      this.unmatchCategories = _(this.formState.records)
        .filter(record => (<any>record).categorySearch && !(<any>record).category && !(<any>record).categoryId)
        .uniqBy('categorySearch')
        .map(record => {
          const formGroup = this.buildFormChildCategory((<any>record).categorySearch);
          return {
            label: (<any>record).categorySearch,
            form: formGroup,
          };
        })
        .value();

      this.unmatchUnits = _(this.formState.records)
        .filter(record => (<any>record).uomSearch && !(<any>record).uom && !(<any>record).uomId)
        .uniqBy('uomSearch')
        .map(record => {
          const formGroup = this.buildFormChildUnit((<any>record).uomSearch);
          return {
            label: (<any>record).uomSearch,
            form: formGroup,
          };
        })
        .value();
    });

    if (
      !this.unmatchAttributes.length &&
      !this.unmatchBrands.length &&
      !this.unmatchCategories.length &&
      !this.unmatchUnits.length
    ) {
      this.onSkip.emit();
    }
  }

  buildFormChildBrand(targetBrand: string) {
    const formGroup = new FormGroup({
      brand: new FormControl(null),
      brandId: new FormControl(null),
    });
    formGroup.get('brand').valueChanges.subscribe(brand => {
      if (brand) {
        this.patchRecordsBrand(targetBrand, brand);
      }
    });
    const formBrands = this.form.get('brands') as FormArray;
    formBrands.push(formGroup);

    return formGroup;
  }

  patchRecordsBrand(targetBrand: string, brand: IProductBrand) {
    const records = _.filter(this.formState.records, { brandSearch: targetBrand });
    _.forEach(records, record => {
      (<any>record).brand = brand;
      (<any>record).brandId = brand.id;
    });
  }

  buildFormChildCategory(targetCategory: string) {
    const formGroup = new FormGroup({
      category: new FormControl(null),
      categoryId: new FormControl(null),
    });
    formGroup.get('category').valueChanges.subscribe(category => {
      if (category) {
        this.patchRecordsCategory(targetCategory, category);
      }
    });
    const formCategories = this.form.get('categories') as FormArray;
    formCategories.push(formGroup);

    return formGroup;
  }

  patchRecordsCategory(targetCategory: string, category: IProductCategory) {
    const records = _.filter(this.formState.records, { categorySearch: targetCategory });
    _.forEach(records, record => {
      (<any>record).category = category;
      (<any>record).categoryId = category.id;
    });
  }

  buildFormChildUnit(targetUnit: string) {
    const formGroup = new FormGroup({
      uom: new FormControl(null, Validators.required),
      uomId: new FormControl(null, Validators.required),
    });
    formGroup.get('uom').valueChanges.subscribe(uom => {
      if (uom) {
        this.patchRecordsUnit(targetUnit, uom);
      }
    });
    const formUnits = this.form.get('units') as FormArray;
    formUnits.push(formGroup);

    return formGroup;
  }

  patchRecordsUnit(targetUnit: string, unit: IUOM) {
    const records = _.filter(this.formState.records, { uomSearch: targetUnit });
    _.forEach(records, record => {
      (<any>record).uom = unit;
      (<any>record).uomId = unit.id;
    });
  }

  buildFormChildAttribute(targetAttribute: string, index: number) {
    const formGroup = new FormGroup({
      [`variantAttribute${index}`]: new FormControl(null, Validators.required),
      [`variantAttribute${index}Id`]: new FormControl(null, Validators.required),
    });
    formGroup.get(`variantAttribute${index}`).valueChanges.subscribe(attribute => {
      if (attribute) {
        this.patchRecordsAttribute(targetAttribute, attribute, index);
      }
    });
    const formAttributes = this.form.get('attributes') as FormArray;
    formAttributes.push(formGroup);

    return formGroup;
  }

  patchRecordsAttribute(targetAttribute: string, attribute: IProductAttribute, index: number) {
    const records = _.filter(this.formState.records, record => {
      return record[`attribute${index}Search`] === targetAttribute && !record['isVariant'];
    });
    _.forEach(records, record => {
      (<any>record)[`variantAttribute${index}`] = attribute;
      (<any>record)[`variantAttribute${index}Id`] = attribute.id;
    });
  }

  formValid(): boolean {
    if (!this.form.valid) {
      CommonService.markAsDirty(this.form);
      return false;
    }
    return true;
  }

  process() {
    if (!this.formValid()) {
      throw false;
    }

    this.formState.records.forEach(record => {
      delete record.status;
      delete record.errors;
    });

    return Observable.of(true);
  }
}

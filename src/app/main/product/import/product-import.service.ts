import { Injectable } from '@angular/core';
import * as _ from 'lodash';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';

import { ApiFileRestService } from '../../../core/http/api-file-rest.service';
import { HttpFileService } from '../../../core/http/http-file.service';
import { BasePageComponent } from '../../../shared/base/page/base-page.component';
import { ProductRestService } from '../product-rest.service';
import { MProduct } from '../product.model';
import { ProductService } from '../product.service';
import { MProductImport } from './product-import.model';

@Injectable()
export class ProductImportService {
  constructor(
    private _apiFileRest: ApiFileRestService,
    private _httpFile: HttpFileService,
    private _product: ProductService,
    private _productRest: ProductRestService,
  ) { }

  doImportProducts(products: MProduct[], page: BasePageComponent) {
    page.setLoadingTitle(page._translate.instant('product.import.uploadingProducts'));

    const mappedProducts = _.map(products, (product, index) => {
      this._product.normalizeDoc(product);

      return {
        seqNumber: product._identifier,
        product,
      };
    });
    const chunks = _.chunk(mappedProducts, 100);
    const observables = chunks.map(chunk => this._productRest.imports(chunk));
    return Observable.of(...observables).concatAll();
  }

  doImportProductsWithImages(stateProducts: any, products: MProduct[], productImagesUrl: string[][], page: BasePageComponent): Observable<any[]> {
    const productWithImages = [];
    const uploadImageSeq = 100;

    page.setLoadingTitle(page._translate.instant('product.import.uploadingImages'));

    const chunks = _.chunk(products, 100);
    const observables: any = chunks.map((chunk, chunkIdx) => {
      const uploadImageSession = chunk.map((product, productIdx) => {
        const currentProductIdx = (uploadImageSeq * chunkIdx) + productIdx;
        const productImageUrls = productImagesUrl[currentProductIdx];
        return this.uploadProductImage(product, productImageUrls).do(productWithImage => {
          productWithImages.push(productWithImage);
        }).catch(error => {
          const stateProduct = _.find<MProductImport>(stateProducts, { seqNumber: product._identifier });
          stateProduct.status = 'failed';
          stateProduct.errors = ['Upload image failed'];

          return Observable.throw(error);
        });
      });
      return Observable.onErrorResumeNext(...uploadImageSession);
    });

    return Observable.forkJoin(observables).switchMap(() => this.doImportProducts(productWithImages, page));
  }

  uploadProductImage(product: MProduct, productImageUrls: string[]) {
    if (productImageUrls.length) {
      const multipleUploadSeq = 3;
      const obs = [];

      const imageUrlChunks = _.chunk(productImageUrls, multipleUploadSeq);
      imageUrlChunks.forEach((imageUrlChunk, imageUrlChunkIdx) => {
        const chunkObs = imageUrlChunk.map(
          (imageUrl, imageUrlIdx) => {
            const currentImageIdx = (multipleUploadSeq * imageUrlChunkIdx) + imageUrlIdx;
            return this._httpFile.downloadFileToBase64(imageUrl).switchMap(fileModel => {
              return this._apiFileRest.uploadBase64CompanyFile(
                fileModel.base64,
                fileModel.fileName,
                fileModel.contentType,
              ).do(fileResponse => {
                _.set(product, `pictures[${currentImageIdx}]`, fileResponse);
              });
            });
          }
        );
        obs.push(
          Observable.zip(
            ...chunkObs,
          )
        );
      });

      return Observable.zip(...obs).switchMap(() => {
        if (product.pictures && product.pictures.length) {
          product.mainPictureId = product.pictures[0].id;
        }
        return Observable.of(product);
      });
    } else {
      return Observable.of(product);
    }
  }

  resolveRecords(records: any, related: {
    accounts: IAccount[],
    attributes: IProductAttribute[],
    brands: IProductBrand[],
    categories: IProductCategory[],
    taxes: ITax[],
    uoms: IUOM[],
  }) {
    _.forEach(records, record => {
      this.resolveAttributes(related.attributes, record);
      this.resolveBrand(related.brands, record);
      this.resolveCategory(related.categories, record);
      this.resolveExpenseAccount(related.accounts, record);
      this.resolveIncomeAccount(related.accounts, record);
      this.resolveInventoryAccount(related.accounts, record);
      this.resolvePurchaseTax(related.taxes, record);
      this.resolveSalesTax(related.taxes, record);
      this.resolveUOM1(related.uoms, record);
    });

    return records;
  }

  private resolveAttributes(attributes: IProductAttribute[], record: any) {
    this.resolveAttribute(attributes, record, 1);
    this.resolveAttribute(attributes, record, 2);
    this.resolveAttribute(attributes, record, 3);
  }

  private resolveAttribute(attributes: IProductAttribute[], record: any, index: number) {
    if (record[`attribute${index}Search`]) {
      const attribute = _.find(attributes, (o) => {
        const attributeRegEx = new RegExp(record[`attribute${index}Search`], 'i');
        return attributeRegEx.test(o.name);
      });

      if (attribute) {
        record[`variantAttribute${index}`] = attribute;
        record[`variantAttribute${index}Id`] = attribute.id;
      }
    }
  }

  private resolveBrand(brands: IProductBrand[], record: any) {
    if (record.brandSearch) {
      const brand = _.find(brands, (o) => {
        const brandRegEx = new RegExp(record.brandSearch, 'i');
        return brandRegEx.test(o.name);
      });

      if (brand) {
        record.brand = brand;
        record.brandId = brand.id;
      }
    }
  }

  private resolveCategory(categories: IProductCategory[], record: any) {
    if (record.categorySearch) {
      const category = _.find(categories, (o) => {
        const categoryRegEx = new RegExp(record.categorySearch, 'i');
        return categoryRegEx.test(o.name);
      });

      if (category) {
        record.category = category;
        record.categoryId = category.id;
      }
    }
  }

  private resolvePurchaseTax(taxes: ITax[], record: any) {
    if (record.purchaseTaxCode) {
      const tax = _.find(taxes, (o) => {
        const taxRegEx = new RegExp(record.purchaseTaxCode, 'i');
        return taxRegEx.test(o.code);
      });

      if (tax) {
        record.purchaseTax = tax;
        record.purchaseTaxId = tax.id;
      }
    }
  }

  private resolveSalesTax(taxes: ITax[], record: any) {
    if (record.salesTaxCode) {
      const tax = _.find(taxes, (o) => {
        const taxRegEx = new RegExp(record.salesTaxCode, 'i');
        return taxRegEx.test(o.code);
      });

      if (tax) {
        record.salesTax = tax;
        record.salesTaxId = tax.id;
      }
    }
  }

  private resolveUOM1(uoms: IUOM[], record: any) {
    if (record.uomSearch) {
      const uom = _.find(uoms, (o) => {
        const uomRegEx = new RegExp(record.uomSearch, 'i');
        return uomRegEx.test(o.name);
      });

      if (uom) {
        record.uom = uom;
        record.uomId = uom.id;
      }
    }
  }

  private resolveIncomeAccount(accounts: IAccount[], record: any) {
    if (record.incomeAccountSearch) {
      const account = _.find(accounts, (a) => {
        const accountRegEx = new RegExp(record.incomeAccountSearch, 'i');
        return accountRegEx.test(a.code);
      });

      if (account) {
        record.incomeAccount = account;
        record.incomeAccountId = account.id;
      }
    }
  }

  private resolveExpenseAccount(accounts: IAccount[], record: any) {
    if (record.expenseAccountSearch) {
      const account = _.find(accounts, (a) => {
        const accountRegEx = new RegExp(record.expenseAccountSearch, 'i');
        return accountRegEx.test(a.code);
      });

      if (account) {
        record.expenseAccount = account;
        record.expenseAccountId = account.id;
      }
    }
  }

  private resolveInventoryAccount(accounts: IAccount[], record: any) {
    if (record.inventoryAccountSearch) {
      const account = _.find(accounts, (a) => {
        const accountRegEx = new RegExp(record.inventoryAccountSearch, 'i');
        return accountRegEx.test(a.code);
      });

      if (account) {
        record.inventoryAccount = account;
        record.inventoryAccountId = account.id;
      }
    }
  }
}

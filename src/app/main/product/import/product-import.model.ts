import { MProduct } from '../product.model';

export class MProductImport extends MProduct {
  seqNumber: number;
  categorySearch: string;
  brandSearch: string;
  uomSearch: string;
  incomeAccountSearch: string;
  expenseAccountSearch: string;
  inventoryAccountSearch: string;
  isVariant: boolean;
  attribute1Search: string;
  attribute2Search: string;
  attribute3Search: string;
  status: any;
  errors: any;
}

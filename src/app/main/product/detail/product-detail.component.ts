import * as _ from 'lodash';
import swal from 'sweetalert2';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';

import { AccountingService, SystemMessageService } from '../../../core/core.module';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';
import { ProductRestService } from '../product-rest.service';
import { TranslateService } from '@ngx-translate/core';

import { MProduct } from '../product.model';

@Component({
  selector: 'app-product-detail',
  templateUrl: 'product-detail.component.html',
  styleUrls: ['product-detail.component.scss'],
  providers: [SystemMessageService],
})
export class ProductDetailComponent implements OnInit {
  @ViewChild('productVariantModal') elProductVariantModal: ModalDirective;

  _ = _;
  doc: MProduct;
  modalDoc: MProduct;
  productVariant: MProduct;
  routeParams: any;
  showShareDialog: boolean = false;
  productVariantState: string = 'create';

  constructor(
    private _accounting: AccountingService,
    private _productRest: ProductRestService,
    private _route: ActivatedRoute,
    public _systemMessage: SystemMessageService,
    private _translate: TranslateService,
  ) {
    this._route.params.subscribe(params => this.routeParams = params);
  }

  ngOnInit() {
    this.loadData();
  }

  loadData() {
    this._productRest.load(this.routeParams.id, {
      includeTags: true,
      includeVariants: true,
      includeVariantPrices: true,
      includePictures: true,
      includeSalesChannels: true,
    }).subscribe(product => {
      this.doc = product;
    });
  }

  showVariantModal(variant, state) {
    this.productVariant = variant;
    this.modalDoc = this.doc;
    this.productVariantState = state;
    this.elProductVariantModal.show();
  }

  closeVariantModal() {
    this.productVariant = null;
    this.modalDoc = null;
    this.elProductVariantModal.hide();
  }

  showDeleteDialog(productVariantId: string) {
    swal({
      title: this._translate.instant('ui.productVariant.confirm.delete.label'),
      text: this._translate.instant('ui.productVariant.confirm.delete.description'),
      type: 'question',
      showCancelButton: true,
    })
      .then(() => {
        this._productRest.deleteVariant(this.doc.id, productVariantId)
        .catch((error) => {
          this._systemMessage.log({
            message: error.response.data[0].errorMessage,
            showAs: 'growl',
            showSnackBar: false,
            type: 'error'
          });
          return Observable.throw(error);
        })
        .subscribe(response => {
          this.loadData();
          this._systemMessage.log({
            message: this._translate.instant('ui.productVariant.success.delete'),
            showAs: 'growl',
            showSnackBar: false,
            type: 'success'
          });
        });
      })
      .catch(() => { });
  }

}

declare interface IProduct {
  id: string;
  sku: string;
  variants: IProductVariant[];
  name: string;
  obQty: number;
  obDate: string | Date;
  detailDescription: string;
  categoryId: string;
  sellable: boolean;
  salesTaxId: string;
  salesDescription: string;
  purchasable: boolean;
  purchaseTaxId: string;
  purchaseDescription: string;
  variantCount: number;
  variantAttribute1: any;
  variantAttribute1Id: string;
  variantAttribute2: any;
  variantAttribute2Id?: string;
  variantAttribute3: any;
  variantAttribute3Id?: string;
  pictureId: string;
  picture: {
    id: string;
    fileType: string;
    fileName: string;
    fileExt: string;
    fileSize: boolean;
    description: string;
    uploadByUserId: string;
    fileUrl: string;
    thumbnailUrl: string;
  };
  pictures: any;
  productType: string;

  uom: IUOM;
  uomId: string;
  uom2: IUOM;
  uom2Id: string;
  uom3: IUOM;
  uom3Id: string;

  hasVariants: boolean;

  salesChannels?: any;
}

declare interface IProductVariant {
  id: string;
  tempId?: any;
  sku: string;
  obQty: number;
  obDate: string | Date;
  attribute1Value: string;
  attribute2Value?: string;
  attribute3Value?: string;
  prices: IProductVariantPrice[];
  unitCost?: number;
  isMaster?: boolean;
  salesChannels?: any;
}

declare interface IProductVariantPrice {
  priceLevelId: string;
  unitPrice: number;
  unitPrice2: number;
  unitPrice3: number;
}

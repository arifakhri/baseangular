import { Input, QueryList, ViewChildren } from '@angular/core';
import { FormGroup } from '@angular/forms';
import * as _ from 'lodash';

import { MSalesChannelAccount } from '../sales-channel/sales-channel.model';
import { ProductFormFieldsetChannelComponent } from './form/fieldset-channel/product-form-fieldset-channel.component';
import { MProductRelatedData } from './product.model';

export class ProductFormBComponent {
  @Input() doc: any;
  @Input() existingPictures: any;
  @Input() form: FormGroup;
  @Input() salesChannelAccounts: MSalesChannelAccount[] = [];
  @Input() salesChannelProducts: any = [];
  @Input() selectedUOMS: IUOM[] = [];
  @Input() formType: string;
  @Input() relatedData: MProductRelatedData;
  @Input() relatedDataReady: boolean = false;

  @ViewChildren('formChannel') elFormChannel: QueryList<ProductFormFieldsetChannelComponent>;

  onImageUploaded(image) {
    if (!image.id) {
      return;
    }
    const picturesControl = this.form.get('pictures');

    const pictures = picturesControl.value;
    pictures.push({
      ...image,
      isMain: pictures.length === 0,
    });

    picturesControl.setValue(pictures);
  }

  onImageDeleted(image) {
    const picturesControl = this.form.get('pictures');
    const pictures = picturesControl.value;
    _.remove(pictures, { id: image.id });

    if (pictures.length === 1) {
      pictures[0].isMain = true;
    }

    picturesControl.setValue(pictures);
  }

  onImagePrimaryChange(image) {
    const picturesControl = this.form.get('pictures');
    const pictures = picturesControl.value;

    const prevPrimary: any = _.find(pictures, { isMain: true });
    prevPrimary.isMain = false;

    const nextPrimary: any = _.find(pictures, { id: image.id }) as any;
    nextPrimary.isMain = true;

    picturesControl.setValue(pictures);
  }
}

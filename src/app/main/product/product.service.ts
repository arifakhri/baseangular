import { Injectable } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import * as _ from 'lodash';
import { moment } from 'ngx-bootstrap/chronos/test/chain';

import { CommonService } from '../../core/core.module';
import { MProductVariant, MProductVariantSortIdentifier } from './product.model';

@Injectable()
export class ProductService {
  constructor(
    private _formBuilder: FormBuilder,
    private _translate: TranslateService
  ) { }

  normalizeDoc(formValue: any) {
    formValue.tags = _.map(formValue.tags, tag => {
      return _.isObject(tag) && _.has(tag, 'id') ? tag : { name: tag };
    });

    _.forEach(formValue.variants, variant => {
      _.forEach(variant.openingBalances, openingBalance => {
        if (openingBalance.date) {
          openingBalance.date = moment(openingBalance.date).format('YYYY-MM-DD');
        }
      });
    });

    return formValue;
  }

  markVariantsSort(productVariants: MProductVariant[]): MProductVariantSortIdentifier[] {
    const uniqueIds = CommonService.getMultipleUniqueId(productVariants.length);
    _.forEach(productVariants, (productVariant, productVariantIdx) => {
      productVariant.tempId = uniqueIds[productVariantIdx];
    });

    return _.map(uniqueIds, (uniqueId, uniqueIdx) => ({
      uniqueId,
      index: uniqueIdx,
    }));
  }

  restoreVariantsSort(productVariants: MProductVariant[], sorts: MProductVariantSortIdentifier[]) {
    const copyProductVariants = productVariants.slice();
    const sortedProductVariants: MProductVariant[] = [];

    sorts = _.sortBy(sorts, 'index');
    _.forEach(sorts, sort => {
      const sortProductVariantIdx = _.findIndex(sortedProductVariants, { tempId: sort.uniqueId });
      if (sortProductVariantIdx > -1) {
        sortedProductVariants[sort.index] = copyProductVariants[sortProductVariantIdx];
        copyProductVariants.splice(sortProductVariantIdx, 1);
      }
    });

    _.forEach(copyProductVariants, copyProductVariant => {
      sortedProductVariants.push(copyProductVariant);
    });

    return sortedProductVariants;
  }
}

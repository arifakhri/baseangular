import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ProductCreateComponent } from './create/product-create.component';
import { ProductDetailComponent } from './detail/product-detail.component';
import { ProductImportChannelComponent } from './import-channel/product-import-channel.component';
import { ProductImportTableComponent } from './import-table/product-import-table.component';
import { ProductListComponent } from './list/product-list.component';
import { ProductUpdateComponent } from './update/product-update.component';

import { AuthorizationService } from '../../core/core.module';

export const routes: Routes = [{
  path: '',
  children: [{
    path: '',
    redirectTo: 'all',
    pathMatch: 'full',
  }, {
    path: 'import-table',
    component: ProductImportTableComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'product.import'
    }
  }, {
    path: 'all',
    component: ProductListComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'product.list.all'
    }
  }, {
    path: 'inventory',
    component: ProductListComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'product.list.inventory'
    }
  }, {
    path: 'non-inventory',
    component: ProductListComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'product.list.nonInventory'
    }
  }, {
    path: 'service',
    component: ProductListComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'product.list.service'
    }
  }, {
    path: 'create-inventory',
    component: ProductCreateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'product.create.inventory'
    }
  }, {
    path: 'create-non-inventory',
    component: ProductCreateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'product.create.nonInventory'
    }
  }, {
    path: 'create-service',
    component: ProductCreateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'product.create.service'
    }
  }, {
    path: ':id',
    component: ProductDetailComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'product.detail'
    }
  }, {
    path: ':id/update',
    component: ProductUpdateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'product.update'
    }
  }, {
    path: 'import-channel/:channelAccountId',
    component: ProductImportChannelComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'product.importChannel'
    }
  }]
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule
  ]
})
export class ProductRoutingModule { }

import * as _ from 'lodash';
import * as moment from 'moment';
import { ActivatedRoute } from '@angular/router';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { DataTable } from 'primeng/components/datatable/datatable';
import { Observable, Subscription } from 'rxjs';
import 'app/rxjs-imports.ts';
import { TranslateService } from '@ngx-translate/core';

import { RetryDialogComponent } from '../../../shared/retry-dialog/retry-dialog.component';

import {
  AccountingService,
  ExportDataTableService,
  GridTableDataSource,
  GridTableService,
  GridTableToggleService,
} from '../../../core/core.module';
import { ProductRestService } from '../product-rest.service';
import { TransactionRoutingService } from '../../transaction/transaction-routing.service';

import { MProduct } from '../product.model';

@Component({
  selector: 'app-product-transaction',
  templateUrl: 'product-transaction.component.html'
})
export class ProductTransactionComponent implements OnInit {
  @Input() doc: MProduct;
  @ViewChild('retryDialog') elRetryDialog: RetryDialogComponent;

  filters: any = {};

  gridDataSource: GridTableDataSource<IProductTransaction> = new GridTableDataSource<IProductTransaction>();
  selectedRecords: IProductTransaction[] = [];

  retryDialogShownState: boolean = false;
  retryDialogOnRetry: Function;

  tableColumns: IGridTableColumn[] = [{
    i18nLabel: 'ui.product.transaction.column.transactionDate',
    field: 'transactionDate',
    dateFormat: 'DD/MM/YYYY',
    sort: true,
  }, {
    i18nLabel: 'ui.product.transaction.column.transactionNumber',
    field: 'transactionNumber',
    link: (row: IProductTransaction) => {
      const transactionType = row.transactionType.substr(0, row.transactionType.indexOf('_'));
      let routesType = _.kebabCase(row.transactionType.substr(row.transactionType.indexOf('_') + 1));
      switch (transactionType) {
        case 'purchase':
          if (routesType === 'receipt') {
            routesType = 'expense';
          }
          return `/purchases/${routesType}s/${row.transactionId}`;
        case 'sales':
          return `/sales/${routesType}s/${row.transactionId}`;
        case 'inventory':
          return `/inventory-adjustments/${row.transactionId}`;
      }
    },
    sort: true,
  }, {
    i18nLabel: 'ui.product.transaction.column.transactionType',
    field: 'transactionType',
    sort: true,
    formatter: (value, row: IProductTransaction) => {
      return _.startCase((row.transactionType));
    },
  }, {
    i18nLabel: 'ui.product.transaction.column.contact',
    field: 'contact.displayName',
    sort: true,
  }, {
    i18nLabel: 'ui.product.transaction.column.productVariantName',
    field: 'productVariantName',
    sort: true,
  }, {
    i18nLabel: 'ui.product.transaction.column.qty',
    field: 'qty',
    sort: true,
  }, {
    i18nLabel: 'ui.product.transaction.column.unitPrice',
    field: 'unitPrice',
    formatter: (value, row: IProductTransaction) => {
      return this._accounting.ac.formatMoney(value, '');
    },
    columnClasses: 'right-align',
    sort: true,
  }, {
    i18nLabel: 'ui.product.transaction.column.unitCost',
    field: 'unitCost',
    formatter: (value, row: IProductTransaction) => {
      return this._accounting.ac.formatMoney(value, '');
    },
    columnClasses: 'right-align',
    sort: true,
  }];
  tableColumnsShow: boolean = false;
  tableColumnsToggle = this._gridTableToggle.mapToggleOptionsFromColumns(this.tableColumns);

  private loadDataTimeoutId: number;
  private gridFirstLoad: boolean = true;
  private subscriptions: { [key: string]: Subscription[] } = {
    load: []
  };

  @ViewChild('gridTable') public gridTable: DataTable;

  constructor(
    public _accounting: AccountingService,
    private _activatedRoute: ActivatedRoute,
    private _dataTableExport: ExportDataTableService,
    private _gridTable: GridTableService,
    private _gridTableToggle: GridTableToggleService,
    private _productRest: ProductRestService,
    private _translate: TranslateService,
    private _transactionRoutingService: TransactionRoutingService
  ) {
    this.gridDataSource.pager.itemsPerPage = 10;
  }

  ngOnInit() {
    this.loadData();
  }

  gridLoadDataWrapper() {
    // Grid always load at the first time. we dont want that. we already call on ngOnInit
    if (this.gridFirstLoad) {
      this.gridFirstLoad = false;
      return;
    }

    if (this.loadDataTimeoutId) {
      window.clearTimeout(this.loadDataTimeoutId);
      this.loadDataTimeoutId = null;
    }

    this.loadDataTimeoutId = window.setTimeout(() => {
      this.loadData();
    }, 300);
  }

  get exportRecords(): Observable<IProductTransaction[]> {
    const { qParams } = this.buildApiOptions({
      take: this._dataTableExport.exportLimit,
      skip: 0,
    });

    return this._productRest.findAllTransactions(this.doc.id, qParams).map(response => response.data);
  }

  exportPDF() {
    this.exportRecords.subscribe(records => {
      this._dataTableExport.export({
        records,
        mapOptions: this._dataTableExport.mapFromTableColumns(this.tableColumns),
        fileName: 'all-product-transactions',
        extension: 'pdf',
        templateData: {
          title: this._translate.instant('ui.product.transaction.list.title'),
        },
      });
    });
  }

  exportExcel() {
    this.exportRecords.subscribe(records => {
      this._dataTableExport.export({
        records: records,
        mapOptions: this._dataTableExport.mapFromTableColumns(this.tableColumns),
        fileName: 'all-product-transactions',
        extension: 'xls',
      });
    });
  }

  loadData() {
    const { qParams } = this.buildApiOptions({
      take: this.gridDataSource.pager.itemsPerPage,
      skip: (this.gridDataSource.pager.pageNum - 1) * this.gridDataSource.pager.itemsPerPage,
      includeTotalCount: true,
    });

    this.subscriptions.load.forEach(sub => sub.unsubscribe());
    this.subscriptions.load.push(
    this.elRetryDialog.createRetryEntry(this._productRest.findAllTransactions(this.doc.id, qParams))
      .subscribe(response => {
        this.gridDataSource.updateFromApiPaginationResult(response);
      })
    );
  }

  private buildApiOptions(defaultOptions: any = {}): {
    qBody: any,
    qParams: any,
  } {
    const populatedFilters: any = {};

    if (this.filters.lowDate) {
      populatedFilters.lowDate = moment(this.filters.lowDate).format('YYYY-MM-DD');
    }
    if (this.filters.highDate) {
      populatedFilters.highDate = moment(this.filters.highDate).format('YYYY-MM-DD');
    }

    return {
      qBody: {},
      qParams: Object.assign({}, defaultOptions, populatedFilters)
    };
  }

  public onGridChange() {
    this.gridLoadDataWrapper();
  }

  public onGridPageChange() {
    this.gridLoadDataWrapper();
  }

  getUpdateLink(doc: any) {
    return this._transactionRoutingService.resolve(doc, 'update');
  }

}

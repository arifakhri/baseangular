declare interface IProductTransaction {
  transactionId: string;
  transactionType: string;
  transactionNumber: string;
  transactionDate: string;
  contactId: string;
  productId: string;
  productVariantId: string;
  productVariantSKU: string;
  productVariantName: string;
  warehouseId: string;
  qty: number;
  unitPrice: number;
  total: number;
  unitCost: number;
  totalCost: number;
  contact: {
    id: string;
    contactType: string;
    code: string;
    displayName: string;
    company: string;
    fullName: string;
    phone: string;
    mobile: string;
    fax: string;
    email: string;
    website: string;
    inactive: boolean;
    postalAddress: {
      attention: string;
      street1: string;
      street2: string;
      city: string;
      stateProvince: string;
      countryId: string;
      postalCode: string;
      longitude: number;
      latitude: number;
      country: {
        code: string;
        name: string;
      }
    }
  };
  warehouse: {
    id: string;
    name: string;
    isMaster: boolean;
  }
}
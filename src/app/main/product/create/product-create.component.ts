import { Component, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import * as _ from 'lodash';
import { ModalDirective } from 'ngx-bootstrap';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';
import swal from 'sweetalert2';

import { CommonService, UserVariableService } from '../../../core/core.module';
import { BaseCreateBComponent } from '../../../shared/base/base.module';
import { SalesChannelMarketplaceRestService } from '../../sales-channel/sales-channel-marketplace-rest.service';
import { SalesChannelWebstoreRestService } from '../../sales-channel/sales-channel-webstore-rest.service';
import { SettingsApplicatorService } from '../../settings/settings-applicator.service';
import { MSalesChannelProductSyncResult } from '../form-channel/product-form-channel.model';
import { ProductFormChannelService } from '../form-channel/product-form-channel.service';
import { ProductFormBaseComponent } from '../form/base/product-form-base.component';
import { ProductFormService } from '../product-form.service';
import { ProductRestService } from '../product-rest.service';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-product-create',
  templateUrl: 'product-create.component.html',
})
export class ProductCreateComponent extends BaseCreateBComponent {
  @ViewChild('submitProductChannelsModal') elSubmitProductChannelsModal: ModalDirective;
  @ViewChildren('productFormBase') elProductFormBase: QueryList<ProductFormBaseComponent>;

  fullView: boolean = false;
  fullViewState: IAppVariable = this._userVariable.get('simpledetailstate.product', {
    value: false,
  });

  formNavigations: IFormNav[] = [];
  productStateType: string;

  observableSubmitChannelProducts: Observable<MSalesChannelProductSyncResult>;

  get salesChannelSelectedAccounts() {
    if (this.elProductFormBase.first && this.elProductFormBase.first.elFormChannel.first) {
      return this.elProductFormBase.first.elFormChannel.first.selectedAccounts.map(selectedAccount => selectedAccount.account);
    }
    return [];
  }

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _userVariable: UserVariableService,
    private _product: ProductService,
    private _productForm: ProductFormService,
    private _productFormChannel: ProductFormChannelService,
    private _productRest: ProductRestService,
    private _router: Router,
    private _salesChannelMarketplaceRest: SalesChannelMarketplaceRestService,
    private _salesChannelWebstoreRest: SalesChannelWebstoreRestService,
    private _settingsApplicator: SettingsApplicatorService,
  ) {
    super();

    this.componentId = 'ProductCreate';
    this.headerTitle = 'ui.product.create.title';
    this.routeURL = '/products';
    this.entrySuccessI18n = 'success.product.create';

    this.registerHook('buildForm', event => {
      this._productForm.setFormDefinitions(this.form, '');

      this._activatedRoute.data.subscribe(data => {
        this.productStateType = _.snakeCase(data.name.substr(data.name.lastIndexOf('.') + 1));

        this.saveAndNewURL = `${this.routeURL}/${'create-' + _.kebabCase(data.name.substr(data.name.lastIndexOf('.') + 1))}`;

        this.form.controls.productType.setValue(this.productStateType);

        this._productForm.getFormNavigations(this.productStateType).subscribe(formNavigations => this.formNavigations = formNavigations);
      });

      this.fullView = this.fullViewState.value;
    });

    this.registerHook('submitPreprocess', event => {
      return this._productRest.checkDuplicate({ name: this.form.value.name }).switchMap(exist => {
        if (exist) {
          return swal({
            title: this.page._translate.instant('confirm.product.exist.label'),
            text: this.page._translate.instant('confirm.product.exist.description'),
            type: 'warning',
            showCancelButton: true
          });
        } else {
          return Observable.of(exist);
        }
      });
    });

    this.registerHook('save', event => {
      let product: any = {};

      product = _.assign(product, this.form.value);
      this._product.normalizeDoc(product);

      const masterWarehouseId = this.elProductFormBase.first.relatedData.settings.productSetting.masterWarehouseId;

      if (product.hasVariants && product.variants && product.variants.length && product.variants.length > 0) {
        product.variants.forEach(variant => {
          if (variant && variant.obQty && variant.obQty > 0 && variant.obDate) {
            variant.openingBalances = [{
              date: variant.obDate,
              qty: variant.obQty,
              warehouseId: masterWarehouseId
            }];
          }
        });
      } else {
        const masterVariant = product.variants[0];
        if (product && product.obQty && product.obQty > 0 && product.obDate) {
          masterVariant.openingBalances = [{
            date: product.obDate,
            qty: product.obQty,
            warehouseId: masterWarehouseId
          }];
        }
      }

      if (this.salesChannelSelectedAccounts.length) {
        const channelProductsMetadata = this.elProductFormBase.first.elFormChannel.first.getChannelProductsMetadata(product);
        channelProductsMetadata.forEach(channelProductMetadata => {
          this._productFormChannel.attachToProduct(product, channelProductMetadata);
        });
      }

      return this._productRest.create(product, { includeSalesChannels: true, includeVariants: true })
        .do(result => {
          this.submittedDoc = result;

          if (result.salesChannels.length) {
            this.observableSubmitChannelProducts = Observable.merge(
              this._productFormChannel.syncMultipleProducts(
                result.salesChannels.map(productSalesChannel => ({
                  channelAccountId: productSalesChannel.channelAccountId,
                  channelAccountType: productSalesChannel.channelAccountType,
                  clodeoProductId: result.id,
                }))
              )
            );

            this.saveAndRedirectEnabled = false;

            this.elSubmitProductChannelsModal.show();
          } else {
            this.saveAndRedirectEnabled = true;
          }
        });
    });
  }

  onFullViewStateChange() {
    this.fullViewState.value = this.fullView;
    this._userVariable.update();
  }

  formValid(): boolean {
    if (
      (this.elProductFormBase.first.elFormChannel.first && !this.elProductFormBase.first.elFormChannel.first.formValid) ||
      !this.form.valid ||
      (this.form.value.hasVariants && !this.form.value.variants.length)
    ) {
      CommonService.markAsDirty(this.form);

      const errorMessages = [
        this.page._translate.instant('error.form'),
      ];

      if (this.form.value.hasVariants && !this.form.value.variants.length) {
        errorMessages.push(this.page._translate.instant('error.product.variants.none'));
      }

      this.page._systemMessage.log({
        message: errorMessages,
        type: 'error'
      });
      return false;
    }
    return true;
  }

  onProductChannelsSubmitted() {
    this.page._globalSystemMessage.log({
      message: 'success.product.productChannelsCreate',
      type: 'success',
      showAs: 'growl',
      showSnackBar: false,
    });

    this.redirectAfterSave(this.submittedDoc.id);
  }

  onSalesChannelProgressClose() {
    this.redirectAfterSave(this.submittedDoc.id);
  }
}

import * as _ from 'lodash';
import { Component, Input, OnInit } from '@angular/core';

import { ProductRestService } from '../product-rest.service';

import { MProduct } from '../product.model';

@Component({
    selector: 'app-product-detail-warehouse',
    templateUrl: 'product-detail-warehouse.component.html'
})
export class ProductDetailWarehouseComponent implements OnInit {
    @Input() doc: MProduct;

    _ = _;
    warehouseInventory: { [key: string]: MProduct[] };

    constructor(
        private _productRest: ProductRestService,
    ) {  }

    ngOnInit() {
        this.loadData();
    }

    loadData() {
        this._productRest.findWarehouseInventory(this.doc.id).subscribe(result => {
            this.warehouseInventory = _.groupBy(result, 'warehouseId');
        });
    }

}

import { Component, Input } from '@angular/core';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';

import { EImportDataTypes, ImportDataService } from '../../../../core/core.module';
import { ProductImportTableService } from '../product-import-table.service';

@Component({
  selector: 'app-product-import-table-browse',
  templateUrl: './product-import-table-browse.component.html'
})
export class ProductImportTableBrowseComponent {
  @Input() formState: any = {};

  constructor(
    private _importData: ImportDataService,
    private _productImportTable: ProductImportTableService,
  ) { }

  process() {
    this.formState.importData = this._importData.prepare({
      mapOptions: this._productImportTable.importMap,
      from: EImportDataTypes.EXCEL,
      fileInputSelector: '#import-product',
    });
    return Observable.fromPromise(this.formState.importData.header()).do(header => {
      this.formState.header = header;

      this.formState.importData.mapByHeader(this.formState.header);
    });
  }
}

import { Component } from '@angular/core';
import { FormArray, FormControl, FormGroup } from '@angular/forms';
import * as _ from 'lodash';

import { CommonService, SystemMessageService } from '../../../../core/core.module';
import { ProductImportTableService } from '../../import-table/product-import-table.service';
import { ProductImportEditorBComponent } from '../../import/product-import-editor.bcomponent';

@Component({
  selector: 'app-product-import-table-editor',
  templateUrl: 'product-import-table-editor.component.html',
  providers: [SystemMessageService],
})
export class ProductImportTableEditorComponent extends ProductImportEditorBComponent {
  constructor(
    private _productImportTable: ProductImportTableService,
    public _systemMessage: SystemMessageService,
  ) {
    super();

    this.componentId = 'ProductImportTableEditor';
  }

  save() {
    const { products } = this.form.value;

    const normalizedProducts = this._productImportTable.normalizeDoc(products, this.filteredRecords);

    const loading = this.page.createLoading();

    return this._productImport.doImportProducts(normalizedProducts, this.page).do(results => {
      this.lastResults = results;

      const successResults = _.filter(results, { success: true });
      if (successResults.length === products.length) {
        this._globalSystemMessage.log({
          message: this.page._translate.instant('success.product.importTable'),
          type: 'success',
          showAs: 'growl',
          showSnackBar: false,
        });

        this._router.navigateByUrl('/products');
      } else {
        this.onSaveFailed();
      }
    }, null, () => {
      loading.dispose();
    });
  }

  buildFormFromRecords(records: any) {
    this._ngZone.run(() => {
      const formArray = <FormArray>this.form.get('products');
      while (formArray.length) {
        formArray.removeAt(0);
      }

      _.forEach(records, (record, recordIdx) => {
        this.selectedUOMS[recordIdx] = [];

        if (record.uom) {
          this.selectedUOMS[recordIdx][0] = record.uom;
        }

        const newFormGroup = new FormGroup({});
        this._productForm.setFormDefinitions(newFormGroup, record.productType);

        if (record.isVariant) {
          const excludedControls = ['variants.0.sku', 'variants.0.prices.0.unitPrice', 'variants.0.unitCost'];
          CommonService.iterateFormControls(newFormGroup, (control, controlName) => {
            if (!excludedControls.includes(controlName)) {
              (<FormControl>control).clearValidators();
            }
          });
        }

        const priceLevel = _.find(this.relatedData.priceLevels, { isMaster: true });
        _.set(record, 'variants[0].prices[0].priceLevelId', priceLevel.id);

        this._productForm.applySettings(this.form, this.relatedData).subscribe(() => {
          const defaultUOM = this.form.get('uom').value;
          if (defaultUOM) {
            this.selectedUOMS[0] = defaultUOM;
          }
        });

        newFormGroup.patchValue({
          ...record,
          _identifier: record.seqNumber,
        });

        formArray.push(newFormGroup);
      });
    });
  }
}

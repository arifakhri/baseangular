import { BehaviorSubject, Observable } from 'rxjs';
import 'app/rxjs-imports.ts';
import { Component, OnDestroy, ViewChild } from '@angular/core';
import { Store } from '@ngrx/store';

import { ProductImportTableBrowseComponent } from './browse/product-import-table-browse.component';
import { ProductImportTableEditorComponent } from './editor/product-import-table-editor.component';
import { ProductImportMapRelatedComponent } from '../import/map-related/product-import-map-related.component';
import { ProductImportTableMapFieldsComponent } from './map-fields/product-import-table-map-fields.component';

import { ProductRestService } from '../product-rest.service';

import * as ProductImportStateStore from '../import/product-import-state.store';
import * as ProductImportStepStore from '../import/product-import-step.store';

@Component({
  selector: 'app-product-import-table',
  templateUrl: 'product-import-table.component.html',
})
export class ProductImportTableComponent implements OnDestroy {
  @ViewChild('productImportTableBrowse') elProductImportTableBrowse: ProductImportTableBrowseComponent;
  @ViewChild('productImportTableMapFields') elProductImportTableMapFields: ProductImportTableMapFieldsComponent;
  @ViewChild('productImportMapRelated') elProductImportMapRelated: ProductImportMapRelatedComponent;
  @ViewChild('productImportTableEditor') elProductImportTableEditor: ProductImportTableEditorComponent;

  compReady$: BehaviorSubject<boolean> = new BehaviorSubject(false);

  accounts: IAccount[] = [];
  attributes: IProductAttribute[] = [];
  brands: IProductBrand[] = [];
  categories: IProductCategory[] = [];
  taxes: ITax[] = [];
  uoms: IUOM[] = [];
  priceLevels: IPriceLevel[] = [];

  maxStep = 4;

  formStep$: Observable<number>;
  formState$: Observable<any>;

  formStep: number;
  formState: any;

  constructor(
    private _productRest: ProductRestService,
    private _store: Store<any>,
  ) {
    this.formStep$ = _store.select(ProductImportStepStore.STORE_ID) as any;
    this.formState$ = _store.select(ProductImportStateStore.STORE_ID) as any;

    this.formStep$.subscribe(formStep => this.formStep = formStep);
    this.formState$.subscribe(formState => {
      return this.formState = formState;
    });

    this.loadRelatedData().subscribe();
  }

  ngOnDestroy() {
    this._store.dispatch({ type: ProductImportStepStore.ACTIONS.RESET });
    this._store.dispatch({ type: ProductImportStateStore.ACTIONS.RESET });
  }

  loadRelatedData() {
    return this._productRest.loadRelatedData().do(related => {
      this.accounts = related.accounts;
      this.attributes = related.attributes;
      this.brands = related.productBrands;
      this.categories = related.categories;
      this.taxes = related.taxes;
      this.uoms = related.uoms;
      this.priceLevels = related.priceLevels;

      this.compReady$.next(true);
    });
  }

  formPrev() {
    this.formPrevStep();

    if (this.formState.prevSkipped) {
      this.formState.prevSkipped = false;
      this.formPrevStep();
    }
  }

  formPrevStep() {
    this._store.dispatch({ type: ProductImportStepStore.ACTIONS.PREV });
  }

  formNext(skipProcess: boolean = false) {
    if (skipProcess) {
      this.formState.prevSkipped = true;
      this.formNextStep();
      return;
    }

    let obs: Observable<any>;

    switch (this.formStep) {
      case 1:
        obs = this.elProductImportTableBrowse.process();
        break;
      case 2:
        obs = this.elProductImportTableMapFields.process();
        break;
      case 3:
        obs = this.elProductImportMapRelated.process();
        break;
      case 4:
        obs = this.elProductImportTableEditor.process();
        break;
    }

    obs.subscribe(null, null, () => {
      if (this.formStep !== this.maxStep) {
        this.formNextStep();
      }
    });
  }

  formNextStep() {
    this._store.dispatch({ type: ProductImportStepStore.ACTIONS.NEXT });
  }
}

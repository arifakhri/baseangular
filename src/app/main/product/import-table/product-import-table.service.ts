import * as _ from 'lodash';
import { Injectable } from '@angular/core';

@Injectable()
export class ProductImportTableService {
  importMap: IImportDataMap = {
    'variants[0].sku': {
      label: 'ui.product.import.fieldSku',
      criteria: [
        /SKU/,
      ],
    },
    productType: {
      label: 'ui.product.import.fieldProductType',
      criteria: [
        /Product Type/,
        /Item Type/,
        /Type/,
      ],
      processorProvider: {
        allowedValues: ['inventory', 'non_inventory', 'service'],
        defaultValue: 'service',
      },
    },
    name: {
      label: 'ui.product.import.fieldName',
      criteria: [
        /Product Name/,
        /Item Name/,
      ],
    },
    categorySearch: {
      label: 'ui.product.import.fieldCategory',
      criteria: [
        /Category/,
      ],
    },
    brandSearch: {
      label: 'ui.product.import.fieldBrand',
      criteria: [
        /Brand/,
      ],
    },
    uomSearch: {
      label: 'ui.product.import.fieldUnit',
      criteria: [
        /UOM/,
        /^Unit$/,
      ],
    },
    sellable: {
      label: 'ui.product.import.fieldSellable',
      criteria: [
        /Sellable/,
        /^Sell$/,
      ],
      processorProvider: {
        convertTo: 'boolean',
      },
    },
    'variants[0].prices[0].unitPrice': {
      label: 'ui.product.import.fieldSalesPrice',
      criteria: [
        /Selling Price/,
        /^Price$/,
        /^Unit Price$/,
      ],
      processorProvider: {
        convertTo: 'number',
      },
    },
    salesDescription: {
      label: 'ui.product.import.fieldSalesDescription',
      criteria: [
        /Sales Description/,
      ],
    },
    incomeAccountSearch: {
      label: 'ui.product.import.fieldIncomeAccount',
      criteria: [
        /Income Account/,
        /Income Account Code/,
        /Income Acc Code/,
      ],
    },
    salesTaxCode: {
      label: 'ui.product.import.fieldSalesTax',
      criteria: [
        /Sales Tax/,
        /Sales Tax Code/,
      ],
    },
    purchasable: {
      label: 'ui.product.import.fieldPurchasable',
      criteria: [
        /Purchasable/,
        /^Buy$/,
      ],
      processorProvider: {
        convertTo: 'boolean',
      },
    },
    'variants[0].unitCost': {
      label: 'ui.product.import.fieldPurchasePrice',
      criteria: [
        /Purchase Price/,
        /Unit Cost/,
      ],
      processorProvider: {
        convertTo: 'number',
      },
    },
    expenseAccountSearch: {
      label: 'ui.product.import.fieldExpenseAccount',
      criteria: [
        /Expense Account/,
        /Expense Account Code/,
        /Expense Acc Code/,
      ],
    },
    purchaseTaxCode: {
      label: 'ui.product.import.fieldPurchaseTax',
      criteria: [
        /Purchase Tax/,
        /Purchase Tax Code/,
      ],
    },
    purchaseDescription: {
      label: 'ui.product.import.fieldPurchaseDescription',
      criteria: [
        /Purchase Description/,
      ],
    },
    inventoryAccountSearch: {
      label: 'ui.product.import.fieldInventoryAccount',
      criteria: [
        /Inventory Account/,
        /Inventory Account Code/,
        /Inventory Acc Code/,
      ],
    },
    isVariant: {
      label: 'ui.product.import.fieldIsVariant',
      criteria: [
        /Is Variant/,
      ],
    },
    attribute1Search: {
      label: 'ui.product.import.fieldAttribute1',
      criteria: [
        /Attribute 1/,
        /Attr 1/,
      ],
    },
    attribute2Search: {
      label: 'ui.product.import.fieldAttribute2',
      criteria: [
        /Attribute 2/,
        /Attr 2/,
      ],
    },
    attribute3Search: {
      label: 'ui.product.import.fieldAttribute3',
      criteria: [
        /Attribute 3/,
        /Attr 3/,
      ],
    },
  };

  normalizeDoc(rawProducts: any = [], rawRecords: any = []) {
    const products = [];

    let currentProduct;
    let currentVariantIdx = 0;
    _.forEach<any, any>(rawProducts, (record, recordIdx) => {
      const rawRecord = _.find<any>(rawRecords, { seqNumber: record._identifier });
      if (!rawRecord.isVariant) {
        currentProduct = record;
        products.push(currentProduct);
        currentVariantIdx = 0;
      } else if (currentProduct) {
        _.set(currentProduct, `variants[${currentVariantIdx}].prices[0].unitPrice`, _.get(rawRecord, 'variants[0].prices[0].unitPrice', 0));
        _.set(currentProduct, `variants[${currentVariantIdx}].unitCost`, _.get(rawRecord, 'variants[0].unitCost', 0));
        _.set(currentProduct, `variants[${currentVariantIdx}].sku`, _.get(rawRecord, 'variants[0].sku', null));
        _.set(currentProduct, `variants[${currentVariantIdx}].attribute1Value`, _.get(rawRecord, 'attribute1Search', null));
        _.set(currentProduct, `variants[${currentVariantIdx}].attribute2Value`, _.get(rawRecord, 'attribute2Search', null));
        _.set(currentProduct, `variants[${currentVariantIdx}].attribute3Value`, _.get(rawRecord, 'attribute3Search', null));

        currentVariantIdx++;
      }
    });

    return products;
  }
}

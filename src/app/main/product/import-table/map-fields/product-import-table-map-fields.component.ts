import * as _ from 'lodash';
import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';

import { ProductImportTableService } from '../product-import-table.service';

@Component({
  selector: 'app-product-import-table-map-fields',
  templateUrl: './product-import-table-map-fields.component.html',
})
export class ProductImportTableMapFieldsComponent implements OnInit {
  @Input() formState: any = {};

  clodeoFields: any = _.map(this._productImportTable.importMap, (map, field) => {
    return {
      field,
      ..._.pick(map, ['index', 'label'])
    };
  });

  userFields: any = [];

  constructor(
    public _productImportTable: ProductImportTableService,
  ) { }

  ngOnInit() {
    this.formState.importMap = Object.assign({}, this._productImportTable.importMap);
  }

  process() {
    this.formState.importData.setMap(this.formState.importMap);

    return Observable.fromPromise(this.formState.importData.process()).do(records => {
      this.formState.records = _.map(<any>records, (record, index) => {
        (<any>record).seqNumber = index;
        return record;
      });
    });
  }
}

import { MProductAttribute } from '../product-attribute/product-attribute.model';
import { MProductCategory } from '../product-category/product-category.model';
import { MProductTag } from '../product-tag/product-tag.model';
import { MSalesChannelAccountProduct, MSalesChannelAccountProductVariant } from '../sales-channel/sales-channel.model';
import { MTax } from '../tax/tax.model';
import { MUOM } from '../uom/uom.model';

export class MProduct {
  variants: MProductVariant[];
  masterPrice: MProductPrice;
  inventory: {
    qty: number;
    reservedQty: number;
    availableQty: number;
    salesOrderQty: number;
    purchaseOrderQty: number;
    totalCost: number;
    averageCost: number;
    lastCost: number
  };
  id: string;
  productType: string;
  sku: string;
  barcode: string;
  name: string;
  obQty: number;
  obDate: string | Date;
  detailDescription: string;
  categoryId: string;
  categoryId2: string;
  categoryId3: string;
  categoryId4: string;
  brandId: string;
  model: string;
  sellable: boolean;
  salesDescription: string;
  salesTaxId: string;
  incomeAccountId: string;
  autoReserveInventoryOnSalesOrder: boolean;
  purchasable: boolean;
  purchaseDescription: string;
  purchaseTaxId: string;
  expenseAccountId: string;
  unitCost: number;
  inventoryAccountId: string;
  minimumQty: number;
  uomId: string;
  uom2Id: string;
  uom2Conversion: number;
  uom3Id: string;
  uom3Conversion: number;
  defaultSalesUom: number;
  defaultPurchaseUom: number;
  dimension: {
    weight: number;
    height: number;
    width: number;
    length: number
  };

  variantAttribute1Id: string;
  variantAttribute2Id: string;
  variantAttribute3Id: string;

  variantCount: number;
  mainPictureId: string;
  inactive: boolean;
  rowVersion: string;

  category: MProductCategory;
  category2: MProductCategory;
  category3: MProductCategory;
  category4: MProductCategory;

  salesTax: MTax;
  purchaseTax: MTax;

  incomeAccount: {
    id: string;
    accountClassId: string;
    accountClassName: string;
    accountClassSortOrder: number;
    statementType: string;
    journalPosition: string;
    accountTypeId: string;
    accountTypeName: string;
    accountTypeSortOrder: string;
    code: string;
    name: string;
    description: string;
    isChildAccount: boolean;
    level: number;
    systemLocked: boolean;
    systemType: string;
    inactive: boolean
  };
  expenseAccount: {
    id: string;
    accountClassId: string;
    accountClassName: string;
    accountClassSortOrder: number;
    statementType: string;
    journalPosition: string;
    accountTypeId: string;
    accountTypeName: string;
    accountTypeSortOrder: string;
    code: string;
    name: string;
    description: string;
    isChildAccount: boolean;
    level: number;
    systemLocked: boolean;
    systemType: string;
    inactive: boolean
  };
  inventoryAccount: {
    id: string;
    accountClassId: string;
    accountClassName: string;
    accountClassSortOrder: number;
    statementType: string;
    journalPosition: string;
    accountTypeId: string;
    accountTypeName: string;
    accountTypeSortOrder: string;
    code: string;
    name: string;
    description: string;
    isChildAccount: boolean;
    level: number;
    systemLocked: boolean;
    systemType: string;
    inactive: boolean
  };

  uom: MUOM;
  uom2: MUOM;
  uom3: MUOM;

  variantAttribute1: MProductAttribute;
  variantAttribute2: MProductAttribute;
  variantAttribute3: MProductAttribute;

  tags: MProductTag;

  mainPicture: {
    id: string;
    fileType: string;
    fileName: string;
    fileExt: string;
    fileSize: number;
    description: string;
    uploadByUserId: string;
    fileUrl: string;
    thumbnailUrl: string;
    smallUrl: string;
    mediumUrl: string;
    largeUrl: string;
    tabletUrl: string
  };
  pictures: [
    {
      id: string;
      isMain: boolean;
      sortOrder: number;
      fileType: string;
      fileName: string;
      fileExt: string;
      fileSize: number;
      description: string;
      uploadByUserId: string;
      fileUrl: string;
      thumbnailUrl: string;
      smallUrl: string;
      mediumUrl: string;
      largeUrl: string;
      tabletUrl: string
    }
  ];

  salesChannels?: MSalesChannelAccountProduct[];

  _identifier: any;
}

export class MProductVariant {
  id: string;
  tempId?: any;
  productId: string;
  sku: string;
  name: string;
  obQty: number;
  obDate: string | Date;
  variantName: string;
  attribute1Value: string;
  attribute2Value: string;
  attribute3Value: string;
  unitCost: number;
  isMaster: boolean;
  inactive: boolean;
  rowVersion: string;
  masterPrice: MProductPrice;
  prices: MProductPrice[];
  inventory: {
    qty: number;
    reservedQty: number;
    availableQty: number;
    salesOrderQty: number;
    purchaseOrderQty: number;
    totalCost: number;
    averageCost: number;
    lastCost: number
  };
  salesChannels?: MSalesChannelAccountProductVariant[];
}

export class MProductPrice {
  priceLevelId: string;
  productId: string;
  productVariantId: string;
  unitPrice: number;
  discountPercent: number;
  discountAmount: number;
  unitPrice2: number;
  discountPercent2: number;
  discountAmount2: number;
  unitPrice3: number;
  discountPercent3: number;
  discountAmount3: number;
  priceLevel: {
    id: string;
    name: string;
    isMaster: boolean
  };
  isMaster: boolean;
}

export class MProductRelatedData {
  categories: IProductCategory[];
  attributes: IProductAttribute[];
  taxes: ITax[];
  uoms: IUOM[];
  priceLevels: IPriceLevel[];
  accounts: IAccount[];
  productBrands: IProductBrand[];
  settings: ISettings;
}

export class MProductVariantSortIdentifier {
  uniqueId: string;
  index: number;
}

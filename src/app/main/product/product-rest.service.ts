import { Injectable } from '@angular/core';
import 'app/rxjs-imports.ts';

import { ApiQueryOption, RequestService } from '../../core/core.module';

import { APP_CONST } from '../../app.const';
import { MProduct, MProductRelatedData } from './product.model';

@Injectable()
export class ProductRestService {
  baseURL = `${APP_CONST.API_MAIN}/products`;
  request = this._request.new(this.baseURL);

  constructor(
    private _request: RequestService,
  ) { }

  create(product: any, queryParams: any = {}) {
    return this.request.post<MProduct>(``, product, {
      params: queryParams,
    });
  }

  createVariant(productId, productVariant: IProductVariant) {
    return this.request.post<IProductVariant>(`${productId}/variants`, productVariant);
  }

  delete(productId: string) {
    return this.request.delete<any>(`${productId}`);
  }

  deleteVariant(productId: string, productVariantId: string) {
    return this.request.delete<any>(`${productId}/variants/${productVariantId}`);
  }

  findAll(queryOption: ApiQueryOption, queryParams: any = {}) {
    return this.request.post<IApiPaginationResult<MProduct>>('q', queryOption, {
      params: queryParams
    });
  }

  findAllTransactions(productId, queryParams: any) {
    return this.request.get<IApiPaginationResult<IProductTransaction>>(`${productId}/transactions`, {
      params: queryParams,
    });
  }

  findWarehouseInventory(productId) {
    return this.request.get<MProduct[]>(`${productId}/inventories`);
  }

  load(productId: string, params?: any) {
    return this.request.get<MProduct>(`${productId}`, { params });
  }

  loadRelatedData() {
    return this.request.get<MProductRelatedData>(`entry-related-data`);
  }

  update(productId: string, updateObj: MProduct, queryParams: any = {}) {
    return this.request.put<MProduct>(`${productId}`, updateObj, {
      params: queryParams,
    });
  }

  updateVariant(productId: string, productVariantId: string, updateObj: IProductVariant) {
    return this.request.put<IProductVariant>(`${productId}/variants/${productVariantId}`, updateObj);
  }

  checkDuplicate(
    values: {
      id?: string;
      name: string;
    }
  ) {
    return this.request.post<boolean>(`check-duplicate`, values);
  }

  imports(records: {
    seqNumber: number;
    product: MProduct;
  }[]) {
    return this.request.post<[{
      seqNumber: 0,
      success: true,
      errors: [
        string
      ]
    }]>(`batch`, records);
  }
}

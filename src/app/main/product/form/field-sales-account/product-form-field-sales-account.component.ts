import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { AutocompleteService } from '../../../../core/core.module';

@Component({
  selector: 'app-product-form-field-sales-account',
  templateUrl: 'product-form-field-sales-account.component.html'
})
export class ProductFormFieldSalesAccountComponent {
  @Input() label: boolean = true;
  @Input() parentForm: FormGroup;
  @Input() accounts: any[];

  constructor(
    public _autocomplete: AutocompleteService
  ) { }
}

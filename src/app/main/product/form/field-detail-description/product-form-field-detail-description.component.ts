import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-product-form-field-detail-description',
  templateUrl: 'product-form-field-detail-description.component.html'
})
export class ProductFormFieldDetailDescriptionComponent {
  @Input() label: boolean = true;
  @Input() parentForm: FormGroup;
  @Input() widget: 'html' | 'raw' = 'raw';
}

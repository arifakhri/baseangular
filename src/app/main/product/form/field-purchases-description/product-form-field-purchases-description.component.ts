import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-product-form-field-purchases-description',
  templateUrl: 'product-form-field-purchases-description.component.html'
})
export class ProductFormFieldPurchasesDescriptionComponent {
  @Input() label: boolean = true;
  @Input() parentForm: FormGroup;
}

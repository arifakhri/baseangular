import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { FormArray, FormGroup } from '@angular/forms';
import * as _ from 'lodash';

import { ProductFormService } from '../../product-form.service';
import { MProduct } from '../../product.model';
import { ProductService } from '../../product.service';

@Component({
  selector: 'app-product-form-field-variants-update',
  templateUrl: 'product-form-field-variants-update.component.html'
})
export class ProductFormFieldVariantsUpdateComponent implements OnChanges {
  @Input() doc: MProduct;
  @Input() parentForm: FormGroup;
  @Input() priceLevels: IPriceLevel[] = [];
  @Input() UOMS: IUOM[] = [];

  constructor(
    private _product: ProductService,
    private _productForm: ProductFormService,
  ) { }

  ngOnChanges(changes: SimpleChanges) {
    if (_.has(changes, 'doc') && this.doc && this.doc.variants.length) {
      this.applyDoc();
    }
  }

  applyDoc() {
    _.forEach(this.doc.variants, (variant, variantIdx) => {
      let variantFormGroup = <FormGroup>(<FormArray>this.parentForm.get('variants')).at(<any>variantIdx);
      if (!variantFormGroup) {
        variantFormGroup = this._productForm.buildFormChildVariant();
        (<FormArray>this.parentForm.get('variants')).push(variantFormGroup);

        this._productForm.setFormChildVariantValidations(<any>variantIdx, this.parentForm);
      }

      variantFormGroup.patchValue({
        ...variant,
        prices: [],
      });

      _.forEach(variant.prices, price => {
        let currentPriceFormGroup: FormGroup;

        const currentPrices = variantFormGroup.get('prices').value;
        const currentFormPriceIndex = _.findIndex(currentPrices, { priceLevelId: price.priceLevelId });
        if (currentFormPriceIndex === -1) {
          currentPriceFormGroup = this._productForm.buildFormChildVariantPrice(price.priceLevelId);
          (<FormArray>variantFormGroup.get('prices')).push(currentPriceFormGroup);
        } else {
          currentPriceFormGroup = <FormGroup>(<FormArray>variantFormGroup.get('prices')).at(currentFormPriceIndex);
        }
        currentPriceFormGroup.patchValue(price);
      });
    });
  }

  private addVariant() {
    const formVariants = (<FormArray>this.parentForm.get('variants'));
    const variantFormGroup = this._productForm.buildFormChildVariant();
    formVariants.push(variantFormGroup);

    this._productForm.setFormChildVariantValidations(formVariants.length - 1, this.parentForm);
  }
}

import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { AutocompleteService } from '../../../../core/core.module';

@Component({
  selector: 'app-product-form-field-purchases-account',
  templateUrl: 'product-form-field-purchases-account.component.html'
})
export class ProductFormFieldPurchasesAccountComponent {
  @Input() label: boolean = true;
  @Input() parentForm: FormGroup;
  @Input() accounts: any[];

  constructor(
    public _autocomplete: AutocompleteService
  ) { }
}

import * as _ from 'lodash';
import { AutoComplete } from 'primeng/components/autocomplete/autocomplete';
import { Component, Input, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ModalDirective } from 'ngx-bootstrap';

import { AutocompleteService } from '../../../../core/core.module';

@Component({
  selector: 'app-product-form-field-attribute',
  templateUrl: './product-form-field-attribute.component.html',
})
export class ProductFormFieldAttributeComponent {
  @ViewChild('attributeAutoComplete') elAttributeAutoComplete: AutoComplete;
  @ViewChild('quickCreateModal') elQuickCreateModal: ModalDirective;

  @Input() attributes: any[] = [];
  @Input() label: boolean = true;
  @Input() index: number = 1;
  @Input() parentForm: FormGroup;

  constructor(
    public _autocomplete: AutocompleteService
  ) { }

  assignQuickCreatedAttribute(newRecord: any) {
    const record = _.pick(newRecord, ['id', 'name']);
    this.elAttributeAutoComplete.selectItem(record);
    this.attributes.push(record);
    this.elQuickCreateModal.hide();
  }

  onAttributeChosen(attribute: IProductAttribute) {
    this.parentForm.patchValue({ [`variantAttribute${this.index}Id`]: attribute.id });
  }

  onAttributeClear() {
    this.parentForm.patchValue({
      [`variantAttribute${this.index}`]: null,
      [`variantAttribute${this.index}Id`]: null,
    });
  }
}

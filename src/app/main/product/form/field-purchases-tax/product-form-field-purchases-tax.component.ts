import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { AutocompleteService } from '../../../../core/core.module';

@Component({
  selector: 'app-product-form-field-purchases-tax',
  templateUrl: 'product-form-field-purchases-tax.component.html'
})
export class ProductFormFieldPurchasesTaxComponent {
  @Input() label: boolean = true;
  @Input() parentForm: FormGroup;
  @Input() taxes: any[];

 constructor(
   public _autocomplete: AutocompleteService
 ) { }
}

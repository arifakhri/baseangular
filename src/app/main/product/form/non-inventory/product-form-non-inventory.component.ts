import { Component } from '@angular/core';

import { ProductFormBComponent } from '../../product-form.bcomponent';

@Component({
  selector: 'app-product-form-non-inventory',
  templateUrl: 'product-form-non-inventory.component.html'
})
export class ProductFormNonInventoryComponent extends ProductFormBComponent { }

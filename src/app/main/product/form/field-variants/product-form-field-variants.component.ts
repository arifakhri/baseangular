import * as _ from 'lodash';
import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { FormArray, FormGroup } from '@angular/forms';

import { ProductFormService } from '../../product-form.service';
import { ProductService } from '../../product.service';

import { MProduct } from '../../product.model';

@Component({
  selector: 'app-product-form-field-variants',
  templateUrl: 'product-form-field-variants.component.html'
})
export class ProductFormFieldVariantsComponent implements OnChanges, OnInit {
  @Input() doc: MProduct;
  @Input() parentForm: FormGroup;
  @Input() priceLevels: IPriceLevel[] = [];
  @Input() UOMS: IUOM[] = [];

  constructor(
    private _product: ProductService,
    private _productForm: ProductFormService,
  ) { }

  ngOnInit() {
    (<FormArray>this.parentForm.controls.attributes).valueChanges.subscribe(values => {
      this.transformFormAttributeValues(values);
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (_.has(changes, 'doc') && this.doc && this.doc.variants.length) {
      this.applyDoc();
    }
  }

  applyDoc() {
    _.forEach(this.doc.variants, (variant, variantIdx) => {
      let variantFormGroup = (<FormArray>this.parentForm.get('variants')).at(<any>variantIdx);

      if (!variantFormGroup) {
        variantFormGroup = this._productForm.buildFormChildVariant();
        (<FormArray>this.parentForm.get('variants')).push(variantFormGroup);
      }

      variantFormGroup.patchValue({
        ...variant,
        prices: [],
      });

      _.forEach(variant.prices, price => {
        let currentPriceFormGroup: FormGroup;

        const currentPrices = variantFormGroup.get('prices').value;
        const currentFormPriceIndex = _.findIndex(currentPrices, { priceLevelId: price.priceLevelId });
        if (currentFormPriceIndex === -1) {
          currentPriceFormGroup = this._productForm.buildFormChildVariantPrice(price.priceLevelId);
          (<FormArray>variantFormGroup.get('prices')).push(currentPriceFormGroup);
        } else {
          currentPriceFormGroup = <FormGroup>(<FormArray>variantFormGroup.get('prices')).at(currentFormPriceIndex);
        }
        currentPriceFormGroup.patchValue(price);
      });
    });
  }

  private transformFormAttributeValues(values: any) {
    const variantsAttribute = [];

    const attributeValues1 = _.get(values, `[${0}].values`, []) || [];
    const attributeValues2 = _.get(values, `[${1}].values`, []) || [];
    const attributeValues3 = _.get(values, `[${2}].values`, []) || [];

    _.forEach(attributeValues1, (attributeValue1, attributeIdx1) => {
      if (attributeValues2.length) {
        _.forEach(attributeValues2, (attributeValue2, attributeIdx2) => {
          if (attributeValues3.length) {
            _.forEach(attributeValues3, (attributeValue3, attributeIdx3) => {
              variantsAttribute.push({
                attribute1Value: attributeValue1,
                attribute2Value: attributeValue2,
                attribute3Value: attributeValue3
              });
            });
          } else {
            variantsAttribute.push({
              attribute1Value: attributeValue1,
              attribute2Value: attributeValue2,
              attribute3Value: null,
            });
          }
        });
      } else {
        variantsAttribute.push({
          attribute1Value: attributeValue1,
          attribute2Value: null,
          attribute3Value: null,
        });
      }
    });

    this.patchFormVariantAttribute(variantsAttribute);
  }

  private patchFormVariantAttribute(variantsAttribute) {
    const formVariants = (<FormArray>this.parentForm.controls.variants);
    const oldFormVariantsValue = formVariants.value;

    const formVariantsControls = formVariants.controls;
    formVariantsControls.forEach((formVariant, formVariantIdx) => {
      if (!(<any>formVariant).get('isMaster').value) {
        formVariants.removeAt(formVariantIdx);
      }
    });

    let masterVariantValue;
    variantsAttribute.forEach((variantAttributeValue, variantAttributeIdx) => {
      let isNewFormGroup;
      let formVariant: FormGroup;
      if (!formVariantsControls[variantAttributeIdx]) {
        isNewFormGroup = true;
        formVariant = this._productForm.buildFormChildVariant();
        formVariants.push(formVariant);
      } else {
        isNewFormGroup = false;
        formVariant = <FormGroup>formVariantsControls[variantAttributeIdx];
      }
      const currentFormValue = oldFormVariantsValue[variantAttributeIdx] || {};
      if (variantAttributeIdx === 0) {
        currentFormValue.isMaster = true;
        masterVariantValue = currentFormValue;
      } else if (isNewFormGroup && masterVariantValue) {
        if (masterVariantValue.prices && masterVariantValue.prices.length) {
          _.forEach(masterVariantValue.prices, masterVariantPrice => {
            const variantPriceForm = this._productForm.buildFormChildVariantPrice(masterVariantPrice.priceLevelId, masterVariantPrice.isMaster);
            variantPriceForm.patchValue(masterVariantPrice);
            (<FormArray>formVariant.get('prices')).push(variantPriceForm);
          });
        }
        formVariant.patchValue({
          ...masterVariantValue,
          isMaster: false,
          sku: '',
        });
      }
      formVariant.patchValue(Object.assign({}, currentFormValue, variantAttributeValue));
    });
  }

  copyFromMaster(isSingleTarget: boolean, targetForm?: FormArray) {
    const masterVariant = _.find(this.parentForm.value.variants, { 'isMaster': true });
    let formToPatch = [];

    if (isSingleTarget) {
      formToPatch.push(targetForm);
    }
    if (!targetForm) {
      formToPatch = this.parentForm.controls.variants['controls'];
    }

    _.forEach(formToPatch, target => {
      target.patchValue({
        prices: masterVariant.prices,
        unitCost: masterVariant.unitCost,
      });
    });
  }
}

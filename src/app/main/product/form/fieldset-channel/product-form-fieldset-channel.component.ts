import { Component, Inject, Input, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { FormGroup } from '@angular/forms';
import * as _ from 'lodash';
import { ModalDirective } from 'ngx-bootstrap';

import { SystemMessageService } from '../../../../core/core.module';
import { BasePageComponent } from '../../../../shared/base/page/base-page.component';
import { MSalesChannelAccount } from '../../../sales-channel/sales-channel.model';
import { ProductFormChannelBComponent } from '../../form-channel/product-form-channel.bcomponent';
import { MFormChannelSubmitResult, MSalesChannelProductMetadata } from '../../form-channel/product-form-channel.model';
import { ProductFormChannelService } from '../../form-channel/product-form-channel.service';
import { MProduct } from '../../product.model';

@Component({
  selector: 'app-product-form-fieldset-channel',
  templateUrl: './product-form-fieldset-channel.component.html',
})
export class ProductFormFieldsetChannelComponent implements OnInit {
  @Input() doc: MProduct;
  @Input() formType: string;
  @Input() accounts: MSalesChannelAccount[];
  @Input() selectedAccounts: any = [];
  @Input() salesChannelProducts: any = [];
  @Input() productForm: FormGroup;
  @Input() masterPage: BasePageComponent;

  @ViewChild('addChannelModal') addChannelModal: ModalDirective;
  @ViewChildren('formSalesChannel') elFormSalesChannels: QueryList<ProductFormChannelBComponent>;

  get formValid() {
    return this.elFormSalesChannels.first ? this.elFormSalesChannels.first.formValid : true;
  }

  constructor(
    @Inject('GlobalSystemMessage') public _globalSystemMessage: SystemMessageService,
    private _productFormChannel: ProductFormChannelService,
  ) { }

  onSelectedAccounts(selectedAccounts: any) {
    this.selectedAccounts = selectedAccounts;

    this.addChannelModal.hide();
  }

  ngOnInit() {
    if (this.formType === 'update') {
      this.selectedAccounts = this.selectedAccounts.concat(
        this.doc.salesChannels.map(
          (productChannelAccount, productChannelAccountIdx) => {
            const account = _.find(this.accounts, { channelAccountId: productChannelAccount.channelAccountId });
            return {
              account,
              product: this.salesChannelProducts[productChannelAccountIdx],
              formType: 'update',
            };
          }
        )
      );
    }
  }

  getChannelProductsMetadata(product: MProduct): MSalesChannelProductMetadata[] {
    return this.elFormSalesChannels.map(elFormSalesChannel => ({
        channelAccountId: elFormSalesChannel.account.channelAccountId,
        channelProduct: elFormSalesChannel.getChannelProduct(product)
      })
    );
  }

  onCloseTab($event) {
    this.selectedAccounts.splice($event.index, 1);
  }
}

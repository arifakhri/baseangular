import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-product-form-field-name',
  templateUrl: 'product-form-field-name.component.html'
})
export class ProductFormFieldNameComponent {
  @Input() label: boolean = true;
  @Input() parentForm: FormGroup;
}

import * as _ from 'lodash';
import { AutoComplete } from 'primeng/components/autocomplete/autocomplete';
import { Component, Input, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ModalDirective } from 'ngx-bootstrap';

import { AutocompleteService } from '../../../../core/core.module';

@Component({
  selector: 'app-product-form-field-category',
  templateUrl: 'product-form-field-category.component.html'
})
export class ProductFormFieldCategoryComponent {
  @ViewChild('categoryAutoComplete') elCategoryAutoComplete: AutoComplete;
  @ViewChild('quickCreateModal') elQuickCreateModal: ModalDirective;

  @Input() label: boolean = true;
  @Input() categories: any[] = [];
  @Input() parentForm: FormGroup;

  constructor(
    public _autocomplete: AutocompleteService
  ) { }

  assignQuickCreatedCategory(newRecord: any) {
    const record = _.pick(newRecord, ['id', 'name']);
    this.elCategoryAutoComplete.selectItem(record);
    this.categories.push(record);
    this.elQuickCreateModal.hide();
  }


}

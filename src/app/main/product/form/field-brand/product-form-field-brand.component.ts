import * as _ from 'lodash';
import { AutoComplete } from 'primeng/components/autocomplete/autocomplete';
import { Component, Input, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ModalDirective } from 'ngx-bootstrap';

import { AutocompleteService } from '../../../../core/core.module';

@Component({
  selector: 'app-product-form-field-brand',
  templateUrl: 'product-form-field-brand.component.html'
})
export class ProductFormFieldBrandComponent {
  @ViewChild('brandAutoComplete') elBrandAutoComplete: AutoComplete;
  @ViewChild('quickCreateModal') elQuickCreateModal: ModalDirective;

  @Input() label: boolean = true;
  @Input() brands: any[] = [];
  @Input() parentForm: FormGroup;

  constructor(
    public _autocomplete: AutocompleteService
  ) { }

  assignQuickCreatedBrand(newRecord: any) {
    const record = _.pick(newRecord, ['id', 'name']);
    this.elBrandAutoComplete.selectItem(record);
    this.brands.push(record);
    this.elQuickCreateModal.hide();
  }

}

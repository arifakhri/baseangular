import { Component, Input, OnInit, QueryList, ViewChildren } from '@angular/core';
import * as _ from 'lodash';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';

import { BaseFormBComponent } from '../../../../shared/base/base.module';
import { SalesChannelMarketplaceRestService } from '../../../sales-channel/sales-channel-marketplace-rest.service';
import { SalesChannelRestService } from '../../../sales-channel/sales-channel-rest.service';
import { SalesChannelWebstoreRestService } from '../../../sales-channel/sales-channel-webstore-rest.service';
import { MSalesChannelAccount } from '../../../sales-channel/sales-channel.model';
import { ProductFormService } from '../../product-form.service';
import { ProductRestService } from '../../product-rest.service';
import { ProductFormFieldsetChannelComponent } from '../fieldset-channel/product-form-fieldset-channel.component';

@Component({
  selector: 'app-product-form-base',
  templateUrl: './product-form-base.component.html',
})
export class ProductFormBaseComponent extends BaseFormBComponent implements OnInit {
  @ViewChildren('formChannel') elFormChannel: QueryList<ProductFormFieldsetChannelComponent>;

  @Input() productType: 'inventory' | 'non_inventory' | 'service';
  @Input() displayType: 'small' | 'full';

  salesChannelAccounts: MSalesChannelAccount[] = [];
  salesChannelProducts: any = [];
  selectedUOMS: IUOM[] = [];

  existingPictures: any = [];

  salesChannelReady: boolean = false;

  constructor(
    private _productForm: ProductFormService,
    private _productRest: ProductRestService,
    private _salesChannelMarketplaceRest: SalesChannelMarketplaceRestService,
    private _salesChannelRest: SalesChannelRestService,
    private _salesChannelWebstoreRest: SalesChannelWebstoreRestService,
  ) {
    super();

    this.containerType = 0;

    this.registerHook('loadRelated', event => {
      return this._productRest.loadRelatedData();
    }, relatedData => {
      if (this.formType === 'create') {
        this._productForm.applySettings(this.form, relatedData).subscribe(() => {
          const defaultUOM = this.form.get('uom').value;
          if (defaultUOM) {
            this.selectedUOMS[0] = defaultUOM;
          }
        });
      }
    });
  }

  onPageInit() {
    super.onPageInit();

    let obs = this.loadSalesChannelAccounts();

    if (this.doc && this.formType === 'update') {
      obs = obs.switchMap(() => this.loadSalesChannelProducts());
    }

    obs.subscribe(() => {
      this.salesChannelReady = true;
    });
  }

  ngOnInit() {
    this.existingPictures = this.form.get('pictures').value;
    this.form.get('pictures').valueChanges.subscribe(pictures => {
      this.existingPictures = pictures;
    });
  }

  loadSalesChannelAccounts() {
    return this._salesChannelRest.findAllAccounts().do(accounts => {
      return this.salesChannelAccounts = accounts;
    });
  }

  loadSalesChannelProducts() {
    if (this.doc.salesChannels.length) {
      const loading = this.page.createLoading();

      return this.page.createRetriableTask(
        Observable.zip(
          ..._.map<any, any>(this.doc.salesChannels, (salesChannel, salesChannelIdx) => {
            let targetServiceFunc;
            switch (salesChannel.channelAccountType) {
              case 'webstore':
                targetServiceFunc = this._salesChannelWebstoreRest.loadProduct.bind(this._salesChannelWebstoreRest);
                break;
              case 'marketplace':
                targetServiceFunc = this._salesChannelMarketplaceRest.loadProduct.bind(this._salesChannelMarketplaceRest);
                break;
            }

            return targetServiceFunc(salesChannel.channelAccountId, this.doc.id).do((productChannel) => {
              this.salesChannelProducts[salesChannelIdx] = productChannel.data;
            });
          })
        ).do(() => {
          loading.dispose();
        })
      );
    } else {
      return Observable.of(null);
    }
  }
}

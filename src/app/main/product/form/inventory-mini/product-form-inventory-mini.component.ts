import { Component } from '@angular/core';

import { ProductFormBComponent } from '../../product-form.bcomponent';

@Component({
  selector: 'app-product-form-inventory-mini',
  templateUrl: 'product-form-inventory-mini.component.html'
})
export class ProductFormInventoryMiniComponent extends ProductFormBComponent { }


import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-product-form-field-sales-auto-reserve',
  templateUrl: 'product-form-field-sales-auto-reserve.component.html'
})
export class ProductFormFieldSalesAutoReserveComponent {
  @Input() label: boolean = true;
  @Input() parentForm: FormGroup;
}

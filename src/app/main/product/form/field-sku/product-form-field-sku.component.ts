import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-product-form-field-sku',
  templateUrl: 'product-form-field-sku.component.html'
})
export class ProductFormFieldCodeComponent {
  @Input() label: boolean = true;
  @Input() parentForm: FormGroup;
}

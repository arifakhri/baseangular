import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-product-form-field-purchases-purchasable',
  templateUrl: 'product-form-field-purchases-purchasable.component.html'
})
export class ProductFormFieldPurchasesPurchasableComponent {
  @Input() label: boolean = true;
  @Input() parentForm: FormGroup;
}

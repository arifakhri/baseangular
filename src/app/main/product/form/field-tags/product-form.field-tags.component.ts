import * as _ from 'lodash';
import { AfterViewInit, Component, Input, OnInit, ViewChild } from '@angular/core';
import { AutoComplete } from 'primeng/components/autocomplete/autocomplete';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';

import { ProductTagRestService } from '../../../product-tag/product-tag-rest.service';

import { MProduct } from '../../product.model';

@Component({
  selector: 'app-product-form-field-tags',
  templateUrl: 'product-form-field-tags.component.html'
})
export class ProductFormFieldTagsComponent implements AfterViewInit, OnInit {
  @Input() doc: MProduct;
  @Input() parentForm: FormGroup;

  @ViewChild('tagsAutoComplete') elTagsAutocomplete: AutoComplete;

  tags: any[] = [];
  tagsSuggestion: any[] = [];

  constructor(
    private _fb: FormBuilder,
    private _productTagRest: ProductTagRestService
  ) { }

  ngAfterViewInit() {
    const tagsControl = this.parentForm.controls.tags;
    const elACInput = $(this.elTagsAutocomplete.multiInputEL.nativeElement);
    elACInput.keypress(function (e) {
      if (e.which === 13) {
        e.preventDefault();
        const inputValue = e.target.value;
        const currentTags = tagsControl.value || [];
        currentTags.push(inputValue);
        tagsControl.setValue(currentTags);
        elACInput.val('');
        return false;
      }
    });
  }

  ngOnInit() {
    this.buildFields();
  }

  buildFields() {
    this.parentForm.addControl('tags', new FormControl([]));
  }

  eliminateNullTags() {
    const tags = this.parentForm.controls.tags.value;
    this.parentForm.controls.tags.setValue(tags.filter(_.size));
  }

  tagSearch(ev) {
    this._productTagRest.autoCompleteSearch(ev.query).subscribe(results => {
      this.tagsSuggestion = results;
    });
  }

  showSelectedTag(tag) {
    if (_.isString(tag)) {
      return tag;
    } else if (_.isObject(tag) && _.has(tag, 'name')) {
      return tag.name;
    }
  }
}

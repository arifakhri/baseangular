import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-product-form-field-type',
  templateUrl: './product-form-field-type.component.html',
})
export class ProductFormFieldTypeComponent {
  @Input() label: boolean = true;
  @Input() parentForm: FormGroup;

  productTypes: string[] = ['inventory', 'non_inventory', 'service'];
}

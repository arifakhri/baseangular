import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-product-form-field-purchases-price',
  templateUrl: './product-form-field-purchases-price.component.html',
})
export class ProductFormFieldPurchasesPriceComponent {
  @Input() label: boolean = true;
  @Input() parentForm: FormGroup;
}

import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-product-form-field-sales-sellable',
  templateUrl: 'product-form-field-sales-sellable.component.html'
})
export class ProductFormFieldSalesSellableComponent {
  @Input() label: boolean = true;
  @Input() parentForm: FormGroup;
}

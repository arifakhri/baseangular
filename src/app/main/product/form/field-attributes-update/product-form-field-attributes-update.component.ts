import * as _ from 'lodash';
import { AutoComplete } from 'primeng/components/autocomplete/autocomplete';
import { Component, Input, OnChanges, OnInit, QueryList, SimpleChanges, ViewChild, ViewChildren } from '@angular/core';
import { FormArray, FormGroup } from '@angular/forms';
import { ModalDirective } from 'ngx-bootstrap';

import { AutocompleteService } from '../../../../core/core.module';
import { ProductFormService } from '../../product-form.service';

import { MProduct } from '../../product.model';

@Component({
  selector: 'app-product-form-field-attributes-update',
  templateUrl: 'product-form-field-attributes-update.component.html'
})
export class ProductFormFieldAttributesUpdateComponent implements OnChanges, OnInit {
  @Input() attributes: any[] = [];
  @Input() doc: MProduct;
  @Input() parentForm: FormGroup;
  @ViewChildren(AutoComplete) autocompletes: QueryList<AutoComplete>;
  @ViewChild('quickCreateModal') elQuickCreateModal: ModalDirective;

  ACIndex: number;

  constructor(
    public _autocomplete: AutocompleteService,
    public _productForm: ProductFormService,
  ) { }

  ngOnInit() {
    this.parentForm.controls.attributes.valueChanges.subscribe(values => {
      if (!values.length) {
        this.parentForm.controls.hasVariants.setValue(false);
      }
    });

    this.parentForm.controls.hasVariants.valueChanges.subscribe(hasVariants => {
      if (hasVariants && !(<FormArray>this.parentForm.controls.attributes).controls.length) {
        this._productForm.addFormChildAttribute(this.parentForm);
      }
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (_.has(changes, 'doc')) {
      this.applyDoc();
    }
  }

  applyDoc() {
    if (this.doc.variantAttribute1Id) {
      let attributeFormGroup = (<FormArray>this.parentForm.get('attributes')).at(0);
      if (!attributeFormGroup) {
        attributeFormGroup =  this._productForm.addFormChildAttribute(this.parentForm);
      }
      attributeFormGroup.patchValue(this.doc.variantAttribute1);
    }

    if (this.doc.variantAttribute2Id) {
      let attributeFormGroup = (<FormArray>this.parentForm.get('attributes')).at(1);
      if (!attributeFormGroup) {
        attributeFormGroup =  this._productForm.addFormChildAttribute(this.parentForm);
      }
      attributeFormGroup.patchValue(this.doc.variantAttribute2);
    }

    if (this.doc.variantAttribute3Id) {
      let attributeFormGroup = (<FormArray>this.parentForm.get('attributes')).at(2);
      if (!attributeFormGroup) {
        attributeFormGroup =  this._productForm.addFormChildAttribute(this.parentForm);
      }
      attributeFormGroup.patchValue(this.doc.variantAttribute3);
    }
  }

  attributeChosen(index: number, attribute: any) {
    const targetIndex = index + 1;
    this.parentForm.controls[`variantAttribute${targetIndex}Id`].setValue(attribute.id);
    return true;
  }

  assignCreatedAttribute(productAttribute) {
    const attributesAutocomplete = this.autocompletes.toArray();
    const record = _.pick(productAttribute, ['id', 'name']);
    attributesAutocomplete[this.ACIndex].selectItem(record);
    this.attributes.push(record);
    this.elQuickCreateModal.hide();
  }

  getAttributeAddButtonDisabledState() {
    const formValues = this.parentForm.value;
    if (formValues.attributes.length) {
      const lastAttributeValue = formValues.attributes[formValues.attributes.length - 1];
      return !lastAttributeValue.id;
    }
    return false;
  }

  doRemoveAttribute(idx: number) {
    const variants = this.parentForm.get('variants').value;
    _.forEach((<FormArray>this.parentForm.get('variants')).controls, variant => {
      variant.get(`attribute${idx + 1}Value`).setValue(null);
    });

    this._productForm.removeFormChildAttribute(this.parentForm, idx);
  }
}

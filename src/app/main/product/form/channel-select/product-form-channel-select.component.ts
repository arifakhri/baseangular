import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import * as _ from 'lodash';
import { SelectItem } from 'primeng/api';

import { MSalesChannelAccount } from '../../../sales-channel/sales-channel.model';

@Component({
  selector: 'app-product-form-channel-select',
  templateUrl: './product-form-channel-select.component.html'
})
export class ProductFormChannelSelectComponent implements OnInit {
  @Input() accounts: MSalesChannelAccount[] = [];
  @Input() preSelectedAccounts: any = [];
  @Input() multiple: boolean = true;
  @Output() cancel: EventEmitter<any> = new EventEmitter;
  @Output() submit: EventEmitter<any> = new EventEmitter;

  listboxOptions: SelectItem[] = [];
  selectedAccounts: any = [];

  ngOnInit() {
    this.listboxOptions = this.accounts.map(account => ({
      label: account.name || account.channelName,
      value: {
        account,
        product: null,
        formType: 'create',
      },
    }));

    if (this.preSelectedAccounts.length) {
      this.preSelectedAccounts.forEach(preSelectedAccount => {
        this.selectedAccounts.push(preSelectedAccount);
      });
    }
  }

  doSubmit() {
    if (_.size(this.selectedAccounts)) {
      this.submit.emit(this.selectedAccounts);
    }
  }
}

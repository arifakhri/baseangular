import { Component } from '@angular/core';

import { ProductFormBComponent } from '../../product-form.bcomponent';

@Component({
  selector: 'app-product-form-service',
  templateUrl: 'product-form-service.component.html'
})
export class ProductFormServiceComponent extends ProductFormBComponent { }

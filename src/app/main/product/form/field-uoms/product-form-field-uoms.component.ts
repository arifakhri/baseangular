import * as _ from 'lodash';
import { AutoComplete } from 'primeng/components/autocomplete/autocomplete';
import { Component, Input, OnChanges, SimpleChanges, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ModalDirective } from 'ngx-bootstrap';

import { AutocompleteService } from '../../../../core/core.module';

import { MProduct } from '../../product.model';

@Component({
  selector: 'app-product-form-field-uoms',
  templateUrl: 'product-form-field-uoms.component.html'
})
export class ProductFormFieldUOMSComponent implements OnChanges {
  @Input() doc: MProduct;
  @Input() label: boolean = true;
  @Input() parentForm: FormGroup;
  @Input() selectedUOMS: IUOM[] = [];
  @Input() singleUOM: boolean = false;
  @Input() uoms: any[] = [];

  @ViewChild('quickCreateModal') elQuickCreateModal: ModalDirective;

  elTargetQuickCreate: AutoComplete;

  constructor(
    public _autocomplete: AutocompleteService
  ) { }

  ngOnChanges(changes: SimpleChanges) {
    if (_.has(changes, 'doc') && this.doc) {
      if (this.doc.uom) {
        this.selectedUOMS[0] = this.doc.uom;
      }
      if (this.doc.uom2) {
        this.parentForm.get('multipleUOM').setValue(true);
        this.selectedUOMS[1] = this.doc.uom2;
      }
      if (this.doc.uom3) {
        this.parentForm.get('multipleUOM').setValue(true);
        this.selectedUOMS[2] = this.doc.uom3;
      }
    }
  }

  assignCreatedUOM(newRecord: any) {
    const record = _.pick(newRecord, ['id', 'name']);
    this.elTargetQuickCreate.selectItem(record);
    this.uoms.push(record);
    this.elQuickCreateModal.hide();
  }

  getUOMGroupDisabledState(index: number) {
    const formValues = this.parentForm.value;
    if (index === 2) {
      return !formValues.uom || !formValues.uomId;
    } else if (index === 3) {
      return !formValues.uom2 || !formValues.uom2Id || !formValues.uom2Conversion;
    }
    return false;
  }

}

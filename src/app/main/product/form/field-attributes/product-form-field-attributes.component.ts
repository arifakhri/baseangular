import * as _ from 'lodash';
import { AutoComplete } from 'primeng/components/autocomplete/autocomplete';
import { Component, Input, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { FormArray, FormGroup } from '@angular/forms';
import { ModalDirective } from 'ngx-bootstrap';

import { AutocompleteService } from '../../../../core/core.module';
import { ProductFormService } from '../../product-form.service';

@Component({
  selector: 'app-product-form-field-attributes',
  templateUrl: 'product-form-field-attributes.component.html'
})
export class ProductFormFieldAttributesComponent implements OnInit {
  @Input() attributes: any[] = [];
  @Input() parentForm: FormGroup;
  @ViewChildren(AutoComplete) autocompletes: QueryList<AutoComplete>;
  @ViewChild('quickCreateModal') elQuickCreateModal: ModalDirective;

  ACIndex: number;

  constructor(
    public _autocomplete: AutocompleteService,
    public _productForm: ProductFormService,
  ) { }

  ngOnInit() {
    this.parentForm.controls.attributes.valueChanges.subscribe(values => {
      if (!values.length) {
        this.parentForm.controls.hasVariants.setValue(false);
      }
    });

    this.parentForm.controls.hasVariants.valueChanges.subscribe(hasVariants => {
      if (hasVariants && !(<FormArray>this.parentForm.controls.attributes).controls.length) {
        this._productForm.addFormChildAttribute(this.parentForm);
      }
    });
  }

  attributeChosen(index: number, attribute: any) {
    const targetIndex = index + 1;
    this.parentForm.controls[`variantAttribute${targetIndex}Id`].setValue(attribute.id);
    return true;
  }

  assignCreatedAttribute(productAttribute) {
    const attributesAutocomplete = this.autocompletes.toArray();
    const record = _.pick(productAttribute, ['id', 'name']);
    attributesAutocomplete[this.ACIndex].selectItem(record);
    this.attributes.push(record);
    this.elQuickCreateModal.hide();
  }

  getAttributeAddButtonDisabledState() {
    const formValues = this.parentForm.value;
    if (formValues.attributes.length) {
      const lastAttributeValue = formValues.attributes[formValues.attributes.length - 1];
      return !lastAttributeValue.id || !lastAttributeValue.values.length;
    }
    return false;
  }
}

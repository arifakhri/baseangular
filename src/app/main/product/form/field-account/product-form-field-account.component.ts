import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { AutocompleteService } from '../../../../core/core.module';

@Component({
  selector: 'app-product-form-field-account',
  templateUrl: 'product-form-field-account.component.html'
})
export class ProductFormFieldAccountComponent {
  @Input() label: boolean = true;
  @Input() parentForm: FormGroup;
  @Input() accounts: any[];

  constructor(
    public _autocomplete: AutocompleteService
  ) { }
}

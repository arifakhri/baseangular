import { Component } from '@angular/core';

import { ProductFormBComponent } from '../../product-form.bcomponent';

@Component({
  selector: 'app-product-form-inventory',
  templateUrl: 'product-form-inventory.component.html'
})
export class ProductFormInventoryComponent extends ProductFormBComponent { }

import * as _ from 'lodash';
import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { FormArray, FormGroup } from '@angular/forms';

import { ProductFormService } from '../../product-form.service';

import { MProduct } from '../../product.model';

@Component({
  selector: 'app-product-form-field-prices',
  templateUrl: 'product-form-field-prices.component.html',
  styleUrls: ['product-form-field-prices.component.scss']
})
export class ProductFormFieldPricesComponent implements OnChanges {
  @Input() doc: MProduct;
  @Input() formGroupClass: boolean = true;
  @Input() parentForm: FormGroup;
  @Input() priceLevels: IPriceLevel[] = [];
  @Input() selectedUOMS: IUOM[] = [];
  @Input() singleInputLabel: string = '';
  @Input() autoRebuildPrices: boolean = true;

  constructor(
    private _productForm: ProductFormService,
  ) { }

  ngOnChanges(changes: SimpleChanges) {
    if (this.autoRebuildPrices && _.has(changes, 'priceLevels') && this.parentForm && this.parentForm.contains('prices')) {
      this._productForm.syncFormChildVariantPrices(this.parentForm, this.priceLevels);
    }
  }

  getMasterPriceLevelFormControl(): FormGroup {
    const idx = _.findIndex(this.priceLevels, { isMaster: true });
    return <FormGroup>(<FormArray>this.parentForm.get('prices')).at(idx);
  }
}

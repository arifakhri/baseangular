import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { AutocompleteService } from '../../../../core/core.module';

@Component({
  selector: 'app-product-form-field-sales-tax',
  templateUrl: 'product-form-field-sales-tax.component.html'
})
export class ProductFormFieldSalesTaxComponent {
  @Input() label: boolean = true;
  @Input() parentForm: FormGroup;
  @Input() taxes: any[];

  constructor(
    public _autocomplete: AutocompleteService
  ) { }
}

import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-product-form-field-sales-description',
  templateUrl: 'product-form-field-sales-description.component.html'
})
export class ProductFormFieldSalesDescriptionComponent {
  @Input() label: boolean = true;
  @Input() parentForm: FormGroup;
}

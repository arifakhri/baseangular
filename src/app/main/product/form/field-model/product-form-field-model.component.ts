import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-product-form-field-model',
  templateUrl: 'product-form-field-model.component.html'
})
export class ProductFormFieldModelComponent {
  @Input() label: boolean = true;
  @Input() parentForm: FormGroup;
}

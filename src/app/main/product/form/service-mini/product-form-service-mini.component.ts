import { Component } from '@angular/core';

import { ProductFormBComponent } from '../../product-form.bcomponent';

@Component({
  selector: 'app-product-form-service-mini',
  templateUrl: 'product-form-service-mini.component.html'
})
export class ProductFormServiceMiniComponent extends ProductFormBComponent { }

import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { AutocompleteService } from '../../../../core/core.module';

@Component({
  selector: 'app-product-form-field-inventory-opening-balance',
  templateUrl: 'product-form-field-inventory-opening-balance.component.html'
})
export class ProductFormFieldInventoryOpeningBalanceComponent {
  @Input() label: boolean = true;
  @Input() parentForm: FormGroup;
  @Input() accounts: any[];

  constructor(
    public _autocomplete: AutocompleteService
  ) { }
}

import { Component } from '@angular/core';

import { ProductFormBComponent } from '../../product-form.bcomponent';

@Component({
  selector: 'app-product-form-non-inventory-mini',
  templateUrl: 'product-form-non-inventory-mini.component.html'
})
export class ProductFormNonInventoryMiniComponent extends ProductFormBComponent { }

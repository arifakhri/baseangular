import 'app/rxjs-imports.ts';

import { AfterViewInit, ElementRef, Input, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import * as _ from 'lodash';

import { InjectorService } from '../../../core/core.module';
import { BasePageBComponent } from '../../../shared/base/base.module';
import { BasePageComponent } from '../../../shared/base/page/base-page.component';
import { SpinnerService } from '../../../shared/spinner/spinner.module';
import {
  MSalesChannelMarketplaceProduct,
  MSalesChannelMarketplaceProductDynamicField,
} from '../../sales-channel/sales-channel-marketplace.model';
import {
  MSalesChannelWebstoreProduct,
  MSalesChannelWebstoreProductDynamicField,
} from '../../sales-channel/sales-channel-webstore.model';
import { MSalesChannelAccount } from '../../sales-channel/sales-channel.model';
import { MProduct } from '../product.model';
import { ProductFormChannelDefaultComponent } from './default/product-form-channel-default.component';
import { ProductFormChannelDynamicComponent } from './dynamic/product-form-channel-dynamic.component';
import { ProductFormChannelDynamicService } from './dynamic/product-form-channel-dynamic.service';
import { ProductFormChannelService } from './product-form-channel.service';
import { ProductFormChannelVariantsComponent } from './variants/product-form-channel-variants.component';

export class ProductFormChannelBComponent extends BasePageBComponent implements AfterViewInit {
  @Input() productChannel: MSalesChannelWebstoreProduct | MSalesChannelMarketplaceProduct;
  @Input() account: MSalesChannelAccount;
  @Input() masterPage: BasePageComponent;
  @Input() formType: string;
  @Input() productForm: FormGroup;

  @ViewChild('formDefault') elFormDefault: ProductFormChannelDefaultComponent;
  @ViewChild('formDynamicRequired') elFormDynamicRequired: ProductFormChannelDynamicComponent;
  @ViewChild('formDynamicNonRequired') elFormDynamicNonRequired: ProductFormChannelDynamicComponent;
  @ViewChild('formDynamicRequiredVariant') elFormDynamicRequiredVariant: ProductFormChannelDynamicComponent;
  @ViewChild('formDynamicNonRequiredVariant') elFormDynamicNonRequiredVariant: ProductFormChannelDynamicComponent;
  @ViewChild('formVariants') elFormVariants: ProductFormChannelVariantsComponent;

  dynamicFields: MSalesChannelMarketplaceProductDynamicField[] | MSalesChannelWebstoreProductDynamicField[] = [];
  dynamicFieldsProduct: MSalesChannelMarketplaceProductDynamicField[] | MSalesChannelWebstoreProductDynamicField[] = [];
  dynamicFieldsVariant: MSalesChannelMarketplaceProductDynamicField[] | MSalesChannelWebstoreProductDynamicField[] = [];

  public _elementRef: ElementRef;
  public _productFormChannel = InjectorService.get<ProductFormChannelService>(ProductFormChannelService);
  public _productFormChannelDynamic = InjectorService.get<ProductFormChannelDynamicService>(ProductFormChannelDynamicService);
  public _spinner = InjectorService.get<SpinnerService>(SpinnerService);

  get formValid() {
    const valDefaultValid = this.elFormDefault.formValid;
    const valVariantsValid = this.productForm.get('variantCount').value ? this.elFormVariants.formValid : true;
    const valVariantsDyanmicFieldsValid = this.productForm.get('variantCount').value ? this.elFormVariants.formDynamicFieldsValid : true;

    return !(
      !valDefaultValid ||
      !this._productFormChannelDynamic.isFormValid(this.elFormDynamicRequired, this.elFormDynamicNonRequired) ||
      !this._productFormChannelDynamic.isFormValid(this.elFormDynamicRequiredVariant, this.elFormDynamicNonRequiredVariant) ||
      !valVariantsDyanmicFieldsValid ||
      !valVariantsValid
    );
  }

  getChannelProduct(product: MProduct) {
    const valDefault = this.elFormDefault.formValue;
    const valVariants = this.productForm.get('variantCount').value ? this.elFormVariants.formValue : [];
    const productDynamicFieldsValue = this._productFormChannelDynamic.getFormValue(this.dynamicFields, this.elFormDynamicRequired, this.elFormDynamicNonRequired).product;
    const variantDynamicFieldsValue = this._productFormChannelDynamic.getFormValue(this.dynamicFields, this.elFormDynamicRequiredVariant, this.elFormDynamicNonRequiredVariant).variant;

    let productChannel = {
      ...valDefault,
      dynamic_fields: productDynamicFieldsValue,
      variants: valVariants,
    };

    productChannel = this._productFormChannel.normalizeProduct(
      this.productChannel,
      productChannel,
      product,
      variantDynamicFieldsValue,
    );

    return productChannel;
  }

  ngAfterViewInit() {
    const categoryField = this.elFormDefault.formgen.form.get('category');

    const categoryVal = categoryField.value;
    if (categoryVal) {
      this.loadDynamicFields();
    }

    categoryField.valueChanges.subscribe(category => {
      this.loadDynamicFields();
    });
  }

  loadDynamicFields() {
    if (this.hasHook('loadDynamicFields')) {
      const loading = this._spinner.show({
        element: this._elementRef.nativeElement,
        loaderType: 2,
        blockElementDisplay: true,
      });

      return this.page.createRetriableTask(
        this.callHook('loadDynamicFields').finally(() => {
          loading.dispose();
        })
      ).subscribe(responseDynamicFields => {
        this.dynamicFields = responseDynamicFields.data.records;

        const dynamicFieldsParts = _.partition(this.dynamicFields, { field_target: 'product' });
        this.dynamicFieldsProduct = <MSalesChannelWebstoreProductDynamicField[] | MSalesChannelMarketplaceProductDynamicField[]>dynamicFieldsParts[0];
        this.dynamicFieldsVariant = <MSalesChannelWebstoreProductDynamicField[] | MSalesChannelMarketplaceProductDynamicField[]>dynamicFieldsParts[1];

        this.page._changeDetectorRef.detectChanges();
      });
    }
  }

  getProductChannelDynamicFieldsValue() {
    return _.get(this.productChannel, `dynamic_fields`) || [];
  }

  getMasterVariantChannelDynamicFieldsValue() {
    return _.get(this.productChannel, 'variants[0].dynamic_fields') || [];
  }
}

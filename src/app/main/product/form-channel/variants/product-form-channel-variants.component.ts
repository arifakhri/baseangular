import { Component, Input, OnChanges, OnInit, QueryList, SimpleChanges, ViewChildren } from '@angular/core';
import { FormArray, FormControl, FormGroup } from '@angular/forms';
import * as _ from 'lodash';

import { CommonService, EventService } from '../../../../core/core.module';
import {
  MSalesChannelMarketplaceProduct,
  MSalesChannelMarketplaceProductDynamicField,
} from '../../../sales-channel/sales-channel-marketplace.model';
import {
  MSalesChannelWebstoreProduct,
  MSalesChannelWebstoreProductDynamicField,
} from '../../../sales-channel/sales-channel-webstore.model';
import { ProductFormChannelDynamicComponent } from '../dynamic/product-form-channel-dynamic.component';
import { ProductFormChannelDynamicService } from '../dynamic/product-form-channel-dynamic.service';
import { MProductVariant } from '../../product.model';

@Component({
  selector: 'app-product-form-channel-variants',
  templateUrl: './product-form-channel-variants.component.html',
})
export class ProductFormChannelVariantsComponent implements OnChanges, OnInit {
  @Input() formType: string;
  @Input() channelId: string;
  @Input() priceLevelId: string;
  @Input() productChannel: MSalesChannelWebstoreProduct | MSalesChannelMarketplaceProduct;
  @Input() productForm: FormGroup;
  @Input() dynamicFieldsVariant: MSalesChannelMarketplaceProductDynamicField[] | MSalesChannelWebstoreProductDynamicField[] = [];

  @ViewChildren('formDynamicRequired') elFormDynamicRequired: QueryList<ProductFormChannelDynamicComponent>;
  @ViewChildren('formDynamicNonRequired') elFormDynamicNonRequired: QueryList<ProductFormChannelDynamicComponent>;

  form: FormGroup = new FormGroup({});

  get formValid() {
    if (!this.form.valid) {
      CommonService.markAsDirty(this.form);
      return false;
    }
    return true;
  }

  get formValue() {
    const formDynamicFieldsValues = this.formDynamicFieldsValues;
    return _.map(this.form.value.variants, (formValueVariant, formValueVariantIdx) => {
      return {
        ...formValueVariant,
        dynamic_fields: formDynamicFieldsValues[formValueVariantIdx],
      };
    });
  }

  get formDynamicFieldsValid() {
    return this._productFormChannelDynamicField.isFormValid(...this.elFormDynamicRequired.toArray());
  }

  get formDynamicFieldsValues() {
    const elFormsDynamicRequired = this.elFormDynamicRequired.toArray();
    const elFormsDynamicNonRequired = this.elFormDynamicNonRequired.toArray();

    const formDynamicFieldsVariantsValue = _.map(this.form.get('variants')['controls'], (formVariant, formVariantIdx) => {
      return this._productFormChannelDynamicField.getFormValue(this.dynamicFieldsVariant, elFormsDynamicRequired[formVariantIdx], elFormsDynamicNonRequired[formVariantIdx]).variant;
    });

    return formDynamicFieldsVariantsValue;
  }

  constructor(
    private _event: EventService,
    private _productFormChannelDynamicField: ProductFormChannelDynamicService,
  ) {
    this.buildForm();
  }

  buildForm() {
    this.form.addControl('variants', new FormArray([]));

    this.form.get('variants').valueChanges.debounceTime(500).subscribe((variantsValue) => {
      this._event.emit(`PRODUCT:CHANNEL:${this.channelId}:VARIANTSCHANGE`, variantsValue);
    });
  }

  rebuildFormChildVariants(syncChannel: boolean = false) {
    const selfVariantsForm = (<FormArray>this.form.get('variants'));

    while (selfVariantsForm.length) {
      selfVariantsForm.removeAt(0);
    }

    if (this.productForm.get('hasVariants').value) {
      (<FormArray>this.productForm.get('variants')).controls.forEach((variantFormGroup, variantFormGroupIdx) => {
        const variantFormGroupValue = variantFormGroup.value;
        if (variantFormGroupValue.attribute1Value || variantFormGroupValue.attribute2Value || variantFormGroupValue.attribute3Value) {
          const variantChannel = _.find<any>(_.get(this.productChannel, 'variants', []), {
            clodeo_product_variant_id: variantFormGroupValue.id,
          });
          const mapVariantFormGroup = this.buildChannelVariantForm(variantFormGroupIdx);
          let mapVariantFormPatch = {};
          if (syncChannel) {
            mapVariantFormPatch = this.syncByChannelVariantPatch(variantFormGroupValue, variantChannel);
          } else {
            mapVariantFormPatch = this.syncByProductVariantPatch(variantFormGroupValue, variantChannel);
          }
          mapVariantFormGroup.patchValue(mapVariantFormPatch);
          selfVariantsForm.setControl(variantFormGroupIdx, mapVariantFormGroup);
        }
      });
    }
  }

  buildChannelVariantForm(productVariantFormGroupIdx: number) {
    const variantFormGroup = new FormGroup({
      channel_product_id: new FormControl(null),
      channel_product_variant_id: new FormControl(null),
      clodeo_product_variant_id: new FormControl(null),
      variantName: new FormControl,
      name: new FormControl,
      sku: new FormControl,
      price: new FormControl,
      attribute1_value: new FormControl,
      attribute2_value: new FormControl,
      attribute3_value: new FormControl,
    });

    variantFormGroup.get('attribute1_value').setValue(this.productForm.get(`variants.${productVariantFormGroupIdx}.attribute1Value`).value);
    this.productForm.get(`variants.${productVariantFormGroupIdx}.attribute1Value`).valueChanges.subscribe(varAttribute1Val => {
      variantFormGroup.get('attribute1_value').setValue(varAttribute1Val);
    });

    variantFormGroup.get('attribute2_value').setValue(this.productForm.get(`variants.${productVariantFormGroupIdx}.attribute2Value`).value);
    this.productForm.get(`variants.${productVariantFormGroupIdx}.attribute2Value`).valueChanges.subscribe(varAttribute2Val => {
      variantFormGroup.get('attribute2_value').setValue(varAttribute2Val);
    });

    variantFormGroup.get('attribute3_value').setValue(this.productForm.get(`variants.${productVariantFormGroupIdx}.attribute3Value`).value);
    this.productForm.get(`variants.${productVariantFormGroupIdx}.attribute3Value`).valueChanges.subscribe(varAttribute3Val => {
      variantFormGroup.get('attribute3_value').setValue(varAttribute3Val);
    });

    return variantFormGroup;
  }

  syncByProductVariantPatch(
    variantFormGroupValue: any,
    variantChannel: any = {},
    type: 'new' | 'map' = 'new',
  ) {
    const variantName = [
      variantFormGroupValue.attribute1Value,
      variantFormGroupValue.attribute2Value,
      variantFormGroupValue.attribute3Value,
    ].filter(_.identity).join('/');
    return {
      channel_product_id: variantChannel.channel_product_id || null,
      channel_product_variant_id: variantChannel.channel_product_variant_id || null,
      clodeo_product_variant_id: variantFormGroupValue.id || null,
      variantName: variantName,
      name: variantChannel.name || '',
      sku: variantFormGroupValue.sku,
      price: this.getParentVariantPrice(variantFormGroupValue) || 0,
    };
  }

  syncByChannelVariantPatch(
    variantFormGroupValue: any,
    variantChannel: any = {},
  ) {
    const variantName = [
      variantFormGroupValue.attribute1Value,
      variantFormGroupValue.attribute2Value,
      variantFormGroupValue.attribute3Value,
    ].filter(_.identity).join('/');
    return {
      channel_product_id: variantChannel.channel_product_id || null,
      channel_product_variant_id: variantChannel.channel_product_variant_id || null,
      clodeo_product_variant_id: variantFormGroupValue.id || null,
      variantName: variantName,
      name: variantChannel.name || '',
      sku: variantChannel.sku,
      price: variantChannel.price || 0,
    };
  }

  ngOnChanges(changes: SimpleChanges) {
    if (_.has(changes, 'productChannel') && this.productChannel && !changes.productChannel.firstChange) {
      this.rebuildFormChildVariants();
    }
  }

  ngOnInit() {
    this.rebuildFormChildVariants(this.formType === 'update');

    this.productForm.get('variants').valueChanges.debounceTime(500).subscribe(() => {
      this.rebuildFormChildVariants();
    });
  }

  getChannelVariantDyanmicFieldsValue(channelVariantIdx: number) {
    return _.get(this.productChannel, `variants[${channelVariantIdx}].dynamic_fields`) || [];
  }

  getParentVariantPrice(variantFormGroupValue: MProductVariant) {
    let targetPrice = _.find(variantFormGroupValue.prices, { isMaster: true });
    if (this.priceLevelId) {
      targetPrice = _.find(variantFormGroupValue.prices, { priceLevelId: this.priceLevelId });
    }
    return _.get(targetPrice, 'unitPrice');
  }
}

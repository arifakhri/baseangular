import 'app/rxjs-imports.ts';

import { AfterViewInit } from '@angular/core';

import { InjectorService } from '../../../core/core.module';
import { SalesChannelWebstoreRestService } from '../../sales-channel/sales-channel-webstore-rest.service';
import { MSalesChannelWebstoreProductCategory } from '../../sales-channel/sales-channel-webstore.model';
import { ProductFormChannelBComponent } from './product-form-channel.bcomponent';

export class ProductFormChannelWebstoreBComponent extends ProductFormChannelBComponent implements AfterViewInit {
  public _salesChannelWebstoreRest = InjectorService.get<SalesChannelWebstoreRestService>(SalesChannelWebstoreRestService);

  categories: MSalesChannelWebstoreProductCategory[];

  ngAfterViewInit() {
    super.ngAfterViewInit();

    this.loadCategories();
  }

  loadCategories() {
    const loading = this.page.createLoading();

    return this.page.createRetriableTask(
      this._salesChannelWebstoreRest.findAllCategories(this.account.channelAccountId).finally(() => {
        loading.dispose();
      })
    ).subscribe(categories => {
      this.categories = categories.data;
    });
  }
}

import { Injectable } from '@angular/core';
import * as _ from 'lodash';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';

import { MessageParserService } from '../../../core/core.module';
import { SalesChannelMarketplaceRestService } from '../../sales-channel/sales-channel-marketplace-rest.service';
import {
  MSalesChannelMarketplaceProduct,
  MSalesChannelMarketplaceProductDynamicField,
  MSalesChannelMarketplaceProductDynamicFieldValue,
} from '../../sales-channel/sales-channel-marketplace.model';
import { SalesChannelWebstoreRestService } from '../../sales-channel/sales-channel-webstore-rest.service';
import { MSalesChannelWebstoreProduct } from '../../sales-channel/sales-channel-webstore.model';
import { MProduct } from '../product.model';
import {
  MSalesChannelProductMetadata,
  MFormChannelSubmitResult,
  MSalesChannelProductSyncResponse,
  MSalesChannelProductSyncItem,
  MSalesChannelProductSyncResult,
} from './product-form-channel.model';

@Injectable()
export class ProductFormChannelService {
  constructor(
    private _messageParser: MessageParserService,
    private _salesChannelMarketplaceRest: SalesChannelMarketplaceRestService,
    private _salesChannelWebstoreRest: SalesChannelWebstoreRestService,
  ) { }

  normalizeProduct(
    oldProductChannel: MSalesChannelWebstoreProduct | MSalesChannelMarketplaceProduct,
    productChannel: MSalesChannelWebstoreProduct | MSalesChannelMarketplaceProduct,
    product: MProduct,
    valDynamicVariant: any = [],
  ) {
    const productPictures = _.map<any, any>(_.sortBy(product.pictures, ['sortOrder']), picture => ({
      src: picture.fileUrl,
    }));

    const productChannelNormalized: any = {
      ...productChannel,
      clodeo_product_id: product.id,
      attribute1: _.get(product.variantAttribute1, 'name', null),
      attribute2: _.get(product.variantAttribute2, 'name', null),
      attribute3: _.get(product.variantAttribute3, 'name', null),
    };

    productChannelNormalized.variants = _.reject(productChannelNormalized.variants, variant => !variant.name && !variant.price);

    if (!productChannelNormalized.variants.length) {
      productChannelNormalized.variants[0] = Object.assign({}, _.get(oldProductChannel, `variants[${0}]`) || {}, {
        clodeo_product_variant_id: product.variants[0].id,
        name: productChannel.name || product.name,
        sku: productChannel.sku || product.variants[0].sku,
        price: productChannel.price || 0,
        qty: product.variants[0].obQty || 0,
        dynamic_fields: valDynamicVariant,
      });
    }

    productChannelNormalized.variants.forEach((channelVariant, channelVariantIdx) => {
      const productVariant = product.variants[channelVariantIdx];
      if (!channelVariant.clodeo_product_variant_id) {
        channelVariant.clodeo_product_variant_id = productVariant.id;
      }

      if (!channelVariant.sku && productVariant.sku) {
        channelVariant.sku = productVariant.sku;
      }

      channelVariant.qty = productVariant.obQty || 0;
    });

    return productChannelNormalized;
  }

  normalizeDynamicFieldsValue(
    dynamicFieldsValue: any,
    dynamicFieldsDefinition: MSalesChannelMarketplaceProductDynamicField[] = [],
  ) {
    return _.map(dynamicFieldsValue, (dynamicFieldValue, dynamicFieldValueKey): MSalesChannelMarketplaceProductDynamicFieldValue => {
      const dynamicField = _.find(dynamicFieldsDefinition, { name: dynamicFieldValueKey });

      return {
        key: dynamicFieldValueKey.toString(),
        value: this.extractDynamicFieldValue(dynamicFieldValue),
        field_type: dynamicField.input_type,
      };
    }
    );
  }

  extractDynamicFieldValue(dynamicFieldValueVal: any) {
    let extractedVal = dynamicFieldValueVal;

    if (_.has(dynamicFieldValueVal, 'key')) {
      extractedVal = _.get(dynamicFieldValueVal, 'key');
    }

    if (_.isArray(extractedVal)) {
      const arrExtractedValues = [];

      _.forEach(extractedVal, val => {
        const valTransformed = this.extractDynamicFieldValue(val);
        arrExtractedValues.push(valTransformed);
      });

      extractedVal = arrExtractedValues.join(',');
    }

    return extractedVal;
  }

  attachToProduct(product: MProduct, channelProductMetadata: MSalesChannelProductMetadata) {
    const productChannelMain = _.omit(channelProductMetadata.channelProduct, 'variants');
    const productChannelVariants = _.get(_.pick(channelProductMetadata.channelProduct, 'variants'), 'variants') || [];

    if (!product.salesChannels) {
      product.salesChannels = [];
    }

    const targetProductSalesChannel: any = {
    ...productChannelMain,
      channelAccountId: channelProductMetadata.channelAccountId,
      individual_variant: false,
      allow_variants: true,
    };

    const existingProductSalesChannel = _.find(product.salesChannels, { channelAccountId: channelProductMetadata.channelAccountId });
    if (existingProductSalesChannel) {
      _.assign(existingProductSalesChannel, targetProductSalesChannel);
    } else {
      product.salesChannels.push(targetProductSalesChannel);
    }

    if (productChannelVariants.length) {
      product.variants.forEach((productVariant, productVariantIdx) => {
        if (!productVariant.salesChannels) {
          productVariant.salesChannels = [];
        }

        const productChannelVariant = productChannelVariants[productVariantIdx];
        const targetProductVariantSalesChannel: any = {
          ...productChannelVariant,
          channelAccountId: channelProductMetadata.channelAccountId,
        };

        const existingProductVariantSalesChannel = _.find(productVariant.salesChannels, { channelAccountId: channelProductMetadata.channelAccountId });
        if (existingProductVariantSalesChannel) {
          _.assign(existingProductVariantSalesChannel, targetProductVariantSalesChannel);
        } else {
          productVariant.salesChannels.push(targetProductVariantSalesChannel);
        }
      });
    }
  }

  syncProduct(item: MSalesChannelProductSyncItem): Observable<MSalesChannelProductSyncResult> {
    let obs;
    switch (item.channelAccountType) {
      case 'marketplace':
        obs = this._salesChannelMarketplaceRest.syncProduct(item.channelAccountId, item.clodeoProductId);
        break;
      case 'webstore':
        obs = this._salesChannelWebstoreRest.syncProduct(item.channelAccountId, item.clodeoProductId);
        break;
    }

    return obs.switchMap(
      response => Observable.of({
        channelAccountId: item.channelAccountId,
        response,
      })
    );
  }

  syncMultipleProducts(items: MSalesChannelProductSyncItem[]): Observable<MSalesChannelProductSyncResult> {
    const observables = _.map(items, item => this.syncProduct(item));

    return Observable.merge(...observables);
  }
}

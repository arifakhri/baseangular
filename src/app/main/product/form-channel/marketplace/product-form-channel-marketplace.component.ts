import { Component, ElementRef, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';

import { SalesChannelMarketplaceRestService } from '../../../sales-channel/sales-channel-marketplace-rest.service';
import { ProductFormChannelMarketplaceBComponent } from '../product-form-channel-marketplace.bcomponent';

@Component({
  selector: 'app-product-form-channel-marketplace',
  templateUrl: './product-form-channel-marketplace.component.html',
})
export class ProductFormChannelMarketplaceComponent extends ProductFormChannelMarketplaceBComponent implements OnInit {
  constructor(
    public _elementRef: ElementRef,
    private _salesChannelMarketplaceRest: SalesChannelMarketplaceRestService,
  ) {
    super();

    this.registerHook('loadDynamicFields', event => {
      const category: any = this.elFormDefault.formgen.form.get('category').value;
      return category ?
        this._salesChannelMarketplaceRest.findAllDynamicFields(this.account.channelAccountId, category.id) :
        Observable.of([]);
    });
  }

  ngOnInit() {
    this.componentId = `ProductFormChannelMarketplace${this.account.channelAccountId}`;
  }
}

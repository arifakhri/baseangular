import { MHttpExtsrvResponse } from '../../../core/http/http-extsrv.model';
import { MSalesChannelMarketplaceProduct } from '../../sales-channel/sales-channel-marketplace.model';
import { MSalesChannelWebstoreProduct } from '../../sales-channel/sales-channel-webstore.model';
import { MSalesChannelAccount } from '../../sales-channel/sales-channel.model';

export class MSalesChannelProductMetadata {
  channelAccountId: string;
  channelProduct: MSalesChannelMarketplaceProduct | MSalesChannelWebstoreProduct;
}

export class MSalesChannelProductSyncItem {
  channelAccountId: string;
  clodeoProductId: string;
  channelAccountType: string;
}

export class MSalesChannelProductSyncResult {
  channelAccountId: string;
  response: MSalesChannelProductSyncResponse;
}

export class MSalesChannelProductSyncResponse extends MHttpExtsrvResponse<MSalesChannelProductSyncResponseData> {}

export class MSalesChannelProductSyncResponseData {
  clodeo_product_id: string;
  channel_product_id: string;
  success: boolean;
  individual_variant: boolean;
  variants: [
    {
      clodeo_product_variant_id: string;
      channel_product_id: string;
      channel_variant_id: string;
      success: boolean;
    }
  ];
}

export class MFormChannelSubmitResult {
  channelAccountId: string;
  payloadProductChannel: MSalesChannelMarketplaceProduct | MSalesChannelWebstoreProduct;
  responseProductChannel: MSalesChannelMarketplaceProduct | MSalesChannelWebstoreProduct;
  success: boolean;
  errorReasons?: string[];
  processType: string;
}

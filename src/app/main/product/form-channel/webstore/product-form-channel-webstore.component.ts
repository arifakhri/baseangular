import { Component, ElementRef, OnInit } from '@angular/core';

import { ProductFormChannelWebstoreBComponent } from '../product-form-channel-webstore.bcomponent';

@Component({
  selector: 'app-product-form-channel-webstore',
  templateUrl: './product-form-channel-webstore.component.html',
})
export class ProductFormChannelWebstoreComponent extends ProductFormChannelWebstoreBComponent implements OnInit {
  constructor(
    public _elementRef: ElementRef,
  ) {
    super();
  }

  ngOnInit() {
    this.componentId = `ProductFormChannelWebstore${this.account.channelAccountId}`;
  }
}

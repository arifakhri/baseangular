import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import * as _ from 'lodash';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';

import { MSalesChannelAccount } from '../../../sales-channel/sales-channel.model';
import { MSalesChannelProductSyncResult } from '../product-form-channel.model';
import { ProductFormChannelService } from '../product-form-channel.service';

@Component({
  selector: 'app-product-form-channel-progress',
  templateUrl: './product-form-channel-progress.component.html',
})
export class ProductFormChannelProgressComponent implements OnInit {
  @Input() productId: string;
  @Input() observableSubmit: Observable<MSalesChannelProductSyncResult>;
  @Input() accounts: MSalesChannelAccount[] = [];

  @Output() success: EventEmitter<boolean> = new EventEmitter;

  salesChannelProductsResult: MSalesChannelProductSyncResult[] = [];
  hasErrors: boolean = false;

  constructor(
    private _productFormChannel: ProductFormChannelService,
    private _router: Router,
  ) { }

  ngOnInit() {
    this.observableSubmit
      .finally(() => {
        this.checkSuccess();
      })
      .subscribe((result) => {
        this.salesChannelProductsResult.push(result);

        this.checkError();
      });
  }

  getSuccessResultByChannelId(channelAccountId: string) {
    const accountResult = this.getResultByChannelId(channelAccountId);

    if (accountResult) {
      return accountResult.response.data.success;
    }

    return null;
  }

  getResultByChannelId(channelAccountId: string) {
    return _.find(this.salesChannelProductsResult, { channelAccountId });
  }

  checkSuccess() {
    const errorsResult = this.getErrorsResult();
    if (!errorsResult.length) {
      this.hasErrors = false;

      this.success.emit(true);
    }
  }

  checkError() {
    const errorsResult = this.getErrorsResult();
    if (errorsResult.length) {
      this.hasErrors = true;
    }
  }

  getErrorsResult() {
    return _.filter(this.salesChannelProductsResult, { response: { data: { success: false } } });
  }

  doRetry(channelAccountId: string) {
    const errorResult = _.find(this.getErrorsResult(), { channelAccountId });
    const channelAcount = _.find(this.accounts, { channelAccountId });

    _.remove(this.salesChannelProductsResult, { channelAccountId });

    this._productFormChannel
      .syncProduct({
        channelAccountId: channelAcount.channelAccountId,
        channelAccountType: channelAcount.channelAccountType,
        clodeoProductId: this.productId,
      })
      .finally(() => {
        this.checkSuccess();
      })
      .subscribe(result => {
        this.salesChannelProductsResult.push(result);
      }, (result) => {
        this.salesChannelProductsResult.push(result);

        this.checkError();
      });
  }

  goEdit() {
    this._router.navigateByUrl('/empty', { skipLocationChange: true }).then(() => {
      this._router.navigate(['/products', this.productId, 'update']);
    });
  }
}

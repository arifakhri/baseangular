import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import * as _ from 'lodash';

@Injectable()
export class ProductFormChannelDynamicService {
  isFormValid(...dynamicFieldsComponents) {
    dynamicFieldsComponents = _.filter(dynamicFieldsComponents, _.identity);

    for (const dynamicFieldsComponent of dynamicFieldsComponents) {
      if (!dynamicFieldsComponent.formValid) {
        return false;
      }
    }

    return true;
  }

  getFormValue(dynamicFields, ...dynamicFieldsComponents) {
    let dynamicFieldsValues = [];

    _(dynamicFieldsComponents)
      .filter(_.identity)
      .forEach(dynamicFieldsComponent => {
        dynamicFieldsValues = [].concat(dynamicFieldsValues, dynamicFieldsComponent.formValue || []);
      });

    const allDynamicValuesParts = _.partition(dynamicFieldsValues, dynamicValue => {
      const dynamicField: any = _.find(dynamicFields, { name: dynamicValue.key });
      return dynamicField.field_target === 'product';
    });

    return {
      product: allDynamicValuesParts[0],
      variant: allDynamicValuesParts[1],
    };
  }

  mapFieldsValue(dynamicFieldsForm: FormGroup, dynamicFields, productForm: FormGroup, variantForm?: FormGroup) {
    const observables = [];

    _.forEach(dynamicFields, dynamicField => {
      if (dynamicField.map_field && dynamicField.map_field.map_type) {
        let targetForm: FormGroup;
        if (dynamicField.map_field.map_type === 'product') {
          targetForm = productForm;
        } else if (dynamicField.map_field.map_type === 'variant' && variantForm) {
          targetForm = variantForm;
        }
        if (targetForm) {
          const targetField = targetForm.get(dynamicField.map_field.map_to);
          if (targetField) {
            this.patchDynamicField(dynamicFieldsForm, dynamicField.name, targetField.value);

            observables.push(
              targetField.valueChanges.debounceTime(500).subscribe(value => {
                this.patchDynamicField(dynamicFieldsForm, dynamicField.name, value);
              })
            );
          }
        }
      }
    });

    return {
      dispose: () => observables.forEach(observable => observable.unsubcribe()),
    };
  }

  patchDynamicField(dynamicFieldsForm: FormGroup, dynamicFieldName: string, value: any) {
    const targetField = dynamicFieldsForm.get(dynamicFieldName);
    if (targetField) {
      targetField.setValue(value);
    }
  }
}

import { ChangeDetectorRef, Component, Input, OnChanges, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import * as _ from 'lodash';

import { CommonService } from '../../../../core/core.module';
import { FormgenComponent } from '../../../../shared/formgen/formgen.component';
import { MFormgenSchema, MFormgenSchemaField } from '../../../../shared/formgen/formgen.model';
import {
  MSalesChannelMarketplaceProduct,
  MSalesChannelMarketplaceProductDynamicField,
  MSalesChannelMarketplaceProductDynamicFieldValue,
} from '../../../sales-channel/sales-channel-marketplace.model';
import {
  MSalesChannelWebstoreProduct,
  MSalesChannelWebstoreProductDynamicField,
  MSalesChannelWebstoreProductDynamicFieldValue,
} from '../../../sales-channel/sales-channel-webstore.model';
import { ProductFormChannelService } from '../product-form-channel.service';
import { ProductFormChannelDynamicService } from './product-form-channel-dynamic.service';

@Component({
  selector: 'app-product-form-channel-dynamic',
  templateUrl: './product-form-channel-dynamic.component.html',
})
export class ProductFormChannelDynamicComponent implements OnChanges, OnInit {
  @Input() productForm: FormGroup;
  @Input() variantForm: FormGroup;
  @Input() productChannel: MSalesChannelWebstoreProduct | MSalesChannelMarketplaceProduct;
  @Input() fields: MSalesChannelMarketplaceProductDynamicField[] | MSalesChannelWebstoreProductDynamicField[] = [];
  @Input() showType: string;
  @Input() values: MSalesChannelMarketplaceProductDynamicFieldValue[] | MSalesChannelWebstoreProductDynamicFieldValue[] = [];
  @ViewChild('formgen') formgen: FormgenComponent;

  schema: MFormgenSchema;
  fieldsChunk: any = [];

  expanded: boolean = false;

  fieldsFiltered: MSalesChannelMarketplaceProductDynamicField[] | MSalesChannelWebstoreProductDynamicField[] = [];

  existingMapFieldsValue: any;

  constructor(
    private _changeDetectorRef: ChangeDetectorRef,
    private _productFormChannel: ProductFormChannelService,
    private _productformChannelDynamic: ProductFormChannelDynamicService,
  ) { }

  get formValue() {
    return this._productFormChannel.normalizeDynamicFieldsValue(this.formgen.form.value, this.fields);
  }

  get formValid() {
    if (!this.formgen.form.valid) {
      CommonService.markAsDirty(this.formgen.form);
      return false;
    }
    return true;
  }

  ngOnChanges(changes: SimpleChanges) {
    if (_.has(changes, 'fields') && this.fields) {
      this.filterFields();

      this.populateSchema();
    }
  }

  ngOnInit() {
    this.patchDoc();
  }

  patchDoc() {
    // this will fail if product & variant has some same named fields
    if (this.values && this.values.length) {
      this.formgen.patchValue(this.convertDynamicFieldsValueForForm(this.values));
    }
  }

  populateSchema() {
    this.schema = {
      customLayout: true,
      fields: _.filter(
        _.map<any, MFormgenSchemaField | null>(
          this.fieldsFiltered, field => {
            const convertedField = this.convertDynamicFieldToFormgenField(field);
            if (convertedField.field) {
              return convertedField;
            } else {
              return null;
            }
          }
        ),
        _.identity,
      ),
    };

    this.fieldsChunk = _.chunk(_.map(_.values(this.schema.fields), field => field.name), 2);

    this._changeDetectorRef.detectChanges();

    if (this.existingMapFieldsValue) {
      this.existingMapFieldsValue.dispose();
    }
    this.existingMapFieldsValue = this._productformChannelDynamic.mapFieldsValue(this.formgen.form, this.fields, this.productForm, this.variantForm);
  }

  convertDynamicFieldToFormgenField(field: MSalesChannelMarketplaceProductDynamicField | MSalesChannelWebstoreProductDynamicField): MFormgenSchemaField {
    const convertedField: MFormgenSchemaField = {
      name: field.name,
      label: field.label,
      validations: _.chain(field.validations)
        .keyBy('key')
        .mapValues('value')
        .value(),
      templateConfig: {
        options: field.options || [],
        dropdown: true,
        acField: 'value',
        acKey: 'key',
        acLocal: true,
        acLocalFilterBy: ['value'],
        manualInput: true,
        multiple: false,
      },
    };

    switch (field.input_type) {
      case 'select':
        convertedField.field = 'autocomplete';
        break;
      case 'select_multiple':
        convertedField.field = 'autocomplete';
        convertedField.templateConfig.multiple = true;
        break;
      case 'date':
        convertedField.field = 'date';
        convertedField.templateConfig.dateFormat = 'dd/mm/yy';
        break;
      case 'text':
        convertedField.field = 'text';
        break;
      case 'numeric':
        convertedField.field = 'text';
        convertedField.templateConfig.type = 'number';
        break;
      case 'textarea_html':
        convertedField.field = 'textarea_html';
        break;
      default:
        convertedField.field = 'text';
        break;
    }

    if (field.provider && field.provider.autocomplete_url) {
      convertedField.templateConfig.acLocal = false;
      convertedField.templateConfig.acRemoteUrl = field.provider.autocomplete_url;
      convertedField.templateConfig.acRemoteParamKey = field.provider.autocomplete_param_key;
      convertedField.templateConfig.acRemoteLoadFirst = true;

      if (field.provider.autocomplete_label_key) {
        convertedField.templateConfig.acField = field.provider.autocomplete_label_key;
      }

      if (field.provider.autocomplete_value_key) {
        convertedField.templateConfig.acKey = field.provider.autocomplete_value_key;
      }
    }

    return convertedField;
  }

  convertDynamicFieldsValueForForm(dynamicFieldsValue: MSalesChannelWebstoreProductDynamicFieldValue[] | MSalesChannelMarketplaceProductDynamicFieldValue[]) {
    const formValue: any = {};

    _.forEach(dynamicFieldsValue, fieldValue => {
      const targetField = _.find(this.fields, { name: fieldValue.key });
      if (targetField) {
        if (targetField.input_type === 'select_multiple') {
          if (fieldValue.value === null) {
            fieldValue.value = '';
          }
          formValue[fieldValue.key] = _.map(fieldValue.value.toString().split(','), val => ({ key: val, value: val }));
        } else if (targetField.input_type === 'select' && !(targetField.provider && targetField.provider.autocomplete_url)) {
          formValue[fieldValue.key] = {
            key: fieldValue.value,
            value: fieldValue.value,
          };
        } else {
          try {
            formValue[fieldValue.key] = JSON.parse(fieldValue.value);
          } catch (e) {
            formValue[fieldValue.key] = fieldValue.value;
          }
        }
      }
    });

    return formValue;
  }

  getTemplatesChunk(templates: { [key: string]: any }) {
    const templatesArr = _.map(templates, (template, templateKey) => {
      return {
        key: templateKey,
        ...template,
      };
    });

    return _.chunk(templatesArr, 2);
  }

  filterFields() {
    const requiredFilterCriteria = field => _.filter(field.validations, validation => validation.key === 'required' && validation.value).length;
    const nonRequiredFilterCriteria = field => _.filter(field.validations, validation => validation.key === 'required' && !validation.value).length;
    switch (this.showType) {
      case 'required':
        this.fieldsFiltered = _.filter(this.fields, requiredFilterCriteria);
        break;
      case 'non_required':
        this.fieldsFiltered = _.filter(this.fields, nonRequiredFilterCriteria);
        break;
    }
  }
}

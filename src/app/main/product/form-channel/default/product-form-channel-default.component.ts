import { AfterViewInit, ChangeDetectorRef, Component, Input, OnChanges, OnInit, SimpleChanges, ViewChild, } from '@angular/core';
import { FormGroup } from '@angular/forms';
import * as _ from 'lodash';

import { APP_CONST } from '../../../../app.const';
import { CommonService, EventService } from '../../../../core/core.module';
import { FormgenComponent } from '../../../../shared/formgen/formgen.component';
import { MFormgenSchema } from '../../../../shared/formgen/formgen.model';
import { MSalesChannelMarketplaceProduct } from '../../../sales-channel/sales-channel-marketplace.model';
import { MSalesChannelWebstoreProduct, MSalesChannelWebstoreProductCategory, } from '../../../sales-channel/sales-channel-webstore.model';

@Component({
  selector: 'app-product-form-channel-default',
  templateUrl: './product-form-channel-default.component.html',
})
export class ProductFormChannelDefaultComponent implements AfterViewInit, OnChanges, OnInit {
  @Input() channelAccountType: string;
  @Input() channelId: string;
  @Input() priceLevelId: string;
  @Input() productChannel: MSalesChannelWebstoreProduct | MSalesChannelMarketplaceProduct;
  @Input() productForm: FormGroup;
  @ViewChild('formgen') formgen: FormgenComponent;

  @Input() categories: MSalesChannelWebstoreProductCategory[] = [];

  schema: MFormgenSchema;

  get formValue() {
    return this.formgen.form.value;
  }

  get formValid() {
    if (!this.formgen.form.valid) {
      CommonService.markAsDirty(this.formgen.form);
      return false;
    }
    return true;
  }

  constructor(
    private _changeDetectorRef: ChangeDetectorRef,
    private _event: EventService,
  ) { }

  ngOnInit() {
    this.schema = {
      customLayout: true,
      fields: [{
        name: 'clodeo_product_id',
        ignore: true,
      }, {
        name: 'channel_product_id',
        ignore: true,
      }, {
        name: 'category',
        field: 'autocomplete',
        label: 'Category',
        templateConfig: {
          dropdown: true,
          options: [],
          acField: 'name',
          acKey: 'id',
          acLocal: this.channelAccountType === 'webstore',
          acLocalFilterBy: ['name'],
          manualInput: false,
          acRemoteParamKey: 'Search',
          acRemoteUrl: `${APP_CONST.API_SALESCHANNEL}/${this.channelAccountType}/${this.channelId}/product-categories/findAll`,
        },
      }, {
        name: 'name',
        field: 'text',
        label: 'Name',
      }, {
        name: 'sku',
        field: 'text',
        label: 'SKU',
      }, {
        name: 'price',
        field: 'text',
        label: 'Price',
        templateConfig: {
          type: 'number',
        },
      }, {
        name: 'description',
        field: 'textarea_html',
        label: 'Description',
        validations: {
          required: true,
        },
      }],
    };

    this._changeDetectorRef.detectChanges();

    this._event.listen(`PRODUCT:CHANNEL:${this.channelId}:VARIANTSCHANGE`).subscribe(event => {
      this.formgen.form.patchValue({ sku: _.get(event.data, '0.sku') });
      this.formgen.form.patchValue({ price: _.get(event.data, '0.price') });
    });
  }

  ngAfterViewInit() {
    if (!this.productChannel) {
      this.formgen.form.patchValue({
        name: this.productForm.get('name').value,
        sku: this.productForm.get('variants.0').value.sku,
        price: this.getProductPrice(this.productForm.get('variants.0').value),
        description: this.productForm.get('detailDescription').value,
      });
    } else {
      this.applyDoc();
    }

    this.watchMainProductChanges();
  }

  getProductPrice(masterVariantValue: any) {
    let targetPrice = _.find(masterVariantValue.prices, { isMaster: true });
    if (this.priceLevelId) {
      targetPrice = _.find(masterVariantValue.prices, { priceLevelId: this.priceLevelId });
    }
    return _.get(targetPrice, 'unitPrice');
  }

  getProductPriceIndex(masterVariantValue: any) {
    let targetPriceIndex = _.findIndex(masterVariantValue.prices, { isMaster: true });
    if (this.priceLevelId) {
      targetPriceIndex = _.findIndex(masterVariantValue.prices, { priceLevelId: this.priceLevelId });
    }
    return targetPriceIndex;
  }

  watchMainProductChanges() {
    this.productForm.get('name').valueChanges.debounceTime(500).subscribe(value => {
      this.formgen.form.patchValue({ name: value });
    });

    this.productForm.get('detailDescription').valueChanges.debounceTime(500).subscribe(value => {
      this.formgen.form.patchValue({ description: value });
    });

    this.productForm.get('variants.0.sku').valueChanges.debounceTime(500).subscribe(value => {
      this.formgen.form.patchValue({ sku: value });
    });

    const masterVariantPriceIndex = this.getProductPriceIndex(this.productForm.get('variants.0').value);
    this.productForm.get(`variants.0.prices.${masterVariantPriceIndex}.unitPrice`).valueChanges.debounceTime(500).subscribe(value => {
      this.formgen.form.patchValue({ price: value });
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (_.has(changes, 'categories') && this.categories && this.categories.length) {
      const categoryField = _.find(this.schema.fields, field => field.name === 'category');
      categoryField.templateConfig.options = this.categories;
    }
  }

  applyDoc() {
    const productChannel: any = this.productChannel || {};
    this.formgen.patchValue({
      ...productChannel,
      sku: _.get(this.productChannel, 'variants.0.sku'),
      price: _.get(this.productChannel, `variants.0.price`),
    });
  }
}

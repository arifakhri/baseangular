import * as _ from 'lodash';
import { Component, Input, OnChanges, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import 'app/rxjs-imports.ts';

import { RetryDialogComponent } from '../../../shared/retry-dialog/retry-dialog.component';

import { ProductRestService } from '../product-rest.service';

import { MProduct } from '../product.model';

@Component({
  selector: 'app-product-form-variant',
  templateUrl: './product-form-variant.component.html',
})
export class ProductFormVariantComponent implements OnChanges, OnInit {
  @Input() doc: MProduct;
  @Input() productVariant: IProductVariant;
  @Input() form: FormGroup;
  @ViewChild('retryDialog') elRetryDialog: RetryDialogComponent;

  priceLevels: IPriceLevel[] = [];
  uoms: IUOM[] = [];

  retryDialogShownState: boolean = false;
  retryDialogOnRetry: Function;

  constructor(
    private _productRest: ProductRestService,
  ) {
  }

  ngOnInit() {
    this.loadRelatedData();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (_.has(changes, 'doc') && this.productVariant) {
      this.form.patchValue(this.productVariant);
    }
  }

  loadRelatedData() {
    this.elRetryDialog.createRetryEntry(this._productRest.loadRelatedData())
      .subscribe(related => {
        this.priceLevels = related.priceLevels;
        this.uoms = related.uoms;
      });
  }
}

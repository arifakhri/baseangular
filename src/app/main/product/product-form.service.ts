import * as _ from 'lodash';
import { ConditionallyValidateService } from 'ng-conditionally-validate';
import { CustomValidators } from 'ng2-validation';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';
import { TranslateService } from '@ngx-translate/core';

import { SettingsApplicatorService } from '../settings/settings-applicator.service';
import { MPriceLevel } from '../price-level/price-level.model';

@Injectable()
export class ProductFormService {
  constructor(
    private _conditionallyValidate: ConditionallyValidateService,
    private _formBuilder: FormBuilder,
    private _settingsApplicator: SettingsApplicatorService,
    private _translate: TranslateService,
  ) { }

  setFormDefinitions(form: FormGroup, productType: string = '') {
    form.addControl('_identifier', new FormControl);

    const nameControl = new FormControl('', [Validators.required, Validators.maxLength(150)]);
    (<any>nameControl).validatorData = {
      maxLength: 150
    };

    form.addControl('productType', new FormControl(productType));

    form.addControl('name', nameControl);
    form.addControl('model', new FormControl);
    form.addControl('brand', new FormControl);
    form.addControl('brandId', new FormControl(null));
    form.addControl('pictureId', new FormControl(null));
    form.addControl('pictures', new FormControl([]));

    form.addControl('detailDescription', new FormControl(''));

    form.addControl('category', new FormControl());
    form.addControl('categoryId', new FormControl(null));

    form.addControl('tags', new FormControl(''));

    form.addControl('inventoryAccount', new FormControl(''));
    form.addControl('inventoryAccountId', new FormControl(null));
    form.addControl('obQty', new FormControl(null));
    form.addControl('obDate', new FormControl(new Date()));

    form.addControl('multipleUOM', new FormControl(false));
    form.addControl('uom', new FormControl(''));
    form.addControl('uomId', new FormControl(null, Validators.required));
    form.addControl('uom2', new FormControl());
    form.addControl('uom2Id', new FormControl(null));
    form.addControl('uom2Conversion', new FormControl(null, CustomValidators.number));
    form.addControl('uom3', new FormControl());
    form.addControl('uom3Id', new FormControl(null));
    form.addControl('uom3Conversion', new FormControl(null, CustomValidators.number));
    form.addControl('defaultPurchaseUom', new FormControl(null, CustomValidators.number));
    form.addControl('defaultSalesUom', new FormControl(null, CustomValidators.number));

    form.addControl('sellable', new FormControl(true));
    form.addControl('salesTax', new FormControl(''));
    form.addControl('salesTaxId', new FormControl(null));
    form.addControl('salesDescription', new FormControl(''));
    form.addControl('incomeAccount', new FormControl(''));
    form.addControl('incomeAccountId', new FormControl(null));
    form.addControl('autoReserveInventoryOnSalesOrder', new FormControl(false));

    form.addControl('purchasable', new FormControl(true));
    form.addControl('purchaseTax', new FormControl(''));
    form.addControl('purchaseTaxId', new FormControl(null));
    form.addControl('purchaseDescription', new FormControl(''));
    form.addControl('expenseAccount', new FormControl(''));
    form.addControl('expenseAccountId', new FormControl(null));

    form.addControl('attributes', new FormArray([]));
    form.addControl('variantCount', new FormControl(0));
    form.addControl('variantAttribute1', new FormControl());
    form.addControl('variantAttribute1Id', new FormControl(null));
    form.addControl('variantAttribute2', new FormControl());
    form.addControl('variantAttribute2Id', new FormControl(null));
    form.addControl('variantAttribute3', new FormControl());
    form.addControl('variantAttribute3Id', new FormControl(null));

    const hasVariantsControl = new FormControl(false);
    form.addControl('hasVariants', hasVariantsControl);
    hasVariantsControl.valueChanges.subscribe((hasVariants) => {
      const formAttributes = <FormArray>form.get('attributes');
      const formAttributesControls = formAttributes.controls;
      _.forEach(formAttributesControls, formAttributesControl => {
        if (hasVariants) {
          this.setFormChildAttributeValidation(<FormGroup>formAttributesControl);
        } else {
          this.resetFormChildAttributeValidation(<FormGroup>formAttributesControl);
        }
      });

      const formVariants = <FormArray>form.get('variants');
      formVariants.valueChanges.subscribe(variants => {
        if (
          _.get(variants, '0.attribute1Value') ||
          _.get(variants, '0.attribute2Value') ||
          _.get(variants, '0.attribute3Value')
        ) {
          form.get('variantCount').setValue(variants.length);
        }
      });
      const formVariantsControls = formVariants.controls;
      _.forEach(formVariantsControls, formVariantsControl => {
        if (hasVariants) {
          this.setFormChildVariantValidation(<FormGroup>formVariantsControl);
        } else {
          this.resetFormChildVariantValidation(<FormGroup>formVariantsControl);
        }
      });
    });

    form.addControl('variants', new FormArray([
      this.buildFormChildVariant({
        isMaster: true,
      })
    ]));

    form.addControl('dimension', new FormGroup({
      weight: new FormControl(null, CustomValidators.number),
      height: new FormControl(null, CustomValidators.number),
      width: new FormControl(null, CustomValidators.number),
      length: new FormControl(null, CustomValidators.number)
    }));
  }

  buildFormChildAttribute() {
    const formGroup = new FormGroup({
      id: new FormControl(null, Validators.required),
      values: new FormControl([]),
    });

    this.setFormChildAttributeValidation(formGroup);

    return formGroup;
  }

  setFormChildAttributeValidation(formGroup: FormGroup) {
    formGroup.get('id').setValidators([Validators.required]);
    formGroup.get('id').updateValueAndValidity();
  }

  resetFormChildAttributeValidation(formGroup: FormGroup) {
    formGroup.get('id').setValidators([]);
    formGroup.get('id').updateValueAndValidity();
  }

  addFormChildAttribute(formGroup: FormGroup) {
    const formGroupAttribute = this.buildFormChildAttribute();
    (<FormArray>formGroup.get('attributes')).push(formGroupAttribute);

    return formGroupAttribute;
  }

  removeFormChildAttribute(formGroup: FormGroup, idx: number) {
    (<FormArray>formGroup.get('attributes')).removeAt(idx);
  }

  buildFormChildVariant(defaultValue: any = {}): FormGroup {
    const formGroup = this._formBuilder.group({
      id: new FormControl(null),
      sku: new FormControl(''),
      attribute1Value: new FormControl(''),
      attribute2Value: new FormControl(''),
      attribute3Value: new FormControl(''),
      prices: new FormArray([]),
      unitCost: new FormControl(0),
      obQty: new FormControl(null),
      obDate: new FormControl(new Date()),
      isMaster: false
    });

    this.setFormChildVariantValidation(formGroup);

    formGroup.patchValue(defaultValue);

    return formGroup;
  }

  setFormChildVariantValidations(formGroupIdx: number, productForm: FormGroup) {
    this._conditionallyValidate.validate(productForm, `variants.${formGroupIdx}.attribute1Value`)
      .using(Validators.required)
      .when('variantAttribute1Id')
      .isNot(null);

    this._conditionallyValidate.validate(productForm, `variants.${formGroupIdx}.attribute2Value`)
      .using(Validators.required)
      .when('variantAttribute2Id')
      .isNot(null);

    this._conditionallyValidate.validate(productForm, `variants.${formGroupIdx}.attribute3Value`)
      .using(Validators.required)
      .when('variantAttribute3Id')
      .isNot(null);
  }

  buildFormChildVariantPrice(priceLevelId: string, isMaster: boolean = false) {
    return new FormGroup({
      priceLevelId: new FormControl(priceLevelId),
      isMaster: new FormControl(isMaster),
      unitPrice: new FormControl(),
      unitPrice2: new FormControl(),
      unitPrice3: new FormControl(),
    });
  }

  syncFormChildVariantPrices(parentForm: FormGroup, priceLevels: MPriceLevel[]) {
    const lastFormArrayValues = parentForm.get('prices').value;

    const newFormArray = new FormArray([]);
    priceLevels.forEach(priceLevel => {
      const formArrayItem = this.buildFormChildVariantPrice(priceLevel.id, priceLevel.isMaster);

      const lastFormArrayValue = _.find(lastFormArrayValues, { priceLevelId: priceLevel.id });
      if (lastFormArrayValue) {
        formArrayItem.patchValue(_.pick(lastFormArrayValue, ['unitPrice', 'unitPrice2', 'unitPrice3']));
      }

      newFormArray.push(formArrayItem);
    });

    parentForm.setControl('prices', newFormArray);
  }

  setFormChildVariantValidation(formGroup: FormGroup) {
    const skuControl = formGroup.get('sku');
    skuControl.setValidators([Validators.maxLength(80)]);
    (<any>skuControl).validatorData = {
      maxLength: 80,
    };
    skuControl.updateValueAndValidity();

    const attribute1ValueControl = formGroup.get('attribute1Value');
    attribute1ValueControl.setValidators([Validators.maxLength(80)]);
    (<any>attribute1ValueControl).validatorData = {
      maxLength: 80,
    };
    attribute1ValueControl.updateValueAndValidity();

    const attribute2ValueControl = formGroup.get('attribute2Value');
    attribute2ValueControl.setValidators([Validators.maxLength(80)]);
    (<any>attribute2ValueControl).validatorData = {
      maxLength: 80,
    };
    attribute2ValueControl.updateValueAndValidity();

    const attribute3ValueControl = formGroup.get('attribute3Value');
    attribute3ValueControl.setValidators([Validators.maxLength(30)]);
    (<any>attribute3ValueControl).validatorData = {
      maxLength: 30,
    };
    attribute3ValueControl.updateValueAndValidity();

    const unitCostControl = formGroup.get('unitCost');
    unitCostControl.setValidators([Validators.required, CustomValidators.number]);
    unitCostControl.updateValueAndValidity();
  }

  resetFormChildVariantValidation(formGroup: FormGroup) {
    const skuControl = formGroup.get('sku');
    skuControl.setValidators([]);
    skuControl.updateValueAndValidity();

    const attribute1ValueControl = formGroup.get('attribute1Value');
    attribute1ValueControl.setValidators([]);
    attribute1ValueControl.updateValueAndValidity();

    const attribute2ValueControl = formGroup.get('attribute2Value');
    attribute2ValueControl.setValidators([]);
    attribute2ValueControl.updateValueAndValidity();

    const attribute3ValueControl = formGroup.get('attribute3Value');
    attribute3ValueControl.setValidators([]);
    attribute3ValueControl.updateValueAndValidity();

    const unitCostControl = formGroup.get('unitCost');
    unitCostControl.setValidators([]);
    unitCostControl.updateValueAndValidity();
  }

  getFormNavigations(type: string): Observable<IFormNav[]> {
    return Observable.race(
      this._translate.getTranslation(this._translate.currentLang),
      <any>this._translate.onLangChange,
    ).map(() => {
      switch (type) {
        case 'inventory':
          return [{
            name: this._translate.instant('ui.product.form.productDetails'),
            target: '#fieldset-1'
          }, {
            name: this._translate.instant('ui.product.form.salesInfo'),
            target: '#fieldset-sales-info'
          }, {
            name: this._translate.instant('ui.product.form.purchasesInfo'),
            target: '#fieldset-purchases-info'
          }, {
            name: this._translate.instant('ui.product.form.variants'),
            target: '#fieldset-variants'
          }, {
            name: this._translate.instant('ui.product.form.detailDescriptionInfo'),
            target: '#fieldset-detail-description-info'
          }, {
            name: this._translate.instant('ui.product.form.otherDetails'),
            target: '#fieldset-other-details'
          }];
        case 'non_inventory':
          return [{
            name: this._translate.instant('ui.product.form.productDetails'),
            target: '#fieldset-1'
          }, {
            name: this._translate.instant('ui.product.form.salesInfo'),
            target: '#fieldset-sales-info'
          }, {
            name: this._translate.instant('ui.product.form.purchasesInfo'),
            target: '#fieldset-purchases-info'
          }];
        case 'service':
          return [{
            name: this._translate.instant('ui.product.form.serviceDetails'),
            target: '#fieldset-1'
          }, {
            name: this._translate.instant('ui.product.form.salesInfo'),
            target: '#fieldset-sales-info'
          }, {
            name: this._translate.instant('ui.product.form.purchasesInfo'),
            target: '#fieldset-purchases-info'
          }];
      }
    });
  }

  applySettings(form: FormGroup, relatedData: any) {
    return this._settingsApplicator.applyToForm(relatedData.settings, form, {
      'accounts': {
        [form.controls.productType.value === 'inventory' ? 'cogsAccountId' : 'purchaseExpenseAccountId']: 'expenseAccountId',
        'salesIncomeAccountId': 'incomeAccountId',
      },
      'purchases': {
        'defaultPurchaseTaxId': 'purchaseTaxId',
      },
      'sales': {
        'defaultSalesTaxId': 'salesTaxId',
        'autoReserveInventoryOnSalesOrder': 'autoReserveInventoryOnSalesOrder',
      },
      'product': {
        'defaultUomId': 'uomId',
      },
    }, {
        accounts: relatedData.accounts,
        purchases: relatedData.taxes,
        sales: relatedData.taxes,
        product: relatedData.uoms,
      });
  }
}

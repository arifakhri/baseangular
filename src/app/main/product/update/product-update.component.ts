import { Component, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { Router } from '@angular/router';
import * as _ from 'lodash';
import { ModalDirective } from 'ngx-bootstrap';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';
import swal from 'sweetalert2';

import { CommonService, UserVariableService } from '../../../core/core.module';
import { BaseUpdateBComponent } from '../../../shared/base/base.module';
import { MSalesChannelProductSyncResult } from '../form-channel/product-form-channel.model';
import { ProductFormChannelService } from '../form-channel/product-form-channel.service';
import { ProductFormBaseComponent } from '../form/base/product-form-base.component';
import { ProductFormService } from '../product-form.service';
import { ProductRestService } from '../product-rest.service';
import { MProduct } from '../product.model';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-product-update',
  templateUrl: 'product-update.component.html',
})
export class ProductUpdateComponent extends BaseUpdateBComponent {
  @ViewChild('submitProductChannelsModal') elSubmitProductChannelsModal: ModalDirective;
  @ViewChildren('productFormBase') elProductFormBase: QueryList<ProductFormBaseComponent>;

  fullView: boolean = false;
  fullViewState: IAppVariable = this._userVariable.get('simpledetailstate.product', {
    value: false,
  });
  formNavigations: IFormNav[] = [];

  observableSubmitChannelProducts: Observable<MSalesChannelProductSyncResult>;

  get salesChannelSelectedAccounts() {
    if (this.elProductFormBase.first && this.elProductFormBase.first.elFormChannel.first) {
      return this.elProductFormBase.first.elFormChannel.first.selectedAccounts.map(selectedAccount => selectedAccount.account);
    }
    return [];
  }

  constructor(
    private _userVariable: UserVariableService,
    private _product: ProductService,
    private _productForm: ProductFormService,
    private _productFormChannel: ProductFormChannelService,
    private _productRest: ProductRestService,
    private _router: Router,
  ) {
    super();

    this.componentId = 'ProductUpdate';
    this.headerTitle = 'ui.product.update.title';
    this.routeURL = '/products';
    this.entrySuccessI18n = 'success.product.update';

    this.registerHook('buildForm', event => {
      this._productForm.setFormDefinitions(this.form, '');

      this.fullView = this.fullViewState.value;
    });

    this.registerHook('load', event => {
      return this._productRest.load(this.page.routeParams.id, {
        includeVariants: true,
        includeVariantPrices: true,
        includePictures: true,
        includeTags: true,
        includeSalesChannels: true,
      }).switchMap(doc => {
        if (doc.pictures && doc.pictures.length) {
          doc.pictures = _.sortBy(doc.pictures, ['sortOrder']) as any;
        }
        return Observable.of(doc);
      });
    }, (product) => {

      this.form.get('productType').setValue(product.productType);

      if (product.variantCount > 1) {
        this.form.get('hasVariants').setValue(true);
      }

      const variants = product.variants;
      if (
        _.get(variants, '0.attribute1Value') ||
        _.get(variants, '0.attribute2Value') ||
        _.get(variants, '0.attribute3Value')
      ) {
        this.form.get('variantCount').setValue(variants.length);
      } else {
        this.form.get('variantCount').setValue(0);
      }

      this.saveAndNewURL = `${this.routeURL}/create-${product.productType}`;

      this._productForm.getFormNavigations(this.doc.productType).subscribe(formNavigations => this.formNavigations = formNavigations);
    });

    this.registerHook('submitPreprocess', event => {
      return this._productRest.checkDuplicate({ name: this.form.value.name, id: this.doc.id }).switchMap(exist => {
        if (exist) {
          return swal({
            title: this.page._translate.instant('confirm.product.exist.label'),
            text: this.page._translate.instant('confirm.product.exist.description'),
            type: 'warning',
            showCancelButton: true
          });
        } else {
          return Observable.of(exist);
        }
      });
    });

    this.registerHook('save', event => {
      const product: MProduct = Object.assign({}, this.doc);
      this._product.normalizeDoc(product);

      _.mergeWith(product, this.form.value, (leftValue, rightValue, objKey) => {
        if (objKey === 'variants') {
          return _.map(rightValue, (rightValueObj, rightValueIdx) => {
            const leftValueToMerge = leftValue[rightValueIdx];
            if (leftValueToMerge) {
              rightValueObj = Object.assign({}, leftValueToMerge, rightValueObj);
            }
            return rightValueObj;
          });
        } else if (objKey === 'pictures') {
          return rightValue;
        }
      });

      if (this.salesChannelSelectedAccounts.length) {
        const channelProductsMetadata = this.elProductFormBase.first.elFormChannel.first.getChannelProductsMetadata(product);
        channelProductsMetadata.forEach(channelProductMetadata => {
          this._productFormChannel.attachToProduct(product, channelProductMetadata);
        });
      }

      return this._productRest.update(this.page.routeParams.id, product, { includeSalesChannels: true, includeVariants: true })
        .do(result => {
          this.doc = result;

          if (result.salesChannels.length) {
            this.observableSubmitChannelProducts = Observable.merge(
              this._productFormChannel.syncMultipleProducts(
                result.salesChannels.map(productSalesChannel => ({
                  channelAccountId: productSalesChannel.channelAccountId,
                  channelAccountType: productSalesChannel.channelAccountType,
                  clodeoProductId: result.id,
                }))
              )
            );

            this.saveAndRedirectEnabled = false;

            this.elSubmitProductChannelsModal.show();
          } else {
            this.saveAndRedirectEnabled = true;
          }
        });
    });
  }

  onFullViewStateChange() {
    this.fullViewState.value = this.fullView;
    this._userVariable.update();
  }

  formValid(): boolean {
    if (
      (this.elProductFormBase.first.elFormChannel.first && !this.elProductFormBase.first.elFormChannel.first.formValid) ||
      !this.form.valid ||
      (
        this.form.value.hasVariants &&
        !this.form.value.variants.length
      )
    ) {
      CommonService.markAsDirty(this.form);

      const errorMessages = [
        this.page._translate.instant('error.form')
      ];

      if (this.form.value.hasVariants && !this.form.value.variants.length) {
        errorMessages.push(this.page._translate.instant('error.product.variants.none'));
      }

      this.page._systemMessage.log({
        message: errorMessages,
        type: 'error'
      });
      return false;
    }
    return true;
  }

  onProductChannelsSubmitted() {
    this.page._globalSystemMessage.log({
      message: 'success.product.productChannelsUpdate',
      type: 'success',
      showAs: 'growl',
      showSnackBar: false,
    });

    this.redirectAfterSave(this.doc.id);
  }

  onSalesChannelProgressClose() {
    this.redirectAfterSave(this.doc.id);
  }
}

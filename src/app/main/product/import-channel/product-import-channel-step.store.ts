export const ACTIONS = {
  NEXT: 'ProductImportChannelStepNext',
  PREV: 'ProductImportChannelStepPrev',
  RESET: 'ProductImportChannelStepReset',
};

export const STORE_ID = 'ProductImportChannelStepStore';

export function STORE(step: number = 1, action: any) {
  switch (action.type) {
    case ACTIONS.NEXT:
      return step + 1;
    case ACTIONS.PREV:
      return step - 1;
    case ACTIONS.RESET:
      return 1;
    default:
      return step;
  }
}

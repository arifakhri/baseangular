import * as _ from 'lodash';
import { Injectable } from '@angular/core';

@Injectable()
export class ProductImportChannelService {
  importMap: IImportDataMap = {
    channel_product_id: {},
    attribute1Search: {
      identifier: 'attribute1',
    },
    attribute2Search: {
      identifier: 'attribute2',
    },
    attribute3Search: {
      identifier: 'attribute3',
    },
    sku: {},
    name: {},
    images: {},
    categorySearch: {
      identifier: 'category.name',
    },
    salesDescription: {
      identifier: 'description',
    },
    rawVariants: {
      identifier: 'variants',
    },
  };

  normalizeProducts(channelProducts) {
    this.markRecordsSeqNumber(channelProducts);

    return _.map<any>(channelProducts, (channelProduct, channelProductIdx) => {
      channelProduct.productType = 'inventory';

      channelProduct.hasVariants = false;
      if (
        channelProduct.rawVariants.length &&
        (
          channelProduct.rawVariants.length > 1 ||
          (channelProduct.rawVariants[0].attribute1_value || channelProduct.rawVariants[0].attribute2_value || channelProduct.rawVariants[0].attribute3_value || channelProduct.rawVariants[0].sku)
        )
      ) {
        channelProduct.hasVariants = true;
        channelProduct.variantCount = channelProduct.rawVariants.length;
      }

      _.forEach(channelProduct.rawVariants, (channelProductRawVariant, channelProductRawVariantIdx) => {
        _.set(channelProduct, `variants[${channelProductRawVariantIdx}].sku`, channelProductRawVariant.sku);
        _.set(channelProduct, `variants[${channelProductRawVariantIdx}].price`, channelProductRawVariant.price);
        _.set(channelProduct, `variants[${channelProductRawVariantIdx}].attribute1Value`, channelProductRawVariant.attribute1_value);
        _.set(channelProduct, `variants[${channelProductRawVariantIdx}].attribute2Value`, channelProductRawVariant.attribute2_value);
        _.set(channelProduct, `variants[${channelProductRawVariantIdx}].attribute3Value`, channelProductRawVariant.attribute3_value);
      });

      return channelProduct;
    });
  }

  markRecordsSeqNumber(channelProducts) {
    return _.map<any>(channelProducts, (channelProduct, channelProductIdx) => {
      channelProduct.seqNumber = channelProductIdx;

      return channelProduct;
    });
  }
}

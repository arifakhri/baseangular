import { AfterViewInit, Component, OnDestroy, ViewChild } from '@angular/core';
import { Store } from '@ngrx/store';
import * as _ from 'lodash';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';

import { BasePageBComponent } from '../../../shared/base/base.module';
import { SalesChannelRestService } from '../../sales-channel/sales-channel-rest.service';
import { MSalesChannelAccount } from '../../sales-channel/sales-channel.model';
import { ProductImportMapRelatedComponent } from '../import/map-related/product-import-map-related.component';
import { ProductRestService } from '../product-rest.service';
import { MProductRelatedData } from '../product.model';
import { ProductImportChannelEditorComponent } from './editor/product-import-channel-editor.component';
import { ProductImportChannelFetchComponent } from './fetch/product-import-channel-fetch.component';
import * as ProductImportChannelStateStore from './product-import-channel-state.store';
import * as ProductImportChannelStepStore from './product-import-channel-step.store';

@Component({
  selector: 'app-product-import-channel',
  templateUrl: './product-import-channel.component.html',
})
export class ProductImportChannelComponent extends BasePageBComponent implements OnDestroy {
  @ViewChild('productImportChannelFetch') elProductImportChannelFetch: ProductImportChannelFetchComponent;
  @ViewChild('productImportMapRelated') elProductImportMapRelated: ProductImportMapRelatedComponent;
  @ViewChild('productImportChannelEditor') elProductImportChannelEditor: ProductImportChannelEditorComponent;

  formStep$: Observable<number>;
  formState$: Observable<any>;

  formStep: number;
  formState: any;

  maxStep = 3;
  relatedData: MProductRelatedData;

  salesChannelAccount: MSalesChannelAccount;

  constructor(
    private _productRest: ProductRestService,
    private _salesChannelRest: SalesChannelRestService,
    private _store: Store<any>,
  ) {
    super();

    this.componentId = 'ProductImportChannel';
    this.containerType = 1;
    this.headerTitle = 'ui.product.importChannel.title';

    this.formStep$ = _store.select(ProductImportChannelStepStore.STORE_ID) as any;
    this.formState$ = _store.select(ProductImportChannelStateStore.STORE_ID) as any;

    this.formStep$.subscribe(formStep => this.formStep = formStep);
    this.formState$.subscribe(formState => {
      return this.formState = formState;
    });
  }

  onPageInit() {
    const loading = this.page.createLoading();

    this.page.createRetriableTask(
      Observable.zip(
        this.loadAccount(),
        this.loadRelatedData(),
      ).finally(() => {
        loading.dispose();

        this.formNext();
      })
    ).subscribe();
  }

  ngOnDestroy() {
    this._store.dispatch({ type: ProductImportChannelStepStore.ACTIONS.RESET });
    this._store.dispatch({ type: ProductImportChannelStateStore.ACTIONS.RESET });
  }

  loadAccount() {
    return this._salesChannelRest.loadAccount(this.page.routeParams.channelAccountId)
      .do(salesChannelAccount => this.salesChannelAccount = salesChannelAccount);
  }

  loadRelatedData() {
    return this._productRest.loadRelatedData().do(related => this.relatedData = related);
  }

  formPrev() {
    this.formPrevStep();

    if (this.formState.prevSkipped) {
      this.formState.prevSkipped = false;
      this.formPrevStep();
    }
  }

  formPrevStep() {
    this._store.dispatch({ type: ProductImportChannelStepStore.ACTIONS.PREV });
  }

  formNext(skipProcess: boolean = false) {
    if (skipProcess) {
      this.formState.prevSkipped = true;
      this.formNextStep();
      return;
    }

    const loading = this.page.createLoading();

    let obs: Observable<any>;
    switch (this.formStep) {
      case 1:
        obs = this.elProductImportChannelFetch.process();
        break;
      case 2:
        obs = this.elProductImportMapRelated.process();
        break;
      case 3:
        obs = this.elProductImportChannelEditor.process();
        break;
    }

    obs.subscribe(null, () => {
      loading.dispose();
    }, () => {
      loading.dispose();

      if (this.formStep !== this.maxStep) {
        this.formNextStep();
      }
    });
  }

  formNextStep() {
    this._store.dispatch({ type: ProductImportChannelStepStore.ACTIONS.NEXT });
  }
}

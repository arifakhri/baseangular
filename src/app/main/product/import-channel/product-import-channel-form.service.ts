import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { FormService } from '../../../core/core.module';

import { MFieldDefinition } from '../../../core/common/form.model';

@Injectable()
export class ProductImportChannelFormService {
  formDefinitions: MFieldDefinition[] = [{
    key: 'name',
  }, {
    key: 'category',
  }, {
    key: 'brand',
  }, {
    key: 'description',
  }, {
    key: 'attribute1',
  }, {
    key: 'attribute2',
  }, {
    key: 'attribute3',
  }, {
    key: 'variants',
    isFormArray: true,
    children: [{
      key: 'sku',
    }, {
      key: 'price',
    }, {
      key: 'attribute1_value',
    }, {
      key: 'attribute2_value',
    }, {
      key: 'attribute3_value',
    }],
  }];

  constructor(
    private _form: FormService,
  ) { }

  setFormDefinitions(form: FormGroup) {
    return this._form.build(this.formDefinitions, form);
  }
}

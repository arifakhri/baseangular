import { Component, Input } from '@angular/core';
import { FormArray, FormGroup } from '@angular/forms';
import * as _ from 'lodash';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';

import { FileService } from '../../../../core/file/file.service';
import { SystemMessageService } from '../../../../core/system-message/system-message.service';
import { MPriceLevel } from '../../../price-level/price-level.model';
import { SalesChannelMarketplaceService } from '../../../sales-channel/sales-channel-marketplace.service';
import { SalesChannelWebstoreService } from '../../../sales-channel/sales-channel-webstore.service';
import { MSalesChannelAccount } from '../../../sales-channel/sales-channel.model';
import { ProductImportEditorBComponent } from '../../import/product-import-editor.bcomponent';

@Component({
  selector: 'app-product-import-channel-editor',
  templateUrl: 'product-import-channel-editor.component.html',
  providers: [SystemMessageService],
})
export class ProductImportChannelEditorComponent extends ProductImportEditorBComponent {
  @Input() salesChannelAccount: MSalesChannelAccount;

  tableColumnLength: number = 10;
  tableColumnIndex: number[] = _.range(1, this.tableColumnLength + 2);

  constructor(
    private _fileService: FileService,
    private _salesChannelMarketplace: SalesChannelMarketplaceService,
    private _salesChannelWebstore: SalesChannelWebstoreService,
    public _systemMessage: SystemMessageService,
  ) {
    super();

    this.componentId = 'ProductImportChannelEditor';
  }

  save() {
    this.filterRecords('import');

    const { products } = this.form.value;

    const chunkSize = 50;
    const successResults = [];
    const failedResults = [];
    const filteredSeqNumbers = _.map<any>(this.filteredRecords, filteredRecord => filteredRecord.seqNumber);
    const formProductsToProcess = _.filter(products, product => filteredSeqNumbers.includes(product._identifier));
    const productImages = this.getProductImages(formProductsToProcess);

    const productsChunk = _.chunk(formProductsToProcess, chunkSize);
    return Observable.onErrorResumeNext(
      ..._.map(productsChunk, (productChunk, productChunkIdx) => {
        const endIndex = chunkSize * (productChunkIdx + 1);
        const chunkProductImages = productImages.slice(endIndex - chunkSize, endIndex);
        return this.savePartial(productChunk, productImages).do(([chunkSuccessResults, chunkFailedResults]) => {
          successResults.push(...chunkSuccessResults);
          failedResults.push(...chunkFailedResults);
        });
      })
    ).finally(() => {
      this.lastResults = successResults.concat(failedResults);

      if (successResults.length === formProductsToProcess.length) {
        this.onSaveSuccess();
      } else {
        this.onSaveFailed();
      }
    });
  }

  onSaveSuccess() {
    this._globalSystemMessage.log({
      message: this.page._translate.instant('success.product.importChannel'),
      type: 'success',
      showAs: 'growl',
      showSnackBar: false,
    });

    this._router.navigateByUrl('/products');
  }

  savePartial(products, productImages) {
    const channelAccountId = this.salesChannelAccount.channelAccountId;
    const channelAccountType = this.salesChannelAccount.channelAccountType;
    _.forEach(products, clodeoProduct => {
      const rawChannelProduct: any = _.find(this.formState.rawRecords, { seqNumber: clodeoProduct._identifier });
      switch (channelAccountType) {
        case 'marketplace':
          this._salesChannelMarketplace.connectToClodeoProduct(channelAccountId, rawChannelProduct, clodeoProduct);
          break;
        case 'webstore':
          this._salesChannelWebstore.connectToClodeoProduct(channelAccountId, rawChannelProduct, clodeoProduct);
          break;
      }
    });

    return this._productImport.doImportProductsWithImages(this.formState.records, products, productImages, this.page)
      .switchMap(results => {
        const resultsPartition = _.partition(results, { success: true });
        return Observable.of(resultsPartition);
      });
  }

  getProductImages(formProductsToProcess) {
    const productImages = [];

    _.forEach(formProductsToProcess, (formProductToProcess, formProductIdx) => {
      productImages[formProductIdx] = [];

      const channelProduct = _.find(this.formState.records, { seqNumber: formProductToProcess._identifier });

      _.forEach(channelProduct.images, image => {
        productImages[formProductIdx].push(
          this._fileService.removeQueryStringFromUrl(image.src)
        );
      });

      _.forEach(channelProduct.variants, recordVariant => {
        _.forEach(recordVariant.images, image => {
          productImages[formProductIdx].push(
            this._fileService.removeQueryStringFromUrl(image.src)
          );
        });
      });
    });

    return productImages;
  }

  buildFormFromRecords(records: any) {
    this._ngZone.run(() => {
      const formArray = <FormArray>this.form.get('products');
      while (formArray.length) {
        formArray.removeAt(0);
      }

      _.forEach(records, (record, recordIdx) => {
        this.selectedUOMS[recordIdx] = [];

        const newFormGroup = new FormGroup({});
        this._productForm.setFormDefinitions(newFormGroup, record.productType);

        this._productForm.applySettings(newFormGroup, this.relatedData).subscribe();

        if (newFormGroup.get('uom').value) {
          this.selectedUOMS[recordIdx][0] = newFormGroup.get('uom').value;
        }

        this.buildFormVariantsFromRecord(newFormGroup, record, this.relatedData.priceLevels);

        newFormGroup.patchValue({
          ...record,
          _identifier: record.seqNumber,
        });

        formArray.setControl(record.seqNumber, newFormGroup);
      });
    });
  }

  buildFormVariantsFromRecord(productForm: FormGroup, record: any, priceLevels: MPriceLevel[]) {
    const formVariants = [];

    _.forEach<any, any>(record.variants, (recordVariant, recordVariantIdx) => {
      let variantFormGroup: FormGroup;
      if (recordVariantIdx > 0) {
        variantFormGroup = this._productForm.buildFormChildVariant();
      } else {
        variantFormGroup = productForm.get('variants.0') as FormGroup;
      }

      this._productForm.syncFormChildVariantPrices(variantFormGroup, priceLevels);

      const prices = variantFormGroup.get('prices').value;
      const targetPriceIndex = _.findIndex(prices, this.salesChannelAccount.priceLevelId ? { priceLevelId: this.salesChannelAccount.priceLevelId } : { isMaster: true });
      variantFormGroup.get(`prices.${targetPriceIndex}.unitPrice`).setValue(recordVariant.price);

      (<FormArray>productForm.get('variants')).setControl(recordVariantIdx, variantFormGroup);
    });

    return formVariants;
  }
}

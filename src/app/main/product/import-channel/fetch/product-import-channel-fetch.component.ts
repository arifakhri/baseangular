import { Component, Input } from '@angular/core';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';

import { EImportDataTypes, ImportDataService } from '../../../../core/core.module';
import { BasePageBComponent } from '../../../../shared/base/base.module';
import { SalesChannelMarketplaceRestService } from '../../../sales-channel/sales-channel-marketplace-rest.service';
import { MSalesChannelMarketplaceProduct } from '../../../sales-channel/sales-channel-marketplace.model';
import { SalesChannelRestService } from '../../../sales-channel/sales-channel-rest.service';
import { SalesChannelWebstoreRestService } from '../../../sales-channel/sales-channel-webstore-rest.service';
import { MSalesChannelWebstoreProduct } from '../../../sales-channel/sales-channel-webstore.model';
import { ProductImportChannelService } from '../product-import-channel.service';
import { MSalesChannelAccount } from '../../../sales-channel/sales-channel.model';

@Component({
  selector: 'app-product-import-channel-fetch',
  templateUrl: './product-import-channel-fetch.component.html',
})
export class ProductImportChannelFetchComponent extends BasePageBComponent {
  @Input() formState: any = {};
  @Input() salesChannelAccount: MSalesChannelAccount;

  constructor(
    private _importData: ImportDataService,
    private _productImportChannel: ProductImportChannelService,
    private _salesChannelMarketplaceRest: SalesChannelMarketplaceRestService,
    private _salesChannelWebstoreRest: SalesChannelWebstoreRestService,
    private _salesChannelRest: SalesChannelRestService,
  ) {
    super();

    this.componentId = 'ProductImportChannelFetch';
  }

  process() {
    let obs: Observable<MSalesChannelMarketplaceProduct[] | MSalesChannelWebstoreProduct[]>;
    switch (this.salesChannelAccount.channelAccountType) {
      case 'marketplace':
        obs = this._salesChannelMarketplaceRest.getConfig(this.salesChannelAccount.channelAccountId).switchMap(channelConfig => {
          return this._salesChannelRest.findAll<MSalesChannelMarketplaceProduct>(
            this.salesChannelAccount.channelAccountId,
            this._salesChannelMarketplaceRest.findAllPartial.bind(this._salesChannelMarketplaceRest),
            channelConfig.pagination.max_limit,
          );
        });
        break;
      case 'webstore':
        obs = this._salesChannelWebstoreRest.getConfig(this.salesChannelAccount.channelAccountId).switchMap(channelConfig => {
          return this._salesChannelRest.findAll<MSalesChannelWebstoreProduct>(
            this.salesChannelAccount.channelAccountId,
            this._salesChannelWebstoreRest.findAllPartial.bind(this._salesChannelWebstoreRest),
            channelConfig.pagination.max_limit,
          );
        });
        break;
    }

    return this.page.createRetriableTask(obs).switchMap(products => {
      this.formState.rawRecords = this._productImportChannel.markRecordsSeqNumber(products);

      const importData = this._importData.prepare({
        mapOptions: this._productImportChannel.importMap,
        from: EImportDataTypes.OBJECT,
        contentToProcess: products,
      });

      this.formState.importData = importData;

      return importData.process();
    }).do(processedProducts => {
      this.formState.records = this._productImportChannel.normalizeProducts(processedProducts);
    });
  }
}

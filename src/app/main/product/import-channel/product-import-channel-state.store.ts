export const ACTIONS = {
  SET: 'ProductImportChannelStateSet',
  SETATTR: 'ProductImportChannelStateSetAttr',
  RESET: 'ProductImportChannelStateReset',
};

export const STORE_ID = 'ProductImportChannelStateStore';

export function STORE(state: any = {}, action: any) {
  switch (action.type) {
    case ACTIONS.SET:
      return {
        ...state,
        [action.payload.key]: action.payload.value,
      };
    case ACTIONS.SETATTR:
      const newState = state;
      newState[action.payload.key][action.payload.attr] = action.payload.value;
      return newState;
    case ACTIONS.RESET:
      return {};
    default:
      return state;
  }
}

import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { BsDropdownModule, ModalModule, PaginationModule, PopoverModule } from 'ngx-bootstrap';
import { AccordionModule } from 'primeng/accordion';
import { AutoCompleteModule } from 'primeng/components/autocomplete/autocomplete';
import { CalendarModule } from 'primeng/components/calendar/calendar';
import { CheckboxModule } from 'primeng/components/checkbox/checkbox';
import { ChipsModule } from 'primeng/components/chips/chips';
import { DataTableModule } from 'primeng/components/datatable/datatable';
import { DropdownModule } from 'primeng/components/dropdown/dropdown';
import { EditorModule } from 'primeng/components/editor/editor';
import { InputSwitchModule } from 'primeng/components/inputswitch/inputswitch';
import { ListboxModule } from 'primeng/components/listbox/listbox';
import { TableModule } from 'primeng/components/table/table';
import { TabViewModule } from 'primeng/components/tabview/tabview';

import { CoreModule } from '../../core/core.module';
import { FormgenModule } from '../../shared/formgen/formgen.module';
import { SharedModule } from '../../shared/shared.module';
import { ProductAttributeModule } from '../product-attribute/product-attribute.module';
import { ProductBrandModule } from '../product-brand/product-brand.module';
import { ProductCategoryModule } from '../product-category/product-category.module';
import { ProductTagModule } from '../product-tag/product-tag.module';
import { UOMModule } from '../uom/uom.module';
import { ProductCreateVariantComponent } from './create-variant/product-create-variant.component';
import { ProductCreateComponent } from './create/product-create.component';
import { ProductDetailWarehouseComponent } from './detail-warehouse/product-detail-warehouse.component';
import { ProductDetailComponent } from './detail/product-detail.component';
import { ProductFormChannelDefaultComponent } from './form-channel/default/product-form-channel-default.component';
import { ProductFormChannelDynamicComponent } from './form-channel/dynamic/product-form-channel-dynamic.component';
import { ProductFormChannelDynamicService } from './form-channel/dynamic/product-form-channel-dynamic.service';
import { ProductFormChannelMarketplaceComponent, } from './form-channel/marketplace/product-form-channel-marketplace.component';
import { ProductFormChannelService } from './form-channel/product-form-channel.service';
import { ProductFormChannelProgressComponent } from './form-channel/progress/product-form-channel-progress.component';
import { ProductFormChannelVariantsComponent } from './form-channel/variants/product-form-channel-variants.component';
import { ProductFormChannelWebstoreComponent } from './form-channel/webstore/product-form-channel-webstore.component';
import { ProductFormVariantComponent } from './form-variant/product-form-variant.component';
import { ProductFormBaseComponent } from './form/base/product-form-base.component';
import { ProductFormChannelSelectComponent } from './form/channel-select/product-form-channel-select.component';
import { ProductFormFieldAccountComponent } from './form/field-account/product-form-field-account.component';
import { ProductFormFieldAttributeComponent } from './form/field-attribute/product-form-field-attribute.component';
import { ProductFormFieldAttributesUpdateComponent, } from './form/field-attributes-update/product-form-field-attributes-update.component';
import { ProductFormFieldAttributesComponent } from './form/field-attributes/product-form-field-attributes.component';
import { ProductFormFieldBrandComponent } from './form/field-brand/product-form-field-brand.component';
import { ProductFormFieldCategoryComponent } from './form/field-category/product-form-field-category.component';
import { ProductFormFieldDetailDescriptionComponent, } from './form/field-detail-description/product-form-field-detail-description.component';
import { ProductFormFieldInventoryOpeningBalanceComponent, } from './form/field-inventory-opening-balance/product-form-field-inventory-opening-balance.component';
import { ProductFormFieldModelComponent } from './form/field-model/product-form-field-model.component';
import { ProductFormFieldNameComponent } from './form/field-name/product-form-field-name.component';
import { ProductFormFieldPricesComponent } from './form/field-prices/product-form-field-prices.component';
import { ProductFormFieldPurchasesAccountComponent, } from './form/field-purchases-account/product-form-field-purchases-account.component';
import { ProductFormFieldPurchasesDescriptionComponent, } from './form/field-purchases-description/product-form-field-purchases-description.component';
import { ProductFormFieldPurchasesPriceComponent, } from './form/field-purchases-price/product-form-field-purchases-price.component';
import { ProductFormFieldPurchasesPurchasableComponent, } from './form/field-purchases-purchasable/product-form-field-purchases-purchasable.component';
import { ProductFormFieldPurchasesTaxComponent, } from './form/field-purchases-tax/product-form-field-purchases-tax.component';
import { ProductFormFieldSalesAccountComponent, } from './form/field-sales-account/product-form-field-sales-account.component';
import { ProductFormFieldSalesAutoReserveComponent, } from './form/field-sales-auto-reserve/product-form-field-sales-auto-reserve.component';
import { ProductFormFieldSalesDescriptionComponent, } from './form/field-sales-description/product-form-field-sales-description.component';
import { ProductFormFieldSalesSellableComponent, } from './form/field-sales-sellable/product-form-field-sales-sellable.component';
import { ProductFormFieldSalesTaxComponent } from './form/field-sales-tax/product-form-field-sales-tax.component';
import { ProductFormFieldCodeComponent } from './form/field-sku/product-form-field-sku.component';
import { ProductFormFieldTagsComponent } from './form/field-tags/product-form.field-tags.component';
import { ProductFormFieldTypeComponent } from './form/field-type/product-form-field-type.component';
import { ProductFormFieldUOMSComponent } from './form/field-uoms/product-form-field-uoms.component';
import { ProductFormFieldVariantsUpdateComponent, } from './form/field-variants-update/product-form-field-variants-update.component';
import { ProductFormFieldVariantsComponent } from './form/field-variants/product-form-field-variants.component';
import { ProductFormFieldsetChannelComponent } from './form/fieldset-channel/product-form-fieldset-channel.component';
import { ProductFormInventoryMiniComponent } from './form/inventory-mini/product-form-inventory-mini.component';
import { ProductFormInventoryComponent } from './form/inventory/product-form-inventory.component';
import { ProductFormNonInventoryMiniComponent } from './form/non-inventory-mini/product-form-non-inventory-mini.component';
import { ProductFormNonInventoryComponent } from './form/non-inventory/product-form-non-inventory.component';
import { ProductFormServiceMiniComponent } from './form/service-mini/product-form-service-mini.component';
import { ProductFormServiceComponent } from './form/service/product-form-service.component';
import { ProductImportChannelEditorComponent } from './import-channel/editor/product-import-channel-editor.component';
import { ProductImportChannelFetchComponent } from './import-channel/fetch/product-import-channel-fetch.component';
import { ProductImportChannelFormService } from './import-channel/product-import-channel-form.service';
import * as ProductImportChannelStateStore from './import-channel/product-import-channel-state.store';
import * as ProductImportChannelStepStore from './import-channel/product-import-channel-step.store';
import { ProductImportChannelComponent } from './import-channel/product-import-channel.component';
import { ProductImportChannelService } from './import-channel/product-import-channel.service';
import { ProductImportTableBrowseComponent } from './import-table/browse/product-import-table-browse.component';
import { ProductImportTableEditorComponent } from './import-table/editor/product-import-table-editor.component';
import { ProductImportTableMapFieldsComponent } from './import-table/map-fields/product-import-table-map-fields.component';
import { ProductImportTableComponent } from './import-table/product-import-table.component';
import { ProductImportTableService } from './import-table/product-import-table.service';
import { ProductImportMapRelatedComponent } from './import/map-related/product-import-map-related.component';
import * as ProductImportStateStore from './import/product-import-state.store';
import * as ProductImportStepStore from './import/product-import-step.store';
import { ProductImportService } from './import/product-import.service';
import { ProductListComponent } from './list/product-list.component';
import { ProductFormService } from './product-form.service';
import { ProductRestService } from './product-rest.service';
import { ProductService } from './product.service';
import { ProductShareComponent } from './share/product-share.component';
import { ProductTabLinksComponent } from './tab-links/product-tab-links.component';
import { ProductTransactionComponent } from './transaction/product-transaction.component';
import { ProductUpdateVariantComponent } from './update-variant/product-update-variant.component';
import { ProductUpdateComponent } from './update/product-update.component';

export const STORES = {};
STORES[ProductImportChannelStepStore.STORE_ID] = ProductImportChannelStepStore.STORE;
STORES[ProductImportChannelStateStore.STORE_ID] = ProductImportChannelStateStore.STORE;
STORES[ProductImportStepStore.STORE_ID] = ProductImportStepStore.STORE;
STORES[ProductImportStateStore.STORE_ID] = ProductImportStateStore.STORE;

export const PROVIDERS = [
  ProductFormChannelDynamicService,
  ProductFormChannelService,
  ProductFormService,
  ProductImportChannelFormService,
  ProductImportChannelService,
  ProductImportService,
  ProductImportTableService,
  ProductRestService,
  ProductService,
];

@NgModule({
  imports: [
    AccordionModule,
    AutoCompleteModule,
    BsDropdownModule,
    CalendarModule,
    CheckboxModule,
    ChipsModule,
    CommonModule,
    CoreModule,
    DataTableModule,
    DropdownModule,
    EditorModule,
    FlexLayoutModule,
    FormgenModule,
    FormsModule,
    HttpModule,
    InputSwitchModule,
    ListboxModule,
    ModalModule,
    PaginationModule,
    PopoverModule,
    ProductAttributeModule,
    ProductBrandModule,
    ProductCategoryModule,
    ProductTagModule,
    ReactiveFormsModule,
    RouterModule,
    SharedModule,
    TableModule,
    TabViewModule,
    TranslateModule,
    UOMModule,
  ],
  declarations: [
    ProductCreateComponent,
    ProductCreateVariantComponent,
    ProductDetailComponent,
    ProductDetailWarehouseComponent,
    ProductFormBaseComponent,
    ProductFormChannelDefaultComponent,
    ProductFormChannelDynamicComponent,
    ProductFormChannelMarketplaceComponent,
    ProductFormChannelProgressComponent,
    ProductFormChannelSelectComponent,
    ProductFormChannelWebstoreComponent,
    ProductFormChannelVariantsComponent,
    ProductFormVariantComponent,
    ProductListComponent,
    ProductTabLinksComponent,
    ProductUpdateComponent,
    ProductUpdateVariantComponent,

    ProductFormFieldAccountComponent,
    ProductFormFieldAttributeComponent,
    ProductFormFieldAttributesComponent,
    ProductFormFieldAttributesUpdateComponent,
    ProductFormFieldBrandComponent,
    ProductFormFieldCategoryComponent,
    ProductFormFieldCodeComponent,
    ProductFormFieldModelComponent,
    ProductFormFieldNameComponent,
    ProductFormFieldDetailDescriptionComponent,
    ProductFormFieldPricesComponent,
    ProductFormFieldPurchasesAccountComponent,
    ProductFormFieldPurchasesDescriptionComponent,
    ProductFormFieldPurchasesPriceComponent,
    ProductFormFieldPurchasesPurchasableComponent,
    ProductFormFieldPurchasesTaxComponent,
    ProductFormFieldSalesAccountComponent,
    ProductFormFieldSalesDescriptionComponent,
    ProductFormFieldSalesSellableComponent,
    ProductFormFieldSalesTaxComponent,
    ProductFormFieldTagsComponent,
    ProductFormFieldTypeComponent,
    ProductFormFieldUOMSComponent,
    ProductFormFieldInventoryOpeningBalanceComponent,
    ProductFormFieldVariantsComponent,
    ProductFormFieldVariantsUpdateComponent,
    ProductFormFieldSalesAutoReserveComponent,

    ProductFormFieldsetChannelComponent,

    ProductFormInventoryComponent,
    ProductFormInventoryMiniComponent,
    ProductFormNonInventoryComponent,
    ProductFormNonInventoryMiniComponent,
    ProductFormServiceComponent,
    ProductFormServiceMiniComponent,

    ProductImportMapRelatedComponent,
    ProductImportTableBrowseComponent,
    ProductImportTableComponent,
    ProductImportTableEditorComponent,
    ProductImportTableMapFieldsComponent,

    ProductImportChannelComponent,
    ProductImportChannelEditorComponent,
    ProductImportChannelFetchComponent,

    ProductShareComponent,
    ProductTransactionComponent,
  ],
})
export class ProductModule { }

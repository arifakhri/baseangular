import { NgModule } from '@angular/core';

import { ReportsAPAgingModule } from './reports-ap-aging.module';
import { ReportsAPAgingRoutingModule } from './reports-ap-aging-routing.module';

@NgModule({
  imports: [
    ReportsAPAgingModule,
    ReportsAPAgingRoutingModule,
  ],
})
export class ReportsAPAgingLazyModule { }

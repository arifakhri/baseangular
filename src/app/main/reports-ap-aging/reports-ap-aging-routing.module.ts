import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ReportsAPAgingPreviewComponent } from './preview/reports-ap-aging-preview.component';

import { AuthorizationService } from '../../core/core.module';

const routes: Routes = [{
  path: '',
  children: [{
    path: '',
    component: ReportsAPAgingPreviewComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'reportsAPAging.preview'
    }
  }]
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ],
})
export class ReportsAPAgingRoutingModule { }

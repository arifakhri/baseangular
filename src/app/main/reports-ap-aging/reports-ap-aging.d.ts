declare interface IReportsAPAging {
  name: string;
  current: number;
  to30Days: number;
  to60Days: number;
  to90Days: number;
  over90Days: number;
  total: number;
}
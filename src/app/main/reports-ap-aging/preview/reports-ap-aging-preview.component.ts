import * as moment from 'moment';
import { Component, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

import { AdminLayoutService } from '../../../layouts/layouts.module';
import { ReportsAPAgingRestService } from '../reports-ap-aging-rest.service';
import { GridTableFilterService } from '../../../core/core.module';

@Component({
  selector: 'app-reports-ap-aging-preview',
  templateUrl: './reports-ap-aging-preview.component.html',
})
export class ReportsAPAgingPreviewComponent {
  @ViewChild('reportViewer') elReportViewer: any;

  compReady: boolean = false;
  data: any = [];
  form: FormGroup = new FormGroup({});
  reportViewerReady: boolean = false;

  reportParams: any = {
    title: 'AP Aging Summary'
  };

  filtersMap: IGridTableFilterMap = {
    date: {
      targetVar: 'date',
      targetFilter: 'param',
      dateFormat: 'YYYY-MM-DD',
    }
  };

  constructor(
    private _adminLayout: AdminLayoutService,
    private _gridTableFilter: GridTableFilterService,
    private _reportsAPAgingRest: ReportsAPAgingRestService,
  ) {
    this._adminLayout.containerType = 1;

    this.buildForm();
    this.loadData();
  }

  buildForm() {
    this.form.addControl('date', new FormControl(new Date()));
  }

  loadData() {
    const filters = this._gridTableFilter.parse(this.form.value, this.filtersMap);
    this.compReady = false;

    this._reportsAPAgingRest.findAll(filters.qParams).subscribe(response => {
      this.data = response.data;

      this.prepareParams();
    }, null, () => this.compReady = true);
  }

  prepareParams() {
    const filterValues = this.form.value;

    if (filterValues.date) {
      this.reportParams.date = moment(filterValues.date).format('DD/MM/YYYY');
    }
  }
}

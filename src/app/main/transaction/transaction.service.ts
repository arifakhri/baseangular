import { Injectable } from '@angular/core';

@Injectable()
export class TransactionService {
    stateList: any = [
        { label: 'Draft', value: 'draft' },
        { label: 'Unapproved', value: 'unapproved' },
        { label: 'Approved', value: 'approved' },
        { label: 'Voided', value: 'voided' },
        { label: 'Deleted', value: 'deleted' },
    ];

    statusList: any = [
        { label: 'Draft', value: 'draft' },
        { label: 'Unapproved', value: 'unapproved' },
        { label: 'Open', value: 'open' },
        { label: 'Partial Delivered', value: 'partial_delivered' },
        { label: 'Partial Paid', value: 'partial_paid' },
        { label: 'Closed', value: 'closed' },
        { label: 'Delivered', value: 'delivered' },
        { label: 'Paid', value: 'paid' },
        { label: 'Applied', value: 'applied' },
        { label: 'Voided', value: 'voided' },
        { label: 'Deleted', value: 'deleted' },
    ];
}

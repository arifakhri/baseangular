import { Component, EventEmitter, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

import { BusinessPartnerRestService } from '../../business-partner/business-partner-rest.service';
import { GridTableFilterService } from '../../../core/core.module';
import { TransactionMoreFilterService } from './transaction-more-filter.service';

@Component({
  selector: 'app-transaction-more-filter',
  templateUrl: 'transaction-more-filter.component.html',
})
export class TransactionMoreFilterComponent {
  @Output() success: EventEmitter<IGridTableFilterParsed> = new EventEmitter();

  contactsSuggestion: IBusinessPartner[] = [];
  form: FormGroup = new FormGroup({});

  filterMap: IGridTableFilterMap = {
    contactId: {
      targetFilter: 'param',
    },
    lowDate: {
      targetVar: 'date',
      targetFilter: 'param',
      dateFormat: 'YYYY-MM-DD',
    },
    highDate: {
      targetVar: 'date',
      targetFilter: 'param',
      dateFormat: 'YYYY-MM-DD',
    },
    status: {
      targetFilter: 'param',
    },
  };

  constructor(
    private _businessPartnerRest: BusinessPartnerRestService,
    private _gridTableFilter: GridTableFilterService,
    private _transactionMoreFilter: TransactionMoreFilterService,
  ) {
    this.buildForm();

    this.form.patchValue(this._transactionMoreFilter.lastMoreFilterValues);
  }

  buildForm() {
    this.form.addControl('contact', new FormControl);
    this.form.addControl('contactId', new FormControl);
    this.form.addControl('lowDate', new FormControl);
    this.form.addControl('highDate', new FormControl);
    this.form.addControl('status', new FormControl(''));
  }

  searchContact(ev) {
    this._businessPartnerRest.findAll({
      filter: [{
        filterValues: [{
          field: 'displayName',
          operator: 'contains',
          value: ev.query
        }]
      }],
      sort: [{
        field: 'displayName',
        dir: 'asc'
      }],
      take: 30,
      skip: 0,
      includeTotalCount: false
    }).subscribe(contacts => {
      this.contactsSuggestion = contacts.data;
    });
  }

  onContactDropdown(ev: any) {
    this.contactsSuggestion = [];
    if (ev.query.length) {
      this.searchContact(ev);
    } else {
      this._businessPartnerRest.findAll({
        filter: [],
        sort: [{
          field: 'displayName',
          dir: 'asc'
        }],
        take: 30,
        skip: 0,
        includeTotalCount: false
      }).subscribe(contacts => {
        this.contactsSuggestion = contacts.data;
      });
    }
  }

  onSubmit() {
    this._transactionMoreFilter.lastMoreFilterValues = this.form.value;
    this.success.emit(this._gridTableFilter.parse(this.form.value, this.filterMap));
  }
}

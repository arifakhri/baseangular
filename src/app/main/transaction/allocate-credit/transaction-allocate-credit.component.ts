import * as _ from 'lodash';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CustomValidators } from 'ng2-validation';
import { FormArray, FormControl, FormGroup } from '@angular/forms';

import { AccountingService } from '../../../core/core.module';
import { BusinessPartnerRestService } from '../../business-partner/business-partner-rest.service';
import { TransactionAllocateCreditService } from './transaction-allocate-credit.service';

@Component({
  selector: 'app-transaction-allocate-credit',
  templateUrl: 'transaction-allocate-credit.component.html'
})
export class TransactionAllocateCreditComponent implements OnInit {
  @Input() allocator: Function;
  @Input() credits: any = [];
  @Input() sourceTransaction: ISalesInvoice | ISalesCreditNote | IPurchasesInvoice | IPurchasesDebitNote;
  @Output() cancel: EventEmitter<boolean> = new EventEmitter;
  @Output() success: EventEmitter<any> = new EventEmitter;

  form: FormGroup = new FormGroup({});

  initalFormValues: any = {};
  compLoading: boolean = false;

  constructor(
    public _accounting: AccountingService,
    private _businessPartnerRest: BusinessPartnerRestService,
    private _transactionAllocateCredit: TransactionAllocateCreditService,
  ) { }

  ngOnInit() {
    this.form.addControl('lines', new FormArray([]));

    this.buildForm();

    if (this.sourceTransaction.transactionType === 'transaction_invoice') {
      const groupedCredits = _.groupBy(this.credits, 'transactionType');
      this.credits = [
        ...(_.get(groupedCredits, 'transaction_down_payment', [])),
        ..._.values(_.omit(groupedCredits, 'transaction_down_payment')),
      ];
    }
  }

  onSubmit() {
    this.compLoading = true;
    if (this.form.valid) {
      const record = this.form.value;
      this._transactionAllocateCredit.normalizeDoc(record);

      this.allocator(record.lines).subscribe(result => {
        this.compLoading = false;
        this.success.emit(result);
        this.form.reset(this.initalFormValues);
      });
    }
  }

  buildForm() {
    this.credits.forEach(credit => {
      const lineFormGroup = this.buildFormChildrenLine(credit);
      (<FormArray>this.form.get('lines')).push(lineFormGroup);
    });

    this.initalFormValues = this.form.value;
  }

  buildFormChildrenLine(credit: any): FormGroup {
    return new FormGroup({
      allocateAmount: new FormControl(0, CustomValidators.max(credit.remaining || credit.balanceDue || 0)),
      transactionId: new FormControl(credit.transactionId || credit.id),
      transactionType: new FormControl(credit.transactionType),
    });
  }

  get totalAmountToAllocate() {
    return _.sumBy(this.form.value.lines, 'allocateAmount') || 0;
  }
}

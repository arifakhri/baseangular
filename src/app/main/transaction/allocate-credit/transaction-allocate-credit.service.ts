import * as _ from 'lodash';
import { Injectable } from '@angular/core';

@Injectable()
export class TransactionAllocateCreditService {
  normalizeDoc(record: any) {
    _.remove<any>(record.lines, line => {
      return !line.allocateAmount;
    });
  }
}

import { NgModule } from '@angular/core';

import { TransactionModule } from './transaction.module';
import { TransactionRoutingModule } from './transaction-routing.module';

@NgModule({
  imports: [
    TransactionModule,
    TransactionRoutingModule
  ]
})
export class TransactionLazyModule { }

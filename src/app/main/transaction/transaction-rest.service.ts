import { Injectable } from '@angular/core';
import 'app/rxjs-imports.ts';

import { ApiQueryOption, RequestService } from '../../core/core.module';

import { APP_CONST } from '../../app.const';

@Injectable()
export class TransactionRestService {
  baseURL = `${APP_CONST.API_MAIN}/transactions`;
  request = this._request.new(this.baseURL);

  constructor(
    private _request: RequestService,
  ) { }

  findAll<T = ITransaction>(queryOption: ApiQueryOption, queryParams: any = {}) {
    return this.request.post<{
      data: IApiPaginationResult<T>;
      transactionSummary: {
        invoiceTotal: number;
        overDue: number;
        theOwe: number;
      };
    }>(`all/q`, queryOption, {
      params: queryParams
    });
  }

  transactionJournal(transactionType: string, transactionId: string) {
    return this.request.get<ITransactionJournal[]>(`../transaction-journal/${transactionType}/${transactionId}`);
  }
}

import { AutoComplete } from 'primeng/components/autocomplete/autocomplete';
import { ViewChild } from '@angular/core';

import { TransactionFormBComponent } from './transaction-form.bcomponent';

import { CommonService, InjectorService } from '../../core/core.module';
import { PurchasesRestService } from '../purchases/purchases-rest.service';

export class TransactionFormPurchasesBComponent extends TransactionFormBComponent {
  @ViewChild('vendorAC') elVendorAC: AutoComplete;

  public _purchasesRest = InjectorService.get<PurchasesRestService>(PurchasesRestService);

  transactionGroup = 'sales';

  ACVendorHandler: any;

  init() {
    this.ACVendorHandler = CommonService.remoteACItemsHandler({
      remoteParams: this.ACBusinessPartnerParams.bind(this),
      remoteRequest: this._businessPartnerRest.findAllVendors.bind(this._businessPartnerRest),
      remoteRequestMap: (response) => response.data,
      element: () => this.elVendorAC,
      ACConfig: {
        prependNull: true,
      },
    });
  }

  onVendorClear() {
    this.form.get('vendorId').reset(this.getDefaultValue('master').vendorId);
  }

  onVendorSelected(vendor: IBusinessPartner, patchEmail: boolean = true) {
    const valueToPatch: any = {
      vendor: vendor,
      vendorId: vendor.id,
    };
    if (patchEmail) {
      valueToPatch.vendorEmail = vendor.email;
    }
    this.form.patchValue(valueToPatch);

    this.callHook('afterVendorSelected', vendor).subscribe();
  }

  onVendorCreated(vendor: IBusinessPartner) {
    this.onVendorSelected(vendor);
    this.elQuickCreateModal.hide();
  }

  generateLineProductDescription(productVariant: IProductVariant): string {
    const purchaseDescription = (productVariant.product.purchaseDescription || '').trim();
    const description = `${(purchaseDescription) ? purchaseDescription : productVariant.name.trim()}`;
    return description;
  }
}

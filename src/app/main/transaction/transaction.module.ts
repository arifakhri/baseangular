import { AutoCompleteModule } from 'primeng/components/autocomplete/autocomplete';
import { BsDropdownModule, ModalModule, PaginationModule, PopoverModule } from 'ngx-bootstrap';
import { CalendarModule } from 'primeng/components/calendar/calendar';
import { CheckboxModule } from 'primeng/components/checkbox/checkbox';
import { CommonModule } from '@angular/common';
import { DataTableModule } from 'primeng/components/datatable/datatable';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { ListboxModule } from 'primeng/components/listbox/listbox';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { TransactionAllocateCreditComponent } from './allocate-credit/transaction-allocate-credit.component';
import { TransactionJournalComponent } from './journal/transaction-journal.component';
import { TransactionListComponent } from './list/transaction-list.component';
import { TransactionMoreFilterComponent } from './more-filter/transaction-more-filter.component';

import { TransactionAllocateCreditService } from './allocate-credit/transaction-allocate-credit.service';
import { TransactionMoreFilterService } from './more-filter/transaction-more-filter.service';
import { TransactionQueryService } from './transaction-query.service';
import { TransactionRestService } from './transaction-rest.service';
import { TransactionRoutingService } from './transaction-routing.service';
import { TransactionService } from './transaction.service';

import { CoreModule } from '../../core/core.module';
import { SharedModule } from '../../shared/shared.module';

export const PROVIDERS = [
  TransactionAllocateCreditService,
  TransactionMoreFilterService,
  TransactionRestService,
  TransactionRoutingService,
  TransactionService,
  TransactionQueryService,
];

@NgModule({
  imports: [
    AutoCompleteModule,
    BsDropdownModule,
    CalendarModule,
    CheckboxModule,
    CommonModule,
    CoreModule,
    DataTableModule,
    FlexLayoutModule,
    FormsModule,
    HttpModule,
    ListboxModule,
    ModalModule,
    PaginationModule,
    PopoverModule,
    ReactiveFormsModule,
    RouterModule,
    SharedModule,
    TranslateModule,
  ],
  declarations: [
    TransactionAllocateCreditComponent,
    TransactionListComponent,
    TransactionJournalComponent,
    TransactionMoreFilterComponent,
  ],
  exports: [
    TransactionAllocateCreditComponent,
    TransactionJournalComponent,
  ]
})
export class TransactionModule { }

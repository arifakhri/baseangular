import * as _ from 'lodash';
import { ActivatedRoute } from '@angular/router';
import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { DataTable } from 'primeng/components/datatable/datatable';
import { Observable, Subscription } from 'rxjs';
import 'app/rxjs-imports.ts';
import { PopoverDirective } from 'ngx-bootstrap';
import { TranslateService } from '@ngx-translate/core';

import { SpinnerDirective } from '../../../shared/spinner/spinner.directive';

import {
  AccountingService,
  ExportDataTableService,
  GridTableDataSource,
  GridTableService,
  GridTableToggleService,
} from '../../../core/core.module';
import { TransactionMoreFilterService } from '../more-filter/transaction-more-filter.service';
import { TransactionQueryService } from '../transaction-query.service';
import { TransactionRestService } from '../transaction-rest.service';
import { RetryDialogComponent } from '../../../shared/retry-dialog/retry-dialog.component';

@Component({
  selector: 'app-transaction-list',
  templateUrl: 'transaction-list.component.html'
})
export class TransactionListComponent implements OnDestroy, OnInit {
  @ViewChild('gridTable') public gridTable: DataTable;
  @ViewChild('pageLoading') elPageLoading: SpinnerDirective;
  @ViewChild('popMoreFilter') elPopMoreFilter: PopoverDirective;
  @ViewChild('retryDialog') elRetryDialog: RetryDialogComponent;

  compReady: boolean = false;
  gridDataSource: GridTableDataSource<ITransaction> = new GridTableDataSource<ITransaction>();
  selectedRecords: ITransaction[] = [];

  tableColumns: IGridTableColumn[] = [{
    i18nLabel: 'ui.transaction.list.column.transactionDate',
    field: 'transactionDate',
    dateFormat: 'DD/MM/YYYY',
    sort: true,
  }, {
    i18nLabel: 'ui.transaction.list.column.transactionType',
    field: 'transactionType',
    sort: true,
    formatter: (value, row: ITransaction) => {
      return _.startCase((row.transactionType));
    },
  }, {
    i18nLabel: 'ui.transaction.list.column.transactionNumber',
    field: 'transactionNumber',
    sort: true,
  }, {
    i18nLabel: 'ui.transaction.list.column.contactName',
    field: 'contact.displayName',
    sort: true,
  }, {
    i18nLabel: 'ui.transaction.list.column.amount',
    field: 'total',
    sort: true,
    formatter: (value, row: ITransaction) => {
      return this._accounting.ac.formatMoney(value);
    },
    columnClasses: 'right-align',
  }, {
    i18nLabel: 'ui.transaction.list.column.balanceDue',
    field: 'balanceDue',
    sort: true,
    formatter: (value, row: ITransaction) => {
      return this._accounting.ac.formatMoney(value);
    },
    columnClasses: 'right-align',
  }];
  tableColumnsShow: boolean = false;
  tableColumnsToggle = this._gridTableToggle.mapToggleOptionsFromColumns(this.tableColumns);

  qParams: any = {};
  formFilterValues: any = {
    transactionType: '',
  };
  moreFilterValues: IGridTableFilterParsed;

  retryDialogShownState: boolean = false;
  retryDialogOnRetry: Function;

  private loadDataTimeoutId: number;
  private gridFirstLoad: boolean = true;
  private subscriptions: { [key: string]: Subscription[] } = {
    load: []
  };

  constructor(
    private _accounting: AccountingService,
    private _activatedRoute: ActivatedRoute,
    private _dataTableExport: ExportDataTableService,
    private _gridTable: GridTableService,
    private _gridTableToggle: GridTableToggleService,
    private _transactionMoreFilter: TransactionMoreFilterService,
    private _transactionQuery: TransactionQueryService,
    private _transactionRest: TransactionRestService,
    private _translate: TranslateService,
  ) { }

  ngOnInit() {
    this.loadData();
  }

  ngOnDestroy() {
    this._transactionMoreFilter.lastMoreFilterValues = {};
  }

  gridLoadDataWrapper() {
    // Grid always load at the first time. we dont want that. we already call on ngOnInit
    if (this.gridFirstLoad) {
      this.gridFirstLoad = false;
      return;
    }

    if (this.loadDataTimeoutId) {
      window.clearTimeout(this.loadDataTimeoutId);
      this.loadDataTimeoutId = null;
    }

    this.loadDataTimeoutId = window.setTimeout(() => {
      this.loadData();
    }, 300);
  }

  onMoreFilter(filters: IGridTableFilterParsed) {
    this.elPopMoreFilter.hide();

    this.moreFilterValues = filters;
    this.loadData();
  }

  populateQParams() {
    let newFilters: any = _.pick(this.qParams, ['keyword']);

    if (!_.isUndefined(this._transactionQuery.lastQuery)) {
      const query = this._transactionQuery.lastQuery;
      newFilters.keyword = query;
      this._transactionQuery.lastQuery = undefined;
    }

    if (this.moreFilterValues) {
      newFilters = Object.assign({}, newFilters, this.moreFilterValues.qParams);
    }

    newFilters = Object.assign({}, newFilters, this.formFilterValues);

    this.qParams = newFilters;
  }

  get exportRecords(): Observable<ITransaction[]> {
    const qOption = {
      ...this._gridTable.generateApiQueryOptionFromGridInfo(
        this.gridDataSource.pager,
        false,
        this.gridTable,
      ),
      skip: 0,
      take: this._dataTableExport.exportLimit,
    };

    this.populateQParams();

    return this.elRetryDialog.createRetryEntry(
      this._transactionRest.findAll(qOption, this.qParams).map(response => response.data)
    );
  }

  exportPDF() {
    this.exportRecords.subscribe(records => {
      this._dataTableExport.export({
        records,
        mapOptions: this._dataTableExport.mapFromTableColumns(this.tableColumns),
        fileName: 'all-transactiones',
        extension: 'pdf',
        templateData: {
          title: this._translate.instant('ui.transaction.list.title'),
        },
      });
    });
  }

  exportExcel() {
    this.exportRecords.subscribe(records => {
      this._dataTableExport.export({
        records: records,
        mapOptions: this._dataTableExport.mapFromTableColumns(this.tableColumns),
        fileName: 'all-transactiones',
        extension: 'xls',
      });
    });
  }

  loadData() {
    this.elPageLoading.forceShow();

    const qOption = this._gridTable.generateApiQueryOptionFromGridInfo(
      this.gridDataSource.pager,
      true,
      this.gridTable,
    );

    this.populateQParams();

    this.subscriptions.load.forEach(sub => sub.unsubscribe());
    this.subscriptions.load.push(
      this.elRetryDialog.createRetryEntry(
        this._transactionRest.findAll(qOption, this.qParams)
          .finally(() => {
            this.elPageLoading.forceHide();
          })
      ).subscribe(response => {
        this.gridDataSource.updateFromApiPaginationResult(response.data);
        this.compReady = true;
      })
    );
  }

  public onGridChange() {
    this.gridLoadDataWrapper();
  }

  public onGridPageChange() {
    this.gridLoadDataWrapper();
  }
}

import * as _ from 'lodash';
import { Injectable } from '@angular/core';

@Injectable()
export class TransactionRoutingService {
  resolve(doc: any, action: string = 'detail') {
    if (_.has(doc, 'transactionType')) {
      const transactionType = _.get(doc, 'transactionType', '');
      const separatorIdx = transactionType.indexOf('_');
      const transType = _.kebabCase(transactionType.substr(separatorIdx + 1));
      const transSrc = transactionType.substr(0, separatorIdx);

      let baseUrl;
      if (transactionType === 'expense') {
        baseUrl = `/expenses`;
      } else {
        switch (transSrc) {
          case 'sales':
            baseUrl = `/sales/${transType}s`;
            break;
          case 'purchase':
            baseUrl = `/purchases/${transType}s`;
            break;
          case 'inventory':
            baseUrl = `/inventory-${transType}s`;
            break;
          case 'receive':
            baseUrl = `/receive-${transType}s`;
            break;
          case 'spend':
            baseUrl = `/spend-${transType}s`;
            break;
        }
      }

      if (baseUrl) {
        const urls = [baseUrl];
        if (action === 'create') {
          urls.push('create');
        }
        if (action === 'detail' || action === 'update') {
          urls.push(doc.id || doc.transactionId);
        }
        if (action === 'update') {
          urls.push('update');
        }
        return urls;
      }
    }

    return [];
  }
}

import 'app/rxjs-imports.ts';

import { ViewChild } from '@angular/core';
import { FormArray } from '@angular/forms';
import * as _ from 'lodash';
import { AutoComplete } from 'primeng/components/autocomplete/autocomplete';
import { Observable } from 'rxjs';

import { CommonService, InjectorService } from '../../core/core.module';
import { SalesRestService } from '../sales/sales-rest.service';
import { TransactionFormBComponent } from './transaction-form.bcomponent';

export class TransactionFormSalesBComponent extends TransactionFormBComponent {
  @ViewChild('customerAC') elCustomerAC: AutoComplete;

  public _salesRest = InjectorService.get<SalesRestService>(SalesRestService);

  transactionGroup = 'sales';

  ACCustomerHandler: any;

  init() {
    this.registerHook('postProductLoad', event => {
      const { formIndex, srcProductVariant } = event.data;
      return this.updateProductsConditionalPrice(formIndex).do(() => {
        this.page._changeDetectorRef.detectChanges();
        this.syncLineAmount(formIndex);
      });
    });

    this.ACCustomerHandler = CommonService.remoteACItemsHandler({
      remoteParams: this.ACBusinessPartnerParams.bind(this),
      remoteRequest: this._businessPartnerRest.findAllCustomersPicker.bind(this._businessPartnerRest),
      remoteRequestMap: (response) => response.data,
      element: () => this.elCustomerAC,
      ACConfig: {
        prependNull: true,
      },
    });
  }

  onCustomerClear() {
    this.form.get('customerId').reset(this.getDefaultValue('master').customerId);
  }

  onCustomerSelected(srcCustomer: IBusinessPartner, patchEmail: boolean = true, patchMobile: boolean = true) {
    const loading = this.page.createLoading();

    return (this._businessPartnerRest.load(srcCustomer.id).do(customer => {
      const valueToPatch: any = {
        customer: customer,
        customerId: customer.id,
      };
      if (patchEmail) {
        valueToPatch.customerEmail = customer.email;
      }
      if (patchMobile) {
        valueToPatch.customerMobile = customer.mobile;
      }
      this.form.patchValue(valueToPatch);
      this.page._changeDetectorRef.detectChanges();
    }).switchMap(
      customer => this.callHook('afterCustomerSelected', customer)
        .switchMap(() => Observable.of(customer))
    ).switchMap(
      customer => this.updateProductsConditionalPrice()
        .switchMap(() => Observable.of(customer))
    )).finally(() => {
      loading.dispose();
    });
  }

  onCustomerCreated(customer: IBusinessPartner) {
    this.onCustomerSelected(customer).subscribe(null, null, () => {
      this.elQuickCreateModal.hide();
    });
  }

  generateLineProductDescription(productVariant: IProductVariant): string {
    const salesDescription = (productVariant.product.salesDescription || '').trim();
    const description = `${(salesDescription) ? salesDescription : productVariant.name.trim()}`;
    return description;
  }

  updateProductsConditionalPrice(targetIndex?: number): Observable<any> {
    const observables = [];
    const defaultCondition: any = { priceLevel: { isMaster: true } };
    let condition: any = defaultCondition;

    if (this.form.controls.customer.value) {
      const customer = this.form.controls.customer.value;
      if (customer.defaultSalesPriceLevelId) {
        condition = { priceLevelId: customer.defaultSalesPriceLevelId };
      }
    }

    let lines = _.toPlainObject((<FormArray>this.form.controls.lines).value);
    if (targetIndex >= 0) {
      lines = _.omitBy(lines, (line, lineIdx) => targetIndex !== parseInt(lineIdx, 0));
    }

    _.forEach(lines, (line, lineIdx) => {
      if (line.productVariantId) {
        observables.push(
          Observable.create(observer => {
            if (line.productVariant && line.productVariant.prices && line.productVariant.prices.length > 0) {
              let targetPrice: any = _.find(line.productVariant.prices, condition);

              if (!targetPrice) {
                targetPrice = _.find(line.productVariant.prices, defaultCondition);
              }

              if (targetPrice) {
                const lineFormGroup = (<FormArray>this.form.controls.lines).at(<any>lineIdx);
                lineFormGroup.patchValue({
                  unitPrice: targetPrice.unitPrice,
                });
              }
            }
            observer.next(false);
            observer.complete();
          }));
      }
    });

    return Observable.combineLatest(observables);
  }
}

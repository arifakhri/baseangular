import 'app/rxjs-imports.ts';

import { FormArray, FormGroup } from '@angular/forms';
import * as _ from 'lodash';

import { AccountingService, InjectorService } from '../../core/core.module';
import { BaseFormBComponent } from '../../shared/base/base.module';
import { SpinnerService } from '../../shared/spinner/spinner.service';
import { BusinessPartnerRestService } from '../business-partner/business-partner-rest.service';
import { ProductVariantRestService } from '../product-variant/product-variant.module';
import { MTax } from '../tax/tax.model';

export class TransactionFormBComponent extends BaseFormBComponent {
  public _businessPartnerRest = InjectorService.get<BusinessPartnerRestService>(BusinessPartnerRestService);
  public _productVariantRest = InjectorService.get<ProductVariantRestService>(ProductVariantRestService);
  public _spinner = InjectorService.get<SpinnerService>(SpinnerService);
  public _accounting = InjectorService.get<AccountingService>(AccountingService);

  transactionGroup: string;
  withSubTotal: boolean = true;

  getDefaultValue(type: 'master' | 'line'): any {
    return {};
  }

  buildFormChildLine(): FormGroup {
    return new FormGroup({});
  }

  lineConditionalValidation(formGroup: FormGroup) { }

  ACBusinessPartnerParams(event: any, type: string) {
    const qParams: any = {};

    if (type === 'search') {
      qParams['keyword'] = event.query;
    }

    return [{
      filter: [],
      sort: [],
      take: 30,
      skip: 0,
      includeTotalCount: false
    }, qParams];
  }

  ACProductVariantParams(event: any, type: string) {
    const qParams: any = {};

    if (type === 'search') {
      qParams['keyword'] = event.query;
    }

    return [{
      filter: [],
      sort: [],
      take: 30,
      skip: 0,
      includeTotalCount: false
    }, qParams];
  }

  onProductClear(formIndex: number) {
    const lineFormGroup = (<FormArray>this.form.get('lines')).at(formIndex);
    lineFormGroup.get('productVariantId').reset(this.getDefaultValue('line').productVariantId);
  }

  generateLineProductDescription(productVariant: IProductVariant): string {
    // const description = `${productVariant.product.name}
    // ${productVariant.attribute1Value ? productVariant.attribute1Value : ''}
    // ${productVariant.attribute2Value ? productVariant.attribute2Value : ''}
    // ${productVariant.attribute3Value ? productVariant.attribute3Value : ''}`.trim();

    const description = `${productVariant.name}`.trim();
    return description;
  }

  onProductSelected(formIndex: number, srcProductVariant: IProductVariant, targetSpinnerElement?: any) {
    const spinner = this._spinner.show({
      element: targetSpinnerElement,
      loaderType: 3,
    });

    const lineFormGroup = (<FormArray>this.form.controls.lines).controls[formIndex];
    if (lineFormGroup.get('productVariantId').value) {
      lineFormGroup.reset(this.getDefaultValue('line'));
    }

    let obs = this._productVariantRest.load(srcProductVariant.id).do(productVariant => {

      const description = this.generateLineProductDescription(productVariant);

      lineFormGroup.patchValue({
        product: productVariant.product,
        productId: productVariant.productId,
        productVariant,
        productVariantId: productVariant.id,
        description,
        uom: productVariant.product.uom,
        uomId: productVariant.product.uomId,
      });
      if (!lineFormGroup.get('qty').value) {
        lineFormGroup.get('qty').setValue(1);
      }

      if (_.has(productVariant, 'product.incomeAccountId') && lineFormGroup.get('incomeAccountId')) {
        lineFormGroup.get('incomeAccountId').setValue(productVariant.product.incomeAccountId);
      }

      if (_.has(productVariant, 'product.expenseAccountId') && lineFormGroup.get('expenseAccountId')) {
        lineFormGroup.get('expenseAccountId').setValue(productVariant.product.expenseAccountId);
      }

      this.syncLineTax(formIndex);
    });

    if (this.hasHook('postProductLoad')) {
      obs = obs.concat(this.callHook('postProductLoad', { formIndex, srcProductVariant }));
    }

    obs = obs.finally(() => {
      spinner.dispose();
    });

    return obs;
  }

  resetLines() {
    const formArray = <FormArray>this.form.controls.lines;
    while (formArray.length) {
      formArray.removeAt(0);
    }
    this.addLines();
    this.withSubTotal ? this.syncSubTotal() : this.syncTotal();
  }

  addLines(length: number = 2) {
    const formArray = (<FormArray>this.form.controls.lines);
    _.range(length).forEach(() => {
      const newFormGroup = this.buildFormChildLine();
      formArray.push(newFormGroup);
    });
    this.page._changeDetectorRef.detectChanges();
  }

  assignQuickCreatedEntity(entity: any, storageVar: string = null) {
    if (storageVar) {
      this.relatedData[storageVar].push(entity);
    }
    this.quickCreateAC.selectItem(entity);
    this.elQuickCreateModal.hide();
  }

  applyDoc() {
    this.page.onCompLoadingDone('loadRelated').subscribe(() => {
      this.resetLines();
      const linesFormArray = <FormArray>this.form.controls.lines;
      const currentLinesLength = linesFormArray.length;
      if (this.doc.lines.length > currentLinesLength) {
        this.addLines(this.doc.lines.length - currentLinesLength);
      }
      this.form.patchValue({ lines: this.doc.lines });

      this.page._changeDetectorRef.detectChanges();

      for (let i = 0; i < this.doc.lines.length; i++) {
        this.syncLineAmount(i);
      }
    });
  }

  clearLine(index: number) {
    const formArray = <FormArray>this.form.controls.lines;
    if (formArray.length > index && formArray.length !== 1) {
      formArray.removeAt(index);
      this.withSubTotal ? this.syncSubTotal() : this.syncTotal();
    } else {
      const lineFormGroup = <FormGroup>formArray.at(index);
      lineFormGroup.reset(this.getDefaultValue('line'));

      this.lineConditionalValidation(lineFormGroup);
      this.syncLineAmount(index);
    }
  }

  clearLineWithTotalOnly(index: number, valueToRemove: string) {
    const formArray = <FormArray>this.form.controls.lines;
    if (formArray.length > index && formArray.length !== 1) {
      formArray.removeAt(index);
    } else {
      const lineFormGroup = <FormGroup>formArray.at(index);
      lineFormGroup.get(valueToRemove).setValue(null);
    }
    this.syncTotal();
  }

  countLineSubtotal(line: any) {
    const qty = line.qty || 0;
    const unitPrice = line.unitPrice || 0;
    const discountAmount = line.discountAmount || 0;
    const total = (discountAmount !== 0)
      ? this._accounting.roundMoney(qty * (unitPrice - discountAmount))
      : this._accounting.roundMoney(qty * unitPrice);

    return total;
  }

  syncLineDiscount(formIndex: number, clearPercent: boolean = false, syncLineAmount: boolean = true) {
    const lineFormGroup = (<FormArray>this.form.controls.lines).at(formIndex);

    const qty = lineFormGroup.get('qty').value || 0;
    let discAmount = 0;

    if (!clearPercent) {
      const unitPrice = lineFormGroup.get('unitPrice').value || 0;
      const discPercent = lineFormGroup.get('discountPercent').value || 0;
      discAmount = (unitPrice * discPercent) / 100;
      lineFormGroup.get('discountAmount').setValue(discAmount);
    } else {
      lineFormGroup.get('discountPercent').reset(null);
      discAmount = lineFormGroup.get('discountAmount').value || 0;
    }

    const discTotal = this._accounting.roundMoney(discAmount * qty);
    lineFormGroup.get('totalDiscount').setValue(discTotal);

    if (syncLineAmount) {
      this.syncLineAmount(formIndex);
    }
  }

  syncLineTax(formIndex: number) {
    const lineFormGroup = (<FormArray>this.form.controls.lines).at(formIndex);
    const productVariant = lineFormGroup.get('product').value;
    if (
      this.form.get('taxed').value &&
      this.doc && this.doc.lines[formIndex] &&
      this.doc.lines[formIndex].taxId && this.doc.lines[formIndex].tax
    ) {
      lineFormGroup.patchValue({
        taxId: this.doc.lines[formIndex].taxId,
        tax: this.doc.lines[formIndex].tax,
      });
    } else if (this.form.get('taxed').value && productVariant.salesTaxId && productVariant.salesTax) {
      lineFormGroup.patchValue({
        taxId: productVariant.salesTaxId,
        tax: productVariant.salesTax,
      });
    }
  }

  syncLineAmount(formIndex: number) {
    const lineFormGroup = (<FormArray>this.form.controls.lines).at(formIndex);
    const lineFormValues = lineFormGroup.value;

    if (lineFormValues.discountPercent && lineFormValues.discountPercent > 0) {
      this.syncLineDiscount(formIndex, false, false);
    }

    const amount = this.countLineSubtotal(lineFormGroup.value);
    lineFormGroup.get('amount').setValue(amount);

    this.withSubTotal ? this.syncSubTotal() : this.syncTotal();
  }

  syncSubTotal() {
    const lineFormGroupValues = this.form.controls.lines.value;
    const subTotal = _.sumBy(lineFormGroupValues, 'amount');

    this.form.get('subtotal').setValue(subTotal);

    this.syncTotalDiscount('discountAmount', this.form.get('discountPercent').value || 0);

    this.syncTax();
  }

  syncTotalDiscount(formControlToCount: string, inputValue: number) {
    switch (formControlToCount) {
      case 'discountPercent':
        this.form.get(formControlToCount).setValue(null);
        break;
      case 'discountAmount':
        const subtotal = this.form.get('subtotal').value;
        const discountAmount = (inputValue && subtotal !== 0)
          ? this._accounting.roundMoney((subtotal * inputValue) / 100) || 0
          : 0;

        this.form.get(formControlToCount).setValue(discountAmount);
        break;
    }

    this.syncTax();
  }

  syncTax() {
    let taxAmount = 0;
    let targetDiscountPercent = 0;

    const discountAmount = this.form.get('discountAmount').value || 0;
    const discountPercent = this.form.get('discountPercent').value || 0;
    const subTotal = this.form.get('subtotal').value || 0;
    if ((discountAmount > 0 || discountPercent > 0) && subTotal > 0) {
      targetDiscountPercent = discountPercent || (discountAmount * 100 / subTotal) || 0;
    }

    if (this.form.get('taxed').value) {
      const lineFormGroupValues = this.form.controls.lines.value;
      const linesByTax = _.groupBy(lineFormGroupValues, 'taxId');
      _.forEach(linesByTax, (lines, taxId) => {
        if (taxId !== 'null') {
          const productsPrice = _.sumBy(lines, 'amount') || 0;
          const tax = _.find<MTax>(this.relatedData.taxes, { id: taxId });
          let taxTotal = (productsPrice * tax.rate) / 100;
          if (targetDiscountPercent > 0) {
            taxTotal -= targetDiscountPercent * taxTotal / 100;
          }
          taxAmount += taxTotal;
        }
      });
    }

    taxAmount = this._accounting.roundMoney(taxAmount);
    this.form.get('taxAmount').setValue(taxAmount);

    this.syncTotal();
  }

  syncTotal() {
    const taxAmount = this.form.get('taxAmount').value || 0;
    const subTotal = this.form.get('subtotal').value || 0;

    const discTotal = this.form.get('discountAmount').value || 0;

    const shippingCharge = this.form.get('shippingCharge') != null ? this.form.get('shippingCharge').value : 0;

    const adjustmentAmount = this.form.get('adjustmentAmount').value || 0;

    const total = this._accounting.roundMoney((subTotal - discTotal) + taxAmount + shippingCharge + adjustmentAmount);
    this.form.get('total').setValue(total);
  }

  onTaxedToggle(checked: boolean) {
    _((<FormArray>this.form.get('lines')).controls)
      .filter(lineFormGroup => lineFormGroup.get('productVariant').value)
      .forEach((lineFormGroup, lineFormGroupIndex) => {
        if (checked) {
          this.syncLineTax(lineFormGroupIndex);
        } else {
          lineFormGroup.get('tax').reset(this.getDefaultValue('line').tax);
          lineFormGroup.get('taxId').reset(this.getDefaultValue('line').taxId);
        }
      });

    this.syncTax();
  }
}

import * as _ from 'lodash';
import { Component, Input, OnInit } from '@angular/core';

import { AccountingService } from '../../../core/core.module';
import { TransactionRestService } from '../transaction-rest.service';

@Component({
  selector: 'app-transaction-journal',
  templateUrl: './transaction-journal.component.html',
})
export class TransactionJournalComponent implements OnInit {
  @Input() transaction: any;

  journals: ITransactionJournal[] = [];

  constructor(
    public _accounting: AccountingService,
    private _transactionRest: TransactionRestService,
  ) { }

  ngOnInit() {
    this._transactionRest.transactionJournal(this.transactionType, this.transaction.id).subscribe(journals => {
      this.journals = journals;
    });
  }

  get transactionType() {
    return this.transaction.transactionType;
  }

  get debitTotal() {
    return _.sumBy(this.journals, 'debit');
  }

  get creditTotal() {
    return _.sumBy(this.journals, 'credit');
  }
}

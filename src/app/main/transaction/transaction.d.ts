declare interface ITransaction {
  transactionType: string;
  transactionTypeGroup: string;
  transactionId: string;
  transactionNumber: string;
  transactionDate: string;
  transactionDueDate: string;
  contactId: string;
  otherRefNumber: string;
  description: string;
  total: number;
  balanceDue: number;
  externalChannelId: string;
  externalDocumentId: string;
  status: number;
  state: number;
  contact: {
    id: string;
    contactType: string;
    code: string;
    displayName: string;
    company: string;
    fullName: string;
    phone: string;
    mobile: string;
    fax: string;
    email: string;
    website: string;
    inactive: boolean;
    postalAddress: {
      attention: string;
      street1: string;
      street2: string;
      city: string;
      stateProvince: string;
      countryId: string;
      postalCode: string;
      longitude: number;
      latitude: number;
      country: {
        code: string;
        name: string;
      }
    }
  }
}

declare interface ITransactionJournal {
  accountId: string;
  description: string;
  debit: number;
  credit: number;
  account: {
    id: string;
    accountClassId: string;
    accountClassName: string;
    accountClassSortOrder: number;
    accountTypeId: string;
    accountTypeName: string;
    accountTypeSortOrder: string;
    code: string;
    name: string;
    description: string;
    isChildAccount: boolean;
    level: number;
    locked: boolean;
    systemType: string;
    inactive: boolean;
  }
}

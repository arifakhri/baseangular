import * as _ from 'lodash';
import { Component, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import 'app/rxjs-imports.ts';

import { SalesCreditNoteFormService } from '../form/sales-credit-note-form.service';
import { SalesCreditNoteRestService } from '../sales-credit-note-rest.service';
import { SalesCreditNoteService } from '../sales-credit-note.service';

import { BaseCreateBComponent } from '../../../shared/base/base.module';
import { MSalesCreditNote } from '../sales-credit-note.model';

@Component({
  selector: 'app-sales-credit-note-create',
  templateUrl: 'sales-credit-note-create.component.html'
})
export class SalesCreditNoteCreateComponent extends BaseCreateBComponent  {
  @ViewChild('reportModal') elReportModal: ModalDirective;

  constructor(
    private _salesCreditNote: SalesCreditNoteService,
    private _salesCreditNoteForm: SalesCreditNoteFormService,
    private _salesCreditNoteRest: SalesCreditNoteRestService
  ) {
    super();

    this.componentId = 'SalesCreditNoteCreate';
    this.headerTitle = 'ui.salesCreditNote.create.title';
    this.containerType = 1;
    this.routeURL = '/sales/credit-notes';
    this.entrySuccessI18n = 'success.salesCreditNote.create';

    this.autoprintButtons = [{
      checkboxConfigId: 'autoprint.screditnote.create',
      checkboxLabel: 'ui.salesCreditNote.create.action.apCheckbox.report',
      previewLabel: 'ui.salesCreditNote.create.action.previewReport',
      onPreview: () => {
        this.saveAndPrint();
      }
    }];

    this.registerHook('buildForm', event => {
      this._salesCreditNoteForm.setFormDefinitions(this.form);
    });

    this.registerHook('save', event => {
      let creditNote = new MSalesCreditNote;
      creditNote = _.assign(creditNote, this.form.value);
      this._salesCreditNote.normalizeDoc(creditNote);

      const isPrinting = _.get(event, 'data.print');

      return this._salesCreditNoteRest.create(creditNote, { includeLines: isPrinting });
    });
   }

  async saveAndPrint() {
    try {
      const submitted = await this.onSubmit({ saveConfirm: true, print: true });
      if (submitted) {
        this._salesCreditNoteRest.loadRelatedData().subscribe(related => {
          this.doc['companyInfo'] = related.settings.companyInfo;
          this.elReportModal.show();
        });
      }
    } catch (e) {

    }
  }
}

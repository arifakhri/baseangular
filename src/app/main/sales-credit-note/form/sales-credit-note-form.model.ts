import { MBase } from '../../../core/common/base.model';

export class MSalesCreditNoteForm extends MBase {
  adjustmentAmount: number = 0;
  branchId: number = 0;
  customerEmail: string;
  customer: string;
  customerId: string;
  customerMobile: string;
  customerNote: string;
  customCustomerName: string;
  customerRefNumber: string;
  description: string;
  discountAmount: number = 0;
  discountPercent: number = null;
  lines: MSalesCreditNoteFormLine[] = [];
  note: string;
  subtotal: number = 0;
  taxAmount: number = 0;
  taxed: boolean = true;
  total: number = 0;
  totalPaid: number = 0;
  transactionDate: string;
  transactionNumber: string;

  parentTransactionType: string;
}

export class MSalesCreditNoteFormLine extends MBase {
  amount: number = 0;
  description: string;
  discountAmount: number = 0;
  discountPercent: number = null;
  incomeAccountId: string;
  note: string;
  parentTransactionId: string;
  parentTransactionLineId: string;
  product: string;
  productId: string;
  productVariant: string;
  productVariantId: string;
  qty: number = 0;
  tax: string;
  taxId: string;
  taxRate: number = 0;
  totalDiscount: number = 0;
  unitPrice: number = 0;
  uomConversion: number = 0;
  uom: string;
  uomId: string;
  warehouseId: string;
}

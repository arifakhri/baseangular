import * as _ from 'lodash';
import { Component, OnChanges, OnInit, SimpleChanges, } from '@angular/core';
import { FormGroup } from '@angular/forms';
import 'app/rxjs-imports.ts';

import { AccountingService, AutocompleteService, CommonService } from '../../../core/core.module';
import { SalesCreditNoteFormService } from '../form/sales-credit-note-form.service';
import { SalesCreditNoteRestService } from '../sales-credit-note-rest.service';

import { TransactionFormSalesBComponent } from '../../transaction/transaction-form-sales.bcomponent';

@Component({
  selector: 'app-sales-credit-note-form',
  templateUrl: 'sales-credit-note-form.component.html'
})
export class SalesCreditNoteFormComponent extends TransactionFormSalesBComponent implements OnChanges, OnInit {
  ACProductVariantHandler = CommonService.remoteACItemsHandler({
    remoteParams: this.ACProductVariantParams.bind(this),
    remoteRequest: this._productVariantRest.findAllPicker.bind(this._productVariantRest),
    remoteRequestMap: (response) => response.data,
  });

  constructor(
    public _autocomplete: AutocompleteService,
    public _accounting: AccountingService,
    public _salesCreditNoteForm: SalesCreditNoteFormService,
    private _salesCreditNoteRest: SalesCreditNoteRestService,
  ) {
    super();
    super.init();
    this.componentId = 'SalesCreditNoteForm';

    this.registerHook('loadRelated', event => {
      return this._salesCreditNoteRest.loadRelatedData();
    }, relatedData => {
      this._salesCreditNoteForm.patchTransactionSettings(this.form, relatedData.settings);
    });

    this.registerHook('patchForm', event => {
      this.applyDoc();
    });
  }

  ngOnInit() {
    super.ngOnInit();

    this.addLines();
  }

  ngOnChanges(changes: SimpleChanges) {
     if (_.has(changes, 'doc') && this.doc) {
      this.applyDoc();
    }
  }

  getDefaultValue(type: 'master' | 'line'): any {
    return this._salesCreditNoteForm.getDefaultValues(type);
  }

  buildFormChildLine() {
    return this._salesCreditNoteForm.buildFormChildLine();
  }

  lineConditionalValidation(formGroup: FormGroup) {
    this._salesCreditNoteForm.lineConditionalValidation(formGroup);
  }
}

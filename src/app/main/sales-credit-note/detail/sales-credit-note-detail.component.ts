import swal from 'sweetalert2';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';
import { TranslateService } from '@ngx-translate/core';

import { AccountingService, RouteStateService } from '../../../core/core.module';
import { AdminLayoutService } from '../../../layouts/layouts.module';
import { SalesCreditNoteRestService } from '../sales-credit-note-rest.service';
import { SalesInvoiceRestService } from '../../sales-invoice/sales-invoice-rest.service';

@Component({
  selector: 'app-sales-credit-note-detail',
  templateUrl: 'sales-credit-note-detail.component.html'
})
export class SalesCreditNoteDetailComponent implements OnInit {
  creditOffered: boolean = false;
  credits: ISalesInvoice[] = [];
  doc: ISalesCreditNote;
  routeParams: any;

  mainReportParams: any;

  @ViewChild('quickRefundModal') elQuickRefundModal: ModalDirective;
  @ViewChild('allocateCreditModal') elAllocateCreditModal: ModalDirective;

  constructor(
    public _accounting: AccountingService,
    private _adminLayout: AdminLayoutService,
    private _route: ActivatedRoute,
    private _routeState: RouteStateService,
    private _salesCreditNoteRest: SalesCreditNoteRestService,
    private _salesInvoiceRest: SalesInvoiceRestService,
    private _translate: TranslateService,
  ) {
    this._adminLayout.containerType = 1;
    this._route.params.subscribe(params => this.routeParams = params);
  }

  ngOnInit() {
    this.loadData().subscribe();
    this.loadRelatedData();
  }

  loadData(): Observable<ISalesCreditNote> {
    return this._salesCreditNoteRest.load(this.routeParams.id).do(creditNote => {
      this.doc = creditNote;

      this._salesInvoiceRest.findAll({
        filter: [],
        sort: [],
        take: 100,
        includeTotalCount: false,
        skip: 0,
      }, {
        customerId: this.doc.customerId,
        isOpen: true,
      }).subscribe(invoices => {
        this.credits = invoices.data || [];

      });
    });
  }

  loadRelatedData() {
    this._salesCreditNoteRest.loadRelatedData().subscribe(relatedData => {
      this.mainReportParams = {
        companyInfo: relatedData.settings.companyInfo,
      };
    });
  }


  onRefundQuickCreated(refund) {
    this.loadData().subscribe(() => {
      this.elQuickRefundModal.hide();
    });
  }
  onCreditAllocated(record: ISalesCreditNote) {
    this.loadData().subscribe(() => {
      this.elAllocateCreditModal.hide();
    });
  }

  offerAllocateCredit() {
    if (!this.creditOffered && this.credits.length && this._routeState.previousUrl === '/sales/credit-notes/create') {
      swal({
        title: this._translate.instant('confirm.salesCreditNote.detail.offerAllocateCredit.label'),
        text: this._translate.instant('confirm.salesCreditNote.detail.offerAllocateCredit.description'),
        type: 'question',
        showCancelButton: true,
      }).then(() => {
        this.elAllocateCreditModal.show();
      }).catch();

      this.creditOffered = true;
    }
  }
}

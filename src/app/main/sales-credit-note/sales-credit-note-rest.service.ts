import { Injectable } from '@angular/core';
import 'app/rxjs-imports.ts';

import { ApiQueryOption, RequestService } from '../../core/core.module';

import { APP_CONST } from '../../app.const';

@Injectable()
export class SalesCreditNoteRestService {
  baseURL = `${APP_CONST.API_MAIN}/sales/credit-notes`;
  request = this._request.new(this.baseURL);

  constructor(
    private _request: RequestService,
  ) { }

  create(creditNote: ISalesCreditNote, queryParams: any = {}) {
    return this.request.post<ISalesCreditNote>(``, creditNote, { params: queryParams });
  }

  findAll(queryOption: ApiQueryOption, queryParams: any = {}) {
    return this.request.post<IApiPaginationResult<ISalesCreditNote>>(`q`, queryOption, { params: queryParams });
  }

  load(creditNoteId: string) {
    return this.request.get<ISalesCreditNote>(`${creditNoteId}?includeLines=true`);
  }

  loadRelatedData() {
    return this.request.get<{ taxes: ITax[], warehouses: IWarehouse[], settings: ISettings }>(`entry-related-data`);
  }

  update(creditNoteId: string, updateObj: ISalesCreditNote, queryParams: any = {}) {
    return this.request.put<ISalesCreditNote>(`${creditNoteId}`, updateObj, { params: queryParams });
  }

  void(creditNoteId: string) {
    return this.request.put<ISalesCreditNote>(`${creditNoteId}/void`, {});
  }

  allocateCredit(creditNoteId: string, credits: ISalesAllocateCredit[]) {
    return this.request.put<ISalesCreditNote>(`${creditNoteId}/allocate-credit`, credits);
  }
}

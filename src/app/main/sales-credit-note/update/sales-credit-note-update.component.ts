import * as _ from 'lodash';
import * as moment from 'moment';
import { Component, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';

import { SalesCreditNoteFormService } from '../form/sales-credit-note-form.service';
import { SalesCreditNoteRestService } from '../sales-credit-note-rest.service';
import { SalesCreditNoteService } from '../sales-credit-note.service';

import { BaseUpdateBComponent } from '../../../shared/base/base.module';

@Component({
  selector: 'app-sales-credit-note-update',
  templateUrl: 'sales-credit-note-update.component.html',
})
export class SalesCreditNoteUpdateComponent extends BaseUpdateBComponent {
  @ViewChild('reportModal') elReportModal: ModalDirective;

  constructor(
    private _salesCreditNote: SalesCreditNoteService,
    private _salesCreditNoteForm: SalesCreditNoteFormService,
    private _salesCreditNoteRest: SalesCreditNoteRestService,
  ) {
    super();

    this.componentId = 'SalesCreditNoteUpdate';
    this.headerTitle = 'ui.salesCreditNote.update.title';
    this.containerType = 1;
    this.routeURL = '/sales/credit-notes';
    this.entrySuccessI18n = 'success.salesCreditNote.update';

    this.registerHook('buildForm', event => {
      this._salesCreditNoteForm.setFormDefinitions(this.form);
    });

    this.registerHook('load', event => {
      return this._salesCreditNoteRest.load(this.page.routeParams.id).switchMap(doc => {
        if (doc.transactionDate) {
          doc.transactionDate = moment(doc.transactionDate).toDate();
        }
        return Observable.of(doc);
      });
    });

    this.registerHook('save', event => {
      const creditNote: ISalesCreditNote = Object.assign({}, this.doc, this.form.value);
      this._salesCreditNote.normalizeDoc(creditNote);

      const isPrinting = _.get(event, 'data.print');

      return this._salesCreditNoteRest.update(this.page.routeParams.id, creditNote, { includeLines: isPrinting });
    });
  }

  async saveAndPrint() {
    try {
      const submitted = await this.onSubmit({ saveConfirm: true, print: true });
      if (submitted) {
        this._salesCreditNoteRest.loadRelatedData().subscribe(related => {
          this.doc['companyInfo'] = related.settings.companyInfo;
          this.elReportModal.show();
        });
      }
    } catch (e) {

    }
  }
}

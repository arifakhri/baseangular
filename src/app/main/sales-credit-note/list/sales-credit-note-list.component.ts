import * as _ from 'lodash';
import * as SnackBar from 'node-snackbar';
import swal from 'sweetalert2';
import { ActivatedRoute } from '@angular/router';
import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { DataTable } from 'primeng/components/datatable/datatable';
import { ModalDirective, PopoverDirective } from 'ngx-bootstrap';
import { Observable, Subscription } from 'rxjs';
import 'app/rxjs-imports.ts';
import { TranslateService } from '@ngx-translate/core';

import { SpinnerDirective } from '../../../shared/spinner/spinner.directive';

import {
  AccountingService,
  ExportDataTableService,
  GridTableDataSource,
  GridTableService,
  GridTableToggleService,
  SystemMessageService,
} from '../../../core/core.module';
import { AdminLayoutService } from '../../../layouts/layouts.module';
import { SalesCreditNoteMoreFilterService } from '../more-filter/sales-credit-note-more-filter.service';
import { SalesCreditNoteRestService } from '../sales-credit-note-rest.service';
import { RetryDialogComponent } from '../../../shared/retry-dialog/retry-dialog.component';

@Component({
  selector: 'app-sales-credit-note-list',
  templateUrl: 'sales-credit-note-list.component.html',
  providers: [SystemMessageService]
})
export class SalesCreditNoteListComponent implements OnDestroy, OnInit {
  @ViewChild('gridTable') public gridTable: DataTable;
  @ViewChild('pageLoading') elPageLoading: SpinnerDirective;
  @ViewChild('popMoreFilter') elPopMoreFilter: PopoverDirective;
  @ViewChild('reportModal') elReportModal: ModalDirective;
  @ViewChild('retryDialog') elRetryDialog: RetryDialogComponent;

  compReady: boolean;
  doc: ISalesCreditNote;
  gridDataSource: GridTableDataSource<ISalesCreditNote> = new GridTableDataSource<ISalesCreditNote>();
  selectedRecords: ISalesCreditNote[] = [];

  combinedFilterValues: string = '';
  combinedFilterFields = {
    transactionNumber: true,
    'customer.displayName': true,
  };

  qParamsFilters: any = {
    keyword: null
  };
  moreFilterValues: IGridTableFilterParsed;

  tableColumns: IGridTableColumn[] = [{
    i18nLabel: 'ui.salesCreditNote.list.column.transactionDate',
    field: 'transactionDate',
    dateFormat: 'DD/MM/YYYY',
    sort: true,
  }, {
    i18nLabel: 'ui.salesCreditNote.list.column.transactionNumber',
    field: 'transactionNumber',
    link: (row: ISalesCreditNote) => {
      return `/sales/credit-notes/${row.id}`;
    },
    sort: true,
  }, {
    i18nLabel: 'ui.salesCreditNote.list.column.description',
    field: 'description',
    sort: true,
  }, {
    i18nLabel: 'ui.salesCreditNote.list.column.customerName',
    field: 'customer.displayName',
    sort: true,
    formatter: (value, row: ISalesCreditNote) => {
      return row.customCustomerName ? value + ' (' + row.customCustomerName + ')' : value;
    },
  }, {
    i18nLabel: 'ui.salesCreditNote.list.column.total',
    field: 'total',
    sort: true,
    formatter: (value, row: ISalesCreditNote) => {
      return this._accounting.ac.formatMoney(value, '');
    },
    columnClasses: 'right-align',
  }, {
    i18nLabel: 'ui.salesCreditNote.list.column.balanceDue',
    field: 'balanceDue',
    sortField: 'adjustmentAmount',
    sort: true,
    formatter: (value, row: ISalesCreditNote) => {
      return this._accounting.ac.formatMoney(value, '');
    },
    columnClasses: 'right-align',
  }, {
    i18nLabel: 'ui.salesCreditNote.list.column.status',
    field: 'status',
    sort: true,
    formatter: (value, row: ISalesCreditNote) => {
      return _.startCase(value);
    },
  }];
  tableColumnsShow: boolean = false;
  tableColumnsToggle = this._gridTableToggle.mapToggleOptionsFromColumns(this.tableColumns);

  retryDialogShownState: boolean = false;
  retryDialogOnRetry: Function;

  private loadDataTimeoutId: number;
  private gridFirstLoad: boolean = true;
  private subscriptions: { [key: string]: Subscription[] } = {
    load: []
  };

  constructor(
    public _accounting: AccountingService,
    private _activatedRoute: ActivatedRoute,
    private _adminLayout: AdminLayoutService,
    private _dataTableExport: ExportDataTableService,
    private _gridTable: GridTableService,
    private _gridTableToggle: GridTableToggleService,
    private _salesCreditNoteMoreFilter: SalesCreditNoteMoreFilterService,
    private _salesCreditNoteRest: SalesCreditNoteRestService,
    public _systemMessage: SystemMessageService,
    private _translate: TranslateService,
  ) {
    this._adminLayout.containerType = 1;
  }

  ngOnInit() {
    this.loadData();
  }

  ngOnDestroy() {
    this._salesCreditNoteMoreFilter.lastMoreFilterValues = {};
  }

  onMoreFilter(filters: IGridTableFilterParsed) {
    this.elPopMoreFilter.hide();

    this.moreFilterValues = filters;
    this.loadData();
  }

  populateFilters() {
    let newFilters: any = _.pick(this.qParamsFilters, ['keyword']);

    if (this.moreFilterValues) {
      newFilters = Object.assign({}, newFilters, this.moreFilterValues.qParams);
    }

    this.qParamsFilters = newFilters;
  }

  gridLoadDataWrapper() {
    // Grid always load at the first time. we dont want that. we already call on ngOnInit
    if (this.gridFirstLoad) {
      this.gridFirstLoad = false;
      return;
    }

    if (this.loadDataTimeoutId) {
      window.clearTimeout(this.loadDataTimeoutId);
      this.loadDataTimeoutId = null;
    }

    this.loadDataTimeoutId = window.setTimeout(() => {
      this.loadData();
    }, 300);
  }

  showVoidDialog(creditNoteId: string) {
    swal({
      title: this._translate.instant('confirm.salesCreditNote.void.label'),
      text: this._translate.instant('confirm.salesCreditNote.void.description'),
      type: 'question',
      showCancelButton: true,
    })
      .then(() => {
        this.voidCreditNote(creditNoteId);
      })
      .catch(() => { });
  }

  voidCreditNote(creditNoteId: string) {
    this._salesCreditNoteRest.void(creditNoteId)
    .catch((error) => {
      this._systemMessage.log({
        message: error.response.data[0].errorMessage,
        type: 'error'
      });
      return Observable.throw(error);
    })
    .subscribe(response => {
      this.gridLoadDataWrapper();
      SnackBar.show({
        text: this._translate.instant('success.salesCreditNote.void'),
        pos: 'bottom-right'
      });
    });
  }

  printReport(creditNoteId: string) {
    this._salesCreditNoteRest.load(creditNoteId).subscribe(response => {
      this.doc = response;
      this._salesCreditNoteRest.loadRelatedData().subscribe(related => {
        this.doc['companyInfo'] = related.settings.companyInfo;
        this.elReportModal.show();
      });
    });
  }

  get exportRecords(): Observable<ISalesCreditNote[]> {
    const qOption = {
      ...this._gridTable.generateApiQueryOptionFromGridInfo(
        this.gridDataSource.pager,
        false,
        this.gridTable,
        this._gridTable.generateCombinedApiFilters(this.combinedFilterValues, this.combinedFilterFields)
      ),
      skip: 0,
      take: this._dataTableExport.exportLimit,
    };

    this.populateFilters();

    return this.elRetryDialog.createRetryEntry(
      this._salesCreditNoteRest.findAll(qOption, this.qParamsFilters).map(response => response.data)
    );
  }

  exportPDF() {
    this.exportRecords.subscribe(records => {
      this._dataTableExport.export({
        records,
        mapOptions: this._dataTableExport.mapFromTableColumns(this.tableColumns),
        fileName: 'sales-credit-note',
        extension: 'pdf',
        templateData: {
          title: this._translate.instant('ui.transaction.list.title'),
        },
      });
    });
  }

  exportExcel() {
    this.exportRecords.subscribe(records => {
      this._dataTableExport.export({
        records: records,
        mapOptions: this._dataTableExport.mapFromTableColumns(this.tableColumns),
        fileName: 'sales-credit-note',
        extension: 'xls',
      });
    });
  }

  loadData() {
    this.elPageLoading.forceShow();

    const qOption = this._gridTable.generateApiQueryOptionFromGridInfo(
      this.gridDataSource.pager,
      true,
      this.gridTable,
      this._gridTable.generateCombinedApiFilters(this.combinedFilterValues, this.combinedFilterFields)
    );

    this.populateFilters();

    this.subscriptions.load.forEach(sub => sub.unsubscribe());
    this.subscriptions.load.push(
      this.elRetryDialog.createRetryEntry(
        this._salesCreditNoteRest.findAll(qOption, this.qParamsFilters)
          .finally(() => {
            this.elPageLoading.forceHide();
          })
      ).subscribe(response => {
        this.gridDataSource.updateFromApiPaginationResult(response);
        this.compReady = true;
      })
    );
  }

  public onGridChange() {
    this.gridLoadDataWrapper();
  }

  public onGridPageChange() {
    this.gridLoadDataWrapper();
  }
}

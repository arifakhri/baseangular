import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SalesCreditNoteCreateComponent } from './create/sales-credit-note-create.component';
import { SalesCreditNoteDetailComponent } from './detail/sales-credit-note-detail.component';
import { SalesCreditNoteListComponent } from './list/sales-credit-note-list.component';
import { SalesCreditNoteUpdateComponent } from './update/sales-credit-note-update.component';

import { AuthorizationService } from '../../core/core.module';

export const routes: Routes = [{
  path: '',
  children: [{
    path: '',
    component: SalesCreditNoteListComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'salesCreditNote.list',
    },
  }, {
    path: 'create',
    component: SalesCreditNoteCreateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'salesCreditNote.create',
    },
  }, {
    path: ':id',
    component: SalesCreditNoteDetailComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'salesCreditNote.detail',
    },
  }, {
    path: ':id/update',
    component: SalesCreditNoteUpdateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'salesCreditNote.update',
    },
  }]
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class SalesCreditNoteRoutingModule { }

import { NgModule } from '@angular/core';

import { SalesCreditNoteModule } from './sales-credit-note.module';
import { SalesCreditNoteRoutingModule } from './sales-credit-note-routing.module';

@NgModule({
  imports: [
    SalesCreditNoteModule,
    SalesCreditNoteRoutingModule
  ]
})
export class SalesCreditNoteLazyModule { }

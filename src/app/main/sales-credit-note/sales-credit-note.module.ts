import { AutoCompleteModule } from 'primeng/components/autocomplete/autocomplete';
import { CalendarModule } from 'primeng/components/calendar/calendar';
import { CheckboxModule } from 'primeng/components/checkbox/checkbox';
import { DataTableModule } from 'primeng/components/datatable/datatable';
import { ListboxModule } from 'primeng/components/listbox/listbox';
import { BsDropdownModule, ModalModule, PaginationModule, PopoverModule } from 'ngx-bootstrap';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { SalesCreditNoteCreateComponent } from './create/sales-credit-note-create.component';
import { SalesCreditNoteDetailComponent } from './detail/sales-credit-note-detail.component';
import { SalesCreditNoteFormComponent } from './form/sales-credit-note-form.component';
import { SalesCreditNoteListComponent } from './list/sales-credit-note-list.component';
import { SalesCreditNoteMoreFilterComponent } from './more-filter/sales-credit-note-more-filter.component';
import { SalesCreditNoteUpdateComponent } from './update/sales-credit-note-update.component';

import { SalesCreditNoteFormService } from './form/sales-credit-note-form.service';
import { SalesCreditNoteMoreFilterService } from './more-filter/sales-credit-note-more-filter.service';
import { SalesCreditNoteRestService } from './sales-credit-note-rest.service';
import { SalesCreditNoteService } from './sales-credit-note.service';

import { BusinessPartnerModule } from '../business-partner/business-partner.module';
import { CoreModule } from '../../core/core.module';
import { ProductVariantModule } from '../product-variant/product-variant.module';
import { SalesModule } from '../sales/sales.module';
import { SalesPaymentCreditListModule } from '../sales/payment-credit-list/sales-payment-credit-list.module';
import { SalesRefundModule } from '../sales-refund/sales-refund.module';
import { SharedModule } from '../../shared/shared.module';
import { TransactionModule } from '../transaction/transaction.module';

export const PROVIDERS = [
  SalesCreditNoteFormService,
  SalesCreditNoteMoreFilterService,
  SalesCreditNoteRestService,
  SalesCreditNoteService,
];

@NgModule({
  imports: [
    AutoCompleteModule,
    BsDropdownModule,
    BusinessPartnerModule,
    CalendarModule,
    CheckboxModule,
    CommonModule,
    CoreModule,
    DataTableModule,
    FlexLayoutModule,
    FormsModule,
    HttpModule,
    ListboxModule,
    ModalModule,
    PaginationModule,
    PopoverModule,
    ProductVariantModule,
    ReactiveFormsModule,
    SalesPaymentCreditListModule,
    RouterModule,
    SalesModule,
    SalesRefundModule,
    SharedModule,
    TransactionModule,
    TranslateModule,
  ],
  declarations: [
    SalesCreditNoteCreateComponent,
    SalesCreditNoteDetailComponent,
    SalesCreditNoteFormComponent,
    SalesCreditNoteListComponent,
    SalesCreditNoteMoreFilterComponent,
    SalesCreditNoteUpdateComponent,
  ],
})
export class SalesCreditNoteModule { }

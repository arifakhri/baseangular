import { Injectable } from '@angular/core';

import { ReportsRestService } from '../reports/reports-rest.service';

@Injectable()
export class ReportsPurchasesRestService {
  constructor(
    private _reportRest: ReportsRestService,
  ) { }

  findAll(reportParams: any = {}) {
    return this._reportRest.requestReport.post<IApiPaginationResult<IPurchasesInvoice>>(
      'purchase-list',
      reportParams,
    );
  }

}

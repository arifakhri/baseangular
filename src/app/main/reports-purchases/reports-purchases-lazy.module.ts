import { NgModule } from '@angular/core';

import { ReportsPurchasesModule } from './reports-purchases.module';
import { ReportsPurchasesRoutingModule } from './reports-purchases-routing.module';

@NgModule({
  imports: [
    ReportsPurchasesModule,
    ReportsPurchasesRoutingModule,
  ],
})
export class ReportsPurchasesLazyModule { }

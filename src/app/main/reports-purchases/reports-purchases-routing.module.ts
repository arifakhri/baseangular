import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ReportsPurchasesPreviewComponent } from './preview/reports-purchases-preview.component';

import { AuthorizationService } from '../../core/core.module';

const routes: Routes = [{
  path: '',
  children: [{
    path: '',
    component: ReportsPurchasesPreviewComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'reportsPurchases.preview'
    }
  }]
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ],
})
export class ReportsPurchasesRoutingModule { }

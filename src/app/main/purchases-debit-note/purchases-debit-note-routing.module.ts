import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PurchasesDebitNoteCreateComponent } from './create/purchases-debit-note-create.component';
import { PurchasesDebitNoteDetailComponent } from './detail/purchases-debit-note-detail.component';
import { PurchasesDebitNoteListComponent } from './list/purchases-debit-note-list.component';
import { PurchasesDebitNoteUpdateComponent } from './update/purchases-debit-note-update.component';

import { AuthorizationService } from '../../core/core.module';

export const routes: Routes = [{
  path: '',
  children: [{
    path: '',
    component: PurchasesDebitNoteListComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'purchasesDebitNote.list',
    },
  }, {
    path: 'create',
    component: PurchasesDebitNoteCreateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'purchasesDebitNote.create',
    },
  }, {
    path: ':id',
    component: PurchasesDebitNoteDetailComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'purchasesDebitNote.detail',
    },
  }, {
    path: ':id/update',
    component: PurchasesDebitNoteUpdateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'purchasesDebitNote.update',
    },
  }]
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class PurchasesDebitNoteRoutingModule { }

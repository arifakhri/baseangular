import * as _ from 'lodash';
import * as moment from 'moment';
import { Component, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';

import { PurchasesDebitNoteFormService } from '../form/purchases-debit-note-form.service';
import { PurchasesDebitNoteRestService } from '../purchases-debit-note-rest.service';
import { PurchasesDebitNoteService } from '../purchases-debit-note.service';

import { BaseUpdateBComponent } from '../../../shared/base/base.module';

@Component({
  selector: 'app-purchases-debit-note-update',
  templateUrl: 'purchases-debit-note-update.component.html',
})
export class PurchasesDebitNoteUpdateComponent extends BaseUpdateBComponent {
  @ViewChild('reportModal') elReportModal: ModalDirective;

  constructor(
    private _purchasesDebitNote: PurchasesDebitNoteService,
    private _purchasesDebitNoteForm: PurchasesDebitNoteFormService,
    private _purchasesDebitNoteRest: PurchasesDebitNoteRestService,
  ) {
    super();

    this.componentId = 'PurchasesDebitNoteUpdate';
    this.headerTitle = 'ui.purchasesDebitNote.update.title';
    this.containerType = 1;
    this.routeURL = '/purchases/debit-notes';
    this.entrySuccessI18n = 'success.purchasesDebitNote.update';


    this.registerHook('buildForm', event => {
      this._purchasesDebitNoteForm.setFormDefinitions(this.form);
    });

    this.registerHook('load', event => {
      return this._purchasesDebitNoteRest.load(this.page.routeParams.id).switchMap(doc => {
        if (doc.transactionDate) {
          doc.transactionDate = moment(doc.transactionDate).toDate();
        }
        return Observable.of(doc);
      });
    });

    this.registerHook('save', event => {
      const debitNote: IPurchasesDebitNote = Object.assign({}, this.doc, this.form.value);
      this._purchasesDebitNote.normalizeDoc(debitNote);

      const isPrinting = _.get(event, 'data.print');

      return this._purchasesDebitNoteRest.update(this.page.routeParams.id, debitNote, { includeLines: isPrinting });
    });
  }

  async saveAndPrint() {
    try {
      const submitted = await this.onSubmit({ saveConfirm: true, print: true });
      if (submitted) {
        this._purchasesDebitNoteRest.loadRelatedData().subscribe(related => {
          this.doc['companyInfo'] = related.settings.companyInfo;
          this.elReportModal.show();
        });
      }
    } catch (e) {

    }
  }
}

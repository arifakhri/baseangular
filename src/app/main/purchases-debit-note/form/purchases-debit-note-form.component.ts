import * as _ from 'lodash';
import { Component, OnChanges, OnInit, SimpleChanges, } from '@angular/core';
import { FormGroup } from '@angular/forms';
import 'app/rxjs-imports.ts';

import { AccountingService, AutocompleteService, CommonService } from '../../../core/core.module';
import { PurchasesDebitNoteFormService } from '../form/purchases-debit-note-form.service';
import { PurchasesDebitNoteRestService } from '../purchases-debit-note-rest.service';
import { PurchasesInvoiceCopyDebitNoteService } from '../../purchases-invoice/purchases-invoice-copy-debit-note.service';

import { TransactionFormPurchasesBComponent } from '../../transaction/transaction-form-purchases.bcomponent';

@Component({
  selector: 'app-purchases-debit-note-form',
  templateUrl: 'purchases-debit-note-form.component.html'
})
export class PurchasesDebitNoteFormComponent extends TransactionFormPurchasesBComponent implements OnChanges, OnInit {
  ACProductVariantHandler = CommonService.remoteACItemsHandler({
    remoteParams: this.ACProductVariantParams.bind(this),
    remoteRequest: this._productVariantRest.findAllPicker.bind(this._productVariantRest),
    remoteRequestMap: (response) => response.data,
  });

  copyTransaction: boolean = false;

  constructor(
    public _autocomplete: AutocompleteService,
    public _accounting: AccountingService,
    public _purchasesDebitNoteForm: PurchasesDebitNoteFormService,
    private _purchasesDebitNoteRest: PurchasesDebitNoteRestService,
    private _purchasesInvoiceCopyDebitNote: PurchasesInvoiceCopyDebitNoteService,
  ) {
    super();
    super.init();
    this.componentId = 'PurchasesDebitNoteForm';

    this.registerHook('loadRelated', event => {
      return this._purchasesDebitNoteRest.loadRelatedData();
    }, relatedData => {
      this._purchasesDebitNoteForm.patchTransactionSettings(this.form, relatedData.settings);

      if (this._purchasesInvoiceCopyDebitNote.docOnHold) {
        const parentDoc = <any>this._purchasesInvoiceCopyDebitNote.docOnHold;
        parentDoc.transactionNumber = null;

        this.copyTransaction = true;
        this.doc = parentDoc;

        this._purchasesInvoiceCopyDebitNote.docOnHold = null;
        this.applyDoc();
        this.form.patchValue(this.doc);
      }
    });

    this.registerHook('patchForm', event => {
      this.applyDoc();
    });

  }

  ngOnInit() {
    super.ngOnInit();

    this.addLines();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (_.has(changes, 'doc') && this.doc) {
      this.applyDoc();
    }
  }

  getDefaultValue(type: 'master' | 'line'): any {
    return this._purchasesDebitNoteForm.getDefaultValues(type);
  }

  buildFormChildLine() {
    return this._purchasesDebitNoteForm.buildFormChildLine();
  }

  lineConditionalValidation(formGroup: FormGroup) {
    this._purchasesDebitNoteForm.lineConditionalValidation(formGroup);
  }

  onVendorSelected(...params) {
    super.onVendorSelected(params[0], params[1]);

    if (this.copyTransaction) {
      this._purchasesInvoiceCopyDebitNote.reset(this.form);
    }
  }
}

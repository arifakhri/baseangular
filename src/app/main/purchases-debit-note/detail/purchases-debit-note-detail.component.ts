import swal from 'sweetalert2';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';
import { TranslateService } from '@ngx-translate/core';

import { AccountingService, RouteStateService } from '../../../core/core.module';
import { AdminLayoutService } from '../../../layouts/layouts.module';
import { PurchasesDebitNoteRestService } from '../purchases-debit-note-rest.service';
import { PurchasesInvoiceRestService } from '../../purchases-invoice/purchases-invoice-rest.service';

@Component({
  selector: 'app-purchases-debit-note-detail',
  templateUrl: 'purchases-debit-note-detail.component.html'
})
export class PurchasesDebitNoteDetailComponent implements OnInit {
  creditOffered: boolean = false;
  credits: IPurchasesInvoice[] = [];
  doc: IPurchasesDebitNote;
  routeParams: any;

  @ViewChild('quickRefundModal') elQuickRefundModal: ModalDirective;
  @ViewChild('allocateCreditModal') elAllocateCreditModal: ModalDirective;

  constructor(
    public _accounting: AccountingService,
    private _adminLayout: AdminLayoutService,
    private _route: ActivatedRoute,
    private _routeState: RouteStateService,
    public _purchasesDebitNoteRest: PurchasesDebitNoteRestService,
    private _purchasesInvoiceRest: PurchasesInvoiceRestService,
    private _translate: TranslateService,
  ) {
    this._adminLayout.containerType = 1;
    this._route.params.subscribe(params => this.routeParams = params);
  }

  ngOnInit() {
    this.loadData().subscribe();
  }

  loadData(): Observable<IPurchasesDebitNote> {
    return this._purchasesDebitNoteRest.load(this.routeParams.id).do(debitNote => {
      this.doc = debitNote;

      this._purchasesInvoiceRest.findAll({
        filter: [],
        sort: [],
        take: 100,
        includeTotalCount: false,
        skip: 0,
      }, {
        customerId: this.doc.vendorId,
        isOpen: true,
      }).subscribe(invoices => {
        this.credits = invoices.data || [];

        this.offerAllocateCredit();
      });
    });
  }

  onRefundQuickCreated(payment) {
    this.elQuickRefundModal.hide();
    this.loadData();
  }

  onCreditAllocated(record: ISalesCreditNote) {
    this.loadData().subscribe(() => {
      this.elAllocateCreditModal.hide();
    });
  }

  offerAllocateCredit() {
    if (!this.creditOffered && this.credits.length && this._routeState.previousUrl === '/purchases/debit-notes/create') {
      swal({
        title: this._translate.instant('confirm.purchasesDebitNote.detail.offerAllocateCredit.label'),
        text: this._translate.instant('confirm.purchasesDebitNote.detail.offerAllocateCredit.description'),
        type: 'question',
        showCancelButton: true,
      }).then(() => {
        this.elAllocateCreditModal.show();
      }).catch();

      this.creditOffered = true;
    }
  }
}

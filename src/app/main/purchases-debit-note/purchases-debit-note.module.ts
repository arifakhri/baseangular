import { AutoCompleteModule } from 'primeng/components/autocomplete/autocomplete';
import { CalendarModule } from 'primeng/components/calendar/calendar';
import { CheckboxModule } from 'primeng/components/checkbox/checkbox';
import { DataTableModule } from 'primeng/components/datatable/datatable';
import { ListboxModule } from 'primeng/components/listbox/listbox';
import { BsDropdownModule, ModalModule, PaginationModule, PopoverModule } from 'ngx-bootstrap';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { PurchasesDebitNoteCreateComponent } from './create/purchases-debit-note-create.component';
import { PurchasesDebitNoteDetailComponent } from './detail/purchases-debit-note-detail.component';
import { PurchasesDebitNoteFormComponent } from './form/purchases-debit-note-form.component';
import { PurchasesDebitNoteListComponent } from './list/purchases-debit-note-list.component';
import { PurchasesDebitNoteMoreFilterComponent } from './more-filter/purchases-debit-note-more-filter.component';
import { PurchasesDebitNoteUpdateComponent } from './update/purchases-debit-note-update.component';

import { PurchasesDebitNoteFormService } from './form/purchases-debit-note-form.service';
import { PurchasesDeditNoteMoreFilterService } from './more-filter/purchases-debit-note-more-filter.service';
import { PurchasesDebitNoteRestService } from './purchases-debit-note-rest.service';
import { PurchasesDebitNoteService } from './purchases-debit-note.service';

import { PurchasesRefundModule } from '../purchases-refund/purchases-refund.module';
import { BusinessPartnerModule } from '../business-partner/business-partner.module';
import { CoreModule } from '../../core/core.module';
import { ProductVariantModule } from '../product-variant/product-variant.module';
import { PurchasesModule } from '../purchases/purchases.module';
import { PurchasesPaymentModule } from '../purchases-payment/purchases-payment.module';
import { SharedModule } from '../../shared/shared.module';
import { TransactionModule } from '../transaction/transaction.module';
import { PurchasesPaymentCreditListModule } from '../purchases/payment-credit-list/purchases-payment-credit-list.module';

export const PROVIDERS = [
  PurchasesDebitNoteFormService,
  PurchasesDeditNoteMoreFilterService,
  PurchasesDebitNoteRestService,
  PurchasesDebitNoteService,
];

@NgModule({
  imports: [
    AutoCompleteModule,
    BsDropdownModule,
    BusinessPartnerModule,
    CalendarModule,
    CheckboxModule,
    CommonModule,
    CoreModule,
    DataTableModule,
    FlexLayoutModule,
    FormsModule,
    HttpModule,
    ListboxModule,
    ModalModule,
    PaginationModule,
    ReactiveFormsModule,
    RouterModule,
    SharedModule,
    PopoverModule,
    ProductVariantModule,
    PurchasesModule,
    PurchasesPaymentCreditListModule,
    PurchasesPaymentModule,
    PurchasesRefundModule,
    TranslateModule,
    TransactionModule,
  ],
  declarations: [
    PurchasesDebitNoteCreateComponent,
    PurchasesDebitNoteDetailComponent,
    PurchasesDebitNoteFormComponent,
    PurchasesDebitNoteListComponent,
    PurchasesDebitNoteMoreFilterComponent,
    PurchasesDebitNoteUpdateComponent,
  ],
})
export class PurchasesDebitNoteModule { }

import * as _ from 'lodash';
import { Component, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import 'app/rxjs-imports.ts';

import { PurchasesDebitNoteFormService } from '../form/purchases-debit-note-form.service';
import { PurchasesDebitNoteRestService } from '../purchases-debit-note-rest.service';
import { PurchasesDebitNoteService } from '../purchases-debit-note.service';

import { BaseCreateBComponent } from '../../../shared/base/base.module';
import { MPurchasesDebitNote } from '../purchases-debit-note.model';

@Component({
  selector: 'app-purchases-debit-note-create',
  templateUrl: 'purchases-debit-note-create.component.html'
})
export class PurchasesDebitNoteCreateComponent extends BaseCreateBComponent {
  @ViewChild('reportModal') elReportModal: ModalDirective;

  constructor(
    private _purchasesDebitNote: PurchasesDebitNoteService,
    private _purchasesDebitNoteForm: PurchasesDebitNoteFormService,
    private _purchasesDebitNoteRest: PurchasesDebitNoteRestService
  ) {
    super();

    this.componentId = 'PurchasesDebitNoteCreate';
    this.headerTitle = 'ui.purchasesDebitNote.create.title';
    this.containerType = 1;
    this.routeURL = '/purchases/debit-notes';
    this.entrySuccessI18n = 'success.purchasesDebitNote.create';


    this.autoprintButtons = [{
      checkboxConfigId: 'autoprint.sdebitnote.create',
      checkboxLabel: 'ui.purchasesDebitNote.create.action.apCheckbox.report',
      previewLabel: 'ui.purchasesDebitNote.create.action.previewReport',
      onPreview: () => {
        this.saveAndPrint();
      }
    }];

    this.registerHook('buildForm', event => {
      this._purchasesDebitNoteForm.setFormDefinitions(this.form);
    });

    this.registerHook('save', event => {
      let debitNote = new MPurchasesDebitNote;
      debitNote = _.assign(debitNote, this.form.value);
      this._purchasesDebitNote.normalizeDoc(debitNote);

      const isPrinting = _.get(event, 'data.print');

      return this._purchasesDebitNoteRest.create(debitNote, { includeLines: isPrinting });
    });
  }

  async saveAndPrint() {
    try {
      const submitted = await this.onSubmit({ saveConfirm: true, print: true });
      if (submitted) {
        this._purchasesDebitNoteRest.loadRelatedData().subscribe(related => {
          this.doc['companyInfo'] = related.settings.companyInfo;
          this.elReportModal.show();
        });
      }
    } catch (e) {

    }
  }
}

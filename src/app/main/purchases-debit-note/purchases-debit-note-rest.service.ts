import { Injectable } from '@angular/core';
import 'app/rxjs-imports.ts';

import { ApiQueryOption, RequestService } from '../../core/core.module';

import { APP_CONST } from '../../app.const';

@Injectable()
export class PurchasesDebitNoteRestService {
  baseURL = `${APP_CONST.API_MAIN}/purchases/debit-notes`;
  request = this._request.new(this.baseURL);

  constructor(
    private _request: RequestService,
  ) { }

  create(debitNote: IPurchasesDebitNote, queryParams: any = {}) {
    return this.request.post<IPurchasesDebitNote>(``, debitNote, { params: queryParams });
  }

  findAll(queryOption: ApiQueryOption, queryParams: any = {}) {
    return this.request.post<IApiPaginationResult<IPurchasesDebitNote>>(`q`, queryOption, { params: queryParams });
  }

  load(debitNoteId: string) {
    return this.request.get<IPurchasesDebitNote>(`${debitNoteId}?includeLines=true`);
  }

  loadRelatedData() {
    return this.request.get<{ taxes: ITax[], warehouses: IWarehouse[], settings: ISettings }>(`entry-related-data`);
  }

  update(debitNoteId: string, updateObj: IPurchasesDebitNote, queryParams: any = {}) {
    return this.request.put<IPurchasesDebitNote>(`${debitNoteId}`, updateObj, { params: queryParams });
  }

  void(debitNoteId: string) {
    return this.request.put<IPurchasesDebitNote>(`${debitNoteId}/void`, {});
  }

  allocateCredit(creditNoteId: string, credits: IPurchasesDebitNote[]) {
    return this.request.put<IPurchasesDebitNote>(`${creditNoteId}/allocate-credit`, credits);
  }
}

import { NgModule } from '@angular/core';

import { PurchasesDebitNoteModule } from './purchases-debit-note.module';
import { PurchasesDebitNoteRoutingModule } from './purchases-debit-note-routing.module';

@NgModule({
  imports: [
    PurchasesDebitNoteModule,
    PurchasesDebitNoteRoutingModule
  ]
})
export class PurchasesDebitNoteLazyModule { }

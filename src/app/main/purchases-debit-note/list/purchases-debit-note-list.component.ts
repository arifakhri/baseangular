import * as _ from 'lodash';
import * as SnackBar from 'node-snackbar';
import swal from 'sweetalert2';
import { ActivatedRoute } from '@angular/router';
import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { DataTable } from 'primeng/components/datatable/datatable';
import { ModalDirective, PopoverDirective } from 'ngx-bootstrap';
import { Observable, Subscription } from 'rxjs';
import 'app/rxjs-imports.ts';
import { TranslateService } from '@ngx-translate/core';

import { SpinnerDirective } from '../../../shared/spinner/spinner.directive';

import {
  AccountingService,
  ExportDataTableService,
  GridTableDataSource,
  GridTableService,
  GridTableToggleService,
  SystemMessageService,
} from '../../../core/core.module';
import { AdminLayoutService } from '../../../layouts/layouts.module';
import { PurchasesDeditNoteMoreFilterService } from '../more-filter/purchases-debit-note-more-filter.service';
import { PurchasesDebitNoteRestService } from '../purchases-debit-note-rest.service';
import { RetryDialogComponent } from '../../../shared/retry-dialog/retry-dialog.component';

@Component({
  selector: 'app-purchases-debit-note-list',
  templateUrl: 'purchases-debit-note-list.component.html',
  providers: [SystemMessageService]
})
export class PurchasesDebitNoteListComponent implements OnDestroy, OnInit {
  @ViewChild('gridTable') public gridTable: DataTable;
  @ViewChild('pageLoading') elPageLoading: SpinnerDirective;
  @ViewChild('popMoreFilter') elPopMoreFilter: PopoverDirective;
  @ViewChild('reportModal') elReportModal: ModalDirective;
  @ViewChild('retryDialog') elRetryDialog: RetryDialogComponent;

  compReady: boolean;
  doc: IPurchasesDebitNote;
  gridDataSource: GridTableDataSource<IPurchasesDebitNote> = new GridTableDataSource<IPurchasesDebitNote>();
  selectedRecords: IPurchasesDebitNote[] = [];

  combinedFilterValues: string = '';
  combinedFilterFields = {
    'vendor.displayName': true,
    transactionNumber: true,
    description: true,
  };

  qParamsFilters: any = {
    keyword: null
  };
  moreFilterValues: IGridTableFilterParsed;

  tableColumns: IGridTableColumn[] = [{
    i18nLabel: 'ui.purchasesDebitNote.list.column.transactionDate',
    field: 'transactionDate',
    dateFormat: 'DD/MM/YYYY',
    sort: true,
  }, {
    i18nLabel: 'ui.purchasesDebitNote.list.column.transactionNumber',
    field: 'transactionNumber',
    link: (row: IPurchasesDebitNote) => {
      return `/purchases/debit-notes/${row.id}`;
    },
    sort: true,
  }, {
    i18nLabel: 'ui.purchasesDebitNote.list.column.description',
    field: 'description',
    sort: true,
  }, {
    i18nLabel: 'ui.purchasesDebitNote.list.column.vendorName',
    field: 'vendor.displayName',
    sort: true,
  }, {
    i18nLabel: 'ui.purchasesDebitNote.list.column.total',
    field: 'total',
    sort: true,
    formatter: (value, row: IPurchasesDebitNote) => {
      return this._accounting.ac.formatMoney(value, '');
    },
    columnClasses: 'right-align',
  }, {
    i18nLabel: 'ui.purchasesDebitNote.list.column.balanceDue',
    field: 'balanceDue',
    sort: true,
    formatter: (value, row: IPurchasesDebitNote) => {
      return this._accounting.ac.formatMoney(value, '');
    },
    columnClasses: 'right-align',
  }, {
    i18nLabel: 'ui.purchasesDebitNote.list.column.status',
    field: 'status',
    sort: true,
    formatter: (value, row: IPurchasesDebitNote) => {
      return _.startCase(value);
    },
  }];
  tableColumnsShow: boolean = false;
  tableColumnsToggle = this._gridTableToggle.mapToggleOptionsFromColumns(this.tableColumns);

  retryDialogShownState: boolean = false;
  retryDialogOnRetry: Function;

  private loadDataTimeoutId: number;
  private gridFirstLoad: boolean = true;
  private subscriptions: { [key: string]: Subscription[] } = {
    load: []
  };

  constructor(
    public _accounting: AccountingService,
    private _activatedRoute: ActivatedRoute,
    private _adminLayout: AdminLayoutService,
    private _gridTable: GridTableService,
    private _dataTableExport: ExportDataTableService,
    private _gridTableToggle: GridTableToggleService,
    private _purchasesDebitNoteMoreFilter: PurchasesDeditNoteMoreFilterService,
    private _purchasesDebitNoteRest: PurchasesDebitNoteRestService,
    public _systemMessage: SystemMessageService,
    private _translate: TranslateService,
  ) {
    this._adminLayout.containerType = 1;
  }

  ngOnInit() {
    this.loadData();
  }

  ngOnDestroy() {
    this._purchasesDebitNoteMoreFilter.lastMoreFilterValues = {};
  }

  onMoreFilter(filters: IGridTableFilterParsed) {
    this.elPopMoreFilter.hide();

    this.moreFilterValues = filters;
    this.loadData();
  }

  populateFilters() {
    let newFilters: any = _.pick(this.qParamsFilters, ['keyword']);

    if (this.moreFilterValues) {
      newFilters = Object.assign({}, newFilters, this.moreFilterValues.qParams);
    }

    this.qParamsFilters = newFilters;
  }

  gridLoadDataWrapper() {
    // Grid always load at the first time. we dont want that. we already call on ngOnInit
    if (this.gridFirstLoad) {
      this.gridFirstLoad = false;
      return;
    }

    if (this.loadDataTimeoutId) {
      window.clearTimeout(this.loadDataTimeoutId);
      this.loadDataTimeoutId = null;
    }

    this.loadDataTimeoutId = window.setTimeout(() => {
      this.loadData();
    }, 300);
  }

  showVoidDialog(debitNoteId: string) {
    swal({
      title: this._translate.instant('confirm.purchasesDebitNote.void.label'),
      text: this._translate.instant('confirm.purchasesDebitNote.void.description'),
      type: 'question',
      showCancelButton: true,
    })
      .then(() => {
        this.voidCreditNote(debitNoteId);
      })
      .catch(() => { });
  }

  voidCreditNote(debitNoteId: string) {
    this._purchasesDebitNoteRest.void(debitNoteId)
      .catch((error) => {
        this._systemMessage.log({
          message: error.response.data[0].errorMessage,
          type: 'error'
        });
        return Observable.throw(error);
      })
      .subscribe(response => {
      this.gridLoadDataWrapper();
      SnackBar.show({
        text: this._translate.instant('success.purchasesDebitNote.void'),
        pos: 'bottom-right'
      });
    });
  }

  printReport(debitNoteId: string) {
    this._purchasesDebitNoteRest.load(debitNoteId).subscribe(response => {
      this.doc = response;
      this._purchasesDebitNoteRest.loadRelatedData().subscribe(related => {
        this.doc['companyInfo'] = related.settings.companyInfo;
        this.elReportModal.show();
      });
    });
  }

  get exportRecords(): Observable<IPurchasesDebitNote[]> {
    const qOption = {
      ...this._gridTable.generateApiQueryOptionFromGridInfo(
        this.gridDataSource.pager,
        false,
        this.gridTable,
        this._gridTable.generateCombinedApiFilters(this.combinedFilterValues, this.combinedFilterFields)
      ),
      skip: 0,
      take: this._dataTableExport.exportLimit,
    };

    this.populateFilters();

    return this.elRetryDialog.createRetryEntry(
      this._purchasesDebitNoteRest.findAll(qOption, this.qParamsFilters).map(response => response.data)
    );
  }

  exportPDF() {
    this.exportRecords.subscribe(records => {
      this._dataTableExport.export({
        records,
        mapOptions: this._dataTableExport.mapFromTableColumns(this.tableColumns),
        fileName: 'puchase-debit',
        extension: 'pdf',
        templateData: {
          title: this._translate.instant('ui.transaction.list.title'),
        },
      });
    });
  }

  exportExcel() {
    this.exportRecords.subscribe(records => {
      this._dataTableExport.export({
        records: records,
        mapOptions: this._dataTableExport.mapFromTableColumns(this.tableColumns),
        fileName: 'puchase-debit',
        extension: 'xls',
      });
    });
  }

  loadData() {
    this.elPageLoading.forceShow();

    const qOption = this._gridTable.generateApiQueryOptionFromGridInfo(
      this.gridDataSource.pager,
      true,
      this.gridTable,
      this._gridTable.generateCombinedApiFilters(this.combinedFilterValues, this.combinedFilterFields)
    );

    this.populateFilters();

    this.subscriptions.load.forEach(sub => sub.unsubscribe());
    this.subscriptions.load.push(
    this.elRetryDialog.createRetryEntry(
      this._purchasesDebitNoteRest.findAll(qOption, this.qParamsFilters)
        .finally(() => {
          this.elPageLoading.forceHide();
        })
    ).subscribe(response => {
        this.gridDataSource.updateFromApiPaginationResult(response);
        this.compReady = true;
      })
    );
  }

  public onGridChange() {
    this.gridLoadDataWrapper();
  }

  public onGridPageChange() {
    this.gridLoadDataWrapper();
  }
}

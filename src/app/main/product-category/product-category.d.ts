declare interface IProductCategory {
  id: string;
  name: string;
}
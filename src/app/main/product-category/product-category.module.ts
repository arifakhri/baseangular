import { BsDropdownModule, PaginationModule } from 'ngx-bootstrap';
import { CommonModule } from '@angular/common';
import { DataTableModule } from 'primeng/components/datatable/datatable';
import { ListboxModule } from 'primeng/components/listbox/listbox';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { ProductCategoryCreateComponent } from './create/product-category-create.component';
import { ProductCategoryFormComponent } from './form/product-category-form.component';
import { ProductCategoryListComponent } from './list/product-category-list.component';
import { ProductCategoryUpdateComponent } from './update/product-category-update.component';

import { ProductCategoryQuickCreateComponent } from './quick-create/product-category-quick-create.component';
import { ProductCategoryQuickFormComponent } from './quick-form/product-category-quick-form.component';

import { ProductCategoryRestService } from './product-category-rest.service';

import { CoreModule } from '../../core/core.module';
import { SharedModule } from '../../shared/shared.module';

export const PROVIDERS = [
  ProductCategoryRestService,
];

@NgModule({
  imports: [
    CommonModule,
    FlexLayoutModule,
    PaginationModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    TranslateModule,
    BsDropdownModule,
    DataTableModule,
    ListboxModule,
    CoreModule,
    SharedModule,
  ],
  declarations: [
    ProductCategoryCreateComponent,
    ProductCategoryFormComponent,
    ProductCategoryListComponent,
    ProductCategoryUpdateComponent,

    ProductCategoryQuickCreateComponent,
    ProductCategoryQuickFormComponent
  ],
  exports: [
    ProductCategoryQuickCreateComponent
  ]
})
export class ProductCategoryModule { }

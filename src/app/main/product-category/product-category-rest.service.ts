import { Injectable } from '@angular/core';
import 'app/rxjs-imports.ts';

import { ApiQueryOption, RequestService } from '../../core/core.module';

import { APP_CONST } from '../../app.const';

@Injectable()
export class ProductCategoryRestService {
  baseURL = `${APP_CONST.API_MAIN}/product-categories`;
  request = this._request.new(this.baseURL);

  constructor(
    private _request: RequestService,
  ) { }

  create(productCategory: IProductCategory) {
    return this.request.post<IProductCategory>(``, productCategory);
  }

  findAll(queryOption: ApiQueryOption) {
    return this.request.post<IApiPaginationResult<IProductCategory>>(`q`, queryOption);
  }

  load(productCategoryId: string) {
    return this.request.get<IProductCategory>(`${productCategoryId}`);
  }

  update(productCategoryId: string, updateObj: IProductCategory) {
    return this.request.put<IProductCategory>(`${productCategoryId}`, updateObj);
  }

  delete(productCategoryId: string) {
    return this.request.delete<any>(`${productCategoryId}`);
  }

  checkDuplicate(
    values: {
      id?: string;
      name: string;
    }
  ) {
    return this.request.post<boolean>(`check-duplicate`, values);
  }

  toggleInactive(productCategoryId: string, inactive: boolean) {
    return this.request.put<any>(`${productCategoryId}/mark-active/${!inactive}`, null, {
      headers: {
        'Content-Type': 'application/json'
      }
    });
  }
}

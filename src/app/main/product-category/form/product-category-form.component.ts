import { Component } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { BaseFormBComponent } from '../../../shared/base/base.module';

@Component({
  selector: 'app-product-category-form',
  templateUrl: 'product-category-form.component.html'
})
export class ProductCategoryFormComponent extends BaseFormBComponent {
  constructor() {
    super();
    this.componentId = 'ProductCategoryForm';
    this.registerHook('buildForm', event => {
      this.buildForm();
    });
  }

  buildForm() {
    const nameControl = new FormControl('', [Validators.required, Validators.maxLength(75)]);
    (<any>nameControl).validatorData = {
      maxLength: 75
    };

    this.form.addControl('name', nameControl);
    this.form.addControl('description', new FormControl(''));
  }
}

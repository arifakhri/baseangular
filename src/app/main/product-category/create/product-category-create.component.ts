import * as _ from 'lodash';
import { Component } from '@angular/core';
import { BaseCreateBComponent } from '../../../shared/base/base.module';
import { MProductCategory } from '../product-category.model';
import { ProductCategoryRestService } from '../product-category-rest.service';

@Component({
  selector: 'app-product-category-create',
  templateUrl: 'product-category-create.component.html'
})
export class ProductCategoryCreateComponent extends BaseCreateBComponent {
  
  constructor(
   private _productCategoryRest: ProductCategoryRestService
  ) { 
    super();
    this.componentId = 'ProductCategoryCreate';
    this.routeURL = '/product-categories';
    this.entrySuccessI18n = 'success.productCategory.create';
    this.headerTitle = 'ui.productCategory.create.title';
    this.registerHook('save', event => {
      const formValue = this.form.value;
      let productCategory = new MProductCategory;
      productCategory = _.assign(productCategory, formValue);
      return this._productCategoryRest.create(productCategory);
    });
   }
}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ProductCategoryCreateComponent } from './create/product-category-create.component';
import { ProductCategoryListComponent } from './list/product-category-list.component';
import { ProductCategoryUpdateComponent } from './update/product-category-update.component';

import { AuthorizationService } from '../../core/core.module';

export const routes: Routes = [{
  path: '',
  children: [{
    path: '',
    component: ProductCategoryListComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'productCategory.list'
    }
  }, {
    path: 'create',
    component: ProductCategoryCreateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'productCategory.create'
    }
  }, {
    path: ':id/update',
    component: ProductCategoryUpdateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'productCategory.update'
    }
  }]
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class ProductCategoryRoutingModule { }

import { NgModule } from '@angular/core';

import { ProductCategoryModule } from './product-category.module';
import { ProductCategoryRoutingModule } from './product-category-routing.module';

@NgModule({
  imports: [
    ProductCategoryModule,
    ProductCategoryRoutingModule
  ]
})
export class ProductCategoryLazyModule { }

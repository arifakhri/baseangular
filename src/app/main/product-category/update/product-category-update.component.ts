import * as _ from 'lodash';
import { Component } from '@angular/core';
import { BaseUpdateBComponent } from '../../../shared/base/base.module';
import { MProductCategory } from '../product-category.model';

import { ProductCategoryRestService } from '../product-category-rest.service';

@Component({
  selector: 'app-product-category-update',
  templateUrl: 'product-category-update.component.html'
})
export class ProductCategoryUpdateComponent extends BaseUpdateBComponent {
   constructor(
     private _productCategoryRest: ProductCategoryRestService,
  ) {
    super();
    this.componentId = 'ProductCategoryUpdate';
    this.routeURL = '/product-categories';
    this.entrySuccessI18n = 'success.productCategory.update';
    this.registerHook('load', event => {
      return this._productCategoryRest.load(this.page.routeParams.id);
    });
    this.registerHook('save', event => {
      const formValue = this.form.value;
      let productCategory = new MProductCategory;
      productCategory = _.assign(productCategory, this.doc, formValue);
      return this._productCategoryRest.update(this.page.routeParams.id, productCategory);
    });

  }
}

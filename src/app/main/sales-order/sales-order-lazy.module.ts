import { NgModule } from '@angular/core';

import { SalesOrderModule } from './sales-order.module';
import { SalesOrderRoutingModule } from './sales-order-routing.module';

@NgModule({
  imports: [
    SalesOrderModule,
    SalesOrderRoutingModule
  ]
})
export class SalesOrderLazyModule { }

import { AutoCompleteModule } from 'primeng/components/autocomplete/autocomplete';
import { BsDropdownModule, ModalModule, PaginationModule, PopoverModule } from 'ngx-bootstrap';
import { CalendarModule } from 'primeng/components/calendar/calendar';
import { CheckboxModule } from 'primeng/components/checkbox/checkbox';
import { CommonModule } from '@angular/common';
import { DataTableModule } from 'primeng/components/datatable/datatable';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { ListboxModule } from 'primeng/components/listbox/listbox';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { SalesOrderCreateComponent } from './create/sales-order-create.component';
import { SalesOrderDetailComponent } from './detail/sales-order-detail.component';
import { SalesOrderFormComponent } from './form/sales-order-form.component';
import { SalesOrderListComponent } from './list/sales-order-list.component';
import { SalesOrderMoreFilterComponent } from './more-filter/sales-order-more-filter.component';
import { SalesOrderUpdateComponent } from './update/sales-order-update.component';

import { SalesOrderCloneService } from './sales-order-clone.service';
import { SalesOrderCopyInvoiceService } from './sales-order-copy-invoice.service';
import { SalesOrderFormDPService } from './form/sales-order-form-dp.service';
import { SalesOrderFormService } from './form/sales-order-form.service';
import { SalesOrderMoreFilterService } from './more-filter/sales-order-more-filter.service';
import { SalesOrderRestService } from './sales-order-rest.service';
import { SalesOrderService } from './sales-order.service';

import { BusinessPartnerModule } from '../business-partner/business-partner.module';
import { CoreModule } from '../../core/core.module';
import { ProductVariantModule } from '../product-variant/product-variant.module';
import { SalesDownPaymentModule } from '../sales-down-payment/sales-down-payment.module';
import { SalesInvoiceModule } from '../sales-invoice/sales-invoice.module';
import { SalesModule } from '../sales/sales.module';
import { SharedModule } from '../../shared/shared.module';
import { ShippingDetailModule } from '../shipping-detail/shipping-detail.module';
import { ShippingPriceModule } from '../shipping-price/shipping-price.module';
import { TransactionModule } from '../transaction/transaction.module';

export const PROVIDERS = [
  SalesOrderCloneService,
  SalesOrderCopyInvoiceService,
  SalesOrderFormDPService,
  SalesOrderFormService,
  SalesOrderMoreFilterService,
  SalesOrderRestService,
  SalesOrderService,
];

@NgModule({
  imports: [
    AutoCompleteModule,
    BsDropdownModule,
    BusinessPartnerModule,
    CalendarModule,
    CheckboxModule,
    CommonModule,
    CoreModule,
    DataTableModule,
    FlexLayoutModule,
    FormsModule,
    HttpModule,
    ListboxModule,
    ModalModule,
    PaginationModule,
    PopoverModule,
    ProductVariantModule,
    ReactiveFormsModule,
    RouterModule,
    SalesDownPaymentModule,
    SalesInvoiceModule,
    SalesModule,
    SharedModule,
    ShippingDetailModule,
    ShippingPriceModule,
    TransactionModule,
    TranslateModule,
  ],
  declarations: [
    SalesOrderCreateComponent,
    SalesOrderDetailComponent,
    SalesOrderFormComponent,
    SalesOrderListComponent,
    SalesOrderMoreFilterComponent,
    SalesOrderUpdateComponent,
  ],
})
export class SalesOrderModule { }

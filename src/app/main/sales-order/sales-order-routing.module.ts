import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SalesOrderCreateComponent } from './create/sales-order-create.component';
import { SalesOrderDetailComponent } from './detail/sales-order-detail.component';
import { SalesOrderListComponent } from './list/sales-order-list.component';
import { SalesOrderUpdateComponent } from './update/sales-order-update.component';

import { AuthorizationService } from '../../core/core.module';

export const routes: Routes = [{
  path: '',
  children: [{
    path: '',
    component: SalesOrderListComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'salesOrder.list',
    },
  }, {
    path: 'create',
    component: SalesOrderCreateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'salesOrder.create',
    },
  }, {
    path: ':id',
    component: SalesOrderDetailComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'salesOrder.detail',
    },
  }, {
    path: ':id/update',
    component: SalesOrderUpdateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'salesOrder.update',
    },
  }]
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class SalesOrderRoutingModule { }

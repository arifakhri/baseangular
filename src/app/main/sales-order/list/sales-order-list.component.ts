import * as _ from 'lodash';
import * as SnackBar from 'node-snackbar';
import swal from 'sweetalert2';
import { ActivatedRoute } from '@angular/router';
import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { DataTable } from 'primeng/components/datatable/datatable';
import { Observable, Subscription } from 'rxjs';
import 'app/rxjs-imports.ts';
import { ModalDirective, PopoverDirective } from 'ngx-bootstrap';
import { TranslateService } from '@ngx-translate/core';

import { RetryDialogComponent } from '../../../shared/retry-dialog/retry-dialog.component';

import {
  AccountingService,
  ExportDataTableService,
  GridTableDataSource,
  GridTableService,
  GridTableToggleService,
  SystemMessageService,
} from '../../../core/core.module';
import { AdminLayoutService } from '../../../layouts/layouts.module';
import { SpinnerDirective } from '../../../shared/spinner/spinner.directive';
import { SalesOrderCloneService } from '../../sales-order/sales-order-clone.service';
import { SalesOrderCopyInvoiceService } from '../../sales-order/sales-order-copy-invoice.service';
import { SalesOrderMoreFilterService } from '../more-filter/sales-order-more-filter.service';
import { SalesOrderRestService } from '../sales-order-rest.service';

import { MSalesOrder } from '../sales-order.model';

@Component({
  selector: 'app-sales-order-list',
  templateUrl: 'sales-order-list.component.html',
  providers: [SystemMessageService]
})
export class SalesOrderListComponent implements OnDestroy, OnInit {
  @ViewChild('gridTable') gridTable: DataTable;
  @ViewChild('popMoreFilter') elPopMoreFilter: PopoverDirective;
  @ViewChild('quickInvoiceModal') elQuickInvoiceModal: PopoverDirective;
  @ViewChild('pageLoading') elPageLoading: SpinnerDirective;
  @ViewChild('quickDepositModal') elQuickDepositModal: ModalDirective;
  @ViewChild('reportModal') elReportModal: ModalDirective;
  @ViewChild('retryDialog') elRetryDialog: RetryDialogComponent;

  compReady: boolean;
  doc: MSalesOrder;
  order: MSalesOrder;
  orderes: MSalesOrder[] = [];
  gridDataSource: GridTableDataSource<MSalesOrder> = new GridTableDataSource<MSalesOrder>();
  selectedRecords: MSalesOrder[] = [];

  combinedFilterValues: string = '';
  combinedFilterFields = {
    transactionNumber: true,
    'customer.displayName': true,
  };

  qParamsFilters: any = {
    keyword: null
  };
  moreFilterValues: IGridTableFilterParsed;

  tableColumns: IGridTableColumn[] = [];
  tableColumnsShow: boolean = false;
  tableColumnsToggle: any;

  retryDialogShownState: boolean = false;
  retryDialogOnRetry: Function;

  quickDepositDoc: any;

  private loadDataTimeoutId: number;
  private gridFirstLoad: boolean = true;
  private subscriptions: { [key: string]: Subscription[] } = {
    load: []
  };

  constructor(
    public _accounting: AccountingService,
    private _activatedRoute: ActivatedRoute,
    private _adminLayout: AdminLayoutService,
    private _gridTable: GridTableService,
    private _gridTableToggle: GridTableToggleService,
    private _dataTableExport: ExportDataTableService,
    private _salesOrderClone: SalesOrderCloneService,
    private _salesOrderCopyInvoice: SalesOrderCopyInvoiceService,
    private _salesOrderMoreFilter: SalesOrderMoreFilterService,
    private _salesOrderRest: SalesOrderRestService,
    public _systemMessage: SystemMessageService,
    private _translate: TranslateService,
  ) {
    this._adminLayout.containerType = 1;
  }

  ngOnInit() {
    this.loadData();

    this.tableColumns = [{
      i18nLabel: 'ui.salesOrder.list.column.transactionDate',
      field: 'transactionDate',
      dateFormat: 'DD/MM/YYYY',
      sort: true,
    }, {
      i18nLabel: 'ui.salesOrder.list.column.transactionNumber',
      field: 'transactionNumber',
      link: (row: MSalesOrder) => {
        return `/sales/orders/${row.id}`;
      },
      sort: true,
    }, {
      i18nLabel: 'ui.salesOrder.list.column.description',
      field: 'description',
      sort: true,
    }, {
      i18nLabel: 'ui.salesOrder.list.column.customerName',
      field: 'customer.displayName',
      sort: true,
      formatter: (value, row: MSalesOrder) => {
        return row.customCustomerName ? value + ' (' + row.customCustomerName + ')' : value;
      },
    }, {
      i18nLabel: 'ui.salesOrder.list.column.deliveryDate',
      field: 'deliveryDate',
      sortField: 'orderExtension.deliveryDate',
      sort: true,
    }, {
      i18nLabel: 'ui.salesOrder.list.column.total',
      field: 'total',
      sort: true,
      formatter: (value, row: MSalesOrder) => {
        return this._accounting.ac.formatMoney(value, '');
      },
      columnClasses: 'right-align',
    }, {
      i18nLabel: 'ui.salesOrder.list.column.deposit',
      field: 'downPaymentAmount',
      sortField: 'orderExtension.downPaymentAmount',
      sort: true,
      formatter: (value, row: MSalesOrder) => {
        return this._accounting.ac.formatMoney(value, '');
      },
      columnClasses: 'right-align',
    }, {
      i18nLabel: 'ui.salesCreditNote.list.column.status',
      field: 'status',
      sort: true,
      formatter: (value, row: MSalesOrder) => {
        return _.startCase(value);
      },
    }];
    this.tableColumnsToggle = this._gridTableToggle.mapToggleOptionsFromColumns(this.tableColumns);
  }

  ngOnDestroy() {
    this._salesOrderMoreFilter.lastMoreFilterValues = {};
  }

  onMoreFilter(filters: IGridTableFilterParsed) {
    this.elPopMoreFilter.hide();

    this.moreFilterValues = filters;
    this.loadData();
  }

  populateFilters() {
    let newFilters: any = _.pick(this.qParamsFilters, ['keyword']);

    if (this.moreFilterValues) {
      newFilters = Object.assign({}, newFilters, this.moreFilterValues.qParams);
    }

    this.qParamsFilters = newFilters;
  }

  gridLoadDataWrapper() {
    // Grid always load at the first time. we dont want that. we already call on ngOnInit
    if (this.gridFirstLoad) {
      this.gridFirstLoad = false;
      return;
    }

    if (this.loadDataTimeoutId) {
      window.clearTimeout(this.loadDataTimeoutId);
      this.loadDataTimeoutId = null;
    }

    this.loadDataTimeoutId = window.setTimeout(() => {
      this.loadData();
    }, 300);
  }

  showVoidDialog(orderId: string) {
    swal({
      title: this._translate.instant('confirm.salesOrder.void.label'),
      text: this._translate.instant('confirm.salesOrder.void.description'),
      type: 'question',
      showCancelButton: true,
    })
      .then(() => {
        this.voidOrder(orderId);
      })
      .catch(() => { });
  }

  voidOrder(orderId: string) {
    this._salesOrderRest.void(orderId)
    .catch((error) => {
      this._systemMessage.log({
        message: error.response.data[0].errorMessage,
        type: 'error'
      });
      return Observable.throw(error);
    })
    .subscribe(response => {
      this.gridLoadDataWrapper();
      SnackBar.show({
        text: this._translate.instant('success.salesOrder.void'),
        pos: 'bottom-right'
      });
    });
  }

  printReport(orderId: string) {
    this._salesOrderRest.load(orderId).subscribe(response => {
      this.doc = response;
      this._salesOrderRest.loadRelatedData().subscribe(related => {
        this.doc['companyInfo'] = related.settings.companyInfo;
        this.elReportModal.show();
      });
    });
  }

  get exportRecords(): Observable<MSalesOrder[]> {
    const qOption = {
      ...this._gridTable.generateApiQueryOptionFromGridInfo(
        this.gridDataSource.pager,
        false,
        this.gridTable,
        this._gridTable.generateCombinedApiFilters(this.combinedFilterValues, this.combinedFilterFields)
      ),
      skip: 0,
      take: this._dataTableExport.exportLimit,
    };

    this.populateFilters();

    return this._salesOrderRest.findAll(qOption, this.qParamsFilters).map(response => response.data);
  }

  exportPDF() {
    this.exportRecords.subscribe(records => {
      this._dataTableExport.export({
        records,
        mapOptions: this._dataTableExport.mapFromTableColumns(this.tableColumns),
        fileName: 'sales-order',
        extension: 'pdf',
        templateData: {
          title: this._translate.instant('ui.transaction.list.title'),
        },
      });
    });
  }

  exportExcel() {
    this.exportRecords.subscribe(records => {
      this._dataTableExport.export({
        records: records,
        mapOptions: this._dataTableExport.mapFromTableColumns(this.tableColumns),
        fileName: 'sales-order',
        extension: 'xls',
      });
    });
  }

  loadData() {
    this.elPageLoading.forceShow();

    this.compReady = false;

    const qOption = this._gridTable.generateApiQueryOptionFromGridInfo(
      this.gridDataSource.pager,
      true,
      this.gridTable,
      this._gridTable.generateCombinedApiFilters(this.combinedFilterValues, this.combinedFilterFields)
    );

    this.populateFilters();

    this.subscriptions.load.forEach(sub => sub.unsubscribe());
    this.subscriptions.load.push(
      this.elRetryDialog.createRetryEntry(
        this._salesOrderRest.findAll(qOption, this.qParamsFilters)
          .finally(() => {
            this.elPageLoading.forceHide();
          })
      ).subscribe(response => {
        this.orderes = response.data;
        this.gridDataSource.updateFromApiPaginationResult(response);

        this.compReady = true;
      })
    );
  }

  cloneTransaction(doc) {
    this.elPageLoading.forceShow();

    this._salesOrderRest.load(doc.id).subscribe(newDoc => {
      this.elPageLoading.forceHide();

      this._salesOrderClone.copy(newDoc);
    });
  }

  showQuickInvoiceModal(doc) {
    this.elPageLoading.forceShow();

    this._salesOrderRest.load(doc.id).subscribe(newDoc => {
      this.order = newDoc;
      this._salesOrderCopyInvoice.copy(newDoc, false);
      this.elQuickInvoiceModal.show();
      this.elPageLoading.forceHide();
    });
  }

  didSalesInvoiceQuickCreate() {
    this.loadData();
    this.elQuickInvoiceModal.hide();
  }

  quickDepositReset() {
    this.quickDepositDoc = undefined;
    this.elQuickDepositModal.hide();
  }

  onQuickDeposited(deposit: ISalesDownPayment) {
    this.quickDepositReset();
    this.loadData();
  }

  public onGridChange() {
    this.gridLoadDataWrapper();
  }

  public onGridPageChange() {
    this.gridLoadDataWrapper();
  }
}

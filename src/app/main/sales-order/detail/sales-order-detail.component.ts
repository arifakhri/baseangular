import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';

import { AccountingService } from '../../../core/core.module';
import { AdminLayoutService } from '../../../layouts/layouts.module';
import { SalesOrderCloneService } from '../sales-order-clone.service';
import { SalesOrderCopyInvoiceService } from '../sales-order-copy-invoice.service';
import { SalesOrderRestService } from '../sales-order-rest.service';

import { MSalesOrder } from '../sales-order.model';

@Component({
  selector: 'app-sales-order-detail',
  templateUrl: 'sales-order-detail.component.html'
})
export class SalesOrderDetailComponent implements OnInit {
  doc: MSalesOrder;
  routeParams: any;
  mainReportParams: any;
  compLoading: boolean = true;

  @ViewChild('quickPayModal') elQuickPayModal: ModalDirective;

  constructor(
    public _accounting: AccountingService,
    private _adminLayout: AdminLayoutService,
    public _salesOrderClone: SalesOrderCloneService,
    public _salesOrderCopyInvoice: SalesOrderCopyInvoiceService,
    private _salesOrderRest: SalesOrderRestService,
    private _route: ActivatedRoute,
  ) {
    this._adminLayout.containerType = 1;
    this._route.params.subscribe(params => this.routeParams = params);
  }

  ngOnInit() {
    this.loadData();
    this.loadRelatedData();
  }

  loadData() {
    this._salesOrderRest.load(this.routeParams.id).subscribe(order => {
      this.doc = order;
      this.compLoading = false;
    });
  }

  loadRelatedData() {
    this._salesOrderRest.loadRelatedData().subscribe(relatedData => {
      this.mainReportParams = {
        companyInfo: relatedData.settings.companyInfo,
      };
    });
  }

  onPaymentQuickCreated(payment) {
    this.elQuickPayModal.hide();
    this.compLoading = true;
    this.loadData();
  }
}

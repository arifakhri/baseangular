import * as _ from 'lodash';
import { CustomValidators } from 'ng2-validation';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { Injectable } from '@angular/core';

import { MSalesOrderForm, MSalesOrderFormLine } from './sales-order-form.model';

import { StartingDataService } from '../../../core/core.module';

@Injectable()
export class SalesOrderFormService {
  constructor(
    private _startingData: StartingDataService,
  ) { }

  getDefaultValues(type: 'master' | 'line'): { [key: string]: any } {
    let model: any;
    switch (type) {
      case 'master':
        const startingData = this._startingData.data$.getValue();
        model = new MSalesOrderForm;
        model.defaultValues = {
          branchId: startingData.masterBranchId,
        };
        break;
      case 'line':
        model = new MSalesOrderFormLine;
        model.defaultValues = {
          uomConversion: 1,
        };
        break;
    }

    return model;
  }

  setFormDefinitions(form: FormGroup) {
    const customerEmailControl = new FormControl('', [CustomValidators.email, Validators.maxLength(150)]);
    (<any>customerEmailControl).validatorData = {
      maxLength: 150
    };
    const customerMobileControl = new FormControl('', [CustomValidators.digits, Validators.maxLength(30)]);
    (<any>customerMobileControl).validatorData = {
      maxLength: 30
    };
    const customerPrintNameControl = new FormControl('', Validators.maxLength(50));
    (<any>customerPrintNameControl).validatorData = {
      maxLength: 50
    };
    const customerRefNumberControl = new FormControl('', Validators.maxLength(30));
    (<any>customerRefNumberControl).validatorData = {
      maxLength: 30
    };
    const dropshipperEmailControl = new FormControl('', [CustomValidators.email, Validators.maxLength(150)]);
    (<any>dropshipperEmailControl).validatorData = {
      maxLength: 150
    };
    const dropshipperMobileControl = new FormControl('', [CustomValidators.digits, Validators.maxLength(30)]);
    (<any>dropshipperMobileControl).validatorData = {
      maxLength: 30
    };

    form.addControl('branchId', new FormControl);
    form.addControl('customer', new FormControl);
    form.addControl('customerId', new FormControl(null, Validators.required));
    form.addControl('customerEmail', customerEmailControl);
    form.addControl('customerMobile', customerMobileControl);
    form.addControl('billingAddress', new FormControl);
    form.addControl('shippingAddress', new FormControl);
    form.addControl('shippingMethod', new FormControl);
    form.addControl('shippingMethodId', new FormControl);
    form.addControl('shippingProvider', new FormControl);
    form.addControl('shippingProviderId', new FormControl);
    form.addControl('shippingProviderProduct', new FormControl);
    form.addControl('shippingProviderProductId', new FormControl);
    form.addControl('shippingInsurance', new FormControl(false));
    form.addControl('shippingTrackingNumber', new FormControl);
    form.addControl('description', new FormControl);
    form.addControl('dropshipperName', new FormControl);
    form.addControl('dropshipperPhoneNumber', dropshipperMobileControl);
    form.addControl('dropshipperEmail', dropshipperEmailControl);
    form.addControl('transactionNumber', new FormControl);
    form.addControl('customCustomerName', customerPrintNameControl);
    form.addControl('customerRefNumber', customerRefNumberControl);
    form.addControl('transactionDate', new FormControl(new Date(), Validators.required));
    form.addControl('deliveryDate', new FormControl);
    form.addControl('customerNote', new FormControl);
    form.addControl('subtotal', new FormControl);
    form.addControl('discountPercent', new FormControl('', CustomValidators.number));
    form.addControl('discountAmount', new FormControl(0, [Validators.required, CustomValidators.number]));
    form.addControl('taxAmount', new FormControl);
    form.addControl('shippingCharge', new FormControl(0, [Validators.required, CustomValidators.number]));
    form.addControl('adjustmentAmount', new FormControl(0, [Validators.required, CustomValidators.number]));
    form.addControl('total', new FormControl(0));
    form.addControl('taxed', new FormControl);
    form.addControl('note', new FormControl);
    form.addControl('isDropship', new FormControl(false));
    form.addControl('useCustomCustomerName', new FormControl(false));
    form.addControl('warehouse', new FormControl);
    form.addControl('warehouseId', new FormControl(null));
    form.addControl('lines', new FormArray([]));

    form.patchValue(this.getDefaultValues('master'));
  }

  buildFormChildLine() {
    const formGroup = new FormGroup({
      id: new FormControl(null),
      rowVersion: new FormControl,
      amount: new FormControl,
      description: new FormControl,
      discountAmount: new FormControl,
      discountPercent: new FormControl,
      note: new FormControl,
      product: new FormControl,
      productId: new FormControl,
      productVariant: new FormControl,
      productVariantId: new FormControl,
      qty: new FormControl,
      tax: new FormControl,
      taxId: new FormControl,
      taxRate: new FormControl,
      totalDiscount: new FormControl,
      unitPrice: new FormControl,
      uom: new FormControl,
      uomConversion: new FormControl,
      uomId: new FormControl,
    });

    formGroup.patchValue(this.getDefaultValues('line'));

    return formGroup;
  }

  lineConditionalValidation(formGroup: FormGroup) {
    const formGroupValue = formGroup.value;
    if (
      formGroup.dirty &&
      _.size(<any>_.pickBy(_.omit(formGroupValue, ['amount', 'uomConversion']), _.identity))
    ) {
      formGroup.get('description').setValidators([Validators.required]);
      formGroup.get('description').updateValueAndValidity();
      formGroup.get('qty').setValidators([Validators.required]);
      formGroup.get('qty').updateValueAndValidity();
      formGroup.get('unitPrice').setValidators([Validators.required]);
      formGroup.get('unitPrice').updateValueAndValidity();
    } else {
      formGroup.get('description').clearValidators();
      formGroup.get('description').updateValueAndValidity();
      formGroup.get('qty').clearValidators();
      formGroup.get('qty').updateValueAndValidity();
      formGroup.get('unitPrice').clearValidators();
      formGroup.get('unitPrice').updateValueAndValidity();
    }
  }
}

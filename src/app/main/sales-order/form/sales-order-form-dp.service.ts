import { FormGroup } from '@angular/forms';
import { Injectable } from '@angular/core';

import { CommonService } from '../../../core/core.module';
import { SalesDownPaymentService } from '../../sales-down-payment/sales-down-payment.service';
import { SalesDownPaymentRestService } from '../../sales-down-payment/sales-down-payment-rest.service';

@Injectable()
export class SalesOrderFormDPService {
  searchACItems = CommonService.searchLocalACItems.bind(this);
  onACDropdown = CommonService.onLocalACDropdown.bind(this);

  paymentMethods: IPaymentMethod[] = [];
  paymentMethodsSuggestion: IPaymentMethod[] = [];

  accounts: IAccount[] = [];
  accountsSuggestion: IAccount[] = [];

  constructor(
    private _salesDownPayment: SalesDownPaymentService,
    private _salesDownPaymentRest: SalesDownPaymentRestService,
  ) { }

  prepareForm(srcForm, dpForm: FormGroup) {
    this._salesDownPayment.setFormDefinitions(dpForm);

    CommonService.syncFormControls([{
      from: srcForm.get('transactionDate'),
      to: dpForm.get('transactionDate'),
    }, {
      from: srcForm.get('customerId'),
      to: dpForm.get('customerId'),
    }, {
      from: srcForm.get('customerEmail'),
      to: dpForm.get('customerEmail'),
    }, {
      from: srcForm.get('customerMobile'),
      to: dpForm.get('customerMobile'),
    }, {
      from: srcForm.get('useCustomCustomerName'),
      to: dpForm.get('useCustomCustomerName'),
    }, {
      from: srcForm.get('customCustomerName'),
      to: dpForm.get('customCustomerName'),
    }]);

    return this.loadRelatedData();
  }

  loadRelatedData() {
    return this._salesDownPaymentRest.loadRelatedData()
      .do(related => {
        this.paymentMethods = [null].concat(related.paymentMethods);
        this.accounts = related.paymentAccounts;
      });
  }

  submit(form: FormGroup) {
    const downPayment: ISalesDownPayment = form.value;
    this._salesDownPayment.normalizeDoc(downPayment);

    return this._salesDownPaymentRest.create(downPayment);
  }
}

import { MBase } from '../../../core/common/base.model';

export class MSalesOrderForm extends MBase {
  adjustmentAmount: number = 0;
  billingAddress: string;
  branchId: number = 0;
  customerEmail: string;
  customerId: string;
  customerMobile: string;
  customerNote: string;
  customerRefNumber: string;
  deliveryDate: string;
  description: string;
  discountAmount: number = 0;
  discountPercent: number = null;
  lines: MSalesOrderFormLine[] = [];
  note: string;
  shippingAddress: string;
  shippingCharge: number = 0;
  subtotal: number = 0;
  taxAmount: number = 0;
  taxed: boolean = true;
  total: number = 0;
  transactionDate: string;
  transactionNumber: string;
}

export class MSalesOrderFormLine extends MBase {
  description: string;
  discountAmount: number = 0;
  discountPercent: number = null;
  note: string;
  productId: string;
  productVariantId: string;
  qty: number = 0;
  taxId: string;
  taxRate: number = 0;
  totalDiscount: number = 0;
  unitPrice: number = 0;
  uomConversion: number = 0;
  uomId: string;
}

import * as _ from 'lodash';
import { Component, Input, OnChanges, OnInit, SimpleChanges, ViewChild, } from '@angular/core';
import { FormArray, FormGroup } from '@angular/forms';
import { ModalDirective } from 'ngx-bootstrap';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';

import { AccountingService, AutocompleteService, CommonService } from '../../../core/core.module';
import { ProductVariantRestService } from '../../product-variant/product-variant.module';
import { SalesOrderCloneService } from '../../sales-order/sales-order-clone.service';
import { SalesOrderFormDPService } from './sales-order-form-dp.service';
import { SalesOrderFormService } from '../form/sales-order-form.service';
import { SalesOrderRestService } from '../sales-order-rest.service';

import { TransactionFormSalesBComponent } from '../../transaction/transaction-form-sales.bcomponent';

import { MSalesOrder } from '../sales-order.model';

@Component({
  selector: 'app-sales-order-form',
  templateUrl: 'sales-order-form.component.html',
  providers: [
    SalesOrderFormDPService,
  ],
})
export class SalesOrderFormComponent extends TransactionFormSalesBComponent implements OnChanges, OnInit {
  @Input() parseResults: any = null;
  @ViewChild('shippingPriceModal') elShippingPriceModal: ModalDirective;

  shippingProviderProducts: IShippingProviderProduct[] = [];

  cloneTransaction: boolean = false;

  ACProductVariantHandler = CommonService.remoteACItemsHandler({
    remoteParams: this.ACProductVariantParams.bind(this),
    remoteRequest: this._productVariantRest.findAllPicker.bind(this._productVariantRest),
    remoteRequestMap: (response) => response.data,
  });

  quickDPForm: FormGroup;

  constructor(
    public _autocomplete: AutocompleteService,
    public _accounting: AccountingService,
    public _productVariantRest: ProductVariantRestService,
    private _salesOrderClone: SalesOrderCloneService,
    public _salesOrderForm: SalesOrderFormService,
    public _salesOrderFormDP: SalesOrderFormDPService,
    private _salesOrderRest: SalesOrderRestService,
  ) {
    super();
    super.init();

    this.componentId = 'SalesOrderForm';

    this.registerHook('loadRelated', event => {
      return this._salesOrderRest.loadRelatedData();
    }, event => {
      if (this._salesOrderClone.docOnHold) {
        const parentDoc = <any>this._salesOrderClone.docOnHold;
        parentDoc.deliveryDate = null;
        parentDoc.transactionDate = new Date;
        parentDoc.transactionNumber = null;

        this.cloneTransaction = true;
        this.doc = parentDoc;
        this._salesOrderClone.docOnHold = null;

        this.applyDoc();

        this.form.patchValue(this.doc);
      }
    });

    this.registerHook('patchForm', event => {
      this.applyDoc();
    });
  }

  ngOnInit() {
    super.ngOnInit();

    this.addLines();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (_.has(changes, 'parseResults') && this.parseResults) {
      this.resetLines();

      this.form.get('shippingAddress').patchValue(this.parseResults.states[1].parsedData.shippingAddress);

      const valueToPatch: any = {
        customerId: this.parseResults.customer.id,
        customer: this.parseResults.customer,
      };
      if (this.parseResults.states[1].parsedData.customerEmail) {
        valueToPatch.customerEmail = this.parseResults.states[1].parsedData.customerEmail;
      }
      if (this.parseResults.states[1].parsedData.customerMobile) {
        valueToPatch.customerMobile = this.parseResults.states[1].parsedData.customerMobile;
      }
      this.form.patchValue(valueToPatch);
      this.onCustomerSelected(
        this.parseResults.customer,
        !Boolean(this.parseResults.states[1].parsedData.customerEmail),
        !Boolean(this.parseResults.states[1].parsedData.customerMobile)
      ).subscribe(null, null, () => {
        this.parseResults.lines.forEach((lineProduct, queryIdx) => {
          let lineFormGroup = (<FormArray>this.form.get('lines')).at(queryIdx);
          if (!lineFormGroup) {
            this.addLines(1);
            lineFormGroup = (<FormArray>this.form.get('lines')).at(queryIdx);
          }

          lineFormGroup.patchValue({
            qty: this.parseResults.states[1].parsedData.lines[queryIdx].qty
          });

          if (_.isObject(lineProduct.productVariant)) {
            this.onProductSelected(queryIdx, lineProduct.productVariant).subscribe();
          } else if (_.isString(lineProduct.productVariant)) {
            lineFormGroup.patchValue({
              description: lineProduct.productVariant,
            });
          }
        });
      });
    }

    if (_.has(changes, 'doc') && this.doc) {
      this.applyDoc();
    }
  }

  onShippingPriceSelected(service) {
    const selectedShippingPrice = {
      shipppingProviderId: this.form.get('shippingProviderId').value,
      id: service.serviceName,
      name: service.serviceDescription
    };
    this.form.get('shippingProviderProductId').patchValue(service.serviceName);
    this.form.get('shippingProviderProduct').patchValue(selectedShippingPrice);
    this.form.get('shippingCharge').patchValue(Number(service.price));
    this.elShippingPriceModal.hide();
    this.syncTotal();
  }

  getDefaultValue(type: 'master' | 'line'): any {
    return this._salesOrderForm.getDefaultValues(type);
  }

  buildFormChildLine() {
    return this._salesOrderForm.buildFormChildLine();
  }

  lineConditionalValidation(formGroup: FormGroup) {
    this._salesOrderForm.lineConditionalValidation(formGroup);
  }

  onShippingProviderSelected(shippingProvider: IShippingProvider) {
    this.form.get('shippingProviderId').setValue(shippingProvider.id);
    this.shippingProviderProducts = shippingProvider.products;
    this.form.get('shippingProviderProduct').reset(null);
    this.form.get('shippingProviderProductId').reset(null);
  }

  createQuickDP() {
    this.quickDPForm = new FormGroup({});
    this._salesOrderFormDP.prepareForm(this.form, this.quickDPForm).subscribe();
  }

  validateDP() {
    if (this.quickDPForm && !this.quickDPForm.valid) {
      CommonService.markAsDirty(this.quickDPForm);
      return false;
    }
    return true;
  }

  submitDP(doc: MSalesOrder) {
    const mainReturnObs = Observable.of(doc);

    if (this.quickDPForm && this.quickDPForm.get('downPaymentAmount').value) {
      this.quickDPForm.get('orderId').setValue(doc.id);

      return this._salesOrderFormDP.submit(this.quickDPForm).switchMap(() => mainReturnObs);
    } else {
      return mainReturnObs;
    }
  }
}

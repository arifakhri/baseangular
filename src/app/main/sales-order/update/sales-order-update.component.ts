import * as _ from 'lodash';
import * as moment from 'moment';
import { Component, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';

import { SalesOrderFormService } from '../form/sales-order-form.service';
import { SalesOrderRestService } from '../sales-order-rest.service';
import { SalesOrderService } from '../sales-order.service';

import { BaseUpdateBComponent } from '../../../shared/base/base.module';
import { MSalesOrder } from '../sales-order.model';

@Component({
  selector: 'app-sales-order-update',
  templateUrl: 'sales-order-update.component.html',
})
export class SalesOrderUpdateComponent extends BaseUpdateBComponent {
  @ViewChild('reportModal') elReportModal: ModalDirective;

  parseResults: any;
  showParseModal: boolean = false;

  constructor(
    private _salesOrder: SalesOrderService,
    private _salesOrderForm: SalesOrderFormService,
    private _salesOrderRest: SalesOrderRestService,
  ) {
    super();

    this.componentId = 'SalesOrderUpdate';
    this.headerTitle = 'ui.salesOrder.update.title';
    this.containerType = 1;
    this.routeURL = '/sales/orders';
    this.entrySuccessI18n = 'success.salesOrder.update';

    this.headerButtons.push({
      type: 'custom',
      label: 'ui.salesOrder.update.button.parse',
      color: 'primary',
      onClick: () => this.showParseModal = true,
    });

    this.registerHook('buildForm', event => {
      this._salesOrderForm.setFormDefinitions(this.form);
    });

    this.registerHook('load', event => {
      return this._salesOrderRest.load(this.page.routeParams.id).switchMap(doc => {
        if (doc.transactionDate) {
          doc.transactionDate = moment(doc.transactionDate).toDate();
        }
        if (doc.deliveryDate) {
          doc.deliveryDate = moment(doc.deliveryDate).toDate();
        }
        return Observable.of(doc);
      });
    });

    this.registerHook('save', event => {
      const order: MSalesOrder = Object.assign({}, this.doc, this.form.value);
      this._salesOrder.normalizeDoc(order);

      const isPrinting = _.get(event, 'data.print');

      return this._salesOrderRest.update(this.page.routeParams.id, order, { includeLines: isPrinting });
    });
  }

  async saveAndPrint() {
    try {
      const submitted = await this.onSubmit({ saveConfirm: true, print: true });
      if (submitted) {
        this._salesOrderRest.loadRelatedData().subscribe(related => {
          this.doc['companyInfo'] = related.settings.companyInfo;
          this.elReportModal.show();
        });
      }
    } catch (e) {

    }
  }
}

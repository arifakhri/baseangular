import * as _ from 'lodash';
import { Component, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import 'app/rxjs-imports.ts';

import { SalesOrderFormComponent } from '../form/sales-order-form.component';

import { SalesOrderFormService } from '../form/sales-order-form.service';
import { SalesOrderRestService } from '../sales-order-rest.service';
import { SalesOrderService } from '../sales-order.service';

import { BaseCreateBComponent } from '../../../shared/base/base.module';
import { MSalesOrder } from '../sales-order.model';

@Component({
  selector: 'app-sales-order-create',
  templateUrl: 'sales-order-create.component.html',
})
export class SalesOrderCreateComponent extends BaseCreateBComponent {
  @ViewChild('reportModal') elReportModal: ModalDirective;
  @ViewChildren('salesOrderForm') elSalesOrderForm: QueryList<SalesOrderFormComponent>;

  compReady: boolean;
  parseResults: any;
  showParseModal: boolean = false;

  constructor(
    private _salesOrder: SalesOrderService,
    private _salesOrderForm: SalesOrderFormService,
    private _salesOrderRest: SalesOrderRestService,
  ) {
    super();

    this.componentId = 'SalesOrderCreate';
    this.headerTitle = 'ui.salesOrder.create.title';
    this.containerType = 1;
    this.routeURL = '/sales/orders';
    this.entrySuccessI18n = 'success.salesOrder.create';

    this.headerButtons.push({
      type: 'custom',
      label: 'ui.salesOrder.create.button.parse',
      color: 'primary',
      onClick: () => this.showParseModal = true,
    });

    this.autoprintButtons = [{
      checkboxConfigId: 'autoprint.sorder.create',
      checkboxLabel: 'ui.salesOrder.create.action.apCheckbox.report',
      previewLabel: 'ui.salesOrder.create.action.previewReport',
      onPreview: () => {
        this.saveAndPrint();
      }
    }];

    this.registerHook('buildForm', event => {
      this._salesOrderForm.setFormDefinitions(this.form);
    });

    this.registerHook('save', event => {
      let order = new MSalesOrder;
      order = _.assign(order, this.form.value);
      this._salesOrder.normalizeDoc(order);

      const isPrinting = _.get(event, 'data.print');

      let obs = this._salesOrderRest.create(order, { includeLines: isPrinting });
      if (!isPrinting) {
        obs = obs.switchMap(result => this.elSalesOrderForm.first.submitDP(result));
      }
      return obs;
    });
  }

  async saveAndPrint() {
    try {
      const submitted = await this.onSubmit({ saveConfirm: true, print: true });
      if (submitted) {
        this._salesOrderRest.loadRelatedData().subscribe(related => {
          this.doc['companyInfo'] = related.settings.companyInfo;
          this.elReportModal.show();
        });
      }
    } catch (e) {

    }
  }
}

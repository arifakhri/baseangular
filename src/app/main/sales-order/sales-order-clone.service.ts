import * as _ from 'lodash';
import { FormArray, FormGroup } from '@angular/forms';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { MSalesOrder } from './sales-order.model';

@Injectable()
export class SalesOrderCloneService {
  private currentDoc: MSalesOrder;

  constructor(
    private _router: Router
  ) { }

  set docOnHold(order) {
    this.currentDoc = order;
  }

  get docOnHold() {
    if (!this.currentDoc) {
      return null;
    }

    const targetDoc: any = _.cloneDeep(this.currentDoc);

    targetDoc.lines.forEach(line => {
      line.incomeAccountId = _.get(line, 'product.incomeAccountId') || null;
    });

    return targetDoc;
  }

  copy(order) {
    this.docOnHold = order;

    this._router.navigateByUrl('/sales/orders/create');
  }

  reset(form: FormGroup) {

    const linesFormGroup = <FormArray>form.get('lines');
    _.forEach(linesFormGroup.controls, (lineFormGroup, lineIdx) => {
      lineFormGroup.get('incomeAccountId').reset(null);
    });
  }
}

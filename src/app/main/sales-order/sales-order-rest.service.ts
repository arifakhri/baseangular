import { Injectable } from '@angular/core';
import 'app/rxjs-imports.ts';

import { ApiQueryOption, RequestService } from '../../core/core.module';

import { APP_CONST } from '../../app.const';
import { MSalesOrder } from './sales-order.model';

@Injectable()
export class SalesOrderRestService {
  baseURL = `${APP_CONST.API_MAIN}/sales/orders`;
  request = this._request.new(this.baseURL);

  constructor(
    private _request: RequestService,
  ) { }

  create(order: MSalesOrder, queryParams: any = {}) {
    return this.request.post<MSalesOrder>(``, order, { params: queryParams });
  }

  findAll(queryOption: ApiQueryOption, queryParams: any = {}) {
    return this.request.post<IApiPaginationResult<MSalesOrder>>(`q`, queryOption, { params: queryParams });
  }

  load(orderId: string, queryParams: any = {
    includeLines: true,
  }) {
    return this.request.get<MSalesOrder>(orderId, {
      params: queryParams,
    });
  }

  loadRelatedData() {
    return this.request.get<{
      shippingMethods: IShippingMethod[],
      shippingProviders: IShippingProvider[],
      taxes: ITax[],
      warehouses: IWarehouse[],
      settings: ISettings,
    }>(`entry-related-data`);
  }

  update(orderId: string, updateObj: MSalesOrder, queryParams: any = {}) {
    return this.request.put<MSalesOrder>(`${orderId}`, updateObj, { params: queryParams });
  }

  void(orderId: string) {
    return this.request.put<MSalesOrder>(`${orderId}/void`, {});
  }
}

import * as _ from 'lodash';
import * as moment from 'moment';
import { FormArray, FormGroup } from '@angular/forms';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { SalesOrderRestService } from './sales-order-rest.service';

import { MSalesOrder } from './sales-order.model';

@Injectable()
export class SalesOrderCopyInvoiceService {
  private currentDoc: MSalesOrder;
  private parentTransactionType: string = 'sales_order';

  constructor(
    private _router: Router,
    private _salesOrderRest: SalesOrderRestService,
  ) { }

  set docOnHold(order) {
    this.currentDoc = order;
  }

  get docOnHold() {
    if (!this.currentDoc) {
      return null;
    }

    const targetDoc: any = _.cloneDeep(this.currentDoc);
    targetDoc.transactionDate = new Date;

    targetDoc.parentTransactionType = this.parentTransactionType;

    _.remove(targetDoc.lines, line => {
      return !((<any>line).qty - (<any>line).qtyFulfilled > 0);
    });

    targetDoc.lines.forEach(line => {
      line.parentTransactionId = line.salesTransactionId;
      line.parentTransactionLineId = line.id;
      line.incomeAccountId = _.get(line, 'product.incomeAccountId') || null;
      line.qty = (<any>line).qty - (<any>line).qtyFulfilled;
    });

    return targetDoc;
  }

  copy(order, navigate: boolean = true) {
    this.docOnHold = order;

    if (navigate) {
      this._router.navigateByUrl('/sales/invoices/create');
    }
  }

  copyById(orderId: string, navigate: boolean = false) {
    return this._salesOrderRest.load(orderId).do(order => {
      this.copy(order, navigate);
    });
  }

  apply() {
    const parentDoc = Object.assign({}, this.docOnHold);
    parentDoc.transactionNumber = null;

    let targetDueDate = new Date;
    if (parentDoc.customer.defaultSalesInvoiceDueDays) {
      targetDueDate = moment().add(parentDoc.customer.defaultSalesInvoiceDueDays, 'days').toDate();
    }
    parentDoc.dueDate = targetDueDate;

    this.docOnHold = null;

    return parentDoc;
  }

  reset(form: FormGroup) {
    form.get('parentTransactionType').reset(null);

    const linesFormGroup = <FormArray>form.get('lines');
    _.forEach(linesFormGroup.controls, (lineFormGroup, lineIdx) => {
      lineFormGroup.get('parentTransactionId').reset(null);
      lineFormGroup.get('parentTransactionLineId').reset(null);
      lineFormGroup.get('incomeAccountId').reset(null);
    });
  }
}

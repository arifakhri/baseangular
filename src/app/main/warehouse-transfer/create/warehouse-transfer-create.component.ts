import * as _ from 'lodash';
import { Component, QueryList, ViewChildren } from '@angular/core';
import 'app/rxjs-imports.ts';

import { WarehouseTransferFormComponent } from '../form/warehouse-transfer-form.component';
import { WarehouseTransferRestService } from '../warehouse-transfer-rest.service';
import { WarehouseTransferService } from '../warehouse-transfer.service';

import { BaseCreateBComponent } from '../../../shared/base/base.module';
import { MWarehouseTransfer } from '../warehouse-transfer.model';

@Component({
  selector: 'app-warehouse-transfer-create',
  templateUrl: 'warehouse-transfer-create.component.html'
})
export class WarehouseTransferCreateComponent extends BaseCreateBComponent {
  @ViewChildren('warehouseTransferForm') elWarehouseTransferForm: QueryList<WarehouseTransferFormComponent>;

  constructor(
    private _warehouseTransfer: WarehouseTransferService,
    private _warehouseTransferRest: WarehouseTransferRestService,
  ) {
    super();
    this.componentId = 'WarehouseTransferCreate';
    this.headerTitle = 'ui.warehouseTransfer.create.title';
    this.containerType = 1;
    this.routeURL = '/warehouse-transfers';
    this.entrySuccessI18n = 'success.warehouseTransfer.create';

    this.registerHook('buildForm', event => {
      this._warehouseTransfer.setFormDefinitions(this.form);
    });

    this.registerHook('save', event => {
      let warehouseTransfer = new MWarehouseTransfer;
      warehouseTransfer = _.assign(warehouseTransfer, this.form.value);
      this._warehouseTransfer.normalizeDoc(warehouseTransfer);
      const obs = this._warehouseTransferRest.create(warehouseTransfer);
      return obs;
    });
  }
}

import * as moment from 'moment';
import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';

import { WarehouseTransferRestService } from '../warehouse-transfer-rest.service';
import { WarehouseTransferService } from '../warehouse-transfer.service';
import { BaseUpdateBComponent } from '../../../shared/base/base.module';

@Component({
  selector: 'app-warehouse-transfer-update',
  templateUrl: 'warehouse-transfer-update.component.html'
})
export class WarehouseTransferUpdateComponent extends BaseUpdateBComponent {

  constructor(
    private _warehouseTransfer: WarehouseTransferService,
    private _warehouseTransferRest: WarehouseTransferRestService,
  ) {
    super();
    this.componentId = 'WarehouseTransferUpdate';
    this.headerTitle = 'ui.warehouseTransfer.update.title';
    this.containerType = 1;
    this.routeURL = '/warehouse-transfers';
    this.entrySuccessI18n = 'success.warehouseTransfer.update';

    this.registerHook('buildForm', event => {
      this._warehouseTransfer.setFormDefinitions(this.form);
    });

    this.registerHook('load', event => {
      return this._warehouseTransferRest.load(this.page.routeParams.id).switchMap(doc => {
        if (doc.transactionDate) {
          doc.transactionDate = moment(doc.transactionDate).toDate();
        }
        return Observable.of(doc);
      });
    });

    this.registerHook('save', event => {
      const warehouseTransfer: IWarehouseTransfer = Object.assign({}, this.doc, this.form.value);
      this._warehouseTransfer.normalizeDoc(warehouseTransfer);
      return this._warehouseTransferRest.update(this.page.routeParams.id, warehouseTransfer);
    });
  }
}

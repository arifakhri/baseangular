import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { WarehouseTransferCreateComponent } from './create/warehouse-transfer-create.component';
import { WarehouseTransferDetailComponent } from './detail/warehouse-transfer-detail.component';
import { WarehouseTransferListComponent } from './list/warehouse-transfer-list.component';
import { WarehouseTransferUpdateComponent } from './update/warehouse-transfer-update.component';

import { AuthorizationService } from '../../core/core.module';

export const routes: Routes = [{
  path: '',
  children: [{
    path: '',
    component: WarehouseTransferListComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'warehouseTransfer.list'
    }
  }, {
    path: 'create',
    component: WarehouseTransferCreateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'warehouseTransfer.create'
    }
  }, {
    path: ':id',
    component: WarehouseTransferDetailComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'warehouseTransfer.detail',
    },
  }, {
    path: ':id/update',
    component: WarehouseTransferUpdateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'warehouseTransfer.update',
    },
  }]
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class WarehouseTransferRoutingModule { }

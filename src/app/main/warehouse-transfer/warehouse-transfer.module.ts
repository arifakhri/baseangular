import { BsDropdownModule, PaginationModule } from 'ngx-bootstrap';
import { AutoCompleteModule } from 'primeng/components/autocomplete/autocomplete';
import { CalendarModule } from 'primeng/components/calendar/calendar';
import { CheckboxModule } from 'primeng/components/checkbox/checkbox';
import { DataTableModule } from 'primeng/components/datatable/datatable';
import { ListboxModule } from 'primeng/components/listbox/listbox';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { WarehouseTransferCreateComponent } from './create/warehouse-transfer-create.component';
import { WarehouseTransferDetailComponent } from './detail/warehouse-transfer-detail.component';
import { WarehouseTransferFormComponent } from './form/warehouse-transfer-form.component';
import { WarehouseTransferListComponent } from './list/warehouse-transfer-list.component';
import { WarehouseTransferUpdateComponent } from './update/warehouse-transfer-update.component';

import { WarehouseTransferRestService } from './warehouse-transfer-rest.service';
import { WarehouseTransferService } from './warehouse-transfer.service';

import { CoreModule } from '../../core/core.module';
import { SharedModule } from '../../shared/shared.module';

export const PROVIDERS = [
  WarehouseTransferRestService,
  WarehouseTransferService,
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    FlexLayoutModule,
    HttpModule,
    ReactiveFormsModule,
    RouterModule,
    TranslateModule,
    BsDropdownModule,
    AutoCompleteModule,
    CalendarModule,
    CheckboxModule,
    DataTableModule,
    ListboxModule,
    PaginationModule,
    CoreModule,
    SharedModule,
  ],
  declarations: [
    WarehouseTransferCreateComponent,
    WarehouseTransferDetailComponent,
    WarehouseTransferFormComponent,
    WarehouseTransferListComponent,
    WarehouseTransferUpdateComponent,
  ],
})
export class WarehouseTransferModule { }

import { NgModule } from '@angular/core';

import { WarehouseTransferModule } from './warehouse-transfer.module';
import { WarehouseTransferRoutingModule } from './warehouse-transfer-routing.module';

@NgModule({
  imports: [
    WarehouseTransferModule,
    WarehouseTransferRoutingModule
  ]
})
export class WarehouseTransferLazyModule { }

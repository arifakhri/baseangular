import * as _ from 'lodash';
import { ChangeDetectorRef, Component, Input, OnChanges, OnInit, SimpleChanges, } from '@angular/core';
import { FormArray, FormGroup } from '@angular/forms';
import 'app/rxjs-imports.ts';

import { AccountingService, AutocompleteService, CommonService } from '../../../core/core.module';
import { WarehouseTransferRestService } from '../warehouse-transfer-rest.service';
import { WarehouseTransferService } from '../warehouse-transfer.service';
import { TransactionFormBComponent } from '../../transaction/transaction-form.bcomponent';

@Component({
  selector: 'app-warehouse-transfer-form',
  templateUrl: 'warehouse-transfer-form.component.html',
})
export class WarehouseTransferFormComponent extends TransactionFormBComponent implements OnChanges, OnInit {
  @Input() doc: IWarehouseTransfer;
  @Input() form: FormGroup;

  productVariantsSuggestion: IProductVariant[] = [];
  ACProductVariantHandler = CommonService.remoteACItemsHandler({
    remoteParams: this.ACProductVariantParams.bind(this),
    remoteRequest: this._productVariantRest.findAllPicker.bind(this._productVariantRest),
    remoteRequestMap: (response) => response.data,
  });

  constructor(
    public _autocomplete: AutocompleteService,
    public _accounting: AccountingService,
    private _changeDetectorRef: ChangeDetectorRef,
    private _warehouseTransfer: WarehouseTransferService,
    private _warehouseTransferRest: WarehouseTransferRestService,
  ) {
    super();
    this.componentId = 'WarehouseTransferForm';

    this.registerHook('loadRelated', event => {
      return this._warehouseTransferRest.loadRelatedData();
    });

    this.registerHook('patchForm', event => {
      this.applyDoc();
    });
  }

  ngOnInit() {
    super.ngOnInit();
    this.addLines();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (_.has(changes, 'doc') && this.doc) {
      this.applyDoc();
    }
  }

  applyDoc() {
    this.resetLines();
    if (this.doc.lines.length > 2) {
      this.addLines(this.doc.lines.length - 2);
    }
    this.form.patchValue({ lines: this.doc.lines });

    this._changeDetectorRef.detectChanges();
  }

  clearLine(index: number) {
    const formArray = <FormArray>this.form.controls.lines;
    if ((formArray.length > 2 && index <= 1) || index > 1) {
      formArray.removeAt(index);
    } else {
      formArray.at(index).reset();
      this._warehouseTransfer.lineConditionalValidation(<FormGroup>formArray.at(index));
    }
  }

  resetLines() {
    const formArray = <FormArray>this.form.controls.lines;
    while (formArray.length) {
      formArray.removeAt(0);
    }
    this.addLines();
  }

  addLines(length: number = 2) {
    const formArray = (<FormArray>this.form.controls.lines);
    _.range(length).forEach(() => {
      const newFormGroup = this._warehouseTransfer.buildFormChildLine();
      formArray.push(newFormGroup);
    });
    this._changeDetectorRef.detectChanges();
  }

  ACProductVariantParams(event: any, type: string) {
    const qParams: any = {};

    if (type === 'search') {
      qParams['keyword'] = event.query;
    }

    return [{
      filter: [],
      sort: [],
      take: 30,
      skip: 0,
      includeTotalCount: false
    }, qParams];
  }

  onProductSelected(formIndex: number, srcProductVariant: IProductVariant, targetSpinnerElement?: any) {
    const spinner = this._spinner.show({
      element: targetSpinnerElement,
      loaderType: 3,
    });
    return this._productVariantRest.load(srcProductVariant.id).do(productVariant => {
      const lineFormGroup = (<FormArray>this.form.get('lines')).at(formIndex);

      lineFormGroup.patchValue({
        product: productVariant.product,
        productId: productVariant.productId,
        productVariant,
        productVariantId: productVariant.id,
        qty: 1,
      });
    }).finally(() => {
      spinner.dispose();
    });
  }

}

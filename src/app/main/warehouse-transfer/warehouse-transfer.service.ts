import * as _ from 'lodash';
import * as moment from 'moment';
import { CustomValidators } from 'ng2-validation';
import { Injectable } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';

import { StartingDataService } from '../../core/core.module';

@Injectable()
export class WarehouseTransferService {
  constructor(
    private _startingData: StartingDataService,
  ) { }

  normalizeDoc(record: any) {
    if (record.transactionDate) {
      record.transactionDate = moment(record.transactionDate).format('YYYY-MM-DD');
    }

    record.lines = _.reject(record.lines, line => {
      return !(<any>line).productId;
    });
  }

  setFormDefinitions(form: FormGroup) {
    const startingData = this._startingData.data$.getValue();

    form.addControl('branchId', new FormControl(startingData.masterBranchId));
    form.addControl('fromWarehouse', new FormControl);
    form.addControl('fromWarehouseId', new FormControl('', [CustomValidators.notEqual('toWarehouseId'), Validators.required]));
    form.addControl('toWarehouse', new FormControl);
    form.addControl('toWarehouseId', new FormControl('', Validators.required));
    form.addControl('transactionNumber', new FormControl);
    form.addControl('transactionDate', new FormControl(new Date(), Validators.required));
    form.addControl('description', new FormControl);
    form.addControl('note', new FormControl);

    form.addControl('lines', new FormArray([]));
  }

  buildFormChildLine() {
    return new FormGroup({
      id: new FormControl(null),
      rowVersion: new FormControl,
      product: new FormControl,
      productId: new FormControl,
      productVariant: new FormControl,
      productVariantId: new FormControl,
      qty: new FormControl,
    });
  }

  lineConditionalValidation(formGroup: FormGroup) {
    const formGroupValue = formGroup.value;
    if (
      formGroup.dirty &&
      _.size(<any>_.pickBy(_.omit(formGroupValue, ['branchId']), _.identity))
    ) {
      formGroup.get('productId').setValidators([Validators.required]);
      formGroup.get('productId').updateValueAndValidity();
      formGroup.get('productVariantId').setValidators([Validators.required]);
      formGroup.get('productVariantId').updateValueAndValidity();
      formGroup.get('qty').setValidators([Validators.required]);
      formGroup.get('qty').updateValueAndValidity();
    } else {
      formGroup.get('productId').clearValidators();
      formGroup.get('productId').updateValueAndValidity();
      formGroup.get('productVariantId').clearValidators();
      formGroup.get('productVariantId').updateValueAndValidity();
      formGroup.get('qty').clearValidators();
      formGroup.get('qty').updateValueAndValidity();
    }
  }
}

export class MWarehouseTransfer {
  id: string;
  transactionDate: string | Date;
  lines: [
    {
      id: string;
      product: {
        id: string;
        name: string;
      }
    }
  ]
}

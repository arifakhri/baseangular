import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { AccountingService } from '../../../core/core.module';
import { AdminLayoutService } from '../../../layouts/layouts.module';
import { WarehouseTransferRestService } from '../warehouse-transfer-rest.service';

@Component({
  selector: 'app-warehouse-transfer-detail',
  templateUrl: 'warehouse-transfer-detail.component.html'
})
export class WarehouseTransferDetailComponent implements OnInit {
  doc: IWarehouseTransfer;
  routeParams: any;

  constructor(
    public _accounting: AccountingService,
    private _adminLayout: AdminLayoutService,
    private _warehouseTransferRest: WarehouseTransferRestService,
    private _route: ActivatedRoute,
  ) {
    this._adminLayout.containerType = 1;
    this._route.params.subscribe(params => this.routeParams = params);
  }

  ngOnInit() {
    this.loadData();
  }

  loadData() {
    this._warehouseTransferRest.load(this.routeParams.id, { includeLines: true }).subscribe(warehouseTransfer => {
      this.doc = warehouseTransfer;
    });
  }
}

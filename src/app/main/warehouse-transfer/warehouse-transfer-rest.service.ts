import { Injectable } from '@angular/core';
import 'app/rxjs-imports.ts';

import { ApiQueryOption, RequestService } from '../../core/core.module';
import { ProductVariantRestService } from '../product-variant/product-variant-rest.service';

import { APP_CONST } from '../../app.const';

@Injectable()
export class WarehouseTransferRestService {
  baseURL = `${APP_CONST.API_MAIN}/warehouse-transfers`;
  request = this._request.new(this.baseURL);

  constructor(
    public _productVariantRest: ProductVariantRestService,
    private _request: RequestService,
  ) { }

  create(warehouseTransfer: IWarehouseTransfer) {
    return this.request.post<IWarehouseTransfer>(``, warehouseTransfer);
  }

  findAll(queryOption: ApiQueryOption) {
    return this.request.post<IApiPaginationResult<IWarehouseTransfer>>(`q`, queryOption);
  }

  load(warehouseTransferId: string, queryParams: any = {}) {
    return this.request.get<IWarehouseTransfer>(`${warehouseTransferId}`);
  }

  loadRelatedData() {
    return this.request.get<{ warehouses: IWarehouse[] }>(`entry-related-data`);
  }

  update(warehouseTransferId: string, updateObj: IWarehouseTransfer) {
    return this.request.put<IWarehouseTransfer>(`${warehouseTransferId}`, updateObj);
  }
}

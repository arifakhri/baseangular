import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ReportsInventoryPreviewComponent } from './preview/reports-inventory-preview.component';

import { AuthorizationService } from '../../core/core.module';

const routes: Routes = [{
  path: '',
  children: [{
    path: '',
    component: ReportsInventoryPreviewComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'reportsInventory.preview'
    }
  }]
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ],
})
export class ReportsInventoryRoutingModule { }

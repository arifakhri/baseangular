import { AutoCompleteModule } from 'primeng/components/autocomplete/autocomplete';
import { CalendarModule } from 'primeng/components/calendar/calendar';
import { CommonModule } from '@angular/common';
import { DataTableModule } from 'primeng/components/datatable/datatable';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { PaginationModule } from 'ngx-bootstrap';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { ReportsInventoryPreviewComponent } from './preview/reports-inventory-preview.component';

import { ReportsInventoryRestService } from './reports-inventory-rest.service';

import { SharedModule } from '../../shared/shared.module';

export const PROVIDERS = [
  ReportsInventoryRestService,
];

@NgModule({
  imports: [
    AutoCompleteModule,
    CalendarModule,
    CommonModule,
    DataTableModule,
    FlexLayoutModule,
    FormsModule,
    PaginationModule,
    ReactiveFormsModule,
    RouterModule,
    SharedModule,
    TranslateModule,
  ],
  declarations: [
    ReportsInventoryPreviewComponent,
  ]
})
export class ReportsInventoryModule {

}

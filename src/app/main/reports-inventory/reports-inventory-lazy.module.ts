import { NgModule } from '@angular/core';

import { ReportsInventoryModule } from './reports-inventory.module';
import { ReportsInventoryRoutingModule } from './reports-inventory-routing.module';

@NgModule({
  imports: [
    ReportsInventoryModule,
    ReportsInventoryRoutingModule,
  ],
})
export class ReportsInventoryLazyModule { }

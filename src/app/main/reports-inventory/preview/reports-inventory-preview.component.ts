import * as moment from 'moment';
import { Component, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

import { ReportsInventoryRestService } from '../reports-inventory-rest.service';

import { AdminLayoutService } from '../../../layouts/layouts.module';
import { ApiQueryOption, GridTableFilterService } from '../../../core/core.module';

@Component({
  selector: 'app-reports-inventory-preview',
  templateUrl: './reports-inventory-preview.component.html',
})
export class ReportsInventoryPreviewComponent {
  @ViewChild('reportViewer') elReportViewer: any;

  compReady: boolean = false;
  data: ISalesInvoice[] = [];
  form: FormGroup = new FormGroup({});
  reportViewerReady: boolean = false;

  filtersMap: IGridTableFilterMap = {
    lowDate: {
      targetVar: 'date',
      targetFilter: 'param',
      dateFormat: 'YYYY-MM-DD',
    },
    highDate: {
      targetVar: 'date',
      targetFilter: 'param',
      dateFormat: 'YYYY-MM-DD',
    },
    name: true,
    sku: true,
  };

  reportParams: any = {};

  constructor(
    private _adminLayout: AdminLayoutService,
    private _gridTableFilter: GridTableFilterService,
    private _reportsInventoryRest: ReportsInventoryRestService,
  ) {
    this._adminLayout.containerType = 1;

    this.buildForm();
    this.loadData();
  }

  buildForm() {
    this.form.addControl('lowDate', new FormControl(moment().set('date', 1).toDate()));
    this.form.addControl('highDate', new FormControl(new Date()));
    this.form.addControl('sku', new FormControl);
    this.form.addControl('name', new FormControl);
  }

  ACCustomerParams(event: any, type: string) {
    let filters = [];

    if (type === 'search') {
      filters = [{
        filterValues: [{
          field: 'displayName',
          operator: 'contains',
          value: event.query
        }]
      }];
    }

    return [{
      filter: filters,
      sort: [{
        field: 'displayName',
        dir: 'asc'
      }],
      take: 30,
      skip: 0,
      includeTotalCount: false
    }];
  }

  loadData() {
    const filters = this._gridTableFilter.parse(this.form.value, this.filtersMap);
    this.compReady = false;

    const qOption: ApiQueryOption = {
      filter: [],
      skip: 0,
      take: 1000,
      includeTotalCount: false,
    };
    this._gridTableFilter.appendQBodyFilters(qOption, filters.qBody);
    this._reportsInventoryRest.findAll(qOption, filters.qParams).subscribe(response => {
      this.data = response.data;

      this.prepareParams();

      this.compReady = true;
    });
  }

  prepareParams() {
    const filterValues = this.form.value;

    this.reportParams.title = `Product and Service List`;

    if (filterValues.lowDate) {
      this.reportParams.lowDate = moment(filterValues.lowDate).format('DD/MM/YYYY');
    }

    if (filterValues.highDate) {
      this.reportParams.highDate = moment(filterValues.highDate).format('DD/MM/YYYY');
    }
  }
}

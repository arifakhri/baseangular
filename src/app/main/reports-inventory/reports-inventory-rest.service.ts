import { Injectable } from '@angular/core';

import { ProductVariantRestService } from '../product-variant/product-variant-rest.service';

@Injectable()
export class ReportsInventoryRestService {
    findAll = this._productVariantRest.findAll.bind(this._productVariantRest);

    constructor(
      private _productVariantRest: ProductVariantRestService,
    ) { }
}

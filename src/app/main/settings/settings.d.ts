declare interface ISettings {
  accountMapping: ISettingsAccountMap;
  accountingSetting: ISettingsAccounting;
  companyInfo: ISettingsCompany;
  companySetting: ISettingsCompany;
  productSetting: ISettingsProduct;
  purchaseSetting: ISettingsPurchases;
  salesSetting: ISettingsSales;
  userSetting: ISettingsUser;
}

declare interface ISettingsAccountMap {
  cashAccountId: string;
  undepositedFundsAccountId: string;
  arAccountId: string;
  unbilledARAccountId: string;
  apAccountId: string;
  unbilledAPAccountId: string;
  salesDepositAccountId: string;
  purchaseDepositAccountId: string;
  salesTaxAccountId: string;
  purchaseTaxAccountId: string;
  fixedAssetAccountId: string;
  accumulatedDepreciationAccountId: string;
  openingBalanceAccountId: string;
  retainEarningAccountId: string;
  salesIncomeAccountId: string;
  purchaseExpenseAccountId: string;
  cogsAccountId: string;
  inventoryAccountId: string;
  inventoryCostVarianceAccountId: string;
  salesDiscountAccountId: string;
  salesReturnAccountId: string;
  shippingIncomeAccountId: string;
  purchaseDiscountAccountId: string;
  shippingChargeAccountId: string;
  inventoryAdjustmentAccountId: string;
  depreciationExpenseAccountId: string;
  roundingAccountId: string;
}

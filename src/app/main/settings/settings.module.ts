import { CommonModule } from '@angular/common';
import { DataTableModule } from 'primeng/components/datatable/datatable';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgModule } from '@angular/core';
import { PaginationModule } from 'ngx-bootstrap';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { SettingsTabsComponent } from './tabs/settings-tabs.component';

import { SettingsApplicatorService } from './settings-applicator.service';

import { CoreModule } from '../../core/core.module';
import { SharedModule } from '../../shared/shared.module';

export const PROVIDERS = [
  SettingsApplicatorService,
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    FlexLayoutModule,
    HttpModule,
    ReactiveFormsModule,
    RouterModule,
    TranslateModule,
    DataTableModule,
    PaginationModule,
    CoreModule,
    SharedModule,
  ],
  declarations: [
    SettingsTabsComponent,
  ],
  exports: [
    SettingsTabsComponent,
  ],
})
export class SettingsModule { }

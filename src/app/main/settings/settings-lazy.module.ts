import { NgModule } from '@angular/core';
import { SettingsModule } from './settings.module';
import { SettingsRoutingModule } from './settings-routing.module';

@NgModule({
  imports: [
    SettingsModule,
    SettingsRoutingModule
  ]
})
export class SettingsLazyModule { }

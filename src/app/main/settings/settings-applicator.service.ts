import * as _ from 'lodash';
import { FormGroup } from '@angular/forms';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';

import { COARestService } from '../coa/coa-rest.service';
import { TaxRestService } from '../tax/tax-rest.service';

@Injectable()
export class SettingsApplicatorService {
  applicantsSettingsMap: any = {
    'accounts': 'accountMapping',
    'purchases': 'purchaseSetting',
    'sales': 'salesSetting',
    'product': 'productSetting',
  };

  constructor(
    private _coaRest: COARestService,
    private _taxRest: TaxRestService,
  ) { }

  getFactoryRecordLoader(settingKey: string) {
    if (settingKey.indexOf('Account') > -1) {
      return (settingId) => this._coaRest.load(settingId);
    } else if (settingKey.indexOf('Tax') > -1) {
      return (settingId) => this._taxRest.load(settingId);
    }
    return false;
  }

  applyToForm(
    settings: ISettings,
    form: FormGroup,
    applicants: any = {},
    resources: any = {},
  ) {
    const observables = [];
    const settingGroups = _.keys(applicants);
    _.forEach(settingGroups, settingGroup => {
      observables.push(
        this.baseApplicator(settingGroup, settings[this.applicantsSettingsMap[settingGroup]], form, _.get(applicants, settingGroup, {}), resources)
      );
    });
    return Observable.merge(...observables);
  }

  baseApplicator(
    settingGroup: string,
    settingsValues: any,
    form: FormGroup,
    applicants: any = {},
    resources: any = {},
  ) {
    const obs = [];

    _.forEach(settingsValues, (settingId, targetModel) => {
      const obsChild = this.baseApplicatorExecutor(
        settingGroup,
        settingsValues,
        form,
        applicants,
        settingId,
        targetModel,
        this.getFactoryRecordLoader(targetModel),
        resources,
      );
      if (obsChild) {
        obs.push(obsChild);
      }
    });

    _.forEach(applicants, (targetModels, settingIdModel) => {
      const settingId = _.get(settingsValues, settingIdModel);
      targetModels = _.castArray(targetModels);
      _.forEach(targetModels, targetModel => {
        const obsChild = this.baseApplicatorExecutor(
          settingGroup,
          settingsValues,
          form,
          applicants,
          settingId,
          targetModel,
          this.getFactoryRecordLoader(settingIdModel),
          resources,
        );
        if (obsChild) {
          obs.push(obsChild);
        }
      });
    });

    return Observable.zip(...obs);
  }

  baseApplicatorExecutor(
    settingGroup: string,
    settingsValues: any,
    form: FormGroup,
    applicants: any = {},
    settingId: string,
    targetModel: string,
    recordLoader: any,
    resources: any = {},
  ) {
    if (form.contains(targetModel)) {
      let obsObj: Observable<any>;
      let targetRecordModel;

      if (targetModel.substr(targetModel.length - 2, targetModel.length) === 'Id') {
        targetRecordModel = targetModel.substr(0, targetModel.length - 2);
        if (_.has(settingsValues, targetRecordModel)) {
          obsObj = Observable.of(_.get(settingsValues, targetRecordModel));
        } else {
          if (_.has(resources, settingGroup)) {
            const obj = _.find(_.get(resources, settingGroup), { id: settingId });
            if (obj) {
              obsObj = Observable.of(obj);
            }
          }

          if (!obsObj && recordLoader) {
            obsObj = recordLoader(settingId);
          }
        }
      } else {
        obsObj = Observable.of(_.get(settingsValues, targetModel));
      }
      if (obsObj) {
        return obsObj.do(targetRecord => {
          if (targetRecordModel) {
            form.get(targetRecordModel).setValue(targetRecord);
          }
          form.get(targetModel).setValue(settingId);
        }).switchMap(targetRecord => {
          return Observable.of({
            targetRecord,
            targetModel,
          });
        });
      }
    }
  }
}

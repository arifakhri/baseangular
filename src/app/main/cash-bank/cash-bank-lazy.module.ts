import { NgModule } from '@angular/core';

import { CashBankModule } from './cash-bank.module';
import { CashBankRoutingModule } from './cash-bank-routing.module';

@NgModule({
  imports: [
    CashBankModule,
    CashBankRoutingModule
  ]
})
export class CashBankLazyModule { }

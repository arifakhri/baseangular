import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CashBankListComponent } from './list/cash-bank-list.component';

import { AuthorizationService } from '../../core/core.module';

export const routes: Routes = [{
  path: '',
  children: [{
    path: '',
    component: CashBankListComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'cashBank.list'
    }
  }]
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class CashBankRoutingModule { }

declare interface ICashBank {
  id: string;
  branchId: number;
  transactionNumber: string;
  transactionDate: string | Date;
  payeeId: string;
  payerId: string;  
  description: string;
  note: string;
  branch: {
    id: number;
    name: string;
  };
  lines: [
    {
      id: string;
      accountId: string;
      description: string;
      amount: number;
      account: {
        id: string;
        accountClassId: string;
        accountClassName: string;
        accountClassSortOrder: number;
        accountTypeId: string;
        accountTypeName: string;
        accountTypeSortOrder: string;
        code: string;
        name: string;
        description: string;
        isChildAccount: boolean;
        level: number;
        locked: boolean;
        systemType: string;
        inactive: boolean;
      }
    }
  ],
  total: number;
}

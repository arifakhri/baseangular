import * as _ from 'lodash';
import { Component, OnChanges, SimpleChanges, ViewChild, } from '@angular/core';
import { FormArray } from '@angular/forms';
import { PopoverDirective } from 'ngx-bootstrap';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';

import { AccountingService, AutocompleteService } from '../../../core/core.module';
import { SalesInvoiceRestService } from '../../sales-invoice/sales-invoice-rest.service';
import { SalesPaymentRestService } from '../sales-payment-rest.service';
import { SalesPaymentService } from '../sales-payment.service';

import { TransactionFormSalesBComponent } from '../../transaction/transaction-form-sales.bcomponent';

@Component({
  selector: 'app-sales-payment-form',
  templateUrl: 'sales-payment-form.component.html',
})
export class SalesPaymentFormComponent extends TransactionFormSalesBComponent implements OnChanges {
  @ViewChild('popoverFindByInvoice') elPopoverFindByInvoice: PopoverDirective;

  constructor(
    public _autocomplete: AutocompleteService,
    public _accounting: AccountingService,
    private _salesInvoiceRest: SalesInvoiceRestService,
    private _salesPayment: SalesPaymentService,
    private _salesPaymentRest: SalesPaymentRestService,
  ) {
    super();
    super.init();
    this.componentId = 'SalesPaymentForm';

    this.registerHook('loadRelated', event => {
      return this._salesPaymentRest.loadRelatedData();
    });

    this.registerHook('patchForm', event => {
      this.applyDoc();
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (_.has(changes, 'doc') && this.doc) {
      this.applyDoc();
    }
  }

  applyDoc() {
    this.applyInvoices(this.doc.lines);
  }

  onInvoiceFound(invoice: ISalesInvoice) {
    this.onCustomerSelected(<any>invoice.customer).subscribe(() => {
      this.elPopoverFindByInvoice.hide();
    });
  }

  onCustomerClear() {
    this.form.get('customerId').reset();
    this.clearInvoices();
  }

  onCustomerSelected(customer: IBusinessPartner) {
    this.form.get('customer').setValue(customer);
    this.form.get('customerId').setValue(customer.id);

    return this.populateInvoices().switchMap(() => Observable.of(customer));
  }

  populateInvoices() {
    const loading = this.page.createLoading();

    return this._salesInvoiceRest.findAll(
      {
        filter: [],
        sort: [{
          field: 'transactionNumber',
          dir: 'asc'
        }],
        take: 30,
        skip: 0,
        includeTotalCount: false
      },
      {
        customerId: this.form.get('customerId').value,
        isOpen: true,
      }
    ).finally(() => {
      loading.dispose();
    }).do(invoices => {
      this.applyInvoices(invoices.data);
    });
  }

  clearInvoices() {
    const formLines = (<FormArray>this.form.get('lines'));
    while (formLines.length) {
      formLines.removeAt(0);
    }
  }

  applyInvoices(invoices) {
    this.clearInvoices();

    invoices.forEach(transaction => {
      const lineFormGroup = this._salesPayment.buildFormChildLine();
      lineFormGroup.patchValue(transaction);

      lineFormGroup.get('transaction').setValue(transaction.transaction || transaction);
      lineFormGroup.get('transactionType').setValue(transaction.transactionType);
      lineFormGroup.get('transactionId').setValue(_.get(transaction, 'transaction.id', transaction.id || null));

      if (transaction.paymentAmount) {
        lineFormGroup.get('paymentAmount').setValue(transaction.paymentAmount);
      }

      (<FormArray>this.form.get('lines')).push(lineFormGroup);
    });
  }

  syncAmountByLinePayment() {
    let totalAmount = 0;
    (<FormArray>this.form.get('lines')).controls.forEach(line => {
      const linePaymentValue = line.get('paymentAmount').value || 0;
      totalAmount += linePaymentValue;
    });

    this.form.get('paymentAmount').setValue(totalAmount);
  }

  syncTotal() {
    this.syncAmountByLinePayment();
  }
}

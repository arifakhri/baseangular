import { NgModule } from '@angular/core';

import { SalesPaymentModule } from './sales-payment.module';
import { SalesPaymentRoutingModule } from './sales-payment-routing.module';

@NgModule({
  imports: [
    SalesPaymentModule,
    SalesPaymentRoutingModule
  ]
})
export class SalesPaymentLazyModule { }

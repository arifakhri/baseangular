import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { AccountingService } from '../../../core/core.module';
import { AdminLayoutService } from '../../../layouts/layouts.module';
import { SalesPaymentRestService } from '../sales-payment-rest.service';

@Component({
  selector: 'app-sales-payment-detail',
  templateUrl: 'sales-payment-detail.component.html'
})
export class SalesPaymentDetailComponent implements OnInit {
  doc: ISalesPayment;
  routeParams: any;

  mainReportParams: any;

  constructor(
    public _accounting: AccountingService,
    private _adminLayout: AdminLayoutService,
    private _route: ActivatedRoute,
    private _salesPaymentRest: SalesPaymentRestService,
  ) {
    this._adminLayout.containerType = 1;
    this._route.params.subscribe(params => this.routeParams = params);
  }

  ngOnInit() {
    this.loadData();
    this.loadRelatedData();
  }

  loadData() {
    this._salesPaymentRest.load(this.routeParams.id, { includeLines: true }).subscribe(receipt => {
      this.doc = receipt;
    });
  }

  loadRelatedData() {
    this._salesPaymentRest.loadRelatedData().subscribe(relatedData => {
      this.mainReportParams = {
        companyInfo: relatedData.settings.companyInfo,
      };
    });
  }
}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SalesPaymentCreateComponent } from './create/sales-payment-create.component';
import { SalesPaymentDetailComponent } from './detail/sales-payment-detail.component';
import { SalesPaymentListComponent } from './list/sales-payment-list.component';
import { SalesPaymentUpdateComponent } from './update/sales-payment-update.component';

import { AuthorizationService } from '../../core/core.module';

export const routes: Routes = [{
  path: '',
  children: [{
    path: '',
    component: SalesPaymentListComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'salesPayment.list'
    }
  }, {
    path: 'create',
    component: SalesPaymentCreateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'salesPayment.create'
    }
  }, {
    path: ':id',
    component: SalesPaymentDetailComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'salesPayment.detail'
    }
  }, {
    path: ':id/update',
    component: SalesPaymentUpdateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'salesPayment.update'
    }
  }]
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class SalesPaymentRoutingModule { }

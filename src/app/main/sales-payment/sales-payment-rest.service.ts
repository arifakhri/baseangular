import { Injectable } from '@angular/core';
import 'app/rxjs-imports.ts';

import { ApiQueryOption, RequestService } from '../../core/core.module';

import { APP_CONST } from '../../app.const';

@Injectable()
export class SalesPaymentRestService {
  baseURL = `${APP_CONST.API_MAIN}/sales/all-payments`;
  baseURLPayment = `${APP_CONST.API_MAIN}/sales/payments`;

  request = this._request.new(this.baseURL);
  requestPayment = this._request.new(this.baseURLPayment);

  constructor(
    private _request: RequestService,
  ) { }

  create(payment: ISalesPayment, queryParams: any = {}) {
    return this.requestPayment.post<ISalesPayment>(``, payment, { params: queryParams });
  }

  findAll(queryOption: ApiQueryOption, queryParams: any = {}) {
    return this.request.post<IApiPaginationResult<ISalesPayment>>(`q`, queryOption, { params: queryParams });
  }

  load(paymentId: string, queryParams: any = {}) {
    return this.requestPayment.get<ISalesPayment>(`${paymentId}`, { params: queryParams });
  }

  loadRelatedData() {
    return this.requestPayment.get<{
      paymentMethods: IPaymentMethod[],
      paymentAccounts: IAccount[],
      settings: ISettings
    }>(`entry-related-data`);
  }

  update(paymentId: string, updateObj: ISalesPayment, queryParams: any = {}) {
    return this.requestPayment.put<ISalesPayment>(`${paymentId}`, updateObj, { params: queryParams });
  }

  void(paymentId: string) {
    return this.requestPayment.put<ISalesPayment>(`${paymentId}/void`, {});
  }
}

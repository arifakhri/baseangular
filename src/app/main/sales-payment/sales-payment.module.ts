import { AutoCompleteModule } from 'primeng/components/autocomplete/autocomplete';
import { CalendarModule } from 'primeng/components/calendar/calendar';
import { CheckboxModule } from 'primeng/components/checkbox/checkbox';
import { DataTableModule } from 'primeng/components/datatable/datatable';
import { ListboxModule } from 'primeng/components/listbox/listbox';
import { BsDropdownModule, ModalModule, PaginationModule, PopoverModule } from 'ngx-bootstrap';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { SalesPaymentCreateComponent } from './create/sales-payment-create.component';
import { SalesPaymentDetailComponent } from './detail/sales-payment-detail.component';
import { SalesPaymentFormComponent } from './form/sales-payment-form.component';
import { SalesPaymentListComponent } from './list/sales-payment-list.component';
import { SalesPaymentQuickCreateComponent } from './quick-create/sales-payment-quick-create.component';
import { SalesPaymentQuickFormComponent } from './quick-form/sales-payment-quick-form.component';
import { SalesPaymentMoreFilterComponent } from './more-filter/sales-payment-more-filter.component';
import { SalesPaymentUpdateComponent } from './update/sales-payment-update.component';

import { SalesPaymentMoreFilterService } from './more-filter/sales-payment-more-filter.service';
import { SalesPaymentRestService } from './sales-payment-rest.service';
import { SalesPaymentService } from './sales-payment.service';

import { BusinessPartnerModule } from '../business-partner/business-partner.module';
import { CoreModule } from '../../core/core.module';
import { PaymentMethodModule } from '../payment-method/payment-method.module';
import { SalesModule } from '../sales/sales.module';
import { SharedModule } from '../../shared/shared.module';
import { TransactionModule } from '../transaction/transaction.module';

export const PROVIDERS = [
  SalesPaymentMoreFilterService,
  SalesPaymentRestService,
  SalesPaymentService,
];

@NgModule({
  imports: [
    AutoCompleteModule,
    BsDropdownModule,
    BusinessPartnerModule,
    CalendarModule,
    CheckboxModule,
    CommonModule,
    CoreModule,
    DataTableModule,
    FlexLayoutModule,
    FormsModule,
    HttpModule,
    ListboxModule,
    ModalModule,
    PaginationModule,
    PaymentMethodModule,
    PopoverModule,
    ReactiveFormsModule,
    RouterModule,
    SalesModule,
    SharedModule,
    TransactionModule,
    TranslateModule,
  ],
  declarations: [
    SalesPaymentCreateComponent,
    SalesPaymentDetailComponent,
    SalesPaymentFormComponent,
    SalesPaymentListComponent,
    SalesPaymentMoreFilterComponent,
    SalesPaymentQuickCreateComponent,
    SalesPaymentQuickFormComponent,
    SalesPaymentUpdateComponent,
  ],
  exports: [
    SalesPaymentQuickCreateComponent,
  ]
})
export class SalesPaymentModule { }

import * as _ from 'lodash';
import { Component, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import 'app/rxjs-imports.ts';

import { SalesPaymentRestService } from '../sales-payment-rest.service';
import { SalesPaymentService } from '../sales-payment.service';

import { BaseCreateBComponent } from '../../../shared/base/base.module';
import { MSalesPayment } from '../sales-payment.model';

@Component({
  selector: 'app-sales-payment-create',
  templateUrl: 'sales-payment-create.component.html'
})
export class SalesPaymentCreateComponent extends BaseCreateBComponent {
  @ViewChild('reportModal') elReportModal: ModalDirective;

  constructor(
    private _salesPayment: SalesPaymentService,
    private _salesPaymentRest: SalesPaymentRestService
  ) {
    super();

    this.componentId = 'SalesPaymentCreate';
    this.headerTitle = 'ui.salesPayment.create.title';
    this.containerType = 1;
    this.routeURL = '/sales/payments';
    this.entrySuccessI18n = 'success.salesPayment.create';

    this.autoprintButtons = [{
      checkboxConfigId: 'autoprint.spayment.create',
      checkboxLabel: 'ui.salesPayment.create.action.apCheckbox.report',
      previewLabel: 'ui.salesPayment.create.action.previewReport',
      onPreview: () => {
        this.saveAndPrint();
      }
    }];

    this.registerHook('buildForm', event => {
      this._salesPayment.setFormDefinitions(this.form);
    });


    this.registerHook('save', event => {
      let payment = new MSalesPayment;

      payment = _.assign(payment, this.form.value);
      this._salesPayment.normalizeDoc(payment);

      const isPrinting = _.get(event, 'data.print');

      return this._salesPaymentRest.create(payment, { includeLines: isPrinting });
    });
  }

  async saveAndPrint() {
    try {
      const submitted = await this.onSubmit({ saveConfirm: true, print: true });
      if (submitted) {
        this._salesPaymentRest.loadRelatedData().subscribe(related => {
          this.doc['companyInfo'] = related.settings.companyInfo;
          this.elReportModal.show();
        });
      }
    } catch (e) {

    }
  }
}

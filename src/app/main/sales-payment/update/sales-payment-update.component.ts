import * as _ from 'lodash';
import * as moment from 'moment';
import { Component, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';

import { SalesPaymentRestService } from '../sales-payment-rest.service';
import { SalesPaymentService } from '../sales-payment.service';

import { BaseUpdateBComponent } from '../../../shared/base/base.module';

@Component({
  selector: 'app-sales-payment-update',
  templateUrl: 'sales-payment-update.component.html',
})
export class SalesPaymentUpdateComponent extends BaseUpdateBComponent {
  @ViewChild('reportModal') elReportModal: ModalDirective;

  constructor(
    private _salesPayment: SalesPaymentService,
    private _salesPaymentRest: SalesPaymentRestService,
  ) {
    super();

    this.componentId = 'SalesPaymentUpdate';
    this.headerTitle = 'ui.salesPayment.update.title';
    this.containerType = 1;
    this.routeURL = '/sales/payments';
    this.entrySuccessI18n = 'success.salesPayment.update';

    this.registerHook('buildForm', event => {
      this._salesPayment.setFormDefinitions(this.form);
    });

    this.registerHook('load', event => {
      return this._salesPaymentRest.load(this.page.routeParams.id, { includeLines: true }).switchMap(doc => {
        if (doc.transactionDate) {
          doc.transactionDate = moment(doc.transactionDate).toDate();
        }
        return Observable.of(doc);
      });
    });

    this.registerHook('save', event => {
      const payment: ISalesPayment = Object.assign({}, this.doc, this.form.value);
      this._salesPayment.normalizeDoc(payment);

      const isPrinting = _.get(event, 'data.print');

      return this._salesPaymentRest.update(this.page.routeParams.id, payment, { includeLines: isPrinting });
    });
  }


  async saveAndPrint() {
    try {
      const submitted = await this.onSubmit({ saveConfirm: true, print: true });
      if (submitted) {
        this._salesPaymentRest.loadRelatedData().subscribe(related => {
          this.doc['companyInfo'] = related.settings.companyInfo;
          this.elReportModal.show();
        });
      }
    } catch (e) {

    }
  }
}

import { AutoComplete } from 'primeng/components/autocomplete/autocomplete';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ModalDirective } from 'ngx-bootstrap';

import { AccountingService, AutocompleteService } from '../../../core/core.module';
import { SalesPaymentRestService } from '../sales-payment-rest.service';

@Component({
  selector: 'app-sales-payment-quick-form',
  templateUrl: 'sales-payment-quick-form.component.html'
})
export class SalesPaymentQuickFormComponent implements OnInit {
  @Input() form: FormGroup;
  @Input() sourceTransaction: ISalesInvoice;
  @ViewChild('quickCreateModal') elQuickCreateModal: ModalDirective;

  quickCreateAC: AutoComplete;

  accounts: IAccount[] = [];
  paymentMethods: IPaymentMethod[] = [];

  constructor(
    public _accounting: AccountingService,
    public _autocomplete: AutocompleteService,
    private _salesPaymentRest: SalesPaymentRestService,
  ) { }

  ngOnInit() {
    this.loadRelatedData();
  }

  assignQuickCreatedEntity(entity: any, storageVar: string) {
    this[storageVar].push(entity);
    this.quickCreateAC.selectItem(entity);
    this.elQuickCreateModal.hide();
  }

  loadRelatedData() {
    this._salesPaymentRest.loadRelatedData().subscribe(related => {
      this.accounts = related.paymentAccounts;
      this.paymentMethods = related.paymentMethods;
    });
  }
}

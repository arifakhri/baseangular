import * as _ from 'lodash';
import * as SnackBar from 'node-snackbar';
import swal from 'sweetalert2';
import { ActivatedRoute } from '@angular/router';
import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { DataTable } from 'primeng/components/datatable/datatable';
import { ModalDirective, PopoverDirective } from 'ngx-bootstrap';
import { Observable, Subscription } from 'rxjs';
import 'app/rxjs-imports.ts';
import { TranslateService } from '@ngx-translate/core';

import { SpinnerDirective } from '../../../shared/spinner/spinner.directive';

import {
  AccountingService,
  ExportDataTableService,
  GridTableDataSource,
  GridTableService,
  GridTableToggleService,
  SystemMessageService,
} from '../../../core/core.module';
import { AdminLayoutService } from '../../../layouts/layouts.module';
import { SalesPaymentMoreFilterService } from '../more-filter/sales-payment-more-filter.service';
import { SalesPaymentRestService } from '../sales-payment-rest.service';
import { SalesDownPaymentRestService } from '../../sales-down-payment/sales-down-payment-rest.service';
import { RetryDialogComponent } from '../../../shared/retry-dialog/retry-dialog.component';

@Component({
  selector: 'app-sales-payment-list',
  templateUrl: 'sales-payment-list.component.html',
  providers: [SystemMessageService]
})
export class SalesPaymentListComponent implements OnDestroy, OnInit {
  @ViewChild('gridTable') public gridTable: DataTable;
  @ViewChild('pageLoading') elPageLoading: SpinnerDirective;
  @ViewChild('popMoreFilter') elPopMoreFilter: PopoverDirective;
  @ViewChild('reportModal') elReportModal: ModalDirective;
  @ViewChild('retryDialog') elRetryDialog: RetryDialogComponent;

  compReady: boolean;
  doc: ISalesPayment;
  payments: ISalesPayment[] = [];
  gridDataSource: GridTableDataSource<ISalesPayment> = new GridTableDataSource<ISalesPayment>();
  selectedRecords: ISalesPayment[] = [];

  combinedFilterValues: string = '';
  combinedFilterFields = {
    transactionNumber: true,
    'customer.displayName': true,
  };

  qParamsFilters: any = {
    keyword: null
  };

  moreFilterValues: IGridTableFilterParsed;

  tableColumns: IGridTableColumn[] = [{
    i18nLabel: 'ui.salesPayment.list.column.transactionDate',
    field: 'transactionDate',
    sortField: 'transactionEntry.transactionDate',
    dateFormat: 'DD/MM/YYYY',
    sort: true,
  }, {
    i18nLabel: 'ui.salesPayment.list.column.transactionType',
    field: 'transactionType',
    sort: true,
    formatter: (value, row: ISalesPayment) => {
      return _.startCase(value);
    },
  }, {
    i18nLabel: 'ui.salesPayment.list.column.transactionNumber',
    field: 'transactionNumber',
    link: (row) => {
      let url;
      switch (row.transactionType) {
        case 'sales_payment':
          url = `/sales/payments/${row.transactionId}`
          break;
        case 'sales_down_payment':
          url = `/sales/down-payments/${row.transactionId}`
          break;
        case 'sales_refund':
          url = `/sales/refunds/${row.transactionId}`
          break;
      }
      return url;
    },
    sort: true,
  }, {
    i18nLabel: 'ui.salesPayment.list.column.customerName',
    field: 'customer.displayName',
    sort: true,
    formatter: (value, row: ISalesPayment) => {
      return row.customCustomerName ? value + ' (' + row.customCustomerName + ')' : value;
    },
  }, {
    i18nLabel: 'ui.salesPayment.list.column.total',
    field: 'total',
    sort: true,
    formatter: (value, row: ISalesPayment) => {
      return this._accounting.ac.formatMoney(value, '');
    },
    columnClasses: 'right-align',
  }, {
    i18nLabel: 'ui.salesPayment.list.column.status',
    field: 'status',
    sort: true,
    formatter: (value, row: ISalesPayment) => {
      return _.startCase(value);
    },
  }];
  tableColumnsShow: boolean = false;
  tableColumnsToggle = this._gridTableToggle.mapToggleOptionsFromColumns(this.tableColumns);

  retryDialogShownState: boolean = false;
  retryDialogOnRetry: Function;

  private loadDataTimeoutId: number;
  private gridFirstLoad: boolean = true;
  private subscriptions: { [key: string]: Subscription[] } = {
    load: []
  };

  constructor(
    public _accounting: AccountingService,
    private _activatedRoute: ActivatedRoute,
    private _adminLayout: AdminLayoutService,
    private _gridTable: GridTableService,
    private _dataTableExport: ExportDataTableService,
    private _translate: TranslateService,
    private _gridTableToggle: GridTableToggleService,
    private _salesPaymentMoreFilter: SalesPaymentMoreFilterService,
    private _salesPaymentRest: SalesPaymentRestService,
    private _salesDownPaymentRest: SalesDownPaymentRestService,
    public _systemMessage: SystemMessageService,
  ) {
    this._adminLayout.containerType = 1;
  }

  ngOnInit() {
    this.loadData();
  }

  ngOnDestroy() {
    this._salesPaymentMoreFilter.lastMoreFilterValues = {};
  }

  onMoreFilter(filters: IGridTableFilterParsed) {
    this.elPopMoreFilter.hide();

    this.moreFilterValues = filters;
    this.loadData();
  }

  populateFilters() {
    let newFilters: any = _.pick(this.qParamsFilters, ['keyword']);

    if (this.moreFilterValues) {
      newFilters = Object.assign({}, newFilters, this.moreFilterValues.qParams);
    }

    this.qParamsFilters = newFilters;
  }

  gridLoadDataWrapper() {
    // Grid always load at the first time. we dont want that. we already call on ngOnInit
    if (this.gridFirstLoad) {
      this.gridFirstLoad = false;
      return;
    }

    if (this.loadDataTimeoutId) {
      window.clearTimeout(this.loadDataTimeoutId);
      this.loadDataTimeoutId = null;
    }

    this.loadDataTimeoutId = window.setTimeout(() => {
      this.loadData();
    }, 300);
  }

  showVoidDialog(paymentId: string, paymentType: string) {
    const type = _.camelCase(paymentType);
    swal({
      title: this._translate.instant(`confirm.${type}.void.label`),
      text: this._translate.instant(`confirm.${type}.void.description`),
      type: 'question',
      showCancelButton: true,
    })
      .then(() => {
        this.voidPayment(paymentId, type);
      })
      .catch(() => { });
  }

  voidPayment(paymentId: string, type: string) {
    const restType = `_${type}Rest`;
    this[restType].void(paymentId)
      .catch((error) => {
        this._systemMessage.log({
          message: error.response.data[0].errorMessage,
          type: 'error'
        });
        return Observable.throw(error);
      })
      .subscribe(response => {
      this.gridLoadDataWrapper();
      SnackBar.show({
        text: this._translate.instant(`success.${type}.void`),
        pos: 'bottom-right'
      });
    });
  }

  printReport(paymentId: string, type: string) {
    const paymentType = _.camelCase(type.slice(0, type.indexOf('_')) + type.slice(type.indexOf('_')));
    const restType = `_${paymentType}Rest`;
    this[restType].load(paymentId).subscribe(response => {
      this.doc = response;
      this[restType].loadRelatedData().subscribe(related => {
        this.doc['companyInfo'] = related.settings.companyInfo;
        this.elReportModal.show();
      });
    });
  }

  get exportRecords(): Observable<ISalesPayment[]> {
    const qOption = {
      ...this._gridTable.generateApiQueryOptionFromGridInfo(
        this.gridDataSource.pager,
        false,
        this.gridTable,
        this._gridTable.generateCombinedApiFilters(this.combinedFilterValues, this.combinedFilterFields)
      ),
      skip: 0,
      take: this._dataTableExport.exportLimit,
    };

    this.populateFilters();

    return this._salesPaymentRest.findAll(qOption, this.qParamsFilters).map(response => response.data);
  }

  exportPDF() {
    this.exportRecords.subscribe(records => {
      this._dataTableExport.export({
        records,
        mapOptions: this._dataTableExport.mapFromTableColumns(this.tableColumns),
        fileName: 'sales-payment',
        extension: 'pdf',
        templateData: {
          title: this._translate.instant('ui.transaction.list.title'),
        },
      });
    });
  }

  exportExcel() {
    this.exportRecords.subscribe(records => {
      this._dataTableExport.export({
        records: records,
        mapOptions: this._dataTableExport.mapFromTableColumns(this.tableColumns),
        fileName: 'sales-payment',
        extension: 'xls',
      });
    });
  }

  loadData() {
    this.elPageLoading.forceShow();

    const qOption = this._gridTable.generateApiQueryOptionFromGridInfo(
      this.gridDataSource.pager,
      true,
      this.gridTable,
      this._gridTable.generateCombinedApiFilters(this.combinedFilterValues, this.combinedFilterFields)
    );

    this.populateFilters();

    this.subscriptions.load.forEach(sub => sub.unsubscribe());
    this.subscriptions.load.push(
      this.elRetryDialog.createRetryEntry(
        this._salesPaymentRest.findAll(qOption, this.qParamsFilters)
          .finally(() => {
            this.elPageLoading.forceHide();
          })
      ).subscribe(response => {
        this.payments = response.data;
        this.gridDataSource.updateFromApiPaginationResult(response);

        this.compReady = true;
      })
    );
  }

  public onGridChange() {
    this.gridLoadDataWrapper();
  }

  public onGridPageChange() {
    this.gridLoadDataWrapper();
  }
}

import * as _ from 'lodash';
import * as moment from 'moment';
import { Component, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';

import { SalesReceiptFormService } from '../form/sales-receipt-form.service';
import { SalesReceiptRestService } from '../sales-receipt-rest.service';
import { SalesReceiptService } from '../sales-receipt.service';

import { BaseUpdateBComponent } from '../../../shared/base/base.module';
import { MSalesReceipt } from '../sales-receipt.model';

@Component({
  selector: 'app-sales-receipt-update',
  templateUrl: 'sales-receipt-update.component.html',
})
export class SalesReceiptUpdateComponent extends BaseUpdateBComponent {
  @ViewChild('reportModal') elReportModal: ModalDirective;

  constructor(
    private _salesReceipt: SalesReceiptService,
    private _salesReceiptForm: SalesReceiptFormService,
    private _salesReceiptRest: SalesReceiptRestService,
  ) {
    super();

    this.componentId = 'SalesReceiptUpdate';
    this.headerTitle = 'ui.salesReceipt.update.title';
    this.containerType = 1;
    this.routeURL = '/sales/receipts';
    this.entrySuccessI18n = 'success.salesReceipt.update';

    this.registerHook('buildForm', event => {
      this._salesReceiptForm.setFormDefinitions(this.form);
    });

    this.registerHook('load', event => {
      return this._salesReceiptRest.load(this.page.routeParams.id).switchMap(doc => {
        if (doc.transactionDate) {
          doc.transactionDate = moment(doc.transactionDate).toDate();
        }
        return Observable.of(doc);
      });
    });

    this.registerHook('save', event => {
      const receipt: MSalesReceipt = Object.assign({}, this.doc, this.form.value);
      this._salesReceipt.normalizeDoc(receipt);

      const isPrinting = _.get(event, 'data.print');

      return this._salesReceiptRest.update(this.page.routeParams.id, receipt, { includeLines: isPrinting });
    });
  }

  async saveAndPrint() {
    try {
      const submitted = await this.onSubmit({ saveConfirm: true, print: true });
      if (submitted) {
        this._salesReceiptRest.loadRelatedData().subscribe(related => {
          this.doc['companyInfo'] = related.settings.companyInfo;
          this.elReportModal.show();
        });
      }
    } catch (e) {

    }
  }
}

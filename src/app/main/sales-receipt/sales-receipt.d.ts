declare interface ISalesReceipt {
  lines: [
    {
      id: string;
      transactionId: string;
      lineNumber: number;
      productId: string;
      productVariantId: string;
      description: string;
      qty: number;
      unitPrice: number;
      discountPercent: number;
      discountAmount: number;
      totalBeforeDiscount: number;
      totalDiscount: number;
      total: number;
      uomId: string;
      uomConversion: number;
      baseQty: number;
      taxId: string;
      taxRate: number;
      taxAmount: number;
      note: string;
      rowVersion: string;
      product: IProduct;
      productVariant: IProductVariant;
      uom: {
        id: string;
        name: string;
        systemDefault: boolean
      };
      tax: {
        id: string;
        code: string;
        name: string;
        rate: number
      }
    }
  ];
  id: string;
  transactionType: string;
  transactionNumber: string;
  transactionDate: string | Date;
  customerId: string;
  customCustomerName: string;
  customerRefNumber: string;
  customerEmail: string;
  customerMobile: string;
  dropship: boolean;
  dropshipCustomerName: string;
  dropshipCustomerPhone: string;
  billingAddress: string;
  shippingAddress: string;
  lineTotalBeforeDiscount: number;
  lineTotalDiscount: number;
  subtotal: number;
  discountPercent: number;
  discountAmount: number;
  taxAmount: number;
  shippingCharge: number;
  adjustmentAmount: number;
  total: number;
  taxed: boolean;
  note: string;
  customerNote: string;
  status: string;
  state: string;
  rowVersion: string;
  customer: {
    salesTaxable: boolean;
    defaultSalesTaxId: string;
    defaultSalesInvoiceDueDays: number;
    purchaseTaxable: boolean;
    defaultPurchaseTaxId: string;
    defaultPurchaseInvoiceDueDays: number;
    isCustomer: boolean;
    isVendor: boolean;
    id: string;
    contactType: string;
    code: string;
    displayName: string;
    company: string;
    fullName: string;
    phone: string;
    mobile: string;
    fax: string;
    email: string;
    website: string;
    inactive: boolean;
    postalAddress: {
      attention: string;
      street1: string;
      street2: string;
      city: string;
      stateProvince: string;
      countryId: string;
      postalCode: string;
      longitude: number;
      latitude: number;
      country: {
        code: string;
        name: string
      }
    }
  }
}
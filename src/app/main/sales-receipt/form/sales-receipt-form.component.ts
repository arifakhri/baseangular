import * as _ from 'lodash';
import { Component, OnChanges, OnInit, SimpleChanges, } from '@angular/core';
import { FormGroup } from '@angular/forms';
import 'app/rxjs-imports.ts';

import { AccountingService, AutocompleteService, CommonService } from '../../../core/core.module';
import { ProductVariantRestService } from '../../product-variant/product-variant.module';
import { SalesReceiptCloneService } from '../../sales-receipt/sales-receipt-clone.service';
import { SalesReceiptFormService } from '../form/sales-receipt-form.service';
import { SalesReceiptRestService } from '../sales-receipt-rest.service';

import { TransactionFormSalesBComponent } from '../../transaction/transaction-form-sales.bcomponent';

@Component({
  selector: 'app-sales-receipt-form',
  templateUrl: 'sales-receipt-form.component.html',
})
export class SalesReceiptFormComponent extends TransactionFormSalesBComponent implements OnChanges, OnInit {
  cloneTransaction: boolean = false;

  ACProductVariantHandler = CommonService.remoteACItemsHandler({
    remoteParams: this.ACProductVariantParams.bind(this),
    remoteRequest: this._productVariantRest.findAllPicker.bind(this._productVariantRest),
    remoteRequestMap: (response) => response.data,
  });

  constructor(
    public _autocomplete: AutocompleteService,
    public _accounting: AccountingService,
    public _productVariantRest: ProductVariantRestService,
    private _salesReceiptClone: SalesReceiptCloneService,
    private _salesReceiptForm: SalesReceiptFormService,
    private _salesReceiptRest: SalesReceiptRestService,
  ) {
    super();
    super.init();

    this.componentId = 'SalesReceiptForm';

    this.registerHook('loadRelated', event => {
      return this._salesReceiptRest.loadRelatedData();
    }, relatedData => {
      this._salesReceiptForm.patchTransactionSettings(this.form, relatedData.settings);

      if (this._salesReceiptClone.docOnHold) {
        const parentDoc = <any>this._salesReceiptClone.docOnHold;
        parentDoc.deliveryDate = null;
        parentDoc.transactionDate = new Date;
        parentDoc.transactionNumber = null;

        this.cloneTransaction = true;
        this.doc = parentDoc;
        this._salesReceiptClone.docOnHold = null;

        this.applyDoc();

        this.form.patchValue(this.doc);
      }
    });

    this.registerHook('patchForm', event => {
      this.applyDoc();
    });
  }

  ngOnInit() {
    super.ngOnInit();

    this.addLines();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (_.has(changes, 'doc') && this.doc) {
      this.applyDoc();
    }
  }

  getDefaultValue(type: 'master' | 'line'): any {
    return this._salesReceiptForm.getDefaultValues(type);
  }

  buildFormChildLine() {
    return this._salesReceiptForm.buildFormChildLine();
  }

  lineConditionalValidation(formGroup: FormGroup) {
    this._salesReceiptForm.lineConditionalValidation(formGroup);
  }
}

import * as _ from 'lodash';
import { CustomValidators } from 'ng2-validation';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { Injectable } from '@angular/core';

import { MSalesReceiptForm, MSalesReceiptFormLine } from './sales-receipt-form.model';

import { StartingDataService } from '../../../core/core.module';

@Injectable()
export class SalesReceiptFormService {
  constructor(
    private _startingData: StartingDataService,
  ) { }

  getDefaultValues(type: 'master' | 'line'): { [key: string]: any } {
    let model: any;
    switch (type) {
      case 'master':
        const startingData = this._startingData.data$.getValue();
        model = new MSalesReceiptForm;
        model.defaultValues = {
          branchId: startingData.masterBranchId,
        };
        break;
      case 'line':
        const startingDataLine = this._startingData.data$.getValue();
        model = new MSalesReceiptFormLine;
        model.defaultValues = {
          uomConversion: 1,
          warehouseId: startingDataLine.masterWarehouseId,
        };
        break;
    }

    return model;
  }

  patchTransactionSettings(form: FormGroup, settings: any) {
    if (form.contains('warehouseId') && !form.get('warehouseId').value && _.has(settings.productSetting, 'masterWarehouseId')) {
      form.patchValue({ warehouse: settings.productSetting.masterWarehouse });
      form.patchValue({ warehouseId: settings.productSetting.masterWarehouseId });
    }
  }

  setFormDefinitions(form: FormGroup) {
    const customerEmailControl = new FormControl('', [CustomValidators.email, Validators.maxLength(150)]);
    (<any>customerEmailControl).validatorData = {
      maxLength: 150
    };
    const customerMobileControl = new FormControl('', [CustomValidators.digits, Validators.maxLength(30)]);
    (<any>customerMobileControl).validatorData = {
      maxLength: 30
    };
    const customerPrintNameControl = new FormControl('', Validators.maxLength(50));
    (<any>customerPrintNameControl).validatorData = {
      maxLength: 50
    };
    const customerRefNumberControl = new FormControl('', Validators.maxLength(30));
    (<any>customerRefNumberControl).validatorData = {
      maxLength: 30
    };


    form.addControl('branchId', new FormControl);
    form.addControl('customer', new FormControl);
    form.addControl('customerId', new FormControl(null, Validators.required));
    form.addControl('customerEmail', customerEmailControl);
    form.addControl('customerMobile', customerMobileControl);
    form.addControl('billingAddress', new FormControl);
    form.addControl('shippingAddress', new FormControl);
    form.addControl('description', new FormControl);
    form.addControl('paymentMethod', new FormControl);
    form.addControl('paymentMethodId', new FormControl);
    form.addControl('paymentAccount', new FormControl(null, Validators.required));
    form.addControl('paymentAccountId', new FormControl(null, Validators.required));
    form.addControl('transactionNumber', new FormControl);
    form.addControl('customCustomerName', customerPrintNameControl);
    form.addControl('customerRefNumber', customerRefNumberControl);
    form.addControl('transactionDate', new FormControl(new Date(), Validators.required));
    form.addControl('customerNote', new FormControl);
    form.addControl('subtotal', new FormControl);
    form.addControl('discountPercent', new FormControl('', CustomValidators.number));
    form.addControl('discountAmount', new FormControl(null, [Validators.required, CustomValidators.number]));
    form.addControl('taxAmount', new FormControl(null));
    form.addControl('shippingCharge', new FormControl(null, [Validators.required, CustomValidators.number]));
    form.addControl('adjustmentAmount', new FormControl(null, [Validators.required, CustomValidators.number]));
    form.addControl('total', new FormControl(0));
    form.addControl('taxed', new FormControl);
    form.addControl('note', new FormControl);
    form.addControl('useCustomCustomerName', new FormControl(false));
    form.addControl('warehouse', new FormControl);
    form.addControl('warehouseId', new FormControl(null, Validators.required));
    form.addControl('lines', new FormArray([]));

    form.get('paymentMethod').valueChanges.subscribe(paymentMethod => {
      if (paymentMethod && paymentMethod.defaultAccount) {
        form.get('paymentAccount').setValue(paymentMethod.defaultAccount);
        form.get('paymentAccountId').setValue(paymentMethod.defaultAccount.id);
      }
    });

    form.patchValue(this.getDefaultValues('master'));
  }

  buildFormChildLine() {
    const startingData = this._startingData.data$.getValue();

    const formGroup = new FormGroup({
      id: new FormControl(null),
      rowVersion: new FormControl,
      amount: new FormControl,
      description: new FormControl,
      discountPercent: new FormControl,
      discountAmount: new FormControl,
      incomeAccountId: new FormControl,
      note: new FormControl,
      product: new FormControl,
      productId: new FormControl,
      productVariant: new FormControl,
      productVariantId: new FormControl,
      qty: new FormControl,
      tax: new FormControl,
      taxId: new FormControl,
      taxRate: new FormControl,
      totalDiscount: new FormControl,
      unitPrice: new FormControl,
      uom: new FormControl,
      uomId: new FormControl,
      uomConversion: new FormControl,
      warehouseId: new FormControl,
    });

    formGroup.patchValue(this.getDefaultValues('line'));

    return formGroup;
  }

  lineConditionalValidation(formGroup: FormGroup) {
    const formGroupValue = formGroup.value;
    if (
      formGroup.dirty &&
      _.size(<any>_.pickBy(_.omit(formGroupValue, ['amount', 'uomConversion', 'warehouseId']), _.identity))
    ) {
      formGroup.get('description').setValidators([Validators.required]);
      formGroup.get('description').updateValueAndValidity();
      formGroup.get('qty').setValidators([Validators.required]);
      formGroup.get('qty').updateValueAndValidity();
      formGroup.get('unitPrice').setValidators([Validators.required]);
      formGroup.get('unitPrice').updateValueAndValidity();
    } else {
      formGroup.get('description').clearValidators();
      formGroup.get('description').updateValueAndValidity();
      formGroup.get('qty').clearValidators();
      formGroup.get('qty').updateValueAndValidity();
      formGroup.get('unitPrice').clearValidators();
      formGroup.get('unitPrice').updateValueAndValidity();
    }
  }
}

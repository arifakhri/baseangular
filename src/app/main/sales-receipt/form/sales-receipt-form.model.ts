import { MBase } from '../../../core/common/base.model';

export class MSalesReceiptForm extends MBase {
  adjustmentAmount: number = 0;
  branchId: number = 0;
  billingAddress: string;
  customerEmail: string;
  customer: string;
  customerId: string;
  customerMobile: string;
  customerNote: string;
  customCustomerName: string;
  customerRefNumber: string;
  description: string;
  discountAmount: number = 0;
  discountPercent: number = null;
  lines: MSalesReceiptFormLine[] = [];
  note: string;
  paymentAccount: string;
  paymentAccountId: string;
  paymentMethod: string;
  paymentMethodId: string;
  shippingAddress: string;
  shippingCharge: number = 0;
  subtotal: number = 0;
  taxAmount: number = 0;
  taxed: boolean = true;
  total: number = 0;
  transactionDate: string;
  transactionNumber: string;
}

export class MSalesReceiptFormLine extends MBase {
  amount: number = 0;
  description: string;
  discountAmount: number = 0;
  discountPercent: number = null;
  incomeAccountId: string;
  note: string;
  product: string;
  productId: string;
  productVariant: string;
  productVariantId: string;
  qty: number = 0;
  tax: string;
  taxId: string;
  taxRate: number = 0;
  totalDiscount: number = 0;
  unitPrice: number = 0;
  uomConversion: number = 0;
  uom: string;
  uomId: string;
  warehouseId: string;
}

import * as _ from 'lodash';
import { Component, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import 'app/rxjs-imports.ts';

import { SalesReceiptFormService } from '../form/sales-receipt-form.service';
import { SalesReceiptRestService } from '../sales-receipt-rest.service';
import { SalesReceiptService } from '../sales-receipt.service';

import { BaseCreateBComponent } from '../../../shared/base/base.module';
import { MSalesReceipt } from '../sales-receipt.model';

@Component({
  selector: 'app-sales-receipt-create',
  templateUrl: 'sales-receipt-create.component.html',
})
export class SalesReceiptCreateComponent extends BaseCreateBComponent {
  @ViewChild('reportModal') elReportModal: ModalDirective;

  constructor(
    private _salesReceipt: SalesReceiptService,
    private _salesReceiptForm: SalesReceiptFormService,
    private _salesReceiptRest: SalesReceiptRestService,
  ) {
    super();

    this.componentId = 'SalesReceiptCreate';
    this.headerTitle = 'ui.salesReceipt.create.title';
    this.containerType = 1;
    this.routeURL = '/sales/receipts';
    this.entrySuccessI18n = 'success.salesReceipt.create';


    this.autoprintButtons = [{
      checkboxConfigId: 'autoprint.sreceipt.create',
      checkboxLabel: 'ui.salesReceipt.create.action.apCheckbox.report',
      previewLabel: 'ui.salesReceipt.create.action.previewReport',
      onPreview: () => {
        this.saveAndPrint();
      }
    }];

    this.registerHook('buildForm', event => {
      this._salesReceiptForm.setFormDefinitions(this.form);
    });

    this.registerHook('save', event => {
      let receipt = new MSalesReceipt;
      receipt = _.assign(receipt, this.form.value);
      this._salesReceipt.normalizeDoc(receipt);

      const isPrinting = _.get(event, 'data.print');

      return this._salesReceiptRest.create(receipt, { includeLines: isPrinting });
    });
  }

  async saveAndPrint() {
    try {
      const submitted = await this.onSubmit({ saveConfirm: true, print: true });
      if (submitted) {
        this._salesReceiptRest.loadRelatedData().subscribe(related => {
          this.doc['companyInfo'] = related.settings.companyInfo;
          this.elReportModal.show();
        });
      }
    } catch (e) {

    }
  }
}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SalesReceiptCreateComponent } from './create/sales-receipt-create.component';
import { SalesReceiptDetailComponent } from './detail/sales-receipt-detail.component';
import { SalesReceiptListComponent } from './list/sales-receipt-list.component';
import { SalesReceiptUpdateComponent } from './update/sales-receipt-update.component';

import { AuthorizationService } from '../../core/core.module';

export const routes: Routes = [{
  path: '',
  children: [{
    path: '',
    component: SalesReceiptListComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'salesReceipt.list',
    },
  }, {
    path: 'create',
    component: SalesReceiptCreateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'salesReceipt.create',
    },
  }, {
    path: ':id',
    component: SalesReceiptDetailComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'salesReceipt.detail',
    },
  }, {
    path: ':id/update',
    component: SalesReceiptUpdateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'salesReceipt.update',
    },
  }]
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class SalesReceiptRoutingModule { }

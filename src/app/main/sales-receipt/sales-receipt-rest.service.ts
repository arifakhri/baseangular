import { Injectable } from '@angular/core';
import 'app/rxjs-imports.ts';

import { ApiQueryOption, RequestService } from '../../core/core.module';

import { APP_CONST } from '../../app.const';
import { MSalesReceipt } from './sales-receipt.model';

@Injectable()
export class SalesReceiptRestService {
  baseURL = `${APP_CONST.API_MAIN}/sales/receipts`;
  request = this._request.new(this.baseURL);

  constructor(
    private _request: RequestService,
  ) { }

  create(receipt: MSalesReceipt, queryParams: any = {}) {
    return this.request.post<MSalesReceipt>(``, receipt, { params: queryParams });
  }

  findAll(queryOption: ApiQueryOption, queryParams: any = {}) {
    return this.request.post<IApiPaginationResult<MSalesReceipt>>(`q`, queryOption, { params: queryParams });
  }

  load(receiptId: string) {
    return this.request.get<MSalesReceipt>(`${receiptId}?includeLines=true`);
  }

  loadRelatedData() {
    return this.request.get<{
      paymentAccounts: IAccount[],
      paymentMethods: IPaymentMethod[],
      taxes: ITax[],
      warehouses: IWarehouse[],
      settings: ISettings,
    }>(`entry-related-data`);
  }

  update(receiptId: string, updateObj: MSalesReceipt, queryParams: any = {}) {
    return this.request.put<MSalesReceipt>(`${receiptId}`, updateObj, { params: queryParams });
  }

  void(receiptId: string) {
    return this.request.put<MSalesReceipt>(`${receiptId}/void`, {});
  }
}

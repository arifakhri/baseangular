import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { AccountingService } from '../../../core/core.module';
import { AdminLayoutService } from '../../../layouts/layouts.module';
import { SalesReceiptCloneService } from '../sales-receipt-clone.service';
import { SalesReceiptRestService } from '../sales-receipt-rest.service';

import { MSalesReceipt } from '../sales-receipt.model';

@Component({
  selector: 'app-sales-receipt-detail',
  templateUrl: 'sales-receipt-detail.component.html'
})
export class SalesReceiptDetailComponent implements OnInit {
  doc: MSalesReceipt;
  routeParams: any;

  mainReportParams: any;

  constructor(
    public _accounting: AccountingService,
    private _adminLayout: AdminLayoutService,
    private _route: ActivatedRoute,
    private _salesReceiptClone: SalesReceiptCloneService,
    private _salesReceiptRest: SalesReceiptRestService,
  ) {
    this._adminLayout.containerType = 1;
    this._route.params.subscribe(params => this.routeParams = params);
  }

  ngOnInit() {
    this.loadData();
    this.loadRelatedData();
  }

  loadData() {
    this._salesReceiptRest.load(this.routeParams.id).subscribe(receipt => {
      this.doc = receipt;
    });
  }

  loadRelatedData() {
    this._salesReceiptRest.loadRelatedData().subscribe(relatedData => {
      this.mainReportParams = {
        companyInfo: relatedData.settings.companyInfo,
      };
    });
  }
}

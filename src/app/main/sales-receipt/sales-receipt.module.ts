import { AutoCompleteModule } from 'primeng/components/autocomplete/autocomplete';
import { BsDropdownModule, ModalModule, PaginationModule, PopoverModule } from 'ngx-bootstrap';
import { CalendarModule } from 'primeng/components/calendar/calendar';
import { CheckboxModule } from 'primeng/components/checkbox/checkbox';
import { CommonModule } from '@angular/common';
import { DataTableModule } from 'primeng/components/datatable/datatable';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { ListboxModule } from 'primeng/components/listbox/listbox';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { SalesReceiptCloneService } from './sales-receipt-clone.service';
import { SalesReceiptCreateComponent } from './create/sales-receipt-create.component';
import { SalesReceiptDetailComponent } from './detail/sales-receipt-detail.component';
import { SalesReceiptFormComponent } from './form/sales-receipt-form.component';
import { SalesReceiptListComponent } from './list/sales-receipt-list.component';
import { SalesReceiptMoreFilterComponent } from './more-filter/sales-receipt-more-filter.component';
import { SalesReceiptUpdateComponent } from './update/sales-receipt-update.component';

import { SalesReceiptFormService } from './form/sales-receipt-form.service';
import { SalesReceiptMoreFilterService } from './more-filter/sales-receipt-more-filter.service';
import { SalesReceiptRestService } from './sales-receipt-rest.service';
import { SalesReceiptService } from './sales-receipt.service';

import { BusinessPartnerModule } from '../business-partner/business-partner.module';
import { CoreModule } from '../../core/core.module';
import { PaymentMethodModule } from '../payment-method/payment-method.module';
import { ProductVariantModule } from '../product-variant/product-variant.module';
import { SalesModule } from '../sales/sales.module';
import { SharedModule } from '../../shared/shared.module';
import { TransactionModule } from '../transaction/transaction.module';

export const PROVIDERS = [
  SalesReceiptCloneService,
  SalesReceiptFormService,
  SalesReceiptMoreFilterService,
  SalesReceiptRestService,
  SalesReceiptService,
];

@NgModule({
  imports: [
    AutoCompleteModule,
    BsDropdownModule,
    BusinessPartnerModule,
    CalendarModule,
    CheckboxModule,
    CommonModule,
    CoreModule,
    DataTableModule,
    FlexLayoutModule,
    FormsModule,
    HttpModule,
    ListboxModule,
    ModalModule,
    PaginationModule,
    PaymentMethodModule,
    PopoverModule,
    ProductVariantModule,
    ReactiveFormsModule,
    RouterModule,
    SalesModule,
    SharedModule,
    TransactionModule,
    TranslateModule,
  ],
  declarations: [
    SalesReceiptCreateComponent,
    SalesReceiptDetailComponent,
    SalesReceiptFormComponent,
    SalesReceiptListComponent,
    SalesReceiptMoreFilterComponent,
    SalesReceiptUpdateComponent,
  ],
})
export class SalesReceiptModule { }

import { NgModule } from '@angular/core';

import { SalesReceiptModule } from './sales-receipt.module';
import { SalesReceiptRoutingModule } from './sales-receipt-routing.module';

@NgModule({
  imports: [
    SalesReceiptModule,
    SalesReceiptRoutingModule
  ]
})
export class SalesReceiptLazyModule { }

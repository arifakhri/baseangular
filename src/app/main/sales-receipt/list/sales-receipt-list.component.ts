import * as _ from 'lodash';
import * as SnackBar from 'node-snackbar';
import swal from 'sweetalert2';
import { ActivatedRoute } from '@angular/router';
import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { DataTable } from 'primeng/components/datatable/datatable';
import { ModalDirective, PopoverDirective } from 'ngx-bootstrap';
import { Observable, Subscription } from 'rxjs';
import 'app/rxjs-imports.ts';
import { TranslateService } from '@ngx-translate/core';

import { RetryDialogComponent } from '../../../shared/retry-dialog/retry-dialog.component';

import {
  AccountingService,
  ExportDataTableService,
  GridTableDataSource,
  GridTableService,
  GridTableToggleService,
  SystemMessageService,
} from '../../../core/core.module';
import { AdminLayoutService } from '../../../layouts/layouts.module';
import { SpinnerDirective } from '../../../shared/spinner/spinner.directive';
import { SalesReceiptCloneService } from '../../sales-receipt/sales-receipt-clone.service';
import { SalesReceiptMoreFilterService } from '../more-filter/sales-receipt-more-filter.service';
import { SalesReceiptRestService } from '../sales-receipt-rest.service';

import { MSalesReceipt } from '../sales-receipt.model';

@Component({
  selector: 'app-sales-receipt-list',
  templateUrl: 'sales-receipt-list.component.html',
  providers: [SystemMessageService]
})
export class SalesReceiptListComponent implements OnDestroy, OnInit {
  @ViewChild('gridTable') public gridTable: DataTable;
  @ViewChild('popMoreFilter') elPopMoreFilter: PopoverDirective;
  @ViewChild('pageLoading') elPageLoading: SpinnerDirective;
  @ViewChild('reportModal') elReportModal: ModalDirective;
  @ViewChild('retryDialog') elRetryDialog: RetryDialogComponent;

  compReady: boolean;
  doc: MSalesReceipt;
  gridDataSource: GridTableDataSource<MSalesReceipt> = new GridTableDataSource<MSalesReceipt>();
  selectedRecords: MSalesReceipt[] = [];

  combinedFilterValues: string = '';
  combinedFilterFields = {
    transactionNumber: true,
    'customer.displayName': true,
    description: true,
    'paymentMethod.name': true,
  };

  qParamsFilters: any = {
    keyword: null
  };
  moreFilterValues: IGridTableFilterParsed;

  tableColumns: IGridTableColumn[] = [{
    i18nLabel: 'ui.salesReceipt.list.column.transactionDate',
    field: 'transactionDate',
    dateFormat: 'DD/MM/YYYY',
    sort: true,
  }, {
    i18nLabel: 'ui.salesReceipt.list.column.transactionNumber',
    field: 'transactionNumber',
    link: (row: MSalesReceipt) => {
      return `/sales/receipts/${row.id}`;
    },
    sort: true,
  }, {
    i18nLabel: 'ui.salesReceipt.list.column.description',
    field: 'description',
    sort: true,
  }, {
    i18nLabel: 'ui.salesReceipt.list.column.customerName',
    field: 'customer.displayName',
    sort: true,
    formatter: (value, row: MSalesReceipt) => {
      return row.customCustomerName ? value + ' (' + row.customCustomerName + ')' : value;
    },
  }, {
    i18nLabel: 'ui.salesReceipt.list.column.paymentMethod',
    field: 'paymentMethod.name',
    sortField: 'paymentDetail.paymentMethod.name',
    sort: true,
  }, {
    i18nLabel: 'ui.salesReceipt.list.column.total',
    field: 'total',
    sort: true,
    formatter: (value, row: MSalesReceipt) => {
      return this._accounting.ac.formatMoney(value, '');
    },
    columnClasses: 'right-align',
  }, {
    i18nLabel: 'ui.salesReceipt.list.column.status',
    field: 'status',
    sort: true,
    formatter: (value, row: MSalesReceipt) => {
      return _.startCase(value);
    },
  }];
  tableColumnsShow: boolean = false;
  tableColumnsToggle = this._gridTableToggle.mapToggleOptionsFromColumns(this.tableColumns);

  retryDialogShownState: boolean = false;
  retryDialogOnRetry: Function;

  private loadDataTimeoutId: number;
  private gridFirstLoad: boolean = true;
  private subscriptions: { [key: string]: Subscription[] } = {
    load: []
  };

  constructor(
    public _accounting: AccountingService,
    private _activatedRoute: ActivatedRoute,
    private _adminLayout: AdminLayoutService,
    private _dataTableExport: ExportDataTableService,
    private _gridTable: GridTableService,
    private _gridTableToggle: GridTableToggleService,
    private _salesReceiptClone: SalesReceiptCloneService,
    private _salesReceiptMoreFilter: SalesReceiptMoreFilterService,
    private _salesReceiptRest: SalesReceiptRestService,
    public _systemMessage: SystemMessageService,
    private _translate: TranslateService,
  ) {
    this._adminLayout.containerType = 1;
  }

  ngOnInit() {
    this.loadData();
  }

  ngOnDestroy() {
    this._salesReceiptMoreFilter.lastMoreFilterValues = {};
  }

  onMoreFilter(filters: IGridTableFilterParsed) {
    this.elPopMoreFilter.hide();

    this.moreFilterValues = filters;
    this.loadData();
  }

  populateFilters() {
    let newFilters: any = _.pick(this.qParamsFilters, ['keyword']);

    if (this.moreFilterValues) {
      newFilters = Object.assign({}, newFilters, this.moreFilterValues.qParams);
    }

    this.qParamsFilters = newFilters;
  }

  gridLoadDataWrapper() {
    // Grid always load at the first time. we dont want that. we already call on ngOnInit
    if (this.gridFirstLoad) {
      this.gridFirstLoad = false;
      return;
    }

    if (this.loadDataTimeoutId) {
      window.clearTimeout(this.loadDataTimeoutId);
      this.loadDataTimeoutId = null;
    }

    this.loadDataTimeoutId = window.setTimeout(() => {
      this.loadData();
    }, 300);
  }

  showVoidDialog(receiptId: string) {
    swal({
      title: this._translate.instant('confirm.salesReceipt.void.label'),
      text: this._translate.instant('confirm.salesReceipt.void.description'),
      type: 'question',
      showCancelButton: true,
    })
      .then(() => {
        this.voidReceipt(receiptId);
      })
      .catch(() => { });
  }

  voidReceipt(receiptId: string) {
    this._salesReceiptRest.void(receiptId)
      .catch((error) => {
        this._systemMessage.log({
          message: error.response.data[0].errorMessage,
          type: 'error'
        });
        return Observable.throw(error);
      })
      .subscribe(response => {
      this.gridLoadDataWrapper();
      SnackBar.show({
        text: this._translate.instant('success.salesReceipt.void'),
        pos: 'bottom-right'
      });
    });
  }

  printReport(receiptId: string) {
    this._salesReceiptRest.load(receiptId).subscribe(response => {
      this.doc = response;
      this._salesReceiptRest.loadRelatedData().subscribe(related => {
        this.doc['companyInfo'] = related.settings.companyInfo;
        this.elReportModal.show();
      });
    });
  }

  get exportRecords(): Observable<MSalesReceipt[]> {
    const qOption = {
      ...this._gridTable.generateApiQueryOptionFromGridInfo(
        this.gridDataSource.pager,
        false,
        this.gridTable,
        this._gridTable.generateCombinedApiFilters(this.combinedFilterValues, this.combinedFilterFields)
      ),
      skip: 0,
      take: this._dataTableExport.exportLimit,
    };

    this.populateFilters();

    return this._salesReceiptRest.findAll(qOption, this.qParamsFilters).map(response => response.data);
  }

  exportPDF() {
    this.exportRecords.subscribe(records => {
      this._dataTableExport.export({
        records,
        mapOptions: this._dataTableExport.mapFromTableColumns(this.tableColumns),
        fileName: 'sales-receipt',
        extension: 'pdf',
        templateData: {
          title: this._translate.instant('ui.transaction.list.title'),
        },
      });
    });
  }

  exportExcel() {
    this.exportRecords.subscribe(records => {
      this._dataTableExport.export({
        records: records,
        mapOptions: this._dataTableExport.mapFromTableColumns(this.tableColumns),
        fileName: 'sales-receipt',
        extension: 'xls',
      });
    });
  }

  loadData() {
    this.elPageLoading.forceShow();

    const qOption = this._gridTable.generateApiQueryOptionFromGridInfo(
      this.gridDataSource.pager,
      true,
      this.gridTable,
      this._gridTable.generateCombinedApiFilters(this.combinedFilterValues, this.combinedFilterFields)
    );

    this.populateFilters();

    this.subscriptions.load.forEach(sub => sub.unsubscribe());
    this.subscriptions.load.push(
      this.elRetryDialog.createRetryEntry(
        this._salesReceiptRest.findAll(qOption, this.qParamsFilters)
          .finally(() => {
            this.elPageLoading.forceHide();
          })
      ).subscribe(response => {
        this.gridDataSource.updateFromApiPaginationResult(response);
        this.compReady = true;
      })
    );
  }

  cloneTransaction(doc) {
    this.elPageLoading.forceShow();

    this._salesReceiptRest.load(doc.id).subscribe(newDoc => {
      this.elPageLoading.forceHide();

      this._salesReceiptClone.copy(newDoc);
    });
  }

  public onGridChange() {
    this.gridLoadDataWrapper();
  }

  public onGridPageChange() {
    this.gridLoadDataWrapper();
  }
}

import { Injectable } from '@angular/core';
import * as _ from 'lodash';

import { MSalesChannelMarketplaceProductDynamicField } from './sales-channel-marketplace.model';
import { MSalesChannelWebstoreProductDynamicField } from './sales-channel-webstore.model';

@Injectable()
export class SalesChannelService {
  normalizeDynamicFields(dynamicFields: MSalesChannelWebstoreProductDynamicField[] | MSalesChannelMarketplaceProductDynamicField[]) {
    _.forEach(dynamicFields, dynamicField => {
      this.normalizeDynamicFieldsValidations(dynamicField.validations);
    });
  }

  normalizeDynamicFieldsValidations(dynamicFieldsValidations) {
    _.forEach(dynamicFieldsValidations, dynamicFieldsValidation => {
      switch (dynamicFieldsValidation.value) {
        case 'true':
          dynamicFieldsValidation.value = true;
          break;
        case 'false':
          dynamicFieldsValidation.value = false;
          break;
      }
    });
  }
}

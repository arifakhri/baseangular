import 'app/rxjs-imports.ts';

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { APP_CONST } from '../../app.const';
import { RequestService } from '../../core/core.module';
import { MHttpExtsrvResponse } from '../../core/http/http-extsrv.model';
import { MSalesChannelProductSyncResponse } from '../product/form-channel/product-form-channel.model';
import {
  MSalesChannelMarketplaceConfig,
  MSalesChannelMarketplaceProduct,
  MSalesChannelMarketplaceProductCategory,
  MSalesChannelMarketplaceProductDynamicField,
} from './sales-channel-marketplace.model';
import { SalesChannelService } from './sales-channel.service';

@Injectable()
export class SalesChannelMarketplaceRestService {
  request = this._request.new(`${APP_CONST.API_MAIN}/integrations/marketplaces/accounts`);

  constructor(
    private _request: RequestService,
    private _salesChannel: SalesChannelService,
  ) { }

  getConfigA(channelAccountId: string) {
    return this.request.get<MSalesChannelMarketplaceConfig>(`config`, {
      params: {
        channelAccountId,
      }
    });
  }

  getConfig(channelAccountId: string) {
    const request = this._request.new(`${APP_CONST.API_SALESCHANNEL}/marketplace/lazada/config`);
    return request.post<MSalesChannelMarketplaceConfig>(``);
  }

  findAllPartial(channelAccountId: string, pageSize: number, offset: number, limit: number) {
    return this.request.get<MSalesChannelMarketplaceProduct[]>(`${channelAccountId}/products`, {
      params: {
        pageSize,
        offset,
        limit,
      },
    });
  }

  findAllPartialA(channelAccountId: string, perPage: number, offset: number, limit: number) {
    const request = this._request.new(`${APP_CONST.API_SALESCHANNEL}/marketplace/lazada/products/findAll`);
    return request.post<{ data: MSalesChannelMarketplaceProduct[] }>(``, null, {
      params: {
        PerPage: perPage,
        Offset: offset,
        Limit: limit,
      },
      headers: {
        'x-seller_user_id': 'reynaldio@gmail.com',
        'x-api_key': 'xUnPSEYToInn2431kKZvUz5IAF-O8E6PMxIzvkugigaIP1rgiVC4Rk8f',
      },
    }).switchMap(response => Observable.of(response.data));
  }

  findAllCategories(channelAccountId: string) {
    return this.request.get<MHttpExtsrvResponse<MSalesChannelMarketplaceProductCategory[]>>(`${channelAccountId}/product-categories`);
  }

  findAllCategoriesA(channelAccountId: string) {
    const request = this._request.new(`${APP_CONST.API_SALESCHANNEL}/marketplace/lazada/product-categories/findAll`);
    return request.post<{ data: MSalesChannelMarketplaceProductCategory[] }>(``, {}, {
      headers: {
        'x-seller_user_id': 'reynaldio@gmail.com',
        'x-api_key': 'xUnPSEYToInn2431kKZvUz5IAF-O8E6PMxIzvkugigaIP1rgiVC4Rk8f',
      },
    });
  }

  findAllDynamicFields(channelAccountId: string, categoryId) {
    return this.request.post<MHttpExtsrvResponse<{
      allow_variants: boolean;
      records: MSalesChannelMarketplaceProductDynamicField[];
    }>>(`${channelAccountId}/dynamic-fields`, {
      channel_category_id: categoryId,
    }).switchMap(response => {
      this._salesChannel.normalizeDynamicFields(response.data.records);
      return Observable.of(response);
    });
  }

  findAllDynamicFieldsA(channelAccountId: string, categoryId) {
    const request = this._request.new(
      `${APP_CONST.API_SALESCHANNEL}/marketplace/lazada/dynamic-fields/findAll?ParamID=${categoryId}`
    );
    return request.post<MHttpExtsrvResponse<{
      allow_variants: boolean;
      records: MSalesChannelMarketplaceProductDynamicField[];
    }>>(``, {}, {
      headers: {
        'x-seller_user_id': 'reynaldio@gmail.com',
        'x-api_key': 'xUnPSEYToInn2431kKZvUz5IAF-O8E6PMxIzvkugigaIP1rgiVC4Rk8f',
      },
    }).switchMap(response => {
      this._salesChannel.normalizeDynamicFields(response.data.records);
      return Observable.of(response);
    });
  }

  loadProduct(channelAccountId: string, clodeoProductId: string) {
    return this.request.post<MHttpExtsrvResponse<MSalesChannelMarketplaceProduct>>(`${channelAccountId}/mapped-product`, {
      clodeo_product_id: clodeoProductId,
    });
  }

  syncProduct(channelAccountId: string, clodeoProductId: string) {
    return this.request.post<MSalesChannelProductSyncResponse>(`${channelAccountId}/product-sync`, {
      clodeo_product_id: clodeoProductId,
    });
  }
}

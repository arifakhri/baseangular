import { NgModule } from '@angular/core';

import { SalesChannelMarketplaceRestService } from './sales-channel-marketplace-rest.service';
import { SalesChannelMarketplaceService } from './sales-channel-marketplace.service';
import { SalesChannelRestService } from './sales-channel-rest.service';
import { SalesChannelWebstoreRestService } from './sales-channel-webstore-rest.service';
import { SalesChannelWebstoreService } from './sales-channel-webstore.service';
import { SalesChannelService } from './sales-channel.service';

export const PROVIDERS = [
  SalesChannelMarketplaceRestService,
  SalesChannelMarketplaceService,
  SalesChannelRestService,
  SalesChannelService,
  SalesChannelWebstoreRestService,
  SalesChannelWebstoreService,
];

@NgModule()
export class SalesChannelModule { }

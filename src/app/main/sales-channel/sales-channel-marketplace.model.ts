export class MSalesChannelMarketplaceProduct {
  clodeo_product_id: string;

  channel_product_id: string;

  id: string | number;
  sku: string;
  name: string;
  price: number;
  description: string;
  category: MSalesChannelMarketplaceProductCategory;
  images: MSalesChannelMarketplaceProductImage[];
  attribute1: string;
  attribute2: string;
  attribute3: string;
  variants: MSalesChannelMarketplaceProductVariant[];
  dynamic_fields?: MSalesChannelMarketplaceProductDynamicFieldValue[];

  map_field?: {
    map_type: string;
    map_to: string;
  };
}

export class MSalesChannelMarketplaceProductImage {
  id: string | number;
  alt: string;
  src: string;
  isMain: boolean;
}

export class MSalesChannelMarketplaceProductCategory {
  id: string | number;
  name: string;
}

export class MSalesChannelMarketplaceProductVariant {
  clodeo_product_variant_id: string;

  channel_product_id: string;
  channel_product_variant_id: string;

  id: string | number;
  name: string;
  sku: string;
  price: string | number;
  qty: string | number;
  images: MSalesChannelMarketplaceProductImage[];
  attribute1_value: string;
  attribute2_value: string;
  attribute3_value: string;
  dynamic_fields?: MSalesChannelMarketplaceProductDynamicFieldValue[];
}

export class MSalesChannelMarketplaceProductDynamicField {
  name: string;
  standard_name: string;
  label: string;
  input_type: string;
  field_target: string;
  validations: MSalesChannelMarketplaceProductDynamicFieldValidation[];
  options: MSalesChannelMarketplaceProductDynamicFieldOption[];

  provider: {
    autocomplete_url: string;
    autocomplete_param_key: string;
    autocomplete_label_key: string;
    autocomplete_value_key: string;
  };
}

export class MSalesChannelMarketplaceProductDynamicFieldValidation {
  key: string;
  value: string;
}

export class MSalesChannelMarketplaceProductDynamicFieldOption {
  label: string;
  value: any;
}

export class MSalesChannelMarketplaceProductDynamicFieldValue {
  key: string;
  value: string;
  field_type: string;
}

export class MSalesChannelMarketplaceConfig {
  pagination: {
    max_limit: number;
    max_per_page: number;
  };
}

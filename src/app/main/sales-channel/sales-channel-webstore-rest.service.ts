import 'app/rxjs-imports.ts';

import { Injectable } from '@angular/core';
import * as _ from 'lodash';
import { Observable } from 'rxjs';

import { APP_CONST } from '../../app.const';
import { RequestService } from '../../core/core.module';
import { MHttpExtsrvResponse } from '../../core/http/http-extsrv.model';
import { HttpExtsrvService } from '../../core/http/http-extsrv.service';
import { MSalesChannelProductSyncResponse } from '../product/form-channel/product-form-channel.model';
import { MProduct } from '../product/product.model';
import {
  MSalesChannelWebstoreConfig,
  MSalesChannelWebstoreProduct,
  MSalesChannelWebstoreProductCategory,
} from './sales-channel-webstore.model';

@Injectable()
export class SalesChannelWebstoreRestService {
  request = this._request.new(`${APP_CONST.API_MAIN}/integrations/webstores/accounts`);

  constructor(
    private _httpExtsrv: HttpExtsrvService,
    private _request: RequestService,
  ) {
    this._httpExtsrv.axiosInterceptors(this.request.axios);
  }

  getConfigA(channelAccountId: string) {
    return this.request.get<MSalesChannelWebstoreConfig>(`config`, {
      params: {
        channelAccountId,
      }
    });
  }

  getConfig(channelAccountId: string) {
    const request = this._request.new(`${APP_CONST.API_SALESCHANNEL}/webstore/shopify/config`);
    return request.post<MSalesChannelWebstoreConfig>(``);
  }

  findAllPartial(channelAccountId: string, pageSize: number, offset: number, limit: number) {
    return this.request.get<MSalesChannelWebstoreProduct[]>(`${channelAccountId}/products`, {
      params: {
        pageSize,
        offset,
        limit,
      }
    });
  }

  findAllPartialA(channelAccountId: string, perPage: number, offset: number, limit: number) {
    const request = this._request.new(`${APP_CONST.API_SALESCHANNEL}/webstore/shopify/products/findAll`);
    return request.post<{ data: MSalesChannelWebstoreProduct[] }>(``, {
      offset,
      limit,
    }, {
      headers: {
        'x-store_key': 'hanako-baby-shop',
        'x-api_key': 'a71b70f11093772f084b07285896566d',
        'x-password': '40ae1d13179fead47a47]cf3e75c2110d',
      },
    }).switchMap(response => Observable.of(response.data));
  }

  findAllCategories(channelAccountId: string) {
    return this.request.get<MHttpExtsrvResponse<MSalesChannelWebstoreProductCategory[]>>(`${channelAccountId}/product-categories`);
  }

  findAllCategoriesA(channelAccountId: string) {
    const request = this._request.new(`${APP_CONST.API_SALESCHANNEL}/webstore/shopify/product-categories/findAll`);
    return request.post<MHttpExtsrvResponse<MSalesChannelWebstoreProductCategory[]>>(``, {}, {
      headers: {
        'x-store_key': 'clodeodev',
        'x-api_key': '86d42b670bd5822538b165b8d8846e8b',
        'x-password': 'a9e28fc74fdef411d823bcbcc2207c43',
      },
    });
  }

  loadProduct(channelAccountId: string, clodeoProductId: string) {
    return this.request.post<MHttpExtsrvResponse<MSalesChannelWebstoreProduct>>(`${channelAccountId}/mapped-product`, {
      clodeo_product_id: clodeoProductId,
    });
  }

  syncProduct(channelAccountId: string, clodeoProductId: string) {
    return this.request.post<MHttpExtsrvResponse<MSalesChannelProductSyncResponse>>(`${channelAccountId}/product-sync`, {
      clodeo_product_id: clodeoProductId,
    });
  }
}

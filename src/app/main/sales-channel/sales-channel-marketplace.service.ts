import { Injectable } from '@angular/core';

import { ProductFormChannelService } from '../product/form-channel/product-form-channel.service';
import { MProduct } from '../product/product.model';

@Injectable()
export class SalesChannelMarketplaceService {
  constructor(
    private _productFormChannel: ProductFormChannelService,
  ) { }

  connectToClodeoProduct(
    channelAccountId: string,
    channelProduct,
    clodeoProduct: MProduct,
  ) {
    this._productFormChannel.attachToProduct(clodeoProduct, {
      channelAccountId,
      channelProduct,
    });
  }
}

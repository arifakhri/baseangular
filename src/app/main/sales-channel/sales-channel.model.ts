import { MSalesChannelMarketplaceProductDynamicFieldValue } from './sales-channel-marketplace.model';
import { MSalesChannelWebstoreProductDynamicFieldValue } from './sales-channel-webstore.model';

export class MSalesChannelAccount {
  channelAccountId: string;

  channelAccountType?: string;
  channelId?: string;
  channelName?: string;
  name?: string;
  channelDescription?: string;
  description?: string;
  priceLevelId: string;
  warehouseId: string;

  channelProductId?: string;
  channelProductSKU?: string;
  channelProductVariantId?: string;
  channelProductVariantSKU?: string;
}

export class MSalesChannelAccountProduct extends MSalesChannelAccount {
  name: string;
  description: string;
  category: {
    id: string;
    name: string;
  };
  dynamicFields: MSalesChannelMarketplaceProductDynamicFieldValue[] | MSalesChannelWebstoreProductDynamicFieldValue[];
  processType: string;
  state?: string;
  allowVariants: boolean;
}

export class MSalesChannelAccountProductVariant extends MSalesChannelAccount {
  sku: string;
  name: string;
  price: number;
  dynamicFields: MSalesChannelMarketplaceProductDynamicFieldValue[] | MSalesChannelWebstoreProductDynamicFieldValue[];
  processType: string;
  state?: string;
}

export class MSalesChannelResponse {
  success: boolean;
  errors: MSalesChannelResponseError[];
  data: any;
}

export class MSalesChannelResponseError {
  error_message: string;
  error_code: string;
}

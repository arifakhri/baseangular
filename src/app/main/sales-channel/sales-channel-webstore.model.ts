export class MSalesChannelWebstoreProduct {
  clodeo_product_id: string;

  channel_product_id: string;

  id: string | number;
  sku: string;
  name: string;
  price: number;
  description: string;
  category: MSalesChannelWebstoreProductCategory;
  images: MSalesChannelWebstoreProductImage[];
  attribute1: string;
  attribute2: string;
  attribute3: string;
  variants: MSalesChannelWebstoreProductVariant[];
  dynamic_fields?: MSalesChannelWebstoreProductDynamicFieldValue[];
}

export class MSalesChannelWebstoreProductImage {
  id: string | number;
  alt: string;
  src: string;
  isMain: boolean;
}

export class MSalesChannelWebstoreProductCategory {
  id: string | number;
  name: string;
}

export class MSalesChannelWebstoreProductVariant {
  clodeo_product_variant_id: string;

  channel_product_id: string;
  channel_product_variant_id: string;

  id: string | number;
  name: string;
  sku: string;
  price: string | number;
  qty: string | number;
  images: MSalesChannelWebstoreProductImage[];
  attribute1_value: string;
  attribute2_value: string;
  attribute3_value: string;
  dynamic_fields?: MSalesChannelWebstoreProductDynamicFieldValue[];

  map_field?: {
    map_type: string;
    map_to: string;
  };
}

export class MSalesChannelWebstoreProductDynamicFieldValue {
  key: string;
  value: any;
  field_type: string;
}

export class MSalesChannelWebstoreProductDynamicField {
  name: string;
  standard_name: string;
  label: string;
  input_type: string;
  field_target: string;
  validations: MSalesChannelWebstoreProductDynamicFieldValidation[];
  options: MSalesChannelWebstoreProductDynamicFieldOption[];

  provider: {
    autocomplete_url: string;
    autocomplete_param_key: string;
    autocomplete_label_key: string;
    autocomplete_value_key: string;
  };
}

export class MSalesChannelWebstoreProductDynamicFieldValidation {
  key: string;
  value: string;
}

export class MSalesChannelWebstoreProductDynamicFieldOption {
  label: string;
  value: any;
}

export class MSalesChannelWebstoreConfig {
  pagination: {
    max_limit: number;
    max_per_page: number;
  };
}

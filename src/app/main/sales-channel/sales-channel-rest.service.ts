import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import 'app/rxjs-imports.ts';

import { APP_CONST } from '../../app.const';
import { RequestService } from '../../core/core.module';
import { MSalesChannelAccount } from './sales-channel.model';
import { MHttpExtsrvResponse } from '../../core/http/http-extsrv.model';
import { MSalesChannelMarketplaceProduct } from './sales-channel-marketplace.model';
import { MSalesChannelWebstoreProduct } from './sales-channel-webstore.model';

@Injectable()
export class SalesChannelRestService {
  request = this._request.new(`${APP_CONST.API_MAIN}/integrations/sales-channels`);

  constructor(
    private _request: RequestService,
  ) { }

  findAll<T>(
    channelAccountId: string,
    findAllPartialFunc: (...params) => Observable<MHttpExtsrvResponse<T[]>>,
    limit: number = 300,
    perPage: number = null,
    offset: number = 0,
    results: T[] = [],
    subjectObservable = new Subject<T[]>()
  ): Observable<T[]> {
    const obsExecute = findAllPartialFunc(channelAccountId, perPage, offset, limit)
      .expand(response => {
        if (!response.data || response.data.length < limit) {
          return Observable.empty();
        }
        offset = offset + limit;
        return findAllPartialFunc(channelAccountId, perPage, offset, limit);
      }).finally(() => {
        subjectObservable.next(results);
        subjectObservable.complete();
      }).subscribe(response => {
        results.push(...response.data);
      }, error => {
        subjectObservable.error(error);
      });

    subjectObservable.unsubscribe = () => {
      obsExecute.unsubscribe();
    };

    return subjectObservable.asObservable();
  }

  findAllAccounts() {
    return this.request.get<MSalesChannelAccount[]>('accounts');
  }

  loadAccount(channelAccountId: string) {
    return this.request.get<MSalesChannelAccount>(`accounts/${channelAccountId}`);
  }
}

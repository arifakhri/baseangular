import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { StockOpnameCreateComponent } from './create/stock-opname-create.component';
import { StockOpnameDetailComponent } from './detail/stock-opname-detail.component';
import { StockOpnameListComponent } from './list/stock-opname-list.component';
import { StockOpnameUpdateComponent } from './update/stock-opname-update.component';

import { AuthorizationService } from '../../core/core.module';

export const routes: Routes = [{
  path: '',
  children: [{
    path: '',
    component: StockOpnameListComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'stockOpname.list',
    }
  }, {
    path: 'create',
    component: StockOpnameCreateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'stockOpname.create',
    }
  }, {
    path: ':id',
    component: StockOpnameDetailComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'stockOpname.detail'
    }
  }, {
    path: ':id/update',
    component: StockOpnameUpdateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'stockOpname.update'
    }
  }]
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class StockOpnameRoutingModule { }

import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { AccountingService } from '../../../core/core.module';
import { StockOpnameRestService } from '../stock-opname-rest.service';

@Component({
  selector: 'app-stock-opname-detail',
  templateUrl: 'stock-opname-detail.component.html'
})
export class StockOpnameDetailComponent implements OnInit {
  doc: IStockOpname;
  routeParams: any;

  constructor(
    public _accounting: AccountingService,
    private _stockOpnameRest: StockOpnameRestService,
    private _route: ActivatedRoute,
  ) {
    this._route.params.subscribe(params => this.routeParams = params);
  }

  ngOnInit() {
    this.loadData();
  }

  loadData() {
    this._stockOpnameRest.load(this.routeParams.id).subscribe(stockOpname => {
      this.doc = stockOpname;
    });
  }
}

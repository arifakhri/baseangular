import * as _ from 'lodash';
import * as moment from 'moment';
import { FormGroup, Validators } from '@angular/forms';
import { Injectable } from '@angular/core';

@Injectable()
export class StockOpnameService {
  normalizeDoc(record: any) {
    if (record.transactionDate) {
      record.transactionDate = moment(record.transactionDate).format('YYYY-MM-DD');
    }

    record.lines = _.reject(record.lines, line => {
      return !(<any>line).productId || !(<any>line).productVariantId || !(<any>line).changeQty;
    });
  }

  lineConditionalValidation(formGroup: FormGroup) {
    const formGroupValue = formGroup.value;
    if (
      formGroup.dirty &&
      _.size(<any>_.pickBy(_.omit(formGroupValue, ['originalQty', 'unitCost']), _.identity))
    ) {
      formGroup.get('productVariant').setValidators([Validators.required]);
      formGroup.get('productVariant').updateValueAndValidity();
      formGroup.get('productVariantId').setValidators([Validators.required]);
      formGroup.get('productVariantId').updateValueAndValidity();
    } else {
      formGroup.get('productVariant').clearValidators();
      formGroup.get('productVariant').updateValueAndValidity();
      formGroup.get('productVariantId').clearValidators();
      formGroup.get('productVariantId').updateValueAndValidity();
    }
  }
}

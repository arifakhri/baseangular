import { AutoCompleteModule } from 'primeng/components/autocomplete/autocomplete';
import { BsDropdownModule, ModalModule, PaginationModule } from 'ngx-bootstrap';
import { CalendarModule } from 'primeng/components/calendar/calendar';
import { CommonModule } from '@angular/common';
import { DataTableModule } from 'primeng/components/datatable/datatable';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { ListboxModule } from 'primeng/components/listbox/listbox';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { StockOpnameCreateComponent } from './create/stock-opname-create.component';
import { StockOpnameDetailComponent } from './detail/stock-opname-detail.component';
import { StockOpnameFormComponent } from './form/stock-opname-form.component';
import { StockOpnameImportComponent } from './import/stock-opname-import.component';
import { StockOpnameListComponent } from './list/stock-opname-list.component';
import { StockOpnameUpdateComponent } from './update/stock-opname-update.component';

import { StockOpnameFormService } from './form/stock-opname-form.service';
import { StockOpnameImportService } from './import/stock-opname-import.service';
import { StockOpnameRestService } from './stock-opname-rest.service';
import { StockOpnameService } from './stock-opname.service';

import { CoreModule } from '../../core/core.module';
import { ProductVariantModule } from '../product-variant/product-variant.module';
import { SharedModule } from '../../shared/shared.module';
import { TransactionModule } from '../transaction/transaction.module';

export const PROVIDERS = [
  StockOpnameFormService,
  StockOpnameImportService,
  StockOpnameRestService,
  StockOpnameService,
];

@NgModule({
  imports: [
    AutoCompleteModule,
    BsDropdownModule,
    CalendarModule,
    CommonModule,
    CoreModule,
    DataTableModule,
    FlexLayoutModule,
    FormsModule,
    HttpModule,
    ListboxModule,
    ModalModule,
    PaginationModule,
    ProductVariantModule,
    ReactiveFormsModule,
    RouterModule,
    SharedModule,
    TransactionModule,
    TranslateModule,
  ],
  declarations: [
    StockOpnameCreateComponent,
    StockOpnameDetailComponent,
    StockOpnameFormComponent,
    StockOpnameImportComponent,
    StockOpnameListComponent,
    StockOpnameUpdateComponent,
  ],
})
export class StockOpnameModule { }

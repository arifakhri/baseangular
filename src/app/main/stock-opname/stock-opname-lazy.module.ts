import { NgModule } from '@angular/core';
import { StockOpnameModule } from './stock-opname.module';
import { StockOpnameRoutingModule } from './stock-opname-routing.module';

@NgModule({
  imports: [
    StockOpnameModule,
    StockOpnameRoutingModule
  ]
})
export class StockOpnameLazyModule { }

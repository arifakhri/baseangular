import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { ProductVariantRestService } from '../../product-variant/product-variant.module';
import { StockOpnameFormService } from '../form/stock-opname-form.service';

@Injectable()
export class StockOpnameImportService {
  importMap: IImportDataMap = {
    sku: {
      identifier: 0,
    },
    newQty: {
      identifier: 1,
    },
    unitCost: {
      identifier: 2,
    },
  };

  importProcessedRecords: any;

  constructor(
    private _productVariantRest: ProductVariantRestService,
    private _router: Router,
    private _stockOpnameForm: StockOpnameFormService,
  ) { }

  async resolveRecords(records: any) {
    for (const record of records) {
      try {
        const productsFound = await this._productVariantRest.findAll({
          filter: [{
            filterValues: [{
              field: 'sku',
              operator: 'contains',
              value: record.sku
            }]
          }],
          sort: [{
            field: 'name',
            dir: 'asc'
          }],
          take: 1,
          skip: 0,
          includeTotalCount: false
        }).toPromise();
        if (productsFound && productsFound.data && productsFound.data.length) {
          record.productVariant = productsFound.data[0];
        }
      } catch (e) {}
    }
  }

  import(records: any) {
    this.importProcessedRecords = records;

    this._router.navigateByUrl('/inventory-adjustments/create');
  }
}

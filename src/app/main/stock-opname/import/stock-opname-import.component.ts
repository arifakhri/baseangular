import { BehaviorSubject } from 'rxjs';
import 'app/rxjs-imports.ts';
import { Component, ElementRef } from '@angular/core';

import { EImportDataTypes, ImportDataService } from '../../../core/core.module';
import { StockOpnameImportService } from './stock-opname-import.service';

@Component({
  selector: 'app-stock-opname-import',
  templateUrl: './stock-opname-import.component.html',
  styleUrls: ['./stock-opname-import.component.scss'],
})
export class StockOpnameImportComponent {
  importProcessed$: BehaviorSubject<boolean> = new BehaviorSubject(null);

  constructor(
    public _elementRef: ElementRef,
    private _importData: ImportDataService,
    private _stockOpnameImport: StockOpnameImportService,
  ) { }

  async doImport() {
    this.importProcessed$.next(false);

    try {
      const importData = await this._importData.prepare({
        mapOptions: this._stockOpnameImport.importMap,
        from: EImportDataTypes.EXCEL,
        fileInputSelector: '#import-inventory-adjustment',
      });

      const records = await importData.process();

      await this._stockOpnameImport.resolveRecords(records);

      this.importProcessed$.next(true);

      this._stockOpnameImport.import(records);
    } catch (e) {
      this.importProcessed$.next(null);
    }
  }
}

export class MStockOpname {
  id: string;
  branchId: number;
  adjustmentAccount: IAccount;
  adjustmentAccountId: string;
  description: string;
  transactionType: string;
  transactionNumber: string;
  transactionDate: string | Date;
  warehouseId: string;
  note: string;
  status: string;
  state: string;
  rowVersion: string;
  branch: {
    id: number;
    name: string;
  };
  warehouse: {
    id: string;
    name: string;
    isMaster: boolean;
  };
  lines: [
    {
      id: string;
      inventoryAdjustmentId: string;
      productId: string;
      productVariantId: string;
      originalQty: number;
      changeQty: number;
      actualQty?: number;
      unitCost: number;
      changeTotalCost: number;
      sortOrder: number;
      rowVersion: string;
      product: {
        id: string;
        productType: string;
        code: string;
        name: string;
        categoryId: string;
        sellable: boolean;
        salesDescription: string;
        salesAccountId: string;
        salesTaxId: string;
        purchasable: boolean;
        purchaseDescription: string;
        purchaseAccountId: string;
        purchaseTaxId: string;
        inventoryAccountId: string;
        uomId: string;
        variantCount: number;
        inactive: boolean;
        category: {
          id: string;
          name: string;
        };
        salesTax: {
          id: string;
          code: string;
          name: string;
          rate: number;
        };
        purchaseTax: {
          id: string;
          code: string;
          name: string;
          rate: number;
        };
        uom: {
          id: string;
          name: string;
          systemDefault: boolean;
        };
        masterVariant: {
          id: string;
          barcode: string;
          name: string;
          variantName: string;
          unitPrice: number;
          unitCost: number;
        };
      };
      productVariant: IProductVariant;
    }
  ]
}

export class MStockOpnameRelatedData {
  accounts: IAccount[];
  settings: ISettings;
}

import { Injectable } from '@angular/core';
import 'app/rxjs-imports.ts';

import { ProductVariantRestService } from '../product-variant/product-variant-rest.service';
import { ApiQueryOption, RequestService } from '../../core/core.module';

import { APP_CONST } from '../../app.const';

@Injectable()
export class StockOpnameRestService {
  baseURL = `${APP_CONST.API_MAIN}/inventory-adjustments`;
  request = this._request.new(this.baseURL);

  constructor(
    private _productVariantRest: ProductVariantRestService,
    private _request: RequestService,
  ) { }

  create(stockOpname: IStockOpname) {
    return this.request.post<IStockOpname>(``, stockOpname);
  }

  findAll(queryOption: ApiQueryOption) {
    return this.request.post<IApiPaginationResult<IStockOpname>>(`q`, queryOption);
  }

  load(stockOpnameId: string) {
    return this.request.get<IStockOpname>(`${stockOpnameId}`);
  }

  update(stockOpnameId: string, updateObj: IStockOpname) {
    return this.request.put<IStockOpname>(`${stockOpnameId}`, updateObj);
  }

  loadRelatedData() {
    return this.request.get<IStockOpnameRelatedData>(`entry-related-data`);
  }
}

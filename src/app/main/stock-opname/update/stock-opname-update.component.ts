import * as _ from 'lodash';
import * as moment from 'moment';
import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';

import { StockOpnameRestService } from '../stock-opname-rest.service';
import { StockOpnameService } from '../stock-opname.service';

import { BaseUpdateBComponent } from '../../../shared/base/base.module';
import { MStockOpname } from '../stock-opname.model';

@Component({
  selector: 'app-stock-opname-update',
  templateUrl: 'stock-opname-update.component.html',
})
export class StockOpnameUpdateComponent extends BaseUpdateBComponent {
  constructor(
    private _stockOpnameRest: StockOpnameRestService,
    private _stockOpnameService: StockOpnameService,
  ) {
    super();

    this.componentId = 'StockOpnameUpdate';
    this.headerTitle = 'ui.stockOpname.update.title';
    this.routeURL = '/inventory-adjustments';
    this.entrySuccessI18n = 'success.stockOpname.update';

    this.registerHook('load', event => {
      return this._stockOpnameRest.load(this.page.routeParams.id).switchMap(doc => {
        if (doc.transactionDate) {
          doc.transactionDate = moment(doc.transactionDate).toDate();
        }
        return Observable.of(doc);
      });
    });

    this.registerHook('save', event => {
      let stockOpname = new MStockOpname;

      stockOpname = _.assign(stockOpname, this.doc, this.form.value);
      this._stockOpnameService.normalizeDoc(stockOpname);

      return this._stockOpnameRest.update(this.page.routeParams.id, stockOpname);
    })
  }
}

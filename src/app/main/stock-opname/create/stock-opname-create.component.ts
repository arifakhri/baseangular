import * as _ from 'lodash';
import { Component } from '@angular/core';

import { StockOpnameRestService } from '../stock-opname-rest.service';
import { StockOpnameService } from '../stock-opname.service';

import { BaseCreateBComponent } from '../../../shared/base/base.module';
import { MStockOpname } from '../stock-opname.model';

@Component({
  selector: 'app-stock-opname-create',
  templateUrl: 'stock-opname-create.component.html',
})
export class StockOpnameCreateComponent extends BaseCreateBComponent {
  constructor(
    private _stockOpnameRest: StockOpnameRestService,
    private _stockOpnameService: StockOpnameService,
  ) {
    super();

    this.componentId = 'StockOpnameCreate';
    this.headerTitle = 'ui.stockOpname.create.title';
    this.routeURL = '/inventory-adjustments';
    this.entrySuccessI18n = 'success.stockOpname.create';

    this.registerHook('save', event => {
      let stockOpname = new MStockOpname;

      stockOpname = _.assign(stockOpname, this.form.value);
      this._stockOpnameService.normalizeDoc(stockOpname);

      return this._stockOpnameRest.create(stockOpname);
    })
  }
}

import * as _ from 'lodash';
import { Component, SimpleChanges } from '@angular/core';
import { FormArray, FormGroup } from '@angular/forms';
import 'app/rxjs-imports.ts';

import { AutocompleteService, CommonService } from '../../../core/core.module';
import { ProductVariantRestService } from '../../product-variant/product-variant.module';
import { SettingsApplicatorService } from '../../settings/settings-applicator.service';
import { SpinnerService } from '../../../shared/spinner/spinner.service';
import { StockOpnameFormService } from '../form/stock-opname-form.service';
import { StockOpnameImportService } from '../import/stock-opname-import.service';
import { StockOpnameRestService } from '../stock-opname-rest.service';
import { StockOpnameService } from '../stock-opname.service';

import { BaseFormBComponent } from '../../../shared/base/base.module';

@Component({
  selector: 'app-stock-opname-form',
  templateUrl: 'stock-opname-form.component.html'
})
export class StockOpnameFormComponent extends BaseFormBComponent {
  ACProductVariantHandler = CommonService.remoteACItemsHandler({
    remoteParams: this.ACProductVariantParams.bind(this),
    remoteRequest: this._productVariantRest.findAllPicker.bind(this._productVariantRest),
    remoteRequestMap: (response) => response.data,
  });

  constructor(
    public _autocomplete: AutocompleteService,
    private _productVariantRest: ProductVariantRestService,
    private _settingsApplicator: SettingsApplicatorService,
    private _spinner: SpinnerService,
    private _stockOpname: StockOpnameService,
    private _stockOpnameForm: StockOpnameFormService,
    private _stockOpnameImport: StockOpnameImportService,
    private _stockOpnameRest: StockOpnameRestService
  ) {
    super();

    this.registerHook('buildForm', event => {
      this._stockOpnameForm.setFormDefinitions(this.form);
    });

    this.registerHook('patchForm', event => {
      this.applyDoc();
    });

    this.registerHook('loadRelated', event => {
      return this._stockOpnameRest.loadRelatedData();
    }, relatedData => {
      if (this.formType === 'create') {
        this._settingsApplicator.applyToForm(relatedData.settings, this.form, {
          'accounts': {
            'inventoryAdjustmentAccountId': 'adjustmentAccountId',
          },
        }, {
            accounts: relatedData.accounts,
          }).subscribe();

          if (relatedData.warehouses.length === 1) {
            this.form.get('warehouse').patchValue(relatedData.warehouses[0]);
            this.form.get('warehouseId').patchValue(relatedData.warehouses[0].id);
          }
      }
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (_.has(changes, 'doc') && this.doc) {
      this.applyDoc();
    }
  }

  ngOnInit() {
    super.ngOnInit();

    this.addLines();

    if (this._stockOpnameImport.importProcessedRecords) {
      this.applyImport();
    }
  }

  addLines(length: number = 2) {
    const formGroups = [];
    const formArray = (<FormArray>this.form.get('lines'));
    _.range(length).forEach(() => {
      const newFormGroup = this._stockOpnameForm.buildFormChildLine();
      formGroups.push(newFormGroup);
      formArray.push(newFormGroup);
    });
    return formGroups;
  }

  applyDoc() {
    this.clearAllLines();

    this.form.patchValue(this.doc);

    _.forEach(this.doc.lines, line => {
      const formArrayLine = (<FormArray>this.form.get('lines'));
      const formGroupLines = this.addLines(1);
      const formGroupLine = formGroupLines[0];

      formGroupLine.patchValue({
        ...line,
        _newQty: line.actualQty,
        sku: line.productVariant.sku,
      });
    });
  }

  applyImport() {
    this.clearAllLines();

    _.forEach(this._stockOpnameImport.importProcessedRecords, record => {
      const formGroupLines = this.addLines(1);
      const formGroupLine = formGroupLines[0];
      const formArrayLine = (<FormArray>this.form.get('lines'));
      const formGroupIndex = formArrayLine.length - 1;
      const newQty = (<any>record).newQty;
      const unitCost = (<any>record).unitCost;

      this.onProductSelected(formGroupIndex, (<any>record).productVariant).subscribe(() => {

        formGroupLine.patchValue({ _newQty: newQty, unitCost });

        this.syncNewQty(formGroupIndex, newQty);
      });
    });

    this._stockOpnameImport.importProcessedRecords = undefined;
  }

  ACProductVariantParams(event: any, type: string) {
    const qParams: any = {};

    if (type === 'search') {
      qParams['keyword'] = event.query;
    }

    return [{
      filter: [],
      sort: [],
      take: 30,
      skip: 0,
      includeTotalCount: false
    }, qParams];
  }

  onProductClear(formIndex: number) {
    this.clearLine(formIndex);

    const lineFormGroup = (<FormArray>this.form.get('lines')).at(formIndex);
    lineFormGroup.get('changeQty').reset(0);
    lineFormGroup.get('_newQty').reset(0);
  }

  onProductSelected(formIndex: number, srcProductVariant: IProductVariant, targetSpinnerElement?: any) {
    const spinner = this._spinner.show({
      element: targetSpinnerElement,
      loaderType: 3,
    });

    return this._productVariantRest.load(srcProductVariant.id).do(productVariant => {
      const lineFormGroup = (<FormArray>this.form.get('lines')).at(formIndex);
      lineFormGroup.patchValue({
        product: productVariant.product,
        productId: productVariant.productId,
        productVariant,
        productVariantId: productVariant.id,
        sku: productVariant.sku,
        originalQty: productVariant.inventory.qty
      });

      lineFormGroup.get('_newQty').setValue(productVariant.inventory.qty);
      this.syncNewQty(formIndex, productVariant.inventory.qty);
    }).finally(() => {
      spinner.dispose();
    });
  }

  syncNewQty(formIndex: number, formValue: any) {
    const lineFormGroup = (<FormArray>this.form.get('lines')).at(formIndex);
    const currentQty = lineFormGroup.get('originalQty').value || 0;

    const changeQty = parseInt(formValue, 0) - currentQty || 0;

    const formGroup = (<FormArray>this.form.get('lines')).at(formIndex);
    formGroup.patchValue({ changeQty });
  }

  syncChangeQty(formIndex: number, formValue: any) {
    const lineFormGroup = (<FormArray>this.form.get('lines')).at(formIndex);
    const currentQty = lineFormGroup.get('originalQty').value || 0;

    const _newQty = currentQty + parseInt(formValue, 0) || 0;

    const formGroup = (<FormArray>this.form.get('lines')).at(formIndex);
    formGroup.patchValue({ _newQty });
  }

  clearLine(index: number) {
    const formArray = <FormArray>this.form.get('lines');
    if (formArray.length > index && formArray.length !== 1) {
      formArray.removeAt(index);
    } else {
      const lineFormGroup = <FormGroup>formArray.at(index);
      formArray.at(index).reset();

      this._stockOpname.lineConditionalValidation(lineFormGroup);
    }
  }

  clearAllLines() {
    const formArray = <FormArray>this.form.get('lines');
    while (formArray.length) {
      formArray.removeAt(0);
    }
  }

  resetLines() {
    this.clearAllLines();
    this.addLines();
  }
}

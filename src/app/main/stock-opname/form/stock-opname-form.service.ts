import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { Injectable } from '@angular/core';

import { StartingDataService } from '../../../core/core.module';

@Injectable()
export class StockOpnameFormService {
  constructor(
    private _startingData: StartingDataService,
  ) { }

  setFormDefinitions(form: FormGroup) {
    const startingData = this._startingData.data$.getValue();
    form.addControl('branchId', new FormControl(startingData.masterBranchId));
    form.addControl('warehouse', new FormControl());
    form.addControl('warehouseId', new FormControl());
    form.addControl('adjustmentAccount', new FormControl(''));
    form.addControl('adjustmentAccountId', new FormControl(''));
    form.addControl('description', new FormControl);
    form.addControl('transactionDate', new FormControl(new Date(), Validators.required));
    form.addControl('transactionNumber', new FormControl);
    form.addControl('note', new FormControl);
    form.addControl('lines', new FormArray([]));
  }

  buildFormChildLine() {
    return new FormGroup({
      id: new FormControl(null),
      inventoryAdjustmentId: new FormControl(null),
      rowVersion: new FormControl,
      product: new FormControl,
      productId: new FormControl(null),
      productVariant: new FormControl,
      productVariantId: new FormControl(null),
      sku: new FormControl,
      originalQty: new FormControl(0),
      changeQty: new FormControl(0),
      unitCost: new FormControl(0),
      _newQty: new FormControl(0),
    });
  }
}

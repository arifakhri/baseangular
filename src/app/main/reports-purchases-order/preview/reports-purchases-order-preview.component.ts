import * as moment from 'moment';
import { AutoComplete } from 'primeng/components/autocomplete/autocomplete';
import { Component, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

import { ReportsPurchasesOrderRestService } from '../reports-purchases-order-rest.service';

import { AdminLayoutService } from '../../../layouts/layouts.module';
import { BusinessPartnerRestService } from '../../business-partner/business-partner-rest.service';
import { CommonService, GridTableFilterService } from '../../../core/core.module';
import { PurchasesRestService } from '../../purchases/purchases-rest.service';
import { TransactionService } from '../../transaction/transaction.service';

@Component({
  selector: 'app-reports-purchases-order-preview',
  templateUrl: './reports-purchases-order-preview.component.html',
})
export class ReportsPurchasesOrderPreviewComponent {
  @ViewChild('vendorAC') elVendorAC: AutoComplete;
  @ViewChild('reportViewer') elReportViewer: any;

  compReady: boolean = false;
  data: IPurchasesOrder[] = [];
  form: FormGroup = new FormGroup({});
  reportViewerReady: boolean = false;

  ACVendorHandler = CommonService.remoteACItemsHandler({
    remoteParams: this.ACVendorParams.bind(this),
    remoteRequest: this._businessPartnerRest.findAllVendorsPicker.bind(this._businessPartnerRest),
    remoteRequestMap: (response) => response.data,
    element: () => this.elVendorAC,
  });

  filtersMap: IGridTableFilterMap = {
    lowDate: {
      targetVar: 'date',
      targetFilter: 'param',
      dateFormat: 'YYYY-MM-DD',
    },
    highDate: {
      targetVar: 'date',
      targetFilter: 'param',
      dateFormat: 'YYYY-MM-DD',
    },
    states: {
      targetFilter: 'param',
      targetVar: 'arrstring',
    },
    statuses: {
      targetFilter: 'param',
      targetVar: 'arrstring',
    },
    vendorId: {
      targetFilter: 'param',
    },
    transactionNumber: {
      targetFilter: 'param',
    },
  };

  reportParams: any = {
    title: 'Purchases Order List'
  };

  constructor(
    private _adminLayout: AdminLayoutService,
    private _businessPartnerRest: BusinessPartnerRestService,
    private _gridTableFilter: GridTableFilterService,
    private _reportsPurchasesOrderRest: ReportsPurchasesOrderRestService,
    private _purchasesRest: PurchasesRestService,
    public _transaction: TransactionService,
  ) {
    this._adminLayout.containerType = 1;

    this.buildForm();
    this.loadData();
  }

  buildForm() {
    this.form.addControl('lowDate', new FormControl(moment().set('date', 1).toDate()));
    this.form.addControl('highDate', new FormControl(new Date()));
    this.form.addControl('vendor', new FormControl);
    this.form.addControl('vendorId', new FormControl);
    this.form.addControl('states', new FormControl);
    this.form.addControl('statuses', new FormControl);
    this.form.addControl('transactionNumber', new FormControl(''));
  }

  ACVendorParams(event: any, type: string) {
    let filters = [];

    if (type === 'search') {
      filters = [{
        filterValues: [{
          field: 'displayName',
          operator: 'contains',
          value: event.query
        }]
      }];
    }

    return [{
      filter: filters,
      sort: [{
        field: 'displayName',
        dir: 'asc'
      }],
      take: 30,
      skip: 0,
      includeTotalCount: false
    }];
  }

  loadData() {
    const filters = this._gridTableFilter.parse(this.form.value, this.filtersMap);
    this.compReady = false;

    this._reportsPurchasesOrderRest.findAll(filters.qParams).subscribe(response => {
      this.data = response.data;

      this.prepareParams();

      this.compReady = true;
    });
  }

  prepareParams() {
    const filterValues = this.form.value;

    if (filterValues.lowDate) {
      this.reportParams.lowDate = moment(filterValues.lowDate).valueOf();
    }

    if (filterValues.highDate) {
      this.reportParams.highDate = moment(filterValues.highDate).valueOf();
    }
  }
}

import { NgModule } from '@angular/core';

import { ReportsPurchasesOrderModule } from './reports-purchases-order.module';
import { ReportsPurchasesOrderRoutingModule } from './reports-purchases-order-routing.module';

@NgModule({
  imports: [
    ReportsPurchasesOrderModule,
    ReportsPurchasesOrderRoutingModule,
  ],
})
export class ReportsPurchasesOrderLazyModule { }

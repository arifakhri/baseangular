import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ReportsPurchasesOrderPreviewComponent } from './preview/reports-purchases-order-preview.component';

import { AuthorizationService } from '../../core/core.module';

const routes: Routes = [{
  path: '',
  children: [{
    path: '',
    component: ReportsPurchasesOrderPreviewComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'reportsPurchasesOrder.preview'
    }
  }]
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ],
})
export class ReportsPurchasesOrderRoutingModule { }

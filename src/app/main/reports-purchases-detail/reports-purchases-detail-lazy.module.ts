import { NgModule } from '@angular/core';

import { ReportsPurchasesDetailModule } from './reports-purchases-detail.module';
import { ReportsPurchasesDetailRoutingModule } from './reports-purchases-detail-routing.module';

@NgModule({
  imports: [
    ReportsPurchasesDetailModule,
    ReportsPurchasesDetailRoutingModule,
  ],
})
export class ReportsPurchasesDetailLazyModule { }

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ReportsPurchasesDetailPreviewComponent } from './preview/reports-purchases-detail-preview.component';

import { AuthorizationService } from '../../core/core.module';

const routes: Routes = [{
  path: '',
  children: [{
    path: '',
    component: ReportsPurchasesDetailPreviewComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'reportsPurchasesDetail.preview'
    }
  }]
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ],
})
export class ReportsPurchasesDetailRoutingModule { }

declare interface IReportsPurchasesDetail {
  accountClass: string;
  accountType: string;
  accountName: string;
  total: 0;
}
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { AccountingService } from '../../../core/core.module';
import { AdminLayoutService } from '../../../layouts/layouts.module';
import { PurchasesPaymentRestService } from '../purchases-payment-rest.service';

@Component({
  selector: 'app-purchases-payment-detail',
  templateUrl: 'purchases-payment-detail.component.html'
})
export class PurchasesPaymentDetailComponent implements OnInit {
  doc: IPurchasesPayment;
  routeParams: any;

  constructor(
    public _accounting: AccountingService,
    private _adminLayout: AdminLayoutService,
    private _route: ActivatedRoute,
    private _purchasesPaymentRest: PurchasesPaymentRestService,
  ) {
    this._adminLayout.containerType = 1;
    this._route.params.subscribe(params => this.routeParams = params);
  }

  ngOnInit() {
    this.loadData();
  }

  loadData() {
    this._purchasesPaymentRest.load(this.routeParams.id, { includeLines: true }).subscribe(receipt => {
      this.doc = receipt;
    });
  }
}

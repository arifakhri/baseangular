import { NgModule } from '@angular/core';

import { PurchasesPaymentModule } from './purchases-payment.module';
import { PurchasesPaymentRoutingModule } from './purchases-payment-routing.module';

@NgModule({
  imports: [
    PurchasesPaymentModule,
    PurchasesPaymentRoutingModule
  ]
})
export class PurchasesPaymentLazyModule { }

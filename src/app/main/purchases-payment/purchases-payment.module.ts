import { AutoCompleteModule } from 'primeng/components/autocomplete/autocomplete';
import { CalendarModule } from 'primeng/components/calendar/calendar';
import { CheckboxModule } from 'primeng/components/checkbox/checkbox';
import { DataTableModule } from 'primeng/components/datatable/datatable';
import { ListboxModule } from 'primeng/components/listbox/listbox';
import { BsDropdownModule, ModalModule, PaginationModule, PopoverModule } from 'ngx-bootstrap';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { PurchasesPaymentCreateComponent } from './create/purchases-payment-create.component';
import { PurchasesPaymentDetailComponent } from './detail/purchases-payment-detail.component';
import { PurchasesPaymentFormComponent } from './form/purchases-payment-form.component';
import { PurchasesPaymentListComponent } from './list/purchases-payment-list.component';
import { PurchasesPaymentMoreFilterComponent } from './more-filter/purchases-payment-more-filter.component';
import { PurchasesPaymentQuickCreateComponent } from './quick-create/purchases-payment-quick-create.component';
import { PurchasesPaymentQuickFormComponent } from './quick-form/purchases-payment-quick-form.component';
import { PurchasesPaymentUpdateComponent } from './update/purchases-payment-update.component';

import { PurchasesPaymentMoreFilterService } from './more-filter/purchases-payment-more-filter.service';
import { PurchasesPaymentRestService } from './purchases-payment-rest.service';
import { PurchasesPaymentService } from './purchases-payment.service';

import { BusinessPartnerModule } from '../business-partner/business-partner.module';
import { CoreModule } from '../../core/core.module';
import { PaymentMethodModule } from '../payment-method/payment-method.module';
import { PurchasesModule } from '../purchases/purchases.module';
import { SharedModule } from '../../shared/shared.module';
import { TransactionModule } from '../transaction/transaction.module';

export const PROVIDERS = [
  PurchasesPaymentMoreFilterService,
  PurchasesPaymentRestService,
  PurchasesPaymentService,
];

@NgModule({
  imports: [
    AutoCompleteModule,
    BsDropdownModule,
    BusinessPartnerModule,
    CalendarModule,
    CheckboxModule,
    CommonModule,
    CoreModule,
    DataTableModule,
    FlexLayoutModule,
    FormsModule,
    HttpModule,
    ListboxModule,
    ModalModule,
    PaginationModule,
    PopoverModule,
    PaymentMethodModule,
    PurchasesModule,
    ReactiveFormsModule,
    RouterModule,
    SharedModule,
    TransactionModule,
    TranslateModule,
  ],
  declarations: [
    PurchasesPaymentCreateComponent,
    PurchasesPaymentDetailComponent,
    PurchasesPaymentFormComponent,
    PurchasesPaymentListComponent,
    PurchasesPaymentMoreFilterComponent,

    PurchasesPaymentQuickCreateComponent,
    PurchasesPaymentQuickFormComponent,
    PurchasesPaymentUpdateComponent,
  ],
  exports: [
    PurchasesPaymentQuickCreateComponent,
  ]
})
export class PurchasesPaymentModule { }

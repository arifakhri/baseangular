import { AutoComplete } from 'primeng/components/autocomplete/autocomplete';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ModalDirective } from 'ngx-bootstrap';

import { AccountingService, AutocompleteService } from '../../../core/core.module';
import { PurchasesPaymentRestService } from '../purchases-payment-rest.service';

@Component({
  selector: 'app-purchases-payment-quick-form',
  templateUrl: 'purchases-payment-quick-form.component.html'
})
export class PurchasesPaymentQuickFormComponent implements OnInit {
  @Input() form: FormGroup;
  @ViewChild('quickCreateModal') elQuickCreateModal: ModalDirective;
  @Input() sourceTransaction: ISalesInvoice;

  quickCreateAC: AutoComplete;

  accounts: IAccount[] = [];
  accountsSuggestion: IAccount[] = [];
  paymentMethods: IPaymentMethod[] = [];
  paymentMethodsSuggestion: IPaymentMethod[] = [];

  constructor(
    public _autocomplete: AutocompleteService,
    public _accounting: AccountingService,
    private _purchasesPaymentRest: PurchasesPaymentRestService,
  ) { }

  ngOnInit() {
    this.loadRelatedData();
  }

  assignQuickCreatedEntity(entity: any, storageVar: string) {
    this[storageVar].push(entity);
    this.quickCreateAC.selectItem(entity);
    this.elQuickCreateModal.hide();
  }

  loadRelatedData() {
    this._purchasesPaymentRest.loadRelatedData().subscribe(related => {
      this.accounts = related.paymentAccounts;
      this.paymentMethods = related.paymentMethods;
    });
  }
}

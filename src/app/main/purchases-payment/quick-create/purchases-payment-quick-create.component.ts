import { Component, EventEmitter, Inject, Input, OnInit, Output } from '@angular/core';
import { FormArray, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

import { CommonService, SystemMessageService } from '../../../core/core.module';
import { PurchasesPaymentRestService } from '../purchases-payment-rest.service';
import { PurchasesPaymentService } from '../purchases-payment.service';

@Component({
  selector: 'app-purchases-payment-quick-create',
  templateUrl: 'purchases-payment-quick-create.component.html',
  providers: [SystemMessageService]
})
export class PurchasesPaymentQuickCreateComponent implements OnInit {
  @Input() sourceTransaction: any;

  @Output() afterSubmit: EventEmitter<IPurchasesPayment> = new EventEmitter();
  @Output() close: EventEmitter<boolean> = new EventEmitter();

  form: FormGroup;
  showLoader: boolean = false;

  constructor(
    @Inject('GlobalSystemMessage') private _globalSystemMessage: SystemMessageService,
    private _purchasesPayment: PurchasesPaymentService,
    private _purchasesPaymentRest: PurchasesPaymentRestService,
    private _router: Router,
    public _systemMessage: SystemMessageService,
    private _translate: TranslateService
  ) { }

  ngOnInit() {
    this.form = new FormGroup({});

    this._purchasesPayment.setFormDefinitions(this.form);

    if (this.sourceTransaction) {
      const formGroupLine = this._purchasesPayment.buildFormChildLine();
      formGroupLine.patchValue({
        transaction: this.sourceTransaction,
        transactionType: this.sourceTransaction.transactionType,
        transactionId: this.sourceTransaction.id,
      });

      if (this.sourceTransaction.vendor) {
        this.form.patchValue({
          vendor: this.sourceTransaction.vendor,
          vendorId: this.sourceTransaction.vendorId,
        });
      }

      (<FormArray>this.form.get('lines')).push(formGroupLine);
    }
  }

  onSubmit() {
    if (this.formValid()) {
      this.showLoader = true;
      this.save();
    }
  }

  formValid(): boolean {
    if (!this.form.valid) {
      CommonService.markAsDirty(this.form);
      this._systemMessage.log({
        message: this._translate.instant('error.form'),
        type: 'error'
      });
      return false;
    }
    return true;
  }

  save(): void {
    const payment: IPurchasesPayment = this.form.value;
    this._purchasesPayment.normalizeDoc(payment);

    this._purchasesPaymentRest.create(payment)
      .catch(error => {
        this.showLoader = false;
        this._systemMessage.log({
          message: error,
          type: 'error'
        });
        return Observable.throw(error);
      })
      .subscribe(record => {
        this.showLoader = false;
        this._globalSystemMessage.log({
          message: this._translate.instant('success.purchasesPayment.quickCreate'),
          type: 'success',
          showAs: 'growl',
          showSnackBar: false,
        });

        this.afterSubmit.emit(record);
        this.form.reset();
      });
  }
}

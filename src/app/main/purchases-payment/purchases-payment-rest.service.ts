import { Injectable } from '@angular/core';
import 'app/rxjs-imports.ts';

import { ApiQueryOption, RequestService } from '../../core/core.module';

import { APP_CONST } from '../../app.const';

@Injectable()
export class PurchasesPaymentRestService {
  baseURL = `${APP_CONST.API_MAIN}/purchases/all-payments`;
  baseURLPayment = `${APP_CONST.API_MAIN}/purchases/payments`;

  request = this._request.new(this.baseURL);
  requestPayment = this._request.new(this.baseURLPayment);

  constructor(
    private _request: RequestService,
  ) { }

  create(payment: IPurchasesPayment, queryParams: any = {}) {
    return this.requestPayment.post<IPurchasesPayment>(``, payment, { params: queryParams });
  }

  findAll(queryOption: ApiQueryOption, queryParams: any = {}) {
    return this.request.post<IApiPaginationResult<IPurchasesPayment>>(`q`, queryOption, { params: queryParams });
  }

  load(paymentId: string, queryParams: any = {}) {
    return this.requestPayment.get<IPurchasesPayment>(`${paymentId}`, { params: queryParams });
  }

  loadRelatedData() {
    return this.requestPayment.get<{
      paymentMethods: IPaymentMethod[],
      paymentAccounts: IAccount[],
      settings: ISettings,
    }>(`entry-related-data`);
  }

  update(paymentId: string, updateObj: IPurchasesPayment, queryParams: any = {}) {
    return this.requestPayment.put<IPurchasesPayment>(`${paymentId}`, updateObj, { params: queryParams });
  }

  void(paymentId: string) {
    return this.requestPayment.put<IPurchasesPayment>(`${paymentId}/void`, {});
  }
}

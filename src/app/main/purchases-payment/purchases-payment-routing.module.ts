import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PurchasesPaymentCreateComponent } from './create/purchases-payment-create.component';
import { PurchasesPaymentDetailComponent } from './detail/purchases-payment-detail.component';
import { PurchasesPaymentListComponent } from './list/purchases-payment-list.component';
import { PurchasesPaymentUpdateComponent } from './update/purchases-payment-update.component';

import { AuthorizationService } from '../../core/core.module';

export const routes: Routes = [{
  path: '',
  children: [{
    path: '',
    component: PurchasesPaymentListComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'purchasesPayment.list'
    }
  }, {
    path: 'create',
    component: PurchasesPaymentCreateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'purchasesPayment.create'
    }
  }, {
    path: ':id',
    component: PurchasesPaymentDetailComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'purchasesPayment.detail'
    }
  }, {
    path: ':id/update',
    component: PurchasesPaymentUpdateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'purchasesPayment.update'
    }
  }]
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class PurchasesPaymentRoutingModule { }

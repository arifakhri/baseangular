import * as _ from 'lodash';
import { Component, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import 'app/rxjs-imports.ts';

import { PurchasesPaymentRestService } from '../purchases-payment-rest.service';
import { PurchasesPaymentService } from '../purchases-payment.service';

import { BaseCreateBComponent } from '../../../shared/base/base.module';
import { MPurchasesPayment } from '../purchases-payment.model';

@Component({
  selector: 'app-purchases-payment-create',
  templateUrl: 'purchases-payment-create.component.html'
})
export class PurchasesPaymentCreateComponent extends BaseCreateBComponent {
  @ViewChild('reportModal') elReportModal: ModalDirective;

  constructor(
    private _purchasesPayment: PurchasesPaymentService,
    private _purchasesPaymentRest: PurchasesPaymentRestService
  ) {
    super();

    this.componentId = 'PurchasesPaymentCreate';
    this.headerTitle = 'ui.purchasesPayment.create.title';
    this.containerType = 1;
    this.routeURL = '/purchases/payments';
    this.entrySuccessI18n = 'success.purchasesPayment.create';

    this.autoprintButtons = [{
      checkboxConfigId: 'autoprint.spayment.create',
      checkboxLabel: 'ui.purchasesPayment.create.action.apCheckbox.report',
      previewLabel: 'ui.purchasesPayment.create.action.previewReport',
      onPreview: () => {
        this.saveAndPrint();
      }
    }];

    this.registerHook('buildForm', event => {
      this._purchasesPayment.setFormDefinitions(this.form);
    });


    this.registerHook('save', event => {
      let payment = new MPurchasesPayment;

      payment = _.assign(payment, this.form.value);
      this._purchasesPayment.normalizeDoc(payment);

      const isPrinting = _.get(event, 'data.print');

      return this._purchasesPaymentRest.create(payment, { includeLines: isPrinting });
    });
  }

  async saveAndPrint() {
    try {
      const submitted = await this.onSubmit({ saveConfirm: true, print: true });
      if (submitted) {
        this._purchasesPaymentRest.loadRelatedData().subscribe(related => {
          this.doc['companyInfo'] = related.settings.companyInfo;
          this.elReportModal.show();
        });
      }
    } catch (e) {

    }
  }
}

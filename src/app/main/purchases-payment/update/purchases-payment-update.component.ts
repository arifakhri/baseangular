import * as _ from 'lodash';
import * as moment from 'moment';
import { Component, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';

import { PurchasesPaymentRestService } from '../purchases-payment-rest.service';
import { PurchasesPaymentService } from '../purchases-payment.service';

import { BaseUpdateBComponent } from '../../../shared/base/base.module';

@Component({
  selector: 'app-purchases-payment-update',
  templateUrl: 'purchases-payment-update.component.html',
})
export class PurchasesPaymentUpdateComponent extends BaseUpdateBComponent {
  @ViewChild('reportModal') elReportModal: ModalDirective;

  constructor(
    private _purchasesPayment: PurchasesPaymentService,
    private _purchasesPaymentRest: PurchasesPaymentRestService,
  ) {
    super();

    this.componentId = 'PurchasesPaymentUpdate';
    this.headerTitle = 'ui.purchasesPayment.update.title';
    this.containerType = 1;
    this.routeURL = '/purchases/payments';
    this.entrySuccessI18n = 'success.purchasesPayment.update';

    this.registerHook('buildForm', event => {
      this._purchasesPayment.setFormDefinitions(this.form);
    });


    this.registerHook('load', event => {
      return this._purchasesPaymentRest.load(this.page.routeParams.id, { includeLines: true }).switchMap(doc => {
        if (doc.transactionDate) {
          doc.transactionDate = moment(doc.transactionDate).toDate();
        }
        return Observable.of(doc);
      });
    });

    this.registerHook('save', event => {
      const payment: IPurchasesPayment = Object.assign({}, this.doc, this.form.value);
      this._purchasesPayment.normalizeDoc(payment);

      const isPrinting = _.get(event, 'data.print');

      return this._purchasesPaymentRest.update(this.page.routeParams.id, payment, { includeLines: isPrinting });
    });
  }


  async saveAndPrint() {
    try {
      const submitted = await this.onSubmit({ saveConfirm: true, print: true });
      if (submitted) {
        this._purchasesPaymentRest.loadRelatedData().subscribe(related => {
          this.doc['companyInfo'] = related.settings.companyInfo;
          this.elReportModal.show();
        });
      }
    } catch (e) {

    }
  }
}

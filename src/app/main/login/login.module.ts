import { CheckboxModule } from 'primeng/components/checkbox/checkbox';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';

import { LoginComponent } from './login.component';

import { CoreModule } from '../../core/core.module';
import { SharedModule as CentricSharedModule } from '../../modules/ng2centric/shared/shared.module';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  imports: [
    CentricSharedModule,
    CheckboxModule,
    CommonModule,
    CoreModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    SharedModule,
    TranslateModule.forChild(),
  ],
  declarations: [
    LoginComponent
  ]
})
export class LoginModule { }

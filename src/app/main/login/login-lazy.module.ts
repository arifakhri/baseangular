import { NgModule } from '@angular/core';

import { LoginModule } from './login.module';
import { LoginRoutingModule } from './login-routing.module';

@NgModule({
  imports: [
    LoginModule,
    LoginRoutingModule
  ]
})
export class LoginLazyModule { }

import * as _ from 'lodash';
import { ActivatedRoute } from '@angular/router';
import { Component, Inject, OnInit } from '@angular/core';
import { CustomValidators } from 'ng2-validation';
import { environment } from '../../../environments/environment';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LocalStorage } from 'ngx-webstorage';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';
import { TranslateService } from '@ngx-translate/core';

import {
  ApiBootstrapService,
  AuthenticationService,
  CommonService,
  RedirectionService,
  SystemMessageService,
} from '../../core/core.module';
import { SpinnerService } from '../../shared/spinner/spinner.service';

@Component({
  selector: 'app-login',
  templateUrl: 'login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [SystemMessageService]
})
export class LoginComponent implements OnInit {
  @LocalStorage() private rememberMe: string;
  @LocalStorage() public rememberMeChecked: boolean;

  loginForm: FormGroup;
  tokenExpired: boolean;

  constructor(
    @Inject('GlobalSystemMessage') private _globalSystemMessage: SystemMessageService,
    private _activatedRoute: ActivatedRoute,
    private _apiBootstrap: ApiBootstrapService,
    private _formBuilder: FormBuilder,
    private _authentication: AuthenticationService,
    private _spinner: SpinnerService,
    private _redirection: RedirectionService,
    private _translate: TranslateService,
    public _systemMessage: SystemMessageService,
  ) {
    this._authentication.logout();

    let username = '';
    if (!environment.production) {
      username = 'admin@demo1.com';
    } else if (this.rememberMe) {
      username = this.rememberMe;
    }

    this.loginForm = _formBuilder.group({
      'username': [username, [Validators.required, CustomValidators.email]],
      'password': [environment.production ? '' : 'password', Validators.required]
    });
  }

  ngOnInit() {
    this._apiBootstrap.removePreloader();

    this.tokenExpired = this._activatedRoute.snapshot.queryParams['tokenExpired'];
    if (this.tokenExpired) {
      this._globalSystemMessage.log({
        message: this._translate.instant('ui.sessionExpired.error.description'),
        type: 'error',
        showAs: 'growl',
        showSnackBar: false,
      });
    }
  }

  submit(e) {
    if (this.formValid()) {
      const spinner = this._spinner.show({
        element: $('.login-box'),
      });

      if (this.rememberMeChecked) {
        this.rememberMe = this.loginForm.value.username;
      } else {
        this.rememberMe = null;
      }

      this._authentication.login(this.loginForm.value)
        .catch(error => {
          spinner.dispose();

          this._systemMessage.log({
            message: _.get(error, 'response.data.error_description', this._translate.instant('error.unknown')),
            type: 'error'
          });

          return Observable.throw(error);
        })
        .subscribe(user => {
          this._apiBootstrap.boot().first().subscribe(() => {
            this._redirection.redirectAfterLogin().then(() => {
              spinner.dispose();
            });
          });
        });
    }
  }

  formValid(): boolean {
    CommonService.markAsDirty(this.loginForm);
    if (!this.loginForm.valid) {
      this._systemMessage.log({
        message: this._translate.instant('error.form'),
        type: 'error'
      });
      return false;
    }
    return true;
  }
}

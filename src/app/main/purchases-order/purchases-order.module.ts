import { AutoCompleteModule } from 'primeng/components/autocomplete/autocomplete';
import { CalendarModule } from 'primeng/components/calendar/calendar';
import { CheckboxModule } from 'primeng/components/checkbox/checkbox';
import { DataTableModule } from 'primeng/components/datatable/datatable';
import { ListboxModule } from 'primeng/components/listbox/listbox';
import { BsDropdownModule, ModalModule, PaginationModule, PopoverModule } from 'ngx-bootstrap';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { PurchasesOrderCreateComponent } from './create/purchases-order-create.component';
import { PurchasesOrderDetailComponent } from './detail/purchases-order-detail.component';
import { PurchasesOrderFormComponent } from './form/purchases-order-form.component';
import { PurchasesOrderListComponent } from './list/purchases-order-list.component';
import { PurchasesOrderMoreFilterComponent } from './more-filter/purchases-order-more-filter.component';
import { PurchasesOrderUpdateComponent } from './update/purchases-order-update.component';

import { PurchasesOrderCloneService } from './purchases-order-clone.service';
import { PurchasesOrderCopyInvoiceService } from './purchases-order-copy-invoice.service';
import { PurchasesOrderFormDPService } from './form/purchases-order-form-dp.service';
import { PurchasesOrderFormService } from './form/purchases-order-form.service';
import { PurchasesOrderMoreFilterService } from './more-filter/purchases-order-more-filter.service';
import { PurchasesOrderRestService } from './purchases-order-rest.service';
import { PurchasesOrderService } from './purchases-order.service';

import { BusinessPartnerModule } from '../business-partner/business-partner.module';
import { CoreModule } from '../../core/core.module';
import { ProductVariantModule } from '../product-variant/product-variant.module';
import { PurchasesModule } from '../purchases/purchases.module';
import { PurchasesDownPaymentModule } from '../purchases-down-payment/purchases-down-payment.module';
import { SharedModule } from '../../shared/shared.module';
import { TransactionModule } from '../transaction/transaction.module';

export const PROVIDERS = [
  PurchasesOrderCloneService,
  PurchasesOrderCopyInvoiceService,
  PurchasesOrderFormDPService,
  PurchasesOrderFormService,
  PurchasesOrderMoreFilterService,
  PurchasesOrderRestService,
  PurchasesOrderService,
];

@NgModule({
  imports: [
    AutoCompleteModule,
    BsDropdownModule,
    BusinessPartnerModule,
    CalendarModule,
    CheckboxModule,
    CommonModule,
    CoreModule,
    DataTableModule,
    FlexLayoutModule,
    FormsModule,
    HttpModule,
    ListboxModule,
    ModalModule,
    PaginationModule,
    PopoverModule,
    ProductVariantModule,
    PurchasesDownPaymentModule,
    PurchasesModule,
    ReactiveFormsModule,
    RouterModule,
    SharedModule,
    TransactionModule,
    TranslateModule,
  ],
  declarations: [
    PurchasesOrderCreateComponent,
    PurchasesOrderDetailComponent,
    PurchasesOrderFormComponent,
    PurchasesOrderListComponent,
    PurchasesOrderMoreFilterComponent,
    PurchasesOrderUpdateComponent,
  ],
})
export class PurchasesOrderModule { }

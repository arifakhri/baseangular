import * as _ from 'lodash';
import * as moment from 'moment';
import { Injectable } from '@angular/core';

@Injectable()
export class PurchasesOrderService {
  normalizeDoc(record: any) {
    if (record.transactionDate) {
      record.transactionDate = moment(record.transactionDate).format('YYYY-MM-DD');
    }
    if (record.deliveryDate) {
      record.deliveryDate = moment(record.deliveryDate).format('YYYY-MM-DD');
    }

    record.lines = _.reject(record.lines, line => {
      return !(<any>line).productId || !(<any>line).productVariantId;
    });

    record.lines.forEach(line => {
      line.discountAmount = +line.discountAmount;
      line.taxRate = +line.taxRate;
    });
  }
}

import * as _ from 'lodash';
import * as moment from 'moment';
import { FormArray, FormGroup } from '@angular/forms';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { PurchasesOrderRestService } from './purchases-order-rest.service';

@Injectable()
export class PurchasesOrderCopyInvoiceService {
  private currentDoc: IPurchasesOrder;
  private parentTransactionType: string = 'purchase_order';

  constructor(
    private _purchasesOrderRest: PurchasesOrderRestService,
    private _router: Router,
  ) { }

  set docOnHold(order) {
    this.currentDoc = order;
  }

  get docOnHold() {
    if (!this.currentDoc) {
      return null;
    }

    const targetDoc: any = _.cloneDeep(this.currentDoc);
    if (targetDoc.transactionDate) {
      targetDoc.transactionDate = moment(targetDoc.transactionDate).toDate();
    }

    targetDoc.parentTransactionType = this.parentTransactionType;

    _.remove(targetDoc.lines, line => {
      return !((<any>line).qty - (<any>line).qtyFulfilled > 0);
    });

    targetDoc.lines.forEach(line => {
      line.parentTransactionId = line.purchaseTransactionId;
      line.parentTransactionLineId = line.id;
      line.expenseAccountId = _.get(line, 'product.expenseAccountId') || null;
      line.qty = (<any>line).qty - (<any>line).qtyFulfilled;
    });

    return targetDoc;
  }

  copy(order, navigate: boolean = true) {
    this.docOnHold = order;

    if (navigate) {
      this._router.navigateByUrl('/purchases/invoices/create');
    }
  }

  copyById(orderId: string, navigate: boolean = false) {
    return this._purchasesOrderRest.load(orderId).do(order => {
      this.copy(order, navigate);
    });
  }

  apply() {
    const parentDoc = Object.assign({}, this.docOnHold);
    parentDoc.transactionNumber = null;

    let targetDueDate = new Date;
    if (parentDoc.vendor.defaultPurchaseInvoiceDueDays) {
      targetDueDate = moment().add(parentDoc.vendor.defaultPurchaseInvoiceDueDays, 'days').toDate();
    }
    parentDoc.dueDate = targetDueDate;

    this.docOnHold = null;

    return parentDoc;
  }

  reset(form: FormGroup) {
    form.get('parentTransactionType').reset(null);

    const linesFormGroup = <FormArray>form.get('lines');
    _.forEach(linesFormGroup.controls, (lineFormGroup, lineIdx) => {
      lineFormGroup.get('parentTransactionId').reset(null);
      lineFormGroup.get('parentTransactionLineId').reset(null);
      lineFormGroup.get('expenseAccountId').reset(null);
    });
  }
}

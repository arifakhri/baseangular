import { NgModule } from '@angular/core';

import { PurchasesOrderModule } from './purchases-order.module';
import { PurchasesOrderRoutingModule } from './purchases-order-routing.module';

@NgModule({
  imports: [
    PurchasesOrderModule,
    PurchasesOrderRoutingModule
  ]
})
export class PurchasesOrderLazyModule { }

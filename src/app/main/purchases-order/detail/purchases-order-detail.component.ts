import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';

import { AccountingService } from '../../../core/core.module';
import { AdminLayoutService } from '../../../layouts/layouts.module';
import { PurchasesOrderCloneService } from '../purchases-order-clone.service';
import { PurchasesOrderCopyInvoiceService } from '../purchases-order-copy-invoice.service';
import { PurchasesOrderRestService } from '../purchases-order-rest.service';

@Component({
  selector: 'app-purchases-order-detail',
  templateUrl: 'purchases-order-detail.component.html'
})
export class PurchasesOrderDetailComponent implements OnInit {
  doc: IPurchasesOrder;
  routeParams: any;
  compLoading: boolean = true;

  @ViewChild('quickPayModal') elQuickPayModal: ModalDirective;

  constructor(
    public _accounting: AccountingService,
    private _adminLayout: AdminLayoutService,
    public _purchasesOrderClone: PurchasesOrderCloneService,
    public _purchasesOrderCopyInvoice: PurchasesOrderCopyInvoiceService,
    private _purchasesOrderRest: PurchasesOrderRestService,
    private _route: ActivatedRoute,
  ) {
    this._adminLayout.containerType = 1;
    this._route.params.subscribe(params => this.routeParams = params);
  }

  ngOnInit() {
    this.loadData();
  }

  loadData() {
    this._purchasesOrderRest.load(this.routeParams.id).subscribe(order => {
      this.doc = order;
      this.compLoading = false;
    });
  }

  onPaymentQuickCreated(payment) {
    this.elQuickPayModal.hide();
    this.compLoading = true;
    this.loadData();
  }
}

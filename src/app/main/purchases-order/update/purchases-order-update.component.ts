import * as _ from 'lodash';
import * as moment from 'moment';
import { Component, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';

import { PurchasesOrderFormService } from '../form/purchases-order-form.service';
import { PurchasesOrderRestService } from '../purchases-order-rest.service';
import { PurchasesOrderService } from '../purchases-order.service';

import { BaseUpdateBComponent } from '../../../shared/base/base.module';

@Component({
  selector: 'app-purchases-order-update',
  templateUrl: 'purchases-order-update.component.html',
})
export class PurchasesOrderUpdateComponent extends BaseUpdateBComponent {
  @ViewChild('reportModal') elReportModal: ModalDirective;

  showParseModal: boolean = false;

  constructor(
    private _purchasesOrder: PurchasesOrderService,
    private _purchasesOrderForm: PurchasesOrderFormService,
    private _purchasesOrderRest: PurchasesOrderRestService,
  ) {
    super();

    this.componentId = 'PurchasesOrderUpdate';
    this.headerTitle = 'ui.purchasesOrder.update.title';
    this.containerType = 1;
    this.routeURL = '/purchases/orders';
    this.entrySuccessI18n = 'success.purchasesOrder.update';



    this.registerHook('buildForm', event => {
      this._purchasesOrderForm.setFormDefinitions(this.form);
    });

    this.registerHook('load', event => {
      return this._purchasesOrderRest.load(this.page.routeParams.id).switchMap(doc => {
        if (doc.transactionDate) {
          doc.transactionDate = moment(doc.transactionDate).toDate();
        }
        if (doc.deliveryDate) {
          doc.deliveryDate = moment(doc.deliveryDate).toDate();
        }
        return Observable.of(doc);
      });
    });

    this.registerHook('save', event => {
      const order: IPurchasesOrder = Object.assign({}, this.doc, this.form.value);
      this._purchasesOrder.normalizeDoc(order);

      const isPrinting = _.get(event, 'data.print');

      return this._purchasesOrderRest.update(this.page.routeParams.id, order, { includeLines: isPrinting });
    });
  }

  async saveAndPrint() {
    try {
      const submitted = await this.onSubmit({ saveConfirm: true, print: true });
      if (submitted) {
        this._purchasesOrderRest.loadRelatedData().subscribe(related => {
          this.doc['companyInfo'] = related.settings.companyInfo;
          this.elReportModal.show();
        });
      }
    } catch (e) {

    }
  }
}

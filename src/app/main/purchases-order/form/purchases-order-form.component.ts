import * as _ from 'lodash';
import { Component, Input, OnChanges, OnInit, SimpleChanges, } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';

import { AccountingService, AutocompleteService, CommonService } from '../../../core/core.module';
import { PurchasesOrderCloneService } from '../../purchases-order/purchases-order-clone.service';
import { PurchasesOrderFormDPService } from './purchases-order-form-dp.service';
import { PurchasesOrderFormService } from '../form/purchases-order-form.service';
import { PurchasesOrderRestService } from '../purchases-order-rest.service';

import { TransactionFormPurchasesBComponent } from '../../transaction/transaction-form-purchases.bcomponent';

@Component({
  selector: 'app-purchases-order-form',
  templateUrl: 'purchases-order-form.component.html',
  providers: [
    PurchasesOrderFormDPService,
  ],
})
export class PurchasesOrderFormComponent extends TransactionFormPurchasesBComponent implements OnChanges, OnInit {
  @Input() parseResults: any = null;

  cloneTransaction: boolean = false;

  ACProductVariantHandler = CommonService.remoteACItemsHandler({
    remoteParams: this.ACProductVariantParams.bind(this),
    remoteRequest: this._productVariantRest.findAllPicker.bind(this._productVariantRest),
    remoteRequestMap: (response) => response.data,
  });

  quickDPForm: FormGroup;

  constructor(
    public _autocomplete: AutocompleteService,
    public _accounting: AccountingService,
    private _purchasesOrderClone: PurchasesOrderCloneService,
    public _purchasesOrderForm: PurchasesOrderFormService,
    public _purchasesOrderFormDP: PurchasesOrderFormDPService,
    private _purchasesOrderRest: PurchasesOrderRestService,
  ) {
    super();
    super.init();

    this.componentId = 'PurchasesOrderForm';

    this.registerHook('loadRelated', event => {
      return this._purchasesOrderRest.loadRelatedData();
    }, event => {
      if (this._purchasesOrderClone.docOnHold) {
        const parentDoc = <any>this._purchasesOrderClone.docOnHold;
        parentDoc.deliveryDate = null;
        parentDoc.transactionDate = new Date;
        parentDoc.transactionNumber = null;

        this.cloneTransaction = true;
        this.doc = parentDoc;
        this._purchasesOrderClone.docOnHold = null;

        this.applyDoc();

        this.form.patchValue(this.doc);
      }
    });

    this.registerHook('patchForm', event => {
      this.applyDoc();
    });
  }

  ngOnInit() {
    super.ngOnInit();

    this.addLines();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (_.has(changes, 'doc') && this.doc) {
      this.applyDoc();
    }
  }

  getDefaultValue(type: 'master' | 'line'): any {
    return this._purchasesOrderForm.getDefaultValues(type);
  }

  buildFormChildLine() {
    return this._purchasesOrderForm.buildFormChildLine();
  }

  lineConditionalValidation(formGroup: FormGroup) {
    this._purchasesOrderForm.lineConditionalValidation(formGroup);
  }

  createQuickDP() {
    this.quickDPForm = new FormGroup({});
    this._purchasesOrderFormDP.prepareForm(this.form, this.quickDPForm).subscribe();
  }

  validateDP() {
    if (this.quickDPForm && !this.quickDPForm.valid) {
      CommonService.markAsDirty(this.quickDPForm);
      return false;
    }
    return true;
  }

  submitDP(doc: IPurchasesOrder) {
    const mainReturnObs = Observable.of(doc);

    if (this.quickDPForm && this.quickDPForm.get('downPaymentAmount').value) {
      this.quickDPForm.get('orderId').setValue(doc.id);

      return this._purchasesOrderFormDP.submit(this.quickDPForm).switchMap(() => mainReturnObs);
    } else {
      return mainReturnObs;
    }
  }
}

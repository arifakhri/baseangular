import { FormGroup } from '@angular/forms';
import { Injectable } from '@angular/core';

import { CommonService } from '../../../core/core.module';
import { PurchasesDownPaymentService } from '../../purchases-down-payment/purchases-down-payment.service';
import { PurchasesDownPaymentRestService } from '../../purchases-down-payment/purchases-down-payment-rest.service';

@Injectable()
export class PurchasesOrderFormDPService {
  searchACItems = CommonService.searchLocalACItems.bind(this);
  onACDropdown = CommonService.onLocalACDropdown.bind(this);

  paymentMethods: IPaymentMethod[] = [];
  paymentMethodsSuggestion: IPaymentMethod[] = [];

  accounts: IAccount[] = [];
  accountsSuggestion: IAccount[] = [];

  constructor(
    private _purchasesDownPayment: PurchasesDownPaymentService,
    private _purchasesDownPaymentRest: PurchasesDownPaymentRestService,
  ) { }

  prepareForm(srcForm, dpForm: FormGroup) {
    this._purchasesDownPayment.setFormDefinitions(dpForm);

    CommonService.syncFormControls([{
      from: srcForm.get('transactionDate'),
      to: dpForm.get('transactionDate'),
    }, {
      from: srcForm.get('vendorId'),
      to: dpForm.get('vendorId'),
    }, {
      from: srcForm.get('vendorEmail'),
      to: dpForm.get('vendorEmail'),
    }]);

    return this.loadRelatedData();
  }

  loadRelatedData() {
    return this._purchasesDownPaymentRest.loadRelatedData()
      .do(related => {
        this.paymentMethods = [null].concat(related.paymentMethods);
        this.accounts = related.paymentAccounts;
      });
  }

  submit(form: FormGroup) {
    const downPayment: IPurchasesDownPayment = form.value;
    this._purchasesDownPayment.normalizeDoc(downPayment);

    return this._purchasesDownPaymentRest.create(downPayment);
  }
}

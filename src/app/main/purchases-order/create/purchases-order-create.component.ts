import * as _ from 'lodash';
import { Component, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import 'app/rxjs-imports.ts';

import { PurchasesOrderFormComponent } from '../form/purchases-order-form.component';

import { PurchasesOrderFormService } from '../form/purchases-order-form.service';
import { PurchasesOrderRestService } from '../purchases-order-rest.service';
import { PurchasesOrderService } from '../purchases-order.service';

import { BaseCreateBComponent } from '../../../shared/base/base.module';
import { MPurchasesOrder } from '../purchases-order.model';

@Component({
  selector: 'app-purchases-order-create',
  templateUrl: 'purchases-order-create.component.html',
})
export class PurchasesOrderCreateComponent extends BaseCreateBComponent {
  @ViewChild('reportModal') elReportModal: ModalDirective;
  @ViewChildren('purchasesOrderForm') elPurchasesOrderForm: QueryList<PurchasesOrderFormComponent>;

  compReady: boolean;
  showParseModal: boolean = false;

  constructor(
    private _purchasesOrder: PurchasesOrderService,
    private _purchasesOrderForm: PurchasesOrderFormService,
    private _purchasesOrderRest: PurchasesOrderRestService,
  ) {
    super();

    this.componentId = 'PurchasesOrderCreate';
    this.headerTitle = 'ui.purchasesOrder.create.title';
    this.containerType = 1;
    this.routeURL = '/purchases/orders';
    this.entrySuccessI18n = 'success.purchasesOrder.create';

    this.autoprintButtons = [{
      checkboxConfigId: 'autoprint.sorder.create',
      checkboxLabel: 'ui.purchasesOrder.create.action.apCheckbox.report',
      previewLabel: 'ui.purchasesOrder.create.action.previewReport',
      onPreview: () => {
        this.saveAndPrint();
      }
    }];

    this.registerHook('buildForm', event => {
      this._purchasesOrderForm.setFormDefinitions(this.form);
    });

    this.registerHook('save', event => {
      let order = new MPurchasesOrder;
      order = _.assign(order, this.form.value);
      this._purchasesOrder.normalizeDoc(order);

      const isPrinting = _.get(event, 'data.print');

      let obs = this._purchasesOrderRest.create(order, { includeLines: isPrinting });
      if (!isPrinting) {
        obs = obs.switchMap(result => this.elPurchasesOrderForm.first.submitDP(result));
      }
      return obs;
    });
  }

  async saveAndPrint() {
    try {
      const submitted = await this.onSubmit({ saveConfirm: true, print: true });
      if (submitted) {
        this._purchasesOrderRest.loadRelatedData().subscribe(related => {
          this.doc['companyInfo'] = related.settings.companyInfo;
          this.elReportModal.show();
        });
      }
    } catch (e) {

    }
  }
}

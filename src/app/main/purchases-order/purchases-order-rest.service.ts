import { Injectable } from '@angular/core';
import 'app/rxjs-imports.ts';

import { ApiQueryOption, RequestService } from '../../core/core.module';

import { APP_CONST } from '../../app.const';

@Injectable()
export class PurchasesOrderRestService {
  baseURL = `${APP_CONST.API_MAIN}/purchases/orders`;
  request = this._request.new(this.baseURL);

  constructor(
    private _request: RequestService,
  ) { }

  create(order: IPurchasesOrder, queryParams: any = {}) {
    return this.request.post<IPurchasesOrder>(``, order, { params: queryParams });
  }

  findAll(queryOption: ApiQueryOption, queryParams: any = {}) {
    return this.request.post<IApiPaginationResult<IPurchasesOrder>>(`q`, queryOption, { params: queryParams });
  }

  load(orderId: string) {
    return this.request.get<IPurchasesOrder>(`${orderId}?includeLines=true`);
  }

  loadRelatedData() {
    return this.request.get<{ taxes: ITax[], warehouses: IWarehouse[], settings: ISettings }>(`entry-related-data`);
  }

  update(orderId: string, updateObj: IPurchasesOrder, queryParams: any = {}) {
    return this.request.put<IPurchasesOrder>(`${orderId}`, updateObj, { params: queryParams });
  }

  void(orderId: string) {
    return this.request.put<IPurchasesOrder>(`${orderId}/void`, {});
  }
}

import * as _ from 'lodash';
import * as SnackBar from 'node-snackbar';
import swal from 'sweetalert2';
import { ActivatedRoute } from '@angular/router';
import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { DataTable } from 'primeng/components/datatable/datatable';
import { Observable, Subscription } from 'rxjs';
import 'app/rxjs-imports.ts';
import { ModalDirective, PopoverDirective } from 'ngx-bootstrap';
import { TranslateService } from '@ngx-translate/core';

import {
  AccountingService,
  ExportDataTableService,
  GridTableDataSource,
  GridTableService,
  GridTableToggleService,
  SystemMessageService,
} from '../../../core/core.module';
import { AdminLayoutService } from '../../../layouts/layouts.module';
import { SpinnerDirective } from '../../../shared/spinner/spinner.directive';
import { PurchasesOrderCloneService } from '../purchases-order-clone.service';
import { PurchasesOrderCopyInvoiceService } from '../../purchases-order/purchases-order-copy-invoice.service';
import { PurchasesOrderMoreFilterService } from '../more-filter/purchases-order-more-filter.service';
import { PurchasesOrderRestService } from '../purchases-order-rest.service';
import { RetryDialogComponent } from '../../../shared/retry-dialog/retry-dialog.component';

@Component({
  selector: 'app-purchases-order-list',
  templateUrl: 'purchases-order-list.component.html',
  providers: [SystemMessageService]
})
export class PurchasesOrderListComponent implements OnDestroy, OnInit {
  @ViewChild('gridTable') public gridTable: DataTable;
  @ViewChild('pageLoading') elPageLoading: SpinnerDirective;
  @ViewChild('popMoreFilter') elPopMoreFilter: PopoverDirective;
  @ViewChild('quickDepositModal') elQuickDepositModal: ModalDirective;
  @ViewChild('reportModal') elReportModal: ModalDirective;
  @ViewChild('retryDialog') elRetryDialog: RetryDialogComponent;

  compReady: boolean;
  doc: IPurchasesOrder;
  orderes: IPurchasesOrder[] = [];
  gridDataSource: GridTableDataSource<IPurchasesOrder> = new GridTableDataSource<IPurchasesOrder>();
  selectedRecords: IPurchasesOrder[] = [];

  combinedFilterValues: string = '';
  combinedFilterFields = {
    'vendor.displayName': true,
    transactionNumber: true,
    description: true,
  };

  qParamsFilters: any = {
    keyword: null
  };
  moreFilterValues: IGridTableFilterParsed;

  tableColumns: IGridTableColumn[] = [{
    i18nLabel: 'ui.purchasesOrder.list.column.transactionDate',
    field: 'transactionDate',
    dateFormat: 'DD/MM/YYYY',
    sort: true,
  }, {
    i18nLabel: 'ui.purchasesOrder.list.column.transactionNumber',
    field: 'transactionNumber',
    link: (row: IPurchasesOrder) => {
      return `/purchases/orders/${row.id}`;
    },
    sort: true,
  }, {
    i18nLabel: 'ui.purchasesOrder.list.column.description',
    field: 'description',
    sort: true,
  }, {
    i18nLabel: 'ui.purchasesOrder.list.column.vendorName',
    field: 'vendor.displayName',
    sort: true,
  }, {
    i18nLabel: 'ui.purchasesOrder.list.column.deliveryDate',
    field: 'deliveryDate',
    dateFormat: 'DD/MM/YYYY',
    sort: true,
  }, {
    i18nLabel: 'ui.purchasesOrder.list.column.total',
    field: 'total',
    sort: true,
    formatter: (value, row: IPurchasesOrder) => {
      return this._accounting.ac.formatMoney(value, '');
    },
    columnClasses: 'right-align',
  }, {
    i18nLabel: 'ui.purchasesOrder.list.column.deposit',
    field: 'downPaymentAmount',
    sort: true,
    formatter: (value, row: IPurchasesOrder) => {
      return this._accounting.ac.formatMoney(value, '');
    },
    columnClasses: 'right-align',
  }, {
    i18nLabel: 'ui.purchasesOrder.list.column.status',
    field: 'status',
    sort: true,
    formatter: (value, row: IPurchasesOrder) => {
      return _.startCase(value);
    },
  }];
  tableColumnsShow: boolean = false;
  tableColumnsToggle = this._gridTableToggle.mapToggleOptionsFromColumns(this.tableColumns);

  retryDialogShownState: boolean = false;
  retryDialogOnRetry: Function;

  quickDepositDoc: any;

  private loadDataTimeoutId: number;
  private gridFirstLoad: boolean = true;
  private subscriptions: { [key: string]: Subscription[] } = {
    load: []
  };

  constructor(
    public _accounting: AccountingService,
    private _activatedRoute: ActivatedRoute,
    private _adminLayout: AdminLayoutService,
    private _dataTableExport: ExportDataTableService,
    private _gridTable: GridTableService,
    private _gridTableToggle: GridTableToggleService,
    public _purchasesOrderClone: PurchasesOrderCloneService,
    private _purchasesOrderCopyInvoice: PurchasesOrderCopyInvoiceService,
    private _purchasesOrderMoreFilter: PurchasesOrderMoreFilterService,
    private _purchasesOrderRest: PurchasesOrderRestService,
    public _systemMessage: SystemMessageService,
    private _translate: TranslateService,
  ) {
    this._adminLayout.containerType = 1;
  }

  ngOnInit() {
    this.loadData();
  }

  ngOnDestroy() {
    this._purchasesOrderMoreFilter.lastMoreFilterValues = {};
  }

  onMoreFilter(filters: IGridTableFilterParsed) {
    this.elPopMoreFilter.hide();

    this.moreFilterValues = filters;
    this.loadData();
  }

  populateFilters() {
    let newFilters: any = _.pick(this.qParamsFilters, ['keyword']);

    if (this.moreFilterValues) {
      newFilters = Object.assign({}, newFilters, this.moreFilterValues.qParams);
    }

    this.qParamsFilters = newFilters;
  }

  gridLoadDataWrapper() {
    // Grid always load at the first time. we dont want that. we already call on ngOnInit
    if (this.gridFirstLoad) {
      this.gridFirstLoad = false;
      return;
    }

    if (this.loadDataTimeoutId) {
      window.clearTimeout(this.loadDataTimeoutId);
      this.loadDataTimeoutId = null;
    }

    this.loadDataTimeoutId = window.setTimeout(() => {
      this.loadData();
    }, 300);
  }

  showVoidDialog(orderId: string) {
    swal({
      title: this._translate.instant('ui.purchasesOrder.confirm.void.label'),
      text: this._translate.instant('ui.purchasesOrder.confirm.void.description'),
      type: 'question',
      showCancelButton: true,
    })
      .then(() => {
        this.voidOrder(orderId);
      })
      .catch(() => { });
  }

  voidOrder(orderId: string) {
    this._purchasesOrderRest.void(orderId)
    .catch((error) => {
      this._systemMessage.log({
        message: error.response.data[0].errorMessage,
        type: 'error'
      });
      return Observable.throw(error);
    })
    .subscribe(response => {
      this.gridLoadDataWrapper();
      SnackBar.show({
        text: this._translate.instant('ui.purchasesOrder.success.void'),
        pos: 'bottom-right'
      });
    });
  }

  printReport(orderId: string) {
    this._purchasesOrderRest.load(orderId).subscribe(response => {
      this.doc = response;
      this._purchasesOrderRest.loadRelatedData().subscribe(related => {
        this.doc['companyInfo'] = related.settings.companyInfo;
        this.elReportModal.show();
      });
    });
  }

  get exportRecords(): Observable<IPurchasesOrder[]> {
    const qOption = {
      ...this._gridTable.generateApiQueryOptionFromGridInfo(
        this.gridDataSource.pager,
        false,
        this.gridTable,
        this._gridTable.generateCombinedApiFilters(this.combinedFilterValues, this.combinedFilterFields)
      ),
      skip: 0,
      take: this._dataTableExport.exportLimit,
    };

    this.populateFilters();

    return this._purchasesOrderRest.findAll(qOption, this.qParamsFilters).map(response => response.data);
  }

  exportPDF() {
    this.exportRecords.subscribe(records => {
      this._dataTableExport.export({
        records,
        mapOptions: this._dataTableExport.mapFromTableColumns(this.tableColumns),
        fileName: 'all-purchase-orders',
        extension: 'pdf',
        templateData: {
          title: this._translate.instant('ui.purchaseOrder.list.title'),
        },
      });
    });
  }

  exportExcel() {
    this.exportRecords.subscribe(records => {
      this._dataTableExport.export({
        records: records,
        mapOptions: this._dataTableExport.mapFromTableColumns(this.tableColumns),
        fileName: 'all-purchase-orders',
        extension: 'xls',
      });
    });
  }

  loadData() {
    this.elPageLoading.forceShow();
    
    const qOption = this._gridTable.generateApiQueryOptionFromGridInfo(
      this.gridDataSource.pager,
      true,
      this.gridTable,
      this._gridTable.generateCombinedApiFilters(this.combinedFilterValues, this.combinedFilterFields)
    );

    this.populateFilters();

    this.subscriptions.load.forEach(sub => sub.unsubscribe());
    this.subscriptions.load.push(
      this.elRetryDialog.createRetryEntry(
        this._purchasesOrderRest.findAll(qOption, this.qParamsFilters)
          .finally(() => {
            this.elPageLoading.forceHide();
          })
      ).subscribe(response => {
        this.orderes = response.data;
        this.gridDataSource.updateFromApiPaginationResult(response);

        this.compReady = true;
      })
    );
  }

  cloneTransaction(doc) {
    this.elPageLoading.forceShow();

    this._purchasesOrderRest.load(doc.id).subscribe(newDoc => {
      this.elPageLoading.forceHide();

      this._purchasesOrderClone.copy(newDoc);
    })
  }

  copyPurchaseOrdertoInvoice(doc) {
    this.elPageLoading.forceShow();

    this._purchasesOrderRest.load(doc.id).subscribe(newDoc => {
      this.elPageLoading.forceHide();

      this._purchasesOrderCopyInvoice.copy(newDoc);
    });
  }

  quickDepositReset() {
    this.quickDepositDoc = undefined;
    this.elQuickDepositModal.hide();
  }

  onQuickDeposited(deposit: IPurchasesDownPayment) {
    this.quickDepositReset();
    this.loadData();
  }

  public onGridChange() {
    this.gridLoadDataWrapper();
  }

  public onGridPageChange() {
    this.gridLoadDataWrapper();
  }
}

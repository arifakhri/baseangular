import { AutoComplete } from 'primeng/components/autocomplete/autocomplete';
import { Component, EventEmitter, Output, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

import { BusinessPartnerRestService } from '../../business-partner/business-partner-rest.service';
import { CommonService, GridTableFilterService } from '../../../core/core.module';
import { PurchasesOrderMoreFilterService } from './purchases-order-more-filter.service';

@Component({
  selector: 'app-purchases-order-more-filter',
  templateUrl: 'purchases-order-more-filter.component.html',
})
export class PurchasesOrderMoreFilterComponent {
  @Output() success: EventEmitter<any> = new EventEmitter();
  @ViewChild('vendorAC') elVendorAC: AutoComplete;

  vendorsSuggestion: IBusinessPartner[] = [];
  form: FormGroup = new FormGroup({});

  filterMap: IGridTableFilterMap = {
    vendorId: {
      targetFilter: 'param',
    },
    lowDate: {
      targetVar: 'date',
      targetFilter: 'param',
      dateFormat: 'YYYY-MM-DD',
    },
    highDate: {
      targetVar: 'date',
      targetFilter: 'param',
      dateFormat: 'YYYY-MM-DD',
    },
    transactionType: {
      targetFilter: 'param',
    },
  };

  ACVendorHandler = CommonService.remoteACItemsHandler({
    remoteParams: this.ACVendorParams.bind(this),
    remoteRequest: this._businessPartnerRest.findAllVendorsPicker.bind(this._businessPartnerRest),
    remoteRequestMap: (response) => response.data,
    element: () => this.elVendorAC,
  });

  constructor(
    private _businessPartnerRest: BusinessPartnerRestService,
    private _gridTableFilter: GridTableFilterService,
    private _purchasesOrderMoreFilter: PurchasesOrderMoreFilterService,
  ) {
    this.buildForm();

    this.form.patchValue(this._purchasesOrderMoreFilter.lastMoreFilterValues);
  }

  buildForm() {
    this.form.addControl('vendor', new FormControl);
    this.form.addControl('vendorId', new FormControl);
    this.form.addControl('lowDate', new FormControl);
    this.form.addControl('highDate', new FormControl);
    this.form.addControl('transactionType', new FormControl(''));
  }

  ACVendorParams(event: any, type: string) {
    let filters = [];

    if (type === 'search') {
      filters = [{
        filterValues: [{
          field: 'displayName',
          operator: 'contains',
          value: event.query
        }]
      }];
    }

    return [{
      filter: filters,
      sort: [{
        field: 'displayName',
        dir: 'asc'
      }],
      take: 30,
      skip: 0,
      includeTotalCount: false
    }];
  }

  onSubmit() {
    this._purchasesOrderMoreFilter.lastMoreFilterValues = this.form.value;
    this.success.emit(this._gridTableFilter.parse(this.form.value, this.filterMap));
  }
}

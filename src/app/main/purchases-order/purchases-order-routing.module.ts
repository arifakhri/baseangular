import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PurchasesOrderCreateComponent } from './create/purchases-order-create.component';
import { PurchasesOrderDetailComponent } from './detail/purchases-order-detail.component';
import { PurchasesOrderListComponent } from './list/purchases-order-list.component';
import { PurchasesOrderUpdateComponent } from './update/purchases-order-update.component';

import { AuthorizationService } from '../../core/core.module';

export const routes: Routes = [{
  path: '',
  children: [{
    path: '',
    component: PurchasesOrderListComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'purchasesOrder.list',
    },
  }, {
    path: 'create',
    component: PurchasesOrderCreateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'purchasesOrder.create',
    },
  }, {
    path: ':id',
    component: PurchasesOrderDetailComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'purchasesOrder.detail',
    },
  }, {
    path: ':id/update',
    component: PurchasesOrderUpdateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'purchasesOrder.update',
    },
  }]
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class PurchasesOrderRoutingModule { }

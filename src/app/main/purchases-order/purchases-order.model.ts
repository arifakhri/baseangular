export class MPurchasesOrder {
  deliveryDate: string | Date;
  downPaymentAmount: number;
  lines: [
    {
      id: string;
      transactionId: string;
      productId: string;
      productVariantId: string;
      description: string;
      qty: number;
      unitPrice: number;
      discountPercent: number;
      discountAmount: number;
      totalBeforeDiscount: number;
      totalDiscount: number;
      total: number;
      uomId: string;
      uomConversion: number;
      baseQty: number;
      taxId: string;
      taxRate: number;
      taxAmount: number;
      note: string;
      sortOrder: number;
      rowVersion: string;
      product: {
        id: string;
        productType: string;
        code: string;
        name: string;
        categoryId: string;
        sellable: boolean;
        salesTaxId: string;
        salesDescription: string;
        purchasable: boolean;
        purchaseTaxId: string;
        purchaseDescription: string;
        uomId: string;
        variantCount: number;
        inactive: boolean;
        category: {
          id: string;
          name: string;
        };
        salesTax: {
          id: string;
          code: string;
          name: string;
          rate: number;
        };
        purchaseTax: {
          id: string;
          code: string;
          name: string;
          rate: number;
        };
        uom: {
          id: string;
          name: string;
          systemDefault: boolean;
        };
        masterVariant: {
          id: string;
          barcode: string;
          name: string;
          variantName: string;
          unitPrice: number;
          unitCost: number;
        }
      };
      productVariant: {
        id: string;
        productId: string;
        sku: string;
        name: string;
        variantName: string;
        attribute1Value: string;
        attribute2Value: string;
        attribute3Value: string;
        unitPrice: number;
        unitCost: number;
        isMaster: boolean;
        inactive: boolean;
        rowVersion: string;
      };
      uom: {
        id: string;
        name: string;
        systemDefault: boolean;
      };
      tax: {
        id: string;
        code: string;
        name: string;
        rate: number;
      }
    }
  ];
  id: string;
  transactionId: string;
  transactionType: string;
  transactionNumber: string;
  transactionDate: string | Date;
  vendorId: string;
  vendorRefNumber: string;
  vendorEmail: string;
  description: string;
  billingAddress: string;
  shippingAddress: string;
  lineTotalBeforeDiscount: number;
  lineTotalDiscount: number;
  subtotal: number;
  discountPercent: number;
  discountAmount: number;
  taxAmount: number;
  shippingCharge: number;
  adjustmentAmount: number;
  total: number;
  taxed: boolean;
  note: string;
  vendorNote: string;
  externalChannelId: string;
  externalDocumentId: string;
  status: string;
  state: string;
  isOpen: boolean;
  rowVersion: string;
  vendor: {
    salesTaxable: boolean;
    defaultSalesTaxId: string;
    defaultSalesPriceLevelId: string;
    defaultSalesInvoiceDueDays: number;
    purchaseTaxable: boolean;
    defaultPurchaseTaxId: string;
    defaultPurchaseInvoiceDueDays: number;
    isCustomer: boolean;
    isVendor: boolean;
    defaultSalesTax: {
      id: string;
      code: string;
      name: string;
      rate: number;
    };
    defaultSalesPriceLevel: {
      id: string;
      name: string;
      isMaster: boolean;
    };
    defaultPurchaseTax: {
      id: string;
      code: string;
      name: string;
      rate: number;
    };
    id: string;
    contactType: string;
    code: string;
    displayName: string;
    company: string;
    fullName: string;
    phone: string;
    mobile: string;
    fax: string;
    email: string;
    website: string;
    inactive: boolean;
    postalAddress: {
      attention: string;
      street1: string;
      street2: string;
      city: string;
      stateProvince: string;
      countryId: string;
      postalCode: string;
      longitude: number;
      latitude: number;
      country: {
        code: string;
        name: string;
      }
    }
  };
}

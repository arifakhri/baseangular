import { Injectable } from '@angular/core';

import { ReportsRestService } from '../reports/reports-rest.service';

@Injectable()
export class ReportsSalesDetailRestService {
  constructor(
    private _reportsRest: ReportsRestService,
  ) { }

  findAll(reportParams: any = {}) {
    return this._reportsRest.requestReport.post<IApiPaginationResult<IReportsSalesDetail>>(`sales-list-detail`, reportParams);
  }
}

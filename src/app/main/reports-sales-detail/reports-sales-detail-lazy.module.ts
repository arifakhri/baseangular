import { NgModule } from '@angular/core';

import { ReportsSalesDetailModule } from './reports-sales-detail.module';
import { ReportsSalesDetailRoutingModule } from './reports-sales-detail-routing.module';

@NgModule({
  imports: [
    ReportsSalesDetailModule,
    ReportsSalesDetailRoutingModule,
  ],
})
export class ReportsSalesDetailLazyModule { }

declare interface IReportsSalesDetail {
  accountClass: string;
  accountType: string;
  accountName: string;
  total: 0;
}
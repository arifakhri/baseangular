import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ReportsSalesDetailPreviewComponent } from './preview/reports-sales-detail-preview.component';

import { AuthorizationService } from '../../core/core.module';

const routes: Routes = [{
  path: '',
  children: [{
    path: '',
    component: ReportsSalesDetailPreviewComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'reportsSalesDetail.preview'
    }
  }]
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ],
})
export class ReportsSalesDetailRoutingModule { }

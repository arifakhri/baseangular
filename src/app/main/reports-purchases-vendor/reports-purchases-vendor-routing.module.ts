import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ReportsPurchasesVendorPreviewComponent } from './preview/reports-purchases-vendor-preview.component';

import { AuthorizationService } from '../../core/core.module';

const routes: Routes = [{
  path: '',
  children: [{
    path: '',
    component: ReportsPurchasesVendorPreviewComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'reportsPurchasesVendor.preview'
    }
  }]
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ],
})
export class ReportsPurchasesVendorRoutingModule { }

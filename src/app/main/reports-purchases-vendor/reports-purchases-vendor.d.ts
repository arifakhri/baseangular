declare interface IReportsPurchasesVendor {
  accountClass: string;
  accountType: string;
  accountName: string;
  total: 0;
}
import { Injectable } from '@angular/core';

import { ReportsRestService } from '../reports/reports-rest.service';

@Injectable()
export class ReportsPurchasesVendorRestService {
  constructor(
    private _reportsRestService: ReportsRestService,
  ) { }

  findAll(queryParams: any = {}) {
    return this._reportsRestService.requestReport.post<IApiPaginationResult<IReportsPurchasesVendor>>(
      `purchase-by-vendor-summary`,
      queryParams
    );
  }
}

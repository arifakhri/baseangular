import { NgModule } from '@angular/core';

import { ReportsPurchasesVendorModule } from './reports-purchases-vendor.module';
import { ReportsPurchasesVendorRoutingModule } from './reports-purchases-vendor-routing.module';

@NgModule({
  imports: [
    ReportsPurchasesVendorModule,
    ReportsPurchasesVendorRoutingModule,
  ],
})
export class ReportsPurchasesVendorLazyModule { }

import { AutoCompleteModule } from 'primeng/components/autocomplete/autocomplete';
import { BsDropdownModule, ModalModule, PaginationModule, PopoverModule } from 'ngx-bootstrap';
import { CalendarModule } from 'primeng/components/calendar/calendar';
import { CheckboxModule } from 'primeng/components/checkbox/checkbox';
import { CommonModule } from '@angular/common';
import { DataTableModule } from 'primeng/components/datatable/datatable';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { ListboxModule } from 'primeng/components/listbox/listbox';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { SalesAllComponent } from './all/sales-all.component';
import { SalesButtonAddComponent } from './button-add/sales-button-add.component';
import { SalesFindInvoiceByNumberComponent } from './find-invoice-by-number/sales-find-invoice-by-number.component';
import { SalesMoreFilterComponent } from './more-filter/sales-more-filter.component';
import { SalesTabLinksComponent } from './tab-links/sales-tab-links.component';
import { SalesParseComponent } from './parse/sales-parse.component';
import { SalesParseStep1Component } from './parse/step1/sales-parse-step1.component';
import { SalesParseStep2Component } from './parse/step2/sales-parse-step2.component';

import { SalesMoreFilterService } from './more-filter/sales-more-filter.service';
import { SalesRestService } from './sales-rest.service';
import { SalesService } from './sales.service';

import { BusinessPartnerModule } from '../business-partner/business-partner.module';
import { CoreModule } from '../../core/core.module';
import { ProductVariantModule } from '../product-variant/product-variant.module';
import { SharedModule } from '../../shared/shared.module';

import * as SalesParseFormStepStore from './parse/sales-parse-form-step.store';
import * as SalesParseFormStateStore from './parse/sales-parse-form-state.store';

export const STORES = {};
STORES[SalesParseFormStepStore.STORE_ID] = SalesParseFormStepStore.STORE;
STORES[SalesParseFormStateStore.STORE_ID] = SalesParseFormStateStore.STORE;

export const PROVIDERS = [
  SalesMoreFilterService,
  SalesRestService,
  SalesService,
];

@NgModule({
  imports: [
    AutoCompleteModule,
    BsDropdownModule,
    BusinessPartnerModule,
    CalendarModule,
    CheckboxModule,
    CommonModule,
    CoreModule,
    DataTableModule,
    FlexLayoutModule,
    FormsModule,
    HttpModule,
    ListboxModule,
    ModalModule,
    PaginationModule,
    PopoverModule,
    ProductVariantModule,
    ReactiveFormsModule,
    RouterModule,
    SharedModule,
    TranslateModule,
  ],
  declarations: [
    SalesAllComponent,
    SalesButtonAddComponent,
    SalesFindInvoiceByNumberComponent,
    SalesMoreFilterComponent,
    SalesParseComponent,
    SalesParseStep1Component,
    SalesParseStep2Component,
    SalesTabLinksComponent,
  ],
  exports: [
    SalesButtonAddComponent,
    SalesFindInvoiceByNumberComponent,
    SalesParseComponent,
    SalesParseStep1Component,
    SalesParseStep2Component,
    SalesTabLinksComponent
  ],
})
export class SalesModule { }

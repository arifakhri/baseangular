import Axios, { AxiosInstance, AxiosResponse } from 'axios';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';

import { ApiQueryOption, AuthenticationService } from '../../core/core.module';
import { ProductVariantRestService } from '../product-variant/product-variant-rest.service';

import { APP_CONST } from '../../app.const';

@Injectable()
export class SalesRestService {
  axios: AxiosInstance;
  axiosCreditAllocation: AxiosInstance;
  axiosOrderParse: AxiosInstance;

  baseURL = `${APP_CONST.API_MAIN}/sales`;
  baseURLCreditAllocation = `${APP_CONST.API_MAIN}/sales/credit-allocations`;
  baseURLParse = `${APP_CONST.API_MAIN}/sales-order-parsing`;

  constructor(
    private _auth: AuthenticationService,
    public _productVariantRest: ProductVariantRestService,
  ) {
    this.axios = Axios.create();
    this.axios.defaults.baseURL = this.baseURL;
    _auth.axiosInterceptors(this.axios);

    this.axiosOrderParse = Axios.create();
    this.axiosOrderParse.defaults.baseURL = this.baseURLParse;
    _auth.axiosInterceptors(this.axiosOrderParse);

    this.axiosCreditAllocation = Axios.create();
    this.axiosCreditAllocation.defaults.baseURL = this.baseURLCreditAllocation;
    _auth.axiosInterceptors(this.axiosCreditAllocation);
  }

  findAll(queryOption: ApiQueryOption, queryParams: any = {}): Observable<IApiPaginationResult<ISalesInvoice>> {
    return Observable.fromPromise(
      <Promise<AxiosResponse>>this.axios.post(`all/q`, queryOption, {
        params: queryParams,
      })
    ).map(response => response.data);
  }

  findAllCreditAllocations(
    transactionType: string,
    transactionId: string,
    customerId?: string,
  ): Observable<IApiPaginationResult<ISalesAllocatedCredit>> {
    return Observable.fromPromise(
      <Promise<AxiosResponse>>this.axiosCreditAllocation.get('', {
        params: {
          targetTransactionType: transactionType,
          targetTransactionId: transactionId,
          customerId,
          options: {
            take: 100,
          },
        }
      })
    ).map(response => response.data);
  }

  findAllProducts(queryOption: ApiQueryOption = new ApiQueryOption): Observable<IApiPaginationResult<IProductVariant>> {
    return this._productVariantRest.findAll(queryOption);
  }

  getTransactionPaymentsAndCredits(
    transactionType: string,
    transactionId: string,
  ): Observable<IApiPaginationResult<ISalesPaymentAndCredit>> {
    return Observable.fromPromise(
      <Promise<AxiosResponse>>this.axios.get(`payments-and-credits/${transactionType}/${transactionId}`)
    ).map(response => response.data);
  }

  validateParsingData(customQueryOption: {
    customerName: string;
    customerEmail: string;
    customerMobile: string;
    products: string[];
  }) {
    return Observable.fromPromise(
      <Promise<AxiosResponse>>this.axiosOrderParse.post(`match-data`, customQueryOption)
    ).map(response => <{
      customers: [
        {
          id: string,
          displayName: string,
          email: string,
          mobile: string
        }
      ],
      lines: [
        {
          parseText: string,
          productId: string,
          productVariantId: string,
          sku: string,
          name: string,
          salesDescription: string
        }
      ]
    }>response.data);
  }
}

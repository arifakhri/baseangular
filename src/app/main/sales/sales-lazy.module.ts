import { NgModule } from '@angular/core';

import { SalesModule } from './sales.module';
import { SalesRoutingModule } from './sales-routing.module';

@NgModule({
  imports: [
    SalesModule,
    SalesRoutingModule
  ]
})
export class SalesLazyModule { }

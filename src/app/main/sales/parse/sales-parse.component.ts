import * as _ from 'lodash';
import { ChangeDetectorRef, Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ModalDirective } from 'ngx-bootstrap';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';
import { Store } from '@ngrx/store';

import { BusinessPartnerRestService } from '../../business-partner/business-partner-rest.service';
import * as SalesParseFormStepStore from './sales-parse-form-step.store';
import * as SalesParseFormStateStore from './sales-parse-form-state.store';
import { SalesService } from '../sales.service';
import { SalesRestService } from '../sales-rest.service';

@Component({
  selector: 'app-sales-parse',
  templateUrl: 'sales-parse.component.html'
})
export class SalesParseComponent implements OnChanges, OnInit {
  @Input() show: boolean = false;
  @Output() onFinished: EventEmitter<any> = new EventEmitter();
  @Output() close: EventEmitter<boolean> = new EventEmitter();
  @ViewChild('salesParseModal') elSalesParseModal: ModalDirective;

  form: FormGroup;

  formStep$: Observable<number>;
  formState$: Observable<any>;

  formStep: number;
  formState: any;
  formProcessing: boolean = false;

  finished: boolean = false;
  maxStep: number = 2;

  results: any = {};

  constructor(
    private _businessPartnerRest: BusinessPartnerRestService,
    private _changeDetectorRef: ChangeDetectorRef,
    private _sales: SalesService,
    private _salesRest: SalesRestService,
    private _store: Store<any>,
  ) {
    this.buildForm();

    this.formStep$ = _store.select(SalesParseFormStepStore.STORE_ID) as any;
    this.formState$ = _store.select(SalesParseFormStateStore.STORE_ID) as any;

    this.formStep$.subscribe(formStep => this.formStep = formStep);
    this.formState$.subscribe(formState => {
      return this.formState = formState;
    });
  }

  return() {
    this.results.states = this.formState;
    this.onFinished.emit(this.results);
    this.elSalesParseModal.hide();
    this.finished = true;
  }

  buildForm() {
    this.form = new FormGroup({});
  }

  ngOnInit() {
    this.elSalesParseModal.onHidden.subscribe(() => {
      this.close.emit(true);
      this.reset();
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (_.has(changes, 'show')) {
      if (this.show && !this.elSalesParseModal.isShown) {
        this.elSalesParseModal.show();
      } else if (!this.show && this.elSalesParseModal.isShown) {
        this.elSalesParseModal.hide();
      }
    }
  }

  async formNext() {
    if (!this.form.valid) {
      return;
    }

    this.formProcessing = true;

    this._store.dispatch({
      type: SalesParseFormStateStore.ACTIONS.SET, payload: {
        key: this.formStep,
        value: this.form.value
      }
    });

    if (this.formStep === 1) {
      await this.form1Process(this.form.value);
    }

    if (this.formStep === 2) {
      await this.form2Process(this.form.value);
    }

    if (this.finished) {
      this.finished = false;
      return;
    }

    this._store.dispatch({ type: SalesParseFormStepStore.ACTIONS.NEXT });

    this.buildForm();
    this._changeDetectorRef.detectChanges();
    this.form.patchValue(_.get(this.formState, this.formStep, {}));

    this.formProcessing = false;
  }

  formPrev() {
    this._store.dispatch({ type: SalesParseFormStepStore.ACTIONS.PREV });

    this.buildForm();
    this._changeDetectorRef.detectChanges();
    this.form.patchValue(_.get(this.formState, this.formStep, {}));
  }

  reset() {
    this._store.dispatch({ type: SalesParseFormStateStore.ACTIONS.RESET });
    this._store.dispatch({ type: SalesParseFormStepStore.ACTIONS.RESET });
    this.formProcessing = false;
    this.buildForm();
  }

  async form1Process(formValue: any) {
    const parsedData: any = this._sales.parseOrderText(formValue.raw);

    const { customers, lines } = await this._salesRest.validateParsingData({
      ...parsedData,
      lines: parsedData.lines.map(p => p.productName),
    }).toPromise();

    this._store.dispatch({
      type: SalesParseFormStateStore.ACTIONS.SETATTR,
      payload: { key: 1, attr: 'parsedData', value: parsedData }
    });

    if (customers.length === 1 && _.filter(lines, (p) => !(<any>p).productVariant).length === 0) {
      this.results = Object.assign(
        {}, {
          customer: _.first(customers),
          lines: _(lines).map(p => {
            return {
              productVariant: (<any>p).productVariant,
              ignore: false,
            };
          }),
        });
      this.return();
    } else {
      this._store.dispatch({
        type: SalesParseFormStateStore.ACTIONS.SETATTR,
        payload: { key: 1, attr: 'customers', value: customers }
      });
      this._store.dispatch({
        type: SalesParseFormStateStore.ACTIONS.SETATTR,
        payload: { key: 1, attr: 'lines', value: lines }
      });
    }
  }

  async form2Process(formValue: any) {
    if (!formValue.customer) {
      const targetCustomer: any = {
        isCustomer: true,
        isVendor: false,
      };
      targetCustomer.displayName = this.formState[1].parsedData.customerName;

      if (this.formState[1].parsedData.customerEmail) {
        targetCustomer.email = this.formState[1].parsedData.customerEmail;
      }
      if (this.formState[1].parsedData.customerMobile) {
        targetCustomer.mobile = this.formState[1].parsedData.customerMobile;
      }

      formValue.customer = await this._businessPartnerRest.create(targetCustomer).toPromise();
    }
    this.results = Object.assign({}, formValue);
    this.return();
  }
}

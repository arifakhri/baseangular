import { Component, Input, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';

import { CommonService } from '../../../../core/core.module';
import { SalesRestService } from '../../sales-rest.service';

@Component({
  selector: 'app-sales-parse-step2',
  templateUrl: 'sales-parse-step2.component.html'
})
export class SalesParseStep2Component implements OnInit {
  @Input() form: FormGroup;
  @Input() formState: any;

  productVariantsSuggestion: IProductVariant[] = [];

  ACProductVariantHandler = CommonService.remoteACItemsHandler({
    remoteParams: this.ACProductVariantParams.bind(this),
    remoteRequest: this._salesRest.findAllProducts.bind(this._salesRest),
    remoteRequestMap: (response) => response.data,
  });

  constructor(
    private _salesRest: SalesRestService
  ) { }

  ngOnInit() {
    this.form.addControl('lines', new FormArray([], Validators.required));
    this.form.addControl('customer', new FormControl(''));

    this.form.get('customer').setValue(this.formState[1].customers[0]);

    this.formState[1].lines.forEach((productQueryResult, idx) => {
      const productControl = new FormControl(productQueryResult.parseText, Validators.required);
      if (productQueryResult.productVariant) {
        productControl.setValue(productQueryResult.productVariant);
      }

      const ignoreControl = new FormControl(!productQueryResult.productVariant);
      ignoreControl.valueChanges.subscribe(checked => {
        if (checked) {
          productControl.setValue(productQueryResult.parseText);
        } else {
          productControl.setValue(null);
        }
      });

      const lineFormGroup = new FormGroup({
        ignore: ignoreControl,
        productVariant: productControl,
      });

      (<FormArray>this.form.get('lines')).push(lineFormGroup);
    });
  }

  ACProductVariantParams(event: any, type: string) {
    let filters = [];

    if (type === 'search') {
      filters = [{
        filterValues: [{
          field: 'name',
          operator: 'contains',
          value: event.query
        }, {
          field: 'sku',
          operator: 'contains',
          value: event.query
        }]
      }];
    }

    return [{
      filter: filters,
      sort: [{
        field: 'name',
        dir: 'asc'
      }],
      take: 30,
      skip: 0,
      includeTotalCount: false
    }];
  }
}

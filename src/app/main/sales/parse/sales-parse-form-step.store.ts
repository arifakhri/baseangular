export const ACTIONS = {
  NEXT: 'SPFormStepNext',
  PREV: 'SPFormStepPrev',
  RESET: 'SPFormStepReset',
};

export const STORE_ID = 'SalesParseFormStepStore';

export function STORE(step: number = 1, action: any) {
  switch (action.type) {
    case ACTIONS.NEXT:
      return step + 1;
    case ACTIONS.PREV:
      return step - 1;
    case ACTIONS.RESET:
      return 1;
    default:
      return step;
  }
}

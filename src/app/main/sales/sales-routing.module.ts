import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SalesAllComponent } from './all/sales-all.component';

import { AuthorizationService } from '../../core/core.module';

export const routes: Routes = [{
  path: '',
  children: [{
    path: '',
    redirectTo: 'all',
    pathMatch: 'full',
  }, {
    path: 'all',
    component: SalesAllComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'sales.all'
    }
  }, {
    path: 'orders',
    loadChildren: '../sales-order/sales-order-lazy.module#SalesOrderLazyModule'
  }, {
    path: 'credit-notes',
    loadChildren: '../sales-credit-note/sales-credit-note-lazy.module#SalesCreditNoteLazyModule'
  }, {
    path: 'invoices',
    loadChildren: '../sales-invoice/sales-invoice-lazy.module#SalesInvoiceLazyModule'
  }, {
    path: 'payments',
    loadChildren: '../sales-payment/sales-payment-lazy.module#SalesPaymentLazyModule'
  }, {
    path: 'down-payments',
    loadChildren: '../sales-down-payment/sales-down-payment-lazy.module#SalesDownPaymentLazyModule'
  }, {
    path: 'receipts',
    loadChildren: '../sales-receipt/sales-receipt-lazy.module#SalesReceiptLazyModule'
  }, {
    path: 'refunds',
    loadChildren: '../sales-refund/sales-refund-lazy.module#SalesRefundLazyModule'
  }]
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class SalesRoutingModule { }

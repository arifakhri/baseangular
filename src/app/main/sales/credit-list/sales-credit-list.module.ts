import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

import { SalesCreditListComponent } from './sales-credit-list.component';

import { CoreModule } from '../../../core/core.module';
import { SharedModule } from '../../../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    CoreModule,
    SharedModule,
    TranslateModule,
  ],
  declarations: [
    SalesCreditListComponent,
  ],
  exports: [
    SalesCreditListComponent,
  ],
})
export class SalesCreditListModule { }

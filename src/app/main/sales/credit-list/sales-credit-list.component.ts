import * as _ from 'lodash';
import { Component, Input, OnInit } from '@angular/core';

import { AccountingService, } from '../../../core/core.module';
import { SalesRestService } from '../sales-rest.service';

@Component({
  selector: 'app-sales-credit-list',
  templateUrl: './sales-credit-list.component.html',
})
export class SalesCreditListComponent implements OnInit {
  @Input() transaction: any;

  credits: any;
  total: number = 0;

  constructor(
    public _accounting: AccountingService,
    private _salesRest: SalesRestService,
  ) { }

  ngOnInit() {
    this._salesRest.findAllCreditAllocations(
      this.transaction.transactionType,
      this.transaction.id,
    ).subscribe(credits => {
      this.credits = credits.data;
      this.total = _.sumBy(this.credits, 'total');
    });
  }
}

import { AutoComplete } from 'primeng/components/autocomplete/autocomplete';
import { Component, EventEmitter, Output, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

import { BusinessPartnerRestService } from '../../business-partner/business-partner-rest.service';
import { CommonService, GridTableFilterService } from '../../../core/core.module';
import { SalesMoreFilterService } from './sales-more-filter.service';

@Component({
  selector: 'app-sales-more-filter',
  templateUrl: 'sales-more-filter.component.html',
})
export class SalesMoreFilterComponent {
  @Output() success: EventEmitter<any> = new EventEmitter();
  @ViewChild('customerAC') elCustomerAC: AutoComplete;

  customersSuggestion: IBusinessPartner[] = [];
  form: FormGroup = new FormGroup({});

  filterMap: IGridTableFilterMap = {
    customerId: {
      targetFilter: 'param',
    },
    lowDate: {
      targetVar: 'date',
      targetFilter: 'param',
      dateFormat: 'YYYY-MM-DD',
    },
    highDate: {
      targetVar: 'date',
      targetFilter: 'param',
      dateFormat: 'YYYY-MM-DD',
    },
    transactionType: {
      targetFilter: 'param',
    },
  };

  ACCustomerHandler = CommonService.remoteACItemsHandler({
    remoteParams: this.ACCustomerParams.bind(this),
    remoteRequest: this._businessPartnerRest.findAllCustomersPicker.bind(this._businessPartnerRest),
    remoteRequestMap: (response) => response.data,
    element: () => this.elCustomerAC,
  });

  constructor(
    private _businessPartnerRest: BusinessPartnerRestService,
    private _gridTableFilter: GridTableFilterService,
    private _salesMoreFilter: SalesMoreFilterService,
  ) {
    this.buildForm();

    this.form.patchValue(this._salesMoreFilter.lastMoreFilterValues);
  }

  buildForm() {
    this.form.addControl('customer', new FormControl);
    this.form.addControl('customerId', new FormControl);
    this.form.addControl('lowDate', new FormControl);
    this.form.addControl('highDate', new FormControl);
    this.form.addControl('transactionType', new FormControl(''));
  }

  ACCustomerParams(event: any, type: string) {
    let filters = [];

    if (type === 'search') {
      filters = [{
        filterValues: [{
          field: 'displayName',
          operator: 'contains',
          value: event.query
        }]
      }];
    }

    return [{
      filter: filters,
      sort: [{
        field: 'displayName',
        dir: 'asc'
      }],
      take: 30,
      skip: 0,
      includeTotalCount: false
    }];
  }

  onSubmit() {
    this._salesMoreFilter.lastMoreFilterValues = this.form.value;
    this.success.emit(this._gridTableFilter.parse(this.form.value, this.filterMap));
  }
}

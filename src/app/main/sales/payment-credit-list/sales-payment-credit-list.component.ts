import * as _ from 'lodash';
import { Component, Input, OnInit } from '@angular/core';

import { AccountingService, } from '../../../core/core.module';
import { SalesRestService } from '../sales-rest.service';

@Component({
  selector: 'app-sales-payment-credit-list',
  templateUrl: './sales-payment-credit-list.component.html',
})
export class SalesPaymentCreditListComponent implements OnInit {
  @Input() transaction: any;

  paymentAndCredits: any;
  total: number = 0;
  loading: boolean = false;

  constructor(
    public _accounting: AccountingService,
    private _salesRest: SalesRestService,
  ) { }

  ngOnInit() {
    this.loading = true;
    this._salesRest.getTransactionPaymentsAndCredits(
      this.transaction.transactionType,
      this.transaction.id,
    ).subscribe(
      response => {
        this.loading = false;
        this.paymentAndCredits = response;
        this.total = _.sumBy(this.paymentAndCredits, 'amount');
      },
      error => {
        this.loading = false;
      });
  }
}

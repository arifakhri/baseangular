import * as _ from 'lodash';
import { Component, EventEmitter, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';

import { SalesInvoiceRestService } from '../../sales-invoice/sales-invoice-rest.service';

@Component({
  selector: 'app-sales-find-invoice-by-number',
  templateUrl: 'sales-find-invoice-by-number.component.html',
})
export class SalesFindInvoiceByNumberComponent {
  @Output() cancel: EventEmitter<boolean> = new EventEmitter;
  @Output() success: EventEmitter<ISalesInvoice> = new EventEmitter;

  form: FormGroup = new FormGroup({});

  constructor(
    private _salesInvoiceRest: SalesInvoiceRestService,
  ) {
    this.form.addControl('transactionNumber', new FormControl('', Validators.required));
  }

  onSubmit() {
    if (this.form.valid) {
      this.findInvoice()
        .catch(obsError => {
          this.invoiceNotFound();
          return Observable.throw(obsError);
        })
        .subscribe(invoice => {
          if (!invoice) {
            this.invoiceNotFound();
          } else {
            this.success.emit(invoice);
          }
        });
    }
  }

  findInvoice() {
    const { transactionNumber } = this.form.value;

    return this._salesInvoiceRest.findAll({
      filter: [{
        filterValues: [{
          field: 'transactionNumber',
          operator: 'eq',
          value: transactionNumber,
        }]
      }],
      skip: 0,
      includeTotalCount: false,
      take: 1,
    }).map(records => _.first(records.data));
  }

  invoiceNotFound() {
    this.form.get('transactionNumber').setErrors({
      recordNotFound: true,
    });
  }
}

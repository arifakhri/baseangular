import * as _ from 'lodash';
import * as SnackBar from 'node-snackbar';
import swal from 'sweetalert2';
import { ActivatedRoute } from '@angular/router';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { DataTable } from 'primeng/components/datatable/datatable';
import { PopoverDirective } from 'ngx-bootstrap';
import { Observable, Subscription } from 'rxjs';
import 'app/rxjs-imports.ts';
import { TranslateService } from '@ngx-translate/core';

import { SpinnerDirective } from '../../../shared/spinner/spinner.directive';

import {
  AccountingService,
  ExportDataTableService,
  GridTableDataSource,
  GridTableService,
  GridTableToggleService,
  SystemMessageService,
} from '../../../core/core.module';
import { AdminLayoutService } from '../../../layouts/layouts.module';
import { SalesRestService } from '../sales-rest.service';
import { SalesCreditNoteRestService } from '../../sales-credit-note/sales-credit-note-rest.service';
import { SalesDownPaymentRestService } from '../../sales-down-payment/sales-down-payment-rest.service';
import { SalesInvoiceRestService } from '../../sales-invoice/sales-invoice-rest.service';
import { SalesMoreFilterService } from '../more-filter/sales-more-filter.service';
import { SalesOrderRestService } from '../../sales-order/sales-order-rest.service';
import { SalesPaymentRestService } from '../../sales-payment/sales-payment-rest.service';
import { SalesReceiptRestService } from '../../sales-receipt/sales-receipt-rest.service';
import { TransactionRoutingService } from '../../transaction/transaction-routing.service';
import { RetryDialogComponent } from '../../../shared/retry-dialog/retry-dialog.component';

@Component({
  selector: 'app-sales-all',
  templateUrl: 'sales-all.component.html',
  providers: [SystemMessageService]
})
export class SalesAllComponent implements OnInit {
  @ViewChild('gridTable') public gridTable: DataTable;
  @ViewChild('pageLoading') elPageLoading: SpinnerDirective;
  @ViewChild('popMoreFilter') elPopMoreFilter: PopoverDirective;
  @ViewChild('tableColumn2') elTableColumn2: ElementRef;
  @ViewChild('retryDialog') elRetryDialog: RetryDialogComponent;

  compReady: boolean;
  gridDataSource: GridTableDataSource<ISales> = new GridTableDataSource<ISales>();
  selectedRecords: ISales[] = [];

  combinedFilterValues: string = '';
  combinedFilterFields = {
    transactionNumber: true,
    'customer.displayName': true,
  };

  qParamsFilters: any = {
    keyword: null
  };
  moreFilterValues: IGridTableFilterParsed;

  retryDialogShownState: boolean = false;
  retryDialogOnRetry: Function;

  tableColumns: IGridTableColumn[] = [];
  tableColumnsShow: boolean = false;
  tableColumnsToggle: any;

  private loadDataTimeoutId: number;
  private gridFirstLoad: boolean = true;
  private subscriptions: { [key: string]: Subscription[] } = {
    load: []
  };

  constructor(
    public _accounting: AccountingService,
    private _activatedRoute: ActivatedRoute,
    private _adminLayout: AdminLayoutService,
    private _dataTableExport: ExportDataTableService,
    private _gridTableToggle: GridTableToggleService,
    private _gridTable: GridTableService,
    private _salesCreditNoteRest: SalesCreditNoteRestService,
    private _salesDownPaymentRest: SalesDownPaymentRestService,
    private _salesInvoiceRest: SalesInvoiceRestService,
    private _salesMoreFilter: SalesMoreFilterService,
    private _salesOrderRest: SalesOrderRestService,
    private _salesPaymentRest: SalesPaymentRestService,
    private _salesReceiptRest: SalesReceiptRestService,
    private _salesRest: SalesRestService,
    public _systemMessage: SystemMessageService,
    private _translate: TranslateService,
    private _transactionRoutingService: TransactionRoutingService
  ) {
    this._adminLayout.containerType = 1;
  }

  ngOnInit() {
    this.loadData();

    this.tableColumns = [{
      i18nLabel: 'ui.sales.all.column.transactionDate',
      field: 'transactionDate',
      dateFormat: 'DD/MM/YYYY',
      sort: true,
    }, {
      i18nLabel: 'ui.sales.all.column.transactionType',
      field: 'transactionType',
      sort: true,
      formatter: (value, row: ISales) => {
        return _.startCase(value);
      },
    }, {
      i18nLabel: 'ui.sales.all.column.partner',
      field: '',
      template: this.elTableColumn2,
    }, {
      i18nLabel: 'ui.sales.all.column.transactionNumber',
      field: 'transactionNumber',
      link: (row: ISales) => {
        const type = _.kebabCase(row.transactionType.substr(row.transactionType.indexOf('_') + 1));
        return `/sales/${type}s/${row.transactionId}`;
      },
      sort: true,
    }, {
      i18nLabel: 'ui.sales.all.column.customerName',
      field: 'customer.displayName',
      sort: true,
      formatter: (value, row: ISales) => {
        return row.customCustomerName ? value + ' (' + row.customCustomerName + ')' : value;
      },
    }, {
      i18nLabel: 'ui.sales.all.column.total',
      field: 'total',
      sortField: 'amount',
      sort: true,
      formatter: (value, row: ISales) => {
        return this._accounting.ac.formatMoney(value, '');
      },
      columnClasses: 'right-align',
    }, {
      i18nLabel: 'ui.sales.all.column.status',
      field: 'status',
      sort: true,
      formatter: (value, row: ISales) => {
        return _.startCase(value);
      },
    }];
    this.tableColumnsToggle = this._gridTableToggle.mapToggleOptionsFromColumns(this.tableColumns);
  }

  getUpdateLink(doc: any) {
    return this._transactionRoutingService.resolve(doc, 'update');
  }

  gridLoadDataWrapper() {
    // Grid always load at the first time. we dont want that. we already call on ngOnInit
    if (this.gridFirstLoad) {
      this.gridFirstLoad = false;
      return;
    }

    if (this.loadDataTimeoutId) {
      window.clearTimeout(this.loadDataTimeoutId);
      this.loadDataTimeoutId = null;
    }

    this.loadDataTimeoutId = window.setTimeout(() => {
      this.loadData();
    }, 300);
  }

  populateFilters() {
    let newFilters: any = _.pick(this.qParamsFilters, ['keyword']);

    if (this.moreFilterValues) {
      newFilters = Object.assign({}, newFilters, this.moreFilterValues.qParams);
    }

    this.qParamsFilters = newFilters;
  }

  showVoidDialog(salesId: string, salesType: string) {
    const type = _.camelCase(salesType);
    swal({
      title: this._translate.instant(`confirm.${type}.void.label`),
      text: this._translate.instant(`confirm.${type}.void.description`),
      type: 'question',
      showCancelButton: true,
    })
      .then(() => {
        this.voidSales(salesId, type);
      })
      .catch(() => { });
  }

  voidSales(salesId: string, type: string) {
    const restType = `_${type}Rest`;
    this[restType].void(salesId)
    .catch((error) => {
      this._systemMessage.log({
        message: error.response.data[0].errorMessage,
        type: 'error'
      });
      return Observable.throw(error);
    })
    .subscribe(response => {
      this.gridLoadDataWrapper();
      SnackBar.show({
        text: this._translate.instant(`success.${type}.void`),
        pos: 'bottom-right'
      });
    });
  }

  onMoreFilter(filters: IGridTableFilterParsed) {
    this.elPopMoreFilter.hide();

    this.moreFilterValues = filters;

    this.loadData();
  }

  get exportRecords(): Observable<ISalesInvoice[]> {
    const qOption = {
      ...this._gridTable.generateApiQueryOptionFromGridInfo(
        this.gridDataSource.pager,
        false,
        this.gridTable,
        this._gridTable.generateCombinedApiFilters(this.combinedFilterValues, this.combinedFilterFields)
      ),
      skip: 0,
      take: this._dataTableExport.exportLimit,
    };

    return this._salesRest.findAll(qOption).map(response => response.data);
  }

  exportPDF() {
    this.exportRecords.subscribe(records => {
      this._dataTableExport.export({
        records,
        mapOptions: this._dataTableExport.mapFromTableColumns(this.tableColumns),
        fileName: 'all-transactiones',
        extension: 'pdf',
        templateData: {
          title: this._translate.instant('ui.transaction.list.title'),
        },
      });
    });
  }

  exportExcel() {
    this.exportRecords.subscribe(records => {
      this._dataTableExport.export({
        records: records,
        mapOptions: this._dataTableExport.mapFromTableColumns(this.tableColumns),
        fileName: 'all-transactiones',
        extension: 'xls',
      });
    });
  }

  loadData() {
    this.elPageLoading.forceShow();

    const qOption = this._gridTable.generateApiQueryOptionFromGridInfo(
      this.gridDataSource.pager,
      true,
      this.gridTable,
      this._gridTable.generateCombinedApiFilters(this.combinedFilterValues, this.combinedFilterFields)
    );

    this.populateFilters();

    this.subscriptions.load.forEach(sub => sub.unsubscribe());
    this.subscriptions.load.push(
      this.elRetryDialog.createRetryEntry(
        this._salesRest.findAll(qOption, this.qParamsFilters)
          .finally(() => {
            this.elPageLoading.forceHide();
          })
      ).subscribe(response => {
        this.gridDataSource.updateFromApiPaginationResult(<any>response);
        this.compReady = true;
      })
    );
  }

  public onGridChange() {
    this.gridLoadDataWrapper();
  }

  public onGridPageChange() {
    this.gridLoadDataWrapper();
  }
}

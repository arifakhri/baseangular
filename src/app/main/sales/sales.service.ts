import * as _ from 'lodash';
import { Injectable } from '@angular/core';

import { TextParseService } from '../../core/core.module';

@Injectable()
export class SalesService {
  private parserOptions: IParseMap = {
    explodeOperator: '\n',
    fields: {
      customerName: {
        patterns: [
          /^(Nama)\s*?:/i,
        ]
      },
      customerEmail: {
        patterns: [
          /^(Email)\s*?:/i,
        ]
      },
      customerMobile: {
        patterns: [
          /^(No HP|No\. HP|No\.HP)\s*?:/i,
        ]
      },
      shippingAddress: {
        patterns: [
          /^(Alamat)\s*?:/i,
        ]
      },
      lines: {
        mapLine: (line) => {
          if (!line.length) {
            return;
          }
          line = line.replace(/^(\s*\-?\s*)/, '');
          const productName = line.replace(/^(\d+\sx\s)/, '');
          const qty = _.first(line.replace(productName, '').match(/^(\d+)/)) || 1;
          return { productName, qty };
        },
        patterns: [
          /^(Orderan)\s*?:/i
        ]
      }
    }
  };

  constructor(
    private _textParse: TextParseService,
  ) { }

  parseOrderText(content: string) {
    return this._textParse.parse(content, this.parserOptions);
  }
}

import { AutoCompleteModule } from 'primeng/components/autocomplete/autocomplete';
import { BsDropdownModule, ModalModule, PaginationModule } from 'ngx-bootstrap';
import { CalendarModule } from 'primeng/components/calendar/calendar';
import { CommonModule } from '@angular/common';
import { DataTableModule } from 'primeng/components/datatable/datatable';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ListboxModule } from 'primeng/components/listbox/listbox';
import { HttpModule } from '@angular/http';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { SpendMoneyCreateComponent } from './create/spend-money-create.component';
import { SpendMoneyDetailComponent } from './detail/spend-money-detail.component';
import { SpendMoneyFormComponent } from './form/spend-money-form.component';
import { SpendMoneyListComponent } from './list/spend-money-list.component';
import { SpendMoneyUpdateComponent } from './update/spend-money-update.component';

import { SpendMoneyRestService } from './spend-money-rest.service';
import { SpendMoneyService } from './spend-money.service';

import { BusinessPartnerModule } from '../business-partner/business-partner.module';
import { CoreModule } from '../../core/core.module';
import { SharedModule } from '../../shared/shared.module';
import { TransactionModule } from '../transaction/transaction.module';

export const PROVIDERS = [
  SpendMoneyRestService,
  SpendMoneyService,
];

@NgModule({
  imports: [
    AutoCompleteModule,
    BsDropdownModule,
    BusinessPartnerModule,
    CalendarModule,
    CommonModule,
    CoreModule,
    DataTableModule,
    FlexLayoutModule,
    FormsModule,
    HttpModule,
    ListboxModule,
    ModalModule,
    PaginationModule,
    ReactiveFormsModule,
    RouterModule,
    SharedModule,
    TransactionModule,
    TranslateModule,
  ],
  declarations: [
    SpendMoneyCreateComponent,
    SpendMoneyDetailComponent,
    SpendMoneyFormComponent,
    SpendMoneyListComponent,
    SpendMoneyUpdateComponent,
  ],
})
export class SpendMoneyModule { }

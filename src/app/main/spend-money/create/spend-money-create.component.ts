import * as _ from 'lodash';
import { Component } from '@angular/core';
import 'app/rxjs-imports.ts';

import { SpendMoneyRestService } from '../spend-money-rest.service';
import { SpendMoneyService } from '../spend-money.service';

import { BaseCreateBComponent } from '../../../shared/base/base.module';
import { MSpendMoney } from '../spend-money.model';

@Component({
  selector: 'app-spend-money-create',
  templateUrl: 'spend-money-create.component.html'
})
export class SpendMoneyCreateComponent extends BaseCreateBComponent {
  constructor(
    private _spendMoney: SpendMoneyService,
    private _spendMoneyRest: SpendMoneyRestService,
  ) {
    super();

    this.componentId = 'SpendMoneyCreate';
    this.headerTitle = 'ui.spendMoney.create.title';
    this.containerType = 1;
    this.routeURL = '/spend-moneys';
    this.entrySuccessI18n = 'success.spendMoney.create';

    this.registerHook('buildForm', event => {
      this._spendMoney.setFormDefinitions(this.form);
    });

    this.registerHook('save', event => {
      let spendMoney = new MSpendMoney;

      spendMoney = _.assign(spendMoney, this.form.value);
      this._spendMoney.normalizeDoc(spendMoney);

      return this._spendMoneyRest.create(spendMoney);
    });
  }
}

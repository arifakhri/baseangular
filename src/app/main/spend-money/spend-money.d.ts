declare interface ISpendMoney {
  id: string;
  branchId: number;
  transactionNumber: string;
  transactionDate: string | Date;
  transactionType: string;
  payeeId: string;
  description: string;
  total: number;
  note: string;
  status: string;
  state: string;
  isOpen: boolean;
  rowVersion: string;
  branch: {
    id: number;
    name: string;
  };
  payee: {
    id: string;
    contactType: string;
    code: string;
    displayName: string;
    company: string;
    fullName: string;
    phone: string;
    mobile: string;
    fax: string;
    email: string;
    website: string;
    inactive: boolean;
    postalAddress: {
      attention: string;
      street1: string;
      street2: string;
      city: string;
      stateProvince: string;
      countryId: string;
      postalCode: string;
      longitude: number;
      latitude: number;
      country: {
        code: string;
        name: string;
      };
    };
  };
  lines: [
    {
      id: string;
      spendMoneyId: string;
      accountId: string;
      description: string;
      amount: number;
      sortOrder: number;
      rowVersion: string;
      account: {
        id: string;
        accountClassId: string;
        accountClassName: string;
        accountClassSortOrder: number;
        accountTypeId: string;
        accountTypeName: string;
        accountTypeSortOrder: string;
        code: string;
        name: string;
        description: string;
        isChildAccount: boolean;
        level: number;
        locked: boolean;
        systemType: string;
        inactive: boolean;
      };
    }
  ];
  paymentAccountId: string;
  paymentAccount: {
    id: string;
    accountClassId: string;
    accountClassName: string;
    accountClassSortOrder: number;
    accountTypeId: string;
    accountTypeName: string;
    accountTypeSortOrder: string;
    code: string;
    name: string;
    description: string;
    isChildAccount: boolean;
    level: number;
    locked: boolean;
    systemType: string;
    inactive: boolean;
  };
}

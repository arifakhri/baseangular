import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';

import { ApiQueryOption, RequestService } from '../../core/core.module';
import { BusinessPartnerRestService } from '../business-partner/business-partner-rest.service';

import { APP_CONST } from '../../app.const';

@Injectable()
export class SpendMoneyRestService {
  baseURL = `${APP_CONST.API_MAIN}/spend-moneys`;
  request = this._request.new(this.baseURL);

  constructor(
    private _request: RequestService,
    public _businessPartnerRest: BusinessPartnerRestService,
  ) { }

  create(spendMoney: ISpendMoney) {
    return this.request.post<ISpendMoney>(``, spendMoney);
  }

  findAll(queryOption: ApiQueryOption, queryParams: any = {}) {
    return this.request.post<IApiPaginationResult<ISpendMoney>>(`q`, queryOption, { params: queryParams });
  }

  findAllBusinessPartner(queryOption: ApiQueryOption = new ApiQueryOption): Observable<IApiPaginationResult<IBusinessPartner>> {
    return this._businessPartnerRest.findAll(queryOption);
  }

  load(spendMoneyId: string, queryParams: any = {}) {
    return this.request.get<ISpendMoney>(`${spendMoneyId}`, { params: queryParams });
  }

  loadRelatedData() {
    return this.request.get<{ accounts: IAccount[] }>(`entry-related-data`);
  }

  update(spendMoneyId: string, updateObj: ISpendMoney) {
    return this.request.put<ISpendMoney>(`${spendMoneyId}`, updateObj);
  }

  void(spendMoneyId: string) {
    return this.request.put<ISpendMoney>(`${spendMoneyId}/void`, {});
  }
}

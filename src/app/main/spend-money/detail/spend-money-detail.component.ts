import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { SpendMoneyRestService } from '../spend-money-rest.service';
import { AccountingService } from '../../../core/core.module';

@Component({
  selector: 'app-spend-money-detail',
  templateUrl: 'spend-money-detail.component.html'
})
export class SpendMoneyDetailComponent implements OnInit {
  doc: ISpendMoney;
  routeParams: any;

  constructor(
    public _accounting: AccountingService,
    private _spendMoneyRest: SpendMoneyRestService,
    private _route: ActivatedRoute,
  ) {
    this._route.params.subscribe(params => this.routeParams = params);
  }

  ngOnInit() {
    this.loadData();
  }

  loadData() {
    this._spendMoneyRest.load(this.routeParams.id).subscribe(spendMoney => {
      this.doc = spendMoney;
    });
  }
}

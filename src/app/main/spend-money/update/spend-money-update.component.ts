import * as moment from 'moment';
import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';

import { SpendMoneyService } from '../spend-money.service';
import { SpendMoneyRestService } from '../spend-money-rest.service';

import { BaseUpdateBComponent } from '../../../shared/base/base.module';

@Component({
  selector: 'app-spend-money-update',
  templateUrl: 'spend-money-update.component.html'
})
export class SpendMoneyUpdateComponent extends BaseUpdateBComponent {
  constructor(
    private _spendMoney: SpendMoneyService,
    private _spendMoneyRest: SpendMoneyRestService,
  ) {
    super();

    this.componentId = 'SpendMoneyUpdate';
    this.headerTitle = 'ui.spendMoney.update.title';
    this.containerType = 1;
    this.routeURL = '/spend-moneys';
    this.entrySuccessI18n = 'success.spendMoney.update';

    this.registerHook('buildForm', event => {
      this._spendMoney.setFormDefinitions(this.form);
    });

    this.registerHook('load', event => {
      return this._spendMoneyRest.load(this.page.routeParams.id).switchMap(doc => {
        if (doc.transactionDate) {
          doc.transactionDate = moment(doc.transactionDate).toDate();
        }
        return Observable.of(doc);
      });
    });

    this.registerHook('save', event => {
      const spendMoney: ISpendMoney = Object.assign({}, this.doc, this.form.value);
      this._spendMoney.normalizeDoc(spendMoney);
      return this._spendMoneyRest.update(this.page.routeParams.id, spendMoney);
    });
  }
}

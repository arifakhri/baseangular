import { NgModule } from '@angular/core';

import { SpendMoneyModule } from './spend-money.module';
import { SpendMoneyRoutingModule } from './spend-money-routing.module';

@NgModule({
  imports: [
    SpendMoneyModule,
    SpendMoneyRoutingModule
  ]
})
export class SpendMoneyLazyModule { }

import * as _ from 'lodash';
import { AutoComplete } from 'primeng/components/autocomplete/autocomplete';
import { Component, OnChanges, OnInit, SimpleChanges, ViewChild, } from '@angular/core';
import { FormGroup } from '@angular/forms';
import 'app/rxjs-imports.ts';

import { AccountingService, AutocompleteService, CommonService } from '../../../core/core.module';
import { SpendMoneyRestService } from '../spend-money-rest.service';
import { SpendMoneyService } from '../spend-money.service';

import { TransactionFormBComponent } from '../../transaction/transaction-form.bcomponent';

@Component({
  selector: 'app-spend-money-form',
  templateUrl: 'spend-money-form.component.html',
})
export class SpendMoneyFormComponent extends TransactionFormBComponent implements OnChanges, OnInit {
  @ViewChild('payeeAC') elPayeeAC: AutoComplete;

  payeesSuggestion: IBusinessPartner[] = [];

  payeeChanged: boolean = false;

  ACPayeeHandler = CommonService.remoteACItemsHandler({
    remoteParams: this.ACPayeeParams.bind(this),
    remoteRequest: this._spendMoneyRest.findAllBusinessPartner.bind(this._spendMoneyRest),
    remoteRequestMap: (response) => response.data,
    element: () => this.elPayeeAC,
    ACConfig: {
      prependNull: true,
    },
  });

  constructor(
    public _autocomplete: AutocompleteService,
    public _accounting: AccountingService,
    private _spendMoney: SpendMoneyService,
    private _spendMoneyRest: SpendMoneyRestService,
  ) {
    super();
    this.componentId = 'SpendMoneyForm';
    this.withSubTotal = false;

    this.registerHook('loadRelated', event => {
      return this._spendMoneyRest.loadRelatedData();
    });

    this.registerHook('patchForm', event => {
      this.applyDoc();
    });
   }

  ngOnInit() {
    super.ngOnInit();

    this.addLines();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (_.has(changes, 'doc') && this.doc) {
      this.applyDoc();
    }
  }

  ACPayeeParams(event: any, type: string) {
    let filters = [];

    if (type === 'search') {
      filters = [{
        filterValues: [{
          field: 'displayName',
          operator: 'contains',
          value: event.query
        }]
      }];
    }

    return [{
      filter: filters,
      sort: [{
        field: 'displayName',
        dir: 'asc'
      }],
      take: 30,
      skip: 0,
      includeTotalCount: false
    }];
  }

  syncTotal() {
    const formValueLines = this.form.get('lines').value;
    const total = _.sumBy(formValueLines, 'amount');
    this.form.get('total').setValue(total);
  }

  buildFormChildLine() {
    return this._spendMoney.buildFormChildLine();
  }

  lineConditionalValidation(formGroup: FormGroup) {
    return this._spendMoney.lineConditionalValidation(formGroup);
  }
}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SpendMoneyCreateComponent } from './create/spend-money-create.component';
import { SpendMoneyDetailComponent } from './detail/spend-money-detail.component';
import { SpendMoneyListComponent } from './list/spend-money-list.component';
import { SpendMoneyUpdateComponent } from './update/spend-money-update.component';

import { AuthorizationService } from '../../core/core.module';

export const routes: Routes = [{
  path: '',
  children: [{
    path: '',
    component: SpendMoneyListComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'spendMoney.list'
    }
  }, {
    path: 'create',
    component: SpendMoneyCreateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'spendMoney.create'
    }
  }, {
    path: ':id/create',
    component: SpendMoneyCreateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'spendMoney.create'
    }
  }, {
    path: ':id',
    component: SpendMoneyDetailComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'spendMoney.detail'
    }
  }, {
    path: ':id/update',
    component: SpendMoneyUpdateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'spendMoney.update'
    }
  }]
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class SpendMoneyRoutingModule { }

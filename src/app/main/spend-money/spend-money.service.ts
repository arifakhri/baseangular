import * as _ from 'lodash';
import * as moment from 'moment';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { Injectable } from '@angular/core';

import { StartingDataService } from '../../core/core.module';

@Injectable()
export class SpendMoneyService {
  constructor(
    private _startingData: StartingDataService,
  ) { }

  normalizeDoc(record: any) {
    if (record.transactionDate) {
      record.transactionDate = moment(record.transactionDate).format('YYYY-MM-DD');
    }

    record.lines = _.reject(record.lines, line => {
      return !(<any>line).accountId;
    });
  }

  setFormDefinitions(form: FormGroup) {
    const startingData = this._startingData.data$.getValue();


    form.addControl('branchId', new FormControl(startingData.masterBranchId));
    form.addControl('transactionNumber', new FormControl);
    form.addControl('transactionDate', new FormControl(new Date(), Validators.required));
    form.addControl('paymentAccount', new FormControl);
    form.addControl('paymentAccountId', new FormControl(Validators.required));
    form.addControl('payee', new FormControl);
    form.addControl('payeeId', new FormControl(Validators.required));
    form.addControl('description', new FormControl);
    form.addControl('note', new FormControl);

    form.addControl('total', new FormControl(0));

    form.addControl('lines', new FormArray([]));
  }

  buildFormChildLine() {
    return new FormGroup({
      id: new FormControl(null),
      rowVersion: new FormControl,
      account: new FormControl,
      accountId: new FormControl,
      amount: new FormControl,
      description: new FormControl,
    });
  }

  lineConditionalValidation(formGroup: FormGroup) {
    const formGroupValue = formGroup.value;
    if (
      formGroup.dirty &&
      _.size(<any>_.pickBy(_.omit(formGroupValue, ['branchId']), _.identity))
    ) {
      formGroup.get('accountId').setValidators([Validators.required]);
      formGroup.get('accountId').updateValueAndValidity();
      formGroup.get('description').setValidators([Validators.required]);
      formGroup.get('description').updateValueAndValidity();
      formGroup.get('amount').setValidators([Validators.required]);
      formGroup.get('amount').updateValueAndValidity();
    } else {
      formGroup.get('accountId').clearValidators();
      formGroup.get('accountId').updateValueAndValidity();
      formGroup.get('description').clearValidators();
      formGroup.get('description').updateValueAndValidity();
      formGroup.get('amount').clearValidators();
      formGroup.get('amount').updateValueAndValidity();
    }
  }
}

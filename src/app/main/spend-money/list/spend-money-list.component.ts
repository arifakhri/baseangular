import * as SnackBar from 'node-snackbar';
import swal from 'sweetalert2';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { DataTable } from 'primeng/components/datatable/datatable';
import { Observable, Subscription } from 'rxjs';
import 'app/rxjs-imports.ts';
import { TranslateService } from '@ngx-translate/core';

import { SpinnerDirective } from '../../../shared/spinner/spinner.directive';

import {
  AccountingService,
  ExportDataTableService,
  GridTableDataSource,
  GridTableService,
  GridTableToggleService,
} from '../../../core/core.module';
import { SpendMoneyRestService } from '../spend-money-rest.service';
import { RetryDialogComponent } from '../../../shared/retry-dialog/retry-dialog.component';

@Component({
  selector: 'app-spend-money-list',
  templateUrl: 'spend-money-list.component.html'
})
export class SpendMoneyListComponent implements OnInit {
  @ViewChild('gridTable') public gridTable: DataTable;
  @ViewChild('pageLoading') elPageLoading: SpinnerDirective;
  @ViewChild('retryDialog') elRetryDialog: RetryDialogComponent;

  compReady: boolean;
  gridDataSource: GridTableDataSource<ISpendMoney> = new GridTableDataSource<ISpendMoney>();
  selectedRecords: ISpendMoney[] = [];

  combinedFilterValues: string = '';
  combinedFilterFields = {};

  tableColumns: IGridTableColumn[] = [{
    i18nLabel: 'ui.spendMoney.list.column.transactionDate',
    field: 'transactionDate',
    sort: true,
  }, {
    i18nLabel: 'ui.spendMoney.list.column.transactionNumber',
    field: 'transactionNumber',
    link: (row: ISpendMoney) => {
      return `/spend-moneys/${row.id}`;
    },
    sort: true,
  }, {
    i18nLabel: 'ui.spendMoney.list.column.description',
    field: 'description',
    sort: true,
  }, {
    i18nLabel: 'ui.spendMoney.list.column.payFrom',
    field: 'paymentAccount.name',
    sort: true,
  }, {
    i18nLabel: 'ui.spendMoney.list.column.payee',
    field: 'payee.displayName',
    sort: true,
  }, {
    i18nLabel: 'ui.spendMoney.list.column.total',
    field: 'total',
    sort: true,
    formatter: (value, row: ISpendMoney) => {
      return this._accounting.ac.formatMoney(value, '');
    },
    columnClasses: 'right-align',
  }];
  tableColumnsShow: boolean = false;
  tableColumnsToggle = this._gridTableToggle.mapToggleOptionsFromColumns(this.tableColumns);

  retryDialogShownState: boolean = false;
  retryDialogOnRetry: Function;

  private loadDataTimeoutId: number;
  private gridFirstLoad: boolean = true;
  private subscriptions: { [key: string]: Subscription[] } = {
    load: []
  };

  constructor(
    private _accounting: AccountingService,
    private _activatedRoute: ActivatedRoute,
    private _dataTableExport: ExportDataTableService,
    private _gridTable: GridTableService,
    private _gridTableToggle: GridTableToggleService,
    private _spendMoneyRest: SpendMoneyRestService,
    private _translate: TranslateService,
  ) { }

  ngOnInit() {
    this.loadData();
  }

  gridLoadDataWrapper() {
    // Grid always load at the first time. we dont want that. we already call on ngOnInit
    if (this.gridFirstLoad) {
      this.gridFirstLoad = false;
      return;
    }

    if (this.loadDataTimeoutId) {
      window.clearTimeout(this.loadDataTimeoutId);
      this.loadDataTimeoutId = null;
    }

    this.loadDataTimeoutId = window.setTimeout(() => {
      this.loadData();
    }, 300);
  }

  showVoidDialog(spendMoneyId: string) {
    swal({
      title: this._translate.instant('confirm.spendMoney.void.label'),
      text: this._translate.instant('confirm.spendMoney.void.description'),
      type: 'question',
      showCancelButton: true,
    })
      .then(() => {
        this.voidOrder(spendMoneyId);
      })
      .catch(() => { });
  }

  voidOrder(spendMoneyId: string) {
    this._spendMoneyRest.void(spendMoneyId).subscribe(response => {
      this.gridLoadDataWrapper();
      SnackBar.show({
        text: this._translate.instant('success.spendMoney.void'),
        pos: 'bottom-right'
      });
    });
  }

  get exportRecords(): Observable<ISpendMoney[]> {
    const qOption = {
      ...this._gridTable.generateApiQueryOptionFromGridInfo(
        this.gridDataSource.pager,
        false,
        this.gridTable,
        this._gridTable.generateCombinedApiFilters(this.combinedFilterValues, this.combinedFilterFields)
      ),
      skip: 0,
      take: this._dataTableExport.exportLimit,
    };
    return this.elRetryDialog.createRetryEntry(
      this._spendMoneyRest.findAll(qOption).map(response => response.data)
    );
  }

  exportPDF() {
    this.exportRecords.subscribe(records => {
      this._dataTableExport.export({
        records,
        mapOptions: this._dataTableExport.mapFromTableColumns(this.tableColumns),
        fileName: 'all-spend-moneys',
        extension: 'pdf',
        templateData: {
          title: this._translate.instant('ui.spendMoney.list.title'),
        },
      });
    });
  }

  exportExcel() {
    this.exportRecords.subscribe(records => {
      this._dataTableExport.export({
        records: records,
        mapOptions: this._dataTableExport.mapFromTableColumns(this.tableColumns),
        fileName: 'all-spend-moneys',
        extension: 'xls',
      });
    });
  }

  loadData() {
    this.elPageLoading.forceShow();

    const qOption = this._gridTable.generateApiQueryOptionFromGridInfo(
      this.gridDataSource.pager,
      true,
      this.gridTable,
      this._gridTable.generateCombinedApiFilters(this.combinedFilterValues, this.combinedFilterFields)
    );

    this.subscriptions.load.forEach(sub => sub.unsubscribe());
    this.subscriptions.load.push(
      this.elRetryDialog.createRetryEntry(
        this._spendMoneyRest.findAll(qOption)
          .finally(() => {
            this.elPageLoading.forceHide();
          })
      ).subscribe(response => {
        this.gridDataSource.updateFromApiPaginationResult(response);
        this.compReady = true;
      })
    );
  }

  public onGridChange() {
    this.gridLoadDataWrapper();
  }

  public onGridPageChange() {
    this.gridLoadDataWrapper();
  }
}

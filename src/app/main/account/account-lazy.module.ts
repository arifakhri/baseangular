import { NgModule } from '@angular/core';

import { AccountModule } from './account.module';
import { AccountRoutingModule } from './account-routing.module';

@NgModule({
  imports: [
    AccountModule,
    AccountRoutingModule
  ]
})
export class AccountLazyModule { }

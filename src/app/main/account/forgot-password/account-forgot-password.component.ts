import { Component, Inject } from '@angular/core';
import { CustomValidators } from 'ng2-validation';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

import { AccountRestService } from '../account-rest.service';
import { CommonService, SystemMessageService } from '../../../core/core.module';

import { BSafeExitComponent } from '../../../core/base/safe-exit-component.base';

@Component({
  selector: 'app-account-forgot-password',
  templateUrl: 'account-forgot-password.component.html',
  styleUrls: ['./account-forgot-password.component.scss'],
  providers: [SystemMessageService],
})
export class AccountForgotPasswordComponent extends BSafeExitComponent {
  form: FormGroup = new FormGroup({});

  constructor(
    @Inject('GlobalSystemMessage') private _globalSystemMessage: SystemMessageService,
    private _accountRest: AccountRestService,
    private _router: Router,
    public _systemMessage: SystemMessageService,
    private _translate: TranslateService,
  ) {
    super();

    this.buildForm();
  }

  buildForm() {
    this.form.addControl('email', new FormControl('', [CustomValidators.email, Validators.required]));
  }

  onSubmit() {
    if (this.formValid()) {
      this.subscriptions.main = this._accountRest.forgotPassword(this.form.value.email).subscribe(response => {
        if (response.resultMessage === 'success') {
          this._router.navigate(['/account/forgot-password/confirmation', this.form.value.email]);
        } else {
          let errorMessage = 'error.unknown';
          if (response.resultMessage === 'unregistered') {
            errorMessage = 'error.account.forgotPassword.unregistered';
          } else if (response.resultMessage === 'unconfirmed') {
            errorMessage = 'error.account.forgotPassword.unconfirmed';
          }
          this._systemMessage.log({
            message: this._translate.instant(errorMessage),
            type: 'error'
          });
        }
      });
    }
  }

  formValid(): boolean {
    if (!this.form.valid) {
      CommonService.markAsDirty(this.form);
      this._systemMessage.log({
        message: this._translate.instant('error.form'),
        type: 'error'
      });
      return false;
    }
    return true;
  }
}

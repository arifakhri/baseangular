import { ActivatedRoute } from '@angular/router';
import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-account-confirmation',
  templateUrl: 'account-confirmation.component.html',
  styleUrls: ['./account-confirmation.component.scss'],
})
export class AccountConfirmationComponent {
  routeParams: any = {};

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _translate: TranslateService,
  ) {
    this.routeParams = this._activatedRoute.snapshot.queryParams;
  }
}

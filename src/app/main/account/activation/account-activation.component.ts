import { ActivatedRoute, Router } from '@angular/router';
import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';

import { AccountRestService } from '../account-rest.service';

import { BSafeExitComponent } from '../../../core/base/safe-exit-component.base';

@Component({
  selector: 'app-account-activation',
  styleUrls: ['./account-activation.component.scss'],
  templateUrl: 'account-activation.component.html',
})
export class AccountActivationComponent extends BSafeExitComponent {
  confirmationResult: boolean = null;
  routeParams: any = {};

  constructor(
    private _accountRest: AccountRestService,
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
  ) {
    super();

    this.routeParams = this._activatedRoute.snapshot.queryParams;
      this.subscriptions.main = this._accountRest.confirmEmail(this.routeParams.userId, this.routeParams.token)
        .catch(error => {
          this.confirmationResult = false;
          return Observable.throw(error);
        })
        .subscribe(() => {
          this.confirmationResult = true;

          setTimeout(() => {
            this.onLogin();
          }, 7000);
        });
  }

  onLogin() {
    this._router.navigateByUrl('/login');
  }
}

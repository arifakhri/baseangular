import { ActivatedRoute } from '@angular/router';
import { Component } from '@angular/core';

@Component({
  selector: 'app-account-forgot-password-confirmation',
  templateUrl: 'account-forgot-password-confirmation.component.html',
  styleUrls: ['./account-forgot-password-confirmation.component.scss'],
})
export class AccountForgotPasswordConfirmationComponent {
  routeParams: any = {};

  constructor(
    private _activatedRoute: ActivatedRoute,
  ) {
    this.routeParams = this._activatedRoute.snapshot.queryParams;
  }
}

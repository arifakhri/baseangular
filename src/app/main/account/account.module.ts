import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HttpModule } from '@angular/http';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { AccountConfirmationComponent } from './confirmation/account-confirmation.component';
import { AccountActivationComponent } from './activation/account-activation.component';
import { AccountForgotPasswordComponent } from './forgot-password/account-forgot-password.component';
import { AccountForgotPasswordConfirmationComponent } from './forgot-password-confirmation/account-forgot-password-confirmation.component';
import { AccountResetPasswordComponent } from './reset-password/account-reset-password.component';

import { AccountRestService } from './account-rest.service';

import { CoreModule } from '../../core/core.module';
import { LayoutsModule } from '../../layouts/layouts.module';
import { SharedModule } from '../../shared/shared.module';

export const PROVIDERS = [
    AccountRestService,
];

@NgModule({
  imports: [
    CommonModule,
    CoreModule,
    FlexLayoutModule,
    HttpModule,
    LayoutsModule,
    ReactiveFormsModule,
    RouterModule,
    SharedModule,
    TranslateModule.forChild(),
  ],
  declarations: [
    AccountConfirmationComponent,
    AccountActivationComponent,
    AccountForgotPasswordComponent,
    AccountForgotPasswordConfirmationComponent,
    AccountResetPasswordComponent,
  ]
})
export class AccountModule { }

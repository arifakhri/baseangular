import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AccountConfirmationComponent } from './confirmation/account-confirmation.component';
import { AccountActivationComponent } from './activation/account-activation.component';
import { AccountForgotPasswordComponent } from './forgot-password/account-forgot-password.component';
import { AccountForgotPasswordConfirmationComponent } from './forgot-password-confirmation/account-forgot-password-confirmation.component';
import { AccountResetPasswordComponent } from './reset-password/account-reset-password.component';

import { AuthorizationService } from '../../core/core.module';

import { BasicLayoutComponent } from '../../layouts/layouts.module';

export const routes: Routes = [{
  path: '',
  children: [{
    path: '',
    component: BasicLayoutComponent,
    children: [{
      path: 'confirmation',
      component: AccountConfirmationComponent,
      canActivate: [AuthorizationService],
      data: {
        name: 'account.confirmation'
      }
    }, {
      path: 'activation',
      component: AccountActivationComponent,
      canActivate: [AuthorizationService],
      data: {
        name: 'account.activation'
      }
    }, {
      path: 'forgot-password',
      component: AccountForgotPasswordComponent,
      canActivate: [AuthorizationService],
      data: {
        name: 'account.forgotPassword'
      }
    }, {
      path: 'forgot-password/confirmation',
      component: AccountForgotPasswordConfirmationComponent,
      canActivate: [AuthorizationService],
      data: {
        name: 'account.forgotPasswordConfirmation'
      }
    }, {
      path: 'reset-password',
      component: AccountResetPasswordComponent,
      canActivate: [AuthorizationService],
      data: {
        name: 'account.resetPassword'
      }
    }],
  }]
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AccountRoutingModule { }

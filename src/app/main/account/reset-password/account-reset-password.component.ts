import { ActivatedRoute, Router } from '@angular/router';
import { Component, Inject } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';

import { AccountRestService } from '../account-rest.service';
import { CommonService, SystemMessageService } from '../../../core/core.module';

import { BSafeExitComponent } from '../../../core/base/safe-exit-component.base';

@Component({
  selector: 'app-account-reset-password',
  templateUrl: 'account-reset-password.component.html',
  styleUrls: ['./account-reset-password.component.scss'],
  providers: [SystemMessageService]
})
export class AccountResetPasswordComponent extends BSafeExitComponent {
  form: FormGroup;
  routeParams: any = {};

  constructor(
    @Inject('GlobalSystemMessage') private _globalSystemMessage: SystemMessageService,
    private _accountRest: AccountRestService,
    private _activatedRoute: ActivatedRoute,
    private _formBuilder: FormBuilder,
    private _router: Router,
    private _translate: TranslateService,
    public _systemMessage: SystemMessageService,
  ) {
    super();

    this.routeParams = this._activatedRoute.snapshot.queryParams;
    this.buildForm();
  }

  onSubmit() {
    if (this.formValid()) {
      this.subscriptions.main = this._accountRest.resetPassword(
        this.routeParams.userId,
        this.routeParams.token,
        this.form.value.password,
      ).subscribe(() => {
        this._globalSystemMessage.log({
          message: this._translate.instant('success.account.resetPassword'),
          type: 'success',
          showAs: 'growl',
          showSnackBar: false,
        });

        this._router.navigate(['/login']);
      });
    }
  }

  formValid(): boolean {
    if (!this.form.valid) {
      CommonService.markAsDirty(this.form);
      this._systemMessage.log({
        message: this._translate.instant('error.form'),
        type: 'error'
      });
      return false;
    }
    return true;
  }

  buildForm() {
    this.form = this._formBuilder.group({
      password: new FormControl('', Validators.required),
      _passwordConfirm: new FormControl('', Validators.required),
    },
      {
        validator: this.matchingPasswords('password', '_passwordConfirm')
      }
    );
  }

  matchingPasswords(passwordKey: string, confirmPasswordKey: string) {
    return (group: FormGroup): {
      [key: string]: any
    } => {
      const password = group.controls[passwordKey];
      const confirmPassword = group.controls[confirmPasswordKey];
      if (password.value !== confirmPassword.value) {
        return {
          mismatchedPasswords: true
        };
      }
    };
  }
}

import { Injectable } from '@angular/core';
import 'app/rxjs-imports.ts';

import { RequestService } from '../../core/core.module';

import { APP_CONST } from '../../app.const';

@Injectable()
export class AccountRestService {
  baseURL = `${APP_CONST.API_ACCOUNT}/users`;
  request = this._request.new(this.baseURL);

  constructor(
    private _request: RequestService,
  ) { }

  confirmEmail(userId: string, token: string) {
    return this.request.get('confirm-email', {
      params: {
        userId,
        token,
      }
    });
  }

  forgotPassword(email: string) {
    return this.request.post<{ resultMessage: 'unregistered' | 'unconfirmed' | 'success' }>('forget-password', { email });
  }

  resetPassword(userId: string, token: string, newPassword: string) {
    return this.request.post('reset-password', { newPassword }, {
      params: {
        userId,
        token,
      }
    });
  }
}

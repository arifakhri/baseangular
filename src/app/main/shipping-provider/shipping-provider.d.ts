declare interface IShippingProvider {
  id: string;
  name: string;
  products: IShippingProviderProduct[];
}

declare interface IShippingProviderProduct {
  shippingProviderId: string;
  id: string;
  name: string;
}

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';

import { RequestService } from '../../core/core.module';
import { APP_CONST } from '../../app.const';

@Injectable()
export class ShippingPriceRestService {
  baseURL = `${APP_CONST.API_MAIN}/shippings`;
  request = this._request.new(this.baseURL);

  constructor(
    private _request: RequestService,
  ) { }

  findAllCity(keyword: string): Observable<IApiPaginationResult<ISettingsUser>> {
    return this.request.get<any>(`cities/${keyword}`);
  }

  checkPrice(data: {
    courierId: string,
    originCityId: string,
    destinationCityId: string,
    weight: number
  }) {
    return this.request.post<any>(`prices`, data);
  }
}

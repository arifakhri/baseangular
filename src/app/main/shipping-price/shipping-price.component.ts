import * as _ from 'lodash';
import { Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { AccountingService, CommonService, SystemMessageService } from '../../core/core.module';

import { ShippingPriceRestService } from './shipping-price-rest.service';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';

@Component({
  selector: 'app-shipping-price',
  templateUrl: 'shipping-price.component.html',
  styleUrls: ['./shipping-price.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [SystemMessageService],
})

export class ShippingPriceComponent implements OnInit {
  @Input() courier: any;
  @Output() afterSelected: EventEmitter<any> = new EventEmitter();
  form: FormGroup = new FormGroup({});

  compReady: boolean = true;
  originCities: any;
  destinationCities: any;
  providerServices: any;

  constructor(
    public _accounting: AccountingService,
    private _shippingPrice: ShippingPriceRestService,
    public _systemMessage: SystemMessageService,
    private _translate: TranslateService,

  ) { }

  ngOnInit() {
    this.buildForm();
  }

  buildForm() {
    this.form.addControl('destinationCity', new FormControl);
    this.form.addControl('destinationCityId', new FormControl(null, Validators.required));
    this.form.addControl('originCity', new FormControl);
    this.form.addControl('originCityId', new FormControl(null, Validators.required));
    this.form.addControl('weight', new FormControl(null, Validators.required));
  }

  formValid(): boolean {
    if (!this.form.valid) {
      CommonService.markAsDirty(this.form);
      this._systemMessage.log({
        message: this._translate.instant('error.form'),
        type: 'error'
      });
      return false;
    }
    return true;
  }

  searchCity(event: any, type: string) {
    this._shippingPrice.findAllCity(event.query).subscribe(result => {
      switch (type) {
        case 'origin':
          this.originCities = result;
        break;
        case 'destination':
          this.destinationCities = result;
        break;
      }
    });
  }

  checkPrice() {
    if (this.formValid()) {
      this.compReady = false;
      this._shippingPrice.checkPrice({
        courierId: this.courier.id,
        originCityId: this.form.value.originCityId,
        destinationCityId: this.form.value.destinationCityId,
        weight: this.form.value.weight
      })
      .subscribe(response => {
        this.compReady = true;
        if (_.isObject(response)) {
          this.providerServices = response;
        } else {
          // Temporarly error will be handled like this since the response status from back-end is always 200, and error will return as string
          this.providerServices = null;
          this._systemMessage.log({
            message: response,
            type: 'error'
          });
          return Observable.throw(response);
        }
      });
    }
  }

}

import { AutoCompleteModule } from 'primeng/components/autocomplete/autocomplete';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';

import { ShippingPriceComponent } from './shipping-price.component';
import { ShippingPriceRestService } from './shipping-price-rest.service';

import { CoreModule } from '../../core/core.module';
import { SharedModule } from '../../shared/shared.module';

export const PROVIDERS = [
  ShippingPriceRestService,
];

@NgModule({
  imports: [
    AutoCompleteModule,
    CommonModule,
    CoreModule,
    ReactiveFormsModule,
    SharedModule,
    FlexLayoutModule,
    TranslateModule,
  ],
  declarations: [
    ShippingPriceComponent,
  ],
  exports: [
    ShippingPriceComponent,
  ]
})
export class ShippingPriceModule {  }

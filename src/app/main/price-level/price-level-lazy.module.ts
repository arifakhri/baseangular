import { NgModule } from '@angular/core';

import { PriceLevelModule } from './price-level.module';
import { PriceLevelRoutingModule } from './price-level-routing.module';

@NgModule({
  imports: [
    PriceLevelModule,
    PriceLevelRoutingModule
  ]
})
export class PriceLevelLazyModule { }

import * as _ from 'lodash';
import { Component } from '@angular/core';

import { PriceLevelRestService } from '../price-level-rest.service';

import { BaseCreateBComponent } from '../../../shared/base/base.module';
import { MPriceLevel } from '../price-level.model';

@Component({
  selector: 'app-price-level-create',
  templateUrl: 'price-level-create.component.html',
})
export class PriceLevelCreateComponent extends BaseCreateBComponent {
  constructor(
    private _priceLevelRest: PriceLevelRestService,
  ) {
    super();

    this.componentId = 'PriceLevelCreate';
    this.routeURL = '/price-levels';
    this.entrySuccessI18n = 'success.priceLevel.create';

    this.headerTitle = 'ui.priceLevel.create.title';

    this.registerHook('save', event => {
      const formValue = this.form.value;
      let priceLevel = new MPriceLevel;
      priceLevel = _.assign(priceLevel, formValue);

      return this._priceLevelRest.create(priceLevel);
    });
  }
}

import { Component } from '@angular/core';

import { PriceLevelRestService } from '../price-level-rest.service';

import { BaseDetailBComponent } from '../../../shared/base/base.module';

@Component({
  selector: 'app-price-level-detail',
  templateUrl: 'price-level-detail.component.html',
})
export class PriceLevelDetailComponent extends BaseDetailBComponent {
  constructor(
    private _priceLevelRest: PriceLevelRestService,
  ) {
    super();

    this.componentId = 'PriceLevelDetail';
    this.headerButtons.push({
      type: 'edit',
    });

    this.registerHook('load', event => {
      return this._priceLevelRest
        .load(this.page.routeParams.id)
        .do(doc => this.headerTitle = doc.name);
    });
  }
}

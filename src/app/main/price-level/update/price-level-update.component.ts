import * as _ from 'lodash';
import { Component } from '@angular/core';

import { PriceLevelRestService } from '../price-level-rest.service';

import { BaseUpdateBComponent } from '../../../shared/base/base.module';
import { MPriceLevel } from '../price-level.model';

@Component({
  selector: 'app-price-level-update',
  templateUrl: 'price-level-update.component.html',
})
export class PriceLevelUpdateComponent extends BaseUpdateBComponent {
  constructor(
    private _priceLevelRest: PriceLevelRestService,
  ) {
    super();

    this.componentId = 'PriceLevelUpdate';
    this.routeURL = '/price-levels';
    this.entrySuccessI18n = 'success.priceLevel.update';

    this.headerTitle = 'ui.priceLevel.update.title';

    this.registerHook('load', event => {
      return this._priceLevelRest.load(this.page.routeParams.id);
    });

    this.registerHook('save', event => {
      const formValue = this.form.value;
      let priceLevel = new MPriceLevel;
      priceLevel = _.assign(priceLevel, this.doc, formValue);

      return this._priceLevelRest.update(this.page.routeParams.id, priceLevel);
    });
  }
}

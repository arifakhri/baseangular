import { Injectable } from '@angular/core';
import 'app/rxjs-imports.ts';

import { ApiQueryOption, RequestService } from '../../core/core.module';

import { APP_CONST } from '../../app.const';

@Injectable()
export class PriceLevelRestService {
  baseURL = `${APP_CONST.API_MAIN}/price-levels`;
  request = this._request.new(this.baseURL);

  constructor(
    private _request: RequestService,
  ) { }

  create(priceLevel: IPriceLevel) {
    return this.request.post<IPriceLevel>(``, priceLevel);
  }

  findAll(queryOption: ApiQueryOption) {
    return this.request.post<IApiPaginationResult<IPriceLevel>>(`q`, queryOption);
  }

  load(priceLevelId: string) {
    return this.request.get<IPriceLevel>(`${priceLevelId}`);
  }

  update(priceLevelId: string, updateObj: IPriceLevel) {
    return this.request.put<IPriceLevel>(`${priceLevelId}`, updateObj);
  }

  delete(priceLevelId: string) {
    return this.request.delete<any>(`${priceLevelId}`);
  }

  toggleInactive(priceLevelId: string, inactive: boolean) {
    return this.request.put<any>(`${priceLevelId}/mark-active/${!inactive}`, null, {
      headers: {
        'Content-Type': 'application/json'
      }
    });
  }
}

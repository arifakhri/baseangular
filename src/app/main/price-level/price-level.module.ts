import { BsDropdownModule, PaginationModule } from 'ngx-bootstrap';
import { CommonModule } from '@angular/common';
import { DataTableModule } from 'primeng/components/datatable/datatable';
import { ListboxModule } from 'primeng/components/listbox/listbox';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { PriceLevelCreateComponent } from './create/price-level-create.component';
import { PriceLevelDetailComponent } from './detail/price-level-detail.component';
import { PriceLevelFormComponent } from './form/price-level-form.component';
import { PriceLevelListComponent } from './list/price-level-list.component';
import { PriceLevelUpdateComponent } from './update/price-level-update.component';

import { PriceLevelRestService } from './price-level-rest.service';
import { PriceLevelService } from './price-level.service';

import { CoreModule } from '../../core/core.module';
import { SharedModule } from '../../shared/shared.module';

export const PROVIDERS = [
  PriceLevelRestService,
  PriceLevelService,
];

@NgModule({
  imports: [
    CommonModule,
    BsDropdownModule,
    ListboxModule,
    FlexLayoutModule,
    PaginationModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    TranslateModule,
    DataTableModule,
    CoreModule,
    SharedModule
  ],
  declarations: [
    PriceLevelCreateComponent,
    PriceLevelDetailComponent,
    PriceLevelFormComponent,
    PriceLevelListComponent,
    PriceLevelUpdateComponent,
  ],
})
export class PriceLevelModule { }

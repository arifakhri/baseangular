import { Component } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

import { BaseFormBComponent } from '../../../shared/base/base.module';

@Component({
  selector: 'app-price-level-form',
  templateUrl: 'price-level-form.component.html'
})
export class PriceLevelFormComponent extends BaseFormBComponent {
  constructor() {
    super();
    this.componentId = 'PriceLevelForm';

    this.registerHook('buildForm', event => {
      this.buildForm();
    });
  }

  buildForm() {
    const nameControl = new FormControl('', [Validators.required, Validators.maxLength(50)]);
    (<any>nameControl).validatorData = {
      maxLength: 50
    };

    this.form.addControl('name', nameControl);
  }
}

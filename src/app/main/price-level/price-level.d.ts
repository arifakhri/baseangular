declare interface IPriceLevel {
  id: string;
  name: string;
  inactive: boolean;
  isMaster: boolean;
}
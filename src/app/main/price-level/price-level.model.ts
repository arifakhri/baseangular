export class MPriceLevel {
  id: string;
  name: string;
  inactive: boolean;
  isMaster: boolean;
}
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PriceLevelCreateComponent } from './create/price-level-create.component';
import { PriceLevelDetailComponent } from './detail/price-level-detail.component';
import { PriceLevelListComponent } from './list/price-level-list.component';
import { PriceLevelUpdateComponent } from './update/price-level-update.component';

import { AuthorizationService } from '../../core/core.module';

export const routes: Routes = [{
  path: '',
  children: [{
    path: '',
    component: PriceLevelListComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'priceLevel.list'
    }
  }, {
    path: 'create',
    component: PriceLevelCreateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'priceLevel.create'
    }
  }, {
    path: ':id',
    component: PriceLevelDetailComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'priceLevel.detail'
    }
  }, {
    path: ':id/update',
    component: PriceLevelUpdateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'priceLevel.update'
    }
  }]
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class PriceLevelRoutingModule { }

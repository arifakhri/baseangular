import swal from 'sweetalert2';
import * as SnackBar from 'node-snackbar';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';
import { TranslateService } from '@ngx-translate/core';

import { PriceLevelRestService } from './price-level-rest.service';

@Injectable()
export class PriceLevelService {
  constructor(
    private _priceLevelRest: PriceLevelRestService,
    private _translate: TranslateService
  ) { }

  showToggleInactiveDialog(priceLevelId: string, inactive: boolean): Observable<IPriceLevel> {
    let confirmTitle;
    let confirmMessage;
    let successMessage;

    if (inactive) {
      confirmTitle = this._translate.instant('ui.priceLevel.confirm.setInactive.label');
      confirmMessage = this._translate.instant('ui.priceLevel.confirm.setInactive.description');
      successMessage = this._translate.instant('ui.priceLevel.success.setInactive');
    } else {
      confirmTitle = this._translate.instant('ui.priceLevel.confirm.setActive.label');
      confirmMessage = this._translate.instant('ui.priceLevel.confirm.setActive.description');
      successMessage = this._translate.instant('ui.priceLevel.success.setActive');
    }

    return Observable.fromPromise(
      swal({
        title: confirmTitle,
        text: confirmMessage,
        type: 'question',
        showCancelButton: true,
      })
    ).switchMap(() => this._priceLevelRest.toggleInactive(priceLevelId, inactive).do(() => {
      SnackBar.show({
        text: successMessage,
        pos: 'bottom-right'
      });
    }));
  }

  setFormDefinitions(form: FormGroup) {
    const nameControl = new FormControl('', [Validators.required, Validators.maxLength(50)]);
    (<any>nameControl).validatorData = {
      maxLength: 50
    };

    form.addControl('name', nameControl);
    form.addControl('inactive', new FormControl(false));
    form.addControl('isMaster', new FormControl(false));
  }
}

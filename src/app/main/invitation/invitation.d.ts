declare interface IInvitation {
  id: string;
  companyId: number;
  email: string;
  firstName: string;
  lastName: string;
  defaultBranchId: number;
  roles: {
    roleId: string;
  }[];
}
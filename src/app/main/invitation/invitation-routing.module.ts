import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { InvitationComponent } from './invitation.component';

import { AuthorizationService } from '../../core/core.module';

const routes: Routes = [{
  path: '',
  children: [{
    path: ':id',
    component: InvitationComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'invitation',
    },
  }]
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ]
})
export class InvitationRoutingModule { }

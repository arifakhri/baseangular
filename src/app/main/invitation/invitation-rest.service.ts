import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';

import { RequestService } from '../../core/core.module';

import { APP_CONST } from '../../app.const';

@Injectable()
export class InvitationRestService {
  baseURL = `${APP_CONST.API_ACCOUNT}/company-invites`;
  request = this._request.new(this.baseURL);

  constructor(
    private _request: RequestService,
  ) { }

  load(inviteId: string): Observable<IInvitation> {
    return this.request.get<IInvitation>(`${inviteId}`);
  }
}

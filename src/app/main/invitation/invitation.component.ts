import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { InvitationRegistrationService } from './invitation-registration.service';
import { InvitationRestService } from './invitation-rest.service';

@Component({
  selector: 'app-invitation',
  template: '',
})
export class InvitationComponent {
  routeParams: any;

  constructor(
    private _invitationRegistration: InvitationRegistrationService,
    private _invitationRest: InvitationRestService,
    private _route: ActivatedRoute,
    private _router: Router,
  ) {
    this._route.params.subscribe(routeParams => {
      this._invitationRest.load(routeParams.id).subscribe(invitation => {
        this._invitationRegistration.invitation = invitation;
        this._router.navigateByUrl('/registration/account');
      });
    });
  }
}

import { NgModule } from '@angular/core';

import { InvitationModule } from './invitation.module';
import { InvitationRoutingModule } from './invitation-routing.module';

@NgModule({
  imports: [
    InvitationModule,
    InvitationRoutingModule,
  ]
})
export class InvitationLazyModule { }

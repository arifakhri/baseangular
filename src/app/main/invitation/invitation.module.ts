import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { InvitationComponent } from './invitation.component';

import { InvitationRegistrationService } from './invitation-registration.service';
import { InvitationRestService } from './invitation-rest.service';

export const PROVIDERS = [
  InvitationRegistrationService,
  InvitationRestService,
];

@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [
    InvitationComponent,
  ],
})
export class InvitationModule { }

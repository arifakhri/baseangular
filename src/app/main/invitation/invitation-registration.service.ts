import * as _ from 'lodash';
import { FormGroup } from '@angular/forms';
import { Injectable } from '@angular/core';

@Injectable()
export class InvitationRegistrationService {
  _invitation: IInvitation;

  set invitation(invitation: IInvitation) {
    this._invitation = invitation;
  }

  get invitaiton() {
    return this._invitation;
  }

  patchRegistrationForm(form: FormGroup) {
    if (this.invitaiton) {
      const objToPatch = _.pick(this.invitaiton, ['firstName', 'lastName', 'email']);
      form.patchValue(objToPatch);

      this.invitation = undefined;
    }
  }
}

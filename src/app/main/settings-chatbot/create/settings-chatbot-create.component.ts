import * as _ from 'lodash';
import { Component, OnInit } from '@angular/core';

import { SettingsChatbotExternalRestService } from '../settings-chatbot-external-rest.service';
import { SettingsChatbotInternalRestService } from '../settings-chatbot-internal-rest.service';

import { BaseCreateBComponent } from '../../../shared/base/base.module';
import { MSettingsChatbotExternal, MSettingsChatbotInternal } from '../settings-chatbot.model';

@Component({
  selector: 'app-settings-chatbot-create',
  templateUrl: './settings-chatbot-create.component.html',
})
export class SettingsChatbotCreateComponent extends BaseCreateBComponent implements OnInit {
  constructor(
    public _settingsChatbotExternalRest: SettingsChatbotExternalRestService,
    public _settingsChatbotInternalRest: SettingsChatbotInternalRestService,
  ) {
    super();

    this.componentId = 'SettingsChatbotCreate';
    this.containerType = 1;
    this.entrySuccessI18n = 'success.settingsChatbot.create';
    this.saveAndViewEnabled = false;

    this.registerHook('save', event => {
      switch (this.page.routeData.type) {
        case 'external':
          let accountExternal = new MSettingsChatbotExternal;
          accountExternal = _.assign(accountExternal, this.form.value);
          return this._settingsChatbotExternalRest.create(accountExternal);
        case 'internal':
          let accountInternal = new MSettingsChatbotInternal;
          accountInternal = _.assign(accountInternal, this.form.value);
          return this._settingsChatbotInternalRest.create(accountInternal);
      }
    });
  }

  ngOnInit() {
  }

  onPageInit() {
    super.onPageInit();
    this.routeURL = `/settings/chatbot/${this.page.routeData.type}s`;
    this.headerTitle = `ui.settingsChatbot.create.${this.page.routeData.type}Title`;
  }
}

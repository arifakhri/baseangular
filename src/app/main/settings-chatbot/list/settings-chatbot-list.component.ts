import { Component, OnInit } from '@angular/core';

import * as SnackBar from 'node-snackbar';
import swal from 'sweetalert2';

import { SettingsChatbotExternalRestService } from '../settings-chatbot-external-rest.service';
import { SettingsChatbotInternalRestService } from '../settings-chatbot-internal-rest.service';

import { BasePageBComponent } from '../../../shared/base/base.module';
import { MSettingsChatbotExternal, MSettingsChatbotInternal } from '../settings-chatbot.model';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-settings-chatbot-list',
  templateUrl: './settings-chatbot-list.component.html',
})
export class SettingsChatbotListComponent extends BasePageBComponent implements OnInit {
  dataExternal: MSettingsChatbotExternal[] = [];
  dataInternal: MSettingsChatbotInternal[] = [];

  data: any = {};

  constructor(
    private _settingsChatbotExternalRest: SettingsChatbotExternalRestService,
    private _settingsChatbotInternalRest: SettingsChatbotInternalRestService,
    private _translate: TranslateService,
  ) {
    super();

    this.componentId = 'SettingsChatbotList';

    this.headerButtons.push({
      type: 'custom',
      label: 'ui.button.create',
      color: 'success',
      routerLink: 'create',
    });
  }

  ngOnInit() {
  }

  onPageInit() {
    super.onPageInit();

    this.headerTitle = `ui.settingsChatbot.list.${this.page.routeData.type}Title`;

    this.loadData(this.page.routeData.type);
  }

  loadData(type: 'external' | 'internal') {
    let obs: any;
    switch (type) {
      case 'external':
        obs = this._settingsChatbotExternalRest.findAll();
        break;
      case 'internal':
        obs = this._settingsChatbotInternalRest.findAll();
        break;
    }

    const loading = this.page.createLoading();

    obs
      .do(result => {
        this.data[this.page.routeData.type] = result.data;
      })
      .finally(() => {
        loading.dispose();
      })
      .subscribe();
  }

  showDeleteDialog(channelAccountId: string, name: string) {
    swal({
      title: this._translate.instant(`confirm.settingsChatbot.delete.label`) + name + '?',
      text: this._translate.instant(`confirm.settingsChatbot.delete.description`),
      type: 'question',
      showCancelButton: true,
    })
      .then(() => {
        this.deleteAccount(channelAccountId);
      })
      .catch(() => { });
  }

  deleteAccount(channelAccountId: string) {
    const loading = this.page.createLoading();
    this._settingsChatbotInternalRest.delete(channelAccountId).subscribe(response => {
      SnackBar.show({
        text: this._translate.instant(`success.settingsChatbot.delete`),
        pos: 'bottom-right'
      });
      loading.dispose();
    });
  }
}

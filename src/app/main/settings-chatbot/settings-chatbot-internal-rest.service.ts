import { Injectable } from '@angular/core';

import { RequestService } from '../../core/core.module';
import { APP_CONST } from '../../app.const';
import { MSettingsChatbotInternal } from './settings-chatbot.model';

@Injectable()
export class SettingsChatbotInternalRestService {
  baseURL = `${APP_CONST.API_MAIN}/integrations/internal-bots/accounts`;
  request = this._request.new(this.baseURL);

  constructor(
    private _request: RequestService,
  ) { }

  create(account: MSettingsChatbotInternal, queryParams: any = {}) {
    return this.request.post<MSettingsChatbotInternal>(``, account, { params: queryParams });
  }

  findAll(queryParams: any = {}) {
    return this.request.get<IApiPaginationResult<MSettingsChatbotInternal>>(``, { params: queryParams });
  }

  load(accountId: string) {
    return this.request.get<MSettingsChatbotInternal>(accountId);
  }

  update(accountId: string, updateObj: MSettingsChatbotInternal, queryParams: any = {}) {
    return this.request.put<MSettingsChatbotInternal>(accountId, updateObj, { params: queryParams });
  }
  delete(accountId: string) {
    return this.request.delete<MSettingsChatbotInternal>(accountId);
  }
}

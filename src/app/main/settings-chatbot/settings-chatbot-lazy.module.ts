import { NgModule } from '@angular/core';

import { SettingsChatbotModule } from './settings-chatbot.module';
import { SettingsChatbotRoutingModule } from './settings-chatbot-routing.module';

@NgModule({
  imports: [
    SettingsChatbotModule,
    SettingsChatbotRoutingModule,
  ]
})
export class SettingsChatbotLazyModule { }

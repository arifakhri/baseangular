import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SettingsChatbotCreateComponent } from './create/settings-chatbot-create.component';
import { SettingsChatbotListComponent } from './list/settings-chatbot-list.component';
import { SettingsChatbotUpdateComponent } from './update/settings-chatbot-update.component';

const routes: Routes = [{
  path: '',
  children: [{
    path: 'externals/create',
    component: SettingsChatbotCreateComponent,
    data: {
      type: 'external',
    },
  }, {
    path: 'internals/create',
    component: SettingsChatbotCreateComponent,
    data: {
      type: 'internal',
    },
  }, {
    path: 'externals',
    component: SettingsChatbotListComponent,
    data: {
      type: 'external',
    },
  }, {
    path: 'internals',
    component: SettingsChatbotListComponent,
    data: {
      type: 'internal',
    },
  }, {
    path: 'externals/:id/update',
    component: SettingsChatbotUpdateComponent,
    data: {
      type: 'external',
    },
  }, {
    path: 'internals/:id/update',
    component: SettingsChatbotUpdateComponent,
    data: {
      type: 'internal',
    },
  }]
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ],
})
export class SettingsChatbotRoutingModule { }

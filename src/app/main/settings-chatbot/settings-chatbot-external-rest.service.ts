import { Injectable } from '@angular/core';

import { RequestService } from '../../core/core.module';
import { APP_CONST } from '../../app.const';
import { MSettingsChatbotExternal } from './settings-chatbot.model';

@Injectable()
export class SettingsChatbotExternalRestService {
  baseURL = `${APP_CONST.API_MAIN}/integrations/external-bots/accounts`;
  request = this._request.new(this.baseURL);

  constructor(
    private _request: RequestService,
  ) { }

  create(account: MSettingsChatbotExternal, queryParams: any = {}) {
    return this.request.post<MSettingsChatbotExternal>(``, account, { params: queryParams });
  }

  findAll(queryParams: any = {}) {
    return this.request.get<IApiPaginationResult<MSettingsChatbotExternal>>(``, { params: queryParams });
  }

  load(accountId: string) {
    return this.request.get<MSettingsChatbotExternal>(accountId);
  }

  update(accountId: string, updateObj: MSettingsChatbotExternal, queryParams: any = {}) {
    return this.request.put<MSettingsChatbotExternal>(accountId, updateObj, { params: queryParams });
  }
}

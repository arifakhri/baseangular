import { Component, ViewChild } from '@angular/core';

import { AutoComplete } from 'primeng/components/autocomplete/autocomplete';
import { BaseFormBComponent } from '../../../shared/base/base.module';
import { CommonService } from '../../../core/core.module';
import { FormControl, Validators } from '@angular/forms';
import { SettingsUserRestService } from '../../settings-user/settings-user-rest.service';

@Component({
  selector: 'app-settings-chatbot-form-internal',
  templateUrl: './settings-chatbot-form-internal.component.html',
})
export class SettingsChatbotFormInternalComponent extends BaseFormBComponent {
  @ViewChild('userAC') elUserAC: AutoComplete;

  ACUserHandler = CommonService.remoteACItemsHandler({
    remoteParams: this.ACUserParams.bind(this),
    remoteRequest: this._settingsUserRest.findAllUsersPicker.bind(this._settingsUserRest),
    remoteRequestMap: (response) => response.data,
    element: () => this.elUserAC,
  });

  constructor(
    private _settingsUserRest: SettingsUserRestService,
  ) {
    super();

    this.componentId = 'SettingsChatbotFormInternal';

    this.registerHook('buildForm', event => {
      this.buildForm();
    });
  }

  buildForm() {
    this.form.addControl('name', new FormControl(null, Validators.required));
    this.form.addControl('description', new FormControl);
    this.form.addControl('channelId', new FormControl('line', Validators.required));
    this.form.addControl('channelUserId', new FormControl(null, Validators.required));
    this.form.addControl('channelUserName', new FormControl(null, Validators.required));
    this.form.addControl('user', new FormControl);
    this.form.addControl('userId', new FormControl);
  }

  ACUserParams(event: any, type: string) {
    const qParams: any = {};

    if (type === 'search') {
      qParams['keyword'] = event.query;
    }

    return [{
      filter: [],
      sort: [],
      take: 30,
      skip: 0,
      includeTotalCount: false
    }, qParams];
  }
}

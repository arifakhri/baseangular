export class MSettingsChatbotExternal {
  id: string;
  name: string;
  description: string;
  clientSecret: string;
  clientToken: string;
  channelId: string;
  rowVersion: string;
}

export class MSettingsChatbotInternal {
  id: string;
  channelId: string;
  channelUserId: string;
  channelUserName: string;
  userId: string;
  rowVersion: string;
}

import { Component } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

import { BaseFormBComponent } from '../../../shared/base/base.module';

@Component({
  selector: 'app-settings-chatbot-form-external',
  templateUrl: './settings-chatbot-form-external.component.html',
})
export class SettingsChatbotFormExternalComponent extends BaseFormBComponent {
  constructor() {
    super();

    this.componentId = 'SettingsChatbotFormExternal';

    this.registerHook('buildForm', event => {
      this.buildForm();
    });
  }

  buildForm() {
    this.form.addControl('name', new FormControl(null, Validators.required));
    this.form.addControl('callbackCode', new FormControl);
    this.form.addControl('description', new FormControl);
    this.form.addControl('clientSecret', new FormControl(null, Validators.required));
    this.form.addControl('clientToken', new FormControl(null, Validators.required));
    this.form.addControl('channelId', new FormControl('line', Validators.required));
  }
}

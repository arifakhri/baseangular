import * as _ from 'lodash';
import { Component, OnInit } from '@angular/core';

import { SettingsChatbotExternalRestService } from '../settings-chatbot-external-rest.service';
import { SettingsChatbotInternalRestService } from '../settings-chatbot-internal-rest.service';

import { BaseUpdateBComponent } from '../../../shared/base/base.module';
import { MSettingsChatbotExternal, MSettingsChatbotInternal } from '../settings-chatbot.model';

@Component({
  selector: 'app-settings-chatbot-update',
  templateUrl: './settings-chatbot-update.component.html',
})
export class SettingsChatbotUpdateComponent extends BaseUpdateBComponent implements OnInit {
  constructor(
    public _settingsChatbotExternalRest: SettingsChatbotExternalRestService,
    public _settingsChatbotInternalRest: SettingsChatbotInternalRestService,
  ) {
    super();

    this.componentId = 'SettingsChatbotUpdate';
    this.containerType = 1;
    this.entrySuccessI18n = 'success.settingsChatbot.update';
    this.saveAndViewEnabled = false;

    this.registerHook('load', event => {
      this.routeURL = `/settings/chatbot/${this.page.routeData.type}s`;
      switch (this.page.routeData.type) {
        case 'external':
          return this._settingsChatbotExternalRest.load(this.page.routeParams.id);
        case 'internal':
          return this._settingsChatbotInternalRest.load(this.page.routeParams.id);
      }
    });

    this.registerHook('save', event => {
      switch (this.page.routeData.type) {
        case 'external':
          let accountExternal = new MSettingsChatbotExternal;
          accountExternal = _.assign(accountExternal, this.form.value);
          return this._settingsChatbotExternalRest.update(this.page.routeParams.id, accountExternal);
        case 'internal':
          let accountInternal = new MSettingsChatbotInternal;
          accountInternal = _.assign(accountInternal, this.form.value);
          return this._settingsChatbotInternalRest.update(this.page.routeParams.id, accountInternal);
      }
    });
  }

  ngOnInit() {
  }

  onPageInit() {
    super.onPageInit();
    this.headerTitle = `ui.settingsChatbot.update.${this.page.routeData.type}Title`;
  }
}

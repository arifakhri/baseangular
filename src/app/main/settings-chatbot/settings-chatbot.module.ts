import { AutoCompleteModule } from 'primeng/components/autocomplete/autocomplete';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { SettingsChatbotCreateComponent } from './create/settings-chatbot-create.component';
import { SettingsChatbotFormExternalComponent } from './form-external/settings-chatbot-form-external.component';
import { SettingsChatbotFormInternalComponent } from './form-internal/settings-chatbot-form-internal.component';
import { SettingsChatbotListComponent } from './list/settings-chatbot-list.component';
import { SettingsChatbotUpdateComponent } from './update/settings-chatbot-update.component';

import { SettingsChatbotExternalRestService } from './settings-chatbot-external-rest.service';
import { SettingsChatbotInternalRestService } from './settings-chatbot-internal-rest.service';

import { CoreModule } from '../../core/core.module';
import { SharedModule } from '../../shared/shared.module';

export const PROVIDERS = [
  SettingsChatbotExternalRestService,
  SettingsChatbotInternalRestService,
];

@NgModule({
  imports: [
    AutoCompleteModule,
    CommonModule,
    CoreModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    RouterModule,
    SharedModule,
    TranslateModule,
  ],
  declarations: [
    SettingsChatbotCreateComponent,
    SettingsChatbotFormExternalComponent,
    SettingsChatbotFormInternalComponent,
    SettingsChatbotListComponent,
    SettingsChatbotUpdateComponent,
  ],
})
export class SettingsChatbotModule { }

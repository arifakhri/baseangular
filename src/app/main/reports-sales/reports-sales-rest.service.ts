import { Injectable } from '@angular/core';

import { ReportsRestService } from '../reports/reports-rest.service';

@Injectable()
export class ReportsSalesRestService {
  constructor(
    private _reportRest: ReportsRestService,
  ) { }

  findAll(reportParams: any = {}) {
    return this._reportRest.requestReport.post<IApiPaginationResult<IPurchasesInvoice>>(
      'sales-list',
      reportParams,
    );
  }
}

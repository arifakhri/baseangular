import { AutoCompleteModule } from 'primeng/components/autocomplete/autocomplete';
import { CalendarModule } from 'primeng/components/calendar/calendar';
import { CommonModule } from '@angular/common';
import { DataTableModule } from 'primeng/components/datatable/datatable';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MultiSelectModule } from 'primeng/multiselect';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { ReportsSalesPreviewComponent } from './preview/reports-sales-preview.component';

import { ReportsSalesRestService } from './reports-sales-rest.service';

import { SharedModule } from '../../shared/shared.module';

export const PROVIDERS = [
  ReportsSalesRestService,
];

@NgModule({
  imports: [
    AutoCompleteModule,
    CalendarModule,
    CommonModule,
    DataTableModule,
    FlexLayoutModule,
    FormsModule,
    MultiSelectModule,
    ReactiveFormsModule,
    RouterModule,
    SharedModule,
    TranslateModule,
  ],
  declarations: [
    ReportsSalesPreviewComponent,
  ]
})
export class ReportsSalesModule {

}

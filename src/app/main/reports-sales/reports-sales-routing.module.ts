import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ReportsSalesPreviewComponent } from './preview/reports-sales-preview.component';

import { AuthorizationService } from '../../core/core.module';

const routes: Routes = [{
  path: '',
  children: [{
    path: '',
    component: ReportsSalesPreviewComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'reportsSales.preview'
    }
  }]
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ],
})
export class ReportsSalesRoutingModule { }

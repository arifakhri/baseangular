import { NgModule } from '@angular/core';

import { ReportsSalesModule } from './reports-sales.module';
import { ReportsSalesRoutingModule } from './reports-sales-routing.module';

@NgModule({
  imports: [
    ReportsSalesModule,
    ReportsSalesRoutingModule,
  ],
})
export class ReportsSalesLazyModule { }

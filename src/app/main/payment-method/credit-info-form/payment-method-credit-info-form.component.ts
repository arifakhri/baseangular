import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-payment-method-credit-info-form',
  templateUrl: 'payment-method-credit-info-form.component.html'
})
export class PaymentMethodCreditInfoFormComponent implements OnInit {
  @Input() form: FormGroup;

  ngOnInit() {
    this.buildForm();
  }

  buildForm() {

  }
}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PaymentMethodCreateComponent } from './create/payment-method-create.component';
import { PaymentMethodListComponent } from './list/payment-method-list.component';
import { PaymentMethodUpdateComponent } from './update/payment-method-update.component';

import { AuthorizationService } from '../../core/core.module';

export const routes: Routes = [{
  path: '',
  children: [{
    path: '',
    component: PaymentMethodListComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'paymentMethod.list'
    }
  }, {
    path: 'create',
    component: PaymentMethodCreateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'paymentMethod.create'
    }
  }, {
    path: ':id/update',
    component: PaymentMethodUpdateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'paymentMethod.update'
    }
  }]
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class PaymentMethodRoutingModule { }

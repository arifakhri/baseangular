import { NgModule } from '@angular/core';

import { PaymentMethodModule } from './payment-method.module';
import { PaymentMethodRoutingModule } from './payment-method-routing.module';

@NgModule({
  imports: [
    PaymentMethodModule,
    PaymentMethodRoutingModule
  ]
})
export class PaymentMethodLazyModule { }

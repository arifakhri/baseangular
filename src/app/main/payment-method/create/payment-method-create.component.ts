import * as _ from 'lodash';
import { Component } from '@angular/core';

import { PaymentMethodRestService } from '../payment-method-rest.service';

import { BaseCreateBComponent } from '../../../shared/base/base.module';
import { MPaymentMethod } from '../payment-method.model';

@Component({
  selector: 'app-payment-method-create',
  templateUrl: 'payment-method-create.component.html',
})

export class PaymentMethodCreateComponent extends BaseCreateBComponent {
  constructor(
    private _paymentMethodRest: PaymentMethodRestService,
  ) {
    super();

    this.componentId = 'PaymentMethodCreate';
    this.routeURL = '/payment-methods';
    this.entrySuccessI18n = 'success.paymentMethod.create';

    this.headerTitle = 'ui.paymentMethod.create.title';

    this.registerHook('save', event => {
      const formValue = this.form.value;
      let paymentMethod = new MPaymentMethod;
      paymentMethod = _.assign(paymentMethod, this.doc, formValue);

      return this._paymentMethodRest.create(paymentMethod);
    });
  }
}

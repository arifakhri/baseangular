import { AfterViewInit, Component, EventEmitter, Inject, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

import { CommonService, SystemMessageService } from '../../../core/core.module';
import { PaymentMethodRestService } from '../payment-method-rest.service';

@Component({
  selector: 'app-payment-method-quick-create',
  templateUrl: 'payment-method-quick-create.component.html',
  providers: [SystemMessageService]
})
export class PaymentMethodQuickCreateComponent implements AfterViewInit {
  @Output() afterSubmit: EventEmitter<IPaymentMethod> = new EventEmitter();
  @Output() close: EventEmitter<boolean> = new EventEmitter();

  form: FormGroup = new FormGroup({});
  initalFormValues: any = {};
  showLoader: boolean = false;

  constructor(
    @Inject('GlobalSystemMessage') private _globalSystemMessage: SystemMessageService,
    private _fb: FormBuilder,
    private _router: Router,
    private _paymentMethodRest: PaymentMethodRestService,
    public _systemMessage: SystemMessageService,
    private _translate: TranslateService
  ) { }

  ngAfterViewInit() {
    this.initalFormValues = this.form.value;
  }

  onSubmit() {
    if (this.formValid()) {
      this.showLoader = true;
      this.save();
    }
  }

  formValid(): boolean {
    if (!this.form.valid) {
      CommonService.markAsDirty(this.form);
      this._systemMessage.log({
        message: this._translate.instant('error.form'),
        type: 'error',
        scroll: false
      });
      return false;
    }
    return true;
  }

  save(): void {
    const paymentMethod: IPaymentMethod = this.form.value;
    this._paymentMethodRest.create(paymentMethod).subscribe(record => {
      this.showLoader = false;
      this._globalSystemMessage.log({
        message: this._translate.instant('success.paymentMethod.quickCreate'),
        type: 'success',
        showAs: 'growl',
          showSnackBar: false,
      });

      this.form.reset(this.initalFormValues);
      this.afterSubmit.emit(record);
    });
  }
}

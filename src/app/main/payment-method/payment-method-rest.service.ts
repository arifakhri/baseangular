import { Injectable } from '@angular/core';
import 'app/rxjs-imports.ts';

import { ApiQueryOption, RequestService } from '../../core/core.module';

import { APP_CONST } from '../../app.const';

@Injectable()
export class PaymentMethodRestService {
  baseURL = `${APP_CONST.API_MAIN}/payment-methods`;
  request = this._request.new(this.baseURL);

  constructor(
    private _request: RequestService,
  ) { }

  create(paymentMethod: IPaymentMethod) {
    return this.request.post<IPaymentMethod>(``, paymentMethod);
  }

  findAll(queryOption: ApiQueryOption) {
    return this.request.post<IApiPaginationResult<IPaymentMethod>>(`q`, queryOption);
  }

  load(paymentMethodId: string) {
    return this.request.get<IPaymentMethod>(`${paymentMethodId}`);
  }

  loadRelatedData() {
    return this.request.get<IPaymentMethodRelatedData>(`entry-related-data`);
  }

  update(paymentMethodId: string, updateObj: IPaymentMethod) {
    return this.request.put<IPaymentMethod>(`${paymentMethodId}`, updateObj);
  }

  delete(paymentMethodId: string) {
    return this.request.delete<any>(`${paymentMethodId}`);
  }

  checkDuplicate(
    values: {
      id?: string;
      name: string;
    }
  ) {
    return this.request.post<boolean>(`check-duplicate`, values);
  }
}

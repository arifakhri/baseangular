import { Component } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import 'app/rxjs-imports.ts';

import { AutocompleteService } from '../../../core/core.module';
import { PaymentMethodRestService } from '../payment-method-rest.service';

import { BaseFormBComponent } from '../../../shared/base/base.module';

@Component({
  selector: 'app-payment-method-form',
  templateUrl: 'payment-method-form.component.html'
})
export class PaymentMethodFormComponent extends BaseFormBComponent {
  constructor(
    public _autocomplete: AutocompleteService,
    private _paymentMethodRest: PaymentMethodRestService
    ) {
    super();
    this.componentId = 'PaymentMethodForm';

    this.registerHook('buildForm', event => {
      this.buildForm();
    });

    this.registerHook('loadRelated', event => {
      return this._paymentMethodRest.loadRelatedData();
    });
  }

  buildForm() {
    const nameControl = new FormControl('', [Validators.required, Validators.maxLength(50)]);
    (<any>nameControl).validatorData = {
      maxLength: 50
    };

    this.form.addControl('name', nameControl);
    this.form.addControl('paymentMethodType', new FormControl(''));
    this.form.addControl('defaultAccount', new FormControl);
    this.form.addControl('defaultAccountId', new FormControl(null));
  }

}

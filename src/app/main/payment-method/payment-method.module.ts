import { AutoCompleteModule } from 'primeng/components/autocomplete/autocomplete';
import { CommonModule } from '@angular/common';
import { DataTableModule } from 'primeng/components/datatable/datatable';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgModule } from '@angular/core';
import { PaginationModule } from 'ngx-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { PaymentMethodCreateComponent } from './create/payment-method-create.component';
import { PaymentMethodCreditInfoFormComponent } from './credit-info-form/payment-method-credit-info-form.component';
import { PaymentMethodFormComponent } from './form/payment-method-form.component';
import { PaymentMethodListComponent } from './list/payment-method-list.component';
import { PaymentMethodUpdateComponent } from './update/payment-method-update.component';

import { PaymentMethodQuickCreateComponent } from './quick-create/payment-method-quick-create.component';
import { PaymentMethodQuickFormComponent } from './quick-form/payment-method-quick-form.component';

import { PaymentMethodRestService } from './payment-method-rest.service';

import { CoreModule } from '../../core/core.module';
import { SharedModule } from '../../shared/shared.module';

export const PROVIDERS = [
  PaymentMethodRestService,
];

@NgModule({
  imports: [
    AutoCompleteModule,
    CommonModule,
    CoreModule,
    DataTableModule,
    FlexLayoutModule,
    FormsModule,
    PaginationModule,
    ReactiveFormsModule,
    RouterModule,
    SharedModule,
    TranslateModule,
  ],
  declarations: [
    PaymentMethodCreateComponent,
    PaymentMethodCreditInfoFormComponent,
    PaymentMethodFormComponent,
    PaymentMethodListComponent,
    PaymentMethodUpdateComponent,

    PaymentMethodQuickCreateComponent,
    PaymentMethodQuickFormComponent
  ],
  exports: [
    PaymentMethodQuickCreateComponent,
    PaymentMethodCreditInfoFormComponent,
  ]
})
export class PaymentMethodModule { }

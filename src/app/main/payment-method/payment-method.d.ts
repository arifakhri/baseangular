declare interface IPaymentMethod {
  id: string;
  paymentMethodType: string;
  name: string;
  inactive: boolean;
  systemLock: boolean;
  systemType: string;
  rowVersion: string;
}

declare interface IPaymentMethodRelatedData {
  settings: {
    companyInfo: {
      companyName: string;
      phone: string;
      fax: string;
      email: string;
      website: string;
      postalAddressSameAsMain: boolean;
      shippingAddressSameAsMain: boolean;
      legalAddressSameAsMain: boolean;
      logoId: string;
      mainAddress: {
        attention: string;
        street1: string;
        street2: string;
        city: string;
        stateProvince: string;
        countryId: string;
        postalCode: string;
        longitude: number;
        latitude: number;
        country: {
          id: string;
          name: string;
          phoneCountryCode: string;
        }
      };
      postalAddress: {
        attention: string;
        street1: string;
        street2: string;
        city: string;
        stateProvince: string;
        countryId: string;
        postalCode: string;
        longitude: number;
        latitude: number;
        country: {
          id: string;
          name: string;
          phoneCountryCode: string;
        }
      };
      shippingAddress: {
        attention: string;
        street1: string;
        street2: string;
        city: string;
        stateProvince: string;
        countryId: string;
        postalCode: string;
        longitude: number;
        latitude: number;
        country: {
          id: string;
          name: string;
          phoneCountryCode: string;
        }
      };
      legalAddress: {
        attention: string;
        street1: string;
        street2: string;
        city: string;
        stateProvince: string;
        countryId: string;
        postalCode: string;
        longitude: number;
        latitude: number;
        country: {
          id: string;
          name: string;
          phoneCountryCode: string;
        }
      };
      logo: {
        id: string;
        fileType: string;
        fileName: string;
        fileExt: string;
        fileSize: number;
        description: string;
        uploadByUserId: string;
        fileUrl: string;
        thumbnailUrl: string;
        smallUrl: string;
        mediumUrl: string;
        largeUrl: string;
        tabletUrl: string;
      }
    };
    companySetting: {
      masterBranchId: number;
      companyName: string;
      industryId: string;
      phone: string;
      fax: string;
      email: string;
      website: string;
      mainAddressId: string;
      postalAddressSameAsMain: boolean;
      postalAddressId: string;
      shippingAddressSameAsMain: boolean;
      shippingAddressId: string;
      legalAddressSameAsMain: boolean;
      legalAddressId: string;
      logoId: string;
    };
    accountingSetting: {
      fiscalYearMonthStart: number;
      accountingStartDate: string;
      lockDate: string;
    };
    accountMapping: {
      cashAccountId: string;
      undepositedFundsAccountId: string;
      arAccountId: string;
      unbilledARAccountId: string;
      apAccountId: string;
      unbilledAPAccountId: string;
      salesDepositAccountId: string;
      purchaseDepositAccountId: string;
      salesTaxAccountId: string;
      purchaseTaxAccountId: string;
      fixedAssetAccountId: string;
      accumulatedDepreciationAccountId: string;
      openingBalanceAccountId: string;
      retainEarningAccountId: string;
      salesIncomeAccountId: string;
      expenseAccountId: string;
      inventoryAccountId: string;
      inventoryVarianceAccountId: string;
      salesDiscountAccountId: string;
      salesReturnAccountId: string;
      shippingIncomeAccountId: string;
      purchaseDiscountAccountId: string;
      shippingChargeAccountId: string;
      inventoryAdjustmentAccountId: string;
      depreciationExpenseAccountId: string;
      roundingAccountId: string;
    };
    salesSetting: {
      defaultSalesTaxId: string;
      defaultCustomerMessage: string;
      shipping: boolean;
      shippingSenderName: string;
      shippingSenderPhoneNumber: string;
      siCepatMemberId: number;
      defaultSalesTax: {
        id: string;
        code: string;
        name: string;
        rate: number;
      }
    };
    purchaseSetting: {
      defaultPurchaseTaxId: string;
    };
    productSetting: {
      masterPriceLevelId: string;
      masterWarehouseId: string;
      allowInventoryLevelBelowZero: boolean;
      defaultUomId: string;
    }
  };
  accounts: [
    {
      id: string;
      accountClassId: string;
      accountClassName: string;
      accountClassSortOrder: number;
      accountTypeId: string;
      accountTypeName: string;
      accountTypeSortOrder: string;
      code: string;
      name: string;
      description: string;
      isChildAccount: boolean;
      parentAccountId: string;
      level: number;
      idIndex: string;
      codeIndex: string;
      nameIndex: string;
      systemType: string;
      balance: number;
      inactive: boolean;
      rowVersion: string;
      accountType: {
        id: string;
        name: string;
        accountClassId: string;
        sortOrder: number;
      };
      parentAccount: {
        id: string;
        accountClassId: string;
        accountClassName: string;
        accountClassSortOrder: number;
        accountTypeId: string;
        accountTypeName: string;
        accountTypeSortOrder: string;
        code: string;
        name: string;
        description: string;
        isChildAccount: boolean;
        level: number;
        systemLocked: boolean;
        systemType: string;
        inactive: boolean;
      }
    }
  ]
}
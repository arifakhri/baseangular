import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-payment-method-quick-form',
  templateUrl: 'payment-method-quick-form.component.html'
})
export class PaymentMethodQuickFormComponent implements OnInit {
  @Input() form: FormGroup;

  constructor(
    private _fb: FormBuilder
  ) { }

  ngOnInit() {
    this.buildForm();
  }

  buildForm() {
    const nameControl = new FormControl('', [Validators.required, Validators.maxLength(50)]);
    (<any>nameControl).validatorData = {
      maxLength: 50
    };

    this.form.addControl('name', nameControl);
    this.form.addControl('paymentMethodType', new FormControl('', Validators.required));
  }
}

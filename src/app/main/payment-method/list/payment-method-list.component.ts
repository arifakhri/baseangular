import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { DataTable } from 'primeng/components/datatable/datatable';
import { Subscription } from 'rxjs';
import 'app/rxjs-imports.ts';

import { GridTableDataSource, GridTableService } from '../../../core/core.module';
import { PaymentMethodRestService } from '../payment-method-rest.service';

import { SpinnerDirective } from '../../../shared/spinner/spinner.directive';
import { RetryDialogComponent } from '../../../shared/retry-dialog/retry-dialog.component';

@Component({
  selector: 'app-payment-method-list',
  templateUrl: 'payment-method-list.component.html'
})
export class PaymentMethodListComponent implements OnInit {
  @ViewChild('gridTable') public gridTable: DataTable;
  @ViewChild('pageLoading') elPageLoading: SpinnerDirective;
  @ViewChild('retryDialog') elRetryDialog: RetryDialogComponent;

  compReady: boolean;
  paymentMethods: IPaymentMethod[] = [];
  gridDataSource: GridTableDataSource<IPaymentMethod> = new GridTableDataSource<IPaymentMethod>();
  selectedRecords: IPaymentMethod[] = [];

  combinedFilterValues: string = '';
  combinedFilterFields = {
    name: true,
    paymentMethodType: true,
  };

  retryDialogShownState: boolean = false;
  retryDialogOnRetry: Function;

  private loadDataTimeoutId: number;
  private gridFirstLoad: boolean = true;
  private subscriptions: { [key: string]: Subscription[] } = {
    load: []
  };

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _gridTable: GridTableService,
    private _paymentMethodRest: PaymentMethodRestService,
  ) { }

  ngOnInit() {
    this.loadData();
  }

  gridLoadDataWrapper() {
    // Grid always load at the first time. we dont want that. we already call on ngOnInit
    if (this.gridFirstLoad) {
      this.gridFirstLoad = false;
      return;
    }

    if (this.loadDataTimeoutId) {
      window.clearTimeout(this.loadDataTimeoutId);
      this.loadDataTimeoutId = null;
    }

    this.loadDataTimeoutId = window.setTimeout(() => {
      this.loadData();
    }, 300);
  }

  loadData() {
    this.elPageLoading.forceShow();

    const qOption = this._gridTable.generateApiQueryOptionFromGridInfo(
      this.gridDataSource.pager,
      true,
      this.gridTable,
      this._gridTable.generateCombinedApiFilters(this.combinedFilterValues, this.combinedFilterFields)
    );

    this.subscriptions.load.forEach(sub => sub.unsubscribe());
    this.subscriptions.load.push(
    this.elRetryDialog.createRetryEntry(
        this._paymentMethodRest.findAll(qOption)
          .finally(() => {
            this.elPageLoading.forceHide();
          })
    ).subscribe(response => {
      this.paymentMethods = response.data;
      this.gridDataSource.updateFromApiPaginationResult(response);

      this.compReady = true;
      })
    );
  }

  public onGridChange() {
    this.gridLoadDataWrapper();
  }

  public onGridPageChange() {
    this.gridLoadDataWrapper();
  }
}

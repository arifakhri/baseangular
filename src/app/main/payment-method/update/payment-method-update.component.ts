import * as _ from 'lodash';
import { Component } from '@angular/core';

import { PaymentMethodRestService } from '../payment-method-rest.service';

import { BaseUpdateBComponent } from '../../../shared/base/base.module';
import { MPaymentMethod } from '../payment-method.model';

@Component({
  selector: 'app-payment-method-update',
  templateUrl: 'payment-method-update.component.html',
})
export class PaymentMethodUpdateComponent extends BaseUpdateBComponent {
  constructor(
    private _paymentMethodRest: PaymentMethodRestService,
  ) {
    super();

    this.componentId = 'PaymentMethodUpdate';
    this.routeURL = '/payment-methods';
    this.entrySuccessI18n = 'success.paymentMethod.update';

    this.headerTitle = 'ui.paymentMethod.update.title';

    this.registerHook('load', event => {
      return this._paymentMethodRest.load(this.page.routeParams.id);
    });

    this.registerHook('save', event => {
      const formValue = this.form.value;
      let paymentMethod = new MPaymentMethod;
      paymentMethod = _.assign(paymentMethod, this.doc, formValue);

      return this._paymentMethodRest.update(this.page.routeParams.id, paymentMethod);
    });
  }
}

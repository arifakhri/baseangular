import { NgModule } from '@angular/core';

import { DashboardModule } from './dashboard.module';
import { DashboardRoutingModule } from './dashboard-routing.module';

@NgModule({
  imports: [
    DashboardModule,
    DashboardRoutingModule,
  ]
})
export class DashboardLazyModule { }

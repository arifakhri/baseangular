import 'app/rxjs-imports.ts';

import { Component } from '@angular/core';
import * as _ from 'lodash';

import { AccountingService } from '../../../core/core.module';
import { DashboardRestService } from '../dashboard-rest.service';

@Component({
  selector: 'app-dashboard-bank-accounts',
  templateUrl: 'dashboard-bank-accounts.component.html',
})
export class DashboardBankAccountsComponent {
  summary: any;

  widgets: any = {
    bankAccount: {
      total: 0,
    },
  };

  constructor(
    public _accounting: AccountingService,
    private _dashboardRest: DashboardRestService,
  ) {
    this.loadData().subscribe(null, null, () => this.prepare());
  }

  loadData() {
    return this._dashboardRest.loadBankAccounts().do(summary => {
      this.summary = summary;
    });
  }

  prepare() {
    this.widgets.bankAccount.total = _.sumBy(this.summary, 'balance');
  }
}

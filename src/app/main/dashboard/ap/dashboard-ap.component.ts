import 'app/rxjs-imports.ts';

import { Component } from '@angular/core';

import { AccountingService } from '../../../core/core.module';
import { TransactionRoutingService } from '../../transaction/transaction-routing.service';
import { DashboardRestService } from '../dashboard-rest.service';

@Component({
  selector: 'app-dashboard-ap',
  templateUrl: 'dashboard-ap.component.html',
})
export class DashboardAPComponent {
  summary: any;

  constructor(
    public _accounting: AccountingService,
    private _dashboardRest: DashboardRestService,
    private _transactionRoutingService: TransactionRoutingService
  ) {
    this.loadData().subscribe();
  }

  loadData() {
    return this._dashboardRest.loadAP().do(summary => {
      this.summary = summary;
    });
  }

  getUpdateLink(doc: any) {
    return this._transactionRoutingService.resolve({ ...doc, transactionType: 'purchase_invoice' });
  }
}

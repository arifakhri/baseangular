import 'app/rxjs-imports.ts';

import { Component } from '@angular/core';
import * as _ from 'lodash';

import { AccountingService } from '../../../core/core.module';
import { DashboardRestService } from '../dashboard-rest.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-dashboard-top-customers',
  templateUrl: 'dashboard-top-customers.component.html',
})
export class DashboardTopCustomersComponent {
  summary: any;
  total: number;

  widgets: any = {
    topCustomers: {
      total: 0,
      colorScheme: {
        domain: ['#75e7eb', '#75eabe', '#69e894', '#c8eec4', '#d3f2dd']
      }
    }
  };

  constructor(
    public _accounting: AccountingService,
    private _dashboardRest: DashboardRestService,
    private _translate: TranslateService,
  ) {
    this.loadData().subscribe(null, null, () => this.prepare());
  }

  loadData() {
    return this._dashboardRest.loadTopCustomers().do(summary => {
      this.summary = summary.data;
      this.total = summary.total;
    });
  }

  prepare() {
    this.widgets.topCustomers.dataset = this.summary.map(topCustomer => {
      return {
        name: topCustomer.customerName || '',
        value: topCustomer.amount,
      };
    });
    this.calculateData(this.total);
  }

  calculateData(amountTotal) {
    const totalDisplayedAmount = _.sumBy(this.summary, 'amount');
    const otherAmount = amountTotal - totalDisplayedAmount;

    this.widgets.topCustomers.total = amountTotal;
    if (otherAmount) {
      this.widgets.topCustomers.dataset.push({
        name: this._translate.instant('ui.dashboard.legend.other'),
        value: otherAmount
      });
    }
  }
}

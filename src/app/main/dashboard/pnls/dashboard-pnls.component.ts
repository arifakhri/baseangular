import 'app/rxjs-imports.ts';

import { Component } from '@angular/core';
import * as _ from 'lodash';
import * as moment from 'moment';

import { AccountingService } from '../../../core/core.module';
import { DashboardRestService } from '../dashboard-rest.service';

@Component({
  selector: 'app-dashboard-pnls',
  templateUrl: 'dashboard-pnls.component.html',
})
export class DashboardPnlsComponent {
  summary: any;

  widgets: any = {
    pnl: {
      colorScheme: {
        domain: ['#75c8ea', '#bac4d0']
      }
    },
  };

  constructor(
    public _accounting: AccountingService,
    private _dashboardRest: DashboardRestService,
  ) {
    this.loadData().subscribe(null, null, () => this.prepare());
  }

  loadData() {
    return this._dashboardRest.loadPNL().do(summary => {
      this.summary = summary;
    });
  }

  prepare() {
    this.widgets.pnl.dataset = this.summary.map((pnl) => {
      return {
        name: moment(pnl.date).format('MMM YYYY'),
        series: [{
          name: 'Profit',
          value: pnl.income,
        }, {
          name: 'Loss',
          value: pnl.expense,
        }]
      };
    });
    this.widgets.pnl.totalProfit = _.sumBy(this.summary, 'income');
  }
}

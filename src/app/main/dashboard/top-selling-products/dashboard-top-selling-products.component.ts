import 'app/rxjs-imports.ts';

import { Component } from '@angular/core';
import * as _ from 'lodash';

import { AccountingService } from '../../../core/core.module';
import { DashboardRestService } from '../dashboard-rest.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-dashboard-top-selling-products',
  templateUrl: 'dashboard-top-selling-products.component.html',
})
export class DashboardTopSellingProductsComponent {
  summary: any;
  total: number;

  widgets: any = {
    topProducts: {
      total: 0,
      colorScheme: {
        domain: ['rgba(117, 132, 235, 1)', 'rgba(151, 152, 240, 1)', 'rgba(192, 163, 241, 1)', 'rgba(193 ,183, 239, 1)', '#d7cdf0']
      }
    }
  };

  constructor(
    public _accounting: AccountingService,
    private _dashboardRest: DashboardRestService,
    private _translate: TranslateService,
  ) {
    this.loadData().subscribe(null, null, () => this.prepare());
  }

  loadData() {
    return this._dashboardRest.loadTopSellingProducts().do(summary => {
      this.summary = summary.data;
      this.total = summary.total;
    });
  }

  prepare() {
    this.widgets.topProducts.dataset = this.summary.map(topProduct => {
      return {
        name: topProduct.productName || '-',
        value: topProduct.amount,
      };
    });
    this.calculateData(this.total);
  }

  calculateData(amountTotal) {
    const totalDisplayedAmount = _.sumBy(this.summary, 'amount');
    const otherAmount = amountTotal - totalDisplayedAmount;

    this.widgets.topProducts.total = amountTotal;
    if (otherAmount) {
      this.widgets.topProducts.dataset.push({
        name: this._translate.instant('ui.dashboard.legend.other'),
        value: otherAmount
      });
    }
  }
}

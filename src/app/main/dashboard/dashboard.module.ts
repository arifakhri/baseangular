import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

import { CoreModule } from '../../core/core.module';
import { SharedModule as CentricSharedModule } from '../../modules/ng2centric/shared/shared.module';
import { SharedModule } from '../../shared/shared.module';

import { DashboardComponent } from './dashboard.component';
import { DashboardAPComponent } from './ap/dashboard-ap.component';
import { DashboardARComponent } from './ar/dashboard-ar.component';
import { DashboardBankAccountsComponent } from './bank-accounts/dashboard-bank-accounts.component';
import { DashboardExpensesComponent } from './expenses/dashboard-expenses.component';
import { DashboardPnlsComponent } from './pnls/dashboard-pnls.component';
import { DashboardTopCustomersComponent } from './top-customers/dashboard-top-customers.component';
import { DashboardTopSellingCategoriesComponent } from './top-selling-categories/dashboard-top-selling-categories.component';
import { DashboardTopSellingProductsComponent } from './top-selling-products/dashboard-top-selling-products.component';

import { DashboardRestService } from './dashboard-rest.service';

import { DashboardRoutingModule } from './dashboard-routing.module';

export const PROVIDERS = [
  DashboardRestService
];

@NgModule({
  imports: [
    CommonModule,
    FlexLayoutModule,
    TranslateModule.forChild(),
    CoreModule,
    SharedModule,
    CentricSharedModule,
    NgxChartsModule,
    DashboardRoutingModule,
  ],
  declarations: [
    DashboardComponent,
    DashboardAPComponent,
    DashboardARComponent,
    DashboardBankAccountsComponent,
    DashboardExpensesComponent,
    DashboardPnlsComponent,
    DashboardTopCustomersComponent,
    DashboardTopSellingCategoriesComponent,
    DashboardTopSellingProductsComponent,
  ]
})
export class DashboardModule { }

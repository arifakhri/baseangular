import 'app/rxjs-imports.ts';

import { Component } from '@angular/core';
import * as _ from 'lodash';

import { AccountingService } from '../../../core/core.module';
import { DashboardRestService } from '../dashboard-rest.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-dashboard-top-selling-categories',
  templateUrl: 'dashboard-top-selling-categories.component.html',
})
export class DashboardTopSellingCategoriesComponent {
  summary: any;
  total: number;

  widgets: any = {
    topCategories: {
      total: 0,
      colorScheme: {
        domain: ['rgba(175, 117, 235, 1)', 'rgba(145, 117, 235, 1)', 'rgba(211, 117, 235, 1)', 'rgba(222, 178, 236, 1)', '#f0cde2']
      }
    }
  };

  constructor(
    public _accounting: AccountingService,
    private _dashboardRest: DashboardRestService,
    private _translate: TranslateService,
  ) {
    this.loadData().subscribe(null, null, () => this.prepare());
  }

  loadData() {
    return this._dashboardRest.loadTopCategory().do(summary => {
      this.summary = summary.data;
      this.total = summary.total;
    });
  }

  prepare() {
    this.widgets.topCategories.dataset = this.summary.map(topCategories => {
      return {
        name: topCategories.productCategoryName || '-',
        value: topCategories.amount,
      };
    });
    this.calculateData(this.total);
  }

  calculateData(amountTotal) {
    const totalDisplayedAmount = _.sumBy(this.summary, 'amount');
    const otherAmount = amountTotal - totalDisplayedAmount;

    this.widgets.topCategories.total = amountTotal;
    if (otherAmount) {
      this.widgets.topCategories.dataset.push({
        name: this._translate.instant('ui.dashboard.legend.other'),
        value: otherAmount
      });
    }
  }
}

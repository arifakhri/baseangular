import 'app/rxjs-imports.ts';

import * as _ from 'lodash';
import { Component } from '@angular/core';

import { AccountingService } from '../../../core/core.module';
import { DashboardRestService } from '../dashboard-rest.service';

@Component({
  selector: 'app-dashboard-expenses',
  templateUrl: 'dashboard-expenses.component.html',
})
export class DashboardExpensesComponent {
  summary: any;

  widgets: any = {
    expense: {
      total: 0,
      colorScheme: {
        domain: ['rgba(84, 215, 230, 0.8)', 'rgba(84, 187, 230, 0.8)', 'rgba(84, 139, 230, 0.8)', '#cddef1']
      }
    },
  };

  constructor(
    public _accounting: AccountingService,
    private _dashboardRest: DashboardRestService,
  ) {
    this.loadData().subscribe(null, null, () => this.prepare());
  }

  loadData() {
    return this._dashboardRest.loadExpenses().do(summary => {
      this.summary = summary;
    });
  }

  prepare() {
    this.widgets.expense.dataset = this.summary.map(expense => {
      return {
        name: expense.accountName,
        value: expense.amount,
      };
    });
    this.widgets.expense.total = _.sumBy(this.summary, 'amount');
  }
}

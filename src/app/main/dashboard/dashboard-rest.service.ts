import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';

import { RequestService } from '../../core/core.module';

import { APP_CONST } from '../../app.const';

@Injectable()
export class DashboardRestService {
  baseURL = `${APP_CONST.API_MAIN}/dashboard`;
  request = this._request.new(this.baseURL);

  constructor(
    private _request: RequestService,
  ) { }

  loadAP() {
    return this.request.get<IDashboardAP>(`ap`);
  }

  loadAR() {
    return this.request.get<IDashboardAR>(`ar`);
  }

  loadBankAccounts() {
    return this.request.get<IDashboardBankAccount[]>(`bank-accounts`);
  }

  loadExpenses() {
    return this.request.get<IDashboardExpense[]>(`expenses`);
  }

  loadPNL() {
    return this.request.get<IDashboardPNL[]>(`pnl`);
  }

  loadTopCustomers() {
    return this.request.get<IDashboardTopCustomers>(`top-selling-customers`);
  }

  loadTopCategory() {
    return this.request.get<IDashboardTopSellingCategories>(`top-selling-product-categories`);
  }

  loadTopSellingProducts() {
    return this.request.get<IDashboardTopSellingProducts>(`top-selling-products`);
  }

  loadAll(): Observable<{
    ap: IDashboardAP,
    ar: IDashboardAR,
    bankAccounts: IDashboardBankAccount[],
    expenses: IDashboardExpense[],
    pnls: IDashboardPNL[],
  }> {
    return Observable.zip(
      this.loadAP(),
      this.loadAR(),
      this.loadBankAccounts(),
      this.loadExpenses(),
      this.loadPNL(),
      (ap, ar, bankAccounts, expenses, pnls) => {
        return { ap, ar, bankAccounts, expenses, pnls };
      }
    );
  }
}

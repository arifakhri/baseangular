import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DashboardComponent } from './dashboard.component';

import { AuthorizationService } from '../../core/auth/authorization.service';

export const routes: Routes = [{
  path: '',
  component: DashboardComponent,
  canActivate: [AuthorizationService],
  data: {
    name: 'dashboard'
  }
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class DashboardRoutingModule { }

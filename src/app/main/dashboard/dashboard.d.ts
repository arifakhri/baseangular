declare interface IDashboardAP {
  totalCount: number;
  totalAmount: number;
  overdueCount: number;
  overdueAmount: number;
}

declare interface IDashboardAR {
  totalCount: number;
  totalAmount: number;
  overdueCount: number;
  overdueAmount: number;
}

declare interface IDashboardBankAccount {
  accountId: string;
  accountCode: string;
  accountName: string;
  balance: number;
  bankBalance: number;
}

declare interface IDashboardExpense {
  accountId: string;
  accountCode: string;
  accountName: string;
  amount: number;
}

declare interface IDashboardPNL {
  date: string;
  income: number;
  expense: number;
}

declare interface IDashboardTopCustomers {
  data: {
    amount: number;
    customerId: string;
    customerName: string;
    percentageOfSales: number;
  };
  total: number;
}

declare interface IDashboardTopSellingCategories {
  data: {
    amount: number;
    productCategoryId: string;
    productCategoryName: string;
    percentageOfSales: number;
  };
  total: number;
}

declare interface IDashboardTopSellingProducts {
  data: {
    amount: number;
    productId: string;
    productName: string;
    percentageOfSales: number;
  };
  total: number;
}

import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { CommonService } from '../../../core/core.module';

@Component({
  selector: 'app-settings-sales-form-shipping',
  templateUrl: 'settings-sales-form-shipping.component.html'
})
export class SettingsSalesFormShippingComponent {
  @Input() countries: ICountry[] = [];
  @Input() doc: string;
  @Input() parentForm: FormGroup;
  @Output() submit: EventEmitter<any> = new EventEmitter();

  countriesSuggestion: ICountry[] = [];
  updateForm: boolean = false;

  searchACItems = CommonService.searchLocalACItems.bind(this);
  onACDropdown = CommonService.onLocalACDropdown.bind(this);

  onSubmit() {
    this.submit.emit();
  }
}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SettingsSalesUpdateComponent } from './update/settings-sales-update.component';

import { AuthorizationService } from '../../core/core.module';

export const routes: Routes = [{
  path: '',
  children: [{
    path: '',
    component: SettingsSalesUpdateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'settingsSales.update'
    }
  }]
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class SettingsSalesRoutingModule { }

import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class SettingsSalesService {
  constructor(
    private _formBuilder: FormBuilder,
    private _translate: TranslateService
  ) { }

  setFormDefinitions(form: FormGroup) {
    form.addControl('autoReserveInventoryOnSalesOrder', new FormControl(true));
    form.addControl('defaultSalesTax', new FormControl);
    form.addControl('defaultSalesTaxId', new FormControl);
    form.addControl('defaultCustomerMessage', new FormControl);
    form.addControl('shipping', new FormControl(true));
    form.addControl('shippingSenderName', new FormControl);
    form.addControl('shippingSenderPhoneNumber', new FormControl);
    form.addControl('shippingSenderEmail', new FormControl);
    form.addControl('siCepatMemberId', new FormControl);
  }
}

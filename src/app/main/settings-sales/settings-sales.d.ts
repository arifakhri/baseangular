declare interface ISettingsSales {
  defaultSalesTaxId: string;
  defaultCustomerMessage: string;
  shipping: boolean;
  shippingSenderName: string;
  shippingSenderPhoneNumber: string;
  shippingSenderEmail: string;
  siCepatMemberId: number;
}

import { Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { SettingsSalesRestService } from '../settings-sales-rest.service';

@Component({
  selector: 'app-settings-sales-form',
  templateUrl: 'settings-sales-form.component.html',
  styleUrls: ['./settings-sales-form.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SettingsSalesFormComponent implements OnInit {
  @Input() doc: string;
  @Input() form: FormGroup;
  @Output() submit: EventEmitter<ISettingsSales> = new EventEmitter();
  @Output('compReady') compReady: EventEmitter<boolean> = new EventEmitter();

  taxes: ITax[] = [];

  constructor(
    private _settingsSalesRest: SettingsSalesRestService,
  ) {
  }

  ngOnInit() {
    this._settingsSalesRest.loadRelatedData().subscribe(related => {
      this.taxes = related.taxes;
      this.compReady.emit(true);
    });
  }

  onSubmit() {
    this.submit.emit();
  }
}

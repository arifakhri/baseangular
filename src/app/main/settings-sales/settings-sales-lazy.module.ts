import { NgModule } from '@angular/core';
import { SettingsSalesModule } from './settings-sales.module';
import { SettingsSalesRoutingModule } from './settings-sales-routing.module';

@NgModule({
  imports: [
    SettingsSalesModule,
    SettingsSalesRoutingModule
  ]
})
export class SettingsSalesLazyModule { }

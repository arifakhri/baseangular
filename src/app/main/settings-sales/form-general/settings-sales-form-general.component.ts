import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { AutocompleteService } from '../../../core/core.module';

@Component({
  selector: 'app-settings-sales-form-general',
  templateUrl: 'settings-sales-form-general.component.html'
})
export class SettingsSalesFormGeneralComponent {
  @Input() taxes: ITax[] = [];
  @Input() doc: string;
  @Input() parentForm: FormGroup;
  @Output() submit: EventEmitter<any> = new EventEmitter();

  updateForm: boolean = false;

  constructor(
    public _autocomplete: AutocompleteService
  ) {}

  onSubmit() {
    this.submit.emit();
  }
}

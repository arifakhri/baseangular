import { AutoCompleteModule } from 'primeng/components/autocomplete/autocomplete';
import { CommonModule } from '@angular/common';
import { CheckboxModule } from 'primeng/components/checkbox/checkbox';
import { DataTableModule } from 'primeng/components/datatable/datatable';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgModule } from '@angular/core';
import { PaginationModule } from 'ngx-bootstrap';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { SettingsSalesFormComponent } from './form/settings-sales-form.component';
import { SettingsSalesFormGeneralComponent } from './form-general/settings-sales-form-general.component';
import { SettingsSalesFormShippingComponent } from './form-shipping/settings-sales-form-shipping.component';
import { SettingsSalesUpdateComponent } from './update/settings-sales-update.component';

import { SettingsSalesService } from './settings-sales.service';
import { SettingsSalesRestService } from './settings-sales-rest.service';

import { CoreModule } from '../../core/core.module';
import { SettingsModule } from '../settings/settings.module';
import { SharedModule } from '../../shared/shared.module';

export const PROVIDERS = [
  SettingsSalesService,
  SettingsSalesRestService,
];

@NgModule({
  imports: [
    AutoCompleteModule,
    CheckboxModule,
    CommonModule,
    CoreModule,
    DataTableModule,
    FlexLayoutModule,
    FormsModule,
    HttpModule,
    PaginationModule,
    ReactiveFormsModule,
    RouterModule,
    SettingsModule,
    SharedModule,
    TranslateModule,
  ],
  declarations: [
    SettingsSalesFormComponent,
    SettingsSalesFormGeneralComponent,
    SettingsSalesFormShippingComponent,
    SettingsSalesUpdateComponent,
  ],
})
export class SettingsSalesModule { }

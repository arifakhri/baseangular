import * as moment from 'moment';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';
import { TranslateService } from '@ngx-translate/core';

import { CommonService, SystemMessageService } from '../../../core/core.module';
import { JournalEntryRestService } from '../journal-entry-rest.service';
import { JournalEntryService } from '../journal-entry.service';
import { SpinnerService } from '../../../shared/spinner/spinner.service';
import { RetryDialogComponent } from '../../../shared/retry-dialog/retry-dialog.component';

@Component({
  selector: 'app-journal-entry-update',
  templateUrl: 'journal-entry-update.component.html',
  providers: [SystemMessageService]
})
export class JournalEntryUpdateComponent implements OnInit {
  @ViewChild('retryDialog') elRetryDialog: RetryDialogComponent;

  compReady: any = {};
  doc: IJournalEntry;
  form: FormGroup = new FormGroup({});
  routeParams: any;

  retryDialogShownState: boolean = false;
  retryDialogOnRetry: Function;

  constructor(
    @Inject('GlobalSystemMessage') private _globalSystemMessage: SystemMessageService,
    private _journalEntry: JournalEntryService,
    private _journalEntryRest: JournalEntryRestService,
    private _spinner: SpinnerService,
    private _route: ActivatedRoute,
    private _router: Router,
    public _systemMessage: SystemMessageService,
    private _translate: TranslateService
  ) {
    this._route.params.subscribe(params => this.routeParams = params);
  }

  ngOnInit() {
    this._journalEntry.setFormDefinitions(this.form);

    this.loadData();
  }

  onSubmit({ saveAndNew, saveAndView }: any = {}) {
    if (this.formValid()) {
      if (this.formValid()) {
        this.save().subscribe(result => {
          if (saveAndNew) {
            this._router.navigateByUrl('/journal-entries/create');
          } else if (saveAndView) {
            this._router.navigate(['/journal-entries', result.id]);
          } else {
            this._router.navigateByUrl('/journal-entries');
          }
        });
      }
    }
  }

  loadData() {
    this.elRetryDialog.createRetryEntry(this._journalEntryRest.load(this.routeParams.id))
      .subscribe(journalEntry => {
        if (journalEntry.transactionDate) {
          journalEntry.transactionDate = moment(journalEntry.transactionDate).toDate();
        }
        this.form.patchValue(journalEntry);

        this.doc = journalEntry;

        this.compReady.self = true;
      });
  }

  formValid(): boolean {
    if (!this.form.valid) {
      CommonService.markAsDirty(this.form);
      this._systemMessage.log({
        message: this._translate.instant('error.form'),
        type: 'error'
      });
      return false;
    }
    return true;
  }

  save() {
    const spinner = this._spinner.showDefault();

    const journalEntry: IJournalEntry = Object.assign({}, this.doc, this.form.value);
    this._journalEntry.normalizeDoc(journalEntry);

    return this._journalEntryRest.update(this.routeParams.id, journalEntry)
      .catch(error => {
        spinner.dispose();

        this._systemMessage.log({
          message: error,
          type: 'error'
        });
        return Observable.throw(error);
      })
      .do(result => {
        spinner.dispose();

        this._globalSystemMessage.log({
          message: this._translate.instant('success.journalEntry.update'),
          type: 'success',
          showAs: 'growl',
          showSnackBar: false,
        });
      });
  }
}

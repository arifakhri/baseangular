declare interface IJournalEntry {
  id: string;
  branchId: number;
  transactionNumber: string;
  transactionDate: string | Date;
  transactionType: string;
  description: string;
  note: string;
  status: string;
  state: string;
  rowVersion: string;
  branch: {
    id: number;
    name: string;
  };
  lines: [
    {
      id: string;
      manualJournalId: string;
      accountId: string;
      description: string;
      debit: number;
      credit: number;
      sortOrder: number;
      rowVersion: string;
      account: {
        id: string;
        accountClassId: string;
        accountClassName: string;
        accountClassSortOrder: number;
        accountTypeId: string;
        accountTypeName: string;
        accountTypeSortOrder: string;
        code: string;
        name: string;
        description: string;
        isChildAccount: boolean;
        level: number;
        locked: boolean;
        systemType: string;
        inactive: boolean;
      }
    }
  ],
  total: number;
}

import { AutoCompleteModule } from 'primeng/components/autocomplete/autocomplete';
import { BsDropdownModule, ModalModule, PaginationModule } from 'ngx-bootstrap';
import { CalendarModule } from 'primeng/components/calendar/calendar';
import { CommonModule } from '@angular/common';
import { DataTableModule } from 'primeng/components/datatable/datatable';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { ListboxModule } from 'primeng/components/listbox/listbox';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { JournalEntryCreateComponent } from './create/journal-entry-create.component';
import { JournalEntryDetailComponent } from './detail/journal-entry-detail.component';
import { JournalEntryFormComponent } from './form/journal-entry-form.component';
import { JournalEntryListComponent } from './list/journal-entry-list.component';
import { JournalEntryUpdateComponent } from './update/journal-entry-update.component';

import { JournalEntryRestService } from './journal-entry-rest.service';
import { JournalEntryService } from './journal-entry.service';

import { CoreModule } from '../../core/core.module';
import { SharedModule } from '../../shared/shared.module';
import { TransactionModule } from '../transaction/transaction.module';

export const PROVIDERS = [
  JournalEntryRestService,
  JournalEntryService,
];

@NgModule({
  imports: [
    AutoCompleteModule,
    BsDropdownModule,
    CalendarModule,
    CommonModule,
    CoreModule,
    DataTableModule,
    FlexLayoutModule,
    FormsModule,
    HttpModule,
    ListboxModule,
    ModalModule,
    PaginationModule,
    ReactiveFormsModule,
    RouterModule,
    SharedModule,
    TransactionModule,
    TranslateModule,
  ],
  declarations: [
    JournalEntryCreateComponent,
    JournalEntryDetailComponent,
    JournalEntryFormComponent,
    JournalEntryListComponent,
    JournalEntryUpdateComponent,
  ]
})
export class JournalEntryModule { }

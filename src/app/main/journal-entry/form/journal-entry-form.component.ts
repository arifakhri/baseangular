import * as _ from 'lodash';
import { ChangeDetectorRef, Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { FormArray, FormGroup } from '@angular/forms';

import { AccountingService, AutocompleteService } from '../../../core/core.module';
import { JournalEntryRestService } from '../journal-entry-rest.service';
import { JournalEntryService } from '../journal-entry.service';
import 'app/rxjs-imports.ts';
import { RetryDialogComponent } from '../../../shared/retry-dialog/retry-dialog.component';

@Component({
  selector: 'app-journal-entry-form',
  templateUrl: 'journal-entry-form.component.html',
})
export class JournalEntryFormComponent implements OnChanges, OnInit {
  @Input() doc: IJournalEntry;
  @Input() form: FormGroup;
  @Output('compReady') compReady: EventEmitter<boolean> = new EventEmitter();
  @ViewChild('retryDialog') elRetryDialog: RetryDialogComponent;

  accounts: IAccount[] = [];

  retryDialogShownState: boolean = false;
  retryDialogOnRetry: Function;

  constructor(
    public _autocomplete: AutocompleteService,
    public _accounting: AccountingService,
    private _changeDetectorRef: ChangeDetectorRef,
    private _journalEntry: JournalEntryService,
    private _journalEntryRest: JournalEntryRestService,
  ) { }

  ngOnInit() {
    this.loadRelatedData();

    this.addLines();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (_.has(changes, 'doc') && this.doc) {
      this.applyDoc();
    }
  }

  applyDoc() {
    this.resetLines();
    if (this.doc.lines.length > 2) {
      this.addLines(this.doc.lines.length - 2);
    }
    this.form.patchValue({ lines: this.doc.lines });

    this._changeDetectorRef.detectChanges();

    this.syncTotalDebit();
    this.syncTotalCredit();
  }

  clearLine(index: number) {
    const formArray = <FormArray>this.form.controls.lines;
    if ((formArray.length > 2 && index <= 1) || index > 1) {
      formArray.removeAt(index);
    } else {
      formArray.controls[index].reset();
    }
  }

  resetLines() {
    const formArray = <FormArray>this.form.controls.lines;
    while (formArray.length) {
      formArray.removeAt(0);
    }
    this.addLines();
  }

  addLines(length: number = 2) {
    const formArray = (<FormArray>this.form.controls.lines);
    _.range(length).forEach(() => {
      const newFormGroup = this._journalEntry.buildFormChildLine();
      formArray.push(newFormGroup);
    });
    this._changeDetectorRef.detectChanges();
  }

  syncTotalCredit() {
    const formValueLines = this.form.get('lines').value;
    const totalCredit = _.sumBy(formValueLines, 'credit');
    this.form.get('totalCredit').setValue(totalCredit);
  }

  syncTotalDebit() {
    const formValueLines = this.form.get('lines').value;
    const totalDebit = _.sumBy(formValueLines, 'debit');
    this.form.get('totalDebit').setValue(totalDebit);
  }

  loadRelatedData() {
    this.elRetryDialog.createRetryEntry(this._journalEntryRest.loadRelatedData())
      .subscribe(related => {
        this.accounts = related.accounts;

        this.compReady.emit(true);
      });
  }
}

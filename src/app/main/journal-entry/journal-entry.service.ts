import * as _ from 'lodash';
import * as moment from 'moment';
import { CustomValidators } from 'ng2-validation';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { Injectable } from '@angular/core';

import { StartingDataService } from '../../core/core.module';

@Injectable()
export class JournalEntryService {
  constructor(
    private _startingData: StartingDataService,
  ) { }

  normalizeDoc(record: any) {
    if (record.transactionDate) {
      record.transactionDate = moment(record.transactionDate).format('YYYY-MM-DD');
    }
  }

  setFormDefinitions(form: FormGroup) {
    const startingData = this._startingData.data$.getValue();

    form.addControl('branchId', new FormControl(startingData.masterBranchId));
    form.addControl('transactionNumber', new FormControl);
    form.addControl('transactionDate', new FormControl(new Date(), Validators.required));
    form.addControl('description', new FormControl);
    form.addControl('note', new FormControl);

    form.addControl('totalCredit', new FormControl(0, CustomValidators.number));
    form.addControl('totalDebit', new FormControl(0, CustomValidators.number));

    form.addControl('lines', new FormArray([]));
  }

  buildFormChildLine() {
    return new FormGroup({
      id: new FormControl(null),
      rowVersion: new FormControl,
      account: new FormControl,
      accountId: new FormControl(null, Validators.required),
      credit: new FormControl,
      debit: new FormControl,
      description: new FormControl,
    });
  }

  lineConditionalValidation(formGroup: FormGroup) {
    const formGroupValue = formGroup.value;
    if (
      formGroup.dirty &&
      _.size(<any>_.pickBy(_.omit(formGroupValue, ['branchId']), _.identity))
    ) {
      formGroup.get('accountId').setValidators([Validators.required]);
      formGroup.get('accountId').updateValueAndValidity();
    } else {
      formGroup.get('accountId').clearValidators();
      formGroup.get('accountId').updateValueAndValidity();
    }
  }
}

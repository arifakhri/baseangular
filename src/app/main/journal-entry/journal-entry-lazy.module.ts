import { NgModule } from '@angular/core';

import { JournalEntryModule } from './journal-entry.module';
import { JournalEntryRoutingModule } from './journal-entry-routing.module';

@NgModule({
  imports: [
    JournalEntryModule,
    JournalEntryRoutingModule
  ]
})
export class JournalEntryLazyModule { }

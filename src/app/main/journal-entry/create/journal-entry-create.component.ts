import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

import { CommonService, SystemMessageService } from '../../../core/core.module';
import { JournalEntryRestService } from '../journal-entry-rest.service';
import { JournalEntryService } from '../journal-entry.service';
import { SpinnerService } from '../../../shared/spinner/spinner.service';

@Component({
  selector: 'app-journal-entry-create',
  templateUrl: 'journal-entry-create.component.html',
  providers: [SystemMessageService]
})
export class JournalEntryCreateComponent implements OnInit {
  compReady: boolean;
  form: FormGroup = new FormGroup({});

  constructor(
    @Inject('GlobalSystemMessage') private _globalSystemMessage: SystemMessageService,
    private _journalEntry: JournalEntryService,
    private _journalEntryRest: JournalEntryRestService,
    private _spinner: SpinnerService,
    private _router: Router,
    public _systemMessage: SystemMessageService,
    private _translate: TranslateService
  ) { }

  ngOnInit() {
    this._journalEntry.setFormDefinitions(this.form);
  }

  onSubmit({ saveAndNew, saveAndView }: any = {}) {
    if (this.formValid()) {
      if (this.formValid()) {
        this.save().subscribe(result => {
          if (saveAndNew) {
            this._router.navigateByUrl('/empty', { skipLocationChange: true }).then(() => {
              this._router.navigateByUrl('/journal-entries/create');
            });
          } else if (saveAndView) {
            this._router.navigate(['/journal-entries', result.id]);
          } else {
            this._router.navigateByUrl('/journal-entries');
          }
        });
      }
    }
  }

  formValid(): boolean {
    if (!this.form.valid) {
      CommonService.markAsDirty(this.form);
      this._systemMessage.log({
        message: this._translate.instant('error.form'),
        type: 'error'
      });
      return false;
    }
    return true;
  }

  save() {
    const spinner = this._spinner.showDefault();

    const journalEntry: IJournalEntry = this.form.value;
    this._journalEntry.normalizeDoc(journalEntry);

    return this._journalEntryRest.create(journalEntry)
      .catch(error => {
        spinner.dispose();

        this._systemMessage.log({
          message: error,
          type: 'error'
        });
        return Observable.throw(error);
      })
      .do(result => {
        spinner.dispose();

        this._globalSystemMessage.log({
          message: this._translate.instant('success.journalEntry.create'),
          type: 'success',
          showAs: 'growl',
          showSnackBar: false,
        });
      });
  }
}

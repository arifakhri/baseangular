import { Injectable } from '@angular/core';
import 'app/rxjs-imports.ts';

import { ApiQueryOption, RequestService } from '../../core/core.module';

import { APP_CONST } from '../../app.const';

@Injectable()
export class JournalEntryRestService {
  baseURL = `${APP_CONST.API_MAIN}/manual-journals`;
  request = this._request.new(this.baseURL);

  constructor(
    private _request: RequestService,
  ) { }

  create(payment: IJournalEntry) {
    return this.request.post<IJournalEntry>(``, payment);
  }

  findAll(queryOption: ApiQueryOption) {
    return this.request.post<IApiPaginationResult<IJournalEntry>>(`q`, queryOption);
  }

  load(journalEntryId: string, queryParams: any = {}) {
    return this.request.get<IJournalEntry>(`${journalEntryId}`, { params: queryParams });
  }

  loadRelatedData() {
    return this.request.get<{ accounts: IAccount[] }>(`entry-related-data`);
  }

  update(journalEntryId: string, updateObj: IJournalEntry) {
    return this.request.put<IJournalEntry>(`${journalEntryId}`, updateObj);
  }

  void(journalEntryId: string) {
    return this.request.put<IJournalEntry>(`${journalEntryId}/void`, {});
  }
}

import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { AccountingService } from '../../../core/core.module';
import { AdminLayoutService } from '../../../layouts/layouts.module';
import { JournalEntryRestService } from '../journal-entry-rest.service';

@Component({
  selector: 'app-journal-entry-detail',
  templateUrl: 'journal-entry-detail.component.html'
})
export class JournalEntryDetailComponent implements OnInit {
  doc: IJournalEntry;
  routeParams: any;

  constructor(
    public _accounting: AccountingService,
    private _adminLayout: AdminLayoutService,
    private _journalEntryRest: JournalEntryRestService,
    private _route: ActivatedRoute,
  ) {
    this._adminLayout.containerType = 1;
    this._route.params.subscribe(params => this.routeParams = params);
  }

  ngOnInit() {
    this.loadData();
  }

  loadData() {
    this._journalEntryRest.load(this.routeParams.id, { includeLines: true }).subscribe(journalEntry => {
      this.doc = journalEntry;
    });
  }
}

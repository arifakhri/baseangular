import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { JournalEntryCreateComponent } from './create/journal-entry-create.component';
import { JournalEntryDetailComponent } from './detail/journal-entry-detail.component';
import { JournalEntryListComponent } from './list/journal-entry-list.component';
import { JournalEntryUpdateComponent } from './update/journal-entry-update.component';

import { AuthorizationService } from '../../core/core.module';

export const routes: Routes = [{
  path: '',
  children: [{
    path: '',
    component: JournalEntryListComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'journalEntry.list'
    }
  }, {
    path: 'create',
    component: JournalEntryCreateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'journalEntry.create'
    }
  }, {
    path: ':id',
    component: JournalEntryDetailComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'journalEntry.detail'
    }
  }, {
    path: ':id/update',
    component: JournalEntryUpdateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'journalEntry.update'
    }
  }]
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class JournalEntryRoutingModule { }

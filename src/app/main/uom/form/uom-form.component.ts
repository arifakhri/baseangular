import { Component } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

import { BaseFormBComponent } from '../../../shared/base/base.module';

@Component({
  selector: 'app-uom-form',
  templateUrl: 'uom-form.component.html'
})
export class UOMFormComponent extends BaseFormBComponent {
  constructor() {
    super();
    this.componentId = 'UOMForm';

    this.registerHook('buildForm', event => {
      this.buildForm();
    });
  }

  buildForm() {
    const nameControl = new FormControl('', [Validators.required, Validators.maxLength(50)]);
    (<any>nameControl).validatorData = {
      maxLength: 50
    };

    this.form.addControl('name', nameControl);
    this.form.addControl('inactive', new FormControl(false));
    this.form.addControl('systemDefault', new FormControl(false));
  }
}

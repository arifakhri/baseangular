import { CommonModule } from '@angular/common';
import { DataTableModule } from 'primeng/components/datatable/datatable';
import { ListboxModule } from 'primeng/components/listbox/listbox';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgModule } from '@angular/core';
import { BsDropdownModule, PaginationModule } from 'ngx-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { UOMCreateComponent } from './create/uom-create.component';
import { UOMFormComponent } from './form/uom-form.component';
import { UOMListComponent } from './list/uom-list.component';
import { UOMUpdateComponent } from './update/uom-update.component';

import { UOMQuickCreateComponent } from './quick-create/uom-quick-create.component';
import { UOMQuickFormComponent } from './quick-form/uom-quick-form.component';

import { UOMRestService } from './uom-rest.service';

import { CoreModule } from '../../core/core.module';
import { SharedModule } from '../../shared/shared.module';

export const PROVIDERS = [
  UOMRestService,
];

@NgModule({
  imports: [
    BsDropdownModule,
    CommonModule,
    FlexLayoutModule,
    PaginationModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    ListboxModule,
    TranslateModule,
    DataTableModule,
    CoreModule,
    SharedModule,
  ],
  declarations: [
    UOMCreateComponent,
    UOMFormComponent,
    UOMListComponent,
    UOMUpdateComponent,

    UOMQuickCreateComponent,
    UOMQuickFormComponent
  ],
  exports: [
    UOMQuickCreateComponent
  ],
})
export class UOMModule { }

import { Injectable } from '@angular/core';
import 'app/rxjs-imports.ts';

import { ApiQueryOption, RequestService } from '../../core/core.module';

import { APP_CONST } from '../../app.const';

@Injectable()
export class UOMRestService {
  baseURL = `${APP_CONST.API_MAIN}/uom`;
  request = this._request.new(this.baseURL);

  constructor(
    private _request: RequestService,
  ) { }

  create(uom: IUOM) {
    return this.request.post<IUOM>(``, uom);
  }

  findAll(queryOption: ApiQueryOption) {
    return this.request.post<IApiPaginationResult<IUOM>>(`q`, queryOption);
  }

  load(uomId: string) {
    return this.request.get<IUOM>(`${uomId}`);
  }

  update(uomId: string, updateObj: IUOM) {
    return this.request.put<IUOM>(`${uomId}`, updateObj);
  }

  delete(uomId: string) {
    return this.request.delete<any>(`${uomId}`);
  }
}

import { AfterViewInit, Component, EventEmitter, Inject, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

import { CommonService, SystemMessageService } from '../../../core/core.module';
import { UOMRestService } from '../uom-rest.service';

@Component({
  selector: 'app-uom-quick-create',
  templateUrl: 'uom-quick-create.component.html',
  providers: [SystemMessageService]
})
export class UOMQuickCreateComponent implements AfterViewInit {
  @Output() afterSubmit: EventEmitter<IUOM> = new EventEmitter();
  @Output() close: EventEmitter<boolean> = new EventEmitter();

  form: FormGroup = new FormGroup({});
  initalFormValues: any = {};
  showLoader: boolean = false;

  constructor(
    @Inject('GlobalSystemMessage') private _globalSystemMessage: SystemMessageService,
    private _router: Router,
    public _systemMessage: SystemMessageService,
    private _translate: TranslateService,
    private _uomRest: UOMRestService,
  ) { }

  ngAfterViewInit() {
    this.initalFormValues = this.form.value;
  }

  onSubmit() {
    if (this.formValid()) {
      this.showLoader = true;
      this.save();
    }
  }

  formValid(): boolean {
    if (!this.form.valid) {
      CommonService.markAsDirty(this.form);
      this._systemMessage.log({
        message: this._translate.instant('error.form'),
        type: 'error'
      });
      return false;
    }
    return true;
  }

  save(): void {
    const uom: IUOM = this.form.value;
    this._uomRest.create(uom)
      .catch(error => {
        this.showLoader = false;
        this._systemMessage.log({
          message: error,
          type: 'error'
        });
        return Observable.throw(error);
      })
      .subscribe(record => {
        this.showLoader = false;
        this._globalSystemMessage.log({
          message: this._translate.instant('success.uom.quickCreate'),
          type: 'success',
          showAs: 'growl',
          showSnackBar: false,
        });

        this.form.reset(this.initalFormValues);
        this.afterSubmit.emit(record);
      });
  }
}

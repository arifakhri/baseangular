import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UOMCreateComponent } from './create/uom-create.component';
import { UOMListComponent } from './list/uom-list.component';
import { UOMUpdateComponent } from './update/uom-update.component';

import { AuthorizationService } from '../../core/core.module';

export const routes: Routes = [{
  path: '',
  children: [{
    path: '',
    component: UOMListComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'uom.list'
    }
  }, {
    path: 'create',
    component: UOMCreateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'uom.create'
    }
  }, {
    path: ':id/update',
    component: UOMUpdateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'uom.update'
    }
  }, ]
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class UOMRoutingModule { }

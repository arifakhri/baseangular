import * as _ from 'lodash';
import { Component } from '@angular/core';

import { UOMRestService } from '../uom-rest.service';

import { BaseCreateBComponent } from '../../../shared/base/base.module';
import { MUOM } from '../uom.model';

@Component({
  selector: 'app-uom-create',
  templateUrl: 'uom-create.component.html',
})
export class UOMCreateComponent extends BaseCreateBComponent {
  constructor(
    private _uomRest: UOMRestService,
  ) {
    super();

    this.componentId = 'UOMCreate';
    this.routeURL = '/uom';
    this.entrySuccessI18n = 'success.uom.create';

    this.headerTitle = 'ui.uom.create.title';

    this.registerHook('save', event => {
      const formValue = this.form.value;
      let uom = new MUOM;
      uom = _.assign(uom, formValue);

      return this._uomRest.create(uom);
    });
  }
}

import * as _ from 'lodash';
import { Component } from '@angular/core';

import { UOMRestService } from '../uom-rest.service';

import { BaseUpdateBComponent } from '../../../shared/base/base.module';
import { MUOM } from '../uom.model';

@Component({
  selector: 'app-uom-update',
  templateUrl: 'uom-update.component.html',
})
export class UOMUpdateComponent extends BaseUpdateBComponent {
  constructor(
    private _uomRest: UOMRestService,
  ) {
    super();

    this.componentId = 'UOMUpdate';
    this.routeURL = '/uom';
    this.entrySuccessI18n = 'success.uom.update';

    this.headerTitle = 'ui.uom.update.title';

    this.registerHook('load', event => {
      return this._uomRest.load(this.page.routeParams.id);
    });

    this.registerHook('save', event => {
      const formValue = this.form.value;
      let uom = new MUOM;
      uom = _.assign(uom, this.doc, formValue);

      return this._uomRest.update(this.page.routeParams.id, uom);
    });
  }
}

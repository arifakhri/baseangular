import { NgModule } from '@angular/core';

import { UOMModule } from './uom.module';
import { UOMRoutingModule } from './uom-routing.module';

@NgModule({
  imports: [
    UOMModule,
    UOMRoutingModule
  ]
})
export class UOMLazyModule { }

declare interface IUOM {
  id: string;
  name: string;
  inactive: boolean;
  systemDefault: boolean;
}
export class MUOM {
    id: string;
    name: string;
    inactive: boolean;
    systemDefault: boolean;
  }
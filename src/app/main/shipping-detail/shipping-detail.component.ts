import * as _ from 'lodash';
import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';

import { ShippingDetailRestService } from './shipping-detail-rest.service';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';
import { SystemMessageService } from '../../core/core.module';

@Component({
  selector: 'app-shipping-detail',
  templateUrl: 'shipping-detail.component.html',
  styleUrls: ['./shipping-detail.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [SystemMessageService],
})
export class ShippingDetailComponent implements OnInit {
  @Input() courier: any;
  @Input() waybill: string;

  compReady: boolean = true;
  doc: any;

  constructor(
    private _shippingDetailRest: ShippingDetailRestService,
    public _systemMessage: SystemMessageService,
  ) { }

  ngOnInit() {
    this.loadData();
  }

  loadData() {
    this.compReady = false;
    this._shippingDetailRest.detail({
      courierId: this.courier.id,
      trackingNumber: this.waybill,
    })
    .subscribe(response => {
      this.compReady = true;
      if (_.isObject(response)) {
        this.doc = response;
      } else {
        // Error will be handled like this since the response status from back-end is always 200, and error will return as string
        this._systemMessage.log({
          message: response,
          type: 'error'
        });
        return Observable.throw(response);
      }
    });
  }
}

import { Injectable } from '@angular/core';
import 'app/rxjs-imports.ts';

import { RequestService } from '../../core/core.module';

import { APP_CONST } from '../../app.const';

@Injectable()
export class ShippingDetailRestService {
  baseURL = `${APP_CONST.API_MAIN}/shippings/waybill-detail`;
  request = this._request.new(this.baseURL);

  constructor(
    private _request: RequestService,
  ) { }

  detail(data: {
    courierId: string;
    trackingNumber: string;
  }) {
    return this.request.post<any>(``, data);
  }
}

import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

import { ShippingDetailComponent } from './shipping-detail.component';

import { ShippingDetailRestService } from './shipping-detail-rest.service';

import { SharedModule } from '../../shared/shared.module';

export const PROVIDERS = [
  ShippingDetailRestService,
];

@NgModule({
  imports: [
    CommonModule,
    FlexLayoutModule,
    SharedModule,
    TranslateModule,
  ],
  declarations: [
    ShippingDetailComponent,
  ],
  exports: [
    ShippingDetailComponent,
  ]
})
export class ShippingDetailModule {

}

import * as _ from 'lodash';
import { Component } from '@angular/core';
import 'app/rxjs-imports.ts';

import { ReceiveMoneyRestService } from '../receive-money-rest.service';
import { ReceiveMoneyService } from '../receive-money.service';

import { BaseCreateBComponent } from '../../../shared/base/base.module';
import { MReceiveMoney } from '../receive-money.model';

@Component({
  selector: 'app-receive-money-create',
  templateUrl: 'receive-money-create.component.html'
})
export class ReceiveMoneyCreateComponent extends BaseCreateBComponent {
  constructor(
    private _receiveMoney: ReceiveMoneyService,
    private _receiveMoneyRest: ReceiveMoneyRestService,
  ) {
    super();

    this.componentId = 'ReceiveMoneyCreate';
    this.headerTitle = 'ui.receiveMoney.create.title';
    this.containerType = 1;
    this.routeURL = '/receive-moneys';
    this.entrySuccessI18n = 'success.receiveMoney.create';

    this.registerHook('buildForm', event => {
      this._receiveMoney.setFormDefinitions(this.form);
    });

    this.registerHook('save', event => {
      let receiveMoney = new MReceiveMoney;

      receiveMoney = _.assign(receiveMoney, this.form.value);
      this._receiveMoney.normalizeDoc(receiveMoney);

      return this._receiveMoneyRest.create(receiveMoney);
    });
  }
}

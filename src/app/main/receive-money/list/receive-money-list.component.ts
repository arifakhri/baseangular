import * as SnackBar from 'node-snackbar';
import swal from 'sweetalert2';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { DataTable } from 'primeng/components/datatable/datatable';
import { Observable, Subscription } from 'rxjs';
import 'app/rxjs-imports.ts';
import { TranslateService } from '@ngx-translate/core';

import { SpinnerDirective } from '../../../shared/spinner/spinner.directive';

import {
  AccountingService,
  ExportDataTableService,
  GridTableDataSource,
  GridTableService,
  GridTableToggleService,
} from '../../../core/core.module';
import { ReceiveMoneyRestService } from '../receive-money-rest.service';
import { RetryDialogComponent } from '../../../shared/retry-dialog/retry-dialog.component';

@Component({
  selector: 'app-receive-money-list',
  templateUrl: 'receive-money-list.component.html'
})
export class ReceiveMoneyListComponent implements OnInit {
  @ViewChild('gridTable') public gridTable: DataTable;
  @ViewChild('pageLoading') elPageLoading: SpinnerDirective;
  @ViewChild('retryDialog') elRetryDialog: RetryDialogComponent;

  compReady: boolean;
  gridDataSource: GridTableDataSource<IReceiveMoney> = new GridTableDataSource<IReceiveMoney>();
  selectedRecords: IReceiveMoney[] = [];

  combinedFilterValues: string = '';
  combinedFilterFields = {};

  tableColumns: IGridTableColumn[] = [{
    i18nLabel: 'ui.receiveMoney.list.column.transactionDate',
    field: 'transactionDate',
    sort: true,
  }, {
    i18nLabel: 'ui.receiveMoney.list.column.transactionNumber',
    field: 'transactionNumber',
    link: (row: IReceiveMoney) => {
      return `/receive-moneys/${row.id}`;
    },
    sort: true,
  }, {
    i18nLabel: 'ui.receiveMoney.list.column.description',
    field: 'description',
    sort: true,
  }, {
    i18nLabel: 'ui.receiveMoney.list.column.payFrom',
    field: 'paymentAccount.name',
    sort: true,
  }, {
    i18nLabel: 'ui.receiveMoney.list.column.payer',
    field: 'payer.displayName',
    sort: true,
  }, {
    i18nLabel: 'ui.receiveMoney.list.column.total',
    field: 'total',
    sort: true,
    formatter: (value, row: IReceiveMoney) => {
      return this._accounting.ac.formatMoney(value, '');
    },
    columnClasses: 'right-align',
  }];
  tableColumnsShow: boolean = false;
  tableColumnsToggle = this._gridTableToggle.mapToggleOptionsFromColumns(this.tableColumns);

  retryDialogShownState: boolean = false;
  retryDialogOnRetry: Function;

  private loadDataTimeoutId: number;
  private gridFirstLoad: boolean = true;
  private subscriptions: { [key: string]: Subscription[] } = {
    load: []
  };

  constructor(
    private _accounting: AccountingService,
    private _activatedRoute: ActivatedRoute,
    private _dataTableExport: ExportDataTableService,
    private _gridTable: GridTableService,
    private _gridTableToggle: GridTableToggleService,
    private _receiveMoneyRest: ReceiveMoneyRestService,
    private _translate: TranslateService,
  ) { }

  ngOnInit() {
    this.loadData();
  }

  gridLoadDataWrapper() {
    // Grid always load at the first time. we dont want that. we already call on ngOnInit
    if (this.gridFirstLoad) {
      this.gridFirstLoad = false;
      return;
    }

    if (this.loadDataTimeoutId) {
      window.clearTimeout(this.loadDataTimeoutId);
      this.loadDataTimeoutId = null;
    }

    this.loadDataTimeoutId = window.setTimeout(() => {
      this.loadData();
    }, 300);
  }

  showVoidDialog(receiveMoneyId: string) {
    swal({
      title: this._translate.instant('confirm.receiveMoney.void.label'),
      text: this._translate.instant('confirm.receiveMoney.void.description'),
      type: 'question',
      showCancelButton: true,
    })
      .then(() => {
        this.voidOrder(receiveMoneyId);
      })
      .catch(() => { });
  }

  voidOrder(receiveMoneyId: string) {
    this._receiveMoneyRest.void(receiveMoneyId).subscribe(response => {
      this.gridLoadDataWrapper();
      SnackBar.show({
        text: this._translate.instant('success.receiveMoney.void'),
        pos: 'bottom-right'
      });
    });
  }

  get exportRecords(): Observable<IReceiveMoney[]> {
    const qOption = {
      ...this._gridTable.generateApiQueryOptionFromGridInfo(
        this.gridDataSource.pager,
        false,
        this.gridTable,
        this._gridTable.generateCombinedApiFilters(this.combinedFilterValues, this.combinedFilterFields)
      ),
      skip: 0,
      take: this._dataTableExport.exportLimit,
    };

    return this.elRetryDialog.createRetryEntry(
      this._receiveMoneyRest.findAll(qOption).map(response => response.data)
    );
  }

  exportPDF() {
    this.exportRecords.subscribe(records => {
      this._dataTableExport.export({
        records,
        mapOptions: this._dataTableExport.mapFromTableColumns(this.tableColumns),
        fileName: 'all-receive-moneys',
        extension: 'pdf',
        templateData: {
          title: this._translate.instant('ui.receiveMoney.list.title'),
        },
      });
    });
  }

  exportExcel() {
    this.exportRecords.subscribe(records => {
      this._dataTableExport.export({
        records: records,
        mapOptions: this._dataTableExport.mapFromTableColumns(this.tableColumns),
        fileName: 'all-receive-moneys',
        extension: 'xls',
      });
    });
  }

  loadData() {
    this.elPageLoading.forceShow();

    const qOption = this._gridTable.generateApiQueryOptionFromGridInfo(
      this.gridDataSource.pager,
      true,
      this.gridTable,
      this._gridTable.generateCombinedApiFilters(this.combinedFilterValues, this.combinedFilterFields)
    );

    this.subscriptions.load.forEach(sub => sub.unsubscribe());
    this.subscriptions.load.push(
      this.elRetryDialog.createRetryEntry(
        this._receiveMoneyRest.findAll(qOption)
          .finally(() => {
            this.elPageLoading.forceHide();
          })
      ).subscribe(response => {
        this.gridDataSource.updateFromApiPaginationResult(response);
        this.compReady = true;
      })
    );
  }

  public onGridChange() {
    this.gridLoadDataWrapper();
  }

  public onGridPageChange() {
    this.gridLoadDataWrapper();
  }
}

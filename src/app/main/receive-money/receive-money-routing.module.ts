import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ReceiveMoneyCreateComponent } from './create/receive-money-create.component';
import { ReceiveMoneyDetailComponent } from './detail/receive-money-detail.component';
import { ReceiveMoneyListComponent } from './list/receive-money-list.component';
import { ReceiveMoneyUpdateComponent } from './update/receive-money-update.component';

import { AuthorizationService } from '../../core/core.module';

export const routes: Routes = [{
  path: '',
  children: [{
    path: '',
    component: ReceiveMoneyListComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'receiveMoney.list'
    }
  }, {
    path: 'create',
    component: ReceiveMoneyCreateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'receiveMoney.create'
    }
  }, {
    path: ':id/create',
    component: ReceiveMoneyCreateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'receiveMoney.create'
    }
  }, {
    path: ':id',
    component: ReceiveMoneyDetailComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'receiveMoney.detail'
    }
  }, {
    path: ':id/update',
    component: ReceiveMoneyUpdateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'receiveMoney.update'
    }
  }]
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class ReceiveMoneyRoutingModule { }

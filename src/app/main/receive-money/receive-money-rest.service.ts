import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';

import { ApiQueryOption, RequestService } from '../../core/core.module';
import { BusinessPartnerRestService } from '../business-partner/business-partner-rest.service';

import { APP_CONST } from '../../app.const';

@Injectable()
export class ReceiveMoneyRestService {
  baseURL = `${APP_CONST.API_MAIN}/receive-moneys`;
  request = this._request.new(this.baseURL);

  constructor(
    public _businessPartnerRest: BusinessPartnerRestService,
    private _request: RequestService,
  ) { }

  create(receiveMoney: IReceiveMoney) {
    return this.request.post<IReceiveMoney>(``, receiveMoney);
  }

  findAll(queryOption: ApiQueryOption, queryParams: any = {}) {
    return this.request.post<IApiPaginationResult<IReceiveMoney>>(`q`, queryOption, { params: queryParams });
  }

  findAllBusinessPartner(queryOption: ApiQueryOption = new ApiQueryOption): Observable<IApiPaginationResult<IBusinessPartner>> {
    return this._businessPartnerRest.findAll(queryOption);
  }

  load(receiveMoneyId: string, queryParams: any = {}) {
    return this.request.get<IReceiveMoney>(`${receiveMoneyId}`, { params: queryParams });
  }

  loadRelatedData(): Observable<{ accounts: IAccount[] }> {
    return this.request.get<{ accounts: IAccount[] }>(`entry-related-data`);
  }

  update(receiveMoneyId: string, updateObj: IReceiveMoney) {
    return this.request.put<IReceiveMoney>(`${receiveMoneyId}`, updateObj);
  }

  void(receiveMoneyId: string) {
    return this.request.put<IReceiveMoney>(`${receiveMoneyId}/void`, {});
  }
}

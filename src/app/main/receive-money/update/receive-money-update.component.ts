import * as moment from 'moment';
import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';

import { ReceiveMoneyService } from '../receive-money.service';
import { ReceiveMoneyRestService } from '../receive-money-rest.service';

import { BaseUpdateBComponent } from '../../../shared/base/base.module';

@Component({
  selector: 'app-receive-money-update',
  templateUrl: 'receive-money-update.component.html'
})
export class ReceiveMoneyUpdateComponent extends BaseUpdateBComponent {

  constructor(
    private _receiveMoney: ReceiveMoneyService,
    private _receiveMoneyRest: ReceiveMoneyRestService,
  ) {
    super();

    this.componentId = 'ReceiveMoneyUpdate';
    this.headerTitle = 'ui.receiveMoney.update.title';
    this.containerType = 1;
    this.routeURL = '/receive-moneys';
    this.entrySuccessI18n = 'success.receiveMoney.update';

    this.registerHook('buildForm', event => {
      this._receiveMoney.setFormDefinitions(this.form);
    });

    this.registerHook('load', event => {
      return this._receiveMoneyRest.load(this.page.routeParams.id).switchMap(doc => {
        if (doc.transactionDate) {
          doc.transactionDate = moment(doc.transactionDate).toDate();
        }
        return Observable.of(doc);
      });
    });

    this.registerHook('save', event => {
      const receiveMoney: IReceiveMoney = Object.assign({}, this.doc, this.form.value);
      this._receiveMoney.normalizeDoc(receiveMoney);
      return this._receiveMoneyRest.update(this.page.routeParams.id, receiveMoney);
    });
  }
}

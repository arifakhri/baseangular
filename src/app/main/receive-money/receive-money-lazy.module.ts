import { NgModule } from '@angular/core';

import { ReceiveMoneyModule } from './receive-money.module';
import { ReceiveMoneyRoutingModule } from './receive-money-routing.module';

@NgModule({
  imports: [
    ReceiveMoneyModule,
    ReceiveMoneyRoutingModule
  ]
})
export class ReceiveMoneyLazyModule { }

import { AutoCompleteModule } from 'primeng/components/autocomplete/autocomplete';
import { BsDropdownModule, ModalModule, PaginationModule } from 'ngx-bootstrap';
import { CalendarModule } from 'primeng/components/calendar/calendar';
import { CommonModule } from '@angular/common';
import { DataTableModule } from 'primeng/components/datatable/datatable';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ListboxModule } from 'primeng/components/listbox/listbox';
import { HttpModule } from '@angular/http';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { ReceiveMoneyCreateComponent } from './create/receive-money-create.component';
import { ReceiveMoneyDetailComponent } from './detail/receive-money-detail.component';
import { ReceiveMoneyFormComponent } from './form/receive-money-form.component';
import { ReceiveMoneyListComponent } from './list/receive-money-list.component';
import { ReceiveMoneyUpdateComponent } from './update/receive-money-update.component';

import { ReceiveMoneyRestService } from './receive-money-rest.service';
import { ReceiveMoneyService } from './receive-money.service';

import { BusinessPartnerModule } from '../business-partner/business-partner.module';
import { CoreModule } from '../../core/core.module';
import { SharedModule } from '../../shared/shared.module';
import { TransactionModule } from '../transaction/transaction.module';

export const PROVIDERS = [
  ReceiveMoneyRestService,
  ReceiveMoneyService,
];

@NgModule({
  imports: [
    AutoCompleteModule,
    BsDropdownModule,
    BusinessPartnerModule,
    CalendarModule,
    CommonModule,
    CoreModule,
    DataTableModule,
    FlexLayoutModule,
    FormsModule,
    HttpModule,
    ListboxModule,
    ModalModule,
    PaginationModule,
    ReactiveFormsModule,
    RouterModule,
    SharedModule,
    TransactionModule,
    TranslateModule,
  ],
  declarations: [
    ReceiveMoneyCreateComponent,
    ReceiveMoneyDetailComponent,
    ReceiveMoneyFormComponent,
    ReceiveMoneyListComponent,
    ReceiveMoneyUpdateComponent,
  ],
})
export class ReceiveMoneyModule { }

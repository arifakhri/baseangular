import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { ReceiveMoneyRestService } from '../receive-money-rest.service';
import { AccountingService } from '../../../core/core.module';

@Component({
  selector: 'app-receive-money-detail',
  templateUrl: 'receive-money-detail.component.html'
})
export class ReceiveMoneyDetailComponent implements OnInit {
  doc: IReceiveMoney;
  routeParams: any;

  constructor(
    public _accounting: AccountingService,
    private _receiveMoneyRest: ReceiveMoneyRestService,
    private _route: ActivatedRoute,
  ) {
    this._route.params.subscribe(params => this.routeParams = params);
  }

  ngOnInit() {
    this.loadData();
  }

  loadData() {
    this._receiveMoneyRest.load(this.routeParams.id).subscribe(receiveMoney => {
      this.doc = receiveMoney;
    });
  }
}

import * as _ from 'lodash';
import { AutoComplete } from 'primeng/components/autocomplete/autocomplete';
import { ChangeDetectorRef, Component, OnChanges, OnInit, SimpleChanges, ViewChild, } from '@angular/core';
import { FormGroup } from '@angular/forms';
import 'app/rxjs-imports.ts';

import { AccountingService, AutocompleteService, CommonService } from '../../../core/core.module';
import { ReceiveMoneyRestService } from '../receive-money-rest.service';
import { ReceiveMoneyService } from '../receive-money.service';

import { TransactionFormBComponent } from '../../transaction/transaction-form.bcomponent';

@Component({
  selector: 'app-receive-money-form',
  templateUrl: 'receive-money-form.component.html',
})
export class ReceiveMoneyFormComponent extends TransactionFormBComponent implements OnChanges, OnInit {
  @ViewChild('payerAC') elPayerAC: AutoComplete;

  payerChanged: boolean = false;

  ACPayerHandler = CommonService.remoteACItemsHandler({
    remoteParams: this.ACPayerParams.bind(this),
    remoteRequest: this._receiveMoneyRest.findAllBusinessPartner.bind(this._receiveMoneyRest),
    remoteRequestMap: (response) => response.data,
    element: () => this.elPayerAC,
    ACConfig: {
      prependNull: true,
    },
  });

  constructor(
    public _autocomplete: AutocompleteService,
    public _accounting: AccountingService,
    private _changeDetectorRef: ChangeDetectorRef,
    private _receiveMoney: ReceiveMoneyService,
    private _receiveMoneyRest: ReceiveMoneyRestService,
  ) {
    super();
    this.componentId = 'ReceiveMoneyForm';
    this.withSubTotal = false;

    this.registerHook('loadRelated', event => {
      return this._receiveMoneyRest.loadRelatedData();
    });

    this.registerHook('patchForm', event => {
      this.applyDoc();
    });
  }

  ngOnInit() {
    super.ngOnInit();

    this.addLines();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (_.has(changes, 'doc') && this.doc) {
      this.applyDoc();
    }
  }

  syncTotal() {
    const formValueLines = this.form.get('lines').value;
    const total = _.sumBy(formValueLines, 'amount');
    this.form.get('total').setValue(total);
  }

  ACPayerParams(event: any, type: string) {
    let filters = [];

    if (type === 'search') {
      filters = [{
        filterValues: [{
          field: 'displayName',
          operator: 'contains',
          value: event.query
        }]
      }];
    }

    return [{
      filter: filters,
      sort: [{
        field: 'displayName',
        dir: 'asc'
      }],
      take: 30,
      skip: 0,
      includeTotalCount: false
    }];
  }

  buildFormChildLine() {
    return this._receiveMoney.buildFormChildLine();
  }

  lineConditionalValidation(formGroup: FormGroup) {
    return this._receiveMoney.lineConditionalValidation(formGroup);
  }
}

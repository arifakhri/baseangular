import { NgModule } from '@angular/core';

import { RegistrationModule } from './registration.module';
import { RegistrationRoutingModule } from './registration-routing.module';

@NgModule({
  imports: [
    RegistrationModule,
    RegistrationRoutingModule
  ]
})
export class RegistrationLazyModule { }

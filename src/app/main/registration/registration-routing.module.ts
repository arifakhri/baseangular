import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { RegistrationAccountComponent } from './account/registration-account.component';
import { RegistrationCompanyComponent } from './company/registration-company.component';

import { AuthorizationService } from '../../core/core.module';

export const routes: Routes = [{
  path: '',
  children: [{
    path: '',
    redirectTo: 'account',
    pathMatch: 'full',
  }, {
    path: 'account',
    component: RegistrationAccountComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'registration.account'
    }
  }, {
    path: 'company',
    component: RegistrationCompanyComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'registration.company'
    }
  }],
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class RegistrationRoutingModule { }

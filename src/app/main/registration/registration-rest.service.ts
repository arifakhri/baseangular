import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';

import { RequestService } from '../../core/core.module';

import { APP_CONST } from '../../app.const';

@Injectable()
export class RegistrationRestService {
  baseURL = `${APP_CONST.API_ACCOUNT}`;
  request = this._request.new(this.baseURL);

  constructor(
    private _request: RequestService,
  ) { }

  registerAccount(registration: IRegistrationAccount) {
    return this.request.post<IRegistrationAccount>('users/signup', registration);
  }

  registerCompany(registration: IRegistrationCompany) {
    return this.request.post<IRegistrationCompany>('company/register', registration);
  }
}

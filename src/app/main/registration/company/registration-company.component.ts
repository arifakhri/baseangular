import { ChangeDetectorRef, Component, Inject, OnInit, ViewChild } from '@angular/core';
import { CustomValidators } from 'ng2-validation';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';

import {
  ApiBootstrapService,
  AuthenticationService,
  AutocompleteService,
  CommonService,
  SystemMessageService
} from '../../../core/core.module';
import { SpinnerService } from '../../../shared/spinner/spinner.service';
import { RegistrationCompanyService } from './registration-company.service';
import { RegistrationRestService } from '../registration-rest.service';
import { RetryDialogComponent } from '../../../shared/retry-dialog/retry-dialog.component';

@Component({
  selector: 'app-registration-company',
  templateUrl: 'registration-company.component.html',
  styleUrls: ['./registration-company.component.scss'],
  providers: [SystemMessageService]
})
export class RegistrationCompanyComponent implements OnInit {
  @ViewChild('retryDialog') elRetryDialog: RetryDialogComponent;

  compReady: boolean = false;
  form: FormGroup = new FormGroup({});

  retryDialogShownState: boolean = false;
  retryDialogOnRetry: Function;

  constructor(
    @Inject('GlobalSystemMessage') private _globalSystemMessage: SystemMessageService,
    public _autocomplete: AutocompleteService,
    private _apiBootstrap: ApiBootstrapService,
    private _authentication: AuthenticationService,
    private _changeDetectorRef: ChangeDetectorRef,
    private _spinner: SpinnerService,
    private _registrationCompany: RegistrationCompanyService,
    private _registrationRest: RegistrationRestService,
    public _systemMessage: SystemMessageService,
    private _translate: TranslateService,
  ) {
    this.buildForm();
  }

  ngOnInit() {
  }

  buildForm() {
    this.form.addControl('companyName', new FormControl('', Validators.required));
    this.form.addControl('industry', new FormControl('', Validators.required));
    this.form.addControl('FiscalYearStartMonth', new FormControl(1, [CustomValidators.digits, Validators.required]));
  }

  onSubmit() {
    if (this.formValid()) {
      this.register();
    }
  }

  formValid(): boolean {
    if (!this.form.valid) {
      CommonService.markAsDirty(this.form);
      this._systemMessage.log({
        message: this._translate.instant('error.form'),
        type: 'error'
      });
      return false;
    }
    return true;
  }

  register(): void {
    const spinner = this._spinner.show({
      element: $('.registration-box'),
    });

    const registration: IRegistrationCompany = this.form.value;
    // this._registrationCompany.normalizeDoc(registration);
    this._registrationRest.registerCompany(registration)
      .catch(error => {
        spinner.dispose();

        this._systemMessage.log({
          message: error,
          type: 'error'
        });
        return Observable.throw(error);
      })
      .subscribe(result => {
        this._authentication.refreshToken(result.companyId).subscribe(() => {
          this._apiBootstrap.boot(true).subscribe(() => {
            spinner.dispose();

            this._globalSystemMessage.log({
              message: this._translate.instant('success.registrationCompany.register'),
              type: 'success',
              showAs: 'growl',
              showSnackBar: false,
            });

            this._authentication.redirectBasedOnRole();
          });
        });
      });
  }
}

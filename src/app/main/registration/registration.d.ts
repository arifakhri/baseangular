declare interface IRegistrationAccount {
  id: string;
  email: string;
  userName: string;
  firstName: string;
  lastName: string;
  fullName: string;
  dateOfBirth: string;
  gender: string;
  pictureId: string;
  picture: {
    id: string;
    fileType: string;
    fileName: string;
    fileExt: string;
    fileSize: number;
    description: string;
    uploadByUserId: string;
    fileUrl: string;
    thumbnailUrl: string;
  };
  emailConfirmed: boolean;
  phoneNumberConfirmed: boolean;
}

declare interface IRegistrationCompany {
  id: string;
  companyId: string;
  companyName: string;
  industry: string;
  fiscalYearStartMonth: string;
}
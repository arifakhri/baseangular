import { ChangeDetectorRef, Component, Inject, ViewEncapsulation } from '@angular/core';
import { CustomValidators } from 'ng2-validation';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

import { ApiBootstrapService, AuthenticationService, CommonService, SystemMessageService } from '../../../core/core.module';
import { InvitationRegistrationService } from '../../invitation/invitation-registration.service';
import { SpinnerService } from '../../../shared/spinner/spinner.service';
import { RegistrationRestService } from '../registration-rest.service';

@Component({
  selector: 'app-registration-account',
  templateUrl: 'registration-account.component.html',
  styleUrls: ['./registration-account.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [SystemMessageService]
})
export class RegistrationAccountComponent {
  form: FormGroup;
  registrationByInvitation: boolean = false;

  constructor(
    @Inject('GlobalSystemMessage') private _globalSystemMessage: SystemMessageService,
    private _apiBootstrap: ApiBootstrapService,
    private _authentication: AuthenticationService,
    private _changeDetectorRef: ChangeDetectorRef,
    private _formBuilder: FormBuilder,
    private _invitationRegistration: InvitationRegistrationService,
    private _spinner: SpinnerService,
    private _registrationRest: RegistrationRestService,
    private _router: Router,
    public _systemMessage: SystemMessageService,
    private _translate: TranslateService,
  ) {
    this.buildForm();

    if (this._invitationRegistration.invitaiton) {
      this._invitationRegistration.patchRegistrationForm(this.form);

      this.registrationByInvitation = true;
    }
  }

  buildForm() {
    const firstNameControl = new FormControl('', [Validators.required, Validators.maxLength(30)]);
    (<any>firstNameControl).validatorData = {
      maxLength: 50
    };
    const lastNameControl = new FormControl('', [Validators.required, Validators.maxLength(30)]);
    (<any>lastNameControl).validatorData = {
      maxLength: 50
    };
    const userNameControl = new FormControl('', [Validators.required, Validators.maxLength(30)]);
    (<any>userNameControl).validatorData = {
      maxLength: 150
    };
    const emailControl = new FormControl('', [CustomValidators.email, Validators.required, Validators.maxLength(150)]);
    (<any>emailControl).validatorData = {
      maxLength: 150
    };

    this.form = this._formBuilder.group({
      firstName: firstNameControl,
      lastName: lastNameControl,
      userName: userNameControl,
      email: emailControl,
      phoneNumberCountryId: new FormControl('id', Validators.required),
      nationalPhoneNumber: new FormControl('', [CustomValidators.digits, Validators.required]),
      password: new FormControl('', Validators.required),

      _passwordConfirm: new FormControl('', Validators.required),
    },
      {
        validator: this.matchingPasswords('password', '_passwordConfirm')
      }
    );
  };

  matchingPasswords(passwordKey: string, confirmPasswordKey: string) {
    return (group: FormGroup): {
      [key: string]: any
    } => {
      const password = group.controls[passwordKey];
      const confirmPassword = group.controls[confirmPasswordKey];
      if (password.value !== confirmPassword.value) {
        return {
          mismatchedPasswords: true
        };
      }
    };
  }

  onSubmit() {
    if (this.formValid()) {
      this.register();
    }
  }

  formValid(): boolean {
    if (!this.form.valid) {
      CommonService.markAsDirty(this.form);
      this._systemMessage.log({
        message: this._translate.instant('error.form'),
        type: 'error'
      });
      return false;
    }
    return true;
  }

  register(): void {
    const spinner = this._spinner.show({
      element: $('.registration-box'),
    });

    const registration: IRegistrationAccount = this.form.value;
    this._registrationRest.registerAccount(registration)
      .catch(error => {
        spinner.dispose();
        this._systemMessage.log({
          message: error,
          type: 'error'
        });
        return Observable.throw(error);
      })
      .subscribe(result => {
        this._globalSystemMessage.log({
          message: this._translate.instant('success.registrationAccount.register'),
          type: 'success',
          showAs: 'growl',
          showSnackBar: false,
        });

        if (this.registrationByInvitation) {
          spinner.dispose();
          this._router.navigateByUrl('/login');
        } else {
          this._router.navigate(['/account/confirmation'], { queryParams: { email: this.form.value.email } });
        }
      });
  }
}

import { CheckboxModule } from 'primeng/components/checkbox/checkbox';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';

import { RegistrationAccountComponent } from './account/registration-account.component';
import { RegistrationCompanyComponent } from './company/registration-company.component';

import { RegistrationCompanyService } from './company/registration-company.service';
import { RegistrationRestService } from './registration-rest.service';

import { CoreModule } from '../../core/core.module';
import { SharedModule as CentricSharedModule } from '../../modules/ng2centric/shared/shared.module';
import { SharedModule } from '../../shared/shared.module';

export const PROVIDERS = [
  RegistrationCompanyService,
  RegistrationRestService,
];

@NgModule({
  imports: [
    CentricSharedModule,
    CheckboxModule,
    CommonModule,
    CoreModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    SharedModule,
    TranslateModule.forChild(),
  ],
  declarations: [
    RegistrationAccountComponent,
    RegistrationCompanyComponent
  ]
})
export class RegistrationModule { }

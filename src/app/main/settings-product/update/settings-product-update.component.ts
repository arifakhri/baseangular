import { ActivatedRoute, Router } from '@angular/router';
import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';
import { TranslateService } from '@ngx-translate/core';

import { SpinnerService } from '../../../shared/spinner/spinner.service';
import { SettingsProductRestService } from '../settings-product-rest.service';
import { SettingsProductService } from '../settings-product.service';
import { SystemMessageService } from '../../../core/core.module';
import { RetryDialogComponent } from '../../../shared/retry-dialog/retry-dialog.component';

@Component({
  selector: 'app-settings-product-update',
  templateUrl: 'settings-product-update.component.html',
  providers: [SystemMessageService]
})
export class SettingsProductUpdateComponent implements OnInit {
  @ViewChild('retryDialog') elRetryDialog: RetryDialogComponent;

  compReady: boolean;
  doc: ISettingsProduct;
  form: FormGroup = new FormGroup({});

  retryDialogShownState: boolean = false;
  retryDialogOnRetry: Function;

  constructor(
    @Inject('GlobalSystemMessage') private _globalSystemMessage: SystemMessageService,
    private _spinner: SpinnerService,
    private _settingsProduct: SettingsProductService,
    private _settingsProductRest: SettingsProductRestService,
    private _route: ActivatedRoute,
    private _router: Router,
    public _systemMessage: SystemMessageService,
    private _translate: TranslateService,
  ) { }

  ngOnInit() {
    this._settingsProduct.setFormDefinitions(this.form);

    this.loadData();
  }

  loadData() {
    this.elRetryDialog.createRetryEntry(this._settingsProductRest.load())
      .subscribe(settingCompany => {
        this.doc = settingCompany;
        this.form.patchValue(this.doc);

        this.compReady = true;
      });
  }

  save(): void {
    const spinner = this._spinner.showDefault();

    const settingsProduct: ISettingsProduct = Object.assign({}, this.doc, this.form.value);
    this._settingsProductRest.update(settingsProduct)
      .catch(error => {
        spinner.dispose();

        this._systemMessage.log({
          message: error,
          type: 'error'
        });
        return Observable.throw(error);
      })
      .subscribe(result => {
        spinner.dispose();

        this.doc = result;
        this._globalSystemMessage.log({
          message: this._translate.instant('success.settingsProduct.update'),
          type: 'success',
          showAs: 'growl',
          showSnackBar: false,
        });
      });
  }

  onSubmit() {
    this.save();
  }
}

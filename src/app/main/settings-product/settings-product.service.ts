import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class SettingsProductService {
  constructor(
    private _formBuilder: FormBuilder,
    private _translate: TranslateService
  ) { }

  setFormDefinitions(form: FormGroup) {
    form.addControl('defaultUomId', new FormControl);
    form.addControl('allowInventoryLevelBelowZero', new FormControl(true));
  }
}

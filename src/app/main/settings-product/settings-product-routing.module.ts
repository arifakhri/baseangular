import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SettingsProductUpdateComponent } from './update/settings-product-update.component';

import { AuthorizationService } from '../../core/core.module';

export const routes: Routes = [{
  path: '',
  children: [{
    path: '',
    component: SettingsProductUpdateComponent,
    canActivate: [AuthorizationService],
    data: {
      name: 'settingsProduct.update'
    }
  }]
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class SettingsProductRoutingModule { }

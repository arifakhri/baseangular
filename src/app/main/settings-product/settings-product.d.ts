declare interface ISettingsProduct {
  masterPriceLevelId: string;
  masterWarehouseId: string;
  allowInventoryLevelBelowZero: boolean;
  defaultUomId: string;
  masterPriceLevel: {
    id: string;
    name: string;
    isMaster: boolean
  };
  masterWarehouse: {
    id: string;
    name: string;
    isMaster: boolean
  };
  defaultUom: {
    id: string;
    name: string;
    systemDefault: boolean
  };
}

import { NgModule } from '@angular/core';
import { SettingsProductModule } from './settings-product.module';
import { SettingsProductRoutingModule } from './settings-product-routing.module';

@NgModule({
  imports: [
    SettingsProductModule,
    SettingsProductRoutingModule
  ]
})
export class SettingsProductLazyModule { }

import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-settings-product-form',
  templateUrl: 'settings-product-form.component.html'
})
export class SettingsProductFormComponent {
  @Input() doc: string;
  @Input() form: FormGroup;
  @Output() submit: EventEmitter<any> = new EventEmitter();

  updateForm: boolean = false;

  onSubmit() {
    this.submit.emit();
  }
}

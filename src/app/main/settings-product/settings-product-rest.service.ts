import { Injectable } from '@angular/core';
import 'app/rxjs-imports.ts';

import { RequestService } from '../../core/core.module';

import { APP_CONST } from '../../app.const';

@Injectable()
export class SettingsProductRestService {
  baseURL = `${APP_CONST.API_MAIN}/settings`;

  request = this._request.new(this.baseURL);

  constructor(
    private _request: RequestService,
  ) { }

  load() {
    return this.request.get<ISettingsProduct>(`product`);
  }

  update(updateObj: ISettingsProduct) {
    return this.request.put<ISettingsProduct>(`product`, updateObj);
  }
}

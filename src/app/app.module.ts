import { AccordionModule, AlertModule, BsDropdownModule, ModalModule, PaginationModule, PopoverModule } from 'ngx-bootstrap';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireModule } from 'angularfire2';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ConditionallyValidateModule } from 'ng-conditionally-validate';
import { ConnectionBackend, HttpModule, XHRBackend } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { Ng2Webstorage } from 'ngx-webstorage';
import { ActionReducerMap, StoreModule } from '@ngrx/store';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { AppComponent } from './app.component';

import { AppMenuModule } from './app-menu.module';
import { AppRoutingModule } from './app-routing.module';
import { CoreModule as CentricCoreModule } from './modules/ng2centric/core/core.module';
import { CoreModule } from './core/core.module';
import { LayoutsModule } from './layouts/layouts.module';
import { SharedModule } from './shared/shared.module';

import { PROVIDERS as AccountProviders } from './main/account/account.module';
import { PROVIDERS as BusinessPartnerProviders } from './main/business-partner/business-partner.module';
import { PROVIDERS as CashBankRestProviders } from './main/cash-bank/cash-bank.module';
import { PROVIDERS as ChatbotProviders } from './main/chatbot/chatbot.module';
import { PROVIDERS as COAProviders } from './main/coa/coa.module';
import { PROVIDERS as ContactProviders } from './main/contact/contact.module';
import { PROVIDERS as DashboardProviders } from './main/dashboard/dashboard.module';
import { PROVIDERS as EmployeeProviders } from './main/employee/employee.module';
import { PROVIDERS as ExpenseProviders } from './main/expense/expense.module';
import { PROVIDERS as InvitationProviders } from './main/invitation/invitation.module';
import { PROVIDERS as JournalEntryProviders } from './main/journal-entry/journal-entry.module';
import { PROVIDERS as PaymentMethodProviders } from './main/payment-method/payment-method.module';
import { PROVIDERS as PriceLevelProviders } from './main/price-level/price-level.module';
import { PROVIDERS as ProductProviders, STORES as ProductStores } from './main/product/product.module';
import { PROVIDERS as ProductAttributeProviders } from './main/product-attribute/product-attribute.module';
import { PROVIDERS as ProductBrandProviders } from './main/product-brand/product-brand.module';
import { PROVIDERS as ProductCategoryProviders } from './main/product-category/product-category.module';
import { PROVIDERS as ProductTagProviders } from './main/product-tag/product-tag.module';
import { PROVIDERS as ProductVariantProviders } from './main/product-variant/product-variant.module';
import { PROVIDERS as ProfileProviders } from './main/profile/profile.module';
import { PROVIDERS as PurchasesDebitNoteProviders } from './main/purchases-debit-note/purchases-debit-note.module';
import { PROVIDERS as PurchasesDownPaymentProviders } from './main/purchases-down-payment/purchases-down-payment.module';
import { PROVIDERS as PurchasesInvoiceProviders } from './main/purchases-invoice/purchases-invoice.module';
import { PROVIDERS as PurchasesOrderProviders } from './main/purchases-order/purchases-order.module';
import { PROVIDERS as PurchasesPaymentProviders } from './main/purchases-payment/purchases-payment.module';
import { PROVIDERS as PurchasesProviders, STORES as PurchasesStores } from './main/purchases/purchases.module';
import { PROVIDERS as PurchasesReceiptProviders } from './main/purchases-receipt/purchases-receipt.module';
import { PROVIDERS as PurchasesRefundProviders } from './main/purchases-refund/purchases-refund.module';
import { PROVIDERS as ReceiveMoneyRestProviders } from './main/receive-money/receive-money.module';
import { PROVIDERS as ReportsProviders } from './main/reports/reports.module';
import { PROVIDERS as ReportsAPAgingProviders } from './main/reports-ap-aging/reports-ap-aging.module';
import { PROVIDERS as ReportsARAgingProviders } from './main/reports-ar-aging/reports-ar-aging.module';
import { PROVIDERS as ReportsBalanceProviders } from './main/reports-balance/reports-balance.module';
import { PROVIDERS as ReportsExpenseProviders } from './main/reports-expense/reports-expense.module';
import { PROVIDERS as ReportsExpenseDetailProviders } from './main/reports-expense-detail/reports-expense-detail.module';
import { PROVIDERS as ReportsInventoryProviders } from './main/reports-inventory/reports-inventory.module';
import { PROVIDERS as ReportsPNLProviders } from './main/reports-pnl/reports-pnl.module';
import { PROVIDERS as ReportsPurchasesProviders } from './main/reports-purchases/reports-purchases.module';
import { PROVIDERS as ReportsPurchasesDetailProviders } from './main/reports-purchases-detail/reports-purchases-detail.module';
import { PROVIDERS as ReportsPurchasesOrderProviders } from './main/reports-purchases-order/reports-purchases-order.module';
import { PROVIDERS as ReportsPurchasesOrderDetailProviders } from './main/reports-purchases-order-detail/reports-purchases-order-detail.module';
import { PROVIDERS as ReportsPurchasesVendorProviders } from './main/reports-purchases-vendor/reports-purchases-vendor.module';
import { PROVIDERS as ReportsPurchasesProductProviders } from './main/reports-purchases-product/reports-purchases-product.module';
import { PROVIDERS as ReportsSalesProviders } from './main/reports-sales/reports-sales.module';
import { PROVIDERS as ReportsSalesDetailProviders } from './main/reports-sales-detail/reports-sales-detail.module';
import { PROVIDERS as ReportsSalesOrderProviders } from './main/reports-sales-order/reports-sales-order.module';
import { PROVIDERS as ReportsSalesCustomerProviders } from './main/reports-sales-customer/reports-sales-customer.module';
import { PROVIDERS as ReportsSalesProductProviders } from './main/reports-sales-product/reports-sales-product.module';
import { PROVIDERS as ReportsSalesOrderDetailProviders } from './main/reports-sales-order-detail/reports-sales-order-detail.module';
import { PROVIDERS as RegistrationProviders } from './main/registration/registration.module';
import { PROVIDERS as SalesChannelProviders } from './main/sales-channel/sales-channel.module';
import { PROVIDERS as SalesCreditNoteProviders } from './main/sales-credit-note/sales-credit-note.module';
import { PROVIDERS as SalesDownPaymentProviders } from './main/sales-down-payment/sales-down-payment.module';
import { PROVIDERS as SalesInvoiceProviders } from './main/sales-invoice/sales-invoice.module';
import { PROVIDERS as SalesOrderProviders } from './main/sales-order/sales-order.module';
import { PROVIDERS as SalesPaymentProviders } from './main/sales-payment/sales-payment.module';
import { PROVIDERS as SalesProviders, STORES as SalesStores } from './main/sales/sales.module';
import { PROVIDERS as SalesReceiptProviders } from './main/sales-receipt/sales-receipt.module';
import { PROVIDERS as SalesRefundProviders } from './main/sales-refund/sales-refund.module';
import { PROVIDERS as SettingsAccountingProviders } from './main/settings-accounting/settings-accounting.module';
import { PROVIDERS as SettingsChatbotProviders } from './main/settings-chatbot/settings-chatbot.module';
import { PROVIDERS as SettingsCompanyProviders } from './main/settings-company/settings-company.module';
import { PROVIDERS as SettingsProductProviders } from './main/settings-product/settings-product.module';
import { PROVIDERS as SettingsProviders } from './main/settings/settings.module';
import { PROVIDERS as SettingsPurchaseProviders } from './main/settings-purchase/settings-purchase.module';
import { PROVIDERS as SettingsSalesChannelProviders } from './main/settings-sales-channel/settings-sales-channel.module';
import { PROVIDERS as SettingsSalesProviders } from './main/settings-sales/settings-sales.module';
import { PROVIDERS as SettingsUserProviders } from './main/settings-user/settings-user.module';
import { PROVIDERS as ShippingDetailProviders } from './main/shipping-detail/shipping-detail.module';
import { PROVIDERS as ShippingPriceProviders } from './main/shipping-price/shipping-price.module';
import { PROVIDERS as ShippingMethodProviders } from './main/shipping-method/shipping-method.module';
import { PROVIDERS as SpendMoneyRestProviders } from './main/spend-money/spend-money.module';
import { PROVIDERS as StockOpnameProviders } from './main/stock-opname/stock-opname.module';
import { PROVIDERS as TaxProviders } from './main/tax/tax.module';
import { PROVIDERS as TransactionProviders } from './main/transaction/transaction.module';
import { PROVIDERS as UOMProviders } from './main/uom/uom.module';
import { PROVIDERS as WarehouseProviders } from './main/warehouse/warehouse.module';
import { PROVIDERS as WarehouseTransferProviders } from './main/warehouse-transfer/warehouse-transfer.module';
import { environment } from '../environments/environment';

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, '../assets/i18n/', '.json');
}

export const APPLICATION_STORES: ActionReducerMap<any> = {
  ...ProductStores,
  ...PurchasesStores,
  ...SalesStores,
};

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    AccordionModule.forRoot(),
    AlertModule.forRoot(),
    AngularFirestoreModule,
    AngularFireAuthModule,
    AngularFireDatabaseModule,
    AngularFireModule.initializeApp(environment.firebase),
    AppMenuModule,
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    BsDropdownModule.forRoot(),
    CentricCoreModule,
    ConditionallyValidateModule,
    CoreModule.forRoot(),
    FormsModule,
    HttpClientModule,
    HttpModule,
    LayoutsModule.forRoot(),
    ModalModule.forRoot(),
    Ng2Webstorage.forRoot({
      separator: '.',
      prefix: 'clodeo-web'
    }),
    PaginationModule.forRoot(),
    PopoverModule.forRoot(),
    SharedModule.forRoot(),
    StoreModule.forRoot(APPLICATION_STORES),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    }),
  ],
  providers: [
    { provide: LocationStrategy, useClass: HashLocationStrategy },
    { provide: ConnectionBackend, useClass: XHRBackend },

    ...AccountProviders,
    ...BusinessPartnerProviders,
    ...CashBankRestProviders,
    ...ChatbotProviders,
    ...COAProviders,
    ...ContactProviders,
    ...DashboardProviders,
    ...EmployeeProviders,
    ...ExpenseProviders,
    ...InvitationProviders,
    ...JournalEntryProviders,
    ...PaymentMethodProviders,
    ...PriceLevelProviders,
    ...ProductProviders,
    ...ProductAttributeProviders,
    ...ProductBrandProviders,
    ...ProductCategoryProviders,
    ...ProductTagProviders,
    ...ProductVariantProviders,
    ...ProfileProviders,
    ...PurchasesDebitNoteProviders,
    ...PurchasesDownPaymentProviders,
    ...PurchasesInvoiceProviders,
    ...PurchasesOrderProviders,
    ...ReportsPurchasesOrderDetailProviders,
    ...PurchasesPaymentProviders,
    ...PurchasesProviders,
    ...PurchasesReceiptProviders,
    ...PurchasesRefundProviders,
    ...ReceiveMoneyRestProviders,
    ...RegistrationProviders,
    ...ReportsProviders,
    ...ReportsAPAgingProviders,
    ...ReportsARAgingProviders,
    ...ReportsBalanceProviders,
    ...ReportsExpenseProviders,
    ...ReportsExpenseDetailProviders,
    ...ReportsInventoryProviders,
    ...ReportsPNLProviders,
    ...ReportsPurchasesProviders,
    ...ReportsPurchasesDetailProviders,
    ...ReportsPurchasesOrderProviders,
    ...ReportsPurchasesVendorProviders,
    ...ReportsPurchasesProductProviders,
    ...ReportsSalesProviders,
    ...ReportsSalesDetailProviders,
    ...ReportsSalesOrderProviders,
    ...ReportsSalesCustomerProviders,
    ...ReportsSalesProductProviders,
    ...ReportsSalesOrderDetailProviders,
    ...SalesChannelProviders,
    ...SalesCreditNoteProviders,
    ...SalesDownPaymentProviders,
    ...SalesInvoiceProviders,
    ...SalesOrderProviders,
    ...SalesPaymentProviders,
    ...SalesProviders,
    ...SalesReceiptProviders,
    ...SalesRefundProviders,
    ...SettingsAccountingProviders,
    ...SettingsChatbotProviders,
    ...SettingsCompanyProviders,
    ...SettingsProductProviders,
    ...SettingsProviders,
    ...SettingsPurchaseProviders,
    ...SettingsSalesChannelProviders,
    ...SettingsSalesProviders,
    ...SettingsUserProviders,
    ...ShippingDetailProviders,
    ...ShippingPriceProviders,
    ...ShippingMethodProviders,
    ...SpendMoneyRestProviders,
    ...StockOpnameProviders,
    ...TaxProviders,
    ...TransactionProviders,
    ...UOMProviders,
    ...WarehouseProviders,
    ...WarehouseTransferProviders,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-fieldset-accordion',
  templateUrl: './fieldset-accordion.component.html',
})
export class FieldsetAccordionComponent {
  @Input() checkbox: boolean = false;
}

import 'app/rxjs-imports.ts';

import { Component, ElementRef, ViewChild, ViewEncapsulation } from '@angular/core';
import { Observable } from 'rxjs';

import { SpinnerService } from '../spinner/spinner.service';
import { MRetryDialogEntry } from './retry-dialog.model';

@Component({
  selector: 'app-retry-dialog',
  exportAs: 'app-retry-dialog',
  styleUrls: ['./retry-dialog.component.scss'],
  templateUrl: './retry-dialog.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class RetryDialogComponent {
  @ViewChild('retryButton') elRetryButton: ElementRef;

  retries: MRetryDialogEntry[] = [];

  constructor(
    private _spinner: SpinnerService,
  ) { }

  get isShown(): boolean {
    return this.retries.length > 0;
  }

  createRetryEntry(onRetry: Observable<any>, config: {
    maxRetry?: number;
  } = {}): Observable<any> {
    let observableIsError = false;
    return Observable.create(observer => {
      let retryAssigned = false;

      onRetry = onRetry
        .catch(error => {
          observableIsError = true;

          if (!retryAssigned) {
            const retryEntry: MRetryDialogEntry = {
              maxRetry: config.maxRetry,
              onRetry,
              retried: 0,
            };
            const retriesLength = this.retries.push(retryEntry);
            retryEntry.index = retriesLength - 1;

            retryAssigned = true;
          }

          return Observable.throw(error);
        })
        .do(success => {
          observableIsError = false;

          observer.next(success);
        }).finally(() => {
          if (!observableIsError) {
            observer.complete();
          }
        });

      onRetry.subscribe();
    });
  }

  onRetry() {
    const spinner = this._spinner.show({
      element: this.elRetryButton.nativeElement,
      loaderType: 3,
    });

    Observable.merge(
      ...this.retries.map((retry, retryIdx) => this.doRetry(retry))
    ).finally(() => {
      spinner.dispose();
    }).subscribe();
  }

  disposeRetry(retryIdx: number) {
    this.retries.splice(retryIdx, 1);
  }

  private doRetry(retry: MRetryDialogEntry) {
    if (retry.maxRetry !== 0 && retry.retried === retry.maxRetry) {
      this.disposeRetry(retry.index);
      return Observable.throw('max retry');
    } else {
      return retry.onRetry
        .do(() => {
          this.disposeRetry(retry.index);
        })
        .finally(() => {
          retry.retried = retry.retried + 1;
        });
    }
  }
}

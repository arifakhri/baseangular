import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';

export class MRetryDialogEntry {
  onRetry: Observable<any>;
  maxRetry: number = 0;
  retried: number;
  index?: number;
}

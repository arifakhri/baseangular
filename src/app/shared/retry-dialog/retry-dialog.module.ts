import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

import { RetryDialogComponent } from '../retry-dialog/retry-dialog.component';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
  ],
  declarations: [
    RetryDialogComponent,
  ],
  exports: [
    RetryDialogComponent,
  ],
})
export class RetryDialogModule { }

import { ModuleWithProviders, NgModule } from '@angular/core';

import { BaseModule, PROVIDERS as BaseProviders } from './base/base.module';
import { ConnectionStateModule } from './connection-state/connection-state.module';
import { FieldModule } from './field/field.module';
import { FieldsetAccordionModule } from './fieldset-accordion/fieldset-accordion.module';
import { FieldsetCollapsibleModule } from './fieldset-collapsible/fieldset-collapsible.module';
import { FormNavModule } from './form-nav/form-nav.module';
import { IdleGuardModule } from './idle-guard/idle-guard.module';
import { ImgUploadModule } from './img-upload/img-upload.module';
import { PROVIDERS as SpinnerProviders, SpinnerModule } from './spinner/spinner.module';
import { PROVIDERS as ReportProviders, ReportModule } from './report/report.module';
import { RetryDialogModule } from './retry-dialog/retry-dialog.module';
import { SystemMessageDisplayModule } from './system-message-display/system-message-display.module';
import { TextareaAutosizeModule } from './textarea-autosize/textarea-autosize.module';
import { ViewUtilityModule } from './view-utility/view-utility.module';
import { WrapperAutocompleteModule } from './wrapper-autocomplete/wrapper-autocomplete.module';

@NgModule({
  exports: [
    BaseModule,
    ConnectionStateModule,
    FieldModule,
    FormNavModule,
    IdleGuardModule,
    ImgUploadModule,
    SpinnerModule,
    FieldsetAccordionModule,
    FieldsetCollapsibleModule,
    ReportModule,
    RetryDialogModule,
    SystemMessageDisplayModule,
    TextareaAutosizeModule,
    ViewUtilityModule,
    WrapperAutocompleteModule,
  ]
})
export class SharedModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
      providers: [
        ...BaseProviders,
        ...SpinnerProviders,
        ...ReportProviders,
      ],
    };
  }
}

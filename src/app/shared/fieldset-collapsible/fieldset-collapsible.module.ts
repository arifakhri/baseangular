import { FlexLayoutModule } from '@angular/flex-layout';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

import { FieldsetCollapsibleContentComponent, FieldsetCollapsibleTitleComponent } from './fieldset-collapsible.component';
import { FieldsetCollapsibleDirective } from './fieldset-collapsible.directive';

@NgModule({
  imports: [
    FlexLayoutModule,
    TranslateModule
  ],
  declarations: [
    FieldsetCollapsibleContentComponent,
    FieldsetCollapsibleDirective,
    FieldsetCollapsibleTitleComponent,
  ],
  exports: [
    FieldsetCollapsibleContentComponent,
    FieldsetCollapsibleDirective,
    FieldsetCollapsibleTitleComponent
  ]
})
export class FieldsetCollapsibleModule {

}

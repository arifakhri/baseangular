import { NgModule } from '@angular/core';

import { EmptyModule } from './empty.module';
import { EmptyRoutingModule } from './empty-routing.module';

@NgModule({
  imports: [
    EmptyModule,
    EmptyRoutingModule,
  ],
})
export class EmptyLazyModule { }

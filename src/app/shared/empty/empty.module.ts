import { NgModule } from '@angular/core';

import { EmptyComponent } from './empty.component';

@NgModule({
  declarations: [
    EmptyComponent,
  ],
})
export class EmptyModule { }

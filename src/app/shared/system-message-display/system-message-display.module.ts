import { AlertModule } from 'ngx-bootstrap/alert';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

import { SystemMessageDisplayBlockComponent } from './block/system-message-display-block.component';
import { SystemMessageDisplayComponent } from './system-message-display.component';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    AlertModule,
    FlexLayoutModule,
  ],
  declarations: [
    SystemMessageDisplayBlockComponent,
    SystemMessageDisplayComponent,
  ],
  exports: [
    SystemMessageDisplayComponent,
  ]
})
export class SystemMessageDisplayModule { }

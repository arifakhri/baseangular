import { AlertComponent } from 'ngx-bootstrap/alert';
import { Component, Input, OnInit, ViewChild } from '@angular/core';

import { ESystemMessageType, ISystemMessage, SystemMessageService } from '../../../core/core.module';

@Component({
  selector: 'app-system-message-display-block',
  templateUrl: 'system-message-display-block.html'
})
export class SystemMessageDisplayBlockComponent implements OnInit {
  @Input() message: ISystemMessage;
  @Input() scope;
  @Input() service: SystemMessageService;
  @ViewChild('elAlert') elAlert: AlertComponent;

  alertType = '';
  targetIcon = '';
  targetTitle = '';

  ngOnInit() {
    switch (ESystemMessageType[this.message.options.type]) {
      case 'Success':
        this.alertType = 'success';
        this.targetIcon = 'fa fa-check-circle-o';
        this.targetTitle = 'ui.alert.success.title';
        break;
      case 'Warning':
        this.alertType = 'warning';
        this.targetIcon = 'mi mi-info-outline';
        this.targetTitle = 'ui.alert.warning.title';
        break;
      case 'Error':
        this.alertType = 'danger';
        this.targetIcon = 'mi mi-block';
        this.targetTitle = 'ui.alert.error.title';
        break;
      default:
        this.alertType = 'info';
        this.targetIcon = 'mi mi-error-outline';
        this.targetTitle = 'ui.alert.info.title';
        break;
    }
  }

  onClosed() {
    this.service.removeMessage(this.message);
  }
}

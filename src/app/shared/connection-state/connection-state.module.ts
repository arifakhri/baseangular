import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { CoreModule } from '../../core/core.module';

import { ConnectionStateComponent } from './connection-state.component';

@NgModule({
  imports: [
    CommonModule,
    CoreModule
  ],
  declarations: [
    ConnectionStateComponent
  ],
  exports: [
    ConnectionStateComponent
  ]
})
export class ConnectionStateModule { }

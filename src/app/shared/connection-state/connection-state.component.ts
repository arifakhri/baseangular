import { Component } from '@angular/core';

import { ConnectionStateService } from '../../core/core.module';

@Component({
  selector: 'app-connection-state',
  templateUrl: 'connection-state.component.html',
  styleUrls: ['connection-state.component.scss'],
})
export class ConnectionStateComponent {
  constructor(
    public _connectionState: ConnectionStateService
  ) { }

}

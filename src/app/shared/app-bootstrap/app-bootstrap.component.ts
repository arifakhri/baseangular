import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import 'app/rxjs-imports.ts';
import { Router } from '@angular/router';
import { UrlService } from '../../core/common/url.service';

import { RetryDialogComponent } from '../retry-dialog/retry-dialog.component';

import { ApiBootstrapService } from '../../core/http/api-bootstrap.service';
import { AuthenticationService } from '../../core/auth/authentication.service';
import { AuthorizationService } from '../../core/core.module';
import { TokenValidatorService } from '../../core/http/token-validator.service';

@Component({
  selector: 'app-bootstrap',
  templateUrl: './app-bootstrap.component.html',
})
export class AppBootstrapComponent implements OnDestroy, OnInit {
  @ViewChild('retryDialog') elRetryDialog: RetryDialogComponent;

  bootstrapState: boolean;
  urlHash = this._url.extractHashParts();

  subscriptions: Subscription[] = [];

  bootstrapBypassRedirect: boolean;

  constructor(
    private _authentication: AuthenticationService,
    private _authorization: AuthorizationService,
    private _apiBoostrap: ApiBootstrapService,
    private _router: Router,
    private _tokenValidator: TokenValidatorService,
    private _url: UrlService,
  ) { }

  ngOnInit() {
    this.bootstrapBypassRedirect = false;

    this.subscriptions.push(
      this._apiBoostrap.change$.subscribe(state => {
        if (state && !this.bootstrapBypassRedirect) {
          this.forceNavigateRedirect();
        }
      })
    );

    this.elRetryDialog.createRetryEntry(
      this.bootstrapApi()
    ).subscribe();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  bootstrapApi() {
    return Observable.create(observer => {
      if (!this._authentication.user) {
        this.bootstrapBypassRedirect = true;

        this._apiBoostrap.destroy();

        this.forceNavigateLogin();

        observer.next(false);
        observer.complete();
      } else {
        if (!this._authorization.publicRoutePaths.includes(this.urlHash.urlParams.sourcePath)) {
          this._tokenValidator.validate()
            .catch(error => {
              this._apiBoostrap.removePreloader();
              observer.error(error);
              return Observable.throw(error);
            })
            .switchMap(() => this._apiBoostrap.boot())
            .subscribe(() => {
              observer.next(true);
              observer.complete();
            });
        } else {
          this._apiBoostrap.destroy();
          observer.next(false);
          observer.complete();
        }
      }
    });
  }

  async forceNavigateRedirect() {
    let redirected = await this.navigateRedirect();
    while (!redirected) {
      redirected = await this.navigateRedirect();
    }
  }

  navigateRedirect() {
    return this._router.navigateByUrl(`
      ${this.urlHash.urlParams.sourcePath}${this.urlHash.urlParams.sourceQuery ? '?' + this.urlHash.urlParams.sourceQuery : ''}
    `.trim());
  }

  async forceNavigateLogin() {
    let redirected = await this.navigateLogin();
    while (!redirected) {
      redirected = await this.navigateLogin();
    }
  }

  navigateLogin() {
    return this._router.navigate(['/login'], {
      queryParams: {
        destination: `
          ${this.urlHash.urlParams.sourcePath}${this.urlHash.urlParams.sourceQuery ? '?' + this.urlHash.urlParams.sourceQuery : ''}
        `.trim(),
      },
    });
  }
}

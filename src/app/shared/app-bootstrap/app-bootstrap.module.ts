import { AppBootstrapComponent } from './app-bootstrap.component';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RetryDialogModule } from '../retry-dialog/retry-dialog.module';

@NgModule({
  imports: [
    CommonModule,
    RetryDialogModule,
  ],
  declarations: [
    AppBootstrapComponent,
  ],
  exports: [
    AppBootstrapComponent,
  ],
})
export class AppBootstrapModule {

}

import { Component } from '@angular/core';

import { FormgenFieldRenderBComponent } from '../field-render/formgen-field-render.bcomponent';

@Component({
  selector: 'app-formgen-field-text',
  templateUrl: './formgen-field-text.component.html',
})
export class FormgenFieldTextComponent extends FormgenFieldRenderBComponent { }

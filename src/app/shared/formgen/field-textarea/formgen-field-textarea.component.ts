import { Component } from '@angular/core';

import { FormgenFieldRenderBComponent } from '../field-render/formgen-field-render.bcomponent';

@Component({
  selector: 'app-formgen-field-textarea',
  templateUrl: './formgen-field-textarea.component.html',
})
export class FormgenFieldTextareaComponent extends FormgenFieldRenderBComponent { }

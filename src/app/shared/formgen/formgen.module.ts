import { AutoCompleteModule } from 'primeng/components/autocomplete/autocomplete';
import { CalendarModule } from 'primeng/calendar';
import { ChipsModule } from 'primeng/components/chips/chips';
import { CommonModule } from '@angular/common';
import { EditorModule } from 'primeng/editor';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';

import { FormgenComponent } from './formgen.component';
import { FormgenFieldAutocompleteComponent } from './field-autocomplete/formgen-field-autocomplete.component';
import { FormgenFieldChipsComponent } from './field-chips/formgen-field-chips.component';
import { FormgenFieldComponent } from './field/formgen-field.component';
import { FormgenFieldDateComponent } from './field-date/formgen-field-date.component';
import { FormgenFieldRenderComponent } from './field-render/formgen-field-render.component';
import { FormgenFieldTextComponent } from './field-text/formgen-field-text.component';
import { FormgenFieldTextareaComponent } from './field-textarea/formgen-field-textarea.component';
import { FormgenFieldTextareaHtmlComponent } from './field-textarea-html/formgen-field-textarea-html.component';
import { FormgenTemplateDirective } from './formgen-template.directive';

import { CoreModule } from '../../core/core.module';
import { FieldModule } from '../field/field.module';
import { WrapperAutocompleteModule } from '../wrapper-autocomplete/wrapper-autocomplete.module';

@NgModule({
  imports: [
    AutoCompleteModule,
    CalendarModule,
    ChipsModule,
    CommonModule,
    CoreModule,
    EditorModule,
    FieldModule,
    ReactiveFormsModule,
    TranslateModule,
    WrapperAutocompleteModule,
  ],
  declarations: [
    FormgenComponent,
    FormgenFieldAutocompleteComponent,
    FormgenFieldChipsComponent,
    FormgenFieldComponent,
    FormgenFieldDateComponent,
    FormgenFieldRenderComponent,
    FormgenFieldTextComponent,
    FormgenFieldTextareaComponent,
    FormgenFieldTextareaHtmlComponent,
    FormgenTemplateDirective,
  ],
  exports: [
    FormgenComponent,
    FormgenFieldComponent,
    FormgenTemplateDirective,
  ]
})
export class FormgenModule { }

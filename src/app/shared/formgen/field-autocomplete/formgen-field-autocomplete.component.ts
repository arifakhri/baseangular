import * as _ from 'lodash';
import Axios, { AxiosPromise } from 'axios';
import { AutoComplete } from 'primeng/components/autocomplete/autocomplete';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';

import { AutocompleteService, CommonService } from '../../../core/core.module';

import { FormgenFieldRenderBComponent } from '../field-render/formgen-field-render.bcomponent';

@Component({
  selector: 'app-formgen-field-autocomplete',
  templateUrl: './formgen-field-autocomplete.component.html',
})
export class FormgenFieldAutocompleteComponent extends FormgenFieldRenderBComponent implements OnInit {
  @ViewChild('fAC') fAC: AutoComplete;

  ACRemoteHandler: any;
  isACLocal: boolean;

  constructor(
    private _autocomplete: AutocompleteService,
  ) {
    super();
  }

  ngOnInit() {
    this.isACLocal = _.get(this.config, 'templateConfig.acLocal');
    this.ACRemoteHandler = CommonService.remoteACItemsHandler({
      remoteParams: this.buildACRemoteParams.bind(this),
      remoteRequest: this.requestACRemoteUrl.bind(this),
      remoteRequestMap: this.config.templateConfig.acRemoteResponseTransform ?
        this.config.templateConfig.acRemoteResponseTransform :
        (response) => response.data.data,
      element: () => this.fAC,
    });

    if (this.config.control.value && this.config.templateConfig && this.config.templateConfig.acRemoteLoadFirst) {
      this.loadRemoteData();
    }
  }

  onComplete($event) {
    if (this.hasEvent('onComplete')) {
      this.callEvent('onComplete', $event);
    } else if (this.isACLocal) {
      return this._autocomplete.onLocalACSearch(
        this.fAC,
        _.get(this.config, 'templateConfig.options', []),
        _.get(this.config, 'templateConfig.acLocalFilterBy', []),
        $event.query,
        _.get(this.config, 'templateConfig.acPrependNull', false),
      );
    } else if (!this.isACLocal) {
      return this.ACRemoteHandler.search($event);
    }
  }

  onDropdownClick($event) {
    if (this.hasEvent('onDropdown')) {
      this.callEvent('onDropdown', $event);
    } else if (this.isACLocal) {
      this._autocomplete.onLocalACDropdown(
        this.fAC,
        _.get(this.config, 'templateConfig.options', []),
        _.get(this.config, 'templateConfig.acPrependNull', false),
      );
    } else if (!this.isACLocal) {
      return this.ACRemoteHandler.dropdown($event);
    }
  }

  onSelect($event) {
    this.callEvent('onSelect', $event);
  }

  onClear($event) {
    this.callEvent('onClear', $event);

    this.config.control.reset();
  }

  buildACRemoteParams(event: any, type: string) {
    const queryParam: any = {};
    if (type === 'search') {
      queryParam[this.config.templateConfig.acRemoteParamKey] = event.query;
    }

    return [null, queryParam];
  }

  requestACRemoteUrl(payload: any = null, queryParam: any = {}) {
    return Observable.fromPromise(
      <AxiosPromise>Axios.post(this.config.templateConfig.acRemoteUrl, payload, {
        params: queryParam,
      })
    );
  }

  loadRemoteData() {
    Observable.fromPromise(
      <AxiosPromise>Axios.post(this.config.templateConfig.acRemoteUrl, null, {
        params: {
          RefID: this.config.control.value,
        },
      })
    ).subscribe(response => {
      const remoteValue = _.head(response.data.data);
      if (remoteValue) {
        this.config.control.setValue(remoteValue);
      }
    });
  }
}

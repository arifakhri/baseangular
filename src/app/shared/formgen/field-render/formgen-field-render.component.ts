import { Component, Input } from '@angular/core';

import { MFormgenSchemaField } from '../formgen.model';

@Component({
  selector: 'app-formgen-field-render',
  templateUrl: './formgen-field-render.component.html',
})
export class FormgenFieldRenderComponent {
  @Input() schema: MFormgenSchemaField;
}

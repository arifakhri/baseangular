import * as _ from 'lodash';
import { Input } from '@angular/core';

import { MFormgenFieldConfig } from '../formgen-field.model';

export class FormgenFieldRenderBComponent {
  @Input() config: MFormgenFieldConfig;

  hasEvent(eventName: string) {
    return _.has(this.config, `events.${eventName}`);
  }

  callEvent(eventName: string, $event: any) {
    if (this.hasEvent(eventName)) {
      const func = _.get(this.config, `events.${eventName}`);
      return func($event);
    }
  }
}

import { AsyncValidatorFn, FormControl, FormGroup, ValidatorFn } from '@angular/forms';

export class MFormgenFieldConfig {
  control?: FormControl;
  form?: FormGroup;
  label?: string;
  value?: any;
  syncValidators?: ValidatorFn | ValidatorFn[];
  asyncValidators?: AsyncValidatorFn | AsyncValidatorFn[];
  templateConfig?: {
    options?: any;
    placeholder?: string;
    type?: string;
    dropdown?: boolean;
    acField?: string;
    acKey?: string;
    acLocal?: boolean;
    acLocalFilterBy?: string[];
    acRemoteUrl?: string;
    acRemoteParamKey?: string;
    acRemoteResponseTransform?: Function;
    acRemoteLoadFirst?: boolean;
    acLimit?: any;
    dateFormat?: string;
    manualInput?: boolean;
    multiple?: boolean;
  } = {
    options: [],
    dropdown: true,
    acField: 'value',
    acKey: 'key',
    acLocal: true,
    acLimit: false,
    dateFormat: 'dd/mm/yy',
    manualInput: false,
    multiple: false,
  };
  events?: {
    onAdd?: Function;
    onRemove?: Function;
    onChange?: Function;
    onClear?: Function;
    onComplete?: Function;
    onDropdown: Function;
    onSelect?: Function;
  };
}

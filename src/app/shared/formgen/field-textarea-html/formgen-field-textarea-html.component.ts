import { Component } from '@angular/core';

import { FormgenFieldRenderBComponent } from '../field-render/formgen-field-render.bcomponent';

@Component({
  selector: 'app-formgen-field-textarea-html',
  templateUrl: './formgen-field-textarea-html.component.html',
})
export class FormgenFieldTextareaHtmlComponent extends FormgenFieldRenderBComponent { }

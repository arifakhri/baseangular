import { MFormgenFieldConfig } from './formgen-field.model';

export class MFormgenSchema {
  fields: MFormgenSchemaField[];
  customLayout: boolean;
}

export class MFormgenSchemaField extends MFormgenFieldConfig {
  name: string;
  field?: string;
  ignore?: boolean;
  validations?: {
    [key: string]: any;
  };
  preprocess?: Function;
  children?: {
    [key: string]: MFormgenSchemaField;
  };
}

import { Component, Input } from '@angular/core';

import { FormgenComponent } from '../formgen.component';

@Component({
  selector: 'app-formgen-field',
  templateUrl: './formgen-field.component.html',
})
export class FormgenFieldComponent {
  @Input() formgen: FormgenComponent;
  @Input() name: string;
}

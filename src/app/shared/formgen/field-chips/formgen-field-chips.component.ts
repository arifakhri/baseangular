import { Component } from '@angular/core';

import { FormgenFieldRenderBComponent } from '../field-render/formgen-field-render.bcomponent';

@Component({
  selector: 'app-formgen-field-chips',
  templateUrl: './formgen-field-chips.component.html',
})
export class FormgenFieldChipsComponent extends FormgenFieldRenderBComponent {
  onAdd($event) {
    this.callEvent('onAdd', $event);
  }

  onRemove($event) {
    this.callEvent('onRemove', $event);
  }
}

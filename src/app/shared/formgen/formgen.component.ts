import {
  ChangeDetectorRef,
  Component,
  ContentChildren,
  DoCheck,
  Input,
  KeyValueDiffer,
  KeyValueDiffers,
  QueryList,
  ViewChildren,
} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import * as _ from 'lodash';
import { BehaviorSubject } from 'rxjs';
import 'app/rxjs-imports.ts';

import { FormgenTemplateDirective } from './formgen-template.directive';
import { MFormgenSchema, MFormgenSchemaField } from './formgen.model';

@Component({
  selector: 'app-formgen',
  templateUrl: './formgen.component.html',
})
export class FormgenComponent implements DoCheck {
  @Input() schema: MFormgenSchema;
  @ContentChildren(FormgenTemplateDirective) cTemplates: QueryList<any>;
  @ViewChildren(FormgenTemplateDirective) vTemplates: QueryList<any>;

  form = new FormGroup({});

  schemaDiffer: KeyValueDiffer<string, any> = this._keyValueDiffers.find({}).create();

  controlsReady$: BehaviorSubject<boolean> = new BehaviorSubject(false);

  vTemplatesMapped: {
    [key: string]: any,
  };

  constructor(
    private _changeDetectorRef: ChangeDetectorRef,
    private _keyValueDiffers: KeyValueDiffers,
  ) { }

  ngDoCheck() {
    const changes = this.schemaDiffer.diff(this.schema);
    if (changes) {
      this.mapControls();
    }
  }

  mapVTemplates() {
    const vTemplatesMapped = {};

    this.vTemplates.forEach(template => {
      vTemplatesMapped[template.getType()] = template;
    });

    if (_.size(vTemplatesMapped)) {
      this.vTemplatesMapped = vTemplatesMapped;
    }
  }

  patchValue(formValue: any = {}, key: string = '') {
    if (!key) {
      this.controlsReady$.filter(val => !!val).subscribe(() => {
        this.form.patchValue(formValue || {});
      });
    }
  }

  getTemplate(templateType: string) {
    if (this.cTemplates) {
      return _.find(this.cTemplates.toArray(), template => template.getType() === templateType);
    }
    return null;
  }

  mapControls() {
    this.controlsReady$.next(false);

    _.forEach(this.form.controls, (control, controlName) => {
      this.form.removeControl(controlName);
    });

    _.forEach(this.schema.fields, field => {
      this.buildControl(field);
    });

    if (!this.controlsReady$.value) {
      this.controlsReady$.next(true);
    }

    this._changeDetectorRef.detectChanges();

    this.mapVTemplates();
  }

  buildControl(field: MFormgenSchemaField) {
    const control = new FormControl(field.value);
    field.control = control;

    this.form.setControl(field.name, field.control);

    const fieldSyncValidators = _.castArray(field.syncValidators).filter(_.identity);
    const fieldAsyncValidators = _.castArray(field.asyncValidators).filter(_.identity);

    const syncValidators = fieldSyncValidators.concat(this.buildSyncValidators(field));
    const asyncValidators = fieldAsyncValidators.concat(this.buildAsyncValidators(field));

    this.form.get(field.name).setValidators(syncValidators);
    this.form.get(field.name).setAsyncValidators(asyncValidators);

    return control;
  }

  buildSyncValidators(field: MFormgenSchemaField) {
    const validators = [];

    if (_.get(field, 'validations.required')) {
      validators.push(Validators.required);
    }

    return validators;
  }

  buildAsyncValidators(field: MFormgenSchemaField) {
    const validators = [];

    return validators;
  }
}

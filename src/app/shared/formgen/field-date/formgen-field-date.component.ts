import { Component } from '@angular/core';

import { FormgenFieldRenderBComponent } from '../field-render/formgen-field-render.bcomponent';

@Component({
  selector: 'app-formgen-field-date',
  templateUrl: './formgen-field-date.component.html',
})
export class FormgenFieldDateComponent extends FormgenFieldRenderBComponent { }

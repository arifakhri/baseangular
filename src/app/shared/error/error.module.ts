import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

import { SharedModule as CentricSharedModule } from '../../modules/ng2centric/shared/shared.module';

import { ErrorComponentService } from './error.service';
import { Error403Component } from './403/403.component';
import { Error404Component } from './404/404.component';

@NgModule({
  imports: [
    CommonModule,
    FlexLayoutModule,
    TranslateModule.forChild(),
    CentricSharedModule
  ],
  declarations: [
    Error403Component,
    Error404Component
  ],
  exports: [
    Error403Component,
    Error404Component
  ],
  providers: [
    ErrorComponentService
  ]
})
export class ErrorModule { }

import { Injectable } from '@angular/core';
import { Location } from '@angular/common';

@Injectable()
export class ErrorComponentService {
  constructor(private location: Location) { }

  back() {
    this.location.back();
  }
}

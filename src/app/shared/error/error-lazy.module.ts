import { NgModule } from '@angular/core';

import { ErrorModule } from './error.module';
import { ErrorRoutingModule } from './error-routing.module';

@NgModule({
  imports: [
    ErrorModule,
    ErrorRoutingModule,
  ]
})
export class ErrorLazyModule { }

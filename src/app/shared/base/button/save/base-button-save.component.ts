import { Component, Input } from '@angular/core';

import { BaseEntryBComponent } from '../../entry/base-entry.bcomponent';

@Component({
  selector: 'app-base-button-save',
  templateUrl: './base-button-save.component.html',
})
export class BaseButtonSaveComponent {
  @Input() component: BaseEntryBComponent;
}

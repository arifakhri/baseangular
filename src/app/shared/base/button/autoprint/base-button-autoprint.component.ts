import { Component, Input, OnInit } from '@angular/core';

import { UserVariableService } from '../../../../core/core.module';

import { BaseEntryBComponent } from '../../entry/base-entry.bcomponent';

@Component({
  selector: 'app-base-button-autoprint',
  templateUrl: './base-button-autoprint.component.html',
})
export class BaseButtonAutoprintComponent implements OnInit {
  @Input() component: BaseEntryBComponent;

  constructor(
    public _userVariable: UserVariableService,
  ) { }

  ngOnInit() {
    this.component.autoprintButtons.forEach(autoprintButton => {
      autoprintButton.checkboxConfigModel = this._userVariable.get(autoprintButton.checkboxConfigId, { value: false });
    });
  }
}

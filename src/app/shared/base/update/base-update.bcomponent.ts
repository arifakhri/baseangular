import 'app/rxjs-imports.ts';

import { AfterViewInit } from '@angular/core';

import { AppEvent } from '../../../core/core.module';
import { BaseEntryBComponent } from '../entry/base-entry.bcomponent';

export class BaseUpdateBComponent extends BaseEntryBComponent implements AfterViewInit {
  constructor() {
    super();

    this.registerHook('patchForm', this.patchForm.bind(this));
  }

  ngAfterViewInit() {
    this.loadData();
  }

  loadData() {
    const loading = this.page.createLoading('load');

    return this.page.createRetriableTask(
      this.callHook('load')
        .finally(() => {
          loading.dispose();
        })
        .do(doc => {
          this.doc = doc;
        })
        .switchMap(doc => this.callHook('patchForm', doc))
    ).subscribe();
  }

  patchForm(event: AppEvent) {
    this.form.patchValue(event.data);
  }
}

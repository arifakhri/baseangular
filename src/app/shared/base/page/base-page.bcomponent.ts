import 'app/rxjs-imports.ts';

import { Input, ViewChild } from '@angular/core';
import * as _ from 'lodash';
import { Observable, Subscription } from 'rxjs';

import { AppEvent, SystemMessageService } from '../../../core/core.module';
import { MBasePageHeaderActionButton } from '../base.model';
import { BasePageComponent } from './base-page.component';

export class BasePageBComponent {
  @ViewChild('page') elPage: BasePageComponent;
  @Input() existingPage: BasePageComponent;
  @Input() masterPage: BasePageComponent;

  headerButtons: MBasePageHeaderActionButton[] = [];
  headerTitle: string;

  componentId: string;
  hooks: {
    [key: string]: {
      handle: (event: AppEvent) => any,
      success: Function,
      error: Function,
      eventSubscription?: Subscription,
    },
  } = {};

  routeURL: string;

  containerType: number;

  get page(): BasePageComponent {
    return this.elPage || this.existingPage;
  }

  // this method will be called when initPage (in component) success
  public onPageInit() {
  }

  // this method will be called when initPage (in component) error
  public onPageInitError(error: any) {
  }

  registerHook(hookName: string, handle: (event: AppEvent) => any, success?: Function, error?: Function) {
    _.set(this.hooks, hookName, { handle, success, error });
  }

  hasHook(hookName: string): boolean {
    return Boolean(_.get(this.hooks, hookName));
  }

  callHook(hookName: string, hookData?: any): Observable<any> {
    if (!this.hasHook(hookName)) {
      return Observable.of(null);
    }

    return Observable.create(observer => {
      const hook = _.get(this.hooks, hookName);

      const complete = (handleSubscription?: Subscription) => {
        if (handleSubscription) {
          handleSubscription.unsubscribe();
        }
        if (hook && hook.eventSubscription) {
          hook.eventSubscription.unsubscribe();
        }
        observer.complete();
      };

      if (hook) {
        if (hook.eventSubscription) {
          hook.eventSubscription.unsubscribe();
        }

        hook.eventSubscription = this.page._event.listen(`COMP:${this.componentId}:${hookName}`).subscribe(event => {
          let handle;

          try {
            handle = hook.handle(event);

            if (handle instanceof Observable) {
              const obs = handle.subscribe(data => {
                observer.next(data);
                if (hook.success) {
                  hook.success(data);
                }
                complete(obs);
              }, error => {
                observer.error(error);
                if (hook.error) {
                  hook.error(error);
                }
                complete(obs);
              });
            } else {
              observer.next(handle);
              if (hook.success) {
                hook.success(handle);
              }
              complete();
            }
          } catch (error) {
            console.error(error);

            if (hook.error) {
              hook.error(error);
            }

            complete();
          }
        });

        this.page._event.emit(`COMP:${this.componentId}:${hookName}`, hookData);
      } else {
        complete();
      }
    });
  }
}

import * as _ from 'lodash';
import {
  AfterContentInit,
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ContentChildren,
  Inject,
  Input,
  OnInit,
  QueryList,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subject, Subscription } from 'rxjs';
import 'app/rxjs-imports.ts';

import { ViewUtilityTemplateDirective } from '../../view-utility/view-utility-template.directive';

import { AdminLayoutService } from '../../../layouts/admin/admin-layout.service';
import { EventService, SystemMessageService } from '../../../core/core.module';
import { TranslateService } from '@ngx-translate/core';

import { RetryDialogComponent } from '../../retry-dialog/retry-dialog.component';

import { BasePageBComponent } from './base-page.bcomponent';

@Component({
  selector: 'app-base-page',
  templateUrl: './base-page.component.html',
  providers: [EventService, SystemMessageService],
})
export class BasePageComponent implements OnInit, AfterContentInit, AfterViewInit {
  @Input() component: BasePageBComponent;
  @ContentChildren(ViewUtilityTemplateDirective) cTemplates: QueryList<ViewUtilityTemplateDirective>;
  @ViewChild('retryDialog') elRetryDialog: RetryDialogComponent;

  headerTemplate: TemplateRef<any>;
  headerActionsTemplate: TemplateRef<any>;
  contentTemplate: TemplateRef<any>;
  footerTemplate: TemplateRef<any>;

  initializingPage: boolean = false;
  initializingPageSub: Subscription;
  pageInitialized: boolean = false;

  compLoadingShown: boolean = false;
  compLoadingTitle: string = '';
  compLoadings: any[] = [];
  compLoadingObs: Observable<any>[] = [];
  compLoadingDone$: Subject<string> = new Subject;

  retryDialogShownState: boolean = false;

  _routeData: any;
  _routeParams: any;
  _routeQueryParams: any;

  afterViewInitCalled: boolean = false;

  get hasMasterPage() {
    return this.component && this.component.masterPage;
  }

  constructor(
    @Inject('GlobalSystemMessage') public _globalSystemMessage: SystemMessageService,
    public _activatedRoute: ActivatedRoute,
    public _adminLayout: AdminLayoutService,
    public _changeDetectorRef: ChangeDetectorRef,
    public _event: EventService,
    public _router: Router,
    public _systemMessage: SystemMessageService,
    public _translate: TranslateService,
  ) {
  }

  ngOnInit() {
    this.initPage();
  }

  ngAfterContentInit() {
    this.cTemplates.forEach(cTemplate => {
      switch (cTemplate.getType()) {
        case 'pageHeaderTpl':
          this.headerTemplate = cTemplate.template;
          break;
        case 'pageHeaderActionsTpl':
          this.headerActionsTemplate = cTemplate.template;
          break;
        case 'pageContentTpl':
          this.contentTemplate = cTemplate.template;
          break;
        case 'pageFooterTpl':
          this.footerTemplate = cTemplate.template;
          break;
      }
    });
  }

  ngAfterViewInit() {
    if (this.component.containerType) {
      this._adminLayout.containerType = this.component.containerType;
    }
  }

  private initPage() {
    this.initializingPage = true;
    const obs = this.getInitPageObservables();

    obs.subscribe(
      success => {
        this.pageInitialized = true;
        this.initializingPage = false;
        this.onPageInit();
        this.component.onPageInit();
      },
      error => {
        this.initializingPage = false;
        this.onPageInitError(error);
        this.component.onPageInitError(error);
      });
  }

  // put all observables that need to be run.
  // can be override on child
  // but remember to call super and fork join with super observable
  // after success, isPageInitialized=true
  public getInitPageObservables(): Observable<any> {
    return Observable
      .zip(this._activatedRoute.data, this._activatedRoute.params, this._activatedRoute.queryParams)
      .do(
        responses => {
          this._routeData = responses[0];
          this._routeParams = responses[1];
          this._routeQueryParams = responses[2];
        },
        error => {
        });
  }

  // this method will be called when initPage success
  public onPageInit() {
  }

  // this method will be called when initPage error
  public onPageInitError(error: any) {
  }

  set retryDialogOnRetry(onRetry: () => Observable<any>) {
    //
  }

  createRetriableTask(onRetry: Observable<any>, config?) {
    return this.elRetryDialog.createRetryEntry(onRetry, config);
  }

  createLoading(loadingId?: string) {
    const loadingLength = this.compLoadings.push(loadingId);

    let masterLoading;
    if (this.hasMasterPage) {
      masterLoading = this.component.masterPage.createLoading(loadingId);
    }

    this.compLoadingShown = true;

    return {
      dispose: () => {
        if (_.isString(loadingId)) {
          this.compLoadings = _.remove([].concat(this.compLoadings), loadingId);

          this.compLoadingDone$.next(loadingId);
        } else {
          this.compLoadings.splice(loadingLength - 1, 1);
        }

        if (!this.compLoadings.length) {
          this.compLoadingShown = false;
        }

        if (masterLoading) {
          masterLoading.dispose();
        }
      },
    };
  }

  setLoadingTitle(loadingTitle: string) {
    this.compLoadingTitle = loadingTitle;
  }

  get routeData() {
    return _.size(this._routeData) ? this._routeData : null;
  }

  get routeParams() {
    return _.size(this._routeParams) ? this._routeParams : null;
  }

  get routeQueryParams() {
    return _.size(this._routeQueryParams) ? this._routeQueryParams : null;
  }

  onCompLoadingDone(loadingId: string) {
    return this.compLoadingDone$.filter(loading => loading === loadingId);
  }
}

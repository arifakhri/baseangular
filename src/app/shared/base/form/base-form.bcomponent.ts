import 'app/rxjs-imports.ts';

import { AfterViewInit, Input, OnInit, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ModalDirective } from 'ngx-bootstrap';
import { AutoComplete } from 'primeng/components/autocomplete/autocomplete';

import { BasePageBComponent } from '../page/base-page.bcomponent';

export class BaseFormBComponent extends BasePageBComponent implements AfterViewInit, OnInit {
  @ViewChild('quickCreateModal') elQuickCreateModal: ModalDirective;

  @Input() doc: any;
  @Input() form: FormGroup = new FormGroup({});
  @Input() formType: 'create' | 'update';

  relatedData: any = {};
  relatedDataReady: boolean = false;

  quickCreateAC: AutoComplete;
  quickCreateTarget: string;

  ngAfterViewInit() {
  }

  ngOnInit() {
    this.bBuildForm();
  }

  onPageInit() {
    super.onPageInit();

    this.bLoadRelatedData();
  }

  bBuildForm() {
    return this.callHook('buildForm').subscribe();
  }

  bLoadRelatedData() {
    if (this.hasHook('loadRelated')) {
      const loading = this.page.createLoading('loadRelated');

      return this.page.createRetriableTask(
        this.callHook('loadRelated').finally(() => {
          loading.dispose();
        })
      ).subscribe(relatedData => {
        this.relatedData = relatedData;
        this.relatedDataReady = true;
      });
    }
  }
}

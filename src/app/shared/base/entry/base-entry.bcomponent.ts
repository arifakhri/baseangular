import 'app/rxjs-imports.ts';

import { Input } from '@angular/core';
import { Observable } from 'rxjs';
import swal from 'sweetalert2';

import { CommonService } from '../../../core/core.module';
import { MBaseEntryAutoprintButton } from '../base.model';
import { BaseFormBComponent } from '../form/base-form.bcomponent';

export class BaseEntryBComponent extends BaseFormBComponent {
  @Input() autoprintButtons: MBaseEntryAutoprintButton[] = [];
  @Input() saveButtons: string[] = ['save', 'save_and_new', 'save_and_view'];

  doc: any;

  saveAndRedirectEnabled: boolean = true;
  cancelEnabled: boolean = true;
  cancelURL: string;
  listURL: string;
  saveAndNewEnabled: boolean = true;
  saveAndNewURL: string;
  saveAndViewEnabled: boolean = true;
  saveAndViewURL: string;

  saveAndRedirectType: string;

  entrySuccessI18n: string;

  formValid(): boolean {
    if (!this.form.valid) {
      CommonService.markAsDirty(this.form);
      this.page._systemMessage.log({
        message: this.page._translate.instant('error.form'),
        type: 'error'
      });
      return false;
    }
    return true;
  }

  async onSubmit({ saveAndRedirect, saveConfirm, ...customParams }: any = {}) {
    let obs = Observable.of(false);

    if (this.formValid()) {
      if (saveConfirm) {
        await swal({
          title: this.page._translate.instant(`confirm.save.label`),
          text: this.page._translate.instant(`confirm.save.description`),
          type: 'warning',
          showCancelButton: true
        });
      }

      if (this.hasHook('submitPreprocess')) {
        const loading = this.page.createLoading();
        try {
          await this.callHook('submitPreprocess', { saveAndRedirect, saveConfirm, ...customParams }).toPromise();
        } finally {
          loading.dispose();
        }
      }

      obs = this.save({ saveAndRedirect, saveConfirm, ...customParams }).do(result => {
        if (saveAndRedirect) {
          this.saveAndRedirectType = saveAndRedirect;
          if (this.saveAndRedirectEnabled) {
            this.redirectAfterSave(result.id);
          }
        }
      });
    }

    return await obs.toPromise();
  }

  redirectAfterSave(resultId?: string) {
    if (this.saveAndNewEnabled && this.saveAndRedirectType === 'new') {
      this.page._router.navigateByUrl('/empty', { skipLocationChange: true }).then(() => {
        this.page._router.navigateByUrl(this.bGetSaveAndNewURL());
      });
    } else if (this.saveAndViewEnabled && this.saveAndRedirectType === 'view') {
      this.page._router.navigate([this.saveAndViewURL || this.routeURL, resultId]);
    } else if (this.saveAndRedirectType === 'list') {
      this.page._router.navigateByUrl(this.listURL || this.routeURL);
    }
  }

  save(...args) {
    const loading = this.page.createLoading();

    let obs = this.callHook('save', ...args)
      .catch(error => {
        this.page._systemMessage.log({
          message: error,
          type: 'error'
        });

        return Observable.throw(error);
      })
      .do(result => {
        this.doc = result;

        this.page._globalSystemMessage.log({
          message: this.page._translate.instant(this.entrySuccessI18n),
          type: 'success',
          showAs: 'growl',
          showSnackBar: false,
        });
      })
      .finally(() => {
        loading.dispose();
      });

    if (this.hasHook('savePostprocess')) {
      obs = obs.switchMap(result => this.callHook('savePostprocess', result));
    }

    return obs;
  }

  bGetSaveAndNewURL() {
    let url: string = '';

    if (this.saveAndNewURL) {
      url = this.saveAndNewURL;
    } else if (this.routeURL) {
      url = [this.routeURL, 'create'].join('/');
    }

    return url;
  }

  bGetCancelURL() {
    let url: string = '';

    if (this.cancelURL) {
      url = this.cancelURL;
    } else if (this.listURL) {
      url = this.listURL;
    } else if (this.routeURL) {
      url = this.routeURL;
    }

    return url;
  }
}

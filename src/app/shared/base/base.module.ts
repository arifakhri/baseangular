import { BsDropdownModule } from 'ngx-bootstrap';
import { CheckboxModule } from 'primeng/checkbox';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { BaseButtonAutoprintComponent } from './button/autoprint/base-button-autoprint.component';
import { BaseButtonSaveComponent } from './button/save/base-button-save.component';
import { BaseEntryComponent } from './entry/base-entry.component';
import { BaseEntryFooterComponent } from './entry/footer/base-entry-footer.component';
import { BasePageComponent } from './page/base-page.component';

import { BaseService } from './base.service';

import { SpinnerModule } from '../spinner/spinner.module';
import { RetryDialogModule } from '../retry-dialog/retry-dialog.module';
import { SystemMessageDisplayModule } from '../system-message-display/system-message-display.module';
import { ViewUtilityModule } from '../view-utility/view-utility.module';

export const PROVIDERS = [
  BaseService,
];

@NgModule({
  imports: [
    BsDropdownModule,
    CheckboxModule,
    CommonModule,
    FlexLayoutModule,
    FormsModule,
    SpinnerModule,
    RetryDialogModule,
    RouterModule,
    SystemMessageDisplayModule,
    TranslateModule,
    ViewUtilityModule,
  ],
  declarations: [
    BaseButtonAutoprintComponent,
    BaseButtonSaveComponent,
    BaseEntryComponent,
    BaseEntryFooterComponent,
    BasePageComponent,
  ],
  providers: [
    BaseService,
  ],
  exports: [
    BaseButtonAutoprintComponent,
    BaseButtonSaveComponent,
    BaseEntryComponent,
    BaseEntryFooterComponent,
    BasePageComponent,
  ],
})
export class BaseModule { }

export * from './create/base-create.bcomponent';
export * from './detail/base-detail.bcomponent';
export * from './form/base-form.bcomponent';
export * from './entry/base-entry.bcomponent';
export * from './page/base-page.bcomponent';
export * from './update/base-update.bcomponent';


import 'app/rxjs-imports.ts';

import { AfterViewInit } from '@angular/core';

import { BasePageBComponent } from '../page/base-page.bcomponent';

export class BaseDetailBComponent extends BasePageBComponent implements AfterViewInit {
  doc: any;

  ngAfterViewInit() {
    this.load();
  }

  load() {
    const loading = this.page.createLoading('load');

    return this.page.createRetriableTask(
      this.callHook('load').finally(() => {
        loading.dispose();
      })
    ).subscribe(doc => {
      this.doc = doc;
    });
  }
}

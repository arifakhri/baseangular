export class MBasePageHeaderActionButton {
  type: 'edit' | 'custom' = 'custom';
  label?: string;
  color?: string;
  onClick?: Function;
  children?: { routerLink: any; label?: string; }[] = [];
  routerLink?: any;
}

export class MBaseEntryAutoprintButton {
  checkboxConfigId: string;
  checkboxConfigModel?: any;
  checkboxLabel: string;
  previewLabel: string;
  onPreview: Function;
}

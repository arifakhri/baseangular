import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { FieldStatesDirective } from './states/field-states.directive';

@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [
    FieldStatesDirective,
  ],
  exports: [
    FieldStatesDirective,
  ],
})
export class FieldModule { }

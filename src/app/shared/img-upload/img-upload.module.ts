import { CommonModule } from '@angular/common';
import { ContextMenuModule } from 'primeng/components/contextmenu/contextmenu';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgModule } from '@angular/core';

import { ImgUploadComponent } from './img-upload.component';
import { ImgUploadDisplayComponent } from './display/img-upload-display.component';
import { ImgUploadGalleryComponent } from './gallery/img-upload-gallery.component';
import { ImgUploadItemComponent } from './item/img-upload-item.component';
import { SpinnerModule } from '../spinner/spinner.module';

import { FileSecurityModule } from '../../core/file/file-security.module';

@NgModule({
  imports: [
    CommonModule,
    ContextMenuModule,
    FileSecurityModule,
    FlexLayoutModule,
    SpinnerModule,
  ],
  declarations: [
    ImgUploadComponent,
    ImgUploadDisplayComponent,
    ImgUploadGalleryComponent,
    ImgUploadItemComponent,
  ],
  exports: [
    ImgUploadComponent,
    ImgUploadDisplayComponent,
    ImgUploadGalleryComponent,
  ]
})
export class ImgUploadModule { }

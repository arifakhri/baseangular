import { Component, ElementRef, Input, ViewChild } from '@angular/core';

@Component({
  selector: 'app-img-upload-display',
  exportAs: 'app-img-upload-display',
  templateUrl: './img-upload-display.component.html',
})
export class ImgUploadDisplayComponent {
  @Input() imageSrc: string;
  @ViewChild('image') elImage: ElementRef;

  imagePlaceholder = require('../img-upload-placeholder.png');
}

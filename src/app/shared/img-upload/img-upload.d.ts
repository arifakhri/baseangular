declare interface IImgUploadItemServer {
  id: string;
  fileType: string;
  fileName: string;
  fileExt: string;
  fileSize: number;
  description: string;
  uploadByUserId: string;
  fileUrl: string;
  thumbnailUrl: string;
  smallUrl: string;
  mediumUrl: string;
  largeUrl: string;
  tabletUrl: string;
}

declare interface IImgUploadItemClient {
  id?: number;
  uuid: string;
  name: string;
  size?: number;
  thumbnailUrl?: string;
}
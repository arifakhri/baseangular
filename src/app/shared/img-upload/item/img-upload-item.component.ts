import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FineUploaderBasic, status } from 'fine-uploader/lib/core';
import { MenuItem } from 'primeng/components/common/menuitem';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-img-upload-item',
  templateUrl: './img-upload-item.component.html',
})
export class ImgUploadItemComponent implements OnInit {
  @Input() uploader: FineUploaderBasic;
  @Input() file: any;
  @Input() small: boolean = false;

  @Output() onDeleted: EventEmitter<boolean> = new EventEmitter;
  @Output() onPrimarySelected: EventEmitter<boolean> = new EventEmitter;

  @ViewChild('image') elImage: ElementRef;

  imagePlaceholder = require('../img-upload-placeholder.png');

  contextMenus: MenuItem[] = [{
    label: this._translate.instant('ui.imgUpload.item.contextMenu.setAsPrimary'),
    command: () => this.onPrimarySelected.emit(true),
  }, {
    label: this._translate.instant('ui.imgUpload.item.contextMenu.delete'),
    command: this.onDelete.bind(this),
  }];

  constructor(
    private _translate: TranslateService,
  ) { }

  ngOnInit() {
    setTimeout(() => {
      this.uploader.drawThumbnail(this.file.id, this.elImage.nativeElement, 0, this.file.thumbnailUrl);
    }, 500);
  }

  onDelete() {
    this.onDeleted.emit(true);

    this.uploader.setStatus(this.file.id, status.DELETED);
  }
}

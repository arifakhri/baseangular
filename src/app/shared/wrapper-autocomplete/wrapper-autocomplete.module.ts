import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { WrapperAutocompleteDirective } from './wrapper-autocomplete.directive';

@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [
    WrapperAutocompleteDirective,
  ],
  exports: [
    WrapperAutocompleteDirective,
  ]
})
export class WrapperAutocompleteModule {

}

import { AutoComplete } from 'primeng/components/autocomplete/autocomplete';
import {
  AfterViewInit,
  Directive,
  DoCheck,
  ElementRef,
  Input,
  IterableDiffer,
  IterableDiffers,
  KeyValueDiffer,
  KeyValueDiffers,
  OnInit,
} from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Directive({
  selector: '[appWrapperAC]',
})
export class WrapperAutocompleteDirective implements AfterViewInit, DoCheck, OnInit {
  @Input('appWrapperAC') ac: AutoComplete;
  @Input('appWrapperACLimit') limit: number = 30;

  differPanelVisible: KeyValueDiffer<string, any>;
  differSuggestions: IterableDiffer<any>;
  selectFirstItem: boolean = false;

  limitDescTopOffset: number;

  constructor(
    private _elementRef: ElementRef,
    private _iterableDiffers: IterableDiffers,
    private _keyValueDiffers: KeyValueDiffers,
    private _translate: TranslateService,
  ) {
    this.differSuggestions = this._iterableDiffers.find([]).create(null);
  }

  ngDoCheck() {
    const changes = this.differSuggestions.diff(this.ac.suggestions);
    if (changes) {
      this.handleSuggestionsChange();
    }

    if (this.limit && this.differPanelVisible) {
      const changesPanelVisible = this.differPanelVisible.diff({ visible: this.ac.panelVisible });
      if (changesPanelVisible) {
        if (this.ac.panelVisible) {
          this.appendLimitDescription();
        }
      }
    }
  }

  ngOnInit() {
    this.ac.autoHighlight = true;

    if (this.limit) {
      this.differPanelVisible = this._keyValueDiffers.find({ visible: this.ac.panelVisible }).create();

      this.adjustLimitDescriptionEl();
    }
  }

  ngAfterViewInit() {
    this.ac.inputEL.nativeElement.addEventListener('keydown', event => {
      if (event.keyCode === 13) {
        this.selectFirstItem = true;

        if (this.ac.panelVisible) {
          this.handleSuggestionsChange();
        }
      } else {
        this.selectFirstItem = false;
      }
    });
  }

  handleSuggestionsChange() {
    if (this.selectFirstItem) {
      this.selectFirstItem = false;

      const targetItem = this.getTargetSuggestion();
      if (targetItem) {
        this.ac.highlightOption = targetItem;
        this.ac.selectItem(targetItem);
        this.ac.hide();
      }
    }
  }

  getTargetSuggestion() {
    let targetItem;
    if (this.ac.suggestions.length) {
      targetItem = this.ac.suggestions[0];
      if (targetItem === null) {
        if (this.ac.suggestions.length > 1) {
          targetItem = this.ac.suggestions[1];
        }
      }
    }

    return targetItem;
  }

  appendLimitDescription() {
    const $panelEl = $(this.ac.panelEL.nativeElement);
    if (!$panelEl.hasClass('panel-limited')) {
      $panelEl.addClass('panel-limited');
    }
    let $limitDescriptionEl = $panelEl.find('.limit-description');
    if (!$limitDescriptionEl.length) {
      $limitDescriptionEl = $(this.ac.panelEL.nativeElement).append(`
        <p class="limit-description">${this._translate.instant('ui.wrapperAutocomplete.limitDescription', { limit: this.limit })}</p>
      `);
    }
  }

  adjustLimitDescriptionEl() {
    const $panelEl = $(this.ac.panelEL.nativeElement);
    $panelEl.on('scroll', () => {
      const $limitDescriptionEl = $panelEl.find('.limit-description');
      if ($limitDescriptionEl.length) {
        if (!this.limitDescTopOffset) {
          this.limitDescTopOffset = $limitDescriptionEl.scrollTop() - 1;
        }
        const panelElScrollTop = $panelEl.scrollTop();
        const topOffsetDeviation = this.limitDescTopOffset - panelElScrollTop;
        $limitDescriptionEl.css('bottom', topOffsetDeviation);
      }
    });
  }
}


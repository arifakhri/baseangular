import { Injectable } from '@angular/core';

@Injectable()
export class SpinnerService {
  htmlLoadingIndicators: any = {
    1: ``,
    // no. 2: default page spinner
    2: `
      <div class='spinner-loader spinner-loader2'></div>
    `,
    // no. 3: small spinner
    3: `
      <div class='spinner-loader spinner-loader3'></div>
    `
  };

  showDefault() {
    return this.show({
      element: $('.main-container'),
      stretchToMain: true,
    });
  }

  show({
    element,
    blockElementDisplay = false,
    title,
    stretchToMain = false,
    loaderType = 2,
  }: {
    element: HTMLDocument,
    blockElementDisplay?: boolean,
    title?: string,
    stretchToMain?: boolean,
    loaderType?: number,
  }) {
    let $targetElement;

    if (!$(element).children('.spinner').length) {
      if ($(element).parent().css('display') === 'inline') {
        $(element).parent().css('display', 'block');
      }
      if ($(element).css('position') === 'static') {
        $(element).css('position', 'relative');
      }

      $targetElement = $(`
        <div class='spinner'>
          ${this.htmlLoadingIndicators[loaderType]}
          <div class='spinner-title'>${title || ''}</div>
        </div>
      `);

      if (stretchToMain) {
        this.stretchToMain($targetElement);
      } else {
        $targetElement.find('.spinner').css('height', $(element).height());
      }

      $(element).append($targetElement);

      if (blockElementDisplay) {
        $(element).css('display', 'block');
      }

      $targetElement.find('.spinner-title').css('padding-top', parseInt($targetElement.find('.spinner-loader').css('height').replace('px', '')) + 60);
    }

    return {
      setTitle: (newTitle) => {
        if ($targetElement) {
          const $el = $($targetElement);
          if ($el.length) {
            const $spinnerTitle = $($el).find('.spinner-title');
            $spinnerTitle.text(newTitle);
            if (newTitle) {
              $spinnerTitle.prop('hidden', false);
            } else {
              $spinnerTitle.prop('hidden', true);
            }
          }
        }
      },
      dispose: () => {
        this.dispose(element);
      }
    };
  }

  dispose(element: HTMLDocument) {
    if ($(element).children('.spinner').length) {
      $(element).children('.spinner').remove();
    }
  }

  stretchToMain(element: HTMLDocument) {
    const mainContainerOffset = $('main.main-container').offset();
    $(element).css({
      position: 'fixed',
      top: mainContainerOffset.top,
      left: mainContainerOffset.left,
    });
  }
}

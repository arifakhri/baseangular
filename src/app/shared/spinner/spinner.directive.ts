import * as _ from 'lodash';
import { ChangeDetectorRef, Directive, ElementRef, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';

import { SpinnerService } from './spinner.service';

@Directive({
  selector: '[appSpinner]',
  exportAs: 'app-spinner',
})
export class SpinnerDirective implements OnChanges, OnInit {
  @Input() parent: HTMLElement;
  @Input() show: boolean = false;
  @Input() stretchToMain: boolean = true;
  @Input() title: string;

  initialized: boolean = false;

  currentSpinner: any;

  constructor(
    private _changeDetectorRef: ChangeDetectorRef,
    private _elementRef: ElementRef,
    private _spinner: SpinnerService,
  ) { }

  ngOnInit() {
    if (this.show) {
      this.toggle();
    }

    this.initialized = true;
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.initialized) {
      if (_.has(changes, 'show') && changes.show.currentValue !== changes.show.previousValue) {
        this.toggle();
      }

      if (_.has(changes, 'title')) {
        if (this.currentSpinner) {
          this.currentSpinner.setTitle(this.title);
        }
      }
    }
  }

  get elParent() {
    return this.parent ? this.parent : this._elementRef.nativeElement;
  }

  toggle() {
    if (this.show) {
      this.forceShow();
    } else {
      this.forceHide();
    }

    this._changeDetectorRef.detectChanges();
  }

  forceShow() {
    this.currentSpinner = this._spinner.show({
      element: this.elParent,
      stretchToMain: this.stretchToMain,
      title: this.title,
    });
  }

  forceHide() {
    if (this.currentSpinner) {
      this.currentSpinner.dispose();
      this.currentSpinner = undefined;
    }
  }
}

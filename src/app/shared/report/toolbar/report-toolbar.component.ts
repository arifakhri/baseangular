import { Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';

import { ReportViewerComponent } from '../viewer/report-viewer.component';

@Component({
  selector: 'app-report-toolbar',
  templateUrl: './report-toolbar.component.html',
})
export class ReportToolbarComponent {
  @Input() viewer: ReportViewerComponent;
  @Output() onRefresh: EventEmitter<boolean> = new EventEmitter;

  @ViewChild('printModal') elPrintModal: ModalDirective;

  print() {
    this.elPrintModal.show();
  };
}

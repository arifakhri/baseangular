import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Injectable } from '@angular/core';

@Injectable()
export class ReportPrintFormService {
  setFormDefinitions(form: FormGroup) {
    form.addControl('marginsType', new FormControl(0, Validators.required));
    form.addControl('landscape', new FormControl(false));
    form.addControl('format', new FormControl('A4', Validators.required));
    form.addControl('printBackground', new FormControl(false));
    form.addControl('fitToPage', new FormControl(true));
  }
}

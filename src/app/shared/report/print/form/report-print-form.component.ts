import { Component, EventEmitter, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { ReportPrintFormService } from './report-print-form.service';

@Component({
  selector: 'app-report-print-form',
  templateUrl: './report-print-form.component.html',
})
export class ReportPrintFormComponent {
  @Output() formSubmit: EventEmitter<any> = new EventEmitter();

  form: FormGroup = new FormGroup({});

  constructor(
    private _reportPrintForm: ReportPrintFormService,
  ) {
    this._reportPrintForm.setFormDefinitions(this.form);
  }
}

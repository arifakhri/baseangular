import { Component, Input } from '@angular/core';

import { ReportViewerComponent } from '../viewer/report-viewer.component';

@Component({
  selector: 'app-report-print',
  templateUrl: './report-print.component.html',
})
export class ReportPrintComponent {
  @Input() viewer: ReportViewerComponent;

  templateConfig: any = {};
}

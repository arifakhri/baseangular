import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

import { ReportPrintComponent } from './print/report-print.component';
import { ReportPrintFormComponent } from './print/form/report-print-form.component';
import { ReportToolbarComponent } from './toolbar/report-toolbar.component';
import { ReportViewerComponent } from './viewer/report-viewer.component';

import { ReportPrintFormService } from './print/form/report-print-form.service';

import { CoreModule } from '../../core/core.module';

export const PROVIDERS = [
  ReportPrintFormService,
];

@NgModule({
  imports: [
    CommonModule,
    CoreModule,
    FlexLayoutModule,
    FormsModule,
    ModalModule,
    ReactiveFormsModule,
    TranslateModule,
  ],
  declarations: [
    ReportPrintComponent,
    ReportPrintFormComponent,
    ReportToolbarComponent,
    ReportViewerComponent,
  ],
  exports: [
    ReportPrintComponent,
    ReportPrintFormComponent,
    ReportToolbarComponent,
    ReportViewerComponent,
  ],
})
export class ReportModule { }

export * from './print/form/report-print-form.service';

import * as _ from 'lodash';
import { Component, EventEmitter, Inject, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';

import { CROSS_MESSAGE_EVENT_PREFIX, EventService, ReportService } from '../../../core/core.module';

@Component({
  selector: 'app-report-viewer',
  exportAs: 'app-report-viewer',
  templateUrl: './report-viewer.component.html',
})
export class ReportViewerComponent implements OnChanges, OnInit {
  @Input() applyConfigOnChange: boolean = false;
  @Input() applyParamOnChange: boolean = false;
  @Input() applyParamOnCrossChange: boolean = false;
  @Input() applyDataOnChange: boolean = false;
  @Input() applyTemplateConfigOnChange: boolean = false;
  @Input() config: any = {};
  @Input() data: any;
  @Input() displayOnInit: boolean = true;
  @Input() downloadButton: any = false;
  @Input() height: string = '700px';
  @Input() initDisplay: string = 'html';
  @Input() params: any = {};
  @Input() printButton: any = false;
  @Input() templateConfig: any = {};
  @Input() width: string = '100%';

  @Output() compReady: EventEmitter<boolean> = new EventEmitter();
  @Output() rendered: EventEmitter<boolean> = new EventEmitter();

  reportSource: any;

  constructor(
    @Inject('GlobalEvent') private _event: EventService,
    private _domSanitizer: DomSanitizer,
    private _report: ReportService,
    private _router: Router,
  ) { }

  ngOnChanges(changes: SimpleChanges) {
    if (
      (this.applyParamOnChange && _.has(changes, 'params')) ||
      (this.applyDataOnChange && _.has(changes, 'data')) ||
      (this.applyTemplateConfigOnChange && _.has(changes, 'templateConfig'))
    ) {
      this.reload();
    } else if (this.applyConfigOnChange && _.has(changes, 'config')) {
      this.applyConfig();
    }
  }

  ngOnInit() {
    if (this.displayOnInit) {
      this.reload();
    }

    this._event.listen(`${CROSS_MESSAGE_EVENT_PREFIX}.reporting.params.patch`).subscribe(event => {
      this.params = Object.assign(this.params, event.data);

      if (this.applyParamOnCrossChange) {
        this.reload();
      }
    });

    this._event.listen(`${CROSS_MESSAGE_EVENT_PREFIX}.ready`).subscribe(event => {
      this.rendered.emit(true);
    });

    this._event.listen(`${CROSS_MESSAGE_EVENT_PREFIX}.navigate`).subscribe(event => {
      const routePath = _.get(event, 'data.url', '/error/404');
      const routeOptions = _.get(event, 'data.options', {});
      this._router.navigateByUrl(routePath, routeOptions);
    });
  }

  applyConfig() {

  }

  reload() {
    let recipe = this.initDisplay;

    switch (this.initDisplay) {
      case 'pdf':
        recipe = 'electron-pdf';
        break;
    }

    this._report.report.renderAsync({
      template: {
        name: 'Main',
        recipe,
        engine: 'none',
        electron: {
          waitForJS: true,
          ...this.templateConfig,
        },
        phantom: {
          waitForJS: true,
          ...this.templateConfig,
        },
        options: {
          debug: {
            logsToResponse: true,
          },
        },
      },
      data: {
        params: this.params,
        data: this.data,
        config: this.config,
      }
    }).then(res => {
      if (this.initDisplay === 'html') {
        this.reportSource = this._domSanitizer.bypassSecurityTrustHtml(res.toString());
      } else if (this.initDisplay === 'pdf') {
        this.reportSource = this._domSanitizer.bypassSecurityTrustResourceUrl(res.toDataURI());
      }

      this.compReady.emit(true);
    });
  }
}

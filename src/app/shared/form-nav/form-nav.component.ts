import { AfterViewInit, Component, ElementRef, Input } from '@angular/core';

@Component({
  selector: 'app-form-nav',
  templateUrl: 'form-nav.component.html',
  styleUrls: ['form-nav.component.scss']
})
export class FormNavComponent implements AfterViewInit {
  @Input() navigations: IFormNav[] = [];

  constructor(
    private _element: ElementRef
  ) { }

  ngAfterViewInit() {
    const mainContainer = $('main.main-container');
    const elFormNav = $(this._element.nativeElement);
    mainContainer.on('scroll', () => {
      const elFormNavTop = $(elFormNav).offset().top;
      const containerTop = mainContainer.scrollTop();
      if (containerTop >= elFormNavTop) {
        const targetMarginTop = containerTop - ($('.topbar').height() || 0) - ($('.header-container').height() || 0);
        elFormNav.css('margin-top', targetMarginTop + 'px');

        this.navigations.forEach((nav, idx) => {
          const navTarget = $(nav.target);
          if (navTarget.length) {
            const offset = navTarget.offset().top;

            if (offset <= 85) {
              this.setActive(idx);
            }
          }
        });
      } else {
        elFormNav.css('margin-top', '0px');
      }
    }).scroll();
  }

  setActive(idx: number) {
    $(this._element.nativeElement).find('li').removeClass('active');
    $(`#nav-${idx}`).addClass('active');
  }

  goto(selector: string) {
    $('main.main-container').animate({
      scrollTop: $(selector).position().top + ($('.topbar').height() || 0) + ($('.header-container').height() || 0)
    }, 500);
  }
}

import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { FormNavComponent } from './form-nav.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    FormNavComponent
  ],
  exports: [
    FormNavComponent
  ]
})
export class FormNavModule { }

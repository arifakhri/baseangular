import * as AccountingJS from 'accounting';
import { Injectable } from '@angular/core';

@Injectable()
export class AccountingService {
  ac: any = AccountingJS;

  constructor() {
    this.ac.settings.currency.symbol = 'Rp. ';
    this.ac.settings.currency.decimal = ',';
    this.ac.settings.currency.thousand = '.';

    this.ac.settings.number.decimal = ',';
    this.ac.settings.number.thousand = '.';
  }

  public roundMoney(m: number, decimal: number = 2): number {
    const factor = Math.pow(10, decimal);
    return Math.round(m * factor) / factor;
  }
}

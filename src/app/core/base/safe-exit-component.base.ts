import * as _ from 'lodash';
import { OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import 'app/rxjs-imports.ts';

export class BSafeExitComponent implements OnDestroy {
  subscriptions: { [key: string]: Subscription } = {};

  ngOnDestroy() {
    _.forEach(this.subscriptions, subscription => {
      subscription.unsubscribe();
    });
  }
}

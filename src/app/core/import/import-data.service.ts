import * as _ from 'lodash';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';

import { ImportExcelService } from './excel/import-excel.service';

export const enum EImportDataTypes {
  OBJECT = 1,
  EXCEL = 2,
}

@Injectable()
export class ImportDataService {
  constructor(
    private _importExcel: ImportExcelService,
  ) { }

  getArrayBuffer(selector: string): Observable<ArrayBuffer> {
    return Observable.create(observer => {
      const file: any = _.first($(selector).prop('files'));
      if (file) {
        const fileReader = new FileReader();
        fileReader.onload = (e) => {
          observer.next((<any>e.target).result);
          observer.complete();
        };
        fileReader.readAsArrayBuffer(file);
      }
    });
  }

  prepare(params: {
    mapOptions: IImportDataMap,
    from: EImportDataTypes,
    contentToProcess?: any,
    fileInputSelector?: string,
  }) {
    let fileContent: any;
    let parsedContent: any;
    let parsedHeader: any;
    let records: any = [];

    const returnSet = {
      content: async (selector?: string) => {
        fileContent = await this.getArrayBuffer(selector || params.fileInputSelector).toPromise();
        return fileContent;
      },
      parse: async (srcFileContent?: any) => {
        switch (params.from) {
          case EImportDataTypes.EXCEL:
            parsedContent = this._importExcel.parse(srcFileContent || fileContent || await returnSet.content());
            parsedHeader = parsedContent[0] || [];
            parsedContent.shift();
            break;
          case EImportDataTypes.OBJECT:
            parsedContent = srcFileContent || params.contentToProcess;
            parsedHeader = Object.keys(parsedContent);
            break;
        }
        return parsedContent;
      },
      header: async () => {
        if (!parsedHeader) {
          await returnSet.parse();
        }

        return parsedHeader;
      },
      transform: async () => {
        records = this.transformRecords({
          mapOptions: params.mapOptions,
          records: await returnSet.parse(),
        });

        return records;
      },
      process: async () => {
        await returnSet.transform();

        return await this.processTransformedRecords({
          mapOptions: params.mapOptions,
          records,
        });
      },
      setMap(newMapOptions: IImportDataMap) {
        params.mapOptions = newMapOptions;
      },
      mapByHeader(header: string[], targetMapOptions: IImportDataMap = params.mapOptions) {
        const matchedMap = _.clone(targetMapOptions);
        for (const hIdx in header) {
          let hFound = false;
          const h = header[hIdx];
          for (const mapKey of Object.keys(matchedMap)) {
            if (hFound) {
              continue;
            }
            const map = targetMapOptions[mapKey];
            const match = _.filter(_.map(map.criteria || [], m => m.test(h)), _.identity).length;
            if (match) {
              map.identifier = parseInt(hIdx, 0);
              hFound = true;
              delete matchedMap[mapKey];
            } else {
              map.identifier = -1;
            }
          }
        }
        returnSet.setMap(targetMapOptions);
      },
    };

    return returnSet;
  }

  transformRecords(params: {
    mapOptions: IImportDataMap,
    records: any,
  }) {
    const transformedRecords = [];
    _.forEach(params.records, record => {
      const transformedRecord = {};

      _.forEach(params.mapOptions, (mapOption, field) => {
        _.set(transformedRecord, field, _.get(record, mapOption.identifier || field) || null);
      });

      transformedRecords.push(transformedRecord);
    });

    return transformedRecords;
  }

  async processTransformedRecords(params: { mapOptions: IImportDataMap, records: any } = { mapOptions: {}, records: [] }) {
    const processedRecords = params.records.slice();

    const mapWithProcessors: any = _.pickBy(params.mapOptions, o => Boolean(o.processor));
    if (_.size(mapWithProcessors)) {
      for (const recordIdx in params.records) {
        const record = params.records[recordIdx];
        for (const field in mapWithProcessors) {
          const mapOption = mapWithProcessors[field];
          _.set(processedRecords, `[${recordIdx}].${field}`, await mapOption.processor(_.get(record, field), record));
        }
      }
    }

    const mapWithProcessorProvided: any = _.pickBy(params.mapOptions, o => Boolean(o.processorProvider));
    if (_.size(mapWithProcessorProvided)) {
      for (const recordIdx in params.records) {
        const record = params.records[recordIdx];
        for (const field in mapWithProcessors) {
          const mapOption = mapWithProcessors[field];
          _.set(processedRecords, `[${recordIdx}].${field}`, this.processProcessorProvider(_.get(record, field), mapOption));
        }
      }
    }

    return processedRecords;
  }

  processProcessorProvider(fieldValue: any, mapOption: IImportDataMapField) {
    let fieldValueToProcess = fieldValue;

    switch (mapOption.processorProvider.preConvertCaseTo) {
      case 'snake':
        fieldValueToProcess = _.snakeCase(fieldValueToProcess);
        break;
    }

    switch (mapOption.processorProvider.convertTo) {
      case 'string':
        fieldValueToProcess = _.toString(fieldValueToProcess);
        break;
      case 'number':
        fieldValueToProcess = _.toNumber(fieldValueToProcess);
        break;
      case 'boolean':
        fieldValueToProcess = Boolean(fieldValueToProcess);
        break;
    }

    if (mapOption.processorProvider.allowedValues) {
      if (!mapOption.processorProvider.allowedValues.includes(fieldValueToProcess)) {
        fieldValueToProcess = mapOption.processorProvider.defaultValue || null;
      }
    }

    return fieldValueToProcess;
  }
}

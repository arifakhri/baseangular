declare interface IImportDataMap {
  [key: string]: IImportDataMapField;
}

declare interface IImportDataMapField {
  identifier?: number | string;
  label?: string;
  processor?: Function|Promise<any>;
  criteria?: RegExp[];
  processorProvider?: {
    defaultValue?: any;
    allowedValues?: string[];
    convertTo?: 'string' | 'number' | 'boolean';
    preConvertCaseTo?: 'snake';
  }
}

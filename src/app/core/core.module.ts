import { CommonModule } from '@angular/common';
import { Injector, ModuleWithProviders, NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { Ng2Webstorage } from 'ngx-webstorage';

import { SharedModule as CentricSharedModule } from '../modules/ng2centric/shared/shared.module';
import { AclService } from './auth/acl.service';
import { AuthenticationService } from './auth/authentication.service';
import { AuthorizationService } from './auth/authorization.service';
import { AutocompleteService } from './common/autocomplete.service';
import { EventService } from './common/event.service';
import { FormService } from './common/form.service';
import { IdleGuardService } from './common/idle-guard.service';
import { InjectorService } from './common/injector.service';
import { RedirectionService } from './common/redirection.service';
import { RouteStateService } from './common/route-state.service';
import { TextParseService } from './common/text-parse.service';
import { TranslatorService } from './common/translator.service';
import { UrlService } from './common/url.service';
import { ExportExcelService } from './export/excel/export-excel.service';
import { ExportDataTableService } from './export/export-data-table.service';
import { ExportDataService } from './export/export-data.service';
import { ExportPDFTemplate1Service } from './export/pdf/export-pdf-template1.service';
import { ExportPDFService } from './export/pdf/export-pdf.service';
import { FieldErrorComponent } from './field-error/field-error.component';
import { FileSecurityModule } from './file/file-security.module';
import { FileService } from './file/file.service';
import { GridTableColumnModule } from './grid-table/column/grid-table-column.module';
import { GridTableFilterService } from './grid-table/grid-table-filter.service';
import { GridTableLoadingService } from './grid-table/grid-table-loading.service';
import { GridTableToggleService } from './grid-table/grid-table-toggle.service';
import { GridTableService } from './grid-table/grid-table.service';
import { ApiBootstrapService } from './http/api-bootstrap.service';
import { ApiFileRestService } from './http/api-file-rest.service';
import { AppVariableService } from './http/app-variable.service';
import { ConnectionStateService } from './http/connection-state.service';
import { HttpExtsrvService } from './http/http-extsrv.service';
import { HttpFileService } from './http/http-file.service';
import { HttpHelperService } from './http/http-helper.service';
import { RequestService } from './http/request.service';
import { StartingDataService } from './http/starting-data.service';
import { SyncVariableService } from './http/sync-variable.service';
import { TokenValidatorService } from './http/token-validator.service';
import { UserVariableService } from './http/user-variable.service';
import { ImportExcelService } from './import/excel/import-excel.service';
import { ImportDataService } from './import/import-data.service';
import { AccountingService } from './math/accounting.service';
import { ReportService } from './report/report.service';
import { CrossMessageService } from './system-message/cross-message.service';
import { LogService } from './system-message/log.service';
import { MessageParserService } from './system-message/message-parser.service';
import { SystemMessageService } from './system-message/system-message.service';

@NgModule({
  imports: [
    CentricSharedModule,
    CommonModule,
    FileSecurityModule,
    GridTableColumnModule,
    HttpModule,
    Ng2Webstorage,
    RouterModule,
    TranslateModule.forChild(),
  ],
  declarations: [
    FieldErrorComponent,
  ],
  exports: [
    FieldErrorComponent,
    FileSecurityModule,
    GridTableColumnModule,
  ]
})
export class CoreModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: CoreModule,
      providers: [
        AccountingService,
        AclService,
        ApiBootstrapService,
        ApiFileRestService,
        AppVariableService,
        AuthenticationService,
        AuthorizationService,
        AutocompleteService,
        ConnectionStateService,
        CrossMessageService,
        ExportDataTableService,
        ExportExcelService,
        ExportPDFService,
        ExportPDFTemplate1Service,
        ExportDataService,
        FileService,
        FormService,
        GridTableFilterService,
        GridTableLoadingService,
        GridTableService,
        GridTableToggleService,
        HttpFileService,
        HttpExtsrvService,
        HttpHelperService,
        IdleGuardService,
        ImportDataService,
        ImportExcelService,
        LogService,
        MessageParserService,
        RedirectionService,
        ReportService,
        RequestService,
        RouteStateService,
        InjectorService,
        SyncVariableService,
        StartingDataService,
        TextParseService,
        TokenValidatorService,
        TranslatorService,
        UrlService,
        UserVariableService,
        {
          provide: 'Rollbar',
          useValue: (<any>window).Rollbar,
        },
        {
          provide: 'GlobalEvent',
          useClass: EventService,
        },
        {
          provide: 'GlobalSystemMessage',
          useClass: SystemMessageService,
        },
      ],
    };
  }

  constructor(
    private _authentication: AuthenticationService,
    private _authorization: AuthorizationService,
    private _crossMessage: CrossMessageService,
    private _idleGuard: IdleGuardService,
    private _injector: Injector,
    private _routeState: RouteStateService,
  ) {
    InjectorService.injector = _injector;
  }
}

export * from './auth/authentication.service';
export * from './auth/authorization.service';
export * from './common/autocomplete.service';
export * from './common/base.model';
export * from './common/common.service';
export * from './common/event.service';
export * from './common/form.service';
export * from './common/idle-guard.service';
export * from './common/injector.service';
export * from './common/redirection.service';
export * from './common/route-state.service';
export * from './common/text-parse.service';
export * from './common/url.service';
export * from './export/export-data-table.service';
export * from './export/excel/export-excel.service';
export * from './export/export-data.service';
export * from './export/pdf/export-pdf.service';
export * from './export/pdf/export-pdf-template1.service';
export * from './grid-table/grid-table.service';
export * from './grid-table/grid-table-config.model';
export * from './grid-table/grid-table-filter.service';
export * from './grid-table/grid-table-toggle.service';
export * from './http/api-bootstrap.service';
export * from './http/api-query-option.model';
export * from './http/app-variable.service';
export * from './http/connection-state.service';
export * from './http/http-helper.service';
export * from './http/request.service';
export * from './http/starting-data.service';
export * from './http/user-variable.service';
export * from './import/import-data.service';
export * from './import/excel/import-excel.service';
export * from './math/accounting.service';
export * from './report/report.service';
export * from './system-message/cross-message.service';
export * from './system-message/log.service';
export * from './system-message/message-parser.service';
export * from './system-message/system-message.model';
export * from './system-message/system-message.service';

import * as _ from 'lodash';
import { Component, ElementRef, Input, ViewEncapsulation } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-field-error',
  templateUrl: 'field-error.component.html',
  styleUrls: ['field-error.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FieldErrorComponent {
  @Input() control: FormControl;

  constructor(
    private _elementRef: ElementRef
  ) { }

  get hasError() {
    return Boolean(_.size(_.get(this, 'control.errors', {})));
  }
}

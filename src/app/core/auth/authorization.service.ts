import * as _ from 'lodash';
import { ActivatedRouteSnapshot, Router, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import 'app/rxjs-imports.ts';

import { AclService } from './acl.service';
import { ApiBootstrapService } from '../http/api-bootstrap.service';
import { AuthenticationService } from './authentication.service';
import { IdleGuardService } from '../common/idle-guard.service';
import { UrlService } from '../common/url.service';

import { environment } from '../../../environments/environment';

@Injectable()
export class AuthorizationService {
  url403 = '/error/403';
  url404 = '/error/404';

  publicRoutePaths: string[] = [
    '/',
    '/login',
    '/registration',
    '/account/activation',
    '/account/confirmation',
    '/account/reset-password',
    '/account/forgot-password',
    '/account/forgot-password/confirmation',
  ];

  constructor(
    private _apiBoostrap: ApiBootstrapService,
    private _authentication: AuthenticationService,
    private _acl: AclService,
    private _idleGuard: IdleGuardService,
    private _router: Router,
    private _url: UrlService,
  ) {
    this.aclGuard();
  }

  aclGuard() {
    if (!this._authentication.user) {
      this._acl.flushRoles();
      this._acl.attachRole('guest');
    }
  }

  /**
   * This is the logic form canActivate route guard.
   * If user logged in and user is unauthorized for target route then show 403 page.
   * If user is not logged in and user is unauthorized for target route then show 404 page.
   */
  canActivate(
    _route: ActivatedRouteSnapshot,
    _state: RouterStateSnapshot
  ): boolean {
    const { urlPath } = this._url.extractHashParts(document.location.origin + _state.url);
    const routeName = _.get(_route, 'data.name', null);

    if (this._acl.can(routeName)) {
      return true;
    }

    if (!this._apiBoostrap.state$.value) {
      return false;
    }

    if (routeName) {
      if (!this._acl.can(routeName)) {
        if (this._authentication.user) {
          if (!environment.production) {
            console.log(`403: Cannot access to ${_state.url}`, this._authentication.user);
          }

          this._router.navigateByUrl(this.url403);
        } else {
          if (!environment.production) {
            console.log(`404: Cannot access to ${_state.url}`);
          }

          this._router.navigate(['/login'], {
            queryParams: {
              destination: _state.url,
            },
          });
        }
        return false;
      }
    } else {
      this._authentication.logout();
    }
    return true;
  }
}

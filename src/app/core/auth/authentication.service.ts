import * as _ from 'lodash';
import { ActivatedRoute, Router } from '@angular/router';
import Axios, { AxiosInstance, AxiosPromise, AxiosResponse, AxiosStatic } from 'axios';
import { BehaviorSubject, Observable } from 'rxjs';
import 'app/rxjs-imports.ts';
import { Headers, Http } from '@angular/http';
import { Inject, Injectable } from '@angular/core';
import { LocalStorage } from 'ngx-webstorage';

import { AclService } from './acl.service';
import { EventService } from '../common/event.service';

import { APP_CONST } from '../../app.const';

export class ICredentialLocal {
  username: string;
  password: string;
}

export class IUser {
  access_token: string;
  email: string;
  firstName: string;
  id: string;
  user_id: string;
  lastName: string;
  refresh_token: string;
  roles: string[];
  user_name: string;
}

@Injectable()
export class AuthenticationService {
  @LocalStorage() private currentUser: IUser;

  user$: BehaviorSubject<IUser> = new BehaviorSubject(null);
  clientId = 'clodeo-main-web';
  maxRefreshTokenRetry: number = 2;
  refreshTokenRunning = false;
  refreshTokenObservable: Observable<string>;

  axios: AxiosInstance = Axios.create({ baseURL: APP_CONST.API_ACCOUNT });

  logoutDestination = '/login';
  private loginDestinations = {
    admin: '/dashboard',
    registration_process: '/registration/company',
  };

  constructor(
    private _acl: AclService,
    @Inject('GlobalEvent') private _event: EventService,
    private _http: Http,
    private _route: ActivatedRoute,
    private _router: Router,
  ) {
    if (this.currentUser) {
      this.populateRoles();
      this.user$.next(this.currentUser);
    } else {
      this.reset();
    }
  }

  set user(userObject: IUser) {
    this.currentUser = userObject;

    this.populateRoles();

    this.user$.next(userObject);
  }

  set userAccessToken(accessToken: string) {
    if (this.currentUser) {
      this.currentUser.access_token = accessToken;

      this.populateRoles();

      this.user$.next(this.currentUser);
    }
  }

  get user() {
    return this.currentUser;
  }

  axiosInterceptors(axios: AxiosInstance | AxiosStatic, interceptRequest: boolean = true, interceptResponse: boolean = true) {
    if (interceptRequest) {
      axios.interceptors.request.use(request => {
        if (this.user) {
          request.headers.common['Authorization'] = `Bearer ${this.user.access_token}`;
        }
        return request;
      });
    }

    if (interceptResponse) {
      axios.interceptors.response.use(response => response, async error => {
        const response = error.response;
        if (
          response &&
          response.status === 401 &&
          response.config &&
          response.config.headers &&
          response.config.headers['Authorization'] &&
          (
            !response.config.refreshTokenRetryCount ||
            response.config.refreshTokenRetryCount < this.maxRefreshTokenRetry
          )
        ) {

          try {
            const refreshedTokenResponse = await this.refreshToken().switchMap(function (accessToken) {
              this.userAccessToken = accessToken;
              response.config.headers['Authorization'] = `Bearer ${accessToken}`;
              return axios.request(response.config);
            }).toPromise();

            return refreshedTokenResponse;
          } catch (errRefreshToken) {
            const retryCount = (response.config.refreshTokenRetryCount || 0);
            if (retryCount < this.maxRefreshTokenRetry - 1) {
              response.config.refreshTokenRetryCount = retryCount + 1;
              return axios.request(response.config);
            } else {
              const urlCheck = this._router.url.substr(0, 6);
              if (urlCheck !== '/login') {
                this._router.navigate(['/login'], { queryParams: { tokenExpired: 'true', destination: this._router.url } });
              }
              throw errRefreshToken;
            }
          }
        }
        throw error;
      });
    }
  }

  login(params: ICredentialLocal): Observable<IUser> {
    const data = this.buildLoginInfo(params);

    const tokenUrl = APP_CONST.API_ACCOUNT + '/token';
    return Observable.fromPromise(
      <AxiosPromise>this.axios.post(tokenUrl, data)
    ).map(response => response.data).do(response => {
      this.attachRole(response);
      this.user = response;
      this._event.emit('CORE:LOGIN:SUCCESS', this.user);
    }).switchMap(() => Observable.of(this.user));
  }

  checkPassword(password: string): Observable<any> {
    const axios = Axios.create({
      baseURL: `${APP_CONST.API_ACCOUNT}/users/me/check-password`,
    });
    this.axiosInterceptors(axios);

    return Observable.fromPromise(
      <Promise<AxiosResponse>>axios.request({
        url: '',
        method: 'post',
        data: `"${password}"`,
        headers: {
          'Content-Type': 'application/json'
        }
      })
    ).map(response => response.data);
  }

  logout(redirectToLogin = true) {
    this.reset();

    let obs = Observable.of(true);
    if (redirectToLogin && this._router.url.indexOf('/login') < 0) {
      obs = Observable.fromPromise(this._router.navigateByUrl(this.logoutDestination).then(() => true));
    }

    obs.subscribe(() => {
      this._event.emit('CORE:LOGOUT:SUCCESS');
    });
  }

  /**
   * Refresh current token if current token isn't available anymore for authentication.
   */
  refreshToken(companyId: string = null): Observable<string> {
    if (!this.user) {
      return Observable.throw(new Error('User is not logged in.'));
    }

    const tokenUrl = APP_CONST.API_ACCOUNT + '/token';

    const data = this.buildRefreshInfo(this.user.refresh_token, companyId);

    if (!this.refreshTokenRunning) {
      this.refreshTokenRunning = true;

      this.refreshTokenObservable = Observable.fromPromise(
        <AxiosPromise>this.axios.post(tokenUrl, data)
      ).map(response => response.data).catch(error => {
        this.refreshTokenRunning = false;

        return Observable.throw(error);
      }).do(response => {
        this.refreshTokenRunning = false;
        this.attachRole(response);
        this.user = response;

        this._event.emit('CORE:AUTHENTICATION:TOKENCHANGED', response.access_token);

      }).switchMap(response => Observable.of(response.access_token));
    }

    return this.refreshTokenObservable;
  }

  /**
   * Set authentication headers needed by Restangular
   */
  setAuthHeader(headers: any): any {
    const token = this.user.access_token;
    if (headers instanceof Headers) {
      headers.delete('Authorization');
      headers.append('Authorization', 'Bearer ' + token);
    } else {
      headers['Authorization'] = 'Bearer ' + token;
    }
    return headers;
  }

  redirectBasedOnRole(): Promise<any> {
    const roles = this._acl.roles;
    const role = _.head(roles);
    const destinationRoute = _.get(this.loginDestinations, role, this.logoutDestination);
    return this._router.navigateByUrl(destinationRoute);
  }

  private attachRole(user: any) {
    if (!_.has(user, 'company_id')) {
      user.roles = ['registration_process'];
    } else {
      user.roles = ['admin'];
    }
  }

  /**
   * Build login x-www-form-urlencoded body values.
   */
  private buildLoginInfo(credentials: any) {
    const data = 'grant_type=' + 'password' +
      '&username=' + credentials.username +
      '&password=' + credentials.password +
      '&client_id=' + this.clientId;

    return data;
  }

  /**
   * Build refresh token x-www-form-urlencoded body values.
   */
  private buildRefreshInfo(refreshToken: string, companyId?: string) {
    let data = 'grant_type=' + 'refresh_token' +
      '&refresh_token=' + refreshToken +
      '&client_id=' + this.clientId;

    if (companyId) {
      data += `&company_id=${companyId}`;
    }

    return data;
  }

  private populateRoles() {
    const roles = _.get(this.user, 'roles', []);
    if (roles.length) {
      this._acl.flushRoles();

      _.forEach(roles, role => {
        this._acl.attachRole(role);
      });
    }
  }

  private reset() {
    this.currentUser = null;
    this.user$.next(null);
    this._acl.flushRoles();
    this._acl.attachRole('guest');
  }
}

import * as _ from 'lodash';
import { Injectable } from '@angular/core';

@Injectable()
export class AclService {
  abilities = {
    guest: [
      'login',
      'logout',

      'account.confirmation',
      'account.activation',
      'account.forgotPassword',
      'account.forgotPasswordConfirmation',
      'account.resetPassword',

      'invitation',

      'registration.account',
    ],
    registration_process: [
      'login',
      'logout',

      'registration.company',
    ],
    admin: [
      'businessPartner.customer.create',
      'businessPartner.customer.list',
      'businessPartner.detail',
      'businessPartner.update',
      'businessPartner.vendor.create',
      'businessPartner.vendor.list',

      'cashBank.list',

      'chatbot.panel',

      'coa.create',
      'coa.detail',
      'coa.ledger',
      'coa.list',
      'coa.update',

      'contact.all',
      'contact.employee.create',
      'contact.employee.detail',
      'contact.employee.list',
      'contact.employee.update',

      'dashboard',

      'expense.create',
      'expense.detail',
      'expense.list',
      'expense.update',

      'journalEntry.create',
      'journalEntry.detail',
      'journalEntry.list',
      'journalEntry.update',

      'login',
      'logout',

      'otherLists',

      'paymentMethod.create',
      'paymentMethod.detail',
      'paymentMethod.list',
      'paymentMethod.update',

      'priceLevel.create',
      'priceLevel.detail',
      'priceLevel.list',
      'priceLevel.update',

      'product.create.inventory',
      'product.create.nonInventory',
      'product.create.service',
      'product.detail',
      'product.import',
      'product.importChannel',
      'product.list.all',
      'product.list.inventory',
      'product.list.nonInventory',
      'product.list.service',
      'product.update',

      'productAttribute.create',
      'productAttribute.list',
      'productAttribute.update',

      'productBrand.create',
      'productBrand.detail',
      'productBrand.list',
      'productBrand.update',

      'productCategory.create',
      'productCategory.list',
      'productCategory.update',

      'productTag.create',
      'productTag.list',
      'productTag.update',

      'profile.changePassword',
      'profile.update',

      'purchases.all',

      'purchasesDebitNote.create',
      'purchasesDebitNote.detail',
      'purchasesDebitNote.list',
      'purchasesDebitNote.update',

      'purchasesDownPayment.create',
      'purchasesDownPayment.detail',
      'purchasesDownPayment.list',
      'purchasesDownPayment.update',

      'purchasesInvoice.create',
      'purchasesInvoice.detail',
      'purchasesInvoice.list',
      'purchasesInvoice.update',

      'purchasesOrder.create',
      'purchasesOrder.detail',
      'purchasesOrder.list',
      'purchasesOrder.update',

      'purchasesPayment.create',
      'purchasesPayment.detail',
      'purchasesPayment.list',
      'purchasesPayment.update',

      'purchasesReceipt.create',
      'purchasesReceipt.detail',
      'purchasesReceipt.list',
      'purchasesReceipt.update',

      'purchasesRefund.create',
      'purchasesRefund.detail',
      'purchasesRefund.update',

      'receiveMoney.create',
      'receiveMoney.detail',
      'receiveMoney.list',
      'receiveMoney.update',

      'reports',

      'reportsAPAging.preview',
      'reportsARAging.preview',
      'reportsBalance.preview',
      'reportsExpense.preview',
      'reportsExpenseDetail.preview',
      'reportsInventory.preview',
      'reportsPNL.preview',
      'reportsPurchasesProduct.preview',
      'reportsPurchases.preview',
      'reportsPurchasesDetail.preview',
      'reportsPurchasesOrder.preview',
      'reportsPurchasesOrderDetail.preview',
      'reportsPurchasesVendor.preview',
      'reportsSales.preview',
      'reportsSalesDetail.preview',
      'reportsSalesOrder.preview',
      'reportsSalesCustomer.preview',
      'reportsSalesProduct.preview',
      'reportsSalesOrderDetail.preview',

      'sales.all',

      'salesCreditNote.create',
      'salesCreditNote.detail',
      'salesCreditNote.list',
      'salesCreditNote.update',

      'salesDownPayment.create',
      'salesDownPayment.detail',
      'salesDownPayment.list',
      'salesDownPayment.update',

      'salesInvoice.create',
      'salesInvoice.detail',
      'salesInvoice.list',
      'salesInvoice.update',

      'salesOrder.create',
      'salesOrder.detail',
      'salesOrder.list',
      'salesOrder.update',

      'salesPayment.create',
      'salesPayment.detail',
      'salesPayment.list',
      'salesPayment.update',

      'salesReceipt.create',
      'salesReceipt.detail',
      'salesReceipt.list',
      'salesReceipt.update',

      'salesRefund.create',
      'salesRefund.detail',
      'salesRefund.update',

      'settings.all',

      'settingsCompany.update',

      'settingsIntegration',

      'settingsProduct.update',

      'settingsPurchase.update',

      'settingsSales.update',

      'settingsUser.create',
      'settingsUser.list',

      'shippingMethod.create',
      'shippingMethod.list',
      'shippingMethod.update',

      'spendMoney.create',
      'spendMoney.detail',
      'spendMoney.list',
      'spendMoney.update',

      'stockOpname.create',
      'stockOpname.detail',
      'stockOpname.import',
      'stockOpname.list',
      'stockOpname.update',

      'tax.create',
      'tax.detail',
      'tax.list',
      'tax.update',

      'transaction.list',

      'uom.create',
      'uom.list',
      'uom.update',

      'warehouse.create',
      'warehouse.list',
      'warehouse.update',

      'warehouseTransfer.create',
      'warehouseTransfer.detail',
      'warehouseTransfer.list',
      'warehouseTransfer.update',
    ]
  };
  roles: string[] = [];

  hasRole(role: string) {
    return this.roles.includes(role);
  }

  attachRole(role: string | string[]) {
    const roles = _.castArray(role);
    this.roles = _.uniq(_.concat(this.roles, roles));
  }

  can(access: string | string[], statisfy: boolean = true) {
    const accesses = _.castArray(access);
    const rolesAbilities = this.roles.map(role => this.abilities[role] || []);
    const abilities = _.uniq(_.concat(_.first([...rolesAbilities])));

    const results = accesses.map(ability => {
      return _.includes(abilities, ability);
    });

    const matchAbilities = results.filter(_.identity).length;
    if (statisfy) {
      return matchAbilities === accesses.length;
    } else {
      return matchAbilities;
    }
  }

  flushRoles() {
    this.roles = [];
  }
}

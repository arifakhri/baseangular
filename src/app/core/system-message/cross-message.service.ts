import * as _ from 'lodash';
import { Inject, Injectable } from '@angular/core';

import { EventService } from '../common/event.service';

export const CROSS_MESSAGE_EVENT_PREFIX = 'cross';

@Injectable()
export class CrossMessageService {
  constructor(
    @Inject('GlobalEvent') private _event: EventService,
  ) {
    window.onmessage = (event) => {
      const messageType = _.get(event, 'data.type', null);
      const data = _.get(event, 'data.data', {});

      if (messageType && _.size(data)) {
        this._event.emit(`${CROSS_MESSAGE_EVENT_PREFIX}.${messageType}`, data);
      }
    };
  }
}

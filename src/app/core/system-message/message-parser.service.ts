import * as _ from 'lodash';
import { Injectable } from '@angular/core';

@Injectable()
export class MessageParserService {
  parse(messages: any) {
    const parsedMessages = [];

    if (_.isArray(messages)) {
      messages.forEach(messageSet => this.parseMessageObject(messageSet, parsedMessages));
    } else if (_.isObject(messages)) {
      this.parseMessageObject(messages, parsedMessages);
    } else if (_.isString(messages)) {
      parsedMessages.push(messages);
    }

    return parsedMessages.filter(_.identity);
  }

  private parseMessageObject(messageSet: any, parsedMessages: string[]) {
    if (_.isString(messageSet)) {
      parsedMessages.push(messageSet);
    } else if (_.has(messageSet, 'errorMessage')) {
      parsedMessages.push(this.parseMessageObjectType1(messageSet));
    } else if (_.has(messageSet, 'response.data[0]')) {
      messageSet.response.data.forEach(message => this.parseMessageObject(message, parsedMessages));
    }
  }

  private parseMessageObjectType1(message: any) {
    return message.errorMessage;
  }
}

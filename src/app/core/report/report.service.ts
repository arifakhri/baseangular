import * as jsreport from 'jsreport-browser-client-dist';
import { Injectable } from '@angular/core';

import { APP_CONST } from '../../app.const';

@Injectable()
export class ReportService {
  report = jsreport;

  constructor() {
    this.report.serverUrl = APP_CONST.API_REPORT_RENDERER;
  }
}

declare interface ICountry {
  id: string;
  code: string;
  name: string;
}

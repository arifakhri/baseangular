import * as _ from 'lodash';

export class MBase {
  set defaultValues(defaultValues: any) {
    _.forEach(defaultValues, (val, key) => {
      if (_.has(this, key)) {
        _.set(this, key, val);
      }
    });
  }
}

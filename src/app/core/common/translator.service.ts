import { Injectable } from '@angular/core';
import { LocalStorage } from 'ngx-webstorage';
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class TranslatorService {
    defaultLanguage: string = 'en';
    availablelangs: any;
    @LocalStorage() currentLang: string;

    constructor(
        private _translate: TranslateService,
    ) {
        if (!this.currentLang) {
            this.currentLang = this.defaultLanguage;
        }
        // this language will be used as a fallback when a translation isn't found in the current language
        _translate.setDefaultLang(this.currentLang);

        this.availablelangs = [
            { code: 'en', text: 'English' },
            { code: 'es_AR', text: 'Spanish' }
        ];

        this.useLanguage(this.currentLang);

    }

    useLanguage(lang: string) {
        this._translate.use(lang);
        this.currentLang = lang;
    }

    getAvailableLanguages() {
        return this.availablelangs;
    }

    getCurrentLang() {
        for (const i in this.availablelangs) {
            if (this.availablelangs[i].code === this.currentLang) {
                return this.availablelangs[i].text;
            }
        }
    }

}

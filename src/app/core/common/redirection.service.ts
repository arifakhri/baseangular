import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { AuthenticationService } from '../auth/authentication.service';

import { UrlService } from './url.service';

@Injectable()
export class RedirectionService {
  constructor(
    private _authentication: AuthenticationService,
    private _router: Router,
    private _url: UrlService,
  ) { }

  redirectAfterLogin(): Promise<any> {
    const checkHash = this._url.extractHashParts();
    if (
      checkHash.urlParams.destination &&
      checkHash.urlParams.destination !== '/' &&
      checkHash.urlParams.destination.substr(0, 6) !== '/login'
    ) {
      const { urlPath, urlParams } = this._url.extractHashParts(checkHash.urlParams.destination);
      return this._router.navigate([urlPath], {
        queryParams: urlParams,
      });
    } else {
      return this._authentication.redirectBasedOnRole();
    }
  }
}

import { Injectable } from '@angular/core';
import { AbstractControl, FormArray, FormControl, FormGroup } from '@angular/forms';
import * as _ from 'lodash';

import { MFieldDefinition, MForm, MFormDefinition, MFormValidationFailed } from './form.model';

@Injectable()
export class FormService {
  build(formDefinition: MFormDefinition, existingForm?: FormGroup) {
    const form = this.buildForm(formDefinition, existingForm) as MForm;

    form.formArrayPush = (fieldParentKey: string, targetForm: FormGroup = form) => {
      const fieldDefinition = this.getFieldDefinition(formDefinition, fieldParentKey);
      if (fieldDefinition) {
        const targetParentKey = fieldParentKey.split('.').slice(-1).pop();
        return this.addFormChild(targetForm, targetParentKey, fieldDefinition.children);
      }
      return false;
    };

    form.formArrayRemove = (fieldParentKey: string, index: number, targetForm: FormGroup = form) => {
      this.removeFormChild(targetForm, fieldParentKey, index);
    };

    form.formArrayClear = (fieldParentKey: string, targetForm: FormGroup = form) => {
      this.emptyFormArray(targetForm, fieldParentKey);
    };

    form.populateFromValues = (formValues: any = {}, targetForm: FormGroup = form, targetFormDefinition: MFormDefinition = formDefinition) => {
      this.populateFromValues(targetForm, targetFormDefinition, formValues);
    };

    return form;
  }

  buildForm(formDefinition: MFormDefinition, existingForm?: FormGroup) {
    const form = existingForm || new FormGroup({});

    formDefinition.forEach(fieldDefinition => {
      form.addControl(fieldDefinition.key, this.buildControl(fieldDefinition));
    });

    return form;
  }

  buildControl(field: MFieldDefinition): AbstractControl {
    const hasChildren = field.children && field.children.length;
    const syncValidatiors = _.castArray(field.syncValidatiors).filter(_.identity);
    const asyncValidators = _.castArray(field.asyncValidators).filter(_.identity);

    if (field.isFormArray) {
      return new FormArray(field.defaultValue || [], syncValidatiors, asyncValidators);
    } else {
      if (hasChildren) {
        return this.buildForm(field.children);
      } else {
        return new FormControl(field.defaultValue, syncValidatiors, asyncValidators);
      }
    }
  }

  addFormChild(existingForm: FormGroup | FormArray, fieldParentKey: string, formDefinition: MFormDefinition) {
    const newFormGroup = this.buildForm(formDefinition);

    let formArray: FormArray;
    if (existingForm instanceof FormGroup) {
      formArray = existingForm.get(fieldParentKey) as FormArray;
    } else {
      formArray = existingForm;
    }

    formArray.push(newFormGroup);

    return newFormGroup;
  }

  removeFormChild(existingForm: FormGroup, fieldParentKey: string, index: number) {
    const formArray = existingForm.get(fieldParentKey) as FormArray;
    formArray.removeAt(index);
  }

  emptyFormArray(existingForm: FormGroup, fieldParentKey: string) {
    const formArray = existingForm.get(fieldParentKey) as FormArray;
    while (formArray.length) {
      formArray.removeAt(0);
    }
  }

  populateFromValues(existingForm: FormGroup | FormArray, formDefinition: MFormDefinition, formValues: any = {}) {
    _.forEach(formDefinition, fieldDefinition => {
      const formValue = _.get(formValues, fieldDefinition.key);
      if (formValue) {
        const control = existingForm.get(fieldDefinition.key);
        if (control) {
          if (fieldDefinition.isFormArray) {
            if (_.isArray(formValue)) {
              const controlArr = control as FormArray;
              _.forEach(formValue, (formValueItem, formValueItemIdx) => {
                const formGroupChild = controlArr.at(formValueItemIdx) || this.addFormChild(controlArr, fieldDefinition.key, fieldDefinition.children);
                this.populateFromValues(<FormGroup>formGroupChild, fieldDefinition.children, formValueItem);
              });
            }
          } else {
            const controlGroup = control as FormGroup;
            controlGroup.patchValue(formValue);
          }
        }
      }
    });
  }

  getFormValidationErrors(formGroup: FormGroup, parentControlKey = '', parentArrayKey = null, arrayControlIndex = null): MFormValidationFailed[] {
    let errors: MFormValidationFailed[] = [];
    _.forEach(formGroup.controls, (control, controlKey) => {
      const targetControlKey = parentControlKey ? `${parentControlKey}.${controlKey}` : controlKey;
      if (control instanceof FormGroup) {
        errors = errors.concat(this.getFormValidationErrors(control, targetControlKey));
      } else if (control instanceof FormArray) {
        (<FormArray>control).controls.forEach((controlChild, controlChildKey) => {
          const targetControlChildKey = `${targetControlKey}.${controlChildKey}`;
          errors = errors.concat(this.getFormValidationErrors(<FormGroup>controlChild, targetControlChildKey, controlKey, controlChildKey));
        });
      }
      const controlErrors = control.errors;
      if (controlErrors !== null) {
        Object.keys(controlErrors).forEach(controlKeyError => {
          errors.push({
            parentArrayKey,
            arrayControlIndex,
            fullControlName: targetControlKey,
            control: control,
            controlName: controlKey,
            errorName: controlKeyError,
            errorInfo: controlErrors[controlKeyError]
          });
        });
      }
    });
    return errors;
  }

  private getFieldDefinition(formDefinition: MFormDefinition, fieldNameSearch: string) {
    const fieldNames = fieldNameSearch.split('.');

    let currentFormDefinition = formDefinition;
    let currentField;
    for (const fieldName of fieldNames) {
      currentField = _.find(currentFormDefinition, { key: fieldName });

      if (!currentField || (currentField && !currentField.children)) {
        return false;
      }

      currentFormDefinition = currentField.children;
    }

    return currentField;
  }
}

import * as _ from 'lodash';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import 'app/rxjs-imports.ts';

export class AppEvent {
  name: string;
  timestamp: Date;
  data: any;

  constructor(eventName: string, data: any) {
    this.timestamp = new Date();
    this.name = eventName;
    this.data = data;
  }
}

@Injectable()
export class EventService {
  private eventObservable: Observable<AppEvent>;
  private eventSubject = new Subject<AppEvent>();

  constructor() {
    this.eventObservable = this.eventSubject.asObservable();
  }

  listen(eventName: string | string[]): Observable<AppEvent> {
    const targetEventNames: string[] = _.castArray(eventName);
    return this.eventObservable.filter(ev => targetEventNames.includes(ev.name));
  }

  emit(eventName: string, data: any = null) {
    const ev: AppEvent = new AppEvent(eventName, data);
    this.eventSubject.next(ev);
  }
}

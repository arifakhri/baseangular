import * as _ from 'lodash';
import { AutoComplete } from 'primeng/components/autocomplete/autocomplete';
import { Injectable } from '@angular/core';

@Injectable()
export class AutocompleteService {
  onLocalACDropdown(ac: AutoComplete, options: any, prependNull: boolean = false) {
    ac.suggestions = !prependNull ? options : this.prependNull(options);
  }

  onLocalACSearch(ac: AutoComplete, options: any, keys: string[] = [], query: any = '', prependNull: boolean = false) {
    const suggestions = options.filter(value => {
      const regEx = new RegExp(query, 'ig');
      return keys.filter(key => {
      const valueString = _.toString(_.get(value, key, ''));
        return regEx.test(valueString);
      }).length;
    });

    ac.suggestions = !prependNull ? suggestions : this.prependNull(suggestions);
  }

  private prependNull(suggestions) {
    return [null].concat(suggestions || []);
  }
}

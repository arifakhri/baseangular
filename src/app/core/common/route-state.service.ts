import { Injectable } from '@angular/core';
import { NavigationStart, Router } from '@angular/router';

@Injectable()
export class RouteStateService {
  previousUrl: string;
  currentUrl: string;

  constructor(private router: Router) {
    router.events
      .filter(event => event instanceof NavigationStart)
      .subscribe(e => {
        this.previousUrl = this.currentUrl;
        this.currentUrl = (<any>e).url;
      });
  }
}

import * as _ from 'lodash';
import * as Domurl from 'domurl';
import { Injectable } from '@angular/core';

@Injectable()
export class UrlService {
  extractHashParts(sourceRouteUrl: string = window.location.toString()) {
    const targetUrl = new Domurl(sourceRouteUrl.replace('/#/', '/'));

    let urlOrigin = targetUrl.protocol + '://' + targetUrl.host;
    if (targetUrl.port) {
      urlOrigin += `:${targetUrl.port}`;
    }

    return {
      urlOrigin,
      urlParams: targetUrl.query,
      urlPath: targetUrl.path,
    };
  }

  objectToQueryString(obj) {
    const qs = _.reduce(obj, (result, value, key) => {
      if (!_.isNull(value) && !_.isUndefined(value)) {
        if (_.isArray(value)) {
          result += _.reduce(value, (result1, value1) => {
            if (!_.isNull(value1) && !_.isUndefined(value1)) {
              result1 += key + '=' + encodeURIComponent(value1) + '&';
              return result1;
            } else {
              return result1;
            }
          }, '');
        } else {
          result += key + '=' + encodeURIComponent(<string>value) + '&';
        }
        return result;
      } else {
        return result;
      }
    }, '').slice(0, -1);

    return qs;
  }
}

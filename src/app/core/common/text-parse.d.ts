declare interface IParseMap {
  explodeOperator: string;
  fields: {
    [key: string]: {
      patterns: RegExp[],
      explodeLines?: boolean
      mapLine?: Function
    }
  };
}

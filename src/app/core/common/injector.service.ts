import { Injector } from '@angular/core';

export class InjectorService {
  static injector: Injector;

  static get<T>(varClass: any): T {
    return InjectorService.injector.get(varClass);
  }
}

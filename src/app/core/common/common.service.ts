import * as _ from 'lodash';
import { AutoComplete } from 'primeng/components/autocomplete/autocomplete';
import { FormArray, FormControl, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';
import { constants } from 'os';

export class CommonService {
  static getUniqueId(size: number = 9) {
    return _.sampleSize(<any>'ABCDEFGHIJKLMNOPQRSTUVWXZY1234567890!@#$%^&*()', size).join('');
  }

  static getMultipleUniqueId(total: number, size: number = 9) {
    const uniqueIds = [];

    while (uniqueIds.length < total) {
      let uniqueId = CommonService.getUniqueId(size);
      while (uniqueIds.includes(uniqueId)) {
        uniqueId = CommonService.getUniqueId(size);
      }

      uniqueIds.push(uniqueId);
    }

    return uniqueIds;
  }

  static filterArrayLike(arr: any[], pathToMatch: string, query: string) {
    return arr.filter(o => {
      const regex = new RegExp(query, 'gi');
      const valToMatch = _.get(o, pathToMatch, '');
      return regex.test(valToMatch);
    });
  }

  static markAsDirty(group: FormGroup | FormArray) {
    group.markAsDirty();
    for (const i in group.controls) {
      if (group.controls[i] instanceof FormControl) {
        group.controls[i].markAsDirty();
      } else {
        CommonService.markAsDirty(group.controls[i]);
      }
    }
  }

  static iterateFormControls(group: FormGroup | FormArray, iteratee: Function, prevKey: string = '') {
    for (const i in group.controls) {
      const targetKey = prevKey.length ? `.${i}` : i;
      if (group.controls[i] instanceof FormControl) {
        iteratee(group.controls[i], prevKey + targetKey);
      } else {
        CommonService.iterateFormControls(group.controls[i], iteratee, prevKey + targetKey);
      }
    }
  }

  static syncFormControls(controls: any = {}) {
    _.forEach<any, any>(controls, control => {
      control.to.setValue(control.from.value);
      control.from.valueChanges.debounceTime(500).subscribe(value => {
        control.to.setValue(value);
      });
    });
  }

  static baseName(targetPath: string) {
    return targetPath.split(/[\\/]/).pop();
  }

  // TODO: Refactor autocomplete related functions below to a new service rather than being here

  static searchLocalACItems(ev, sourceVarName: string, suggestionVarName: string) {
    this[suggestionVarName] = [];
    this[suggestionVarName] = CommonService.filterArrayLike(this[sourceVarName], 'name', ev.query);
  }

  static onLocalACDropdown(ev, sourceVarName: string, suggestionVarName: string) {
    setTimeout(() => {
      this[suggestionVarName] = this[sourceVarName];
    }, 100);
  }

  static remoteACItemsHandler(params: {
    remoteParams: Function,
    remoteRequest: Function,
    remoteRequestMap: Function,
    ACConfig?: any,
    element?: Function,
  }) {
    const store: any = {};
    const bind = (type) => {
      const methodParams = [
        store,
        type,
        params.remoteParams,
        params.remoteRequest,
        params.remoteRequestMap,
        params.ACConfig || {},
      ];
      if (params.element) {
        methodParams.push(params.element);
      }
      return CommonService.populateRemoteACItems.bind({}, ...methodParams);
    };
    return {
      search: bind('search'),
      dropdown: bind('dropdown'),
    };
  }

  static observableWaitAll(obs, name) {
    return Observable.zip(...(_.get(obs, name, [])) || []);
  }

  private static populateRemoteACItems<T>(
    store: {
      [key: string]: any,
    },
    type: string,
    remoteParams: Function,
    remoteRequest: Function,
    remoteRequestMap: Function,
    ACConfig: any = {},
    element: Function | AutoComplete,
    event: any,
  ) {
    let suggestions = [];

    let targetElement: AutoComplete;
    if (_.isFunction(element)) {
      targetElement = element();
    } else {
      targetElement = <AutoComplete>element;
    }

    if (!$(targetElement.inputEL.nativeElement).hasClass('loading')) {
      $(targetElement.inputEL.nativeElement).addClass('loading');
    }

    store.lastQuery = store.currentQuery;
    store.currentQuery = event.query;

    const lastSuggestion = _.get(store, 'lastSuggestions', []);
    if (store.currentQuery === store.lastQuery && lastSuggestion.length > 1 && store.skipThisIfLine) {
      /*
      *  restore cached suggestions if query is unchanged and cached suggestions are more than 1
      *  but currently always skip this block because hasn't been accepted by boss
      */
      suggestions = store.lastSuggestions;
      store.obsSuggestions = Observable.of(suggestions);
    } else {
      const params = remoteParams(event, type);
      store.obsSuggestions = remoteRequest(...params).map(remoteRequestMap).do(remoteSuggestions => {
        store.lastSuggestions = remoteSuggestions;
      });
    }

    store.lastExecute = type;

    if (store.subSuggestions) {
      store.subSuggestions.unsubscribe();
    }
    store.subSuggestions = store.obsSuggestions.subscribe(populatedSuggestions => {
      if (ACConfig.prependNull) {
        populatedSuggestions = _.range(0, +ACConfig.prependNull).map(() => null).concat(populatedSuggestions);
      }

      targetElement.suggestions = populatedSuggestions;
      targetElement.handleSuggestionsChange();

      $(targetElement.inputEL.nativeElement).removeClass('loading');
    });
  }
}

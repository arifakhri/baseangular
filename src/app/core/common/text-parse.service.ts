import * as _ from 'lodash';
import { Injectable } from '@angular/core';

@Injectable()
export class TextParseService {
  parse(text: string, parserOptions: IParseMap) {
    const results = {};
    const chunks = text.split(parserOptions.explodeOperator);
    chunks.forEach(chunk => {
      const lastField = _.last(_.keys(results));
      const content = this.parseContent(chunk, lastField, parserOptions);
      if (content) {
        if (_.has(results, content.field)) {
          results[content.field] = _.castArray(results[content.field]);
          results[content.field].push(content.text);
        } else {
          results[content.field] = content.text;
        }
      }
    });

    this.normalizeResults(results);

    return results;
  }

  private parseContent(chunk: string, lastField: string, parserOptions: IParseMap) {
    let field;
    let text;

    _.forEach(parserOptions.fields, (fieldOption, fieldName) => {
      if (field) {
        return;
      }

      const pattern = _.first(
        _.filter(
          fieldOption.patterns.map(regEx => {
            if (regEx.test(chunk)) {
              return regEx;
            }
            return false;
          })
          , _.identity
        )
      );
      if (pattern) {
        field = fieldName;
        text = chunk.replace(pattern, '').trim();
      }
    });

    if (!field) {
      field = lastField;
      text = chunk.trim();
    }

    const mapLine: Function = <any>_.get(parserOptions.fields, `${field}.mapLine`, (value) => value);
    if (mapLine) {
      text = mapLine(text);
    }

    return field ? { field, text } : false;
  }

  private normalizeResults(results) {
    _.forEach(results, (result, fieldName) => {
      if (_.isArray(result)) {
        results[fieldName] = _.filter(result, _.identity);
      }
    });
  }
}

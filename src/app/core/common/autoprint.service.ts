import { AxiosInstance } from 'axios';
import { Inject } from '@angular/core';
import { ReportService, SystemMessageService } from '../core.module';
import { APP_CONST } from '../../app.const';

export class AutoPrintService {
axios: AxiosInstance;

    constructor(
        private _report: ReportService,
        @Inject('GlobalSystemMessage') private _globalSystemMessage: SystemMessageService,
    ) { }

    fromBase64(base64: string) {
        this.axios.post(APP_CONST.API_AUTO_PRINT, {base64: base64})
            .then((res) => {
                if (res.status !== 200) {
                    this._globalSystemMessage.log({
                        message: 'Error, please make sure your "Clodeo Auto Print" is running',
                        type: 'error',
                        showAs: 'growl',
                        showSnackBar: false,
                    });
                }
            });
    }

    fromTemplate(params, data, config) {
        this._report.report.renderAsync({
            template: {
                name: 'Main',
                recipe: 'electron-pdf',
                engine: 'none',
                electron: {
                  waitForJS: true,
                },
                phantom: {
                  waitForJS: true,
                },
                options: {
                  debug: {
                    logsToResponse: true,
                  },
                },
              },
              data: {
                params: params,
                data: data,
                config: config,
              }
            }).then((res) => {
                this.fromBase64(res.toString());
            });
    }
}

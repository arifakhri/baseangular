import * as _ from 'lodash';

export class MCommon {
  set(values: any) {
    _.assign(this, values);
  }
}

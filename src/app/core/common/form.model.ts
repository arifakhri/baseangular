import { AbstractControl, AsyncValidatorFn, FormGroup, ValidatorFn } from '@angular/forms';

export type MFormDefinition = MFieldDefinition[];

export class MFieldDefinition {
  key: string;

  defaultValue?: any;
  isFormArray?: boolean;
  asyncValidators?: AsyncValidatorFn | AsyncValidatorFn[];
  syncValidatiors?: ValidatorFn | ValidatorFn[];
  children?: MFieldDefinition[];
}

export class MForm extends FormGroup {
  formArrayPush: (fieldParentKey: string) => false | FormGroup;
  formArrayRemove: (fieldParentKey: string, index: number) => void;
  formArrayClear: (fieldParentKey: string) => void;
  populateFromValues: (formValues: any, existingForm?: FormGroup, formDefinition?: MFormDefinition) => void;
}

export class MFormValidationFailed {
  fullControlName: string;
  controlName: string;
  control: AbstractControl;
  errorName: string;
  errorInfo: {
    [key: string]: any;
  };

  parentArrayKey?: string;
  arrayControlIndex?: number;
}

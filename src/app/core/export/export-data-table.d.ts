declare interface IExportDataTableMap {
  [key: string]: {
    header?: string;
    formatter?: Function;
    emptyPlaceholder?: string;
  }
}
declare interface IStartingData {
  companyId: string;
  companyName: string;
  masterBranchId: number;
  masterPriceLevelId: string;
  masterWarehouseId: string;
}

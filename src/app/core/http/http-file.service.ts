import 'app/rxjs-imports.ts';

import { Injectable } from '@angular/core';
import { Http, Response, ResponseContentType } from '@angular/http';
import { Observable } from 'rxjs';

import { CommonService } from '../common/common.service';
import { FileService } from '../file/file.service';

@Injectable()
export class HttpFileService {
  constructor(
    private _file: FileService,
    private _http: Http,
  ) { }

  downloadFileToBase64(fileUrl: string): Observable<{
    base64: string,
    contentType: string,
    fileName: string,
    fileSize: number,
  }> {
    return this._http
      .get(fileUrl, { responseType: ResponseContentType.Blob })
      .map((res: Response) => res.blob())
      .switchMap((fileBlob) => {
        return this._file.blobToBase64(fileBlob)
          .switchMap(base64File => {
            return Observable.of({
              base64: base64File.replace(`data:${fileBlob.type};base64,`, ''),
              contentType: fileBlob.type,
              fileName: CommonService.baseName(fileUrl),
              fileSize: fileBlob.size,
            });
          });
      });
  }
}

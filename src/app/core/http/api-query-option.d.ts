declare interface IApiQueryOptionSort {
    field: string;
    dir: string;
}

declare interface IApiQueryOptionFilterValue {
    field: string;
    operator: string;
    value: string;
    ignoreCase?: boolean;
}

declare interface IApiQueryOptionFilter {
    filterValues: IApiQueryOptionFilterValue[];
}
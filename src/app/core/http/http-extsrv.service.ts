import { Injectable } from '@angular/core';
import { AxiosInstance, AxiosResponse, AxiosStatic } from 'axios';
import * as _ from 'lodash';

@Injectable()
export class HttpExtsrvService {
  axiosInterceptors(axios: AxiosInstance | AxiosStatic) {
    axios.interceptors.response.use(response => {
      if (_.has(response, 'data.errors') && _.has(response, 'data.data')) {
        if (_.has(response.data.errors, 'error_message')) {
          this.handleErrors(response.data.errors);
        }

        _.set(response.data, 'summary', {});
      }

      return response;
    }, error => error);
  }

  private handleErrors(response: AxiosResponse) {
    const errorMessages = [];

    _.forEach(response.data.errors, error => {
      errorMessages.push(_.get(error, 'error_message'));

      if (error.error_code) {
        _.set(response.data, `summary.${error.error_code}`, true);
      }
    });

    console.log(errorMessages);
  }
}

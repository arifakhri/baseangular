import { Injectable } from '@angular/core';
import Axios, { AxiosInstance, AxiosRequestConfig, CancelTokenStatic } from 'axios';
// import * as axiosRetry from 'axios-retry';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';

import { AuthenticationService } from '../auth/authentication.service';

@Injectable()
export class RequestService {
  axios: AxiosInstance;
  cancelToken: CancelTokenStatic = Axios.CancelToken;

  constructor(
    private _auth: AuthenticationService,
  ) { }

  new(baseURL: string, useDefaultInterceptor: boolean = true): RequestService {
    const axios: AxiosInstance = Axios.create({ baseURL });
    // (<any>axiosRetry)(axios, { retries: 3 });

    if (useDefaultInterceptor) {
      this._auth.axiosInterceptors(axios);
    }

    const requestService = new RequestService(this._auth);
    requestService.axios = axios;

    return requestService;
  }

  post<T>(url: string, data: any = null, config: AxiosRequestConfig = {}) {
    return new Observable<T>(subscriber => {
      const cancelSource = this.cancelToken.source();
      config.cancelToken = cancelSource.token;

      const promise = this.axios.post(url, data, config).then(response => response.data);
      Observable.fromPromise(promise).subscribe(subscriber);

      return () => cancelSource.cancel();
    });
  }

  put<T>(url: string, data: any = null, config: AxiosRequestConfig = {}) {
    return new Observable<T>(subscriber => {
      const cancelSource = this.cancelToken.source();
      config.cancelToken = cancelSource.token;

      const promise = this.axios.put(url, data, config).then(response => response.data);
      Observable.fromPromise(promise).subscribe(subscriber);

      return () => cancelSource.cancel();
    });
  }

  get<T>(url: string, config: AxiosRequestConfig = {}) {
    return new Observable<T>(subscriber => {
      const cancelSource = this.cancelToken.source();
      config.cancelToken = cancelSource.token;

      const promise = this.axios.get(url, config).then(response => response.data);
      Observable.fromPromise(promise).subscribe(subscriber);

      return () => cancelSource.cancel();
    });
  }

  delete<T>(url: string, data: any = null, config: AxiosRequestConfig = {}) {
    return new Observable<T>(subscriber => {
      const cancelSource = this.cancelToken.source();
      config.cancelToken = cancelSource.token;

      const promise = this.axios.request({
        url,
        method: 'delete',
        data,
        headers: {
          'Content-Type': 'application/json'
        }
      }).then(response => response.data);
      Observable.fromPromise(promise).subscribe(subscriber);

      return () => cancelSource.cancel();
    });
  }
}

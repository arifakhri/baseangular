declare interface IApiPaginationResult<T> {
    data: T[];
    skip: number;
    take: number;
    total: number;
}

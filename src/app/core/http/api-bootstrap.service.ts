import * as _ from 'lodash';
import { BehaviorSubject, Observable } from 'rxjs';
import 'app/rxjs-imports.ts';
import { Inject, Injectable } from '@angular/core';

import { AppVariableService } from './app-variable.service';
import { AuthenticationService } from '../auth/authentication.service';
import { EventService } from '../common/event.service';
import { StartingDataService } from './starting-data.service';
import { UserVariableService } from './user-variable.service';

@Injectable()
export class ApiBootstrapService {
  state$: BehaviorSubject<boolean> = new BehaviorSubject(null);

  constructor(
    private _appVariable: AppVariableService,
    private _userVariable: UserVariableService,
    private _authentication: AuthenticationService,
    @Inject('GlobalEvent') private _event: EventService,
    private _startingData: StartingDataService
  ) {
    this._event.listen('CORE:LOGOUT:SUCCESS').subscribe(() => {
      this.state$.next(null);
    });
  }

  get change$(): Observable<boolean> {
    return this.state$.filter(state => typeof (state) === 'boolean');
  }

  boot(reboot: boolean = false): Observable<boolean> {
    let obs: Observable<boolean> = Observable.of(true);
    const booted = this.state$.getValue();
    if (!booted || reboot) {
      const userRoles = _.get(this._authentication, 'user.roles', []);
      if (this._authentication.user && !userRoles.includes('registration_process')) {
        obs = Observable.concat(
          this._startingData.fetch(),
          this._appVariable.load(),
          this._userVariable.load(),
        ).do(this.success.bind(this), this.failed.bind(this)).map(() => true);
      } else if (userRoles.includes('registration_process')) {
        this.success();
      } else {
        this.removePreloader();
      }
    }
    return obs;
  }

  removePreloader() {
    (<any>window).appBootstrap && (<any>window).appBootstrap();
  }

  destroy() {
    this.success();
  }

  private failed() {
    this.removePreloader();

    const state = this.state$.getValue();
    if (typeof (state) !== 'boolean' || state) {
      this.state$.next(false);
    }
  }

  private success() {
    this.removePreloader();

    const state = this.state$.getValue();
    if (typeof (state) !== 'boolean' || !state) {
      this.state$.next(true);
    }
  }
}

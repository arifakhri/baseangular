import { Http } from '@angular/http';
import { BehaviorSubject, Observable } from 'rxjs';
import 'app/rxjs-imports.ts';
import { Injectable } from '@angular/core';

@Injectable()
export class ConnectionStateService {
  ajax: XMLHttpRequest;
  pingURL: string = 'http://clodeo-api-main-demo.azurewebsites.net/ping';
  state$: BehaviorSubject<boolean> = new BehaviorSubject(null);

  constructor(
    private _http: Http
  ) {
    // setInterval(this.detect.bind(this), 1000);
    // this.disconnected();
    setTimeout(this.connected.bind(this), 5000);
  }

  get change$(): Observable<boolean> {
    return this.state$.asObservable().filter(state => typeof (state) === 'boolean');
  }

  private detect() {
    this._http.get(this.pingURL)
      .catch(this.disconnected.bind(this))
      .subscribe(this.connected.bind(this));
  }

  private disconnected() {
    const state = this.state$.getValue();
    if (typeof (state) !== 'boolean' || state) {
      this.state$.next(false);
    }
  }

  private connected() {
    const state = this.state$.getValue();
    if (typeof (state) !== 'boolean' || !state) {
      this.state$.next(true);
    }
  }
}

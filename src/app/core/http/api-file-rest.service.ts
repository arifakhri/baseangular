import { Injectable } from '@angular/core';

import { RequestService } from './request.service';

import { APP_CONST } from '../../app.const';

@Injectable()
export class ApiFileRestService {
  requestBase64 = this._request.new(`${APP_CONST.API_FILE}/upload-base64`);

  constructor(
    private _request: RequestService,
  ) { }

  uploadBase64CompanyFile(
    base64Image: string,
    fileNameWithExtension: string,
    contentType: string,
    publicFile: boolean = true,
  ) {
    return this.requestBase64.post<any>(`company`, {
      base64Image,
      fileNameWithExtension,
      contentType,
      publicFile,
    });
  }

  uploadTempBase64File(
    base64Image: string,
    fileNameWithExtension: string,
    contentType: string,
    publicFile: boolean = true,
  ) {
    return this.requestBase64.post<any>(`temporary`, {
      base64Image,
      fileNameWithExtension,
      contentType,
      publicFile,
    });
  }
}

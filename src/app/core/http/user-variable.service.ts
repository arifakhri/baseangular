import * as _ from 'lodash';
import Axios, { AxiosInstance, AxiosResponse } from 'axios';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import 'app/rxjs-imports.ts';

import { AuthenticationService } from '../auth/authentication.service';
import { SyncVariableService } from './sync-variable.service';

import { APP_CONST } from '../../app.const';

@Injectable()
export class UserVariableService {
  private appId: string = 'main-web';
  private axios: AxiosInstance;
  private baseURL = `${APP_CONST.API_MAIN}/app-user-variables`;
  private lastVariables: IUserVariable[] = [];
  private variables: IUserVariable[] = [];

  constructor(
    private _auth: AuthenticationService,
    private _syncVariable: SyncVariableService,
  ) {
    this.axios = Axios.create();
    this.axios.defaults.baseURL = this.baseURL;

    _auth.axiosInterceptors(this.axios);
  }

  get(id: string, defaultValue?: any) {

    let variable = _.find(this.variables, { id });

    if (!variable) {
      variable = { ...defaultValue, id };
      this.variables.push(variable);
    }

    return variable;
  }

  load(): Observable<IUserVariable[]> {
    return Observable.fromPromise(
      <Promise<AxiosResponse>>this.axios.get(this.appId)
    ).map(response => response.data).do(variables => {
      const unserializedVariables = this._syncVariable.unserializeVariables(variables);
      this.lastVariables = <any>_.map(unserializedVariables.slice(), _.clone);
      this.variables = <any>_.map(unserializedVariables.slice(), _.clone);
    });
  }

  update(): Observable<IUserVariable[]> {
    let sourceObservable: Observable<any>;

    const changedVariables = this._syncVariable.getChangedVariables(this.lastVariables, this.variables);
    if (changedVariables.length) {
      const serializedVariables = this._syncVariable.serializeVariables(changedVariables);
      sourceObservable = Observable.fromPromise(
        <Promise<AxiosResponse>>this.axios.post(this.appId, { variables: serializedVariables })
      );
      this.updateLastVariables(<IUserVariable[]>changedVariables);
    } else {
      sourceObservable = Observable.of(false);
    }

    return sourceObservable;
  }

  updateLastVariables(changedVariables: IUserVariable[]) {
    _.forEach(changedVariables, changedVariable => {
      const varIdx = _.findIndex(this.lastVariables, { id: changedVariable.id });
      const changedVariableCopy = Object.assign({}, changedVariable);
      if (varIdx) {
        this.lastVariables[varIdx] = changedVariableCopy;
      } else {
        this.lastVariables.push(changedVariableCopy);
      }
    });
  }
}

import * as _ from 'lodash';
import { Injectable } from '@angular/core';

@Injectable()
export class SyncVariableService {
  serializeVariables(variables: IBVariable[] = []) {
    return variables.map(variable => {
      try {
        variable.value = JSON.stringify(variable.value);
      } catch (e) {

      }
      return variable;
    });
  }

  unserializeVariables(variables: IBVariable[] = []) {
    return variables.map(variable => {
      try {
        variable.value = JSON.parse(variable.value);
      } catch (e) {

      }
      return variable;
    });
  }

  getChangedVariables(lastVariables: IBVariable[], variables: IBVariable[]): IBVariable[] {
    const changedVariables = [];
    _.forEach(variables, (variable) => {
      const lastVariable = _.find(lastVariables, { id: variable.id });
      if (lastVariable && JSON.stringify(variable.value) !== JSON.stringify(lastVariable.value)) {
        changedVariables.push(variable);
      } else if (!lastVariable) {
        const newVariable: any = Object.assign({}, variable);
        changedVariables.push(newVariable);
      }
    });
    return changedVariables;
  }
}

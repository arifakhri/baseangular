import Axios, { AxiosInstance } from 'axios';
import { BehaviorSubject, Observable } from 'rxjs';
import 'app/rxjs-imports.ts';
import { Injectable } from '@angular/core';

import { AuthenticationService } from '../auth/authentication.service';

import { APP_CONST } from '../../app.const';

@Injectable()
export class StartingDataService {
  axios: AxiosInstance;
  baseURL = APP_CONST.API_MAIN;

  data$: BehaviorSubject<IStartingData> = new BehaviorSubject(null);

  constructor(
    private _authentication: AuthenticationService,
  ) {
    this.axios = Axios.create();
    this.axios.defaults.baseURL = this.baseURL;

    _authentication.axiosInterceptors(this.axios);
  }

  get data(): IStartingData {
    return this.data$.value;
  }

  fetch(): Observable<IStartingData> {
    return Observable.fromPromise(
      <Promise<IStartingData>>this.axios.get(`starting-data`).then(
        response => response.data
      )
    ).do(data => this.data$.next(data));
  }
}

export class MHttpExtsrvResponse<T> {
  summary: any;
  data: T;
  errors: MHttpExtsrvError[];
}

export class MHttpExtsrvError {
  error_code: string;
  error_message: string;
}

declare interface IBVariable {
  id: string;
  appId: string;
  value: any;
  variableGroup: string;
}

declare interface IAppVariable extends IBVariable { }

declare interface IUserVariable extends IBVariable {
  userId: string;
}
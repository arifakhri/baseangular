import { DataTable } from 'primeng/components/datatable/datatable';
import { Injectable } from '@angular/core';

import { ApiQueryOption } from '../http/api-query-option.model';
import { GridTableFilterService } from './grid-table-filter.service';
import { GridTablePagerConfig } from './grid-table-config.model';

@Injectable()
export class GridTableService {

  constructor(
    private _gridTableFilter: GridTableFilterService,
  ) { }

  generateApiQueryOptionFromGridInfo(
    pager: GridTablePagerConfig,
    includeTotalCount: boolean,
    dataTable: DataTable,
    customFilters?: { [key: string]: IApiQueryOptionFilterValue[] }
  ): ApiQueryOption {

    const qOption = new ApiQueryOption();
    qOption.includeTotalCount = includeTotalCount;

    if (pager) {
      qOption.take = pager.itemsPerPage;
      qOption.skip = (pager.pageNum - 1) * pager.itemsPerPage;
    }

    if (dataTable) {
      if (dataTable.sortField) {
        const so = (dataTable.sortOrder === 1) ? 'asc' : 'desc';
        qOption.sort.push({ field: dataTable.sortField, dir: so });
      }

      this._gridTableFilter.appendQBodyFilters(qOption, customFilters);
    }

    return qOption;
  }

  generateCombinedApiFilters(
    query: string,
    filterMap: any
  ): {
      [key: string]: IApiQueryOptionFilterValue[]
    } {
    const queries = {
      default: query,
    };

    const { qBody } = this._gridTableFilter.parse(queries, filterMap);

    return qBody;
  }
}

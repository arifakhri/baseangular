declare interface IGridTableFilterMap {
  [key: string]: boolean | {
    operator?: string;
    group?: 'or' | 'and';
    ignoreCase?: boolean;

    targetVar?: 'boolean' | 'number' | 'date' | 'arrstring';
    dateFormat?: string;

    targetFilter?: 'body' | 'param';
  }
}

declare interface IGridTableFilterParsed {
  qParams: {
    [key: string]: any,
  },
  qBody: {
    [key: string]: IApiQueryOptionFilterValue[],
  }
}
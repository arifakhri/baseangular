import * as _ from 'lodash';
import { Injectable, NgZone } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { SelectItem } from 'primeng/components/common/selectitem';

@Injectable()
export class GridTableToggleService {
  constructor(
    private _translate: TranslateService,
    private _ngZone: NgZone,
  ) { }

  mapToggleOptionsFromColumns(tableColumns: IGridTableColumn[]): {
    isActive: Function,
    options: SelectItem[],
    toggled: IGridTableColumn[],
  } {
    const instance: any = {
      toggled: tableColumns,
    };

    instance.options = tableColumns.map(col => {
      return {
        label: this._translate.instant(col.i18nLabel),
        value: col
      };
    });

    instance.isActive = (o) => {
      return Boolean(_.find(instance.toggled, o));
    };

    return instance;
  }
}

import { CommonModule } from '@angular/common';
import { DataTableModule } from 'primeng/components/datatable/datatable';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { GridTableColumnComponent } from './grid-table-column.component';

@NgModule({
  imports: [
    CommonModule,
    DataTableModule,
    RouterModule,
    TranslateModule,
  ],
  declarations: [
    GridTableColumnComponent,
  ],
  exports: [
    GridTableColumnComponent,
  ],
})
export class GridTableColumnModule { }

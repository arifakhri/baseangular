declare interface IGridTableColumn {
  dateFormat?: string;
  field: string;
  sortField?: string;  
  formatter?: Function;
  i18nLabel: string;
  link?: Function;
  columnClasses?: string;
  classes?: Function;
  sort?: boolean;
  template?: any;
}
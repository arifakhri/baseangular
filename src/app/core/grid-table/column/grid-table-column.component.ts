import * as _ from 'lodash';
import * as moment from 'moment';
import { AfterViewInit, ChangeDetectorRef, Component, Input, OnInit, ViewChild, ViewContainerRef } from '@angular/core';

@Component({
  selector: 'app-grid-table-column',
  templateUrl: 'grid-table-column.component.html'
})
export class GridTableColumnComponent implements AfterViewInit, OnInit {
  @Input() field: any;
  @Input() record: any;
  @Input() tableColumn: IGridTableColumn;

  @ViewChild('dynamicTemplate', { read: ViewContainerRef }) elDynamicTemplate;

  value: any;

  constructor(
    private _changeDetectorRef: ChangeDetectorRef,
  ) { }

  ngOnInit() {
    const rawValue = _.get(this.record, this.field);
    if (this.tableColumn.formatter) {
      this.value = this.tableColumn.formatter(rawValue, this.record);
    } else if (this.tableColumn.dateFormat) {
      const targetDate = moment(rawValue);
      this.value = targetDate.isValid() ? targetDate.format(this.tableColumn.dateFormat) : rawValue;
    } else {
      this.value = rawValue;
    }
  }

  ngAfterViewInit() {
    if (this.elDynamicTemplate && this.tableColumn.template) {
      this.elDynamicTemplate.createEmbeddedView(this.tableColumn.template, {
        $implicit: this.tableColumn,
        parsedValue: this.value,
        record: this.record,
      });

      this._changeDetectorRef.detectChanges();
    }
  }
}

import { ModuleWithProviders, NgModule } from '@angular/core';

import { AdminLayoutModule, AdminLayoutService } from './admin/admin-layout.module';
import { BasicLayoutModule } from './basic/basic-layout.module';

@NgModule({
  imports: [
    AdminLayoutModule,
    BasicLayoutModule,
  ],
  exports: [
    AdminLayoutModule,
    BasicLayoutModule,
  ]
})
export class LayoutsModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: LayoutsModule,
      providers: [
        AdminLayoutService,
      ]
    };
  }
}

export * from './admin/admin-layout.module';
export * from './basic/basic-layout.module';

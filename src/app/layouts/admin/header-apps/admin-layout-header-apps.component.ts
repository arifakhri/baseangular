import { Component, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

import { AuthenticationService } from '../../../core/core.module';
import { PagetitleService } from '../../../modules/ng2centric/core/pagetitle/pagetitle.service';
import { SettingsService } from '../../../modules/ng2centric/shared/settings/settings.service';
import { TransactionQueryService } from '../../../main/transaction/transaction-query.service';
import { TranslatorService } from '../../../core/common/translator.service';

@Component({
  selector: 'app-admin-layout-header-apps',
  templateUrl: 'admin-layout-header-apps.component.html',
  styleUrls: ['../header/admin-layout-header.component.scss', '../header/admin-layout-header.menu-links.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AdminLayoutHeaderAppsComponent {

  sidebarVisible: true;
  sidebarOffcanvasVisible: boolean;

  constructor(
    public _authentication: AuthenticationService,
    public _pageTitle: PagetitleService,
    private _router: Router,
    public _settings: SettingsService,
    private _transactionQuery: TransactionQueryService,
    public _translate: TranslateService,
    public _translator: TranslatorService,
  ) { }

  toggleSidebarOffcanvasVisible() {
    this._settings.app.sidebar.offcanvasVisible = !this._settings.app.sidebar.offcanvasVisible;
  }

  toggleSidebar(state) {
    //  state === true -> open
    //  state === false -> close
    //  state === undefined -> toggle
    this._settings.app.sidebar.visible = typeof state !== 'undefined' ? state : !this._settings.app.sidebar.visible;
  }

  onSearch(ev: any) {
    this._transactionQuery.lastQuery = ev.target.value;
    this._router.navigateByUrl('/empty', { skipLocationChange: true }).then(() => {
      this._router.navigateByUrl('/transactions');
      ev.target.value = '';
    });
  }
}

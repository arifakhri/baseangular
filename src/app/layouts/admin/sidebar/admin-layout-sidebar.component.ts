import { Component, Injector, OnInit, ViewEncapsulation } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';

import { AuthenticationService } from '../../../core/core.module';
import { MenuService } from '../../../modules/ng2centric/core/menu/menu.service';
import { SettingsService } from '../../../modules/ng2centric/shared/settings/settings.service';

@Component({
  selector: 'app-admin-layout-sidebar',
  templateUrl: 'admin-layout-sidebar.component.html',
  styleUrls: ['admin-layout-sidebar.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AdminLayoutSidebarComponent implements OnInit {

  menu: Array<any>;
  router: Router;
  sidebarOffcanvasVisible: boolean;

  constructor(
    public _authentication: AuthenticationService,
    private menuService: MenuService,
    public settings: SettingsService,
    private injector: Injector
  ) {
    this.menu = menuService.getMenu();
  }

  ngOnInit() {
    this.router = this.injector.get(Router);
    this.router.events
      .filter(event => event instanceof NavigationEnd)
      .subscribe((event) => {
        this.settings.app.sidebar.visible = false;
      });
  }

  closeSidebar() {
    this.settings.app.sidebar.visible = false;
  }

  handleSidebar(event) {
    if (this.isSidebarCollapsed()) {
      const item = this.getItemElement(event);
      // check click is on a tag
      if (!item) {
        return;
      }

      const ele = $(item),
        liparent = ele.parent()[0];

      const lis = ele.parent().parent().children(); // markup: ul > li > a
      // remove .active from childs
      lis.find('li').removeClass('active');
      // remove .active from siblings ()
      $.each(lis, function (key, li) {
        if (li !== liparent) {
          $(li).removeClass('active');
        }
      });

      const next = ele.next();
      if (next.length && next[0].tagName === 'UL') {
        ele.parent().toggleClass('active');
        event.preventDefault();
      }
    } else {
      this.toggleSubmenuHover(event);
    }
  }

  // find the a element in click context
  // doesn't check deeply, asumens two levels only
  getItemElement(event) {
    const element = event.target,
      parent = element.parentNode;
    if (element.tagName.toLowerCase() === 'a') {
      return element;
    }
    if (parent.tagName.toLowerCase() === 'a') {
      return parent;
    }
    if (parent.parentNode.tagName.toLowerCase() === 'a') {
      return parent.parentNode;
    }
  }

  isSidebarCollapsed() {
    return this.settings.app.sidebar.offcanvasVisible;
  }
  removeFloatingNav() {
    $('.nav-floating').remove();
  }

  toggleSubmenuHover(event, hasChildren?: boolean) {
    const self = this;
    if (!this.isSidebarCollapsed()) {
      event.preventDefault();
      this.removeFloatingNav();

      const target = $(event.target || event.srcElement || event.currentTarget);
      let ul, anchor = target;
      // find the UL
      if (!target.is('a')) {
        anchor = target.parent('a');
      }
      ul = anchor.next();

      if (!ul.length) {
        return; // if not submenu return
      }

      const $aside = $('.sidebar-container');
      const $asideInner = $aside.children('.sidebar-content'); // for top offset calculation
      const $sidebar = $asideInner.children('.sidebar-nav');
      const mar = parseInt($asideInner.css('padding-top'), 0) + parseInt($aside.css('padding-top'), 0);
      const itemTop = ((anchor.parent().position().top) + mar) - $sidebar.scrollTop();

      const floatingNav = ul.clone().appendTo($aside);
      const vwHeight = $(window).height();

      floatingNav
        .removeClass('active')
        .addClass('nav-floating')
        .css({
          position: this.settings.app.isFixed ? 'fixed' : 'absolute',
          top: itemTop,
          bottom: (floatingNav.outerHeight(true) + itemTop > vwHeight) ? 0 : 'auto',
          left: '70px',
          zIndex: 99
        });

      floatingNav
        .on('mouseleave', () => { floatingNav.remove(); })
        .find('a').on('click', function (e) {
          e.preventDefault(); // prevents page reload on click
          // get the exact route path to navigate
          self.router.navigate([$(this).attr('route')]);
        });
    } else if (hasChildren) {
      this.handleSidebar(event);
    }

  }
}

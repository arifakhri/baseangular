import { BsDropdownModule } from 'ngx-bootstrap';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { AdminLayoutComponent } from './admin-layout.component';
import { AdminLayoutHeaderComponent } from './header/admin-layout-header.component';
import { AdminLayoutHeaderQuickAddComponent } from './header/quickadd/admin-layout-header-quickadd.component';
import { AdminLayoutSidebarComponent } from './sidebar/admin-layout-sidebar.component';

import { CoreModule } from '../../core/core.module';
import { SharedModule } from '../../shared/shared.module';
import { SharedModule as CentricSharedModule } from '../../modules/ng2centric/shared/shared.module';

@NgModule({
  imports: [
    BsDropdownModule,
    CommonModule,
    RouterModule,
    TranslateModule.forChild(),
    CoreModule,
    SharedModule,
    CentricSharedModule
  ],
  declarations: [
    AdminLayoutComponent,
    AdminLayoutHeaderComponent,
    AdminLayoutHeaderQuickAddComponent,
    AdminLayoutSidebarComponent
  ],
  exports: [
    AdminLayoutComponent
  ]
})
export class AdminLayoutModule { }

export * from './admin-layout.component';
export * from './admin-layout.service';

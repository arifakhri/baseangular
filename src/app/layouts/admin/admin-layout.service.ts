import { Injectable } from '@angular/core';
import { NavigationStart, Router } from '@angular/router';

import { UrlService } from '../../core/core.module';

@Injectable()
export class AdminLayoutService {
  constructor(
    private _router: Router,
    private _url: UrlService,
  ) {
    this._router.events.filter(e => e instanceof NavigationStart).subscribe(e => {
      const url = this._url.extractHashParts();
      if ((<any>e).url !== url.urlPath) {
        this.containerType = 0;
      }
    });
  }
  set containerType(typeId: number) {
    let contentContainer;
    switch (typeId) {
      case 1:
        contentContainer = 'container container-fluid';
        break;
      default:
        contentContainer = 'container container-lg';
        break;
    }
    $('main.main-container > section').eq(0).attr('class', contentContainer);
  }
}

import { Component, ViewEncapsulation } from '@angular/core';

import { SettingsService } from '../../modules/ng2centric/shared/settings/settings.service';

@Component({
  selector: 'app-admin-layout',
  templateUrl: 'admin-layout.component.html',
  styleUrls: ['admin-layout.component.scss', 'admin-layout-variants.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AdminLayoutComponent {
  constructor(
    private _settings: SettingsService
  ) { }

  layout() {
    return [
      this._settings.app.sidebar.visible ? 'sidebar-visible' : '',
      this._settings.app.sidebar.offcanvas ? 'sidebar-offcanvas' : '',
      this._settings.app.sidebar.offcanvasVisible ? 'offcanvas-visible' : ''
    ].join(' ');
  }

  closeSidebar() {
    this._settings.app.sidebar.visible = false;
  }
}

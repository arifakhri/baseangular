import { Component } from '@angular/core';

@Component({
  selector: 'app-basic-layout',
  styleUrls: ['./basic-layout.component.scss'],
  templateUrl: './basic-layout.component.html',
})
export class BasicLayoutComponent { }

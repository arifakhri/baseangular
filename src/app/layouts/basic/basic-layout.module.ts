import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';

import { BasicLayoutComponent } from './basic-layout.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    SharedModule,
  ],
  declarations: [
    BasicLayoutComponent,
  ],
  exports: [
    BasicLayoutComponent,
  ],
})
export class BasicLayoutModule { }

export * from './basic-layout.component';

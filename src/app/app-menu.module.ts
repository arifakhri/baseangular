import { NgModule } from '@angular/core';

import { MenuService } from './modules/ng2centric/core/menu/menu.service';

const menu = [{
  i18nName: 'ui.menu.dashboard',
  link: '/dashboard',
  imgpath: '/assets/img/sidemenu/dashboard.svg'
}, {
  i18nName: 'ui.menu.cashAndBank',
  link: '/cash-bank',
  imgpath: '/assets/img/sidemenu/cash-and-bank.svg'
}, {
  i18nName: 'ui.menu.sales',
  link: '/sales/all',
  imgpath: '/assets/img/sidemenu/sales.svg',
}, {
  i18nName: 'ui.menu.purchases',
  link: '/purchases/all',
  imgpath: '/assets/img/sidemenu/purchases.svg',
}, {
  i18nName: 'ui.menu.expenses',
  link: '/expenses',
  imgpath: '/assets/img/sidemenu/expenses.svg',
  separator: true,
}, {
  i18nName: 'ui.menu.contacts',
  link: '/contacts/all',
  imgpath: '/assets/img/sidemenu/contacts.svg'
}, {
  i18nName: 'ui.menu.products',
  link: '/products/all',
  imgpath: '/assets/img/sidemenu/products.svg',
}, {
  i18nName: 'ui.menu.accounts',
  link: '/accounts',
  imgpath: '/assets/img/sidemenu/accounts.svg',
}, {
  i18nName: 'ui.menu.otherLists',
  link: '/other-lists',
  imgpath: '/assets/img/sidemenu/other-lists.svg',
  separator: true,
}, {
  i18nName: 'ui.menu.reports',
  link: '/reports',
  imgpath: '/assets/img/sidemenu/reports.svg',
}];

@NgModule()
export class AppMenuModule {
  constructor(
    private _menu: MenuService,
  ) {
    this._menu.addMenu(menu);
  }
}

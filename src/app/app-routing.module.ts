import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

import { AdminLayoutComponent, BasicLayoutComponent } from './layouts/layouts.module';

import { AppBootstrapRoutingModule } from './shared/app-bootstrap/app-bootstrap-routing.module';

export const routes: Routes = [{
  path: '',
  children: [{
    path: 'empty',
    loadChildren: './shared/empty/empty-lazy.module#EmptyLazyModule'
  }, {
    path: 'error',
    loadChildren: './shared/error/error-lazy.module#ErrorLazyModule'
  }, {
    path: 'invitation',
    loadChildren: './main/invitation/invitation-lazy.module#InvitationLazyModule'
  }, {
    path: 'logout',
    loadChildren: './main/logout/logout-lazy.module#LogoutLazyModule'
  }, {
    path: 'account',
    loadChildren: './main/account/account-lazy.module#AccountLazyModule'
  }, {
    path: '',
    component: BasicLayoutComponent,
    children: [{
      path: '',
      redirectTo: 'login',
      pathMatch: 'full'
    }, {
      path: 'registration',
      loadChildren: './main/registration/registration-lazy.module#RegistrationLazyModule'
    }, {
      path: 'login',
      loadChildren: './main/login/login-lazy.module#LoginLazyModule'
    }, {
      path: 'account',
      loadChildren: './main/account/account-lazy.module#AccountLazyModule'
    }]
  }, {
    path: 'apps',
    component: BasicLayoutComponent,
    children: [{
      path: 'bot',
      loadChildren: './main/chatbot/chatbot-lazy.module#ChatbotLazyModule'
    }]
  }, {
    path: '',
    component: AdminLayoutComponent,
    children: [{
      path: 'accounts',
      loadChildren: './main/coa/coa-lazy.module#COALazyModule'
    }, {
      path: 'cash-bank',
      loadChildren: './main/cash-bank/cash-bank-lazy.module#CashBankLazyModule'
    }, {
      path: 'contacts',
      loadChildren: './main/contact/contact-lazy.module#ContactLazyModule'
    }, {
      path: 'dashboard',
      loadChildren: './main/dashboard/dashboard-lazy.module#DashboardLazyModule'
    }, {
      path: 'expenses',
      loadChildren: './main/expense/expense-lazy.module#ExpenseLazyModule'
    }, {
      path: 'inventory-adjustments',
      loadChildren: './main/stock-opname/stock-opname-lazy.module#StockOpnameLazyModule'
    }, {
      path: 'journal-entries',
      loadChildren: './main/journal-entry/journal-entry-lazy.module#JournalEntryLazyModule'
    }, {
      path: 'other-lists',
      loadChildren: './main/other-lists/other-lists-lazy.module#OtherListsLazyModule'
    }, {
      path: 'payment-methods',
      loadChildren: './main/payment-method/payment-method-lazy.module#PaymentMethodLazyModule'
    }, {
      path: 'price-levels',
      loadChildren: './main/price-level/price-level-lazy.module#PriceLevelLazyModule'
    }, {
      path: 'products',
      loadChildren: './main/product/product-lazy.module#ProductLazyModule'
    }, {
      path: 'product-attributes',
      loadChildren: './main/product-attribute/product-attribute-lazy.module#ProductAttributeLazyModule'
    }, {
      path: 'product-brands',
      loadChildren: './main/product-brand/product-brand-lazy.module#ProductBrandLazyModule'
    }, {
      path: 'product-categories',
      loadChildren: './main/product-category/product-category-lazy.module#ProductCategoryLazyModule'
    }, {
      path: 'product-tags',
      loadChildren: './main/product-tag/product-tag-lazy.module#ProductTagLazyModule'
    }, {
      path: 'profile',
      loadChildren: './main/profile/profile-lazy.module#ProfileLazyModule'
    }, {
      path: 'purchases',
      loadChildren: './main/purchases/purchases-lazy.module#PurchasesLazyModule'
    }, {
      path: 'receive-moneys',
      loadChildren: './main/receive-money/receive-money-lazy.module#ReceiveMoneyLazyModule'
    }, {
      path: 'reports',
      loadChildren: './main/reports/reports-lazy.module#ReportsLazyModule'
    }, {
      path: 'reports/ap-aging-summary',
      loadChildren: './main/reports-ap-aging/reports-ap-aging-lazy.module#ReportsAPAgingLazyModule'
    }, {
      path: 'reports/ar-aging-summary',
      loadChildren: './main/reports-ar-aging/reports-ar-aging-lazy.module#ReportsARAgingLazyModule'
    }, {
      path: 'reports/balance-sheet',
      loadChildren: './main/reports-balance/reports-balance-lazy.module#ReportsBalanceLazyModule'
    }, {
      path: 'reports/expenses',
      loadChildren: './main/reports-expense/reports-expense-lazy.module#ReportsExpenseLazyModule'
    }, {
      path: 'reports/expenses-detail',
      loadChildren: './main/reports-expense-detail/reports-expense-detail-lazy.module#ReportsExpenseDetailLazyModule'
    }, {
      path: 'reports/inventory',
      loadChildren: './main/reports-inventory/reports-inventory-lazy.module#ReportsInventoryLazyModule'
    }, {
      path: 'reports/purchases',
      loadChildren: './main/reports-purchases/reports-purchases-lazy.module#ReportsPurchasesLazyModule'
    }, {
      path: 'reports/purchases-detail',
      loadChildren: './main/reports-purchases-detail/reports-purchases-detail-lazy.module#ReportsPurchasesDetailLazyModule'
    }, {
      path: 'reports/purchases-order',
      loadChildren: './main/reports-purchases-order/reports-purchases-order-lazy.module#ReportsPurchasesOrderLazyModule'
    }, {
      path: 'reports/purchases-order-detail',
      loadChildren: './main/reports-purchases-order-detail/reports-purchases-order-detail-lazy.module#ReportsPurchasesOrderDetailLazyModule'
    }, {
      path: 'reports/purchases-vendor',
      loadChildren: './main/reports-purchases-vendor/reports-purchases-vendor-lazy.module#ReportsPurchasesVendorLazyModule'
    }, {
      path: 'reports/purchases-product',
      loadChildren: './main/reports-purchases-product/reports-purchases-product-lazy.module#ReportsPurchasesProductLazyModule'
    }, {
      path: 'reports/pnl',
      loadChildren: './main/reports-pnl/reports-pnl-lazy.module#ReportsPNLLazyModule'
    }, {
      path: 'reports/sales',
      loadChildren: './main/reports-sales/reports-sales-lazy.module#ReportsSalesLazyModule'
    }, {
      path: 'reports/sales-detail',
      loadChildren: './main/reports-sales-detail/reports-sales-detail-lazy.module#ReportsSalesDetailLazyModule'
    }, {
      path: 'reports/sales-order',
      loadChildren: './main/reports-sales-order/reports-sales-order-lazy.module#ReportsSalesOrderLazyModule'
    },
    {
      path: 'reports/sales-customer',
      loadChildren: './main/reports-sales-customer/reports-sales-customer-lazy.module#ReportsSalesCustomerLazyModule'
    },
    {
      path: 'reports/sales-product',
      loadChildren: './main/reports-sales-product/reports-sales-product-lazy.module#ReportsSalesProductLazyModule'
    },
    {
      path: 'reports/sales-order-detail',
      loadChildren: './main/reports-sales-order-detail/reports-sales-order-detail-lazy.module#ReportsSalesOrderDetailLazyModule'
    }, {
      path: 'sales',
      loadChildren: './main/sales/sales-lazy.module#SalesLazyModule'
    }, {
      path: 'settings',
      loadChildren: './main/settings/settings-lazy.module#SettingsLazyModule'
    }, {
      path: 'settings/accounting',
      loadChildren: './main/settings-accounting/settings-accounting-lazy.module#SettingsAccountingLazyModule'
    }, {
      path: 'settings/chatbot',
      loadChildren: './main/settings-chatbot/settings-chatbot-lazy.module#SettingsChatbotLazyModule'
    }, {
      path: 'settings/company',
      loadChildren: './main/settings-company/settings-company-lazy.module#SettingsCompanyLazyModule'
    }, {
      path: 'settings/integration',
      loadChildren: './main/settings-integration/settings-integration-lazy.module#SettingsIntegrationLazyModule'
    }, {
      path: 'settings/product',
      loadChildren: './main/settings-product/settings-product-lazy.module#SettingsProductLazyModule'
    }, {
      path: 'settings/purchase',
      loadChildren: './main/settings-purchase/settings-purchase-lazy.module#SettingsPurchaseLazyModule'
    }, {
      path: 'settings/sales',
      loadChildren: './main/settings-sales/settings-sales-lazy.module#SettingsSalesLazyModule'
    }, {
      path: 'settings/saleschannel',
      loadChildren: './main/settings-sales-channel/settings-sales-channel-lazy.module#SettingsSalesChannelLazyModule'
    }, {
      path: 'settings/users',
      loadChildren: './main/settings-user/settings-user-lazy.module#SettingsUserLazyModule'
    }, {
      path: 'shipping-methods',
      loadChildren: './main/shipping-method/shipping-method-lazy.module#ShippingMethodLazyModule'
    }, {
      path: 'spend-moneys',
      loadChildren: './main/spend-money/spend-money-lazy.module#SpendMoneyLazyModule'
    }, {
      path: 'taxes',
      loadChildren: './main/tax/tax-lazy.module#TaxLazyModule'
    }, {
      path: 'transactions',
      loadChildren: './main/transaction/transaction-lazy.module#TransactionLazyModule'
    }, {
      path: 'uom',
      loadChildren: './main/uom/uom-lazy.module#UOMLazyModule'
    }, {
      path: 'warehouses',
      loadChildren: './main/warehouse/warehouse-lazy.module#WarehouseLazyModule'
    }, {
      path: 'warehouse-transfers',
      loadChildren: './main/warehouse-transfer/warehouse-transfer-lazy.module#WarehouseTransferLazyModule'
    }]
  }]
}];

@NgModule({
  imports: [
    AppBootstrapRoutingModule,
    RouterModule.forRoot(routes, {
      preloadingStrategy: PreloadAllModules,
      onSameUrlNavigation: 'reload',
    }),
  ],
  exports: [
    AppBootstrapRoutingModule,
    RouterModule,
  ]
})
export class AppRoutingModule { }

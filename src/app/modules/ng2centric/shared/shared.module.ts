import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { ModalModule } from 'ngx-bootstrap/modal';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { SettingsComponent } from './settings/settings.component';

import { CENTRIC_DIRECTIVES } from './directives';
import { SettingsService } from './settings/settings.service';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        HttpModule,
        ModalModule,
        ReactiveFormsModule,
        TranslateModule.forChild(),
    ],
    providers: [
        SettingsService
    ],
    declarations: [
        ...CENTRIC_DIRECTIVES,
        SettingsComponent,
    ],
    exports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule,
        HttpModule,
        ...CENTRIC_DIRECTIVES,
        SettingsComponent,
    ]
})
export class SharedModule { }

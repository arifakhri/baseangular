import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';

import { SettingsService } from './settings.service';
import { TranslatorService } from '../../../../core/common/translator.service';

import * as screenfull from 'screenfull';

@Component({
    selector: 'app-settings',
    templateUrl: './settings.component.html',
    styleUrls: ['./settings.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class SettingsComponent implements OnInit {

    @ViewChild('fsbutton') fsbutton;
    @ViewChild('settingsModal') public settingsModal: ModalDirective;

    constructor(public settings: SettingsService, public translator: TranslatorService) { }

    ngOnInit() { }

    updateTheme(theme) {
        $('body')
            .removeClass((index, css) => (css.match(/(^|\s)theme-\S+/g) || []).join(' '))
            .addClass(this.settings.getSetting('theme'));
    }

    toggleFullScreen() {

        if (screenfull.enabled) {
            screenfull.toggle();
        }

    }

}
